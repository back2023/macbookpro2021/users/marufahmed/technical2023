
https://github.com/ECMWFCode4Earth/DeepR

" In response to this pressing challenge, the DeepR project, led by Antonio Pérez, Mario Santa Cruz, and Javier Diez, was conceived and executed with the aim of downscaling ERA5 data to a finer resolution, thus enabling enhanced accuracy and applicability across a wide range of industries and research domains."


