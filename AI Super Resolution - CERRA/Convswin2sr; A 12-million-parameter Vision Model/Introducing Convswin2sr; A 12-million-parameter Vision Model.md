
https://predictia.es/en/deepr-12-million-vision-model

# We use Big Data and Machine Learning to develop solutions in sectors such as climate, meteorology and health
https://predictia.es/

### Reanalysis Downscaling
"[C3S defines reanalysis](https://climate.copernicus.eu/reanalysis-qas) as a scientific method which aims to estimate weather conditions for each day over a period as accurately as possible."

"In ERA5, the spatial resolution is about 30x30km which means that any single point value is a representation of the combined environment of all atmosphere states within that 30x30 km area."

"On the other hand, CERRA is a limited-area reanalysis covering Europe with a higher spatial resolution, 5.5 km, and it is forced by ERA5. CERRA is based on the HARMONIE-ALADIN data assimilation and is also published by C3S."

"ConvSwin2SR will be trained to take ERA5 temperature as input data while its corresponding CERRA field will be used as target."

### Data sources

"The models take ERA5 fields as input and try to produce CERRA-like fields."

#### ERA5

"An uncertainty estimate is sampled by an underlying 10-member ensemble at three-hourly intervals. Ensemble mean and spread have been pre-computed for convenience."

"To facilitate many climate applications, monthly-mean averages have been pre-calculated too, though monthly means are not available for the ensemble mean and spread", C3S explained."

"The input grids are structured as 4D tensors with shapes: `(batch size, 1, 60, 44)`"

"...where 60 and 44 are the number of grid points along the longitude and latitude axes, respectively. Geographically, the ERA5 data covers a region extending from a westernmost point at longitude -8.35 to an easternmost point at longitude 6.6, and from a northernmost point at latitude 46.45 to a southernmost point at latitude 35.50."

#### CERRA
"The CERRA system is based on the HARMONIE-ALADIN data assimilation system which has been developed and used within the ACCORD consortium."
"It is implemented and optimised for the entire European area with surrounding sea areas with a horizontal resolution of 5.5 km and 106 vertical levels."
"They are represented as 4D tensors with larger dimensions of: `(batch size, 1, 240, 160)`"
"The geographical coverage for these high-resolution grids extends from a westernmost point at longitude -6.85 to an easternmost point at longitude 5.1, and from a northernmost point at latitude 44.95 to a southernmost point at latitude 37."

### Data Processing

"The methodology employed in each step is discussed comprehensively in the following paragraphs:"
"**Unit Standardisation**:"
"**Coordinate System Rectification**:"
"The longitude values were adjusted to range from -180 to 180 instead of the initial 0 to 360 range, while latitude values were ordered in ascending order,"
"**Grid Interpolation**: The ERA5 dataset is structured on a regular grid with a spatial resolution of 0.25°, whereas the CERRA dataset inhabits a curvilinear grid with a Lambert Conformal projection of higher spatial resolution (0.05º). To overcome this disparity in the grid system, a grid interpolation procedure is performed. This step is crucial in order to align the datasets onto a common format, a regular grid (with different spatial resolutions), thereby ensuring consistency in spatial representation. The interpolation transformed the CERRA dataset to match the regular grid structure of the ERA5 dataset, keeping its initial spatial resolution of 0.05° (5.5 km)."


#### Model architecture
"The [transformers.Swin2SRModel](https://huggingface.co/docs/transformers/model_doc/swin2sr#transformers.Swin2SRModel) is central to our model. This component upsamples the spatial resolution of its inputs by a factor of 8."

"As Swin2SRModel only supports upscaling ratios that are powers of 2, and the real upscale ratio is ~5, a Convolutional Neural Network is included as a preprocessing module. This module converts the inputs into a (20, 30) feature map that can be fed to the Swin2SRModel to match the output shape of the region (160, 240)."

#### Model size
"The specific parameters of this network are available in [config.json](https://huggingface.co/predictia/convswin2sr_mediterranean/blob/main/config.json), corresponding to DL of 12,383,377 parameters."

#### Training details
"The ConvSwin2SR model is trained over 100 epochs, clocking in at 3,648 days with a batch size of 2 accumulating the gradients for 4 batches before updating the model weights."

"The training duration spans 29 years, commencing in January 1985 and culminating in December 2013. Sequentially, the validation phase begins, covering the period from January 2014 to December 2017. This 4-year interval is solely dedicated to evaluating the model's aptitude on data it hasn't been exposed to during training."

"The ConvSwin2SR model demonstrates a commendable inference speed. Specifically, when tasked with downscaling 248 samples, which equals an entire month of ERA5 samples at 3-hour intervals, the model completes the operation in 21 seconds."
"This level of efficiency is observed in a local computing environment outfitted with 16GB of RAM and 4GB of GPU memory."

#### Hardware
"For our project, we have deployed two virtual machines (VM), each featuring a dedicated GPU. One VM is equipped with a 16GB GPU, while the other boasts a more substantial 20GB GPU."

#### Software
"The code used to download and prepare the data, and train and evaluate this model is freely available through the GitHub Repository [ECMWFCode4Earth/DeepR](https://github.com/ECMWFCode4Earth/DeepR) hosted by the [ECMWF Code 4 Earth](http://codeforearth.ecmwf.int/) organisation."





