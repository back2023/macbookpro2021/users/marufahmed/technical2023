
https://huggingface.co/bigscience/bloom


BigScience Large Open-science Open-access Multilingual Language Model  
Version 1.3 / 6 July 2022

Current Checkpoint: **Training Iteration 95000**

Link to paper: [here](https://arxiv.org/abs/2211.05100)

Total seen tokens: **366B**

---

# [](https://huggingface.co/bigscience/bloom#model-details)Model Details

BLOOM is an autoregressive Large Language Model (LLM), trained to continue text from a prompt on vast amounts of text data using industrial-scale computational resources. As such, it is able to output coherent text in 46 languages and 13 programming languages that is hardly distinguishable from text written by humans. BLOOM can also be instructed to perform text tasks it hasn't been explicitly trained for, by casting them as text generation tasks.









