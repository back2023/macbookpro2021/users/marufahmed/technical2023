

https://github.com/Snowflake-Labs/llm-bootcamp-build-2023

# BUILD LLM Bootcamp 2023

This repository contains instrcutions, code and data required to successfully complete LLM Bootcamp for BUILD 2023.

### [](https://github.com/Snowflake-Labs/llm-bootcamp-build-2023#day-1-deploy-llama-2-from-hugging-face-in-snowpark-container-services)Day 1: Deploy Llama 2 from Hugging Face in Snowpark Container Services

Learn more in the [README](https://github.com/Snowflake-Labs/llm-bootcamp-build-2023/blob/main/day1/README.md).

### [](https://github.com/Snowflake-Labs/llm-bootcamp-build-2023#day-2-fine-tune-and-deploy-llama-2-in-snowpark-container-services)Day 2: Fine-tune and Deploy Llama 2 in Snowpark Container Services

Learn more in the [README](https://github.com/Snowflake-Labs/llm-bootcamp-build-2023/blob/main/day2/README.md).


