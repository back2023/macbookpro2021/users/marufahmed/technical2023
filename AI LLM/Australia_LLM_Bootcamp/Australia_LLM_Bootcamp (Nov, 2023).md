
# 2024.02.28

Stored in scratch
```bash
/scratch/fp0/mah900/Australia_LLM_Bootcamp

total 21G
drwxr-sr-x  8 mah900 fp0 4.0K Feb 28 09:36 workspace-nlp-llm
-rw-r--r--  1 mah900 fp0  21G Jan 23 23:20 nemo_llm_23_02.simg.tar
-rw-r--r--  1 mah900 fp0 449M Jan 23 23:20 workspace-nlp-llm.tar
drwxr-sr-x 19 mah900 fp0 4.0K Jan 23 23:17 nemo_llm_23_02.simg
-rw-r--r--  1 mah900 fp0  53K Jan 23 23:17 slurm-8095.out
```


# 2024.10.09

Moving the files from scratch to wb00 to save space
~~Use rsync~~

```
screen -S sess_A_LLM
mv /scratch/fp0/mah900/Australia_LLM_Bootcamp /g/data/wb00/admin/testing/mah900
```

Also, removing nemo on 2024.10.12
Details in the note [2024.10.12 (Env export and remove)] under Nvidia/Nemo folder
