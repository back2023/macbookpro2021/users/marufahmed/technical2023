
https://medium.com/data-science-at-microsoft/efficient-transformers-survey-of-recent-work-75022cddc86a


Transformers such as [Bidirectional Encoder Representations from Transformers (BERT)](https://azure.microsoft.com/en-us/blog/microsoft-makes-it-easier-to-build-popular-language-representation-model-bert-at-large-scale/) and [Turing Natural Language Generation (T-NLG) from Microsoft](https://turing.microsoft.com/) have recently become popular in the Machine Learning world for Natural Language Processing (NLP) tasks such as machine translation, text summarization, question answering, protein fold prediction, and even image processing tasks.





