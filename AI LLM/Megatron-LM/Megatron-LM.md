
https://huggingface.co/docs/accelerate/usage_guides/megatron_lm

[Megatron-LM](https://github.com/NVIDIA/Megatron-LM) enables training large transformer language models at scale. It provides efficient tensor, pipeline and sequence based model parallelism for pre-training transformer based Language Models such as [GPT](https://arxiv.org/abs/2005.14165) (Decoder Only), [BERT](https://arxiv.org/pdf/1810.04805.pdf) (Encoder Only) and [T5](https://arxiv.org/abs/1910.10683) (Encoder-Decoder). For detailed information and how things work behind the scene please refer the github [repo](https://github.com/NVIDIA/Megatron-LM).






