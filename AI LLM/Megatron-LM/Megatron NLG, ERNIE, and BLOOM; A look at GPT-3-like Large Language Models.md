
https://indianexpress.com/article/technology/gpt-3-like-llm-large-language-models-ernie-bloom-megatron-8396267/


Evolved over millions of years, modern human language is incredibly complex. The English language alone is comprised of over 1.7 lakh words that include nouns, verbs, adjectives, and a lot more, which can be put together in billions of ways to create unique sentences every time. While communication comes naturally to us – humans are hard-wired to speak and understand speech seamlessly – computers remain limited in their ability to handle language. But thanks to the onset of LLMs (Large Language Models) and (NLP) natural language processing, things are changing.

One of the most popular LLMs in recent times is [OpenAI’s GPT-3/GPT-3.5](https://indianexpress.com/article/explained/explained-sci-tech/what-is-openais-chatgpt-chatbot-why-it-has-become-a-viral-sensation-8309283/), which is the foundation of the AI chatbot ChatGPT. It’s got a lot of people talking due to its capability of producing text that seems as if written by a human, with remarkable accuracy. This breakthrough can be useful for companies that wish to automate tasks, as well as for regular users looking up specific information. But it’s not the only LLM out there – there are several others – NVIDIA’s MT-NLG, for example, is made up of significantly more parameters. Here’s a look at some of the key LLMs.







