
https://awsdocs-neuron.readthedocs-hosted.com/en/latest/frameworks/torch/torch-neuronx/tutorials/training/megatron_lm_gpt.html

GPT is a large language model that excels at many natural language processing (NLP) tasks. It is derived from the decoder part of the Transformer. [Neuron Reference For Megatron-LM [EOS]](https://github.com/aws-neuron/aws-neuron-reference-for-megatron-lm) is a library that enables large-scale distributed training of language models such as GPT and is adapted from [Megatron-LM](https://github.com/NVIDIA/Megatron-LM). This tutorial explains how to run the Neuron reference for Megatron-LM GPT pretraining on Trainium.

The AWS Neuron SDK provides access to Trainium devices through an extension of PyTorch/XLA - a library that includes the familiar PyTorch interface along with XLA-specific additions. For Trainium customers, this means that existing PyTorch training scripts can be executed on Trn1 instances with minimal code modifications. For additional details relating to PyTorch/XLA, please refer to the [official PyTorch/XLA documentation](https://pytorch.org/xla).




