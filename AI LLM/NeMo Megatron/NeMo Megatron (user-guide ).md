

https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/stable/nlp/nemo_megatron/intro.html

Megatron [[nlp-megatron1](https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/stable/nlp/nemo_megatron/intro.html#id4 "Mohammad Shoeybi, Mostofa Patwary, Raul Puri, Patrick LeGresley, Jared Casper, and Bryan Catanzaro. Megatron-lm: training multi-billion parameter language models using model parallelism. arXiv preprint arXiv:1909.08053, 2019.")] is a large, powerful transformer developed by the Applied Deep Learning Research team at NVIDIA. NeMo Megatron supports several types of models:

- GPT-style models (decoder only)
    
- T5/BART/UL2-style models (encoder-decoder)
    
- BERT-style models (encoder only)
    
- RETRO model (decoder only)



