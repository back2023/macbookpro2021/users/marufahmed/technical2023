
https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/main/nlp/megatron.html

Megatron-LM [[nlp-megatron1](https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/main/nlp/nemo_megatron/intro.html#id4 "Mohammad Shoeybi, Mostofa Patwary, Raul Puri, Patrick LeGresley, Jared Casper, and Bryan Catanzaro. Megatron-lm: training multi-billion parameter language models using model parallelism. arXiv preprint arXiv:1909.08053, 2019.")] is a large, powerful transformer developed by the Applied Deep Learning Research team at NVIDIA. Currently NeMo Megatron supports 3 types of models:

- GPT-style models (decoder only)
- T5/BART-style models (encoder-decoder)
- BERT-style models (encoder only)


## Model Parallelism[](https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/main/nlp/megatron.html#model-parallelism "Permalink to this heading")

[Megatron-LM](https://github.com/NVIDIA/Megatron-LM) is a highly optimized and efficient library for training large language models. With Megatron model parallelism, language models can be trained with billions of weights and then used in NeMo for downstream tasks.

NeMo handles pretrained model parallel checkpoints from Megatron-LM automatically and model parallel models in NeMo have the all the same features as other NeMo Models.








