
https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/stable/nlp/nemo_megatron/gpt/gpt_training.html

GPT is a decoder-only Transformer model.

## Quick start[](https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/stable/nlp/nemo_megatron/gpt/gpt_training.html#quick-start "Permalink to this heading")

Steps below demonstrate training of a GPT style model with NeMo



