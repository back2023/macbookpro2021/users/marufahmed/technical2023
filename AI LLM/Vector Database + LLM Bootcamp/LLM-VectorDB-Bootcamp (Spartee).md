
https://github.com/Spartee/LLM-VectorDB-Bootcamp

# Vector Database + LLM Bootcamp

This repository contains the code for the LLM Bootcamp hosted by Data Science Dojo.

## [](https://github.com/Spartee/LLM-VectorDB-Bootcamp#getting-started)Getting Started

1. Sign up for a Redis cloud account (free)
2. Create a Redis database (follow steps in the deck)
3. Run these notebooks locally or in Jupyterhub hosted by DSD
