

https://www.kaggle.com/code/sreevishnudamodaran/stunning-art-with-clip-guided-diffusion-swinir


  
Generate Stunning Artworks with CLIP Guided Diffusion + SwinIR Super Resolution

Create beautiful artworks by fine-tuning diffusion models on custom datasets, and performing CLIP guided text-conditional sampling, followed by SWIN-transformer based super-resolution


