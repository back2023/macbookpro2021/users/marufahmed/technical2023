https://www.kaggle.com/datasets/franciscoescobar/satellite-images-of-water-bodies/code

Images and Masks of Satellite Images of Water Bodies for Image Segmentation

```bash
kaggle datasets download -d franciscoescobar/satellite-images-of-water-bodies
```



# Code

1) https://www.kaggle.com/code/shahriarrafi1071/water-body
   U-Net 
```bash
57/57 ━━━━━━━━━━━━━━━━━━━━ 29s 511ms/step - accuracy: 0.6927 - loss: 0.4443 - val_accuracy: 0.6887 - val_loss: 0.4371
Epoch 24/50
57/57 ━━━━━━━━━━━━━━━━━━━━ 29s 510ms/step - accuracy: 0.6934 - loss: 0.4391 - val_accuracy: 0.6899 - val_loss: 0.4350  
```

2) https://www.kaggle.com/code/someshverma/water-bodies-segmentation-with-unet-and-tensorflow
U-Net/Tensorflow
```bash
2291/2291 [==============================] - 117s 51ms/step - loss: 0.3828 - val_loss: 0.3848
Epoch 10/10
2291/2291 [==============================] - 113s 49ms/step - loss: 0.3644 - val_loss: 0.3793
```

3) https://www.kaggle.com/code/anudeepanu/water-bodies-segmentation

```
Epoch 58/60
91/91 [==============================] - 113s 1s/step - loss: 0.1412 - accuracy: 0.8143 - val_loss: 0.3213 - val_accuracy: 0.7731
Epoch 59/60
91/91 [==============================] - 112s 1s/step - loss: 0.1442 - accuracy: 0.8132 - val_loss: 0.3040 - val_accuracy: 0.7735
Epoch 60/60
91/91 [==============================] - 113s 1s/step - loss: 0.1505 - accuracy: 0.8114 - val_loss: 0.3013 - val_accuracy: 0.7779
```

4) https://www.kaggle.com/code/maxxcuriosity/unet-waterbody-segmentation
```bash
Epoch 9/10
71/71 [==============================] - 215s 3s/step - loss: 0.5392 - accuracy: 0.6098 - val_loss: 0.5119 - val_accuracy: 0.6158
Epoch 10/10
71/71 [==============================] - 206s 3s/step - loss: 0.5269 - accuracy: 0.6195 - val_loss: 0.5008 - val_accuracy: 0.6209
```


5) https://www.kaggle.com/code/prasadmeesala/water-bodies-segmentation
```bash
Epoch 36/50
57/57 [==============================] - 31s 542ms/step - loss: 0.2734 - accuracy: 0.7638 - val_loss: 0.3226 - val_accuracy: 0.7409
Epoch 37/50
57/57 [==============================] - 31s 542ms/step - loss: 0.2623 - accuracy: 0.7674 - val_loss: 0.3351 - val_accuracy: 0.7401
```

6) https://www.kaggle.com/code/yuchen2066/water-prediction
```
The mean accuracy for threshold 240 is 0.27081787109375
The mean accuracy for threshold 245 is 0.27081787109375
The mean accuracy for threshold 250 is 0.27081787109375
Hence, the best threshold is 35, with accuracy score of 0.7434423828125
```

7) https://www.kaggle.com/code/sarthakmodi/image-segementation-using-unet
```bash
Epoch 28/30
142/142 [==============================] - 18s 127ms/step - loss: 0.2861 - accuracy: 0.8085 - val_loss: 0.2985 - val_accuracy: 0.8120
Epoch 29/30
142/142 [==============================] - 18s 126ms/step - loss: 0.2911 - accuracy: 0.8068 - val_loss: 0.2819 - val_accuracy: 0.8139
Epoch 30/30
142/142 [==============================] - 18s 126ms/step - loss: 0.2953 - accuracy: 0.8051 - val_loss: 0.2879 - val_accuracy: 0.8094
```

8) https://www.kaggle.com/code/dipuk0506/background-image-data
No Model

9) https://www.kaggle.com/code/brsdincer/ai-to-hero-set-xiv
No results shown

10) https://www.kaggle.com/code/mathum/mm-deeplabv3-satellite-water-bodies-segmentation
U-Net
```
Epoch 88/90
91/91 [==============================] - 4s 41ms/step - loss: 0.0880 - dice_coefficient: 0.9282 - accuracy: 0.8244 - val_loss: 0.3392 - val_dice_coefficient: 0.8575 - val_accuracy: 0.7817
Epoch 89/90
91/91 [==============================] - 4s 41ms/step - loss: 0.0878 - dice_coefficient: 0.9295 - accuracy: 0.8245 - val_loss: 0.3107 - val_dice_coefficient: 0.8540 - val_accuracy: 0.7828
Epoch 90/90
91/91 [==============================] - 4s 41ms/step - loss: 0.0880 - dice_coefficient: 0.9289 - accuracy: 0.8243 - val_loss: 0.3255 - val_dice_coefficient: 0.8485 - val_accuracy: 0.7799
```

11) https://www.kaggle.com/code/shivamshinde123/water-bodies-image-segmentation-unet
```bash
71/71 [==============================] - 13s 182ms/step - loss: 0.2522 - accuracy: 0.7572 - val_loss: 0.2612 - val_accuracy: 0.7613
Epoch 29/50
71/71 [==============================] - 13s 183ms/step - loss: 0.2441 - accuracy: 0.7603 - val_loss: 0.2667 - val_accuracy: 0.7554
Epoch 30/50
71/71 [==============================] - 13s 183ms/step - loss: 0.2492 - accuracy: 0.7580 - val_loss: 0.2601 - val_accuracy: 0.7577
```

12) https://www.kaggle.com/code/owaistahir/unet-for-satellite-water-bodies-segmentation

```
Epoch 58/60
91/91 [==============================] - 3s 37ms/step - loss: 0.1374 - accuracy: 0.8156 - val_loss: 0.2823 - val_accuracy: 0.7779
Epoch 59/60
91/91 [==============================] - 3s 37ms/step - loss: 0.1406 - accuracy: 0.8148 - val_loss: 0.3356 - val_accuracy: 0.7789
Epoch 60/60
91/91 [==============================] - 3s 38ms/step - loss: 0.1894 - accuracy: 0.8014 - val_loss: 0.3098 - val_accuracy: 0.7690
```

13) https://www.kaggle.com/code/utkarshsaxenadn/water-bodies-segmentation-deeplabv3
Uses pre-trained models
```
Epoch 19/20
62/62 [==============================] - 4s 61ms/step - loss: 0.0940 - val_loss: 0.3284
Epoch 20/20
62/62 [==============================] - 4s 60ms/step - loss: 0.0933 - val_loss: 0.3383
```

14) https://www.kaggle.com/code/tnmthai/apply-mask-on-satellite-images
 Apply mask on Satellite Images
by Thai Tran
Link article on Medium
https://medium.com/@tnmthai/apply-masks-on-satellite-images-1e17e66d6473
This noteboook referred to the following notebook.  
[https://www.kaggle.com/code/tnmthai/apply-mask-on-satellite-images](https://www.kaggle.com/code/tnmthai/apply-mask-on-satellite-images)

15) https://www.kaggle.com/code/harekrshna/water-extent-traffic-flow-prediction
No model

16) https://www.kaggle.com/code/kdkdkd314/waterbody-project
```bash
Epoch 4/5
26/26 [==============================] - 42s 2s/step - loss: 0.0159 - val_loss: 0.0135
Epoch 5/5
26/26 [==============================] - 41s 2s/step - loss: 0.0106 - val_loss: 0.0071
```


17) https://www.kaggle.com/code/stpeteishii/satellite-images-semantic-segmentation-by-u-net
```bash
Epoch 13/40
39/39 [==============================] - 4s 100ms/step - loss: 0.2549 - dice_coef: 0.7451 - binary_accuracy: 0.7978 - val_loss: 0.2766 - val_dice_coef: 0.7230 - val_binary_accuracy: 0.7868
Epoch 14/40
39/39 [==============================] - 4s 92ms/step - loss: 0.2471 - dice_coef: 0.7529 - binary_accuracy: 0.7922 - val_loss: 0.2867 - val_dice_coef: 0.7100 - val_binary_accuracy: 0.7729
```

28) https://www.kaggle.com/code/uraninjo/satellite-water-bodies-pytorch-segmentation
Torch with Pretrained Weigths and Encoder
```bash
Epochs:5
Train_loss --> Dice: 0.21204679794609546 BCE: 0.283726663608104 
Valid_loss --> Dice: 0.21117153101497227 BCE: 0.25665881236394245
Model Saved
```


29) https://www.kaggle.com/code/gauthamkrishnan119/water-body-segmentation-pytorch
Water Body Segmentation - PyTorch
```
Epoch: 29  Train Loss: 0.6197 | Train DICE Coeff: 0.7289 | Val Loss: 0.6378 | Val DICE Coeff: 0.7375

100%|██████████| 30/30 [36:59<00:00, 73.98s/it]

Epoch: 30  Train Loss: 0.6167 | Train DICE Coeff: 0.7276 | Val Loss: 0.6234 | Val DICE Coeff: 0.7350
```


30) https://www.kaggle.com/code/aryaprince/image-segmentation-using-unet
```
Epoch 3/4
133/133 [==============================] - 85s 643ms/step - loss: 0.0461 - iou_coef: 0.2837 - val_loss: 0.0436 - val_iou_coef: 0.3391
Epoch 4/4
133/133 [==============================] - 87s 656ms/step - loss: 0.0438 - iou_coef: 0.2974 - val_loss: 0.0399 - val_iou_coef: 0.3201
```

31) https://www.kaggle.com/code/utkarshsaxenadn/attention-unet-in-keras-explained-unet-comparision
```
Epoch 20/20
72/72 [==============================] - 3s 46ms/step - loss: 0.2312 - accuracy: 0.7473 - IoU: 0.4569 - val_loss: 0.2372 - val_accuracy: 0.7394 - val_IoU: 0.4188
```

This is a **Soft Attention Module** and it is self-explanatory for code look below :
```
Epoch 11/20
72/72 [==============================] - 7s 100ms/step - loss: 0.2749 - accuracy: 0.7288 - IoU: 0.4164 - val_loss: 0.2563 - val_accuracy: 0.7280 - val_IoU: 0.4161
```

32) https://www.kaggle.com/code/utkarshsaxenadn/water-body-segmentation-unet-keras

```
Epoch 14/100
144/144 [==============================] - 4s 28ms/step - loss: 0.4065 - accuracy: 0.6838 - val_loss: 0.4690 - val_accuracy: 0.6154
```

Adaptive Threshold UNET 
```
Epoch 23/100
72/72 [==============================] - 3s 48ms/step - loss: 0.2762 - accuracy: 0.7321 - val_loss: 0.2848 - val_accuracy: 0.7188
```


33) https://www.kaggle.com/code/priyanagda/water-bodies-segmentation-no-filters
```
Epoch: 11
	Train Loss: 0.493
	 Val. Loss: 0.538

Epoch: 12
	Train Loss: 0.484
	 Val. Loss: 0.523
```

33) https://www.kaggle.com/code/priyanagda/water-bodies-segmentation

```
Valid loss improved from inf to 1.1291. Saving checkpoint: ./checkpoint.pth
Epoch: 01
	Train Loss: 0.948
	 Val. Loss: 1.129

Valid loss improved from 1.1291 to 1.0536. Saving checkpoint: ./checkpoint.pth
Epoch: 02
	Train Loss: 0.820
	 Val. Loss: 1.054
```


37) https://www.kaggle.com/code/naubergois/analise-de-satelite-com-self-supervised-learning
```
[Epoch   0] Loss = 0.00 | Collapse Level: 0.46 / 1.00
[Epoch   1] Loss = 0.00 | Collapse Level: 0.46 / 1.00
```

38) https://www.kaggle.com/code/realtimshady/water-bodies-segmentation-with-unet-and-tensorflow

```
2273/2273 [==============================] - 150s 66ms/step - loss: 0.1147 - val_loss: 0.1258
Epoch 98/100
2273/2273 [==============================] - 147s 65ms/step - loss: 0.1141 - val_loss: 0.1250
Epoch 99/100
2273/2273 [==============================] - 151s 66ms/step - loss: 0.1134 - val_loss: 0.1251
Epoch 100/100
2273/2273 [==============================] - 148s 65ms/step - loss: 0.1129 - val_loss: 0.1259
```


39) https://www.kaggle.com/code/aakashgupta38/flooded-land-segmentation

```
2291/2291 [==============================] - 132s 58ms/step - loss: 0.3743 - val_loss: 0.3543
Epoch 9/10
2291/2291 [==============================] - 133s 58ms/step - loss: 0.3463 - val_loss: 0.3513
Epoch 10/10
2291/2291 [==============================] - 129s 56ms/step - loss: 0.3424 - val_loss: 0.3942

```

40) https://www.kaggle.com/code/baranowskibrt/water-bodies-segmentation-with-unet-and-tensorflow

```
Epoch 00032: ReduceLROnPlateau reducing learning rate to 8.100000013655517e-06.
Epoch 33/80
2291/2291 [==============================] - 133s 58ms/step - loss: 0.2301 - val_loss: 0.2236
Epoch 34/80
2291/2291 [==============================] - 132s 57ms/step - loss: 0.2188 - val_loss: 0.2294
```


41) https://www.kaggle.com/code/honorinc/segmentation-plan-d-eau
No training results 

42) https://www.kaggle.com/code/brsdincer/image-colorization-process-auto-encoder
Another dataset

43) https://www.kaggle.com/code/sudhirbussa/waterdetection
No training results

44) https://www.kaggle.com/code/arunpratihast/satellite-images-semantic-segmentation-by-u-net

```
39/39 [==============================] - 4s 79ms/step - loss: 0.2539 - dice_coef: 0.7459 - binary_accuracy: 0.7915 - val_loss: 0.2741 - val_dice_coef: 0.7243 - val_binary_accuracy: 0.7933
Epoch 13/40
39/39 [==============================] - 3s 78ms/step - loss: 0.2497 - dice_coef: 0.7502 - binary_accuracy: 0.7957 - val_loss: 0.2755 - val_dice_coef: 0.7229 - val_binary_accuracy: 0.7845
```



https://www.kaggle.com/code/sweccharanjan/water-extent-by-u-net

```
39/39 [==============================] - 4s 101ms/step - loss: 0.2591 - dice_coef: 0.7409 - binary_accuracy: 0.7861 - val_loss: 0.2835 - val_dice_coef: 0.7175 - val_binary_accuracy: 0.8005
```

Okay, not possible to document all, just start coding 