

```python
tf.keras.layers.Input()
```

"
There's no equivalent in PyTorch to the Keras' Input. All you have to do is pass on the inputs as a tensor to the PyTorch model.
For eg: If you're working with a Conv net:
```python
# Keras Code
input_image = Input(shape=(32,32,3)) # An input image of 32x32x3 (HxWxC)
feature = Conv2D(16, activation='relu', kernel_size=(3, 3))(input_image)
```

```python
# PyTorch code
input_tensor = torch.randn(1, 3, 32, 32) # An input tensor of shape BatchSizexChannelsxHeightxWidth
feature = nn.Conv2d(in_channels=3, out_channels=16, kernel_size=(2, 2))(input_tensor)
```

If it is a normal Dense layer:
```python
# Keras
dense_input = Input(shape=(1,))
features = Dense(100)(dense_input)
```

```python
# PyTorch
input_tensor = torch.tensor([10]) # A 1x1 tensor with value 10
features = nn.Linear(1, 100)(input_tensor)
```
"
Source: https://stackoverflow.com/a/64781026 

***

```python
 tf.keras.layers.Conv2D(64, (3, 3), padding='same', activation='relu')(x)
```

##### Same padding equivalent in Pytorch
Source: https://discuss.pytorch.org/t/same-padding-equivalent-in-pytorch/85121

"PyTorch does not support `same` padding the way Keras does, but still you can manage it easily using explicit padding before passing the tensor to convolution layer.
```python
import torch
import torch.nn.functional as F

x = torch.randn(64, 32, 100, 20)
x = F.pad(x, (0, 0, 2, 1))  # [left, right, top, bot]
nn.Conv2d(32, 32, (4, 1))(x).shape

```
"
Jun 2020
Source:  https://discuss.pytorch.org/t/same-padding-equivalent-in-pytorch/85121/2

"this was solved since Pytorch 1.10.0  
“same” keyword is accepted as input for padding for conv2d"
Oct 2021
Source: https://discuss.pytorch.org/t/same-padding-equivalent-in-pytorch/85121/8

***


```python
tf.keras.layers.MaxPooling2D((2, 2))(enc2)
```

##### Tensorflow vs Pytorch MaxPool
Sorce: https://www.kaggle.com/code/ahmetnihatekici/tensorflow-vs-pytorch-maxpool


***

### Pytorch equivalent of Keras
https://discuss.pytorch.org/t/pytorch-equivalent-of-keras/29412

"
here is the original keras model:
```python
input_shape = (28, 28, 1)
model = Sequential()
model.add(Conv2D(28, kernel_size=(3,3), input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten()) # Flattening the 2D arrays for fully connected layers
model.add(Dense(128, activation=tf.nn.relu))
model.add(Dropout(0.2))
model.add(Dense(10,activation=tf.nn.softmax))
```
I couldn’t find out what should be output channel in Pytorch corespond with this keras mode.
"

"
- The `in_channels` in Pytorch’s `nn.Conv2d` correspond to the number of channels in your input.  
Based on the input shape, it looks like you have 1 channel and a spatial size of `28x28`.
- Also the `Dense` layers in Keras give you the number of output units.  
For `nn.Linear` you would have to provide the number if `in_features` first, which can be calculated using your layers and input shape or just by printing out the shape of the activation in your `forward` method.
- Here is your revised code:
```python
    def __init__(self):
        super(NeuralNet, self).__init__()
        self.conv = nn.Conv2d(1, 28, kernel_size=3)
        self.pool = nn.MaxPool2d(2)
        self.hidden= nn.Linear(28*13*13, 128)
        self.drop = nn.Dropout(0.2)
        self.out = nn.Linear(128, 10)
        self.act = nn.ReLU()

    def forward(self, x):
        x = self.act(self.conv(x)) # [batch_size, 28, 26, 26]
        x = self.pool(x) # [batch_size, 28, 13, 13]
        x = x.view(x.size(0), -1) # [batch_size, 28*13*13=4732]
        x = self.act(self.hidden(x)) # [batch_size, 128]
        x = self.drop(x)
        x = self.out(x) # [batch_size, 10]
        return x


model = NeuralNet()

batch_size, C, H, W = 1, 1, 28, 28
x = torch.randn(batch_size, C, H, W)
output = model(x)
```
"
Source: https://discuss.pytorch.org/t/pytorch-equivalent-of-keras/29412/2

```
tf.keras.layers.UpSampling2D((2, 2))(enc8)
```

"
Now, it involves the `UpSampling2D` from `keras`. When I used `torch.nn.UpsamplingNearest2d` in pytorch, as default value of `UpSampling2D` in keras is `nearest`, I got different inconsistent results. The example is as follows:

**Keras behaviour**
```python
In [3]: t1 = tf.random_normal([32, 8, 8, 512]) # as we have channels last in keras                                  

In [4]: u_s = tf.keras.layers.UpSampling2D(2)(t1)                               

In [5]: u_s.shape                                                               
Out[5]: TensorShape([Dimension(32), Dimension(16), Dimension(16), Dimension(512)])
```

So the output shape is `(32,16,16,512)`. Now let's do the same thing with PyTorch.

**PyTorch Behaviour**
```python
In [2]: t1 = torch.randn([32,512,8,8]) # as channels first in pytorch

In [3]: u_s = torch.nn.UpsamplingNearest2d(2)(t1)

In [4]: u_s.shape
Out[4]: torch.Size([32, 512, 2, 2])
```

Here output shape is `(32,512,2,2)` as compared to `(32,512,16,16)` from keras.
"

"
In keras, it uses a scaling factor to upsample. [SOURCE](https://www.tensorflow.org/api_docs/python/tf/keras/layers/UpSampling2D).
```python
tf.keras.layers.UpSampling2D(size, interpolation='nearest')
```
> size: Int, or tuple of 2 integers. The upsampling factors for rows and columns.

And, PyTorch provides, both, direct **output size** and **scaling factor**. [SOURCE](https://pytorch.org/docs/stable/generated/torch.nn.UpsamplingNearest2d.html).
```python
torch.nn.UpsamplingNearest2d(size=None, scale_factor=None)
```
> To specify the scale, it takes either the size or the scale_factor as its constructor argument.

So, in your case
```python
# scaling factor in keras 
t1 = tf.random.normal([32, 8, 8, 512])
tf.keras.layers.UpSampling2D(2)(t1).shape
TensorShape([32, 16, 16, 512])

# direct output size in pytorch 
t1 = torch.randn([32,512,8,8]) # as channels first in pytorch
torch.nn.UpsamplingNearest2d(size=(16, 16))(t1).shape
# or torch.nn.UpsamplingNearest2d(size=16)(t1).shape
torch.Size([32, 512, 16, 16])

# scaling factor in pytorch.
torch.nn.UpsamplingNearest2d(scale_factor=2)(t1).shape
torch.Size([32, 512, 16, 16])
```
"
Source: https://stackoverflow.com/a/71585583

***

```python
tf.keras.layers.Concatenate()([dec1, skip2])
```

