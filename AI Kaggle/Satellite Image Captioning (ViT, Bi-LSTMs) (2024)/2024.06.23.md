
### Download data
https://www.kaggle.com/datasets/tomtillo/satellite-image-caption-generation/code
```bash
cd /g/data/wb00/admin/testing/
mkdir satellite-image-caption-generation
cd satellite-image-caption-generation
pwd
/g/data/wb00/admin/testing/satellite-image-caption-generation

# From CMD (ARE will not work)
cd /g/data/wb00/admin/testing/satellite-image-caption-generation
module use /g/data/dk92/apps/Modules/modulefiles/
module load nci-dlwp-cs/2024.04.30

kaggle datasets download -d tomtillo/satellite-image-caption-generation
```


### New notebook
```bash
cd /scratch/fp0/mah900/
mkdir 'Satellite Image Captioning (ViT, Bi-LSTMs)'
cd 'Satellite Image Captioning (ViT, Bi-LSTMs)'
pwd 
'/scratch/fp0/mah900/Satellite Image Captioning (ViT, Bi-LSTMs)'

mkdir NCI-notebooks
```

New notebook created:
`/scratch/fp0/mah900/Satellite Image Captioning (ViT, Bi-LSTMs)/NCI-notebooks/Satellite Image Captioning-01.ipynb`

Need to install
```
cv2
vit_keras

# colorama found
```

Will not run from CMD, run from ARE ( NCI AI-ML module loaded)
```bash
python -m pip install -U opencv-python vit-keras tensorflow-addons==0.21.0 -t "/scratch/fp0/mah900/Satellite Image Captioning (ViT, Bi-LSTMs)/bin"
```

Error
```
ModuleNotFoundError: No module named 'tensorflow_addons'
```

"Before installing tensorflow-addons, please check the compatible version of tensorflow-addons with your TF & Python version. 
You can refer this for compatibility match: https://github.com/tensorflow/addons"
Source: https://stackoverflow.com/questions/61380552/unable-to-import-tensorflow-addons
```bash
tensorflow-addons-0.21.0    2.13
```

  
###### ViT-Keras
"The pre training weights file will be downloaded to C:\Users\user_name\. Keras\weights when "pre_trained = True" 

#### How do I change the default download directory for pre-trained model in Keras?
https://stackoverflow.com/questions/49055214/how-do-i-change-the-default-download-directory-for-pre-trained-model-in-keras

"If you are using the master branch of keras, you can [set the `KERAS_HOME` environment variable](https://github.com/keras-team/keras/blob/master/keras/utils/data_utils.py#L174) to set the cache directory. If it is not set, cache directory defaults to `$HOME/.keras`."
```bash
export KERAS_HOME="/path/to/keras/dir"
```
Try
```python
import os
os.environ["KERAS_HOME"] = "/g/data/wb00/admin/testing/satellite-image-caption-generation/vit_keras_weights/"
```
Does not work
Later have to be downloaded manually. 


# 2024.06.24

Okay, working 
No more major work.
Next plan: Find a PyTorch version or try to convert.
Do some improvements with presentations. 



