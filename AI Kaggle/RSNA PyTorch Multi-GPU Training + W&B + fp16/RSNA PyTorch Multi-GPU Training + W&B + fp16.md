
https://www.kaggle.com/code/heyytanay/rsna-pytorch-multi-gpu-training-w-b-fp16


# PyTorch Multi-GPU trainer using 🤗 accelerate + Mixed Precision + W&B Logging[](https://www.kaggle.com/code/heyytanay/rsna-pytorch-multi-gpu-training-w-b-fp16#PyTorch-Multi-GPU-trainer-using-%F0%9F%A4%97-accelerate-+-Mixed-Precision-+-W&B-Logging)

This training notebook uses HuggingFace accelerate with mixed precision to train a VIT model on 2x T4 GPUs. The current performance is quite good but I am working on optimizing it.

I am also using Probabilistic F1 score with a `beta=0.5` (in hopes that it will penalize false positive). The entire training pipeline is working well and you could fork the notebook and play the model and other hyperparameters.

**Feel free to fork and change the models and do some preprocessing, but if you do please leave an upvote :)**


