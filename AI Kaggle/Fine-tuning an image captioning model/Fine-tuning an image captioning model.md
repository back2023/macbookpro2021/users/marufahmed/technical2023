

https://www.kaggle.com/code/mits2249691/fine-tuning-an-image-captioning-model


## Introduction[](https://www.kaggle.com/code/mits2249691/fine-tuning-an-image-captioning-model#Introduction)

In this notebook I am fine-tuning a GIT, short for GenerativeImage2Text, on a clean version of the "Satellite Image Caption Generation" dataset created in [this](https://www.kaggle.com/code/mits2249691/short-eda-and-data-cleaning) notebook.

The steps I followed are based on [this](https://github.com/NielsRogge/Transformers-Tutorials/blob/master/GIT/Fine_tune_GIT_on_an_image_captioning_dataset.ipynb) tutorial.


