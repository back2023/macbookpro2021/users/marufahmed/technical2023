
# add exception handler for DataLoader when reading a damaged image file #1137
Source: https://github.com/pytorch/pytorch/issues/1137

Follow these steps in order to handle corrupted images:

1. Return `None` in the `__getitem__()` if the image is corrupted

```
def __getitem__(self, idx):
    try:
        img, label = load_img(idx)
    except:
        return None
    return [img, label]
```

2. Filter the `None` values in the `collate_fn()`

```
def collate_fn(batch):
    batch = list(filter(lambda x: x is not None, batch))
    return torch.utils.data.dataloader.default_collate(batch)
```

3. Pass the `collate_fn()` to the `DataLoader()`

```
train_loader = DataLoader(train_dataset, collate_fn=collate_fn, **kwargs)
test_loader = DataLoader(test_dataset, collate_fn=collate_fn, **kwargs)
```



# Questions about Dataloader and Dataset
Source: https://discuss.pytorch.org/t/questions-about-dataloader-and-dataset/806

```python
def my_collate(batch):
    batch = filter (lambda x:x is not None, batch)
    return default_collate(batch)
class MyImageFolder(ImageFolder):
    __init__ = ImageFolder.__init__
    def __getitem__(self, index):
        try: 
            return super(MyImageFolder, self).__getitem__(index)
        except Exception as e:
            print e

dataset= MyImageFolder('/home/x/train/', transform = transforms.Compose([transforms.ToTensor()..]) )
dataloader=t.utils.data.DataLoader(dataset, 4, True, collate_fn=my_collate)
```

