
https://www.kaggle.com/competitions/understanding_cloud_organization

  
###### Start
Aug 17, 2019
###### Close
Nov 19, 2019


https://www.kaggle.com/competitions/understanding_cloud_organization/data

## Dataset Description

In this competition you will be identifying regions in satellite images that contain certain cloud formations, with label names: `Fish`, `Flower`, `Gravel`, `Sugar`. For each image in the test set, you must segment the regions of each cloud formation label. Each image has at least one cloud formation, and can possibly contain up to all all four.  
The images were downloaded from [NASA Worldview](https://worldview.earthdata.nasa.gov/). Three regions, spanning 21 degrees longitude and 14 degrees latitude, were chosen. The true-color images were taken from two polar-orbiting satellites, TERRA and AQUA, each of which pass a specific region once a day. Due to the small footprint of the imager (MODIS) on board these satellites, an image might be stitched together from two orbits. The remaining area, which has not been covered by two succeeding orbits, is marked black.  
The labels were created in a [crowd-sourcing activity](https://www.zooniverse.org/projects/raspstephan/sugar-flower-fish-or-gravel) at the Max-Planck-Institite for Meteorology in Hamburg, Germany, and the Laboratoire de météorologie dynamique in Paris, France. A team of 68 scientists identified areas of cloud patterns in each image, and each images was labeled by approximately 3 different scientists. Ground truth was determined by the union of the areas marked by all labelers for that image, after removing any black band area from the areas.  
The segment for each cloud formation label for an image is encoded into a single row, even if there are several non-contiguous areas of the same formation in an image. If there is no area of a certain cloud type for an image, the corresponding `EncodedPixels` prediction should be left blank. You can read more about the encoding standard on the [Evaluation](https://kaggle.com/c/understanding_cloud_organization/overview/evaluation) page.




### Resized dataset
https://www.kaggle.com/competitions/understanding_cloud_organization/discussion/108079

"I see that most people are doing the resize on the fly over and over again for each epoch. This resize process is quite computationally expensive. Here is a resized dataset scaled down to 350, 525 already. This will significantly reduce the time of each epoch training."

[https://www.kaggle.com/ryches/understanding-clouds-resized](https://www.kaggle.com/ryches/understanding-clouds-resized)