
# Resize
### `Pillow` vs `cv2` resize
Source: https://github.com/python-pillow/Pillow/issues/2718
"Results of reading and resizing can be different in `cv2` and `Pilllow`. This creates problems when you want to reuse a model (neural network) trained using `cv2` with `Pillow`."

```python
import cv2
from PIL import Image
import numpy as np

# read images
a = cv2.imread('images/dogs-and-cats.jpg', cv2.IMREAD_COLOR)
b = Image.open('images/dogs-and-cats.jpg')

# they are equal
print((np.asarray(b)[:, :, [2, 1, 0]] == a).all())
# True

# resize images
b = b.resize((300, 300), Image.NEAREST)
a = cv2.resize(a, (300, 300), interpolation=cv2.INTER_NEAREST)

# results of resizing are different, but visually they are equal
b = np.asarray(b)[:, :, [2, 1, 0]]
print((b - a).mean())
# 71.1218444444
```

"This is because the numpy array's dtype is uint8"
"So, we can transform it to int16"
Source: https://github.com/python-pillow/Pillow/issues/2718#issuecomment-563638995


### It seems that cv2.resize and PIL.resize results are different #4445
Source: https://github.com/python-pillow/Pillow/issues/4445
```python
import cv2
from PIL import Image
import matplotlib.pyplot as plt
img = cv2.imread("./ILSVRC2012/ILSVRC2012_val_00000001.JPEG")
img = img[:, :, ::-1]
cv_img = cv2.resize( img, (224,224), interpolation=cv2.INTER_LINEAR).astype( np.float32 )
pil_image = Image.fromarray( img )
pil_image = pil_image.resize((224, 224), Image.BILINEAR)
pil_image = np.array(pil_image).astype( np.float32 )
print( cv_img.dtype, pil_image.dtype )
diff = np.sum( np.abs(cv_img-pil_image) )
print( diff )
```


### Switching among OpenCV, Tensorflow and Pillow? Wait!!!
Source: https://towardsdatascience.com/image-read-and-resize-with-opencv-tensorflow-and-pil-3e0f29b992be

"As expected from our experience from TF and OpenCV, these two frameworks again open the images differently. PIL, by default, doesn’t provide any way to use integer accurate decompression. However, one can write their own decoder to decode images."

"Since the images are read differently between these frameworks, this difference will propagate when we do resize. This is shown below-"
```python
image_pil_resized = orig_image_pil.resize((300,300),       resample=Image.BILINEAR)  
image_pil_resized = np.asarray(image_pil_resized)#making sure we have the same dtype as OpenCV image.  
print(image_pil_resized.dtype)
```
![](https://miro.medium.com/v2/resize:fit:80/1*ecA7FYj8gW5zM3wQlbFycw.png)
```python
image_cv_pil_resize_diff = np.abs(image_cv_resized - image_pil_resized)  
plt_display(image_cv_pil_resize_diff, 'Resized CV- Resized PIL')
```
"Hence, we get the expected difference in resizing. PIL library doesn’t provide any support for half-pixel correction, and so there is no by default support for this. Since we know TF and OpenCV are consistent, we can expect the same results when considering the difference between PIL and TF as well."



# To Tensor

### How to transform images to tensors
https://discuss.pytorch.org/t/how-to-transform-images-to-tensors/71447

Those transforms are only for PIL Images, so you have two options:

- Using opencv to load the images and then convert to pil image using:

```python
from PIL import Image 
img = cv2.imread('img_path') 
pil_img = Image.fromarray(img).convert('RGB')  #img as opencv
```


- Load the image directly with PIL (better than 1)

```python
from PIL import Image 
pil_img = Image.open(img_path).convert('RGB') # convert('L') if it's a gray scale image
```

"You can resize the pil image directly or also using transforms. 
Also you shouldn’t load all the images into a list, because Dataloader load on the fly, but you have to set your own Dataset or using something like [https://pytorch.org/docs/stable/torchvision/datasets.html#imagefolder 391](https://pytorch.org/docs/stable/torchvision/datasets.html#imagefolder)"


#  Convert image  

### Convert image from PIL to openCV format
https://stackoverflow.com/questions/14134892/convert-image-from-pil-to-opencv-format

```python
pil_image = PIL.Image.open('Image.jpg').convert('RGB')
open_cv_image = numpy.array(pil_image)
# Convert RGB to BGR
open_cv_image = open_cv_image[:, :, ::-1].copy()
```
