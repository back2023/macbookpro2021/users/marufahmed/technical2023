
https://github.com/Orion-Ai-Lab/TeleViT


## About

Teleconnection-driven vision transformers for improved long-term forecasting


This is the official code repository of the [TeleViT paper](https://arxiv.org/abs/2306.10940), accepted at ICCV 2023 [hadr.ai workshop](https://www.hadr.ai/).

🌐 Paper Website: [https://orion-ai-lab.github.io/televit/](https://orion-ai-lab.github.io/televit/)





