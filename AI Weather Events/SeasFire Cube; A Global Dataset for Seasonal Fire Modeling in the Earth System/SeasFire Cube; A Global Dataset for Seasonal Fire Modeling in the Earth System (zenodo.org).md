
https://zenodo.org/record/8055879


Alonso, Lazaro; Gans, Fabian; Karasante, Ilektra; Ahuja, Akanksha; Prapas, Ioannis; Kondylatos, Spyros; Papoutsis, Ioannis; Panagiotou, Eleannna; Mihail, Dimitrios; Cremer, Felix; Weber, Ulrich; Carvalhais, Nuno

The **SeasFire Cube** is a scientific datacube for seasonal fire forecasting around the **globe**. Apart from seasonal fire forecasting, which is the aim of the SeasFire project, the datacube can be used for several other tasks. For example, it can be used to model teleconnections and memory effects in the earth system. Additionally, it can be used to model emissions from wildfires and the evolution of wildfire regimes.  
  
It has been created in the context of the [SeasFire project](https://seasfire.hua.gr/), which deals with "_Earth System Deep Learning for Seasonal Fire Forecasting_" and **is funded by the European Space Agency (ESA)**  in the context of ESA Future EO-1 Science for Society Call.  
  
It contains **21 years** of data (2001-2021) in an **8-days** time resolution and **0.25 degrees grid** resolution. It has a diverse range of seasonal fire drivers. It expands from atmospheric and climatological ones to vegetation variables, socioeconomic and the target variables related to wildfires such as burned areas, fire radiative power, and wildfire-related CO2 emissions.



