
https://github.com/SeasFire/seasfire-datacube


## About

An open and accessible datacube for environmental and earth system monitoring


# SeasFire Cube: A Global Dataset for Seasonal Fire Modeling in the Earth System

An open and accessible datacube for environmental and earth system monitoring

### [](https://github.com/SeasFire/seasfire-datacube#tutorial----how-to-use)Tutorial - How to use

A simple walkthrough to open the dataset, filter it based on location and time and visualise the features.

- [Python Tutorial](https://github.com/SeasFire/seasfire-datacube/blob/main/Tutorial/Seasfire_datacube_tutorial.ipynb)
- [Julia Tutorial](https://github.com/SeasFire/seasfire-datacube/blob/main/Julia-Tutorial/tutorial.ipynb)

### [](https://github.com/SeasFire/seasfire-datacube#website)Website

- Seasfire Project Website: [https://seasfire.hua.gr/earth-as-a-graph/](https://seasfire.hua.gr/earth-as-a-graph/)

### [](https://github.com/SeasFire/seasfire-datacube#zenodo)Zenodo

- Dataset: [https://doi.org/10.5281/zenodo.7108392](https://doi.org/10.5281/zenodo.7108392)



