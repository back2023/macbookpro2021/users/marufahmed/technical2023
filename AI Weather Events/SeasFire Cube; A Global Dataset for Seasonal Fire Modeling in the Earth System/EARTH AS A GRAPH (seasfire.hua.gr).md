
https://seasfire.hua.gr/earth-as-a-graph/

### _Research Question: How can we take advantage of the spatio-temporal connections between physical parameters of a geographic location and use them to predict wildfires using deep learning?_


## State-of-the-art

Current modelling approaches for predicting and explaining fire patterns are mainly based on classic statistical approaches or ML models like Random Forests. More sophisticated methods like Convolutional Neural Networks (CNNs) have been applied to other fire-related problems like short-term fire forecasting. The common baseline in all these implementations is that they rely only on **local interactions** between the different covariates, ignoring the potential **long-scale dependencies** that may exist between the different explanatory variables.

## Beyond state-of-the-art

Although there have been some successful applications of DL for Earth System science, traditional DL architectures like plain CNNs that treat each image patch independently do not account for the large-scale interconnections of the Earth System. In SeasFire, we aim to expand the state-of-the-art, demonstrating the use of the latest progress in the DL paradigm that can capture complex dataset interactions, namely Graph Neural Networks (GNNs).








