

https://github.com/chendelong1999/remoteclip


## About

🛰️ Official repository of paper "RemoteCLIP: A Vision Language Foundation Model for Remote Sensing" (IEEE TGRS)

[arxiv.org/abs/2306.11029](https://arxiv.org/abs/2306.11029 "https://arxiv.org/abs/2306.11029")


