

https://github.com/ruotianluo/self-critical.pytorch


# An Image Captioning codebase

[](https://github.com/ruotianluo/self-critical.pytorch#an-image-captioning-codebase)

This is a codebase for image captioning research.

It supports:

- Self critical training from [Self-critical Sequence Training for Image Captioning](https://arxiv.org/abs/1612.00563)
- Bottom up feature from [ref](https://arxiv.org/abs/1707.07998).
- Test time ensemble
- Multi-GPU training. (DistributedDataParallel is now supported with the help of pytorch-lightning, see [ADVANCED.md](https://github.com/ruotianluo/self-critical.pytorch/blob/master/ADVANCED.md) for details)
- Transformer captioning model.

A simple demo colab notebook is available [here](https://colab.research.google.com/github/ruotianluo/ImageCaptioning.pytorch/blob/colab/notebooks/captioning_demo.ipynb)


