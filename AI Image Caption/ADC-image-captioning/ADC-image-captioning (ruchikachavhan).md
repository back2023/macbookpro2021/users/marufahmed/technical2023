

https://github.com/ruchikachavhan/ADC-image-captioning


# ADC-image-captioning

[](https://github.com/ruchikachavhan/ADC-image-captioning#adc-image-captioning)

Code for our paper: [A Novel Actor Dual-Critic Model for Image Captioning](https://arxiv.org/abs/2010.01999), ICPR 2020  
[Ruchika Chavhan](https://ruchikachavhan.github.io/), [Biplab Banerjee](https://biplab-banerjee.github.io/), [Xiao Xiang Zhu](https://www.lrg.tum.de/sipeo/home/), [Subhasis Chaudhuri](https://www.ee.iitb.ac.in/~sc/)


