

https://github.com/GauravGajbhiye/SCAMET_RSIC


# SCAMET_RSIC

[](https://github.com/GauravGajbhiye/SCAMET_RSIC#scamet_rsic)

This is tensorflow 2.2 based repository of SCAMET framework for remote sensing image captioning. This is official implementation of Spatial-Channel Attention based Memory-guided Transformer (SCAMET) approach. We have designed encode-decoder based CNN-Transformer approach for describing the multi-spectral, multi-resolution, multi-directional remote sensing images.


