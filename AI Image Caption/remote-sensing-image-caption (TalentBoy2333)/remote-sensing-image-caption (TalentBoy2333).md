

https://github.com/TalentBoy2333/remote-sensing-image-caption


This is a project of remote sensing image.  

- Classification of remote sensing image
- Remote sensing image caption

I'm using `torch 0.4`, `opencv-python`, `numpy`, `matplotlib` in `python 3.6`  

## Classification

[](https://github.com/TalentBoy2333/remote-sensing-image-caption#classification)

### Data

[](https://github.com/TalentBoy2333/remote-sensing-image-caption#data)

I used `NWPU-RESISC45` dataset, you can download this dataset([http://www.escience.cn/people/JunweiHan/NWPU-RESISC45.html](http://www.escience.cn/people/JunweiHan/NWPU-RESISC45.html)) to train your model, or you can download other dataset, but you should pay attention to the difference of the way to load data from daataset, we maybe used different way to load data.  
All in all, check the `dataset.py`.  

### Model

[](https://github.com/TalentBoy2333/remote-sensing-image-caption#model)

We can choose two pre-train models, `resnet_v101` and `mobilenet_v2`.

```python
classifier = Classifier(model_name, class_number, True)
```

Then, I just add a full connect layer and softmax to classify.

