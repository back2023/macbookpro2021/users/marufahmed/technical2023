

https://stackoverflow.com/questions/53580088/calculate-the-output-size-in-convolution-layer


you can use this formula `[(W−K+2P)/S]+1`.

- W is the input volume - in your case 128
- K is the Kernel size - in your case 5
- P is the padding - in your case 0 i believe
- S is the stride - which you have not provided.

So, we input into the formula:

```
Output_Shape = (128-5+0)/1+1

Output_Shape = (124,124,40)
```

NOTE: Stride defaults to 1 if not provided and the `40` in `(124, 124, 40)` is the number of filters provided by the user.