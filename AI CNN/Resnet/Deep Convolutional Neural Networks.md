
https://towardsdatascience.com/deep-convolutional-neural-networks-ccf96f830178


The goal of this post is to serve as a nice introduction to deep architectures before diving to read the original publications where they are described.

I feel there is a lack of help in the research community. A little bit of the time of one researcher by making nice visualizations, dashboards, demos or even videos could save the time of all the researchers coming after him/her, and innovation will grow faster.

My contribution is by giving intuition in understanding the evolution of the so used **deep convolutional neural networks** as the default option for computer vision problems.


