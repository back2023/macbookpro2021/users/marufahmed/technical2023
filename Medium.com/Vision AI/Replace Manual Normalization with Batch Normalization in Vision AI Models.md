
https://towardsdatascience.com/replace-manual-normalization-with-batch-normalization-in-vision-ai-models-e7782e82193c


## A neat trick to avoid expensive manual pixel normalization for Vision (Image/Video) AI models is to stick a Batch normalization layer as the first layer of your model

