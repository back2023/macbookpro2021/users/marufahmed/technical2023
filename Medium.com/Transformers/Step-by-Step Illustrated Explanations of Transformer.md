
https://medium.com/@yulemoon/detailed-explanations-of-transformer-step-by-step-dc32d90b3a98

My next post “[An In-Depth Look at Transformer-Based Models](https://medium.com/@yulemoon/an-in-depth-look-at-the-transformer-based-models-22e5f5d17b6b)” will deeply explore the training objectives and architectures of these models, including why GPT-1 to GPT-4 utilize a Transformer decoder-only architecture. Additionally, the post will examine why a decoder-based model is a better choice for a unifying network model for downstream tasks.




