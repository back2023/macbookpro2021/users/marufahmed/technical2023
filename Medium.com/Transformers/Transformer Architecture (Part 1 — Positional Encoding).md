

https://medium.com/@eugeneku123/transformer-architecture-part-1-positional-encoding-9b69c73140f7

T**ransformer Network** is arguably the most popular and influential model architecture that drives the hypes of deep learning today. We will go over the cruxes of the architecture, positional encoding, self-attention, and then put everything together similarly to the model in the paper, “Attention is all you need” by Vaswani et al.

Before proceeding, note that you will need a good understanding of **Recursive Neural Networks** and **Word Embedding**.
