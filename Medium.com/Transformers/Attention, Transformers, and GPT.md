
https://trevormcguire.medium.com/attention-transformers-and-gpt-b3adbbb4a950


By now, you’ve surely [heard of ChatGPT](https://openai.com/blog/chatgpt). This model, developed by OpenAI, has changed the game not only in AI circles, but also in the world at large. Within two months of its release, it crossed 100 million active users, making it the fastest-growing consumer application in history.

As of writing, the model itself is fine-tuned from the GPT-3.5 series, which is a [series of language models](https://platform.openai.com/docs/model-index-for-researchers) trained on data from before Q4 2021.

But, things move fast in the world of AI. Just weeks ago, [OpenAI released GPT-4](https://openai.com/research/gpt-4), which “exhibits human-level performance on various professional and academic benchmarks.” Moreover, OpenAI says that: “GPT-4 is more reliable, creative, and able to handle much more nuanced instructions than GPT-3.5.” In fact, by the time you read this, you’ll probably be able to interact with GPT-4 via the ChatGPT interface (where you can test out their claims out for yourself).


