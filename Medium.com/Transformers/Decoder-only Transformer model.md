

https://medium.com/data-driven-fiction/decoder-only-transformer-model-521ce97e47e2


Over past 6 months, everywhere around the internet — **ChatGPT** was the term buzzing around and it is in fact acknowledged that the next-decade will be the **decade of generative models**.

There are **many Generative models** like _Autoregressive models_, _Variational autoencoders_, _Normalizing flow models, Generative adversarial networks, etc.._

**ChatGPT** could be considered as **one of the deep-generative model**’s **language processing application**. [**GAN(Generative adversarial networks)**](https://medium.com/mlearning-ai/understanding-of-gan-e3fc56855fd4)’s **DeepFake** is one of the **deep-generative model**’s **computer vision application**. **Generative models** is an exciting and rapidly-evolving fields of _statistical machine learning_ and _artificial intelligence_.


