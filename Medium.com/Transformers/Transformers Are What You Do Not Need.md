
https://valeman.medium.com/transformers-are-what-you-do-not-need-cf16a4c13ab7


_In my previous article,_ [_“Deep Learning Is What You Do Not Need”_](https://medium.com/@valeman/-86655805a676)_, I explained that deep learning for time series is not the solution that most companies need even to consider; in this article, we delve deeper into more good reasons why transformers are not the solution that is effective for time series._






