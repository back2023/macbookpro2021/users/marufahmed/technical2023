
https://medium.com/@ds-faxi-yuan/graph-neural-networks-gnn-comparison-between-cnn-and-gnn-5c97fdfb3e31

Based on my previous story on message [passing framework](https://medium.com/@ds-faxi-yuan/graph-learning-message-passing-for-node-classifications-7f042c349763) for node classifications, I am going to introduce the graph neural networks (GNN) including 1) what are the differences between CNN and GNN; 2) how GNN architecture like graph convolutional networks (GCN) enable the deep encoder; and 3) how to train a GNN.





