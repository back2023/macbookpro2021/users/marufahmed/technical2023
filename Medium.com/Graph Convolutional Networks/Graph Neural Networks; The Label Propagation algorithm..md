
https://medium.com/artificialis/graph-neural-networks-the-label-propagation-algorithm-46f75a0c468c


Graphs are all around us. Real-world objects are often defined in terms of their connections to other things. A set of objects, and the connections between them, are naturally expressed as a _graph._ Researchers have developed Neural Networks that operate on graph data (called Graph Neural Networks, or GNNs) for over a decade.





