https://towardsdatascience.com/cook-your-first-u-net-in-pytorch-b3297a844cf3


U-Net is a deep learning architecture used for semantic segmentation tasks in image analysis. It was introduced by Olaf Ronneberger, Philipp Fischer, and Thomas Brox in a paper titled “[U-Net: Convolutional Networks for Biomedical Image Segmentation](https://papers.labml.ai/paper/2e48c3ffdc8311eba3db37f65e372566)”.


