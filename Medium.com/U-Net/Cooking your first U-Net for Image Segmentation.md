

https://medium.com/@foporcher/cooking-your-first-u-net-for-image-segmentation-e812e37e9cd0


Fellow AI cooks, today you are going to learn how to prepare one of the most important recipes in Computer Vision: the U-Net.

==You can find the full code on my== ==[Github](https://github.com/FrancoisPorcher/awesome-medium-tutorials/tree/main/0001%20-%20unet)====, or on== ==[Google Colab](https://drive.google.com/drive/folders/1TrVuiv4-Y1lj_MSneR03QIDoTaFYyUFf?usp=sharing)==

Even better, we are going to apply the U-Net to the MRI segmentation dataset from Kaggle, accessible [here](https://www.kaggle.com/datasets/mateuszbuda/lgg-mri-segmentation):

[

## Brain MRI segmentation

### Brain MRI images together with manual FLAIR abnormality segmentation masks

(https://www.kaggle.com/datasets/mateuszbuda/lgg-mri-segmentation?source=post_page-----e812e37e9cd0--------------------------------)

# Ingredients of the Recipe:

1. Exploration of the Dataset
2. Creation of the Datasets and Dataloader classes
3. Creation of the architecture
4. Examining the losses (DICE and Binary Cross Entropy)
5. Results
6. Post-cooking tips to Spice things up!


