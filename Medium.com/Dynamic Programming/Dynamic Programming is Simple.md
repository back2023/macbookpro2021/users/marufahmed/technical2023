
https://python.plainenglish.io/dynamic-programming-is-simple-1174aed46e4c


1. Dynamic Programming is Simple (this article)
2. [Dynamic programming is simple #2](https://medium.com/@pv.safronov/dynamic-programming-is-simple-2-32147f45d525)
3. [Dynamic programming is simple #3 (multi-root recursion)](https://medium.com/@pv.safronov/dynamic-programming-is-simple-3-multi-root-recursion-c613dfcc15b4)
4. [Dynamic programming is simple #4 (bitmap + optimal solution reconstruction)](https://medium.com/@pv.safronov/dynamic-programming-is-simple-4-bitmap-optimal-solution-reconstruction-a4ffaf949bd6)

You can use the same pattern for solving 90% of DP problems. You can find this DP pattern below.


