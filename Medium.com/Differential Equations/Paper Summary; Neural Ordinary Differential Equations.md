
https://towardsdatascience.com/paper-summary-neural-ordinary-differential-equations-37c4e52df128


NIPS 2018 (Montreal, Canada), or NeurIPS, as it is called now, is over, and I would like to take the opportunity to dissect one of the papers that received the Best Paper Award at this prestigious conference. The name of the paper is _Neural Ordinary Differential Equations_ ([arXiv link](https://arxiv.org/abs/1806.07366)) and its authors are affiliated to the famous Vector Institute at the University of Toronto. In this post, I will try to explain some of the main ideas of this paper as well as discuss their potential implications for the future of the field of Deep Learning. Since the paper is quite advanced and touches on concepts such as _Ordinary Differential Equations_ (**ODE**), _Recurrent Neural Networks_ (**RNN**) or _Normalizing Flows_ (**NF**), I suggest that you read up on these terms if you are not familiar with them, since I will not go into details on these. However, I will try to explain the ideas of the paper as intuitively as possible, so that you may get the main concepts without going too much into the technical details. If you are interested, you may read up on these details afterwards in the original paper. The post is split into multiple sections, each one explaining one or multiple chapters in the paper.


