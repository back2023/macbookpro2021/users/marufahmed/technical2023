
https://medium.com/@northamericangeoscientistsorg/deep-learning-for-satellite-image-classification-with-python-ceff1cdf41fb


Let’s dive into how we can use deep learning, specifically convolutional neural networks (CNN), to classify satellite images. We will be using Python, Keras, and a dataset from [UC Merced Land Use Dataset](http://weegee.vision.ucmerced.edu/datasets/landuse.html), which contains 21 types of land use images.