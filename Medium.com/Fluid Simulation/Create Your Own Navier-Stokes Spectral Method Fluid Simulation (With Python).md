
https://levelup.gitconnected.com/create-your-own-navier-stokes-spectral-method-fluid-simulation-with-python-3f37405524f4


For today’s recreational coding exercise, we solve the Navier-Stokes equations for an incompressible viscous fluid. To do so, we will implement a spectral method. The Navier-Stokes equations approximately describe the motions of a fluid such as water. The equations can capture the phenomenon of _turbulence._ The system is also of interest in mathematics as a general proof for the existence and smoothness of solutions in 3D is one of the open [Millenium Prize Problems](https://en.wikipedia.org/wiki/Millennium_Prize_Problems).

You may find the accompanying [Python code on GitHub](https://github.com/pmocz/navier-stokes-spectral-python).


# navier-stokes-spectral-python
https://github.com/pmocz/navier-stokes-spectral-python

Spectral Solver for Navier-Stokes Equations

## [](https://github.com/pmocz/navier-stokes-spectral-python#create-your-own-navier-stokes-spectral-method-simulation-with-python)Create Your Own Navier-Stokes Spectral Method Simulation (With Python)

### [](https://github.com/pmocz/navier-stokes-spectral-python#philip-mocz-2023-pmocz)Philip Mocz (2023) [@PMocz](https://twitter.com/PMocz)

### [](https://github.com/pmocz/navier-stokes-spectral-python#-read-the-algorithm-write-up-on-medium)[📝 Read the Algorithm Write-up on Medium](https://philip-mocz.medium.com/create-your-own-navier-stokes-spectral-method-fluid-simulation-with-python-3f37405524f4)

Simulate the Navier-Stokes equations with a Spectral method



