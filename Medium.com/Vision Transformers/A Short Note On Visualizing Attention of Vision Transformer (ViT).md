
https://medium.com/@UlamacaLEE/a-short-note-on-visualizing-attention-of-vision-transformer-vit-a6b67377a914


Two years ago, when I was first studying the Vision Transformer paper [1], it was not very clear to me how the model attention is being visualized. Today when I visited ViT once again, I tried to figure out how this visualization works.

I basically learned from a nice blog [2] and an illustrative jupyter notebook [3].

