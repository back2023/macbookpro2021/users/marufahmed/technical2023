
https://wcvanvan.medium.com/backpropagation-for-batch-normalization-87ad825f3a50


I’ve never looked into the details of computing backprop because I think its idea is pretty straightforward while the process of computation is complex.

But this morning I needed to write code to compute backprop for BatchNorm. After it, I think understanding the computing process is pretty beneficial. Now I am posting my procedure to calculate on paper and my code to solve backprop for BatchNorm.


