
https://blog.gopenai.com/how-to-resolve-runtimeerror-cuda-out-of-memory-d48995452a0


In loading a pre-trained model or fine-tuning an existing model, an “CUDA out of memory” error like the following often prompts:

```
RuntimeError: CUDA out of memory. Tried to allocate 576.00 MiB   
(GPU 0; 39.42 GiB total capacity; 20.89 GiB already allocated;   
403.69 MiB free; 20.90 GiB reserved in total by PyTorch)   
If reserved memory is >> allocated memory try setting max_split_size_mb   
to avoid fragmentation. See documentation for Memory Management and   
PYTORCH_CUDA_ALLOC_CONF
```


