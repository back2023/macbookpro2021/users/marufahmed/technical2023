
https://medium.com/mlearning-ai/profiling-a-training-task-with-pytorch-profiler-and-viewing-it-on-tensorboard-2cb7e0fef30e


This post briefly and with an example shows how to profile a training task of a model with the help of PyTorch profiler. Developers use profiling tools for understanding the behavior of their code to be able to optimize it. TensorFlow framework provides a good ecosystem for machine learning developers and optimizer to profile their tasks. The following posts show how to use TensorFlow and TensorBoard.


