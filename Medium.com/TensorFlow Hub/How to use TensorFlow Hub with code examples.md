
https://medium.com/ymedialabs-innovation/how-to-use-tensorflow-hub-with-code-examples-9100edec29af


TensorFlow, now has come up with a better framework known as [TensorFlow Hub](https://www.tensorflow.org/hub/) which is very easy to use and is well organised. ==With TensorFlow Hub, you can confidently perform the widely utilised activity of Transfer Learning by importing large and popular models in a few lines of code==. TensorFlow Hub is very flexible and provides the facility to [host your models](https://www.tensorflow.org/hub/hosting) to be used by other users. These models in TensorFlow Hub are referred to as modules. In this article, let us look into basics of how to use module from TensorFlow Hub, it’s various types and code examples.


