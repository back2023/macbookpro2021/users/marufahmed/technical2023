
https://medium.com/@hermandetest/backpropagation-algorithm-part-1-f3ca23bcaef1


**Introduction to learning**

In this series of lessons, we will learn how the backpropagation algorithm works. Backpropagation is used to train neural networks, and it is also employed in Deep Learning. Since this topic can be quite complex for some people (non technical people with no math background), we will build up our understanding gradually. The following subjects will be covered over several lessons:

- the derivative
- the partial derivative
- the chain rule
- gradient descent
- loss function
- epoch
- weight initialization
- learning rate
- update rule
- Matrix notation (efficiency)




