
https://medium.com/@AIBites/generative-models-a-gentle-introduction-90e53d262ea1


This is the first post in the series of upcoming posts about Generative Deep Learning Models driving the whole new Generative AI revolution in 2023

Machine learning models can be broadly divided into discriminative or generative models.