

https://pub.towardsai.net/introduction-to-stylegan-ec0a6b0706c


Generative Adversarial Networks (GAN) have yielded state-of-the-art results in generative tasks and have become one of the most important frameworks in Deep Learning. Many variants of GAN have been proposed to improve the quality of generated images or allow conditional synthesis. However, they have yet to offer intuitive, scale-specific control of the synthesis procedure until StyleGAN.



