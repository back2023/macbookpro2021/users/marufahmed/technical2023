
https://medium.com/@cch57/exploring-the-promise-of-generative-models-in-chemistry-an-introduction-to-diffusion-models-31530e9d1dcb


# Introduction

Generative modelling, a technique that learns patterns in data to create new data, has experienced a renaissance in the past year. The catalyst was DALLE-2, an innovative image-to-text model revealed by the AI research firm OpenAI in April 2022. With DALLE-2, users can describe an image in plain text — encompassing multiple objects, scenes, or artistic styles — and the model will generate a brand new image from this description. The results have been noteworthy due to the extraordinary fidelity of the produced images and the model’s ability to seamlessly blend diverse abstract concepts within a single image. The model used a relatively new class of generative model called a diffusion model.


