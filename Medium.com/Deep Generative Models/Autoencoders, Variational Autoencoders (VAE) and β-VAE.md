
https://medium.com/@rushikesh.shende/autoencoders-variational-autoencoders-vae-and-%CE%B2-vae-ceba9998773d

Autoencoders (AE), Variational Autoencoders (VAE), and β-VAE are all generative models used in unsupervised learning. Regardless of the architecture, all these models have so-called encoder and decoder structures. AE is a deterministic model, while VAE and β-VAE are both probabilistic models based on the generalized EM (Expectation Maximization) algorithm.


