
https://medium.com/mlearning-ai/infogan-interpretable-representation-learning-to-distangle-data-unsupervised-33a4089d7c09


Unlike conditional GAN, which requires explicit class labels, InfoGAN can capture latent representations unsupervised. With the learnt latent variable, we can manipulate/control the outputs. The method introduces Information Maximizing to the standard GAN network.

1. Introduction
2. The math (about entropy and variational inference)
3. The implementation in PyTorch


