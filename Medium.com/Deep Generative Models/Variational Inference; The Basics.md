
https://towardsdatascience.com/variational-inference-the-basics-f70ac511bcea


We live in the era of quantification. But rigorous quantification is easier said then done. In complex systems such as biology, data can be difficult and expensive to collect. While in high stakes applications, such as in medicine and finance, it is crucial to account for uncertainty. Variational inference — a methodology at the forefront of AI research — is a way to address these aspects.

This tutorial introduces you to the basics: the when, why, and how of variational inference.


