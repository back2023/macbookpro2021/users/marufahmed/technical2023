
https://ruchaa.medium.com/demystifying-diffusion-models-c7e0689a7ca8

Generative models learns a representation of the data they are trained on and allows you to synthesize novel data , different from the real data , but one that looks realistic and also maintains distribution. Generative models have become a prevalent approach for tasks related to image processing and synthesis, including but not limited to editing, in-painting, de-blurring, colorization, and super resolution.


