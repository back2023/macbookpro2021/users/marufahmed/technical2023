
https://medium.com/@siddharth.tumre/how-to-create-a-subset-of-dataloader-in-pytorch-and-why-is-it-important-52c238a7199


In the field of machine learning, training models on large datasets is a common practice. However, dealing with massive amounts of data can be computationally expensive and time-consuming. In order to make sure that we do not encounter any problems during training on the larger dataset, we can run our model on a subset to understand the data better and make sure there are no errors in our code. So, creating a subset of a train loader is a beneficial approach. In this blog post, we will explore how to create a subset of dataloader and the significance of running on a subset before training on the entire dataset.

