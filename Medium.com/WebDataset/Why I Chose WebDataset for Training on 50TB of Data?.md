
https://medium.com/red-buffer/why-did-i-choose-webdataset-for-training-on-50tb-of-data-98a563a916bf

For understanding the solutions implemented to alter this, we first need to understand the difference between Pytorch’s MapDataset and IterableDataset.

**MapDataset**

A PyTorch dataset that loads all the data into memory and applies a user-defined mapping function to each item in the dataset.


