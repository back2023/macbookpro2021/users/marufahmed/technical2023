

https://github.com/goutamgmb/deep-rep


## About

Official implementation of Deep Reparametrization of Multi-Frame Super-Resolution and Denoising


# Deep-Rep-MFIR

[](https://github.com/goutamgmb/deep-rep#deep-rep-mfir)

Official implementation of [**Deep Reparametrization of Multi-Frame Super-Resolution and Denoising**](https://arxiv.org/pdf/2108.08286.pdf)

**Publication:** Deep Reparametrization of Multi-Frame Super-Resolution and Denoising. Goutam Bhat, Martin Danelljan, Fisher Yu, Luc Van Gool, and Radu Timofte. ICCV 2021 **oral** [[Arxiv](https://arxiv.org/pdf/2108.08286.pdf)]

**Note:** The code for our CVPR2021 paper ["Deep Burst Super-Resolution"](https://arxiv.org/pdf/2101.10997.pdf) is available at [goutamgmb/deep-burst-sr](https://github.com/goutamgmb/deep-burst-sr)

## Overview

[](https://github.com/goutamgmb/deep-rep#overview)

We propose a deep reparametrization of the maximum a posteriori formulation commonly employed in multi-frame image restoration tasks. Our approach is derived by introducing a learned error metric and a latent representation of the target image, which transforms the MAP objective to a deep feature space. The deep reparametrization allows us to directly model the image formation process in the latent space, and to integrate learned image priors into the prediction. Our approach thereby leverages the advantages of deep learning, while also benefiting from the principled multi-frame fusion provided by the classical MAP formulation. We validate our approach through comprehensive experiments on burst denoising and burst super-resolution datasets. Our approach sets a new state-of-the-art for both tasks, demonstrating the generality and effectiveness of the proposed formulation.


