

https://github.com/ServiceNow/HighRes-net


## About

Pytorch implementation of HighRes-net, a neural network for multi-frame super-resolution, trained and tested on the European Space Agency’s Kelvin competition. This is a ServiceNow Research project that was started at Element AI.


# HighRes-net: Multi Frame Super-Resolution by Recursive Fusion

[](https://github.com/ServiceNow/HighRes-net#highres-net-multi-frame-super-resolution-by-recursive-fusion)

Pytorch implementation of HighRes-net, a neural network for multi frame super-resolution (MFSR), trained and tested on the [European Space Agency's Kelvin competition](https://kelvins.esa.int/proba-v-super-resolution/home/).

