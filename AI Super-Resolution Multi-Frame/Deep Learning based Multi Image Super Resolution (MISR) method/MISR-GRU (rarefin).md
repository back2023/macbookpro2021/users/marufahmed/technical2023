

https://github.com/rarefin/MISR-GRU

## About

PyTorch Implementation of Deep Learning based Multi Image Super Resolution (MISR) method


# Multi-Image Super-Resolution for Remote Sensing using Deep RecurrentNetworks

[](https://github.com/rarefin/MISR-GRU#multi-image-super-resolution-for-remote-sensing-using-deep-recurrentnetworks)

Pytorch implementation of MISR-GRU, a deep neural network for multi image super-resolution (MISR), for ProbaV Super Resolution Competition [European Space Agency's Kelvin competition](https://kelvins.esa.int/proba-v-super-resolution/home/).

