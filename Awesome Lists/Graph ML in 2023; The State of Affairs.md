
2022 comes to an end and it is about time to sit down and reflect upon the achievements made in Graph ML as well as to hypothesize about possible breakthroughs in 2023. Tune in 🎄☕

_The article is written together with_ [_Hongyu Ren_](http://hyren.me/) _(Stanford University),_ [_Zhaocheng Zhu_](https://kiddozhu.github.io/) _(Mila & University of Montreal). We thank_ [_Christopher Morris_](https://chrsmrrs.github.io/) _and_ [_Johannes Brandstetter_](https://www.microsoft.com/en-us/research/people/johannesb/) _for the feedback and helping with the Theory and PDE sections, respectively. Follow_ [_Michael_](https://twitter.com/michael_galkin)_,_ [_Hongyu_](https://twitter.com/ren_hongyu)_,_ [_Zhaocheng_](https://twitter.com/zhu_zhaocheng), [_Christopher_](https://twitter.com/chrsmrrs)_, and_ [_Johannes_](https://twitter.com/jo_brandstetter) _here on Medium and Twitter for more graph ml-related discussions._

https://towardsdatascience.com/graph-ml-in-2023-the-state-of-affairs-1ba920cb9232






