
https://github.com/weihaox/awesome-image-translation


# Awesome Image Translation [![Awesome](https://camo.githubusercontent.com/3418ba3754faddfb88c5cbdc94c31ad670fc693c8caa59bc2806c9836acc04e4/68747470733a2f2f617765736f6d652e72652f62616467652e737667)](https://awesome.re/)
 
> A curated collection of awesome image-to-image translation.

## Table of Contents

- [2024](https://github.com/weihaox/awesome-image-translation/blob/master/docs/2024.md)
- [2023](https://github.com/weihaox/awesome-image-translation/blob/master/docs/2023.md)
- [2022](https://github.com/weihaox/awesome-image-translation/blob/master/docs/2022.md)
- [2021](https://github.com/weihaox/awesome-image-translation/blob/master/docs/2021.md)
- [2020](https://github.com/weihaox/awesome-image-translation/blob/master/docs/2020.md)
- [2019](https://github.com/weihaox/awesome-image-translation/blob/master/docs/2019.md)
- [Before 2018](https://github.com/weihaox/awesome-image-translation/blob/master/docs/BEFORE-2018.md)

## Open Source Frameworks

- [joliGEN - An integrated framework for training custom generative AI image-to-image models](https://github.com/jolibrain/joliGEN)