
https://github.com/jaychempan/Awesome-LWMs

## About

🌍 A Collection of Awesome Large Weather Models (LWMs) | AI for Earth (AI4Earth) | AI for Science (AI4Science)


# 🌍 Awesome Large Weather Models (LWMs) | AI for Earth (AI4Earth) | AI for Science (AI4Science)

[](https://github.com/jaychempan/Awesome-LWMs#-awesome-large-weather-models-lwms--ai-for-earth-ai4earth--ai-for-science-ai4science)

## 🧭 Guideline

[](https://github.com/jaychempan/Awesome-LWMs#-guideline)

A collection of articles on **Large Weather Models (LWMs)**, to make it easier to find and learn. 👏 Contributions to this hub are welcome!


