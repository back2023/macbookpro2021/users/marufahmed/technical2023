
https://github.com/ModelTC/awesome-lm-system


This repo collects papers, repos, tools for large model system, including training, inference, serving and compression.

- [Awesome Large Model (LM) System](https://github.com/ModelTC/awesome-lm-system#awesome-large-model-lm-system-)
    - [Papers](https://github.com/ModelTC/awesome-lm-system#papers)
        - [Training](https://github.com/ModelTC/awesome-lm-system#training)
        - [Inference](https://github.com/ModelTC/awesome-lm-system#inference)
        - [Benchmark](https://github.com/ModelTC/awesome-lm-system#benchmark)
        - [Survey](https://github.com/ModelTC/awesome-lm-system#survey)
    - [Frameworks](https://github.com/ModelTC/awesome-lm-system#frameworks)





