
https://www.marktechpost.com/2023/07/24/a-study-on-various-deep-learning-based-weather-forecasting-models/

**ClimaX: Foundation Model For Weather & Climate**

**Pangu-Weather For Global Weather Forecasting**

**A Multi-Resolution Deep Learning Framework**

**Real-time Bias Correction of Wind Field Forecasts**


**Predicting Wind Farm Power And Downstream Wakes Using Weather Patterns**

A recent study by the Ocean University of China, the National Marine Environment Forecasting Center, and the University of Portsmouth designed a multi-task learning loss function to correct wind speed and wind direction using a single model. They implemented it in the Multi-Task-Double Encoder Trajectory Gated Recurrent Unit (MT-DETrajGRU) model, which employs an enhanced “double-encoder forecaster” architecture to model the spatiotemporal sequence wind components. The western North Pacific (WNP) served as the research region. The EC’s 10-day wind-field forecasts were corrected for rolling bias in real-time from December 2020 to November 2021 throughout all four seasons. After being adjusted with the MT-DETrajGRU model, the wind speed and wind direction biases in the four seasons were reduced by 8-11% and 9-14%, respectively, compared with the original EC forecasts.


**GraphCast: Providing Efficient Medium-Range Global Weather Forecasting**



**WeatherFusionNet For Predicting Precipitation from Satellite Data**

The first model, sat2rad, is a U-Net-based deep learning model that estimates rainfall in the current satellite frame time step. This model predicts rainfall for the full satellite area using convolutional neural networks’ spatial invariance, even if radar data is only available for a smaller area. The sat2rad model was applied to all four satellite frames separately to generate four channels.

The second model, PhyDNet, is a recurrent convolutional network that separates physical dynamics from supplementary visual input. Two branches of PhyDNet handle physical dynamics and residual information for future prediction. Due to competition limits, PhyDNet was trained on satellite data instead of radar frames. To make the prediction, another U-Net merged the outputs of both models with the input sequence.

The study indicated that employing the sat2rad and PhyDNet models increased rainfall prediction. The spatial invariance of convolutional neural networks helped estimate rainfall for the full satellite area, even if radar data was only available for a smaller area.


**WF-UNet: Weather Fusion UNet for Precipitation Nowcasting**

Collaborative research between Maastricht University and Utrecht University explores the feasibility of using a UNet core model,

Using the ERA5 dataset from Copernicus, the European Union’s Earth observation program, the team compiled radar images of precipitation and wind for six years (January 2016 to December 2021) across 14 European nations, with 1-hour temporal resolution and 31 square km spatial resolution.



**Source:**

- https://arxiv.org/pdf/2210.12137.pdf
- https://arxiv.org/abs/2212.14160
- https://arxiv.org/pdf/2211.16824.pdf
- https://arxiv.org/pdf/2211.02556.pdf
- https://arxiv.org/pdf/2212.12794.pdf
- https://arxiv.org/pdf/2301.10343.pdf
- https://arxiv.org/pdf/2302.04102.pdf
- https://arxiv.org/pdf/2302.05886.pdf
- https://search.zeta-alpha.com/tags/68633