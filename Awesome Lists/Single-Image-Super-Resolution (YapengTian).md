
https://github.com/YapengTian/Single-Image-Super-Resolution


A list of resources for example-based single image super-resolution, inspired by [Awesome-deep-vision](https://github.com/kjw0612/awesome-deep-vision) and [Awesome Computer Vision](https://github.com/jbhuang0604/awesome-computer-vision) .

By [Yapeng Tian](https://github.com/YapengTian), [Yunlun Zhang](https://github.com/yulunzhang), [Xiaoyu Xiang](https://github.com/Mukosame) (if you have any suggestions, please contact us! Email: [yapeng.tian@utdallas.edu](mailto:yapeng.tian@utdallas.edu) OR [yulun100@gmail.com](mailto:yulun100@gmail.com) [ORxiaoyu.xiang.ai@gmail.com](mailto:ORxiaoyu.xiang.ai@gmail.com)).

Tip: For SR beginners, I recommend you to read some early learning based SISR works which will help understand the problem.


