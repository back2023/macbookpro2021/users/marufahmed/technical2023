
https://github.com/shengchaochen82/Awesome-Foundation-Models-for-Weather-and-Climate


## About

A comprehesive survey about foundation models for weather and cliamte data understanding.

# Awesome-Foundation-Models-for-Weather-and-Climate

[](https://github.com/shengchaochen82/Awesome-Foundation-Models-for-Weather-and-Climate#awesome-foundation-models-for-weather-and-climate)

[![Awesome](https://camo.githubusercontent.com/3418ba3754faddfb88c5cbdc94c31ad670fc693c8caa59bc2806c9836acc04e4/68747470733a2f2f617765736f6d652e72652f62616467652e737667)](https://awesome.re/) [![PRs Welcome](https://camo.githubusercontent.com/cb67f919951e40ddd17ba6d048853c12366d417ded3afe55f186e750985b2efa/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f5052732d57656c636f6d652d677265656e)](https://camo.githubusercontent.com/cb67f919951e40ddd17ba6d048853c12366d417ded3afe55f186e750985b2efa/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f5052732d57656c636f6d652d677265656e) [![Stars](https://camo.githubusercontent.com/6b2473637f0a9a78dbbe442a9858349fc17f3c393b8c0ab01c0ae84b4d5b8b73/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f73746172732f7368656e676368616f6368656e38322f417765736f6d652d466f756e646174696f6e2d4d6f64656c732d666f722d576561746865722d616e642d436c696d617465)](https://camo.githubusercontent.com/6b2473637f0a9a78dbbe442a9858349fc17f3c393b8c0ab01c0ae84b4d5b8b73/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f73746172732f7368656e676368616f6368656e38322f417765736f6d652d466f756e646174696f6e2d4d6f64656c732d666f722d576561746865722d616e642d436c696d617465)

A professionally curated list of **Large Foundation Models for Weather and Climate Data Understanding (e.g., time-series, spatio-temporal series, video streams, graphs, and text)** with awesome resources (paper, code, data, etc.), which aims to comprehensively and systematically summarize the recent advances to the best of our knowledge.

**OPEN TO COLLABORATION! If you have any new insights in any relevant research direction or just want to chat, please drop me an email (shengchao.chen.uts AT gmail DOT com).**

**[Paper]** Our survey: [Foundation Models for Weather and Climate Data Understanding: A Comprehensive Survey](https://arxiv.org/pdf/2312.03014.pdf) has appeared on arXiv, which is the first work to comprehensively and systematically summarize DL-based weather and climate data understanding, paving the way for the development of weather and climate foundation models. 🌤️⛈️❄️


