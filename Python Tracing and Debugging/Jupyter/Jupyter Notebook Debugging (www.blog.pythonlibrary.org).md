
https://www.blog.pythonlibrary.org/2018/10/17/jupyter-notebook-debugging/


### Using pdb

- The pdb module is Python’s debugger module. Just as C++ has gdb, Python has pdb.
- One way to figure out what is going on is by adding a breakpoint using pdb’s **set_trace()** function:
```python
def bad_function(var):
    import pdb
    pdb.set_trace()
    return var + 0

bad_function("Mike")
```
- Now when you run the cell, you will get a prompt in the output which you can use to inspect the variables and basically run code live.
- If you happen to have **Python 3.7**, then you can simplify the example above by using the new **breakpoint** built-in,
```python
def bad_function(var):
    breakpoint()
    return var + 0

bad_function("Mike")
```
- This code is functionally equivalent to the previous example but uses the new **breakpoint** function instead.

- You can use any of pdb’s command right inside of your Jupyter Notebook. Here are some examples:
	- w(here) – Print the stack trace
	- d(own) – Move the current frame X number of levels down. Defaults to one.
	- u(p) – Move the current frame X number of levels up. Defaults to one.
	- b(reak) – With a *lineno* argument, set a break point at that line number in the current file / context
	- s(tep) – Execute the current line and stop at the next possible line
	- c(ontinue) – Continue execution


### ipdb

- IPython also has a debugger called **ipdb**. However it does not work with Jupyter Notebook directly.
- You would need to connect to the kernel using something like **Jupyter console** and run it from there to use it.

- However there is an IPython debugger that we can use called **IPython.core.debugger.set_trace**. Let’s create a cell with the following code:
```bash
from IPython.core.debugger import set_trace

def bad_function(var):
    set_trace()
    return var + 0

bad_function("Mike")
```
- The IPython debugger uses the same commands as the Python debugger does. 
- The main difference is that it provides syntax highlighting and was originally designed to work in the IPython console.

- There is one other way to open up the **ipdb** debugger and that is by using the **%pdb** magic. Here is some sample code you can try in a Notebook cell:
```python
%pdb

def bad_function(var):
    return var + 0

bad_function("Mike")
```


### What about `%%debug`?

- You can use `%%debug` to debug the entire cell like this:
```python
%%debug

def bad_function(var):
    return var + 0

bad_function("Mike")
```
- This will start the debugging session immediately when you run the cell.
- Note that you could also use `%debug` if you want to debug a single line.


### Wrapping Up

- I personally prefer to use Python’s pdb module, but you can use the IPython.core.debugger to get the same functionality and it could be better if you prefer to have syntax highlighting.
- There is also a newer “visual debugger” package called the PixieDebugger from the [pixiedust](https://github.com/pixiedust/pixiedust) package: