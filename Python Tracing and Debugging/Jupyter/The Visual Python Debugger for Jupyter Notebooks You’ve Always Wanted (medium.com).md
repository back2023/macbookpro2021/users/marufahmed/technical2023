
https://medium.com/codait/the-visual-python-debugger-for-jupyter-notebooks-youve-always-wanted-761713babc62

- The PixieDust team is happy to introduce the first (to the best of our knowledge) visual Python debugger for Jupyter Notebooks.
- **_Editor’s note:_** PixieDebugger currently only works with classic Jupyter Notebooks; JupyterLab is not yet supported.

# Introducing PixieDebugger

- the PixieDebugger is a visual Python debugger built as a [PixieApp](https://ibm-watson-data-lab.github.io/pixiedust/pixieapps.html),
- To invoke the PixieDebugger for a specific cell, simply add the **`%%pixie_debugger`** magic at the top of the cell and run it.
- **_Note:_** As a prerequisite, install PixieDust using the following pip command: ==`pip install pixiedust`==. You’ll also need to import it into its own cell: `import pixiedust`.

- When running the cell, the PixieDebugger will automatically be invoked to break at the first executable line of code.






