
https://github.com/lckr/jupyterlab-variableInspector#jupyterlab_variableinspector

- Jupyterlab extension that shows currently used variables and their values.

- Allows inspection of variables for both consoles and notebooks.
- Allows inspection of matrices in a datagrid-viewer. This might not work for large matrices.
- Allows an inline and interactive inspection of Jupyter Widgets.

