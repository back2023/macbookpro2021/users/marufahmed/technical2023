
https://betterprogramming.pub/using-decorators-to-instrument-python-code-with-opentelemetry-traces-d7f1c7d6f632


Python decorators can help keep OpenTelemetry tracing instrumentation DRY

Last week, I wrote a [post](https://betterprogramming.pub/using-the-decorator-pattern-to-auto-instrument-net-classes-with-otel-tracing-781bf2be62ff) on the topic of using the Decorator design pattern to help remove some of the boilerplate required to set up tracing. My example code was using .NET, which required some heavy lifting, using a DispatchProxy class to intercept method calls and inject the tracing logic. Python, however, makes our lives much easier with built-in support for function decorators.





