
https://medium.com/@genexu/extracting-the-wrapped-function-from-a-decorator-on-python-call-stack-2ee2e48cdd8e
Mar 22, 2020


- I needed the file path of the desired caller. 
	- This is used to identify ownership of the caller function based on GitHub’s CODEOWNERS file.

- The solution seemed simple enough, either using `inspect.stack()` to get the entire stack, 
- or using `sys._getframe().f_back` to get the caller frames one by one.
- Once I capture the desired frame object `f`, the file path of the caller function is `f.f_code.co_filename`.

- The `tracing` module:
```python
# tracing.py
import inspect
def print_stack():  
    stack = inspect.stack()  
    print(stack)
``` 
 
with the newly added `decorators` module.





