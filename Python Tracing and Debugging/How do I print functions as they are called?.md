
https://stackoverflow.com/questions/8315389/how-do-i-print-functions-as-they-are-called

You can do this with a trace function (props to Spacedman for improving the original version of this to trace returns and use some nice indenting):

```python
def tracefunc(frame, event, arg, indent=[0]):
      if event == "call":
          indent[0] += 2
          print("-" * indent[0] + "> call function", frame.f_code.co_name)
      elif event == "return":
          print("<" + "-" * indent[0], "exit function", frame.f_code.co_name)
          indent[0] -= 2
      return tracefunc

import sys
sys.setprofile(tracefunc)

main()   # or whatever kicks off your script
```

**_Coming back to revisit this four years later, it behooves me to mention that in Python 2.6 and later, you can get better performance by using `sys.setprofile()` rather than `sys.settrace()`. The same trace function can be used; it's just that the profile function is called only when a function is entered or exited, so what's inside the function executes at full speed._**





