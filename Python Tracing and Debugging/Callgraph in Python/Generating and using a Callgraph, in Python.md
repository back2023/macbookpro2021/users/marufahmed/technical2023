
https://cerfacs.fr/coop/pycallgraph

### Getting started[](https://cerfacs.fr/coop/pycallgraph#getting-started "Permanent link")

The [call graph](https://en.wikipedia.org/wiki/Call_graph) is a log of the different routines calling each other. It traces what the reader would look if he were to perform an exhaustive read in the chronological order.








