
https://monkeytype.readthedocs.io/en/stable/tracing.html


The core data type in MonkeyType is the [`CallTrace`](https://monkeytype.readthedocs.io/en/stable/tracing.html#monkeytype.tracing.CallTrace "monkeytype.tracing.CallTrace"). A [`CallTrace`](https://monkeytype.readthedocs.io/en/stable/tracing.html#monkeytype.tracing.CallTrace "monkeytype.tracing.CallTrace") instance represents a single traced call of a single function or method, including the concrete type of each argument and the return or yield type.

A [`CallTrace`](https://monkeytype.readthedocs.io/en/stable/tracing.html#monkeytype.tracing.CallTrace "monkeytype.tracing.CallTrace") is recorded by [monkeytype run](https://monkeytype.readthedocs.io/en/stable/tracing.html#monkeytype-run) or the [`trace()`](https://monkeytype.readthedocs.io/en/stable/tracing.html#monkeytype.trace "monkeytype.trace") context manager (or direct use of a [`CallTracer`](https://monkeytype.readthedocs.io/en/stable/tracing.html#monkeytype.tracing.CallTracer "monkeytype.tracing.CallTracer")), logged via a [`CallTraceLogger`](https://monkeytype.readthedocs.io/en/stable/tracing.html#monkeytype.tracing.CallTraceLogger "monkeytype.tracing.CallTraceLogger"), probably stored in a [`CallTraceStore`](https://monkeytype.readthedocs.io/en/stable/stores.html#monkeytype.db.base.CallTraceStore "monkeytype.db.base.CallTraceStore"), and later queried from that store by [monkeytype stub](https://monkeytype.readthedocs.io/en/stable/generation.html#monkeytype-stub) or [monkeytype apply](https://monkeytype.readthedocs.io/en/stable/generation.html#monkeytype-apply) and combined with all other traces of the same function in order to generate a stub or type annotation for that function.




