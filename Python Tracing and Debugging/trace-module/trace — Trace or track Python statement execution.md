
https://docs.python.org/3/library/trace.html

- The [`trace`](https://docs.python.org/3/library/trace.html#module-trace "trace: Trace or track Python statement execution.") module allows you to trace program execution, 
	- generate annotated statement coverage listings, 
	- print caller/callee relationships and 
	- list functions executed during a program run. 
- It can be used in another program or from the command line.

### Main options

- The [`--listfuncs`](https://docs.python.org/3/library/trace.html#cmdoption-trace-l) option is mutually exclusive with the [`--trace`](https://docs.python.org/3/library/trace.html#cmdoption-trace-t) and [`--count`](https://docs.python.org/3/library/trace.html#cmdoption-trace-c) options.
- -T, --trackcalls[](https://docs.python.org/3/library/trace.html#cmdoption-trace-T "Permalink to this definition") Display the calling relationships exposed by running the program.
