
https://realpython.com/python-traceback/

- Python prints a **traceback** when an exception is raised in your code.


## What Is a Python Traceback?
- A traceback is a report containing the function calls made in your code at a specific point.
- The final line of the traceback output tells you what type of exception was raised along with some relevant information about that exception. 
- The previous lines of the traceback point out the code that resulted in the exception being raised.

**Note**: If you are used to seeing stack traces in other programming languages, then you’ll notice a major difference in the way a Python traceback looks in comparison. Most other languages print the exception at the top and then go from top to bottom, most recent calls to least recent.


### Specific Traceback Walkthrough

**Note**: Python’s feature of displaying the previous exceptions tracebacks were added in Python 3.




