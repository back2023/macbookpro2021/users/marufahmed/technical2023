
https://stackoverflow.com/questions/1095543/get-name-of-calling-functions-module-in-python


```python
import inspect 

def info(msg):
    frm = inspect.stack()[1]
    mod = inspect.getmodule(frm[0])
    print '[%s] %s' % (mod.__name__, msg)
```
*** 

```python
def caller_name():
    frame=inspect.currentframe()
    frame=frame.f_back.f_back
    code=frame.f_code
    return code.co_filename
```
Then update your existing method as follows:
```python
def info(msg):
    caller = caller_name()
    print '[%s] %s' % (caller, msg)
```
***


```python
>>> f = sys._current_frames().values()[0]
>>> # for python3: f = list(sys._current_frames().values())[0]

>>> print f.f_back.f_globals['__file__']
'/base/data/home/apps/apricot/1.6456165165151/caller.py'

>>> print f.f_back.f_globals['__name__']
'__main__'
```






