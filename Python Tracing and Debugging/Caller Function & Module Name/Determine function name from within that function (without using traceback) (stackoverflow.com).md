

https://stackoverflow.com/questions/5067604/determine-function-name-from-within-that-function-without-using-traceback


```python
import inspect

def foo():
   print(inspect.stack()[0][3])
   print(inspect.stack()[1][3])  # will give the caller of foos name, if something called foo

foo()
```