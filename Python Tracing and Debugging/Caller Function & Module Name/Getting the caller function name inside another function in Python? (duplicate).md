
https://stackoverflow.com/questions/900392/getting-the-caller-function-name-inside-another-function-in-python

***
https://stackoverflow.com/a/900404
- For **Python 3.5+**, each frame record is a [named tuple](https://docs.python.org/3.5/glossary.html#term-named-tuple) so you need to replace
    ```python
    print inspect.stack()[1][3]
    ```
    with
    ```python
    print(inspect.stack()[1].function)
    ```


***
https://stackoverflow.com/a/28185561
There are two ways, using `sys` and `inspect` modules:
	- `sys._getframe(1).f_code.co_name`
	- `inspect.stack()[1][3]`
- The `stack()` form is less readable and is implementation dependent since it calls `sys._getframe()`
