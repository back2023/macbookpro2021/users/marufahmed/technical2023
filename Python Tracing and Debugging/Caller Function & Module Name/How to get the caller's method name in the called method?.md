
https://stackoverflow.com/questions/2654113/how-to-get-the-callers-method-name-in-the-called-method


```python
>>> import inspect
>>> def f1(): f2()
... 
>>> def f2():
...   curframe = inspect.currentframe()
...   calframe = inspect.getouterframes(curframe, 2)
...   print('caller name:', calframe[1][3])
... 
>>> f1()
caller name: f1
```


```python
import sys
print sys._getframe().f_back.f_code.co_name
```





