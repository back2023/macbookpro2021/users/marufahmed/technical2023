
https://www.w3resource.com/python-exercises/python-basic-exercise-126.php


```python
import inspect
def add(x, y):
    return x + y
print(inspect.getmodule(add))

```

```python
from inspect import getmodule
from math import sqrt
print(getmodule(sqrt))

```
