
https://code-maven.com/slides/python/name-of-the-caller-function

```python
import inspect

def first():
    print("in first")
    print("Called by", inspect.stack()[1][3])
    second()

def second():
    print("in second")
    print("Called by", inspect.stack()[1][3])

def main():
    first()

main()
```

Copy from
### python/examples/advanced/name_of_caller_function.py
https://github.com/szabgab/slides/blob/main/python/examples/advanced/name_of_caller_function.py





