
https://towardsdatascience.com/3-tools-to-track-and-visualize-the-execution-of-your-python-code-666a153e435e


f so, this article will give you the tools to do exactly the above. Those 3 tools are:

- Loguru — print better exceptions
- snoop — print the lines of code being executed in a function
- heartrate — visualize the execution of a Python program in real-time

And all it takes to use these tools is one line of code!
