
https://towardsdatascience.com/a-simple-way-to-trace-code-in-python-a15a25cbbf51

# introduction

Our goal is to create a reusable way to trace functions in Python. We do this by coding a decorator with Python's `functools` library. This decorator will then be applied to functions whose runtime we are interested in.

# Tracing Decorator: @tracefunc

The code below represents a common decorator pattern that has a reusable and flexible structure. Notice the placement of `functool.wraps`. It is a decorator for our closure. This decorator preserves `func`’s metadata as it is passed to the closure.







