
https://functiontrace.com/#installation

A graphical Python profiler that provides a clear view of your application's execution while being both low-overhead and easy to use.
