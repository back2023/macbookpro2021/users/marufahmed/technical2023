
https://github.com/Vincent-Hoo/Knowledge-Distillation-for-Super-resolution

# Knowledge Distillation for Super-Resolution

### [](https://github.com/Vincent-Hoo/Knowledge-Distillation-for-Super-resolution#introduction)Introduction

This repository is the official implementation of the paper **"FAKD: Feature-Affinity Based Knowledge Distillation for Efficient Image Super-Resolution"** from **ICIP 2020**. In this work, we propose a novel and efficient SR model, name Feature Affinity-based Knowledge Distillation (FAKD), by transferring the structural knowledge of a heavy teacher model to a lightweight student model. To transfer the structural knowledge effectively, FAKD aims to distill the second-order statistical information from feature maps and trains a lightweight student network with low computational and memory cost. Experimental results demonstrate the efficacy of our method and superiority over other knowledge distillation based methods in terms of both quantitative and visual metrics.