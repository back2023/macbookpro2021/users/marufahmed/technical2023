
### Basics: An example workflow
https://snakemake.readthedocs.io/en/stable/tutorial/basics.html

* ***A Snakemake workflow is defined by specifying rules in a Snakefile**. 
* **Rules decompose the workflow into small steps** by specifying how to create sets of **output files** from sets of **input files**. 
* Snakemake automatically **determines the dependencies** between the rules by matching file names.

###### Step 1:
* In the working directory, **create a new file** called `Snakefile`
* A Snakemake rule has a name and a number of directives.
* In the shell command string, we can refer to elements of the rule via braces notation (similar to the Python format function).
* To generate the target files, **Snakemake applies the rules given in the Snakefile in a top-down way**. 
* The application of a rule to generate a set of output files is called **job**.
* For each input file of a job, Snakemake again (i.e. recursively) determines rules that can be applied to generate it. This yields a [directed acyclic graph (DAG)](https://en.wikipedia.org/wiki/Directed_acyclic_graph) of jobs where the edges represent dependencies.
* Whenever executing a workflow, you need to specify the number of cores to use.
* Snakemake **only re-runs jobs if one of the input files is newer than one of the output files or one of the input files will be updated by another job**.

###### Step 2: Generalizing
* Snakemake allows **generalizing rules by using named wildcards**.
* Note that you can have multiple wildcards in your file paths, however, to avoid conflicts with other jobs of the same rule, **all output files** of a rule have to **contain exactly the same wildcards**.

###### Step 3: Sorting
* Snakemake **automatically creates missing directories** before jobs are executed.

###### Step 4:
* create a **visualization of the DAG** using the `dot` command provided by [Graphviz](https://www.graphviz.org/).

###### Step 5:
* Snakemake provides a **helper function for collecting input files** that helps us to describe the aggregation in this step.

###### Step 6: Using custom scripts
* While Snakemake also allows you to directly write Python code inside a rule, it is usually reasonable to move such logic into separate scripts.
* Snakemake offers the `script` directive.
* Script paths are always relative to the referring Snakefile. 
* In the script, all properties of the rule are available as attributes of a global `snakemake` object.
* **the parsing of command line arguments is unnecessary**.

###### Step 7: Adding a target rule
* Apart from filenames, Snakemake **also accepts rule names as targets** if the requested rule does not have wildcards.
* Moreover, if no target is given at the command line, Snakemake will define the **first rule** of the Snakefile as the target.
* Note that, apart from Snakemake considering the first rule of the workflow as the default target, **the order of rules in the Snakefile is arbitrary and does not influence the DAG of jobs**.


 




### Advanced: Decorating the example workflow
https://snakemake.readthedocs.io/en/stable/tutorial/advanced.html

###### Step 1: Specifying the number of used threads
* ***Snakemake can be made aware of the threads a rule needs** with the `threads` directive.

###### Step 2: Config files
* Snakemake provides a [config file mechanism](https://snakemake.readthedocs.io/en/latest/snakefiles/configuration.html). Config files can be written in [JSON](https://json.org/) or [YAML](https://yaml.org/), and are used with the `configfile` directive.

###### Step 3: Input functions
* Snakemake workflows are executed in three phases.
	1.  **Initialization** phase: the files defining the workflow are parsed and all rules are instantiated.
	2. **DAG** phase: the directed acyclic dependency graph of all jobs is built by filling wildcards and matching input files to output files.
	3. **scheduling** phase: the DAG of jobs is executed, with jobs started according to the available resources.
	   
* The expand functions in the list of input files of the rules are executed during the initialization phase.
*  Instead, we need to defer the determination of input files to the DAG phase. This can be achieved by specifying an **input function** instead of a string as inside of the input directive.
* Any normal function would work as well. Input functions take as **single argument** a `wildcards` object, that allows to access the wildcards values via attributes. They have to **return a string or a list of strings**.

###### Step 4: Rule parameters
* Snakemake allows to **define arbitrary parameters** for rules with the `params` directive.

###### Step 5: Logging
* Snakemake allows to **specify log files** for rules.


###### Step 6: Temporary and protected files
* To save disk space, you can **mark output files as temporary**. Snakemake will delete the marked files for you, once all the consuming jobs (that need it as input) have been executed.
*  To **protect** the file **from accidental deletion or modification**. We modify the rule to mark its output file as `protected`.


### Additional features
https://snakemake.readthedocs.io/en/stable/tutorial/additional_features.html

##### Benchmarking 
* With the `benchmark` directive, Snakemake can be instructed to **measure the wall clock time of a job**.

###### Modularization
* Snakemake provides the `include` directive to include another Snakefile into the current one. 

###### Automatic deployment of software dependencies
* With Snakemake, you can go one step further and specify Conda environments per rule.
* `snakemake --use-conda --cores 1` will automatically create required environments and activate them before a job is executed.

###### Tool wrappers
* A wrapper is a short script that wraps (typically) a command line application and makes it directly addressable from within Snakemake.
* For this, Snakemake provides the `wrapper` directive that can be used instead of `shell`, `script`, or `run`.


