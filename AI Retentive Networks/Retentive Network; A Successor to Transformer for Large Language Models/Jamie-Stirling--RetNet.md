
https://github.com/Jamie-Stirling/RetNet


# RetNet

An implementation of [Retentive Network: A Successor to Transformer for Large Language Models](https://arxiv.org/pdf/2307.08621.pdf) in PyTorch.

## [](https://github.com/Jamie-Stirling/RetNet#about-this-repository)About this repository

This is a minimal, pure pytorch implementation of RetNet. RetNet paper: [Retentive Network: A Successor to Transformer for Large Language Models](https://arxiv.org/pdf/2307.08621.pdf).

The contributors(s) to this repository are not authors of the original paper. All credit for the idea and formulation of RetNet goes to the original authors.

The purpose of this repository is to aid scientific and technological understanding and advancement. The code prioritizes correctness and readability over optimization.

## [](https://github.com/Jamie-Stirling/RetNet#features-implemented)Features implemented

- Single-scale and MultiScale retention:
    - parallel paradigm
    - recurrent paradigm
    - chunkwise paradigm
- Multi-layer retentive network with FFN and LayerNorm
    - parallel paradigm
    - recurrent paradigm
    - chunkwise paradigm
- Causal language model (CLM) built on top of the the retentive network


