
https://medium.com/ai-fusion-labs/retentive-networks-retnet-explained-the-much-awaited-transformers-killer-is-here-6c17e3e8add8


· RetNet has **BETTER** language modeling performance

· RetNet achieves that with **3.4x** lower memory consumption

· ….**8.4x** higher throughput

· …**15.6x** lower latency



