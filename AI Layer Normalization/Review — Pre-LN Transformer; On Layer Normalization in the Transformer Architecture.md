

https://sh-tsang.medium.com/review-pre-ln-transformer-on-layer-normalization-in-the-transformer-architecture-b6c91a89e9ab


> **On Layer Normalization in the Transformer Architecture**  
> **Pre-LN Transformer**, by Microsoft Research Asia, University of Chinese Academy of Sciences, Peking University, Microsoft Research, and Nankai University  
> **2020 ICML, Over 100 Citations** ([Sik-Ho Tsang](https://medium.com/u/aff72a0c1243?source=post_page-----b6c91a89e9ab--------------------------------) @ Medium)  
> Natural Language Processing, NLP, Language Model, Machine Translation, [Transformer](https://sh-tsang.medium.com/review-attention-is-all-you-need-transformer-96c787ecdec1), [Layer Normalization](https://sh-tsang.medium.com/overview-my-reviewed-paper-lists-tutorials-946ce59fbf9e#:~:text=%5D%20%5BGELU%5D%20%5B-,Layer%20Norm,-%2C%20LN%5D%0A2017), [BERT](https://sh-tsang.medium.com/59b1684882db)

- **The original-designed Post-LN** [**Transformer**](https://sh-tsang.medium.com/review-attention-is-all-you-need-transformer-96c787ecdec1), which places the [layer normalization](https://sh-tsang.medium.com/overview-my-reviewed-paper-lists-tutorials-946ce59fbf9e#:~:text=%5D%20%5BGELU%5D%20%5B-,Layer%20Norm,-%2C%20LN%5D%0A2017) between the residual blocks, **the expected gradients** of the parameters near the output layer are **large**. Using a large learning rate on those gradients makes the **training unstable**. The warm-up stage can help to avoid this.
- On the other hand, if **Pre-LN** [**Transformer**](https://sh-tsang.medium.com/review-attention-is-all-you-need-transformer-96c787ecdec1) is used where the [**layer normalization**](https://sh-tsang.medium.com/overview-my-reviewed-paper-lists-tutorials-946ce59fbf9e#:~:text=%5D%20%5BGELU%5D%20%5B-,Layer%20Norm,-%2C%20LN%5D%0A2017) **is put inside the residual blocks**, the **gradients** are **well-behaved at initialization**. The **warm-up stage can be removed**.

