
https://www.mybib.com/

Note: Good search function, and list option. 

- DOI search produces accurate result. 
	So, use it for search when possible
- Sometimes full title does not produce a result, but removing part of the title (usually at the front) produces the desired result. 
- Use quotation  for a exact match.
- References are automatically sorted as they are added. 

- Does not show access date