

https://irsc.libguides.com/APA/Authors


**Personal Authors** (9.7-9.12)

List author names in the order they appear in the document or text. Begin with the surname followed by the initials of the first and middle name. Place a comma after the surname. Place a period after each initial in the first and middle name. Separate names with a comma and space. When a work has more than one author put an ampersand (&) before the last surname. End with a period.

Jones, B.R., Van deWater, M.L., Anderson, K., & MacMillan, J.S.

Morgan, J.R. & Tellescue, A. (Eds.).

de Wollen, S.

**2 to 20 authors**: List up to 20 author surnames and initials separated by commas. List surnames as they appear in the document.

**21 or more authors**: List the first **19** then add three ellipses and the last author's name.

Carrey, J. M., Riley, P., James, K. K., Edgerton, R., Smith, T. A., Rowland, P., Perry, T. H., Shimoni, R. D., Dion, C., Tignor, M, Auberry, K., Carlson, A., Williams, B., Johnson, S. Kirkman, L. J., Vista, D. D., Barry, B., Austen, J., Andover, S. S....Henry, J.D.




