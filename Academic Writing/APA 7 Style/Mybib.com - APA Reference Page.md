
https://www.mybib.com/guides/apa-format/apa-reference-page

### The Basics

A Reference Page must:

- Have the title of '**Reference List**' at the very top
- Show every source you've directly referenced in your paper, or taken data from to help you write your paper.
- List all sources in **alphabetical order** by the author's last name. For example, a source by Albert Einstein would be higher in the list than a source by Elon Musk, as the E in Einstein comes before the M in Musk alphabetically.
- Use a hanging indent for each entry after the first line, indented by half an inch from the left margin.
- Retain all original punctuation and formatting for any names and titles of the sources.
