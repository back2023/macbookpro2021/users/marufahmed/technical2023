
https://console.cloud.google.com/vertex-ai/publishers/stability-ai/model-garden/stable-diffusion-4x-upscaler


**Stable Diffusion 4x upscaler** is a text-conditioned super resolution model originally released through the [Stable Diffusion V2 GitHub repository](https://github.com/Stability-AI/stablediffusion#image-upscaling-with-stable-diffusion). The model is a text guided latent diffusion model trained from a subset of the LAION dataset. It uses OpenCLIP as the text encoder. The model can be used as an additional stage of any existing text-conditioned image generation pipeline or as a standalone super resolution process.

Latent diffusion models (LDMs) were first proposed in the paper "[High-Resolution Image Synthesis with Latent Diffusion Models](https://arxiv.org/abs/2112.10752)" by Rombach et al (2022). To enable diffusion models training on limited computational resources while retaining their quality and flexibility, the authors applied them in the latent space of powerful pretrained autoencoders. Training diffusion models on such a representation allows for the first time to reach a near-optimal point between complexity reduction and detail preservation, greatly boosting visual fidelity. The latent diffusion models achieved a new state of the art for image inpainting and highly competitive performance on various tasks, including unconditional image generation, semantic scene synthesis, and super-resolution, while significantly reducing computational requirements compared to pixel-based diffusion models.


