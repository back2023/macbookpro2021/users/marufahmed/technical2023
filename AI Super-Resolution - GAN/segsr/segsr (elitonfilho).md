

https://github.com/elitonfilho/segsr


# Segsr

[](https://github.com/elitonfilho/segsr#segsr)

[![Torch](https://camo.githubusercontent.com/075214aadd0e3ed3fe81c1528025740c7c33b2326ee2a33791f0fa365c3474c9/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d5079546f7263682d7265643f6c6f676f3d7079746f726368266c6162656c436f6c6f723d67726179)](https://pytorch.org/get-started/locally/) [![Hydra](https://camo.githubusercontent.com/c11430878b258aae43158744ca125d13959da1b72785394f60c0f2dbca0a453c/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f636f6e662d68796472612d626c7565)](https://hydra.cc/) [![Ignite](https://camo.githubusercontent.com/3dfae1571d22d65f495210bf55f90ebfce9e30047297a68b7e1128dc4bfc824c/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d49676e6974652d6f72616e67653f6c6f676f3d7079746f726368266c6162656c436f6c6f723d67726179)](https://pytorch-ignite.ai/)

This repository contains the official implementation of the paper "Joint-task learning to improve perceptually-aware super-resolution of aerial images" from the IJRS Vol.44. The article is avalaible [here](https://doi.org/10.1080/01431161.2023.2190469).

SegSR focus on improving image super-resolution by using a segmentation module capable of verifying the performance of the generator module. The extra criterion provided by the seg module captures well the reconstruction quality of synthesized images, especially when considering perceptual metrics, such as LPIPS (Learned Perceptual Image Patch Similarity) or PI (Perceptual Index).

If you find our work useful in your search, don't forget to cite it! You can use the BibTeX entry in the end of this Readme.


