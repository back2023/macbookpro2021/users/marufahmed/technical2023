

https://github.com/binh234/isr?tab=readme-ov-file#isr-general-image-super-resolution


Practical algorithms for real-world Image/Video restoration and Face restoration. It leverages rich and diverse priors encapsulated in a pretrained GAN (e.g., StyleGAN2) for image super resolution.


