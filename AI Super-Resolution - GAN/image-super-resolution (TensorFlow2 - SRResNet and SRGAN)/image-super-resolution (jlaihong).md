

https://github.com/jlaihong/image-super-resolution

## About

TensorFlow2 implementation of SRResNet and SRGAN
# Image Super-Resolution using SRResNet and SRGAN


A TensorFlow 2 implementation of the paper [Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network](https://arxiv.org/pdf/1609.04802.pdf)




