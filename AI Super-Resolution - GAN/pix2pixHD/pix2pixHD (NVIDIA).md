

https://github.com/NVIDIA/pix2pixHD



## About

Synthesizing and manipulating 2048x1024 images with conditional GANs

Pytorch implementation of our method for high-resolution (e.g. 2048x1024) photorealistic image-to-image translation. It can be used for turning semantic label maps into photo-realistic images or synthesizing portraits from face label maps.



