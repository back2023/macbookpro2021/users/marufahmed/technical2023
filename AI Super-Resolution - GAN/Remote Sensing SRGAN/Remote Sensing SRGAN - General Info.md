

https://github.com/simon-donike/Remote-Sensing-SRGAN



This repository is the supplemental code of my [master's thesis](https://www.donike.net/m-sc-thesis-deep-super-resolution-of-sentinel-2-time-series/), which has been fundamentally revisited and improved. The SRGAN follows the 2017 paper by [Ledig et al.](https://arxiv.org/abs/1609.04802v5) for the fundamental GAN model, the multi-temporal fusion is inspired by HighResNet by [Deudon et al.](https://arxiv.org/abs/2002.06460)

#### Synopsis

[](https://github.com/simon-donike/Remote-Sensing-SRGAN#synopsis)

This is an SRGAN for both Single-Image and Multi-Image Super-Resolution of Remote Sensing imagery. This model performs SR only on the RGB bands. It has been trained on SPOT-6 and Sentinel-2 image pairs and time-series of the region of Brittany in France, which means generalization of the provided checkpoints to other regions can not be guaranteed.  
With more rigorous versioning, transfer learning and LR scheduling, this workflow surpasss the results obtained in the master's thesis.

#### Performed updates copared to thesis version

[](https://github.com/simon-donike/Remote-Sensing-SRGAN#performed-updates-copared-to-thesis-version)

- proper implementation in Pytorch Lightning, including versioning, logging, experiment tracking
- new dataloaders including stratification (by landcover), normalization, fusion warmup training


