

https://medium.com/@ramyahrgowda/srgan-paper-explained-3d2d575d09ff


This article gives a detailed explanation of the paper [“Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network”](http://openaccess.thecvf.com/content_cvpr_2017/papers/Ledig_Photo-Realistic_Single_Image_CVPR_2017_paper.pdf) published on CVPR in 2017.

