

This fork of [wonbeomjang/ESRGAN-pytorch](https://github.com/wonbeomjang/ESRGAN-pytorch.git) incorporates [Nvidia/apex](https://github.com/NVIDIA/apex)(`apex`) to enable optional training across multiple nodes with multiple GPUs and/or with mixed precision.

