

https://github.com/Papirapi/ESRGAN_Proba-V



# ESRGAN_Proba-V

[](https://github.com/Papirapi/ESRGAN_Proba-V#esrgan_proba-v)

Tackling Proba-V project with a modified version of ESRGAN.

## About Proba-V

[](https://github.com/Papirapi/ESRGAN_Proba-V#about-proba-v)

Proba-V is a miniaturized ESA satellite tasked with a full-scale mission: to map land cover and vegetation growth across the entire earth every two days. It is the latest in ESA’s Proba series of minisatellites, among the smallest satellites launched by the agency.  
It operates on a sun-synchronous near polar Earth orbit at about 820Km guaranteeing the required swath of 2250Km with an instrument field of view of 102 degrees, compatible with the geographical coverage.  
The Proba-V satellite carries a new Vegetation instrument, as single operational payload.  
In the frame of the In-Orbit Demonstration, the Proba-V platform also flies 5 technological payloads: *X-Band transmitter based on GaN RF amplifier, *Energetic Particle Telescope EPT, *Automatic Dependent Surveillance Broadcast (ADS-B) receiver, *SATRAM radiation monitoring system, complementing EPT, *HERMOD (fiber optic connectivity in-situ testing).

The Proba-V mission provides multispectral images to study the evolution of the vegetation cover on a daily and global basis.  
The ‘V’ stands for Vegetation, and the mission is extending the data set of the long-established Vegetation instrument, flown as a secondary payload aboard France’s SPOT-4 and SPOT-5 satellites launched in 1998 and 2002 respectively.  
The Proba-V mission has been developed in the frame of the ESA General Support Technology Program GSTP and the contributors to the Proba-V mission are Belgium and Luxembourg.  
For more details: [https://www.esa.int/Applications/Observing_the_Earth/Proba-V](https://www.esa.int/Applications/Observing_the_Earth/Proba-V)


