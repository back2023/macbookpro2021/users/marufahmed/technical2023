
https://github.com/milesial/Pytorch-UNet/tree/master
Plan: 
Download data, clone repo, and run in notebooks

### Data download
Source: https://www.kaggle.com/c/carvana-image-masking-challenge/data
```bash
cd /g/data/wb00/admin/testing/
mkdir 'Carvana Image Masking Challenge'
cd 'Carvana Image Masking Challenge'
pwd
/g/data/wb00/admin/testing/Carvana Image Masking Challenge

module use /g/data/dk92/apps/Modules/modulefiles/
module load nci-dlwp-cs/2024.04.30
...
unzip carvana-image-masking-challenge.zip
...
rm carvana-image-masking-challenge.zip


# Make datadirs
mkdir -p data/imgs/
unzip train_hq.zip
mv train_hq/* data/imgs/
rm -d train_hq

mkdir -p data/masks/
unzip train_masks.zip
mv train_masks/* data/masks/
rm -d train_masks
```


Remove the original zip to save space as other files can be downloaded separately
Source: https://github.com/milesial/Pytorch-UNet/blob/master/scripts/download_data.sh

### Code download
```
cd /scratch/fp0/mah900/
git clone https://github.com/milesial/Pytorch-UNet.git
cd Pytorch-UNet/

pwd
/scratch/fp0/mah900/Pytorch-UNet

mkdir Notebooks
```


### Notebooks
```
/scratch/fp0/mah900/Pytorch-UNet/Notebooks/Train-01.ipynb
```


###### Use `Enum` instead of `argparse` ???(Not useful to access)
```python
from enum import Enum
class Weekday(Enum):
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6
    SUNDAY = 7
# Not use to access
print(Weekday.THURSDAY)
Weekday.THURSDAY
```
Source: https://docs.python.org/3/howto/enum.html

Try, directly
```python
epochs = 5
batch_size = 1
lr = 1e-5
load = False
scale = 0.5
validation = val = 10.0
amp = False
bilinear = False
classes  = 2
```

Next Plan: setup wandb

tqdm printing showing multiline
Try (Still mulitline output)
Notebook: `/scratch/fp0/mah900/Pytorch-UNet/Notebooks/Train-01.ipynb`
```python
from tqdm import tqdm
#from tqdm.auto import tqdm

```
Try
File: `/scratch/fp0/mah900/Pytorch-UNet/evaluate.py`
```python
from tqdm import tqdm
#from tqdm.auto import tqdm
...
    #while len(tqdm._instances) > 0:
    #    tqdm._instances.pop().close()
    #tqdm._instances.clear()    
```
File: ` /scratch/fp0/mah900/Pytorch-UNet/utils/data_loading.py `
```python
from tqdm import tqdm
#from tqdm.auto import tqdm

...
        #tqdm._instances.clear()    
```
Source: https://stackoverflow.com/questions/41707229/why-is-tqdm-printing-to-a-newline-instead-of-updating-the-same-line
Now working
Error-
```
                                if w.is_alive():if w.is_alive():if w.is_alive():    if w.is_alive():if w.is_alive():if w.is_alive():if w.is_alive():if w.is_alive():if w.is_alive():
if w.is_alive():
```
Try
` tqdm._instances.clear() ` comment off.   (Not working)

Error
```bash
if w.is_alive():if w.is_alive():    if w.is_alive():    

if w.is_alive():if w.is_alive():
  File "/opt/conda/envs/mlenv/lib/python3.9/multiprocessing/process.py", line 160, in is_alive
  File "/opt/conda/envs/mlenv/lib/python3.9/multiprocessing/process.py", line 160, in is_alive

  File "/opt/conda/envs/mlenv/lib/python3.9/multiprocessing/process.py", line 160, in is_alive

          File "/opt/conda/envs/mlenv/lib/python3.9/multiprocessing/process.py", line 160, in is_alive
  File "/opt/conda/envs/mlenv/lib/python3.9/multiprocessing/process.py", line 160, in is_alive
    assert self._parent_pid == os.getpid(), 'can only test a child process'assert self._parent_pid == os.getpid(), 'can only test a child process'    assert self._parent_pid == os.getpid(), 'can only test a child process'    

assert self._parent_pid == os.getpid(), 'can only test a child process'assert self._parent_pid == os.getpid(), 'can only test a child process'AssertionError
AssertionError
AssertionError
: AssertionError: AssertionErrorcan only test a child process: : can only test a child processcan only test a child process
: 
can only test a child processcan only test a child process
```

# 2024.05.33

"I was able to fix by replacing"
```python
from tqdm.auto import tqdm  
#with just  
from tqdm import tqdm
```
Source: https://discuss.pytorch.org/t/error-while-multiprocessing-in-dataloader/46845/9

Okay, just remove `tqdm` from the evaluation
(The new lines are distracting)
File: `/scratch/fp0/mah900/Pytorch-UNet/evaluate.py`
```python
import logging
...

    logging.info('')
    logging.info('Validation round')
    # iterate over the validation set
    with torch.autocast(device.type if device.type != 'mps' else 'cpu', enabled=amp):
        #for batch in tqdm(dataloader, total=num_val_batches, desc='Validation round', unit='batch', leave=False):
        for batch in (dataloader):        
            image, mask_true = batch['image'], batch['mask']

```

Next, reduce `# Evaluation round` inside each epoch (in the notebook). 
```python
                # Evaluation round
                division_step = (n_train // (5 * batch_size))
                if division_step > 0:
                    if global_step % division_step == 0:
                        print ("n_train:", n_train, " batch_size:", batch_size, 
                               " global_step:", global_step , " division_step:", division_step)

...
n_train: 4580  batch_size: 1  global_step: 916  division_step: 916
...
n_train: 4580  batch_size: 1  global_step: 1832  division_step: 916
```

Try
```python
batch_size = 10 #1
...
                if division_step > 0:
                    #if global_step % division_step == 0:
                    if n_train  % global_step == 0:

```


Next, Try `wandb`
```python
%%wandb

# Your training loop here
```
Source: https://docs.wandb.ai/guides/track/jupyter
Error
```
Access Denied
```
Try
```
wandb.login()
```
Warning
```
Failed to detect the name of this notebook, you can set it manually with the WANDB_NOTEBOOK_NAME environment variable to enable code saving.
```
No solutions on the net. 


Try
```
wandb.login(key=my_secret)
```
Source: https://www.kaggle.com/code/samuelcortinhas/weights-biases-tutorial-beginner
```bash
Failed to detect the name of this notebook, you can set it manually with the WANDB_NOTEBOOK_NAME environment variable to enable code saving.
wandb: Currently logged in as: marufahmed (gadi). Use `wandb login --relogin` to force relogin
wandb: WARNING If you're specifying your api key in code, ensure this code is not shared publicly.
wandb: WARNING Consider setting the WANDB_API_KEY environment variable, or running `wandb login` from the command line.
wandb: Appending key for api.wandb.ai to your netrc file: /home/900/mah900/.netrc
```

Try,
```bash
cat /home/900/mah900/.netrc
machine api.wandb.ai
  login user
  password 2b6ea4e19e1f3527c59557cb8d69938d56e8bee0

ls -lth /home/900/mah900/.netrc
-rw------- 1 mah900 z00 86 May 22 18:10 /home/900/mah900/.netrc

 stat /home/900/mah900/.netrc
  File: /home/900/mah900/.netrc
  Size: 86        Blocks: 8          IO Block: 65536  regular file
Device: 38h/56d Inode: 9264261890754722130  Links: 1
Access: (0600/-rw-------)  Uid: (18042/  mah900)   Gid: ( 1090/     z00)
Access: 2022-09-19 16:50:38.933433000 +1000
Modify: 2024-05-22 18:10:01.226667000 +1000
Change: 2024-05-22 18:10:01.227660000 +1000
 Birth: -
```
Note:
So, the `/home/900/mah900/.netrc` file is changed/stored the key when the login function was called with the key
Also, now weights and biases page is working inside the JuputerLab. 

##### Also, Error
**OutOfMemoryError**
```bash
OutOfMemoryError: CUDA out of memory. Tried to allocate 1.46 GiB (GPU 0; 31.73 GiB total capacity; 30.05 GiB already allocated; 258.69 MiB free; 31.09 GiB reserved in total by PyTorch) If reserved memory is >> allocated memory try setting max_split_size_mb to avoid fragmentation.  See documentation for Memory Management and PYTORCH_CUDA_ALLOC_CONF
```
Try
```python
    torch.cuda.empty_cache()
    gc.collect()
    del variables  
    
    # 5. Begin training
```
Source: https://stackoverflow.com/questions/59129812/how-to-avoid-cuda-out-of-memory-in-pytorch
Try (Not working)
```python
                # Evaluation round
                #division_step = (n_train // (5 * batch_size))
                #if division_step > 0:
                    #if global_step % division_step == 0:
                with torch.no_grad(): 
                    # Wrong order
                    # if n_train  % global_step == 0:
                    if global_step % n_train == 0: #and global_step !=0:                      
                        #print ("n_train:", n_train, " batch_size:", batch_size, 
                        #       " global_step:", global_step )#, " division_step:", division_step)

```
Source: https://github.com/pytorch/pytorch/issues/16417#issuecomment-1432205249
Note:
	Not working
	Try reduce Batch size
	Batch size 5 with 29GB memory working

##### Next problem, the evolution code portion is not called
The `global_step` changed with batch size (As it is increased once for a batch)
tqdm total also has to be changed
Try (Seems working, but loss seems worse)
```python
        with tqdm(total=(n_train // batch_size), desc=f'Epoch {epoch}/{epochs}', unit='img') as pbar:
...
                if global_step % (n_train // batch_size) == 0: #and global_step !=0:  

```
It was designed for 1 image batches (???)
Go back to batch = 1 (???)


Next plan: go back to understanding_cloud notebooks and try to display the dataset with masks. 

