
https://github.com/milesial/Pytorch-UNet/tree/master

## About

PyTorch implementation of the U-Net for image semantic segmentation with high quality images

Customized implementation of the [U-Net](https://arxiv.org/abs/1505.04597) in PyTorch for Kaggle's [Carvana Image Masking Challenge](https://www.kaggle.com/c/carvana-image-masking-challenge) from high definition images.




