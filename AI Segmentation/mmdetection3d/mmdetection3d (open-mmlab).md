

https://github.com/open-mmlab/mmdetection3d


## About

OpenMMLab's next-generation platform for general 3D object detection.

[mmdetection3d.readthedocs.io/en/latest/](https://mmdetection3d.readthedocs.io/en/latest/ "https://mmdetection3d.readthedocs.io/en/latest/")
 