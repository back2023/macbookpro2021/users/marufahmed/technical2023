
https://ai.meta.com/datasets/segment-anything/

Segment Anything 1 Billion (SA-1B) is a dataset designed for training general-purpose object segmentation models from open world images. The dataset was introduced in our paper “Segment Anything”.

