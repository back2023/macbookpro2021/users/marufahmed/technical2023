

https://segment-anything.com/

## Segment Anything Model (SAM): a new AI model from Meta AI that can "cut out" any object, in any image, with a single click

###### SAM is a promptable segmentation system with zero-shot generalization to unfamiliar objects and images, without the need for additional training.


https://segment-anything.com/dataset/index.html


