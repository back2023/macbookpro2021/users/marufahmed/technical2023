
https://arxiv.org/pdf/2304.02643

[Alexander Kirillov](https://arxiv.org/search/cs?searchtype=author&query=Kirillov,+A), [Eric Mintun](https://arxiv.org/search/cs?searchtype=author&query=Mintun,+E), [Nikhila Ravi](https://arxiv.org/search/cs?searchtype=author&query=Ravi,+N), [Hanzi Mao](https://arxiv.org/search/cs?searchtype=author&query=Mao,+H), [Chloe Rolland](https://arxiv.org/search/cs?searchtype=author&query=Rolland,+C), [Laura Gustafson](https://arxiv.org/search/cs?searchtype=author&query=Gustafson,+L), [Tete Xiao](https://arxiv.org/search/cs?searchtype=author&query=Xiao,+T), [Spencer Whitehead](https://arxiv.org/search/cs?searchtype=author&query=Whitehead,+S), [Alexander C. Berg](https://arxiv.org/search/cs?searchtype=author&query=Berg,+A+C), [Wan-Yen Lo](https://arxiv.org/search/cs?searchtype=author&query=Lo,+W), [Piotr Dollár](https://arxiv.org/search/cs?searchtype=author&query=Doll%C3%A1r,+P), [Ross Girshick](https://arxiv.org/search/cs?searchtype=author&query=Girshick,+R)

> We introduce the Segment Anything (SA) project: a new task, model, and dataset for image segmentation. Using our efficient model in a data collection loop, we built the largest segmentation dataset to date (by far), with over 1 billion masks on 11M licensed and privacy respecting images. The model is designed and trained to be promptable, so it can transfer zero-shot to new image distributions and tasks. We evaluate its capabilities on numerous tasks and find that its zero-shot performance is impressive -- often competitive with or even superior to prior fully supervised results. We are releasing the Segment Anything Model (SAM) and corresponding dataset (SA-1B) of 1B masks and 11M images at [this https URL](https://segment-anything.com/) to foster research into foundation models for computer vision.





