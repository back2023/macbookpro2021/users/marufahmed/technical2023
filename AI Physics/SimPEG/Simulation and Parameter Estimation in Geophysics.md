
https://simpeg.xyz/

An open source python package for simulation and gradient based parameter estimation in geophysical applications.

## Geophysical Methods

Contribute to a growing community of geoscientists building an open foundation for geophysics. SimPEG provides a collection of geophysical simulation and inversion tools that are built in a consistent framework.

- Gravity
- Magnetics
- Direct current resistivity
- Induced polarization
- Electromagnetics

- Time domain
- Frequency domain
- Natural source (e.g. Magnetotellurics)
- Viscous remanent magnetization

- Richards Equation

