
A Physics-informed neural network (PINN) library.

[DeepINN](https://github.com/praksharma/DeepINN) is a deep-learning framework for solving forward and inverse problem involving PDEs using physics-informed neural networks (PINNs).

- The geometry module has been borrowed from [TorchPhysics](https://github.com/boschresearch/torchphysics).
- [TODO list](https://github.com/praksharma/DeepINN/blob/main/todo.md).

https://github.com/praksharma/DeepINN
