Torchdyn is a PyTorch library dedicated to **numerical deep learning**: differential equations, integral transforms, numerical methods. Maintained by [DiffEqML](https://github.com/DiffEqML).

https://github.com/DiffEqML/torchdyn