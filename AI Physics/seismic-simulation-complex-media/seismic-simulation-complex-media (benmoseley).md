

https://github.com/benmoseley/seismic-simulation-complex-media


## About

This repository reproduces the results of the paper "Deep learning for fast simulation of seismic waves in complex media" (B. Moseley et al, 2020, Solid Earth)


# Deep learning for fast simulation of seismic waves in complex media

[](https://github.com/benmoseley/seismic-simulation-complex-media#deep-learning-for-fast-simulation-of-seismic-waves-in-complex-media)

---

This repository reproduces the results of the paper _[Deep learning for fast simulation of seismic waves in complex media](https://se.copernicus.org/articles/11/1527/2020/), B. Moseley, T. Nissen-Meyer and A. Markham, 2020 Solid Earth_.


## Overview

[](https://github.com/benmoseley/seismic-simulation-complex-media#overview)

- **Seismic simulation** is crucial for many geophysical applications, yet traditional approaches are **computationally expensive**.
    
- We present two **deep neural networks** which are able to simulate seismic waves in complex media.
    
- The first network simulates seismic waves in **horizontally layered media** and uses a **WaveNet** design.
    
- The second network is significantly more general and simulates seismic waves in **faulted media** with arbitrary layers, fault properties and **an arbitrary location of the seismic source** on the surface of the media, using a **conditional autoencoder** design.
    
- Both are networks are **multiple orders of magnitude faster** than traditional simulation methods.
    
- We discuss the **challenges** of extending deep learning approaches to **more complex, elastic and 3-D Earth models** required for practical simulation tasks.




