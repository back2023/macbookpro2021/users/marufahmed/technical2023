
https://github.com/fatiando/harmonica

# About

_Harmonica_ is a Python library for processing and modeling gravity and magnetic data. It includes common processing steps, like calculation of Bouguer and terrain corrections, reduction to the pole, upward continuation, equivalent sources, and more. There are forward modeling functions for basic geometric shapes, like point sources, prisms and tesseroids. The inversion methods are implemented as classes with an interface inspired by scikit-learn (like [Verde](https://www.fatiando.org/verde)).



Things that will _not_ be covered in Harmonica:

- Multi-physics partial differential equation solvers. Use [SimPEG](http://www.simpeg.xyz/) or [PyGIMLi](https://www.pygimli.org/) instead.
- Generic grid processing methods (like horizontal derivatives and FFT). We'll rely on [Verde](https://www.fatiando.org/verde), [xrft](https://xrft.readthedocs.io/en/latest/) and [xarray](https://xarray.dev/) for those.
- Data visualization.
- GUI applications.