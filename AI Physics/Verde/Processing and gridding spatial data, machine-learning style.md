
https://www.fatiando.org/verde/latest/

**Verde** is a Python library for processing spatial data (bathymetry, geophysics surveys, etc) and interpolating it on regular grids (i.e., _gridding_).

Our core interpolation methods are inspired by machine-learning. As such, Verde implements an interface that is similar to the popular [scikit-learn](https://scikit-learn.org/) library. We also provide other analysis methods that are often used in combination with gridding, like trend removal, blocked/windowed operations, cross-validation, and more!


