

This repository contains three Jupyter notebooks illustrating different approaches to solve partial differential equations (PDEs) by means of neural networks (NNs).

The notebooks serve as supplementary material to the paper:

## [](https://github.com/janblechschmidt/PDEsByNNs#three-ways-to-solve-partial-differential-equations-with-neural-networks---a-review)Three Ways to Solve Partial Differential Equations with Neural Networks - A Review

**Abstract**: Neural networks are increasingly used to construct numerical solution methods for partial differential equations. In this expository review, we introduce and contrast three important recent approaches attractive in their simplicity and their suitability for high-dimensional problems: physics-informed neural networks, methods based on the Feynman-Kac formula and the Deep BSDE solver. The article is accompanied by a suite of expository software in the form of Jupyter notebooks in which each basic methodology is explained step by step, allowing for a quick assimilation and experimentation. An extensive bibliography summarizes the state of the art.

**Keywords**: partial differential equation; Hamilton-Jacobi-Bellman equations; neural networks, curse of dimensionality, Feynman-Kac, backward differential equation, stochastic process



https://github.com/janblechschmidt/PDEsByNNs


