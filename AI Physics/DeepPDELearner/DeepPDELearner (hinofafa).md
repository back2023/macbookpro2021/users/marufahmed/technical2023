https://github.com/hinofafa/DeepPDELearner

This repository introduces Partial Differential Equation Solver using neural network that can learn resolution-invariant solution operators on Navier-Stokes equation. Solving PDE is the core subject of numerical simulation and is widely used in science and engineering, from molecular dynamics to flight simulation, and even weather forecasting.