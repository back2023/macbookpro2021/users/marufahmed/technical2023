
In this notebook, we illustrate physics informed neural networks (PINNs) to solve partial differential equations (PDEs) as proposed in

- Maziar Raissi, Paris Perdikaris, George Em Karniadakis. _Physics Informed Deep Learning (Part I): Data-driven Solutions of Nonlinear Partial Differential Equations_. [arXiv 1711.10561](https://colab.research.google.com/corgiredirector?site=https%3A%2F%2Farxiv.org%2Fabs%2F1711.10561)
- Maziar Raissi, Paris Perdikaris, George Em Karniadakis. _Physics Informed Deep Learning (Part II): Data-driven Discovery of Nonlinear Partial Differential Equations_. [arXiv 1711.10566](https://colab.research.google.com/corgiredirector?site=https%3A%2F%2Farxiv.org%2Fabs%2F1711.10566)
- Maziar Raissi, Paris Perdikaris, George Em Karniadakis. _Physics-informed neural networks: A deep learning framework for solving forward and inverse problems involving nonlinear partial differential equations_. J. Comp. Phys. 378 pp. 686-707 [DOI: 10.1016/j.jcp.2018.10.045](https://colab.research.google.com/corgiredirector?site=https%3A%2F%2Fwww.sciencedirect.com%2Fscience%2Farticle%2Fpii%2FS0021999118307125)

This notebook is partially based on another implementation of the PINN approach published on [GitHub by pierremtb](https://github.com/pierremtb/PINNs-TF2.0) as well as the original code, see [Maziar Raissi on GitHub](https://github.com/maziarraissi/PINNs).


https://colab.research.google.com/github/janblechschmidt/PDEsByNNs/blob/main/PINN_Solver.ipynb



