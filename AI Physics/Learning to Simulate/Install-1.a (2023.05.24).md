
### Try 1 -Tensorflow install (worked)
Virtual env: https://docs.python.org/3/library/venv.html
Tensorflow 1.15: https://www.tensorflow.org/hub/installation
TensorFlow and CUDA compatible: https://stackoverflow.com/questions/50622525/which-tensorflow-and-cuda-version-combinations-are-compatible 
```bash
module load python3/3.7.4
python3 -m venv /scratch/fp0/mah900/env/learning_to_simulate
source /scratch/fp0/mah900/env/learning_to_simulate/bin/activate
pip install "tensorflow>=1.15,<2.0"
```

### Next, Try package installing (Worked)
Requirement file: 
No cache: https://www.reddit.com/r/learnmachinelearning/comments/r4pjqu/tensorflow_and_nocachedir_in_requirementstxt/
```bash
mkdir /scratch/fp0/mah900/learning_to_simulate
cd    /scratch/fp0/mah900/learning_to_simulate

source /scratch/fp0/mah900/env/learning_to_simulate/bin/activate
pip install -r requirements.txt --no-cache-dir 
```

### Next, clean and try GPU
clean: https://stackoverflow.com/questions/11005457/how-do-i-remove-delete-a-virtualenv
```bash
# Clean
deactivate
rm -r /scratch/fp0/mah900/env/learning_to_simulate
```

```bash
module load python3/3.7.4
module load cuda/10.1 cudnn/7.6.5-cuda10.1
python3 -m venv /scratch/fp0/mah900/env/learning_to_simulate
source /scratch/fp0/mah900/env/learning_to_simulate/bin/activate
pip install -r requirements.txt --no-cache-dir 

```
###### Error-1
```bash
tensorboard 1.15.0 has requirement setuptools>=41.0.0, but you'll have setuptools 40.8.0 which is incompatible.

tensorflow 1.15.5 has requirement numpy<1.19.0,>=1.16.0, but you'll have numpy 1.21.6 which is incompatible.
```
Source 1: https://github.com/tensorflow/models/issues/9200
Source 2: https://discuss.tensorflow.org/t/tensorflow-1-15-installation-numpy-dependency/13820
Try, updating the `requirement.txt` file 
```bash
absl-py
graph-nets>=1.1
tensorflow>=1.15,<2
numpy<1.19.0,>=1.16.0 # numpy
dm-sonnet<2
tensorflow_probability<0.9
scikit-learn  #sklearn
dm-tree
matplotlib
setuptools>=41.0.0  # Added
```

###### Error-2
```bash
The 'sklearn' PyPI package is deprecated, use 'scikit-learn'
    rather than 'sklearn' for pip commands.

    Here is how to fix this error in the main use cases:
    - use 'pip install scikit-learn' rather than 'pip install sklearn'
    - replace 'sklearn' by 'scikit-learn' in your pip requirements files (requirements.txt, setup.py, setup.cfg, Pipfile, etc ...)
    - if the 'sklearn' package is used by one of your dependencies,

      it would be great if you take some time to track which package uses

      'sklearn' instead of 'scikit-learn' and report it to their issue tracker
    - as a last resort, set the environment variable
      SKLEARN_ALLOW_DEPRECATED_SKLEARN_PACKAGE_INSTALL=True to avoid this error
```

### Next, clean and try again (No Error)
```bash
deactivate
rm -r /scratch/fp0/mah900/env/learning_to_simulate

module load python3/3.7.4
module load cuda/10.1 cudnn/7.6.5-cuda10.1
python3 -m venv /scratch/fp0/mah900/env/learning_to_simulate
source /scratch/fp0/mah900/env/learning_to_simulate/bin/activate
# Following line did not work
#SKLEARN_ALLOW_DEPRECATED_SKLEARN_PACKAGE_INSTALL=True
pip install -r requirements.txt --no-cache-dir 
```

### Test
```bash
qsub -I g1_h1.pbs

module load python3/3.7.4
module load cuda/10.1 cudnn/7.6.5-cuda10.1
source /scratch/fp0/mah900/env/learning_to_simulate/bin/activate

python
import tensorflow as tf1
tf1.test.is_gpu_available()
tf1.test.is_gpu_available(cuda_only=True)
```
Error: GPU not found. 
Try
```bash
pip uninstall tensorflow
pip uninstall tensorflow-gpu
pip install tensorflow-gpu==1.15 --no-cache-dir 
```

Can not find GPU, 
Pip does work