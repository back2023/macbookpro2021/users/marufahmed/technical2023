
https://docs.fenicsproject.org/dolfinx/main/python/demos.html


These demos illustrate DOLFINx usage. Starting with [Poisson equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_poisson.html) is recommended.

## PDEs (introductory)[](https://docs.fenicsproject.org/dolfinx/main/python/demos.html#pdes-introductory "Link to this heading")

- [Poisson equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_poisson.html)
- [Helmholtz equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_helmholtz.html)
- [Electromagnetic scattering from a wire with scattering boundary conditions](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_scattering_boundary_conditions.html)
- [Electromagnetic scattering from a wire with perfectly matched layer condition](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_pml.html)
- [Electromagnetic modal analysis for a waveguide](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_half_loaded_waveguide.html)
- [Electromagnetic scattering from a sphere (axisymmetric)](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_axis.html)

## PDEs (advanced)[](https://docs.fenicsproject.org/dolfinx/main/python/demos.html#pdes-advanced "Link to this heading")

- [Mixed formulation for the Poisson equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_mixed-poisson.html)
- [Stokes equations using Taylor-Hood elements](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_stokes.html)
- [Divergence conforming discontinuous Galerkin method for the Navier–Stokes equations](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_navier-stokes.html)
- [Elasticity using algebraic multigrid](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_elasticity.html)
- [Cahn-Hilliard equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_cahn-hilliard.html)
- [Static condensation of linear elasticity](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_static-condensation.html)
- [Biharmonic equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_biharmonic.html)
- [Solving PDEs with different scalar (float) types](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_types.html)

## Nonlinear problems[](https://docs.fenicsproject.org/dolfinx/main/python/demos.html#nonlinear-problems "Link to this heading")

- [Cahn-Hilliard equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_cahn-hilliard.html)

## Mesh generation[](https://docs.fenicsproject.org/dolfinx/main/python/demos.html#mesh-generation "Link to this heading")

- [Mesh generation with Gmsh](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_gmsh.html)

## Interpolation, IO and visualisation[](https://docs.fenicsproject.org/dolfinx/main/python/demos.html#interpolation-io-and-visualisation "Link to this heading")

- [Visualization with PyVista](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_pyvista.html)
- [Interpolation and IO](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_interpolation-io.html)

## Advanced iterative solvers[](https://docs.fenicsproject.org/dolfinx/main/python/demos.html#advanced-iterative-solvers "Link to this heading")

- [Stokes equations using Taylor-Hood elements](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_stokes.html)
- [Elasticity using algebraic multigrid](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_elasticity.html)

## User-defined and advanced finite elements[](https://docs.fenicsproject.org/dolfinx/main/python/demos.html#user-defined-and-advanced-finite-elements "Link to this heading")

- [Variants of Lagrange elements](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_lagrange_variants.html)
- [Creating TNT elements using Basix’s custom element interface](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_tnt-elements.html)

## List of all demos[](https://docs.fenicsproject.org/dolfinx/main/python/demos.html#list-of-all-demos "Link to this heading")

- [Poisson equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_poisson.html)
- [Biharmonic equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_biharmonic.html)
- [Cahn-Hilliard equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_cahn-hilliard.html)
- [Stokes equations using Taylor-Hood elements](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_stokes.html)
- [Elasticity using algebraic multigrid](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_elasticity.html)
- [Mesh generation with Gmsh](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_gmsh.html)
- [Helmholtz equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_helmholtz.html)
- [Static condensation of linear elasticity](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_static-condensation.html)
- [Visualization with PyVista](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_pyvista.html)
- [Interpolation and IO](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_interpolation-io.html)
- [Solving PDEs with different scalar (float) types](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_types.html)
- [Variants of Lagrange elements](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_lagrange_variants.html)
- [Creating TNT elements using Basix’s custom element interface](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_tnt-elements.html)
- [Electromagnetic scattering from a wire with scattering boundary conditions](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_scattering_boundary_conditions.html)
- [Electromagnetic scattering from a wire with perfectly matched layer condition](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_pml.html)
- [Electromagnetic modal analysis for a waveguide](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_half_loaded_waveguide.html)
- [Electromagnetic scattering from a sphere (axisymmetric)](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_axis.html)
- [Divergence conforming discontinuous Galerkin method for the Navier–Stokes equations](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_navier-stokes.html)
- [Mixed formulation for the Poisson equation](https://docs.fenicsproject.org/dolfinx/main/python/demos/demo_mixed-poisson.html)