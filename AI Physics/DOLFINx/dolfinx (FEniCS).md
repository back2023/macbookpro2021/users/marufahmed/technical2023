
https://github.com/FEniCS/dolfinx

## About

Next generation FEniCS problem solving environment


DOLFINx is the computational environment of [FEniCSx](https://fenicsproject.org/) and implements the FEniCS Problem Solving Environment in C++ and Python.

DOLFINx is a new version of DOLFIN and is being actively developed.

## [](https://github.com/FEniCS/dolfinx#documentation)