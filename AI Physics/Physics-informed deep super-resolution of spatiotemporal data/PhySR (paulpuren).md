
https://github.com/paulpuren/PhySR


# PhySR

Physics-informed deep super-resolution of spatiotemporal data

This paper has been accepted by the _Journal of Computational Physics_. Please see the official publication here ([Link](https://www.sciencedirect.com/science/article/pii/S0021999123005338?casa_token=VkF7g2e9Q8sAAAAA:uJ04SY6xXHeAq7sTfQ4my4kLUankf9SvvMj7MQzA8Ou87wnXXhieuUCTtZRWPvf8Z7BUeukX6Aci)) and the preprint version here ([Link](https://arxiv.org/abs/2208.01462)).

## [](https://github.com/paulpuren/PhySR#overview)Overview

High-fidelity simulation of complex physical systems is exorbitantly expensive and inaccessible across spatiotemporal scales. Therefore, we propose a new approach to augment scientific data with high resolution based on the coarse-grained data by leveraging deep learning. It is an efficient spatiotemporal super-resolution (ST-SR) framework via physics-informed learning. Inspired by the independence between temporal and spatial derivatives in partial differential equations (PDEs), this method decomposes the holistic ST-SR into temporal upsampling and spatial reconstruction regularized by available physics. Moreover, we consider hard encoding of boundary conditions into the network to improve solution accuracy. Results demonstrate the superior effectiveness and generalizability of the proposed method compared with baseline algorithms through numerical experiments. This repo is for our `PhySR` model, including the source code and the numerical datasets we have tested. The experimental test may be added in the future.
 
