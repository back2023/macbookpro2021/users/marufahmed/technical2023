
https://github.com/astanziola/fourier-neural-operator-flax

This is an **unofficial** [Flax](https://github.com/astanziola/fourier-neural-operator-flax/blob/main) ([JAX](https://github.com/astanziola/fourier-neural-operator-flax/blob/main)) port of the [Fourier Neural Operator](https://arxiv.org/abs/2010.08895) developed by Zongyi Li et al.

Please visit [this repository](https://github.com/zongyi-li/fourier_neural_operator) to access the official PyTorch implementation of FNO form the paper's authors, and for the citation policy in case you use FNO in your research.







