

This is an archive of explorations, in the convex hull of Inference, Probability, Optimisation and Physics. I am putting this together to keep track of the things I study.

# About[¶](https://random-walks.org/content/home.html#about "Permalink to this headline")

My name is Stratis. I am a second-year PhD student at the Computational and Biological Learning Lab (CBL) at the University of Cambridge. I am supervised by [Carl Rasmussen](http://mlg.eng.cam.ac.uk/carl/) and advised by [Richard Turner](http://cbl.eng.cam.ac.uk/Public/Turner/WebHome). I have broad interests in probabilistic machine learning, see [Google Scholar](https://scholar.google.com/citations?user=1tm1wcgAAAAJ&hl=el&oi=ao) for my recent work.


https://random-walks.org/content/home.html


