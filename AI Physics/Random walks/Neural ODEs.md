
This page discusses _Neural Ordinary Differential Equations_.[[CRBD18]](https://random-walks.org/content/misc/node/node.html#chen2018neural) The contribution of this paper is really on the ODE side, and in particular on taking derivatives through ODEs, rather than on the neural side. They present the self adjoint sensitivity method for taking derivatives through ODEs. This method was known before this work (see [David Duvenaud’s retrospective talk](https://slideslive.com/38923497/bullshit-that-i-and-others-have-said-about-neural-odes)), so in some sense, the authors’ real contribution was [porting the self adjoint method to pytorch](https://github.com/rtqichen/torchdiffeq) and using it in the context of ODEs and Normalising Flows.


https://random-walks.org/content/misc/node/node.html




