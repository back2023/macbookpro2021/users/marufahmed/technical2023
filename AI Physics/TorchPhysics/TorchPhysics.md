TorchPhysics is a Python library of (mesh-free) deep learning methods to solve differential equations. You can use TorchPhysics e.g. to

- solve ordinary and partial differential equations
- train a neural network to approximate solutions for different parameters
- solve inverse problems and interpolate external data

The following approaches are implemented using high-level concepts to make their usage as easy as possible:

- physics-informed neural networks (PINN) [[1]](https://github.com/boschresearch/torchphysics#id8)
- QRes [[2]](https://github.com/boschresearch/torchphysics#id9)
- the Deep Ritz method [[3]](https://github.com/boschresearch/torchphysics#id10)
- DeepONets [[4]](https://github.com/boschresearch/torchphysics#id11) and Physics-Informed DeepONets [[5]](https://github.com/boschresearch/torchphysics#id12)

We aim to also include further implementations in the future.

TorchPhysics is build upon the machine learning library [PyTorch](https://pytorch.org/).

https://github.com/boschresearch/torchphysics