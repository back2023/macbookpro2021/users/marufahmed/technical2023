
We have been given a PDE: du/dx=2du/dt+u and boundary condition: u(x,0)=6e^(-3x)

- Independent variables: x,t (input)
- Dependent variables: u (outputs)

We have to find out u(x,t) for all x in range [0,2] and t in range [0,1]

When we solved this problem analytically, we found the solution: u(x,t) = 6e^(-3x-2t)

Our f is f = du/dx - 2du/dt - u


https://github.com/nanditadoloi/PINN/blob/main/solve_PDE_NN.ipynb