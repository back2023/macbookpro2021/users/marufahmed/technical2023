

https://github.com/cognitivemodeling/finn


# FInite volume Neural Network (FINN)

[](https://github.com/cognitivemodeling/finn#finite-volume-neural-network-finn)

This repository contains the PyTorch code for models, training, and testing, and Python code for data generation to conduct the experiments as reported in the work [Composing Partial Differential Equations with Physics-Aware Neural Networks](https://arxiv.org/abs/2111.11798)


