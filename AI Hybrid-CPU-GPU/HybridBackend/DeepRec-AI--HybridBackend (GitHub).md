
https://github.com/DeepRec-AI/HybridBackend


# HybridBackend

[![cibuild](https://github.com/alibaba/HybridBackend/actions/workflows/cibuild.yaml/badge.svg?branch=main&event=push)](https://github.com/alibaba/HybridBackend/actions/workflows/cibuild.yaml) [![readthedocs](https://camo.githubusercontent.com/8cd2d8fbdd4d499be304e9e8a92d39769c2ef3e9a5ce0c77dad8c69682e2f283/68747470733a2f2f72656164746865646f63732e6f72672f70726f6a656374732f6879627269646261636b656e642f62616467652f3f76657273696f6e3d6c6174657374)](https://hybridbackend.readthedocs.io/en/latest/?badge=latest) [![PRs Welcome](https://camo.githubusercontent.com/b0ad703a46e8b249ef2a969ab95b2cb361a2866ecb8fe18495a2229f5847102d/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f5052732d77656c636f6d652d627269676874677265656e2e737667)](http://makeapullrequest.com/) [![license](https://camo.githubusercontent.com/a0336f108dbbb0b60eb442f4778035f553725d027a67dfa91f9bd719b5007afb/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4c6963656e73652d417061636865253230322e302d627269676874677265656e2e737667)](https://opensource.org/licenses/Apache-2.0)

HybridBackend is a high-performance framework for training wide-and-deep recommender systems on heterogeneous cluster.

## [](https://github.com/DeepRec-AI/HybridBackend#features)Features

- Memory-efficient loading of categorical data
- GPU-efficient orchestration of embedding layers
- Communication-efficient training and evaluation at scale
- Easy to use with existing AI workflows





