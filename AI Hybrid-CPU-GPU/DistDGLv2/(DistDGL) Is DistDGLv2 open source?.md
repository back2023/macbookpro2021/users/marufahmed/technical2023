
https://discuss.dgl.ai/t/distdgl-is-distdglv2-open-source/3001


Is DistDGLv2 ([https://arxiv.org/abs/2112.15345 48](https://arxiv.org/abs/2112.15345)) being developed as part of the DGL project on Github, or is it currently closed source?


hello dstoll, thanks for asking. currently, distdglv2 is in a private repo, but we would like to merge it to the master branch. Actually, part of distdglv2 has been merged to DGL’s master branch. We will define the timeline to merge the asynchronous training pipeline.


