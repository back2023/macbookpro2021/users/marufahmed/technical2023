
https://www.amazon.science/blog/scaling-graph-neural-network-training-with-cpu-gpu-clusters

August 17, 2022

Graphs are a useful way to represent data, since they capture connections between data items, and [graph neural networks](https://www.amazon.science/tag/graph-neural-networks) (GNNs) are an increasingly popular way to work on graphs. Common industry applications of GNNs include recommendation, search, and fraud detection.

The graphs used in industry applications are usually massive, with billions of nodes and hundreds of billions or even trillions of edges. Training GNNs on graphs of this scale requires massive memory storage and computational power, with correspondingly long training times and large energy footprints.

In a paper we’re presenting at this year’s [KDD](https://www.amazon.science/conferences-and-events/kdd-2022), my colleagues and I describe a new approach to [distributed training of GNNs](https://www.amazon.science/publications/distributed-hybrid-cpu-and-gpu-training-for-graph-neural-networks-on-billion-scale-heterogeneous-graphs) that uses both CPUs and GPUs, optimizing the allocation of tasks to different processor types to minimize training times.

In tests, our approach — DistDGLv2 — offered an 18-fold speedup over Euler, another distributed GNN training framework, on the same hardware. DistDGLv2 also achieves a speedup of up to 15-fold over distributed CPU training in a cluster of the same size.



