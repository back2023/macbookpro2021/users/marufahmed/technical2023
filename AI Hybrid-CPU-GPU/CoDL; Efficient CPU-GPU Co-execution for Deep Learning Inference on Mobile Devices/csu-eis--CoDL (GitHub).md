
https://github.com/csu-eis/CoDL


# CoDL

## [](https://github.com/csu-eis/CoDL#introduction)Introduction

This is the official implementation of **CoDL: Efficient CPU-GPU Co-execution for Deep Learning Inference on Mobile Devices** in The 20th Annual International Conference on Mobile Systems, Applications and Services, which is a novel framework for co-execution of deep learning models on mobile devices.

CoDL can fully utilize the heterogeneous processors to accelerate each operator of a model, which makes it different from available inference frameworks. CoDL integrates two novel techniques: 1) **hybrid-type-friendly data sharing**, which allows each processor to use its efficient data type for inference. To reduce data sharing overhead, we also propose hybrid-dimension partitioning and operator chain methods; 2) **non-linearity- and concurrency-aware latency prediction**, which can direct proper operator partitioning by building an extremely light-weight but accurate latency predictor for different processors.

We evaluate CoDL on a variety of smartphones and deep learning models. The inference speed of CoDL achieves 680ms on RetinaFace, 140ms on YOLOv2, 137ms on VGG-16, 244ms on PoseNet, and 267ms on Fast Style Transfer in our Snapdragon 855 platform (Xiaomi 9).

Below is the list of all the models and their performance on CoDL.





