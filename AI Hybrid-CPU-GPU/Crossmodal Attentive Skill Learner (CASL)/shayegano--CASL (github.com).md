
https://github.com/shayegano/CASL


# Crossmodal Attentive Skill Learner (CASL)

Hybrid CPU-GPU implementation of Crossmodal Attentive Skill Learner (CASL)  
Codebase design is based on [GA3C](https://github.com/NVlabs/GA3C/).


#### Paper:

S. Omidshafiei, D. K. Kim, J. Pazis, and J. P. How, "Crossmodal Attentive Skill Learner", In NIPS Deep Reinforcement Learning Symposium, 2017.  
Link: [https://arxiv.org/abs/1711.10314](https://arxiv.org/abs/1711.10314)







