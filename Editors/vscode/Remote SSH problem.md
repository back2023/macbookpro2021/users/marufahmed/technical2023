1. Close all open files in the path
   ```bash
   lsof +D /path
   kill # all the ids found 
   ```
https://unix.stackexchange.com/a/11241

2.  Remove/clean remote .vscode file
   ```bash
   rm -r .vscode-server
   ```
https://stackoverflow.com/a/66809539


### 15 Linux lsof Command Examples (Identify Open Files) 
https://www.thegeekstuff.com/2012/08/lsof-command-examples/