
https://crc-docs.abudhabi.nyu.edu/hpc/software/vscode.html

The below steps also achieve the following:

- This will connect to the compute nodes and not the login nodes and hence prevent any overloads on login nodes.
    
- Eliminate the possibility of the non terminated vs code-related background processes which may cause login issues on the HPC


