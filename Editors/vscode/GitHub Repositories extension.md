
https://code.visualstudio.com/docs/sourcecontrol/github#_github-repositories-extension

The [GitHub Repositories](https://marketplace.visualstudio.com/items?itemName=github.remotehub) extension lets you quickly browse, search, edit, and commit to any remote GitHub repository directly from within Visual Studio Code, without needing to clone the repository locally.

GitHub Repositories
https://marketplace.visualstudio.com/items?itemName=github.remotehub







