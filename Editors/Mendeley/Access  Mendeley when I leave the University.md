
Q. Will I lose access to my Mendeley account and references when I leave the University?

No, 
your Mendeley account is your own personal account and you won’t lose access.  
If you registered using your University email address you can change this by signing in to **[Mendeley](https://www.mendeley.com/)** and editing the email details in the **[Account](https://www.mendeley.com/settings/account/)** section before you graduate.
https://libraryhelp.shef.ac.uk/faq/186764
