#### 1. Try

```
cd existing_repo
git remote add origin https://gitlab.com/back2023/macbookpro2021/users/marufahmed/technical2023.git
git branch -M main
git push -uf origin main
```

##### Error
```
remote: HTTP Basic: Access denied. The provided password or token is incorrect or your account has 2FA enabled and you must use a personal access token instead of a password.
```
##### Solution
##### 1. Create access token
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token
Mac_2021
```
glpat-KhebKBwB1siRKRFyGKAs
glpat-KhebKBwB1siRKRFyGKAs
```

##### 2. Direct push
```bash
git push -uf https://gitlab-ci-token:glpat-KhebKBwB1siRKRFyGKAs@gitlab.com/back2023/macbookpro2021/users/marufahmed/technical2023.git main
 ```
https://stackoverflow.com/a/57624220

#### 3. Add remote path

```
git remote remove origin

git remote add origin https://Mac_2021:glpat-KhebKBwB1siRKRFyGKAs@gitlab.com/back2023/macbookpro2021/users/marufahmed/technical2023.git
```
https://stackoverflow.com/a/52074198

##### Error 1
```bash
remote: GitLab: You are not allowed to force push code to a protected branch on this project.
```

Solution:
[Configure a protected branch](https://docs.gitlab.com/ee/user/project/protected_branches.html#configure-a-protected-branch "Permalink")
 