
Obsidian stores your notes as [Markdown-formatted](https://help.obsidian.md/Editing+and+formatting/Basic+formatting+syntax) plain text files in a _vault_. A vault is a folder on your local file system, including any subfolders.

Because notes are plain text files, you can use other text editors and file managers to edit and manage notes. Obsidian automatically refreshes your vault to keep up with any external changes.

You can create a vault anywhere your operating system allows. Obsidian syncs with Dropbox, iCloud, OneDrive, Git, and many other third-party services.

You can open multiple folders as individual vaults, for example to separate notes for work and personal use.


https://help.obsidian.md/Files+and+folders/How+Obsidian+stores+data