### 2023.06.26
Create repo
https://gitlab.com/back2023/gadi/home/900/mah900/scratch_recover

```bash
cd existing_repo
git remote add origin https://gitlab.com/back2023/gadi/home/900/mah900/scratch_recover.git
git branch -M main
git push -uf origin main
```

Update remote url and first push. 
```bash
$ pwd
/home/900/mah900/scratch_recover
git remote show origin
* remote origin
  Fetch URL: https://gitlab.com/mah900-home/scratch_recover.git
  Push  URL: https://gitlab.com/mah900-home/scratch_recover.git
...  
git remote remove origin
git remote add origin https://gitlab.com/back2023/gadi/home/900/mah900/scratch_recover.git
git branch -M main
git push -uf origin main
# Error-1
git remote remove origin
git remote add origin git@gitlab.com:back2023/gadi/home/900/mah900/scratch_recover.git 
git push -uf origin main

```
Source: https://stackoverflow.com/questions/4089430/how-to-determine-the-url-that-a-local-git-repository-was-originally-cloned-from
Source: https://stackoverflow.com/questions/16330404/how-to-remove-remote-origin-from-a-git-repository

Error-1
```bash
remote: You are not allowed to push code to this project.

fatal: unable to access 'https://gitlab.com/back2023/gadi/home/900/mah900/scratch_recover.git/': The requested URL returned error: 403
```
Solution: (Working)
```bash
# Change url from https to ssh
# url = https://gitlab.com/back2023/gadi/home/900/mah900/scratch_recover.git
# to
url = git@gitlab.com:back2023/gadi/home/900/mah900/scratch_recover.git
```
Source: https://stackoverflow.com/questions/7438313/pushing-to-git-returning-error-code-403-fatal-http-request-failed

