
https://www.ldsscriptureteachings.org/2018/05/deuteronomy-328-9-and-the-sons-of-god/


There is quite a bit going on in Deuteronomy 32, perhaps more than meets the eye when it is first read. First of all, it is worth noting that this text has a few variations and for good reason. Here are three versions of this text:

Dead Sea Scrolls: “When Elyon gave the nations as an inheritance, when he separated the sons of man, he set the boundaries of the peoples according to the number of **the sons of God** (bene elohim). For Yahweh’s portion was his people; Jacob was the lot of his inheritance”.

Septuagint (LXX): “When the Most High divided the nations, when he separated the sons of Adam, he set the boundaries of the nations according to the number of **the angels of God** (aggelón theou). And his people Jacob became the portion of the Lord, Israel was the line of his inheritance”.

Masoretic Text (MT): “When Elyon gave the nations their inheritance, when he divided all the sons of man, he set the boundaries of the peoples according to the number of **the sons of Israel** (bene yisrael). For Yahweh’s portion was his people, Jacob was the lot of his inheritance”.


