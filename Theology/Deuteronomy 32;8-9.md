
https://triangulations.wordpress.com/2010/05/09/deuteronomy-328-9/


This is part of my expanding [Bible Manipulations series](https://triangulations.wordpress.com/2010/05/09/2010/05/29/bible-manipulations/).

- **Texts**:  Hebrew –> English  and  Hebrew (ancient- Dead Sea Scrolls) –> Hebrew (Masoretic)
- **Manipulation**: translating dissimilar terms to the same meaning.
- **Purpose**: to cover up polytheism of the Old Testament Jews.
- **Background**: Reading only English translations, one can never see the Polytheism of the Old Testament.  The ancient Canaanites (of which the Israelites were originally one tribe) were polytheistic.  Their gods included El, Bal, Yahweh and more. (for details, see “The Evolution of God” by Robert Wright – “Who was Yahweh Before He Was Yahweh?” p110).  To avoid this, it is often best to transliterate instead of translate, but the average reader has little patience for such readings.




