

https://github.com/diegovalsesia/piunet


## About

Permutation invariance and uncertainty in multitemporal image super-resolution


# Permutation invariance and uncertainty in multitemporal image super-resolution

[](https://github.com/diegovalsesia/piunet#permutation-invariance-and-uncertainty-in-multitemporal-image-super-resolution)

PIUnet is a novel Multi Image Super-Resolution (MISR) deep neural network that exploits both spatial and temporal correlations to recover a single high resolution image from multiple unregistered low resolution images. It is fully invariant to the ordering of the input images and produces an estimate of the aleatoric uncertainty together with the super-resolved image.

This repository contains the pytorch implementation of PIUnet, trained and tested on the PROBA-V dataset provided by ESA’s [Advanced Concepts Team](http://www.esa.int/gsp/ACT/index.html) in the context of the [European Space Agency's Kelvin competition](https://kelvins.esa.int/proba-v-super-resolution/home/).


