
https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md


# Dataset Preparation


[English](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md) **|** [简体中文](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation_CN.md)

📁 Dataset Download: ⏬ [Google Drive](https://drive.google.com/drive/folders/1gt5eT293esqY0yr1Anbm36EdnxWW_5oH?usp=sharing) ⏬ [百度网盘](https://pan.baidu.com/s/1AZDcEAFwwc1OC3KCd7EDnQ) (提取码:basr)

#### [](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#contents)Contents

1. [Data Storage Format](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#Data-Storage-Format)
    1. [How to Use](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#How-to-Use)
    2. [How to Implement](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#How-to-Implement)
    3. [LMDB Description](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#LMDB-Description)
    4. [Data Pre-fetcher](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#Data-Pre-fetcher)
2. [Image Super-Resolution](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#Image-Super-Resolution)
    1. [DIV2K](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#DIV2K)
    2. [Common Image SR Datasets](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#Common-Image-SR-Datasets)
3. [Video Super-Resolution](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#Video-Super-Resolution)
    1. [REDS](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#REDS)
    2. [Vimeo90K](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#Vimeo90K)
4. [StylgeGAN2](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#StyleGAN2)
    1. [FFHQ](https://github.com/XPixelGroup/BasicSR/blob/master/docs/DatasetPreparation.md#FFHQ)

