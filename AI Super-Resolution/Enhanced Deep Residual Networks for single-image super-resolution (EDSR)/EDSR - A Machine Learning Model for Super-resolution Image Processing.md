

This is an introduction to「EDSR」, a machine learning model that can be used with [ailia SDK](https://ailia.jp/en/). You can easily use this model to create AI applications using [ailia SDK](https://ailia.jp/en/) as well as many other ready-to-use [ailia MODELS](https://github.com/axinc-ai/ailia-models).


_EDSR (Enhanced Deep Residual Networks for Single Image Super-Resolution)_ is a machine learning model released in July 2017 which can be used to increase the resolution of an image.


https://medium.com/axinc-ai/edsr-a-machine-learning-model-for-super-resolution-image-processing-9deaf36b24ed


