
**About PyTorch 1.2.0**

- Now the master branch supports PyTorch 1.2.0 by default.
- Due to the serious version problem (especially torch.utils.data.dataloader), MDSR functions are temporarily disabled. If you have to train/evaluate the MDSR model, please use legacy branches.


# EDSR-PyTorch

**About PyTorch 1.1.0**

- There have been minor changes with the 1.1.0 update. Now we support PyTorch 1.1.0 by default, and please use the legacy branch if you prefer older version.


https://github.com/sanghyun-son/EDSR-PyTorch




