
https://github.com/polimi-ispl/sr-bvoc


🌐 🌍 🌱 Super-Resolution of BVOC Emissions 🌱 🌍 🌐
# er-Resolution of BVOC Emissions 🌱 🌍 🌐

This repository is meant to represent a comprehensive collection of all the research works conducted by the Image and Sound Processing Lab ([ISPL](http://ispl.deib.polimi.it/)) research group at [Politecnico di Milano](https://www.polimi.it/en), and focused on the super-resolving Biogenic Volatile Organic Compound ([BVOC](https://www.cnr.it/en/focus/046-4/bvoc-biogenic-volatile-organic-compound-emission-responses-to-climate-change)) emission maps using novel Deep Learning (DL) techniques.

BVOC emissions play a crucial role in understanding the interactions between vegetation and the atmosphere, particularly in the context of climate change and air quality assessment. The application of deep learning methods for super-resolving BVOC emission maps has proven to be a promising approach to enhance the spatial resolution and improve the accuracy of these maps.

The works available in this repository are listed below:

1. 🔗 [Super-Resolution of BVOC Maps by Adapting Deep Learning Methods](https://github.com/polimi-ispl/sr-bvoc#super-resolution-of-bvoc-maps-by-adapting-deep-learning-methods), (2023) - [IEEE-ICIP 2023](https://2023.ieeeicip.org/), link [IEEExplore](https://github.com/polimi-ispl/sr-bvoc/blob/main),

[![arXiv](https://camo.githubusercontent.com/162a59ab3e3ecc11102e77d44002b9f1657b874f9e294152aa39c33eb03d48dc/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f61725869762d323330322e30373537302d6233316231622e706e67)](https://arxiv.org/abs/2302.07570)

2. 🔗 [Multi-BVOC Super-Resolution Exploiting Compounds Inter-Connection](https://github.com/polimi-ispl/sr-bvoc#multi-bvoc-super-resolution-exploiting-compounds-inter-connection), (2023) - [EURASIP-EUSIPCO 2023](https://eusipco2023.org/), link [IEEExplore](https://github.com/polimi-ispl/sr-bvoc/blob/main)

[![arXiv](https://camo.githubusercontent.com/f1feb6fa8c7145069475895a3956e1c70fdd0927751c68e8531d253f5330cf73/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f61725869762d323330352e31343138302d6233316231622e706e67)](https://arxiv.org/abs/2305.14180)

3. 🔗 [Super-Resolution of BVOC Emission Maps Via Domain Adaptation](https://github.com/polimi-ispl/sr-bvoc#super-resolution-of-bvoc-emission-maps-via-domain-adaptation), (2023) - [IEEE-IGARSS 2023](https://2023.ieeeigarss.org/), link [IEEExplore](https://github.com/polimi-ispl/sr-bvoc/blob/main)

[![arXiv](https://camo.githubusercontent.com/1b92100fdec7d0aef41ee3264b63ae0e0d4e94cda7abb32b124d44bcbdfd3c09/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f61725869762d323330362e31323739362d6233316231622e706e67)](https://arxiv.org/abs/2306.12796)

https://github.com/polimi-ispl/sr-bvoc











