
https://debuggercafe.com/image-super-resolution-using-deep-learning-and-pytorch/


In this tutorial, you will learn how to get high-resolution images from low-resolution images using **_deep learning and the PyTorch framework_**. This post will show you how to carry out image super-resolution using deep learning and PyTorch.

In one of my previous articles, I discussed **[Image Deblurring using Convolutional Neural Networks and Deep Learning](https://debuggercafe.com/image-deblurring-using-convolutional-neural-networks-and-deep-learning/)**. We had practical experience of using deep learning and the SRCNN (Super-Resolution Convolutional Neural Network) architecture to deblur the Gaussian blurred images.

This post will take that concept a bit further. We will try to replicate the original implementation of the **[Image Super-Resolution Using Deep Convolutional Networks](https://arxiv.org/pdf/1501.00092v3.pdf)** paper. We will go over the details in the next section.



# Image Super-Resolution Using Deep Convolutional Networks
https://mmlab.ie.cuhk.edu.hk/projects/SRCNN.html
