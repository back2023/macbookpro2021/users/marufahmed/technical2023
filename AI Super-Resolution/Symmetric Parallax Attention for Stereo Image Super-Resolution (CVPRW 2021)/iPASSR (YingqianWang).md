
https://github.com/YingqianWang/iPASSR

### PyTorch implementation of "Symmetric Parallax Attention for Stereo Image Super-Resolution", [CVPRW 2021](https://arxiv.org/pdf/2011.03802.pdf).

