

## About

Official pytorch implementation of the paper: "SinGAN: Learning a Generative Model from a Single Natural Image"


# SinGAN

[Project](https://tamarott.github.io/SinGAN.htm) | [Arxiv](https://arxiv.org/pdf/1905.01164.pdf) | [CVF](http://openaccess.thecvf.com/content_ICCV_2019/papers/Shaham_SinGAN_Learning_a_Generative_Model_From_a_Single_Natural_Image_ICCV_2019_paper.pdf) | [Supplementary materials](https://openaccess.thecvf.com/content_ICCV_2019/supplemental/Shaham_SinGAN_Learning_a_ICCV_2019_supplemental.pdf) | [Talk (ICCV`19)](https://youtu.be/mdAcPe74tZI?t=3191)

### [](https://github.com/tamarott/SinGAN#official-pytorch-implementation-of-the-paper-singan-learning-a-generative-model-from-a-single-natural-image)Official pytorch implementation of the paper: "SinGAN: Learning a Generative Model from a Single Natural Image"

#### [](https://github.com/tamarott/SinGAN#iccv-2019-best-paper-award-marr-prize)ICCV 2019 Best paper award (Marr prize)



