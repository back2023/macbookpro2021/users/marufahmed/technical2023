
https://link.springer.com/chapter/10.1007/978-3-030-87361-5_33


- [ First Online: 30 September 2021](https://link.springer.com/chapter/10.1007/978-3-030-87361-5_33#chapter-info)
Part of the [Lecture Notes in Computer Science](https://link.springer.com/bookseries/558) book series (LNIP,volume 12890)

## Abstract

The goal of the stereoscopic image super-resolution (SR) is to reconstruct a pair of high-resolution (HR) images from corresponding low-resolution (LR) images. The existing stereo SR methods based on convolutional neural network (CNN) benefit from additional information from a different viewpoint to some extent. However, they cannot make good use of the complementary information from the different viewpoint, resulting in a lack of textures and details. The unevenly distributed features from left and right images were treated equally. To overcome the above difficulties, we put forward a stereo attention graph neural network (SA-GNN), which can extract reliable priors non-locally and fuse consistent contents adaptively cross different viewpoints. SA-GNN contains a series of stereo graph neural networks (SGNN), which alternate iteratively between in-view graph and cross-view graph under the aggregation and update mechanism of graph neural networks (GNN) to enhance SR performance. The comparison experiment results on four public datasets demonstrate that our SA-GNN outperforms the state-of-the-art methods.

