
## About

Caffe implementation of "Fast and Accurate Single Image Super-Resolution via Information Distillation Network" (CVPR 2018)
# IDN-Caffe

Caffe implementation of "Fast and Accurate Single Image Super-Resolution via Information Distillation Network"

[[arXiv]](http://arxiv.org/abs/1803.09454) [[CVF]](http://openaccess.thecvf.com/content_cvpr_2018/html/Hui_Fast_and_Accurate_CVPR_2018_paper.html) [[Poster]](https://github.com/Zheng222/IDN-Caffe/blob/master/files/cvpr18_poster.pdf) [[TensorFlow version]](https://github.com/Zheng222/IDN-tensorflow)


