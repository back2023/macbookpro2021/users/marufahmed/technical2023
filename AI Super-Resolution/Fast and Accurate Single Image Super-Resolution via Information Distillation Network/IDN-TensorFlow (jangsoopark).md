
https://github.com/jangsoopark/IDN-TensorFlow
## About

TensorFlow Implementation of "Fast and Accurate Single Image Super-Resolution via Information Distillation Network" (CVPR 2018)


## Introduction

This repository is TensorFlow implementation of IDN(CVPR16).

You can see more details from paper and author's project repository

- Github : [[IDN-Caffe]](https://github.com/Zheng222/IDN-Caffe)
- Paper : ["Fast and Accurate Single Image Super-Resolution via Information Distillation Network"](http://openaccess.thecvf.com/content_cvpr_2018/papers/Hui_Fast_and_Accurate_CVPR_2018_paper.pdf)

