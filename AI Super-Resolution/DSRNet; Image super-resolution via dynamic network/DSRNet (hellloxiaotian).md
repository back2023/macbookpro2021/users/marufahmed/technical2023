

## About

Image super-resolution via dynamic network (CAAI Transactions on Intelligence Technology, 2024)

# DSRNet

[](https://github.com/hellloxiaotian/DSRNet#dsrnet)

## Image super-resolution via dynamic network (DSRNet) is conducted by Chunwei Tian, Xuanyu Zhang, Qi Zhang, Mingming Yang and Zhaojie Ju, and accepted by CAAI Transactions on Intelligence Technology (IF:5.1) in 2023. It is implemented by Pytorch.

[](https://github.com/hellloxiaotian/DSRNet#image-super-resolution-via-dynamic-network-dsrnet-is-conducted-by-chunwei-tian-xuanyu-zhang-qi-zhang-mingming-yang-and-zhaojie-ju-and-accepted-by-caai-transactions-on-intelligence-technology-if51-in-2023-it-is-implemented-by-pytorch)

## It is reported by famous wechat computer technique platforms of Extreme Mart （[https://mp.weixin.qq.com/s/C7EvguC0Do9sWsK-NACx_g](https://mp.weixin.qq.com/s/C7EvguC0Do9sWsK-NACx_g)) and AIWalker ([https://mp.weixin.qq.com/s/L6lQ2X8R-MljQa7nobUYyQ](https://mp.weixin.qq.com/s/L6lQ2X8R-MljQa7nobUYyQ)).

[](https://github.com/hellloxiaotian/DSRNet#it-is-reported-by-famous-wechat-computer-technique-platforms-of-extreme-mart-httpsmpweixinqqcomsc7evguc0do9swsk-nacx_g-and--aiwalker-httpsmpweixinqqcomsl6lq2x8r-mljqa7nobuyyq)

## Its origianl can be obtained at [https://arxiv.org/pdf/2310.10413.pdf](https://arxiv.org/pdf/2310.10413.pdf).


### Abstract

[](https://github.com/hellloxiaotian/DSRNet#abstract)

#### Convolutional neural networks (CNNs) depend on deep network architectures to extract accurate information for image super-resolution. However, obtained information of these CNNs cannot completely express predicted high-quality images for complex scenes. In this paper, we present a dynamic network for image super-resolution (DSRNet), which contains a residual enhancement block, wide enhancement block, feature refinement block and construction block. The residual enhancement block is composed of a residual enhanced architecture to facilitate hierarchical features for image super-resolution. To enhance robustness of obtained super-resolution model for complex scenes, a wide enhancement block achieves a dynamic architecture to learn more robust information to enhance applicability of an obtained super-resolution model for varying scenes. To prevent interference of components in a wide enhancement block, a refinement block utilizes a stacked architecture to accurately learn obtained features. Also, a residual learning operation is embedded in the refinement block to prevent long-term dependency problem. Finally, a construction block is responsible for reconstructing high-quality images. Designed heterogeneous architecture can not only facilitate richer structural information, but also be lightweight, which is suitable for mobile digital devices. Experimental results shows that our method is more competitive in terms of performance and recovering time of image super-resolution and complexity.
