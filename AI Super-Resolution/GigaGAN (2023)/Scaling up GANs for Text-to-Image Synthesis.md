
https://mingukkang.github.io/GigaGAN/

## GigaGAN: Large-scale GAN for Text-to-Image Synthesis
**Can GANs also be trained on a large dataset for a general text-to-image synthesis task?** We present our 1B-parameter GigaGAN, achieving lower FID than Stable Diffusion v1.5, DALL·E 2, and Parti-750M. It generates 512px outputs at 0.13s, orders of magnitude faster than diffusion and autoregressive models, and inherits the disentangled, continuous, and controllable latent space of GANs. We also train a fast upsampler that can generate 4K images from the low-res outputs of text-to-image models.


## Disentangled Prompt Interpolation

GigaGAN comes with a disentangled, continuous, and controllable latent space.  
In particular, it can achieve layout-preserving fine style control by applying a different prompt at fine scales.


## Upscaling to 16-megapixel photos with GigaGAN

Our GigaGAN framework can also be used to train an efficient, higher-quality upsampler. This can be applied to real images, or to the outputs of other text-to-image models like diffusion. GigaGAN can synthesize ultra high-res images at 4k resolution in 3.66 seconds.

![GigaGAN generator](https://mingukkang.github.io/GigaGAN/static/images/G_architecture.jpg)



![GigaGAN discriminator](https://mingukkang.github.io/GigaGAN/static/images/D_architecture.jpg)