
https://github.com/lucidrains/gigagan-pytorch


## About

Implementation of GigaGAN, new SOTA GAN out of Adobe. Culmination of nearly a decade of research into GANs

## GigaGAN - Pytorch

[](https://github.com/lucidrains/gigagan-pytorch#gigagan---pytorch)

Implementation of [GigaGAN](https://arxiv.org/abs/2303.05511v2) [(project page)](https://mingukkang.github.io/GigaGAN/), new SOTA GAN out of Adobe.

I will also add a few findings from [lightweight gan](https://github.com/lucidrains/lightweight-gan), for faster convergence (skip layer excitation) and better stability (reconstruction auxiliary loss in discriminator)

It will also contain the code for the 1k - 4k upsamplers, which I find to be the highlight of this paper.

Please join [![Join us on Discord](https://camo.githubusercontent.com/dc520ec165f0f9cb3f53f870171489ee4ff66b29d22efff27c14d82fadd365a8/68747470733a2f2f696d672e736869656c64732e696f2f646973636f72642f3832333831333135393539323030313533373f636f6c6f723d353836354632266c6f676f3d646973636f7264266c6f676f436f6c6f723d7768697465)](https://discord.gg/xBPBXfcFHd) if you are interested in helping out with the replication with the [LAION](https://laion.ai/) community