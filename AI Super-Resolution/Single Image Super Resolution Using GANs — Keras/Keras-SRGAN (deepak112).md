
https://github.com/deepak112/Keras-SRGAN

# Keras-SRGAN

Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network implemented in Keras

For more about topic check [Single Image Super Resolution Using GANs — Keras](https://medium.com/@birla.deepak26/single-image-super-resolution-using-gans-keras-aca310f33112)

## [](https://github.com/deepak112/Keras-SRGAN#problem-statement)Problem Statement:

```
Enhancing low resolution images by applying deep network with adversarial network (Generative Adversarial Networks) 
to produce high resolutions images.
```
 