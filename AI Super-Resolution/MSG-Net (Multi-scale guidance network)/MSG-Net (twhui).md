

https://github.com/twhui/MSG-Net


## About

Depth Map Super-Resolution by Deep Multi-Scale Guidance, ECCV 2016


This repository (**[https://github.com/twhui/MSG-Net](https://github.com/twhui/MSG-Net)**) is the offical release of **MSG-Net** for our paper [**Depth Map Super-Resolution by Deep Multi-Scale Guidance**](http://personal.ie.cuhk.edu.hk/~ccloy/files/eccv_2016_depth.pdf) in ECCV16. It comes with four trained networks (x2, x4, x8, and x16), one hole-filled RGBD training set, and three hole-filled RGBD testing sets (A, B, and C).

**To the best of our knowledge, MSG-Net is the FIRST convolution neural networkwhich attempts to _upsample depth images under multi-scale guidance from the corresponding high-resolution RGB images_**.

Another [**repository**](https://github.com/twhui/MS-Net) for **MS-Net** (without multi-scale guidance) is also available.

For more details, please visit [**my project page**](http://mmlab.ie.cuhk.edu.hk/projects/guidance_SR_depth.html).


