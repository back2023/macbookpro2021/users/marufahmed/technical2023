
https://github.com/zilongzheng/PatchGenCN

# PatchGenCN

TensorFlow Implementation for the paper:

**[Patchwise Generative ConvNet: Training Energy-Based Models from a Single Natural Image for Internal Learning](https://openaccess.thecvf.com/content/CVPR2021/html/Zheng_Patchwise_Generative_ConvNet_Training_Energy-Based_Models_From_a_Single_Natural_CVPR_2021_paper.html)**  
In CVPR 2021 (Oral)