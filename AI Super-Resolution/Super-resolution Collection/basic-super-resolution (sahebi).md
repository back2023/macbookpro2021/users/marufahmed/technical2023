
https://github.com/sahebi/basic-super-resolution


## About

Test basic super resolution methods with different optimization methods


### Basic Super Resolution

This git cloned from [https://github.com/icpm/super-resolution](https://github.com/icpm/super-resolution) change some modifications.

- [x]  Add CARN method
- [x]  Add different optimization method
- [x]  Log the checkppoints and _logs
- [ ]  Log the result

### [](https://github.com/sahebi/basic-super-resolution#optimizer)Optimizer

- ADAM
- AdamSparse
- Adamax
- Adadelta
- Adagrad
- ASGD
- LAMB
- RProp
- SGD
- RMSprop

### [](https://github.com/sahebi/basic-super-resolution#single-image-super-resolution-methods)Single Image Super Resolution Methods

- [x]  SubPixelCNN
- [x]  SRCNN
- [x]  SRCNNT
- [x]  VDSR
- [x]  EDSR
- [x]  FSRCNN
- [x]  DRCN `batchsize should be small, batchsize=4`
- [ ]  SRGAN `change save checkpoint path patterns`
- [x]  DBPN `batchsize should be small, batchsize=1`
- [x]  MemNet
- [ ]  CARN

 
