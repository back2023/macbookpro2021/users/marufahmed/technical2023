
https://github.com/cedricoeldorf/proba-v-super-resolution-challenge/tree/master


##### Submission Report for Incubit Technical Assignment

The following report outlines the given problem, tackles data preprocessing, model selection and presents results. I'd like to draw special attention to the conclusion, which holds two interesting approaches that weren't tackled in these experiments due to time constraint.