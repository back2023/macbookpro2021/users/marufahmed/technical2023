
https://github.com/lfsimoes/probav

### About

[](https://github.com/lfsimoes/probav#about)

This repository offers `embiggen`, a Python module implementing a set of core functionalities to help participants to the PROBA-V Kelvins competition: scoring function, image file access, image aggregation and upscaling, creating submissions...

A walkthrough of the module is provided in the notebooks:

- [00 - module demo.ipynb](https://nbviewer.jupyter.org/github/lfsimoes/probav/blob/master/00%20-%20module%20demo.ipynb)
- [01 - improved feedback.ipynb](https://nbviewer.jupyter.org/github/lfsimoes/probav/blob/master/01%20-%20improved%20feedback.ipynb?flush_cache=true)
 