
https://paperswithcode.com/task/multi-frame-super-resolution/latest


  
When multiple images of the same view are taken from slightly different positions, perhaps also at different times, then they collectively contain more information than any single image on its own. Multi-Frame Super-Resolution fuses these low-res inputs into a composite high-res image that can reveal some of the original detail that cannot be recovered from any low-res image alone.

( Credit: [HighRes-net](https://github.com/ElementAI/HighRes-net) )


## Benchmarks

Add a Result

These leaderboards are used to track progress in Multi-Frame Super-Resolution

---

| Trend                                                                                                                                                                                              | Dataset                                                                            | Best Model                                                                         | Paper                                                                             | Code                                                                                   | Compare                                                                            |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| [![](https://production-media.paperswithcode.com/sota-thumbs/multi-frame-super-resolution-on-proba-v-small_9b1524ba.png)](https://paperswithcode.com/sota/multi-frame-super-resolution-on-proba-v) | [PROBA-V](https://paperswithcode.com/sota/multi-frame-super-resolution-on-proba-v) | [TR-MISR](https://paperswithcode.com/sota/multi-frame-super-resolution-on-proba-v) | [](https://paperswithcode.com/paper/tr-misr-multiimage-super-resolution-based-on) | [](https://paperswithcode.com/paper/tr-misr-multiimage-super-resolution-based-on#code) | [See all](https://paperswithcode.com/sota/multi-frame-super-resolution-on-proba-v) |