
https://github.com/ENNAJIHYassin/Proba-V


The solution proposed here is a Super Resolution Generative Adversarial Network, as described in this [paper](https://arxiv.org/pdf/1609.04802.pdf).

The architecture is slightly modified from the original paper (mainly for performance concerns).


```
For training: Relatively powerfull GPU, this model was trained on an NVIDIA GTX 1080.
It is advised to train this model using a better GPU with more VRAM.
```


