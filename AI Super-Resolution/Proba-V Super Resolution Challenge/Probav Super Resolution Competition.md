https://github.com/tteresi7/plsProbav/tree/master

## Overview

The Probav Super Resolution Competition can be found [here](https://kelvins.esa.int/proba-v-super-resolution/).

The main idea of this competition is to take a time series of low resolution shots and improve the resolution with respect to an image shot at a higher resolution.

The images are satellite shots taken at distance of 100m per pixel and 300m per pixel.

## My model

My model is a variation on the SRGAN used in this same competition that can be found [here](https://github.com/ENNAJIHYassin/Proba-V).

Instead of training the model as a GAN I wanted to show that the model can be trained without a discriminator, with less layers, and less filters. The results can be seen in the table below (lower is better).



## Running


I am using the embiggen library that can be found [here](https://github.com/lfsimoes/probav). It is included in this respository.




### Train

You can modify the parameters in there as you feel necessary. BATCH_SIZE is probably the only thing you'll need to modify yourself. All parameters and hyperparameters in the file are exactly as I trained the model. However, I did ultimately only train 500 epoch's and select the best from there. train.py is currently configured to go 2000 epochs. You can train for that long if you like, but it seemed as if it started to be fully converged at around 300 epoch's.



