

https://github.com/ShuhangGu/DASR


## About

Training and Testing codes for our paper "Real-world Image Super-resolution via Domain-distance Aware Training"

# DASR

(CVPR-2021) Official PyTorch code for our paper DASR: [Unsupervised Real-world Image Super Resolution via Domain-distance Aware Training](https://arxiv.org/abs/2004.01178).

### [](https://github.com/ShuhangGu/DASR#abstract)Abstract

These days, unsupervised super-resolution (SR) has been soaring due to its practical and promising potential in real scenarios. The philosophy of off-the-shelf approaches lies in the augmentation of unpaired data, \ie first generating synthetic low-resolution (LR) images �� corresponding to real-world high-resolution (HR) images �� in the real-world LR domain ��, and then utilizing the pseudo pairs ��,�� for training in a supervised manner. Unfortunately, since image translation itself is an extremely challenging task, the SR performance of these approaches are severely limited by the domain gap between generated synthetic LR images and real LR images. In this paper, we propose a novel domain-distance aware super-resolution (DASR) approach for unsupervised real-world image SR. The domain gap between training data (e.g. ��) and testing data (e.g. ��) is addressed with our \textbf{domain-gap aware training} and \textbf{domain-distance weighted supervision} strategies. Domain-gap aware training takes additional benefit from real data in the target domain while domain-distance weighted supervision brings forward the more rational use of labeled source domain data. The proposed method is validated on synthetic and real datasets and the experimental results show that DASR consistently outperforms state-of-the-art unsupervised SR approaches in generating SR outputs with more realistic and natural textures. Code will be available at [DASR](https://github.com/ShuhangGu/DASR).

 