

https://github.com/neuralchen/EQSR


## About

An Arbitrary-Scale Image Super-Resolution Framework (CVPR2023)


# Deep Arbitrary-Scale Image Super-Resolution via Scale-Equivariance Pursuit (EQSR)

[](https://github.com/neuralchen/EQSR#deep-arbitrary-scale-image-super-resolution-via-scale-equivariance-pursuit-eqsr)

## Accepted by CVPR2023

[](https://github.com/neuralchen/EQSR#accepted-by-cvpr2023)

**The official repository with Pytorch**

Our paper can be downloaded from [EQSR](https://openaccess.thecvf.com/content/CVPR2023/papers/Wang_Deep_Arbitrary-Scale_Image_Super-Resolution_via_Scale-Equivariance_Pursuit_CVPR_2023_paper.pdf).

## Introduction

[](https://github.com/neuralchen/EQSR#introduction)

EQSR is designed to pursue scale-equivariance image super-resolution. We compare the PSNR degradation rate of our method and ArbSR. Taking the SOTA fixed-scale method HAT as reference, our model presents a more stable degradation as the scale increases, reflecting the equivariance of our method.



