https://github.com/jaroslaw1007/CLIT

This repository contains the PyTorch based official implementation of the paper titled:  
[Cascaded Local Implicit Transformer for Arbitrary-Scale Super-Resolution](https://arxiv.org/abs/2303.16513) CVPR 2023.


### Train

#### [](https://github.com/jaroslaw1007/CLIT#edsr-baseline)EDSR-Baseline
**Stage1**: `python train.py --config configs/train/train_edsr_baseline_lit.yaml --name lit_edsr`
**Stage2**: `python train.py --config configs/train/train_edsr_baseline_clit2.yaml --name clit_edsr2`
**Stage3**: `python train.py --config configs/train/train_edsr_baseline_clit3.yaml --name clit_edsr3`

#### [](https://github.com/jaroslaw1007/CLIT#rdn)RDN
**Stage1**: `python train.py --config configs/train/train_rdn_lit.yaml --name lit_rdn`
**Stage1**: `python train.py --config configs/train/train_rdn_clit2.yaml --name clit_rdn2`
**Stage1**: `python train.py --config configs/train/train_rdn_clit3.yaml --name clit_rdn3`

#### [](https://github.com/jaroslaw1007/CLIT#swinir)SwinIR
**Stage1**: `python train.py --config configs/train/train_swinir_lit.yaml --name lit_swinir`
**Stage2**: `python train.py --config configs/train/train_swinir_clit2.yaml --name clit_swinir2`
**Stage3**: `python train.py --config configs/train/train_swinir_clit3.yaml --name clit_swinir3`

∗ Please note that, if you want to cascadedly train stage2 or stage3 CLIT, you need to modified the "pre_train" property in the configuration so as to load previous stage1 or stage2 model as the pre-trained model.

```
Ex: train the stage2 CLIT using edsr-baseline model

pre_train: save/lit_edsr/epoch-last.pth
```