


https://github.com/junjun-jiang/Hyperspectral-Image-Super-Resolution-Benchmark


A list of hyperspectral image super-resolution resources collected by [Junjun Jiang](http://homepage.hit.edu.cn/jiangjunjun). **If you find that important resources are not included, please feel free to contact me.**

Hyperspectral image super-resolution is a kind of technique that can generate a high spatial and high spectral resolution image from one of the following observed data (1) low-resolution multispectral image, e.g., LR RGB image, (2) high-resolution multispectral image, e.g., HR RGB image or other 2D measurement, (3) low-resolution hyperspectral image, or (4) high-resolution multispectral image and low-resolution hyperspectral image. According to kind of observed data, hyperspectral image super-resolution techniques can be divided into four classes: **spatiospectral super-resolution (SSSR)**, **spectral super-resolution (SSR)**, **single hyperspectral image super-resolution (SHSR)**, and **multispectral image and hyperspectral image fusion (MHF)**. Note that we take hyperspectral image reconstruction from 2D measurement as a class of SSR.


