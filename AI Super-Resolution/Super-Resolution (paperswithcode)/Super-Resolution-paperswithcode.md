
https://paperswithcode.com/task/super-resolution

https://paperswithcode.com/task/super-resolution?page=3


https://paperswithcode.com/task/super-resolution/codeless?page=4


**Super-Resolution** is a task in computer vision that involves increasing the resolution of an image or video by generating missing high-frequency details from low-resolution input. The goal is to produce an output image with a higher resolution than the input image, while preserving the original content and structure.



