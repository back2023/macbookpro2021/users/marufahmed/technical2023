

https://github.com/sunwj/CAR

## About

Content adaptive resampler for image downscaling


# CAR-pytorch

Pytorch implementation of paper **"Learned Image Downscaling for Upscaling using Content Adaptive Resampler"**


