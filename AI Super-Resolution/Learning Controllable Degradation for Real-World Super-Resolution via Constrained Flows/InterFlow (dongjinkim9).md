
https://github.com/dongjinkim9/InterFlow

## About

Learning Controllable Degradation for Real-World Super-Resolution via Constrained Flows, in ICML 2023


# Learning Controllable Degradation  
for Real-World Super-Resolution via Constrained Flows

Seobin Park*, Dongjin Kim*, Sungyong Baik, and Tae Hyun Kim  
*Equal Contribution

[[ICML2023] Paper](https://openreview.net/forum?id=M3IX2zAIdi)

> Recent deep-learning-based super-resolution (SR) methods have been successful in recovering high-resolution (HR) images from their low-resolution (LR) counterparts, albeit on the synthetic and simple degradation setting: bicubic downscaling. On the other hand, super-resolution on real-world images demands the capability to handle complex downscaling mechanism which produces different artifacts (e.g., noise, blur, color distortion) upon downscaling factors. To account for complex downscaling mechanism in real-world LR images, there have been a few efforts in constructing datasets consisting of LR images with real-world downsampling degradation. However, making such datasets entails a tremendous amount of time and effort, thereby resorting to very few number of downscaling factors (e.g., ×2,×3,×4). To remedy the issue, we propose to generate realistic SR datasets for unseen degradation levels by exploring the latent space of real LR images and thereby producing more diverse yet realistic LR images with complex real-world artifacts. Our quantitative and qualitative experiments demonstrate the accuracy of the generated LR images, and we show that the various conventional SR networks trained with our newly generated SR datasets can produce much better HR images.


