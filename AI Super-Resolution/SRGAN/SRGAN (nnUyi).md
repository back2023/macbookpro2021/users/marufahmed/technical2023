https://github.com/nnUyi/SRGAN

## About

An implement of SRGAN(Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network) for tensorflow version

- An implement of [SRGAN](https://arxiv.org/abs/1609.04802)(Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network) for tensorflow version.
- In this repo, vgg19 is not used, instead, MSE is ued to train SRResNet. If you want to use vgg19 to calculate the content loss, you can download model that trained in ImageNet. Then you just need to load to your model during training phase.



### train data

- In this repo, I use parts of [ImageNet](http://www.image-net.org/) datasets as **train data**, [here](https://pan.baidu.com/s/1eSJC0lc) you can download the datasets that I used.
- After you have download the datasets, copy ImageNet(here I only use 3137 images) datsets to _**/data/train**_, then you have _**/data/train/ImageNet**_ path, and training images are stored in _**/data/train/ImageNet**_
- I crop image into **256*256 resolution**, actually you can crop them according to your own.

### [](https://github.com/nnUyi/SRGAN#val-data)val data

- **Set5** dataset is used as **val data**, you can download it [here](https://pan.baidu.com/s/1dFyFFSt).
- After you download **Set5**, please store it in _**/data/val/**_ , then you have _**/data/val/Set5**_ path, and val images are stored in _**/data/val/Set5**_

### [](https://github.com/nnUyi/SRGAN#test-data)test data

- **Set14** dataset is used as **test data**, you can download it [here](https://pan.baidu.com/s/1nvmUkBn).
- After you download **Set14**, please store it in _**/data/test/**_ , then you have _**/data/test/Set14**_ path, and val images are stored in _**/data/test/Set14**_





   