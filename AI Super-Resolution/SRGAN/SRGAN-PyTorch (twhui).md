
https://github.com/twhui/SRGAN-PyTorch

This repository contains the unoffical pyTorch implementation of **SRGAN** and also **SRResNet** in the paper [Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network](https://arxiv.org/abs/1609.04802), CVPR17.

We closely followed the network structure, training strategy and training set as the orignal SRGAN and SRResNet. We also implemented **subpixel convolution layer** as [Real-Time Single Image and Video Super-Resolution Using an Efficient Sub-Pixel Convolutional Neural Network](https://arxiv.org/abs/1609.05158), CVPR16. [My collaborator](https://github.com/waihokwok) also shares contribution to this repository.


