
https://github.com/basiralab/BGSR-PY

BGSR (Brain Graph Super-Resolution) for super-resolving low-resolution graphs code, recoded by Busra Asan. Please contact [busraasan2@gmail.com](mailto:busraasan2@gmail.com) for further inquiries. Thanks.

```
You can download the Matlab version of BGSR at: https://github.com/basiralab/BGSR

```

While a few image super-resolution techniques have been proposed for MRI super-resolution, graph super-resolution techniques are currently absent. To this aim, we design the ﬁrst brain graph super-resolution using functional brain data with the aim to boost neurological disorder diagnosis. Our framework learns how to generate high-resolution (HR) graphs from low-resolution (LR) graphs without resorting to the computationally expensive image processing pipelines for connectome construction at high-resolution scales.



