
The **Neural Process Family** (NPF) is a collection of models (called neural processes (NPs)) that tackles both of these issues, by _meta-learning_ a distribution over predictors, also known as a _stochastic process_. Meta-learning allows neural processes to incorporate data from many related tasks (e.g. many different patients in our medical example) and the stochastic process framework allows the NPF to effectively represent uncertainty.


https://yanndubs.github.io/Neural-Process-Family/text/Intro.html



