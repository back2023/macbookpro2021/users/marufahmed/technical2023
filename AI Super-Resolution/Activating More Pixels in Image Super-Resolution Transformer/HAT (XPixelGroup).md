
## About

CVPR2023 - Activating More Pixels in Image Super-Resolution Transformer

# HAT
[[Paper Link]](https://arxiv.org/abs/2205.04437) [![Replicate](https://camo.githubusercontent.com/b4c3e26b84f9481a4dc498d3fce7583a10951e7535eb753e6895cfc7a330ae10/68747470733a2f2f7265706c69636174652e636f6d2f636a7762772f6861742f6261646765)](https://replicate.com/cjwbw/hat)

### [](https://github.com/XPixelGroup/HAT#activating-more-pixels-in-image-super-resolution-transformer)Activating More Pixels in Image Super-Resolution Transformer

[Xiangyu Chen](https://chxy95.github.io/), [Xintao Wang](https://xinntao.github.io/), [Jiantao Zhou](https://www.fst.um.edu.mo/personal/jtzhou/) and [Chao Dong](https://scholar.google.com.hk/citations?user=OSDCB0UAAAAJ&hl=zh-CN)


https://github.com/XPixelGroup/HAT




