

This repository is the official PyTorch implementation of "Reference-based Image Super-Resolution with Deformable Attention Transformer" ([arxiv](https://arxiv.org/abs/2207.11938), [supp](https://github.com/caojiezhang/DATSR/releases/download/v0.0/supplementary.pdf), [pretrained models](https://github.com/caojiezhang/DATSR/releases), [visual results](https://github.com/caojiezhang/DATSR/releases)).


https://github.com/caojiezhang/DATSR


