

This repository focuses on partially reproducing the results of the [Image Super-Resolution via Iterate Refinement paper](https://iterative-refinement.github.io/). It focuses solely on the celebaHQ dataset and the 16x16 -> 128x128 task.

The repository is based on the model and methodology description from the paper. In some places though, details are lacking and guesses had to be made. What you can expect:

- The overall Unet architecture reproduces the implementation from [Denoising Diffusion Probabilistic Model](https://hojonathanho.github.io/diffusion/), something that was made easier from being able to access [their own paper implementation](https://github.com/hojonathanho/diffusion). It includes attention layers at lower resolutions and residual connections.
- The residual block composition is taken from [bigGAN p17](https://arxiv.org/pdf/1809.11096.pdf).
- The conditional instance normalization is taken from [Generative Modeling by Estimating Gradients of the Data Distribution](https://arxiv.org/pdf/1907.05600.pdf).
- The default hyperparameters are based on the paper description: 256 batch size, 1M steps training, adam optimizer, 1e-4 learning rate with 50000 warmup steps, 0.2 dropout, 3 resnet blocks per unet layer, 128 channel dim, {1, 2, 4, 8, 8} depth multipliers. Despite all of this, the total number of parameters for our model is 480M, against 550M in the paper. Slight differences in the model architecture are likely accounting for the difference.
- The noise schedule is where the paper provides the least details, although it stresses its importance for the quality of the result. We implemented quadratic, linear, constant and warmup schedules. For training, we use a quadratic schedule, with a starting value for alpha of 1, an end value of 0.993 and 2000 timesteps.
 