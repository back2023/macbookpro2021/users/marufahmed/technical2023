
https://github.com/Janspiry/Image-Super-Resolution-via-Iterative-Refinement


## Brief

This is an unofficial implementation of **Image Super-Resolution via Iterative Refinement(SR3)** by **PyTorch**.

There are some implementation details that may vary from the paper's description, which may be different from the actual `SR3` structure due to details missing. Specifically, we:

- Used the ResNet block and channel concatenation style like vanilla `DDPM`.
- Used the attention mechanism in low-resolution features ( 16×16 ) like vanilla `DDPM`.
- Encode the � as `FilM` structure did in `WaveGrad`, and embed it without affine transformation.
- Define the posterior variance as 1−��−11−���� rather than ��, which gives similar results to the vanilla paper.

**If you just want to upscale (64×64)px→(512×512)px images using the pre-trained model, check out [this google colab script](https://colab.research.google.com/drive/1G1txPI1GKueKH0cSi_DgQFKwfyJOXlhY?usp=sharing).**
 



