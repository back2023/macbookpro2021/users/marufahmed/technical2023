
https://github.com/kalpeshjp89/USISResNet


This is repository of code for NTIRE-2020 (CVPRW-2020) paper titled ["Unsupervised Single Image Super-Resolution Network (USISResNet) for Real-World Data Using Generative Adversarial Network"](https://openaccess.thecvf.com/content_CVPRW_2020/papers/w31/Prajapati_Unsupervised_Single_Image_Super-Resolution_Network_USISResNet_for_Real-World_Data_Using_CVPRW_2020_paper.pdf)



We are thankful to Xinntao for their [ESRGAN](https://github.com/xinntao/ESRGAN) code on which we have made this work.

For any problem or query, you may contact to Kalpesh Prajapati at [kalpesh.jp89@gmail.com](mailto:kalpesh.jp89@gmail.com)