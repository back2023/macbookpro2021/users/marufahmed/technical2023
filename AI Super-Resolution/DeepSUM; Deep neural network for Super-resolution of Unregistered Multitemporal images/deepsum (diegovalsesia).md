
https://github.com/diegovalsesia/deepsum



# [DeepSUM: Deep neural network for Super-resolution of Unregistered Multitemporal images](https://arxiv.org/abs/1907.06490)

[](https://github.com/diegovalsesia/deepsum#deepsum-deep-neural-network-for-super-resolution-of-unregistered-multitemporal-images)

DeepSUM is a novel Multi Image Super-Resolution (MISR) deep neural network that exploits both spatial and temporal correlations to recover a single high resolution image from multiple unregistered low resolution images.

This repository contains python/tensorflow implementation of DeepSUM, trained and tested on the PROBA-V dataset provided by ESA’s [Advanced Concepts Team](http://www.esa.int/gsp/ACT/index.html) in the context of the [European Space Agency's Kelvin competition](https://kelvins.esa.int/proba-v-super-resolution/home/).

DeepSUM is the winner of the PROBA-V SR challenge.


