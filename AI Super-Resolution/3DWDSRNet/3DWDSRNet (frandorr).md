
https://github.com/frandorr/3DWDSRNet


## About

Solution to Proba-V Super Resolution


# 3DWDSRNet
Easy, fast and low on resources method that achieved on 02-02-2020 first place with a score of 0.94625 in [Post Mortem Proba-V Super Resolution competition](https://kelvins.esa.int/proba-v-super-resolution-post-mortem/leaderboard/).

