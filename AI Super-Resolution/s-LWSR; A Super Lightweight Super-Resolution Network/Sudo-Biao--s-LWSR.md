
https://github.com/Sudo-Biao/s-LWSR

## About

s-LWSR: A Super Lightweight Super-Resolution Network


# [s-LWSR: Super Lightweight Super-Resolution Network](https://github.com/Sudo-Biao/s-LWSR#s-lwsr-super-lightweight-super-resolution-network)

This is the code of the paper in following:

[Biao Li](https://github.com/Sudo-Biao), [Bo Wang](http://it.uibe.edu.cn/szdw/dsjkxyjzx/50452.htm), [Jiabin Liu](https://github.com/liujiabin008), [Zhiquan Qi](https://github.com/qizhiquan) and [Yong Shi](http://www.feds.ac.cn/index.php/zh-cn/zxjs/zxld/1447-sy)"s-LWSR: Super Lightweight Super-Resolution Network", [[arXiv]](https://arxiv.org/abs/1909.10774) [Accepted by IEEE Transactions on Image Processing]

The code is built on [EDSR (PyTorch)](https://github.com/thstkdgus35/EDSR-PyTorch) and [RCAN(Pytorch)](https://github.com/yulunzhang/RCAN), and tested on Ubuntu 18.04 environment (Python3.7, PyTorch_1.0) with Titan Xp GPU.


