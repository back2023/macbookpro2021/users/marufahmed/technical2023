
https://www.tensorflow.org/hub/tutorials/image_enhancing

This colab demonstrates use of TensorFlow Hub Module for Enhanced Super Resolution Generative Adversarial Network (_by Xintao Wang et.al._) [[Paper](https://arxiv.org/pdf/1809.00219.pdf)] [[Code](https://github.com/captain-pool/GSOC/)]

for image enhancing. _(Preferrably bicubically downsampled images)._

Model trained on DIV2K Dataset (on bicubically downsampled images) on image patches of size 128 x 128.




