
https://github.com/uw-loci/demo_wsi_superres

## About

WSISR: Single image super-resolution for Whole slide Imaging using convolutional neural networks and self-supervised color normalization.

 This is the PyTorch implementation of using generative adversarial neural networks for single-image super-resolution in whole slide imaging. [Paper. _Medical Image Analysis_.](https://www.sciencedirect.com/science/article/abs/pii/S1361841520303029)

