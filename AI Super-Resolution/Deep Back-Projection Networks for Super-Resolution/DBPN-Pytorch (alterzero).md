

https://github.com/alterzero/DBPN-Pytorch


# Deep Back-Projection Networks for Super-Resolution (CVPR2018)

[](https://github.com/alterzero/DBPN-Pytorch#deep-back-projection-networks-for-super-resolution-cvpr2018)

## Winner (1st) of [NTIRE2018](http://openaccess.thecvf.com/content_cvpr_2018_workshops/papers/w13/Timofte_NTIRE_2018_Challenge_CVPR_2018_paper.pdf) Competition (Track: x8 Bicubic Downsampling)

[](https://github.com/alterzero/DBPN-Pytorch#winner-1st-of-ntire2018-competition-track-x8-bicubic-downsampling)

## Winner of [PIRM2018](https://arxiv.org/pdf/1809.07517.pdf) (1st on Region 2, 3rd on Region 1, and 5th on Region 3)


