
https://github.com/jangsoopark/VDSR-TensorFlow

# VDSR-TensorFlow

---

## [](https://github.com/jangsoopark/VDSR-TensorFlow#introduction)Introduction

This repository is TensorFlow implementation of VDSR (CVPR16).

You can see more details from paper and author's project page

- Project page : [VDSR page](https://cv.snu.ac.kr/research/VDSR/)
    
- Paper : ["Accurate Image Super-Resolution Using Very Deep Convolutional Network"](https://cv.snu.ac.kr/research/VDSR/VDSR_CVPR2016.pdf)



