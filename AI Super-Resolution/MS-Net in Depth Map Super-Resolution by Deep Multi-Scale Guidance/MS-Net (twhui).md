

## About

MS-Net in Depth Map Super-Resolution by Deep Multi-Scale Guidance, ECCV 2016



# MS-Net (Multi-scale network)

[](https://github.com/twhui/MS-Net#ms-net-multi-scale-network)

This repository (**[https://github.com/twhui/MS-Net](https://github.com/twhui/MS-Net)**) is the offical release of **MS-Net** for our paper [**Depth Map Super-Resolution by Deep Multi-Scale Guidance**](http://personal.ie.cuhk.edu.hk/~ccloy/files/eccv_2016_depth.pdf) in ECCV16. It comes with four trained networks (x2, x4, x8, and x16) one hole-filled RGBD training set, and three hole-filled RGBD testing sets (A, B, and C).

**To the best of our knowledge, MS-Net is the **FIRST convolution neural network** which attempts to _upsample depth images_**.

Another [**repository**](https://github.com/twhui/MSG-Net) for **MSG-Net** (multi-scale guidance network) is also available.

For more details, please visit [**my project page**](http://mmlab.ie.cuhk.edu.hk/projects/guidance_SR_depth.html).


