
https://github.com/basiralab/GSR-Net
This repository provides the official PyTorch implementation of the following paper:

**Graph Super-Resolution Network for predicting high-resolution connectomes from low-resolution connectomes.** [Megi Isallari](https://github.com/meg-i)1, [Islem Rekik](https://basira-lab.com/)1

> 1BASIRA Lab, Faculty of Computer and Informatics, Istanbul Technical University, Istanbul, Turkey

Please contact [isallari.megi@gmail.com](mailto:isallari.megi@gmail.com) for further inquiries. Thanks.

While a significant number of image super-resolution methods have been proposed for MRI super-resolution, building generative models for super-resolving a low-resolution brain connectome at a higher resolution (i.e., adding new graph nodes/edges) remains unexplored —although this would circumvent the need for costly data collection and manual labelling of anatomical brain regions (i.e. parcellation). To fill this gap, we introduce GSR-Net (Graph Super-Resolution Network), the first super-resolution framework operating on graph-structured data that generates high-resolution brain graphs from low-resolution graphs.

