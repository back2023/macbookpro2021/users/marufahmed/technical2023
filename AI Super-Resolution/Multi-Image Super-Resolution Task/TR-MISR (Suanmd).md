

https://github.com/Suanmd/TR-MISR/tree/master?tab=readme-ov-file


### **Ranked #1** on [Multi-Frame Super-Resolution on PROBA-V](https://paperswithcode.com/sota/multi-frame-super-resolution-on-proba-v)

[](https://github.com/Suanmd/TR-MISR/tree/master?tab=readme-ov-file#ranked-1-on--multi-frame-super-resolution-on-proba-v)

In remote sensing, multi-image super-resolution (MISR) is a challenging problem. The release of the PROBA-V Kelvin dataset has aroused our great interest.

We believe that multiple images contain more information than a single image, so it is necessary to improve image utilization significantly. Besides, the time span of multiple images taken by the PROBA-V satellite is long, so the impact of image position needs to be reduced.

In this repository, we demonstrate a novel Transformer-based MISR framework, namely TR-MISR, which gets the state-of-the-art on the PROBA-V Kelvin dataset. TR-MISR does not pursue the complexity of the encoder and decoder but the fusion capability of the fusion module. Specifically, we rearrange the feature maps encoded by low-resolution images to a set of feature vectors. By adding a learnable embedding vector, these feature vectors can be fused through multi-layers of the Transformer with self-attention. Then, we decode the output of the embedding vector to get a patch of a high-resolution image.

TR-MISR can receive more unclear input images without performance degradation, and it does not require pre-training. Overall, TR-MISR is an attempt to apply Transformers to a specific low-level vision task.

- Recommended GPU platform: An NVIDIA ® Tesla ® V100 GPU.
- If you are using another GPU and facing a memory shortage, please reduce the batch size or choose a smaller model hyperparameter as appropriate.

