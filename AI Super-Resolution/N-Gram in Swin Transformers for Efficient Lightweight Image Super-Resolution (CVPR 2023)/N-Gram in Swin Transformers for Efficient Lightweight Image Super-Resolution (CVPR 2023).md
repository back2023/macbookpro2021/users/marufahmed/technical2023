
https://github.com/rami0205/NGramSwin

- Introduces the N-Gram context to deep learning in the low-level vision domain.
- Our N-Gram context algorithm used at window partitioning is in `my_model/ngswin_model/win_partition.py`
- Two tracks of this paper:

1. Constructing NGswin with an efficient architecture for image super-resolution.
2. Improving other Swin Transformer based SR methods (SwinIR-light, HNCT) with N-Gram.

- NGswin outperforms the previous leading efficient SR methods with a relatively efficient structure.
- SwinIR-NG outperforms the current best state-of-the-art lightweight SR methods.

