



https://paperswithcode.com/sota/multi-frame-super-resolution-on-proba-v


The PROBA-V Super-Resolution dataset is the official dataset of ESA's Kelvins competition for "PROBA-V Super Resolution". It contains satellite data from 74 hand-selected regions around the globe at different points in time. The data is composed of radiometrically and geometrically corrected Top-Of-Atmosphere (TOA) reflectances for the RED and NIR spectral bands at 300m and 100m resolution in Plate Carrée projection. The 300m resolution data is delivered as 128x128 grey-scale pixel images, the 100m resolution data as 384x384 grey-scale pixel images. Additionally, a quality map is provided for each pixel, indicating whether the pixels are concealed (i.e. by clouads, ice, water, missing information, etc.).

The goal of the challenge can be described as Multi-Image Super-resolution: Construct a single high-resolution image out of a series of more frequent low resolution images.

Detailed information about the related competition can be found at https://kelvins.esa.int/proba-v-super-resolution.


