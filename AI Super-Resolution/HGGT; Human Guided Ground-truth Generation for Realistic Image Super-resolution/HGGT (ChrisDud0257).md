
https://github.com/ChrisDud0257/HGGT


## About

Official Code for our CVPR2023 paper "Human Guided Ground-truth Generation for Realistic Image Super-resolution"


# HGGT

Official PyTorch code and dataset for our paper "HGGT" in CVPR 2023.

### [Paper](https://openaccess.thecvf.com/content/CVPR2023/papers/Chen_Human_Guided_Ground-Truth_Generation_for_Realistic_Image_Super-Resolution_CVPR_2023_paper.pdf) | [Supplementary](https://openaccess.thecvf.com/content/CVPR2023/supplemental/Chen_Human_Guided_Ground-Truth_CVPR_2023_supplemental.pdf) | [Arxiv Version](https://arxiv.org/abs/2303.13069)

> **Human Guided Ground-truth Generation for Realistic Image Super-resolution**  
> [Du CHEN*](https://github.com/ChrisDud0257), [Jie LIANG*](https://liangjie.xyz/), Xindong ZHANG, Ming LIU, Hui ZENG and [Lei ZHANG](https://www4.comp.polyu.edu.hk/~cslzhang/).  
> Accepted by CVPR 2023.  

- News (2024-01-08): We provide the [BaiduDrive](https://pan.baidu.com/s/1Z5pO-gwV12tK_yoKK6vg6A?pwd=3uai) Link towards the Training datset parts in HGGT.


## Abstract

[](https://github.com/ChrisDud0257/HGGT#abstract)

How to generate the ground-truth (GT) image is a critical issue for training realistic image super-resolution (Real- ISR) models. Existing methods mostly take a set of highresolution (HR) images as GTs and apply various degradations to simulate the low-resolution (LR) counterparts. Though great progress has been achieved, such an LR-HR pair generation scheme has several limitations. First, the perceptual quality of HR images may not be high enough, limiting the quality of Real-ISR model output. Second, existing schemes do not consider much human perception in GT generation, and the trained models tend to produce oversmoothed results or unpleasant artifacts. With the above considerations, we propose a human guided GT generation scheme. We first elaborately train multiple image enhancement models to improve the perceptual quality of HR images, and enable one LR image having multiple HR counterparts. Human subjects are then involved to annotate the high quality regions among the enhanced HR images as GTs, and label the regions with unpleasant artifacts as negative samples. A human guided GT image dataset with both positive and negative samples is then constructed, and a loss function is proposed to train the Real-ISR models. Experiments show that the Real-ISR models trained on our dataset can produce perceptually more realistic results with less artifacts.


