

In this story, a very classical super resolution technique, **Super-Resolution Convolutional Neural Network (SRCNN)** [1–2], is reviewed. In deep learning or convolutional neural network (CNN), we usually use CNN for image classification. In SRCNN, it is used for **single image super resolution (SR)** which is a classical problem in computer vision.


https://medium.com/coinmonks/review-srcnn-super-resolution-3cb3a4f67a7c