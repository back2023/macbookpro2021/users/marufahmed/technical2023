
https://github.com/shuwang127/SRCNN_Cpp

This project is an open source project of **"C++ Implementation of Super-Resolution resizing with Convolutional Neural Network"**.


### Introduction

This is an open source project from original of this: **SRCNN_Cpp** is a C++ Implementation of Image Super-Resolution using SRCNN which is proposed by Chao Dong in 2014.

- If you want to find the details of SRCNN algorithm, please read the paper:
    
    Chao Dong, Chen Change Loy, Kaiming He, Xiaoou Tang. Learning a Deep Convolutional Network for Image Super-Resolution, in Proceedings of European Conference on Computer Vision (ECCV), 2014
    
- If you want to download the training code(caffe) or test code(Matlab) for SRCNN, please open your browse and visit [http://mmlab.ie.cuhk.edu.hk/projects/SRCNN.html](http://mmlab.ie.cuhk.edu.hk/projects/SRCNN.html) for more details.

### What changed ?

1. Code modification from original [SRCNN](https://github.com/shuwang127/SRCNN_Cpp/tree/V1.0.0-2015).
2. OpenMP parallel processing, improved performance.
3. Supports almost of platform - POSIX compatibled.
    - MSYS2 and MinGW-W64
    - GCC of Linux
    - LLVM or CLANG of macOS, suporting universal binary build.




 