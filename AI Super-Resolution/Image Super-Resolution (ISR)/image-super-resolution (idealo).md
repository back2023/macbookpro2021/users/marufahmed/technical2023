Super-scale your images and run experiments with Residual Dense and Adversarial Networks.
https://github.com/idealo/image-super-resolution

The goal of this project is to upscale and improve the quality of low resolution images.

This project contains Keras implementations of different Residual Dense Networks for Single Image Super-Resolution (ISR) as well as scripts to train these networks using content and adversarial loss components.
