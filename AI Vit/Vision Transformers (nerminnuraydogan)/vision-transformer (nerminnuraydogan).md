

https://github.com/nerminnuraydogan/vision-transformer


# Vision Transformers

[](https://github.com/nerminnuraydogan/vision-transformer#vision-transformers)

The notebook presented in this repository contains a walk through of the Vision Transformer model with illustrations.

The notebook contains a step-by-step implementation of the paper '**An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale**'.


