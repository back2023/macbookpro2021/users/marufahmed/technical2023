
https://github.com/IceClear/StableSR


## Exploiting Diffusion Prior for Real-World Image Super-Resolution

[Paper](https://arxiv.org/abs/2305.07015) | [Project Page](https://iceclear.github.io/projects/stablesr/) | [Video](https://www.youtube.com/watch?v=5MZy9Uhpkw4) | [WebUI](https://github.com/pkuliyi2015/sd-webui-stablesr) | [ModelScope](https://modelscope.cn/models/xhlin129/cv_stablesr_image-super-resolution/summary)

[![google colab logo](https://camo.githubusercontent.com/84f0493939e0c4de4e6dbe113251b4bfb5353e57134ffd9fcab6b8714514d4d1/68747470733a2f2f636f6c61622e72657365617263682e676f6f676c652e636f6d2f6173736574732f636f6c61622d62616467652e737667)](https://colab.research.google.com/drive/11SE2_oDvbYtcuHDbaLAxsKk_o3flsO1T?usp=sharing) [![Hugging Face](https://camo.githubusercontent.com/833f957ae0391d63bce1284ea453d9472afc621088a65572222dd193cb563132/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f44656d6f2d25463025394625413425393725323048756767696e67253230466163652d626c7565)](https://huggingface.co/spaces/Iceclear/StableSR) [![Replicate](https://camo.githubusercontent.com/bf5b8dfc0c8b6c8273cb6f8bbf1ff34f78c6d63b7980d5958142359e6c946fcc/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f44656d6f2d2546302539462539412538302532305265706c69636174652d626c7565)](https://replicate.com/cjwbw/stablesr) [![OpenXLab](https://camo.githubusercontent.com/ac2c2020b7679b75220d708037e0df9018615264f199d941f01d28795d31968b/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f44656d6f2d2546302539462539302542432532304f70656e584c61622d626c7565)](https://openxlab.org.cn/apps/detail/Iceclear/StableSR) [![visitors](https://camo.githubusercontent.com/c362cfb2c6d3ed816f5b2094fe2734a72dd0cc249dbebe4f8a8948cf191a6a83/68747470733a2f2f76697369746f722d62616467652e6c616f62692e6963752f62616467653f706167655f69643d496365436c6561722f537461626c655352)](https://camo.githubusercontent.com/c362cfb2c6d3ed816f5b2094fe2734a72dd0cc249dbebe4f8a8948cf191a6a83/68747470733a2f2f76697369746f722d62616467652e6c616f62692e6963752f62616467653f706167655f69643d496365436c6561722f537461626c655352)

[Jianyi Wang](https://iceclear.github.io/), [Zongsheng Yue](https://zsyoaoa.github.io/), [Shangchen Zhou](https://shangchenzhou.com/), [Kelvin C.K. Chan](https://ckkelvinchan.github.io/), [Chen Change Loy](https://www.mmlab-ntu.com/person/ccloy/)

S-Lab, Nanyang Technological University








