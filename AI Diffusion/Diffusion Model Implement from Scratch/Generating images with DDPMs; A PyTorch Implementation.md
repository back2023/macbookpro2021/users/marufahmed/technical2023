
https://medium.com/@brianpulfer/enerating-images-with-ddpms-a-pytorch-implementation-cef5a2ba8cb1


Denoising Diffusion Probabilistic Models (**DDPM**) are deep generative models that are recently getting a lot of attention due to their impressive performances. Brand new models like OpenAI’s [**DALL-E 2**](https://cdn.openai.com/papers/dall-e-2.pdf) and Google’s [**Imagen**](https://arxiv.org/pdf/2205.11487.pdf) generators are based on DDPMs. They condition the generator on text such that it becomes then possible to generate photo-realistic images given an arbitrary string of text.