
https://github.com/basiralab/Dif-GSR


# Dif-GSR

This Repository contains the implementation of the paper "Dif-GSR" Diffusion-based Graph Super-resolution with Application to Connectomics. This paper is accepted as a MICCAI workshop paper (PRIME-MICCAI 2023).

Link to the paper: [https://link.springer.com/chapter/10.1007/978-3-031-46005-0_9](https://link.springer.com/chapter/10.1007/978-3-031-46005-0_9)

## [](https://github.com/basiralab/Dif-GSR#abstract)Abstract

The super-resolution of low-resolution brain graphs, also known as brain connectomes, is a crucial aspect of neuroimaging research, especially in brain graph super-resolution. Brain graph super-resolution revolutionized neuroimaging research by eliminating the need for costly acquisition and data processing. However, the development of generative models for super-resolving brain graphs remains largely unexplored. The state-of-the-art (SOTA) model in this domain leverages the inherent topology of brain connectomes by employing a Graph Generative Adversarial Network (GAN) coupled with topological feature-based regularization to achieve super-resolution. However, training graph-based GANs is notoriously challenging due to issues regarding scalability and implicit probability modelling. To overcome these limitations and fully capitalize on the capabilities of generative models, we propose Dif-GSR (Diffusion based Graph Super- Resolution) for predicting high-resolution brain graphs from low-resolution ones. Diffusion models have gained significant popularity in recent years as flexible and powerful frameworks for explicitly modelling complex data distributions. Dif-GSR consists of a noising process for adding noise to brain connectomes, a conditional denoiser model which learns to conditionally remove noise with respect to an input low-resolution source connectome and a sampling module which is responsible for the generation of high-resolution brain connectomes. We evaluate Dif-GSR using three-fold cross-validation using a variety of standard metrics for brain connectome super-resolution. We present the first diffusion-based framework for brain graph super-resolution, which is trained on non-isomorphic inter-modality brain graphs, effectively handling variations in graph size, distribution, and structure. This advancement holds promising prospects for multimodal and holistic brain mapping, as well as the development of a multimodal neurological disorder diagnostic frameworks. Our Dif-GSR code is available at [https://github.com/basiralab/Dif-GSR](https://github.com/basiralab/Dif-GSR).

 