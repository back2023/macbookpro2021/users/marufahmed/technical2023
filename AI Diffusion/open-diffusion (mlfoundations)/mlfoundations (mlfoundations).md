

https://github.com/mlfoundations/open-diffusion

# Simple at-scale training of Stable Diffusion

[](https://github.com/mlfoundations/open-diffusion#simple-at-scale-training-of-stable-diffusion)

- [Simple at-scale training of Stable Diffusion](https://github.com/mlfoundations/open-diffusion#simple-at-scale-training-of-stable-diffusion)
    - [Purpose](https://github.com/mlfoundations/open-diffusion#purpose)
    - [Usage](https://github.com/mlfoundations/open-diffusion#usage)
        - [Requirements](https://github.com/mlfoundations/open-diffusion#requirements)
        - [Configuration](https://github.com/mlfoundations/open-diffusion#configuration)
        - [Running a large-scale experiment](https://github.com/mlfoundations/open-diffusion#running-a-large-scale-experiment)
        - [Local Fine-tuning example](https://github.com/mlfoundations/open-diffusion#local-fine-tuning-example)
        - [Inference](https://github.com/mlfoundations/open-diffusion#inference)
        - [Examples](https://github.com/mlfoundations/open-diffusion#examples)
        - [Performance and System Requirements](https://github.com/mlfoundations/open-diffusion#performance-and-system-requirements)
    - [Recommendations for Large-Scale Training](https://github.com/mlfoundations/open-diffusion#recommendations-for-large-scale-training)
    - [Future features](https://github.com/mlfoundations/open-diffusion#future-features)
    - [Contributors](https://github.com/mlfoundations/open-diffusion#contributors)
    - [Acknowledgments](https://github.com/mlfoundations/open-diffusion#acknowledgments)

 