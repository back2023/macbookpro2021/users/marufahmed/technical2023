
https://medium.com/@ninadchaphekar/introduction-to-stochastic-differential-equations-for-score-based-diffusion-modelling-9b8e134f8e2c


I recently started studying about diffusion processes for generating images, for the course GNR 650, an advanced level course on concepts and application of deep learning in image processing and generation at IIT Bombay, Autumn’23. I came across a very interesting [paper](https://arxiv.org/abs/2011.13456) on how differential equations, in particular stochastic in nature are used to model such processes!

This write-up aims at **demystifying the raw mathematical and seemingly complicated nature** of the concept behind stochastic differential equations for diffusion processes. Beware, this might get a bit **math-y**, but bear with me to witness it’s beauty!


