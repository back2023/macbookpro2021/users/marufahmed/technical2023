

https://github.com/NVlabs/RED-diff


# Regularization by denoising diffusion process (RED-diff)

This is the README file for the implementaiton of variaitonal smapling in the paper [https://arxiv.org/abs/2305.04391](https://arxiv.org/abs/2305.04391). It provides an overview of the project, instructions for installation and usage, and other relevant information.

## [](https://github.com/NVlabs/RED-diff#description)Description

This codebase implements a variational posterior sampling method termed RED-diff. It is orginated from varaitional inference by minimizing the KL divergence between the data distribution p(x) and a variational approximaiton q(x). We show that this leads to score matching regulrization, and assuming dirac distribution for q(x), it yields very simple updates that resemble regulrization by denoising diffusion process.

This codebase includes experiments for various linear and nonlinear scenarios such as image inpainting, superresolution, Gaussian deblurring, motion debluring, high dynamic range (HDR), phase retreival, and nonlinear deblurring. We use Palette dataset for evaluation. The animation shows forward noising process ({$x_t$}) [left] and backward denoising prpcess ({$\mu_t$}) [right] based on RED-diff optimization.