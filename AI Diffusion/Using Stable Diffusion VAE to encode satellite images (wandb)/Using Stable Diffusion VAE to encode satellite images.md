
https://wandb.ai/capecape/ddpm_clouds/reports/Using-Stable-Diffusion-VAE-to-encode-satellite-images--VmlldzozNDA2OTgx


We encode our satellite images into latent space using Stable Diffusion VAE. Then we visualize the latents with a wandb.Table. Finally, we decode the latents back to image space, and surprisingly, we get back an almost lossless copy of the input.

> This means that we can train latent-diffusion models using this technique, saving huges amount of compute. Also, the encoding can be done offline (before training the diffusion pipeline).

