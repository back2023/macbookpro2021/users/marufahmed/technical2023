
https://udlbook.github.io/udlbook/

# Understanding Deep Learning

by Simon J.D. Prince  
Published by MIT Press Dec 5th 2023.  

- Download full PDF [here](https://github.com/udlbook/udlbook/releases/download/v.1.20/UnderstandingDeepLearning_16_1_24_C.pdf)
    
    2024-01-16. CC-BY-NC-ND license  
    ![download stats shield](https://img.shields.io/github/downloads/udlbook/udlbook/total)
- Order your copy from [here](https://mitpress.mit.edu/9780262048644/understanding-deep-learning/)
- Known errata can be found here: [PDF](https://github.com/udlbook/udlbook/raw/main/UDL_Errata.pdf)
- Report new errata via [github](https://github.com/udlbook/udlbook/issues) or contact me directly at udlbookmail@gmail.com
- Follow me on [Twitter](https://twitter.com/SimonPrinceAI) or [LinkedIn](https://www.linkedin.com/in/simon-prince-615bb9165/) for updates.

## Table of contents

- Chapter 1 - Introduction
- Chapter 2 - Supervised learning
- Chapter 3 - Shallow neural networks
- Chapter 4 - Deep neural networks
- Chapter 5 - Loss functions
- Chapter 6 - Training models
- Chapter 7 - Gradients and initialization
- Chapter 8 - Measuring performance
- Chapter 9 - Regularization
- Chapter 10 - Convolutional networks
- Chapter 11 - Residual networks
- Chapter 12 - Transformers
- Chapter 13 - Graph neural networks
- Chapter 14 - Unsupervised learning
- Chapter 15 - Generative adversarial networks
- Chapter 16 - Normalizing flows
- Chapter 17 - Variational autoencoders
- Chapter 18 - Diffusion models
- Chapter 19 - Deep reinforcement learning
- Chapter 20 - Why does deep learning work?
- Chapter 21 - Deep learning and ethics