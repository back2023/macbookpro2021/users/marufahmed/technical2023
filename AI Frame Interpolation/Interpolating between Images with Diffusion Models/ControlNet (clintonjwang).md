

https://github.com/clintonjwang/ControlNet


Fork of [ControlNet](https://github.com/lllyasviel/ControlNet) adapted for generating videos that interpolate between two arbitrary given images, as described in ["Interpolating between Images with Diffusion Models"](https://clintonjwang.github.io/interpolation). See installation instructions in the original ControlNet repository.

Images and prompts used in our paper are contained in `sample_imgs` and `sample_scripts`. Image copyrights belong to their original owners. You may get slightly different results as the random seed was not fixed.


