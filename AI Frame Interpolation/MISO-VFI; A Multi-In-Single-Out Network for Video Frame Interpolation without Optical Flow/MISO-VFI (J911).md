
https://github.com/J911/MISO-VFI


## About

Official implementation of "A Multi-In-Single-Out Network for Video Frame Interpolation without Optical Flow"


# MISO-VFI: A Multi-In-Single-Out Network  
for Video Frame Interpolation without Optical Flow

> We have decided to temporarily retract the code and arXiv submission as the manuscript was not clear, and we identified issues with the experimental evaluation method.

