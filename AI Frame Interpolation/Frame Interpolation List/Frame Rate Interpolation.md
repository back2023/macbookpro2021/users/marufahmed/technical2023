
https://www.reddit.com/r/nvidia/comments/kqhu8q/frame_rate_interpolation/

Hi folks, just wanted to bring to your attention a very useful CUDA app that uses AI to interpolate video frames. For those of you who, like me, have 15fps phone videos from 10 years ago, it can work wonders. I am not connected to the developer, just a user

[https://github.com/BurguerJohn/Dain-App/releases/tag/1.0](https://github.com/BurguerJohn/Dain-App/releases/tag/1.0)

UPDATE

I found an app that does the same thing, but many times faster. It's called FlowFrames, and you can find it here :

[https://nmkd.itch.io/flowframes](https://nmkd.itch.io/flowframes)

You also need to install Python version 3.8.6 (NOT later ones) from here :

[https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/)

And carry out some additional installs by following the instructions here :

[https://github.com/n00mkrad/flowframes/blob/main/PythonDependencies.md](https://github.com/n00mkrad/flowframes/blob/main/PythonDependencies.md)

On my i7 5820 machine with a 1080ti card and NVME disk/chip I get 10-12 fps of 720p x2 interpolation. If you are getting <1fps it's not using the GPU properly

