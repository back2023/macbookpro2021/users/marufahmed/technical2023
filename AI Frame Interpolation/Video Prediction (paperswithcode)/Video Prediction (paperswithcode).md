

https://paperswithcode.com/task/video-prediction


**Video Prediction** is the task of predicting future frames given past video frames.

Gif credit: [MAGVIT](https://magvit.cs.cmu.edu/)

Source: [Photo-Realistic Video Prediction on Natural Videos of Largely Changing Frames](https://arxiv.org/abs/2003.08635)


