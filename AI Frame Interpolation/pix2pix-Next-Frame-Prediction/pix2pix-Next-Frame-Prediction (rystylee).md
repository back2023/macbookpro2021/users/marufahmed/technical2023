

https://github.com/rystylee/pix2pix-Next-Frame-Prediction


## Description

[](https://github.com/rystylee/pix2pix-Next-Frame-Prediction#description)

`pix2pix-Next-Frame-Prediction` generates video by recursively generating images with pix2pix.  
We use the [memo's script](https://github.com/memo/webcam-pix2pix-tensorflow) for training and prediction.

training : [affinelayer/pix2pix-tensorflow](https://github.com/affinelayer/pix2pix-tensorflow)  
prediction : [memo/webcam-pix2pix-tensorflow](https://github.com/memo/webcam-pix2pix-tensorflow)


