


https://medium.com/@jctestud/video-generation-with-pix2pix-aed5b1b69f57


# How Can We Generate Videos For Fun?

Generative Machine Learning is cool, it is a fact. We can produce [fun text](https://towardsdatascience.com/yet-another-text-generation-project-5cfb59b26255), and do [all kinds of stuff](https://www.christies.com/features/A-collaboration-between-two-artists-one-human-one-a-machine-9332-1.aspx) with images. This includes now, generating as many high-resolution [cheeseburgers](https://arxiv.org/abs/1809.11096) as we want. Video data, however, is another beast.

To generate videos, the intuitive answer, technology-wise, would also be a GAN. Let’s say we want to generate more videos of cats. We could take all the cat videos from YouTube as training data, and make the generator and discriminator battle for ages until the former produces realistic cat videos.



