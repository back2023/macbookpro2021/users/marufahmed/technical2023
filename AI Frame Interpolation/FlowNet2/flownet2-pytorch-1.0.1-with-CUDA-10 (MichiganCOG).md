
https://github.com/MichiganCOG/flownet2-pytorch-1.0.1-with-CUDA-10

## About

Modified from NVIDIA/flownet2-pytorch with instructions and scripts to work with CUDA 10. Working as of August 2020.

# flownet2-pytorch

## [](https://github.com/MichiganCOG/flownet2-pytorch-1.0.1-with-CUDA-10#setup)Setup

```
# Install dependencies
conda create -p env
conda activate ./env
conda env update -f environment.yml
module load cuda/10.0  # Only needed for compilation
./install.sh

# Manually download FlowNet2 weights (https://drive.google.com/file/d/1hF8vS6YeHkx3j2pfCeQqqZGwA_PJq_Da/view?usp=sharing) into `weights` folder
```

## [](https://github.com/MichiganCOG/flownet2-pytorch-1.0.1-with-CUDA-10#new-inference)New inference

```
python -m src.main2
```

Pytorch implementation of [FlowNet 2.0: Evolution of Optical Flow Estimation with Deep Networks](https://arxiv.org/abs/1612.01925).

Multiple GPU training is supported, and the code provides examples for training or inference on [MPI-Sintel](http://sintel.is.tue.mpg.de/) clean and final datasets. The same commands can be used for training or inference with other datasets. See below for more detail.

Inference using fp16 (half-precision) is also supported.

For more help, type  

```
python main.py --help
```

## [](https://github.com/MichiganCOG/flownet2-pytorch-1.0.1-with-CUDA-10#network-architectures)Network architectures

Below are the different flownet neural network architectures that are provided.  
A batchnorm version for each network is also available.

- **FlowNet2S**
- **FlowNet2C**
- **FlowNet2CS**
- **FlowNet2CSS**
- **FlowNet2SD**
- **FlowNet2**

## [](https://github.com/MichiganCOG/flownet2-pytorch-1.0.1-with-CUDA-10#custom-layers)

