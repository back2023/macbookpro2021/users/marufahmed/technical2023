

https://github.com/xiye20/vptr


Video future frames prediction based on Transformers. Published on ICPR2022, [https://ieeexplore.ieee.org/abstract/document/9956707](https://ieeexplore.ieee.org/abstract/document/9956707)

# Video prediction by efficient transformers

[](https://github.com/xiye20/vptr#video-prediction-by-efficient-transformers)

Published on Image and Vision Computing. [https://arxiv.org/pdf/2212.06026.pdf](https://arxiv.org/pdf/2212.06026.pdf)

