

https://github.com/baowenbo/DAIN


# DAIN (Depth-Aware Video Frame Interpolation)

[Project](https://sites.google.com/view/wenbobao/dain) **|** [Paper](http://arxiv.org/abs/1904.00830)

[Wenbo Bao](https://sites.google.com/view/wenbobao/home), [Wei-Sheng Lai](http://graduatestudents.ucmerced.edu/wlai24/), [Chao Ma](https://sites.google.com/site/chaoma99/), Xiaoyun Zhang, Zhiyong Gao, and [Ming-Hsuan Yang](http://faculty.ucmerced.edu/mhyang/)

IEEE Conference on Computer Vision and Pattern Recognition, Long Beach, CVPR 2019

This work is developed based on our TPAMI work [MEMC-Net](https://github.com/baowenbo/MEMC-Net), where we propose the adaptive warping layer. Please also consider referring to it.

### [](https://github.com/baowenbo/DAIN#table-of-contents)Table of Contents

1. [Introduction](https://github.com/baowenbo/DAIN#introduction)
2. [Citation](https://github.com/baowenbo/DAIN#citation)
3. [Requirements and Dependencies](https://github.com/baowenbo/DAIN#requirements-and-dependencies)
4. [Installation](https://github.com/baowenbo/DAIN#installation)
5. [Testing Pre-trained Models](https://github.com/baowenbo/DAIN#testing-pre-trained-models)
6. [Downloading Results](https://github.com/baowenbo/DAIN#downloading-results)
7. [Slow-motion Generation](https://github.com/baowenbo/DAIN#slow-motion-generation)
8. [Training New Models](https://github.com/baowenbo/DAIN#training-new-models)
9. [Google Colab Demo](https://github.com/baowenbo/DAIN#google-colab-demo)

