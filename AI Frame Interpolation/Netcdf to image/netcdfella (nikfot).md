
https://github.com/nikfot/netcdfella

## About The Project

Netcdfella is a command line interface (cli) tool for converting netcdf files to ASCII and JPG/PNG format, as well as creating graphs based on the vectors of the file.

Netcdfella supports two ways for converting documents at the time being:

- single/multi convert
- conversion upon creation

During the run of the cli the output types, the mapping dimension and variable can be selected aling with other options.


