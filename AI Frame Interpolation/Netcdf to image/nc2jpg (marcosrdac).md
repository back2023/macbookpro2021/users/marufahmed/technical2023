
https://github.com/marcosrdac/nc2jpg


# nc2jpg

This program exports a NetCDF file variable to a JPEG file (values are scaled to fit between 0-255). JPEG quality can be chosen. This code is meant to ease posterior creation of handmade masks for a NetCDF variable with an image editor like _GIMP_.


