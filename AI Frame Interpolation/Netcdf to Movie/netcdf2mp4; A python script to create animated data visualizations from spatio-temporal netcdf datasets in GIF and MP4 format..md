
https://github.com/johannesuhl/netcdf2mp4

This script uses 
- `gdal` to read NetCDF data, 
- uses `matplotlib` to plot the data for each point in time, and 
- uses `imageio` to create an animated GIF. 
- Finally, it uses `moviepy` to convert the animated gif to MP4 video format, which will reduce file size. 
- For example, the below GIF is 29MB, and the corresponding MP4 file is less than 3MB.


https://github.com/johannesuhl/netcdf2mp4/blob/main/netcdf2mp4.py
