
https://github.com/sniklaus/softmax-splatting

# softmax-splatting

This is a reference implementation of the softmax splatting operator, which has been proposed in Softmax Splatting for Video Frame Interpolation [1], using PyTorch. Softmax splatting is a well-motivated approach for differentiable forward warping. It uses a translational invariant importance metric to disambiguate cases where multiple source pixels map to the same target pixel. Should you be making use of our work, please cite our paper [1].


For our previous work on SepConv, see: [https://github.com/sniklaus/revisiting-sepconv](https://github.com/sniklaus/revisiting-sepconv)
 