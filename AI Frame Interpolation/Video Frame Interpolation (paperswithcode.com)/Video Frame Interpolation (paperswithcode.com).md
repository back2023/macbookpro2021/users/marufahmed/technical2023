

https://paperswithcode.com/task/video-frame-interpolation


# Video Frame Interpolation

93 papers with code • 20 benchmarks • 11 datasets

The goal of **Video Frame Interpolation** is to synthesize several frames in the middle of two adjacent frames of the original video. Video Frame Interpolation can be applied to generate slow motion video, increase video frame rate, and frame recovery in video streaming.

Source: [Reducing the X-ray radiation exposure frequency in cardio-angiography via deep-learning based video interpolation](https://arxiv.org/abs/2006.00781)


