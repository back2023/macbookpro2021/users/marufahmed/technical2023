
https://github.com/lllyasviel/ControlNet?tab=readme-ov-file
# News: A nightly version of ControlNet 1.1 is released!

[ControlNet 1.1](https://github.com/lllyasviel/ControlNet-v1-1-nightly) is released. Those new models will be merged to this repo after we make sure that everything is good.

# [](https://github.com/lllyasviel/ControlNet?tab=readme-ov-file#below-is-controlnet-10)Below is ControlNet 1.0

Official implementation of [Adding Conditional Control to Text-to-Image Diffusion Models](https://arxiv.org/abs/2302.05543).

ControlNet is a neural network structure to control diffusion models by adding extra conditions.