

https://github.com/mafiosnik777/enhancr

## About

Video Frame Interpolation & Super Resolution using NVIDIA's TensorRT & Tencent's NCNN inference, beautifully crafted and packaged into a single app

[dsc.gg/enhancr](https://dsc.gg/enhancr "https://dsc.gg/enhancr")


