

https://towardsdatascience.com/unsupervised-learning-for-data-interpolation-e259cf5dc957


Real-world data are noisy. Noise can come in the form of incorrect or missing values. This article describes a way of filling missing values with auto-encoders. An auto-encoder is trained on noisy data with no reference values available for missing entries. The procedure is explained on the task of image reconstruction. The method has been successfully applied to [seismic data reconstruction](http://earthdoc.eage.org/publication/publicationdetails/?publication=92298) in the past. Tensorflow implementation is available [**here**](https://github.com/mikhailiuk/image_reconstruction)**.**


