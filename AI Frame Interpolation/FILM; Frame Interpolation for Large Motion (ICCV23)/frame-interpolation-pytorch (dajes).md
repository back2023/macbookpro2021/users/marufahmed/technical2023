

https://github.com/dajes/frame-interpolation-pytorch


This is an unofficial PyTorch inference implementation of [FILM: Frame Interpolation for Large Motion, In ECCV 2022](https://film-net.github.io/).  
[Original repository link](https://github.com/google-research/frame-interpolation)

The project is focused on creating simple and TorchScript compilable inference interface for the original pretrained TF2 model.


