
### About changing longitude array from 0 - 360 to -180 to 180 with Python xarray
https://stackoverflow.com/questions/53345442/about-changing-longitude-array-from-0-360-to-180-to-180-with-python-xarray

"Another faster approach and much simpler approach without using `where` would be"
```python
 df.coords['lon'] = (df.coords['lon'] + 180) % 360 - 180
 df = df.sortby(df.lon)
```

***

### Updated: How to create an xarray dataset from scratch, reproject and save 
2022-02-26
(https://www.theurbanist.com.au/2022/02/updated-how-to-create-an-xarray-dataset-from-scratch-reproject-and-save/)

```python
#### timeseries dataset ####

times = pd.date_range(start='2000-01-01',freq='1H',periods=6)

# create dataset
ds = xr.Dataset({
    'data1': xr.DataArray(
                data   = np.random.random(6),   # enter data here
                dims   = ['time'],
                coords = {'time': times},
                attrs  = {'long_name': 'random data', 'units': 'm'}
                ),
    'data2': xr.DataArray(
                data   = np.random.random(6),   # enter data here
                dims   = ['time'],
                coords = {'time': times},
                attrs  = {'long_name': 'random data', 'units': 'm'}
                )
            },
        attrs = {'example_attr': 'this is a global attribute'}
    )
```

```python
#### spatial dataset ####

lats = np.linspace(-33,-34,101)
lons = np.linspace(150,151,101)

ds = xr.Dataset({
    'data1': xr.DataArray(
        data = np.random.rand(101,101),
        dims = ['latitude','longitude'],
        coords = {'latitude': lats, 'longitude': lons},
        attrs = {'long_name': 'random data', 'units': 'm'}
        ),
    'data2': xr.DataArray(
        data = np.random.rand(101,101),
        dims = ['latitude','longitude'],
        coords = {'latitude': lats, 'longitude': lons},
        attrs = {'long_name': 'random data', 'units': 'm'}
        )
    },
    attrs = {'example_attr': 'this is a global attribute'}
    )
```


### Intermediate Python III: Xarray for Multidimensional Data
https://rabernat.github.io/research_computing/xarray.html 
"Permalink to Intermediate Python III: Xarray for Multidimensional Data"


