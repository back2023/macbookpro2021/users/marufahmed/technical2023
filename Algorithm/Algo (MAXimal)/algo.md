
There are 145 algorithms presented here. Brief descriptions and C++ programs are given for all algorithms.

---

Show: [last added](https://e-maxx.ru/algo/index.php?mod=lastadded) , [last edited](https://e-maxx.ru/algo/index.php?mod=lastedited) algorithms.

---

You can also download [the PDF book](https://e-maxx.ru/upload/e-maxx_algo.pdf) , which has all the articles in one large file.

---

https://e-maxx.ru/algo/


###  the PDF book
https://e-maxx.ru/upload/e-maxx_algo.pdf





