
https://www.nvidia.com/en-us/on-demand/session/gtcspring21-s31115/

Branch-and-Bound (B&B) algorithms are the key technique to optimally solve combinatorics optimization problems, with vast applications in such areas as machine learning, clustering/classification, feature selection, manufacturing/production, graph theory, network analytics, transportation/supply chain, and more. Even though B&Bs are deterministic algorithms resulting in the best possible solution (global optimum), those are not tractable to satisfy business time constraints. However, the parallelization could be a suitable alternative to cope with this challenge. The literature around the parallel implementation of B&B, more specifically its parallelization on GPUs, is not significant because of B&B’s irregular data structures and poor computation/communication ratio. We'll investigate the feasibility and new capabilities of RAPIDS, a recent NVIDIA CUDA-based data science suite, to speed up the B&B algorithms in conjunction with a few recent theoretical advances.


