
The programming languages and machine learning communities have, over the last few years, developed a shared set of research interests under the umbrella of [_probabilistic programming_](http://probabilistic-programming.org/wiki/Home). The idea is that we might be able to “export” powerful PL concepts like abstraction and reuse to statistical modeling, which is currently an arcane and arduous task.

(You may want to read the [most recent version](http://adriansampson.net/doc/ppl.html) of these lecture notes. The source is [on GitHub](https://github.com/sampsyo/ppl-intro). If you notice any mistakes, please open a pull request.)


https://www.cs.cornell.edu/courses/cs4110/2016fa/lectures/lecture33.html