
# Welcome[](https://bayesiancomputationbook.com/welcome.html#welcome "Permalink to this heading")

Welcome to the online version [Bayesian Modeling and Computation in Python](https://www.routledge.com/Bayesian-Modeling-and-Computation-in-Python/Martin-Kumar-Lao/p/book/9780367894368). If you’d like a physical copy it can purchased [from the publisher here](https://www.routledge.com/Bayesian-Modeling-and-Computation-in-Python/Martin-Kumar-Lao/p/book/9780367894368) or on Amazon.

https://bayesiancomputationbook.com/welcome.html


### 10. Probabilistic Programming Languages
https://bayesiancomputationbook.com/markdown/chp_10.html
