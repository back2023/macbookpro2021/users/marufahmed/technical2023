
CuPy covers the full Fast Fourier Transform (FFT) functionalities provided in NumPy ([`cupy.fft`](https://docs.cupy.dev/en/stable/reference/fft.html#module-cupy.fft "cupy.fft")) and a subset in SciPy ([`cupyx.scipy.fft`](https://docs.cupy.dev/en/stable/reference/scipy_fft.html#module-cupyx.scipy.fft "cupyx.scipy.fft")). In addition to those high-level APIs that can be used as is, CuPy provides additional features to

1. access advanced routines that [cuFFT](https://docs.nvidia.com/cuda/cufft/index.html) offers for NVIDIA GPUs,
2. control better the performance and behavior of the FFT routines.

Some of these features are _experimental_ (subject to change, deprecation, or removal, see [API Compatibility Policy](https://docs.cupy.dev/en/stable/user_guide/compatibility.html)) or may be absent in [hipFFT](https://hipfft.readthedocs.io/en/latest/)/[rocFFT](https://rocfft.readthedocs.io/en/latest/) targeting AMD GPUs.

https://docs.cupy.dev/en/stable/user_guide/fft.html