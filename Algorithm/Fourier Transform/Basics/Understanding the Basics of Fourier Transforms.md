
My aim for this post is to start things off with a refresher on the basics of the math behind the Fourier transformation, and lay the foundation for future posts that will go into more detail on how the Fourier transform should be used and interpreted.

https://blog.endaq.com/fourier-transform-basics