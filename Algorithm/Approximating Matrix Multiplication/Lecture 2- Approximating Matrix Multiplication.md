
### 2 Approximating Matrix Multiplication

Approximating the product of two matrices with random sampling or random projection methods is a fundamental operation that is of interest in and of itself as well as since it is used in a critical way as a primitive for many RandNLA algorithms. In this class, we will introduce a basic algorithm; and in this and the next few classes, we will discusses several related methods. Here is the reading for today. 

• Drineas, Kannan, and Mahoney, “Fast Monte Carlo Algorithms for Matrices I: Approximating Matrix Multiplication”

https://www.stat.berkeley.edu/~mmahoney/f13-stat260-cs294/Lectures/lecture02.pdf