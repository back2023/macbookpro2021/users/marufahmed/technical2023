

https://github.com/cormacpayne/Data-Structures-and-Algorithms/blob/master/dynamic-programming/bitmask-dynamic-programming/bitmask-dynamic-programming.md





### Take II - Bitmask DP

[](https://github.com/cormacpayne/Data-Structures-and-Algorithms/blob/master/dynamic-programming/bitmask-dynamic-programming/bitmask-dynamic-programming.md#take-ii---bitmask-dp)

Let's use bitmasks to help us with this problem: we'll construct a bitmask of length _N_.

What does the bit at index _k_ represent in the scope of the problem? What does it mean for the bit to be zero? What about one?

- if the bit at index _k_ is zero, then we _have not yet_ visited city _k_
- if the bit at index _k_ is one, then we _have_ visited city _k_

For a given bitmask, we can see which cities we have left to travel to by checking which bits have a value of zero.


