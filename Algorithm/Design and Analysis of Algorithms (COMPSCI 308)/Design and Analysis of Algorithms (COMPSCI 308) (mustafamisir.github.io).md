
https://mustafamisir.github.io/compsci308-f2022-s2.html
## Fall 2022-2023 / Session 2 (7 weeks, 35 hours)

### Course Period: October 24 - December 8, 2022

- **Lectures**: Monday / Tuesday / Wednesday / Thursday @ 16:15-17:30 (**Classroom**: IB 1053 + Zoom)

**Instructor**: [Mustafa MISIR](http://mustafamisir.github.io/) (**Office**: CC 3019), [mustafa.misir [at] dukekunshan.edu.cn](mailto:mustafa.misir%20[at]%20dukekunshan.edu.cn)   /   [mm940 [at] duke.edu](mailto:mm940%20[at]%20duke.edu)  
**Teaching Assistant**: [TBA](https://mustafamisir.github.io/compsci308-f2022-s2.html#) (**Office**: TBA), TBA





