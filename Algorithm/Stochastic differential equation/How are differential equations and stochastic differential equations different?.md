


From a cursory skim of the PyMC3 documentation you have cited, there are no stochastic differential equations involved, you are just doing Bayesian statistical inference on the "parameters" of the ordinary differential equations of SIR to infer a posterior.


https://stats.stackexchange.com/questions/520190/how-are-differential-equations-and-stochastic-differential-equations-different


