
### Scope of the project

The objective of this project is to develop a GPU-accelerated implementation of the Smith-Waterman algorithm, an optimal method for locally aligning pairs of sequences. The implementation focuses on processing 1000 sequence pairs, where the sequences have a length of 512, and conducting the necessary backtracing operations.

The [report](https://github.com/agnesetaluzzi/GPU101-project/blob/main/docs/report.pdf) contains additional information about the Smith-Waterman algorithm, a comprehensive explanation of my implementation, as well as performance and profiling data.

https://github.com/agnesetaluzzi/GPU101-project

