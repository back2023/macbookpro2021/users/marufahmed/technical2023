

A GPU Accelerated (CUDA) implementation of the Smith Waterman algorithm for DNA sequence alignment.

https://github.com/romilbhardwaj/cudasmithwaterman

