
**Boolean satisfiability** is a **NP-complete problem** but, a special case of it can be solved in **polynomial time**. This special case is called cas **2-SAT or 2-Satisfiability**.

https://iq.opengenus.org/2-sat/