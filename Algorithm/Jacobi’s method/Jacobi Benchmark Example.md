
#### Description

Jacobi relaxation is an iterative algorithm which, given a set of boundary conditions, finds discretized solutions to differential equations of the form ![tex2html_wrap_in
line73](https://groups.csail.mit.edu/cag/raw/benchmark/doc/img6.gif). Each step of the algorithm replaces each node of a grid with the average of the values of its nearest neighbors. We have implemented an integer version of Jacobi relaxation. A computation element representing each grid point simply produces an average of its four inputs. We use the RawCS generator to generate connections between each computation element and its four neighbors. The resulting circuit has a simple regular grid structure, reflective of the data dependence graph of the algorithm. [Here](https://groups.csail.mit.edu/cag/raw/benchmark/suites/jacobi/doc/jacobi.ps.gz) is a postscript excerpt furthering describing Jacobi from problem set 1 of the 6.846 class at MIT in 1992.



https://groups.csail.mit.edu/cag/raw/benchmark/suites/jacobi/README.html

