
http://math.utoledo.edu/~dgajews/perms/5095-15763-1-PB.pdf


### Abstract 
The most challenging task dealing with permutation is when the element is large. In this paper, a new algorithm for listing down all permutations for n elements is developed based on distinct starter sets. Once the starter sets are obtained, each starter set is then cycled to obtain the first half of distinct permutations. The complete list of permutations is achieved by reversing the order of the first half of permutation. The new algorithm has advantages over the other methods due to its simplicity and easy to use.


