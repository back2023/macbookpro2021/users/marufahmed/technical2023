

Lecturer: Aleks Ignjatovic ([ignjat@cse.unsw.edu.au](mailto:ignjat@cse.unsw.edu.au); office: 504 in K-17)
Office hours:  by appointment, send me a Zoom invite
Lectures: TBA
Lecture recordings: YouTube 
[https://www.youtube.com/playlist?list=PLtKL_m5pPpdjFZKrSEDQORnSq2WrgjOw8](https://www.youtube.com/playlist?list=PLtKL_m5pPpdjFZKrSEDQORnSq2WrgjOw8)
You can self-sign up for our forum at
[https://edstem.org/au/join/CkDhpY](https://edstem.org/au/join/CkDhpY)
myExperience feedback, closes 25/11/2021 at midnight  [https://myexperience.unsw.edu.au/](https://myexperience.unsw.edu.au/)
http://www.cse.unsw.edu.au/~cs4121/

