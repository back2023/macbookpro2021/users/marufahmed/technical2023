Python implementation of a number of algorithms, some of which (but not all) are homeworks taken from the [Coursera Specialization](https://www.coursera.org/specializations/algorithms) by Tim Roughgarden at Stanford.

https://github.com/jazracherif/algorithms/tree/master

