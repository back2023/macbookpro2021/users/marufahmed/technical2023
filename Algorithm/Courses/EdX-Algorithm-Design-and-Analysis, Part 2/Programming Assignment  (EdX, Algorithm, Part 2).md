## Week 1
#### Question 1 & 2
completionJob1.py
https://github.com/jiankang1991/coursera_algorithms/blob/master/job_schedule.py

schedule.ipynb
https://github.com/jazracherif/algorithms/blob/master/greedy/schedule.ipynb
Greedy Algorithms
https://notebook.community/jazracherif/algorithms/greedy/schedule

#### Question 3
https://github.com/jazracherif/algorithms/blob/master/greedy/prim.ipynb

Minimum spanning tree
Egor Ignatenkov
23.03.2015
https://rstudio-pubs-static.s3.amazonaws.com/68800_142ed05b1c0741c18da00431ba1bdbf5.html

## Week 2
https://github.com/harshitkgupta/Algorithms-Design-and-Analysis-Part-2/blob/master/assignments/prog2.pdf

###### dbarabanov
question_1.py
https://github.com/dbarabanov/coursera/blob/master/algorithms_2/assignment_2/question_1.py
question_2.py (Worked with 2^24 index)
https://github.com/dbarabanov/coursera/blob/master/algorithms_2/assignment_2/question_2.py

###### AkeelAli 
(Running for hours, then result is 7115)
https://github.com/AkeelAli/coursera-algorithms-2/blob/master/programming_assignment_2/question2.py

###### max-spacing k-clustering
https://notebook.community/jazracherif/algorithms/max-spacing-k-clustering/max-spacing-k-clustering

###### Clustering on big graph, python
https://rstudio-pubs-static.s3.amazonaws.com/72033_dcd43db591574873aac22be4cde29af6.html

###### Stanford Algorithms: Design and Analysis, Part 2[week 2]
https://blog.csdn.net/zhaoxinfan/article/details/11707797


## Week 3
https://github.com/dbarabanov/coursera/blob/master/algorithms_2/assignment_3/knapsack_1.py


## Week 4
`all-pairs-shortest-path.py`
https://github.com/netusco/algorithms-2/blob/master/all-pairs-shortest-path.py

`4wk1_a1.py`
https://github.com/zhangqi-chen/Algorithms-Stanford/blob/master/4%20Shortest%20Paths%20Revisited%2C%20NP-Complete%20Problems%20and%20What%20To%20Do%20About%20Them/Week%201/4wk1_a1.py

algorithms/course4/week1
Part 29: The Bellman-Ford Algorithm
Part 30: All-Pairs Shortest Paths
https://github-com.translate.goog/y-meguro/algorithms/tree/master/course4/week1?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en-US&_x_tr_pto=wapp
https://github.com/y-meguro/algorithms/tree/master/course4/week1

netusco/algorithms-2
https://github.com/netusco/algorithms-2/blob/master/all-pairs-shortest-path.py

chubbypanda/learning
https://github.com/chubbypanda/learning/blob/master/courses/algo_analysis_II/hw4_apsp/README.md

## Week 5

https://ideone.com/fgMeh7

Traveling Salesman Problem
https://notebook.community/jazracherif/algorithms/tsp/tsp

travelling salesman problem - dynamic programming algorithm implementation in python
https://gist.github.com/kavyakalidindi/5eddebc2f7c3f03ca921582ede3760da

ayamahmod/algorithms-stanford-university-coursera/blob/master/course-4/prog-assign-2.py
https://github.com/ayamahmod/algorithms-stanford-university-coursera/blob/master/course-4/prog-assign-2.py
(Tested, takes an hour but works)

### Week 6

https://github.com/harshitkgupta/Algorithms-Design-and-Analysis-Part-2/blob/master/assignments/prog6.pdf
