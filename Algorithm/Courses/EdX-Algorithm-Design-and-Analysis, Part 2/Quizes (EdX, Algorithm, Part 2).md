### Week 1
Stanford Algorithms Design and Analysis Part 2 week 1
https://codeantenna.com/a/pseoftZefm

### Week 2
Stanford Algorithms: Design and Analysis, Part 2 week 2 
https://blog.csdn.net/zhaoxinfan/article/details/11707797
Problem Set 2
https://blogs.asarkar.com/algorithms-design-analysis-2/set-2/

https://github.com/harshitkgupta/Algorithms-Design-and-Analysis-Part-2/blob/master/quiz/quiz2.pdf

### Week 4
https://github.com/harshitkgupta/Algorithms-Design-and-Analysis-Part-2/blob/master/quiz/quiz4.pdf

https://blogs.asarkar.com/algorithms-design-analysis-2/set-4/

### Week 5
https://github.com/harshitkgupta/Algorithms-Design-and-Analysis-Part-2/blob/master/quiz/quiz5.pdf

https://blogs.asarkar.com/algorithms-design-analysis-2/set-5/

### Week 6
https://github.com/harshitkgupta/Algorithms-Design-and-Analysis-Part-2/blob/master/quiz/quiz6.pdf

https://blogs.asarkar.com/algorithms-design-analysis-2/set-6/


