
Last update: January 4, 2023 

# Algorithms for Competitive Programming


The goal of this project is to translate the wonderful resource [https://e-maxx.ru/algo](https://e-maxx.ru/algo) which provides descriptions of many algorithms and data structures especially popular in field of competitive programming. Moreover we want to improve the collected knowledge by extending the articles and adding new articles to the collection.

Compiled pages are published at [https://cp-algorithms.com/](https://cp-algorithms.com/).


