

https://algorist.com/algorist.html


This page provides a comprehensive collection of algorithm implementations for seventy-five of the most fundamental problems in combinatorial algorithms. The problem taxonomy, implementations, and supporting material are all drawn from my book [The Algorithm Design Manual](http://www.algorist.com/). Since the practical person is more often looking for a program than an algorithm, we provide pointers to solid implementations of useful algorithms when they are available.



## By Problems

[**Data Structures**](https://algorist.com/sections/Data_Structures.html)

[Dictionaries,](https://algorist.com/problems/Dictionaries.html) [Priority Queues,](https://algorist.com/problems/Priority_Queues.html) [Suffix Trees and Arrays,](https://algorist.com/problems/Suffix_Trees_and_Arrays.html) [Graph Data Structures,](https://algorist.com/problems/Graph_Data_Structures.html) [Set Data Structures,](https://algorist.com/problems/Set_Data_Structures.html) [Kd-Trees](https://algorist.com/problems/Kd-Trees.html)  
  

[**Numerical Problems**](https://algorist.com/sections/Numerical_Problems.html)

[Solving Linear Equations,](https://algorist.com/problems/Solving_Linear_Equations.html) [Bandwidth Reduction,](https://algorist.com/problems/Bandwidth_Reduction.html) [Matrix Multiplication,](https://algorist.com/problems/Matrix_Multiplication.html) [Determinants and Permanents,](https://algorist.com/problems/Determinants_and_Permanents.html) [Constrained and Unconstrained Optimization,](https://algorist.com/problems/Constrained_and_Unconstrained_Optimization.html) [Linear Programming,](https://algorist.com/problems/Linear_Programming.html) [Random Number Generation,](https://algorist.com/problems/Random_Number_Generation.html) [Factoring and Primality Testing,](https://algorist.com/problems/Factoring_and_Primality_Testing.html) [Arbitrary-Precision Arithmetic,](https://algorist.com/problems/Arbitrary-Precision_Arithmetic.html) [Knapsack Problem,](https://algorist.com/problems/Knapsack_Problem.html) [Discrete Fourier Transform](https://algorist.com/problems/Discrete_Fourier_Transform.html)  
  

[**Combinatorial Problems**](https://algorist.com/sections/Combinatorial_Problems.html)

[Sorting,](https://algorist.com/problems/Sorting.html) [Searching,](https://algorist.com/problems/Searching.html) [Median and Selection,](https://algorist.com/problems/Median_and_Selection.html) [Generating Permutations,](https://algorist.com/problems/Generating_Permutations.html) [Generating Subsets,](https://algorist.com/problems/Generating_Subsets.html) [Generating Partitions,](https://algorist.com/problems/Generating_Partitions.html) [Generating Graphs,](https://algorist.com/problems/Generating_Graphs.html) [Calendrical Calculations,](https://algorist.com/problems/Calendrical_Calculations.html) [Job Scheduling,](https://algorist.com/problems/Job_Scheduling.html) [Satisfiability](https://algorist.com/problems/Satisfiability.html)  
  

[**Graph: Polynomial-time Problems**](https://algorist.com/sections/Graph_Problems_Polynomial.html)

[Connected Components,](https://algorist.com/problems/Connected_Components.html) [Topological Sorting,](https://algorist.com/problems/Topological_Sorting.html) [Minimum Spanning Tree,](https://algorist.com/problems/Minimum_Spanning_Tree.html) [Shortest Path,](https://algorist.com/problems/Shortest_Path.html) [Transitive Closure and Reduction,](https://algorist.com/problems/Transitive_Closure_and_Reduction.html) [Matching,](https://algorist.com/problems/Matching.html) [Eulerian Cycle/Chinese Postman,](https://algorist.com/problems/Eulerian_Cycle_Chinese_Postman.html) [Edge and Vertex Connectivity,](https://algorist.com/problems/Edge_and_Vertex_Connectivity.html) [Network Flow,](https://algorist.com/problems/Network_Flow.html) [Drawing Graphs Nicely,](https://algorist.com/problems/Drawing_Graphs_Nicely.html) [Drawing Trees,](https://algorist.com/problems/Drawing_Trees.html) [Planarity Detection and Embedding](https://algorist.com/problems/Planarity_Detection_and_Embedding.html)  
  

[**Graph: Hard Problems**](https://algorist.com/sections/Graph_Problems_Hard.html)

[Clique,](https://algorist.com/problems/Clique.html) [Independent Set,](https://algorist.com/problems/Independent_Set.html) [Vertex Cover,](https://algorist.com/problems/Vertex_Cover.html) [Traveling Salesman Problem,](https://algorist.com/problems/Traveling_Salesman_Problem.html) [Hamiltonian Cycle,](https://algorist.com/problems/Hamiltonian_Cycle.html) [Graph Partition,](https://algorist.com/problems/Graph_Partition.html) [Vertex Coloring,](https://algorist.com/problems/Vertex_Coloring.html) [Edge Coloring,](https://algorist.com/problems/Edge_Coloring.html) [Graph Isomorphism,](https://algorist.com/problems/Graph_Isomorphism.html) [Steiner Tree,](https://algorist.com/problems/Steiner_Tree.html) [Feedback Edge/Vertex Set](https://algorist.com/problems/Feedback_Edge_Vertex_Set.html)  
  

[**Computational Geometry**](https://algorist.com/sections/Computational_Geometry.html)

[Robust Geometric Primitives,](https://algorist.com/problems/Robust_Geometric_Primitives.html) [Convex Hull,](https://algorist.com/problems/Convex_Hull.html) [Triangulation,](https://algorist.com/problems/Triangulation.html) [Voronoi Diagrams,](https://algorist.com/problems/Voronoi_Diagrams.html) [Nearest Neighbor Search,](https://algorist.com/problems/Nearest_Neighbor_Search.html) [Range Search,](https://algorist.com/problems/Range_Search.html) [Point Location,](https://algorist.com/problems/Point_Location.html) [Intersection Detection,](https://algorist.com/problems/Intersection_Detection.html) [Bin Packing,](https://algorist.com/problems/Bin_Packing.html) [Medial-Axis Transform,](https://algorist.com/problems/Medial-Axis_Transform.html) [Polygon Partitioning,](https://algorist.com/problems/Polygon_Partitioning.html) [Simplifying Polygons,](https://algorist.com/problems/Simplifying_Polygons.html) [Shape Similarity,](https://algorist.com/problems/Shape_Similarity.html) [Motion Planning,](https://algorist.com/problems/Motion_Planning.html) [Maintaining Line Arrangements,](https://algorist.com/problems/Maintaining_Line_Arrangements.html) [Minkowski Sum](https://algorist.com/problems/Minkowski_Sum.html)  
  

[**Set and String Problems**](https://algorist.com/sections/Set_and_String_Problems.html)

[Set Cover,](https://algorist.com/problems/Set_Cover.html) [Set Packing,](https://algorist.com/problems/Set_Packing.html) [String Matching,](https://algorist.com/problems/String_Matching.html) [Approximate String Matching,](https://algorist.com/problems/Approximate_String_Matching.html) [Text Compression,](https://algorist.com/problems/Text_Compression.html) [Cryptography,](https://algorist.com/problems/Cryptography.html) [Finite State Machine Minimization,](https://algorist.com/problems/Finite_State_Machine_Minimization.html) [Longest Common Substring/Subsequence,](https://algorist.com/problems/Longest_Common_Substring.html) [Shortest Common Superstring](https://algorist.com/problems/Shortest_Common_Superstring.html)  
  

---

## By Language

**[C,](https://algorist.com/languages/C.html) [Ada,](https://algorist.com/languages/Ada.html) [Binary,](https://algorist.com/languages/Binary.html) [C++,](https://algorist.com/languages/C++.html) [C#,](https://algorist.com/languages/CSharp.html) [Fortran,](https://algorist.com/languages/Fortran.html) [Go,](https://algorist.com/languages/Go.html) [Java,](https://algorist.com/languages/Java.html) [JavaScript,](https://algorist.com/languages/JavaScript.html) [Lisp,](https://algorist.com/languages/Lisp.html) [Mathematica,](https://algorist.com/languages/Mathematica.html) [Pascal,](https://algorist.com/languages/Pascal.html) [PHP,](https://algorist.com/languages/PHP.html) [Python](https://algorist.com/languages/Python.html)**

---