[![Ruprecht-Karls-Universität Heidelberg](http://comopt.ifi.uni-heidelberg.de/images/schriftzug_uni_heidelberg.gif)](http://www.uni-heidelberg.de/index.html)

http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/

### TSPLIB is a library of sample instances for the TSP (and related problems) from various sources and of various types.

Note that it is not intended to add further problems instances (1.1.2013)


