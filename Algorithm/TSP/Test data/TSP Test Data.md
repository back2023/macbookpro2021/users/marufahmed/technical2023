https://www.math.uwaterloo.ca/tsp/data/index.html


Throughout the history of the TSP, researchers have relied on the availability of standard test instances to measure the progress of proposed solution methods. Early research papers by Dantzig, Fulkerson, and Johnson (1954), Held and Karp (1962), and Karg and Thompson (1964) included distance matrices for their main test instances, but this quickly became infeasible as the size of the studied problems grew over time. In its place, TSP researchers made their test instances available as computer-readable files stored on tape, punchcards or other media. In 1990, Gerhard Reinelt collected many of these test instances together with a wide collection of new examples drawn from industrial applications and from geographic problems featuring the locations of cities on maps. Reinelt's library is called the [TSPLIB](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/); it contains over 100 examples with sizes from 14 cities up to 85,900 cities.

To provide additional tests for the TSP, particularly for large instances, we have made available the following sets of examples (all stored in TSPLIB format).


