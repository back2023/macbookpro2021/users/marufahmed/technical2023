

https://www.quora.com/How-many-cities-can-the-most-powerful-exact-algorithm-of-traveling-salesman-problem-solve-Are-there-any-recent-references


I believe the largest exact solution remains the 85,900-city tour from [TSPLIB](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/ "comopt.ifi.uni-heidelberg.de") ([pla85900](http://www.math.uwaterloo.ca/tsp/pla85900/index.html "www.math.uwaterloo.ca")). All of TSPLIB has been exactly solved.

This talk from Bill Cook (of U Waterloo) at EURO 2019 mentions several large TSP instances that are being used as benchmarks, but for which no exact solution is known:

