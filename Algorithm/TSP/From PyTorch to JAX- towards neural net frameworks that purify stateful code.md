
 https://github.com/NilsStausberg/travelingSalesman

"Optimization of the time-dependent traveling salesman problem with Monte Carlo method" by Johannes Bentner et al. ( [paper.pdf](https://github.com/NilsStausberg/travelingSalesman/blob/master/paper.pdf)).
 