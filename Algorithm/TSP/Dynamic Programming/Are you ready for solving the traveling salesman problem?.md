
Recently, I encountered a traveling salesman problem (TSP)on leetcode: 943. Find the Shortest Superstring. You can find the problem here.


### [943. Find the Shortest Superstring](https://leetcode.com/problems/find-the-shortest-superstring)
TSP is a famous NP problem. The naive solution’s complexity is O(n!). The DP (dynamic programming)version algorithm ( Bellman-Held-Karp algorithm) will have the complexity of O(2^n * n²). By reducing the complexity from factorial to exponential, if the size of n is relatively small, the problem can be solvable.


https://medium.com/analytics-vidhya/are-you-read-for-solving-the-traveling-salesman-problem-80e3c4ea45fc