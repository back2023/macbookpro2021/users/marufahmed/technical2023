
https://en.wikipedia.org/wiki/Lin%E2%80%93Kernighan_heuristic

From Wikipedia, the free encyclopedia

This article is about the heuristic for the travelling salesman problem. For a heuristic algorithm for the graph partitioning problem, see [Kernighan–Lin algorithm](https://en.wikipedia.org/wiki/Kernighan%E2%80%93Lin_algorithm "Kernighan–Lin algorithm").

In [combinatorial optimization](https://en.wikipedia.org/wiki/Combinatorial_optimization "Combinatorial optimization"), **Lin–Kernighan** is one of the best [heuristics](https://en.wikipedia.org/wiki/Heuristic_algorithm) for solving the symmetric [travelling salesman problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem "Travelling salesman problem").[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] It belongs to the class of [local search](https://en.wikipedia.org/wiki/Local_search_(optimization) "Local search (optimization)") algorithms, which take a tour ([Hamiltonian cycle](https://en.wikipedia.org/wiki/Hamiltonian_cycle "Hamiltonian cycle")) as part of the input and attempt to improve it by searching in the neighbourhood of the given tour for one that is shorter, and upon finding one repeats the process from that new one, until encountering a local minimum. As in the case of the related [2-opt](https://en.wikipedia.org/wiki/2-opt "2-opt") and [3-opt](https://en.wikipedia.org/wiki/3-opt "3-opt") algorithms, the relevant measure of "distance" between two tours is the number of [edges](https://en.wikipedia.org/wiki/Edge_(graph_theory) "Edge (graph theory)") which are in one but not the other; new tours are built by reassembling pieces of the old tour in a different order, sometimes changing the direction in which a sub-tour is traversed. Lin–Kernighan is adaptive and has no fixed number of edges to replace at a step, but favours small numbers such as 2 or 3.


