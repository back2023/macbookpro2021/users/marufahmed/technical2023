
libcuckoo provides a high-performance, compact hash table that allows multiple concurrent reader and writer threads.

The Doxygen-generated documentation is available at the [project page](http://efficient.github.io/libcuckoo/).

Authors: Manu Goyal, Bin Fan, Xiaozhou Li, David G. Andersen, and Michael Kaminsky

For details about this algorithm and citations, please refer to our papers in [NSDI 2013](http://www.cs.cmu.edu/~dga/papers/memc3-nsdi2013.pdf "MemC3: Compact and Concurrent Memcache with Dumber Caching and Smarter Hashing") and [EuroSys 2014](http://www.cs.princeton.edu/~mfreed/docs/cuckoo-eurosys14.pdf "Algorithmic Improvements for Fast Concurrent Cuckoo Hashing"). Some of the details of the hashing algorithm have been improved since that work (e.g., the previous algorithm in [1](http://www.cs.cmu.edu/~dga/papers/memc3-nsdi2013.pdf "MemC3: Compact and Concurrent Memcache with Dumber Caching and Smarter Hashing") serializes all writer threads, while our current implementation supports multiple concurrent writers), however, and this source code is now the definitive reference.


https://github.com/efficient/libcuckoo