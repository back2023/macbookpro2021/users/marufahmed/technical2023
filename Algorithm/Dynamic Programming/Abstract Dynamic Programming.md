https://web.mit.edu/dimitrib/www/Abstract_DP_2ND_EDITION_Complete.pdf

file:///Users/marufahmed/Downloads/Dynamic%20programming%20and%20optimal%20control%20(%20PDFDrive%20).pdf


### Our Print Books
http://www.mit.edu/~dimitrib/

### ORF523 / Dynamic Algorithm Books
https://gitlab.com/nci2022/books/-/tree/main/Dynamic%20Algorithm

### epdf: DYNAMIC PROGRAMMING & OPTIMAL CONTROL, VOL. I
https://epdf.tips/dynamic-programming-amp-optimal-control-vol-i.html

