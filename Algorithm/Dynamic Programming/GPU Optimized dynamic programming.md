
https://towardsdatascience.com/gpu-optimized-dynamic-programming-8d5ba3d7064f


Let us consider the **_Path sum: two ways_** in project euler problem 81 ([Link](https://projecteuler.net/problem=81)). It is an interesting problem for us to explore the dynamic programming paradigm and GPU optimization of the solution.



## Path Sum: Two Ways
### Problem 81
https://projecteuler.net/problem=81