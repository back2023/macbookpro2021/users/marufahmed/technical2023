

This a very interesting question which I am afraid has (as of the moment) a somewhat disappointing answer.

The problem of graph isomorphism has been an object of study of Computational Complexity since the beginnings of the field. It is clearly a problem belonging to NP, that is, the class of problems for which the answers can be easily verified given a 'witness'- an additional piece of information which validates in some sense the answer.

For example, given two isomorphic graphs a witness of its isomorphism could be the permutation which transforms one graph into the other.


https://math.stackexchange.com/questions/1639003/is-there-any-algorithm-to-find-isomorphism-function-between-two-graphs


