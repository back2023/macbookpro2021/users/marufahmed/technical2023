https://github.com/chenxuhao/GraphAIBench

GraphAIBench is a C++ implemented Benchmark Suite for [Graph AI](https://chenxuhao.github.io/graphAI.html). It includes the following benchmarks:

-   Graph Neural Networks (GNN): GCN, GraphSAGE, GAT.
-   Centrality: Betweenness Centrality (BC).
-   Community: Community detection using Louvain algorithm.
-   Components: Connected Components (CC), Srtongly Connected Components (SCC).
-   Corness: k-core decomposition.
-   Flitering: Minimum Spanning Tree (MST), Triangulated Maximally Filtered Graph (TMFG), Planar Maximally Filtered Graph (PMFG).
-   Linear Assignment: Hungarian algorithm.
-   Link Analysis: PageRank (PR).
-   Link Prediction: Node2vec.
-   Mining [1]: triangle counting, clique finding, motif counting, frequent subgraph mining (FSM).
-   Sampling: Random walk.
-   Structure: Graph coarsening.
-   Traversal: Breadth-First Search (BFS) and Single-Source Shortest Paths (SSSP).