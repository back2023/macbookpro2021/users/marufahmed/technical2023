

https://medium.com/vooban-ai/satellite-image-segmentation-a-workflow-with-u-net-7ff992b2a56e


Image Segmentation is a topic of machine learning where one needs to not only categorize what’s seen in an image, but to also do it on a per-pixel level. Here, we want to go from a satellite image to a map representation where things in the image are automatically categorized — just like generating a map view automatically from satellite images.

