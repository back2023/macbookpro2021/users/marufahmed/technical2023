
https://colossalai.org/docs/advanced_tutorials/train_vit_using_pipeline_parallelism


## Introduction[​](https://colossalai.org/docs/advanced_tutorials/train_vit_using_pipeline_parallelism#introduction "Direct link to heading")

In this tutorial, you will learn how to train Vision Transformer for image classification from scratch, using pipeline. Pipeline parallelism is a kind of model parallelism, which is useful when your GPU memory cannot fit your model. By using it, we split the original model into multi stages, and each stage maintains a part of the original model. We assume that your GPU memory cannot fit ViT/L-16, and your memory can fit this model.





