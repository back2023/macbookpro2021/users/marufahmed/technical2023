
https://syncedreview.com/2022/10/19/embedding-training-with-1-gpu-memory-and-100-times-less-budget-an-open-source-solution-for-super-large-recommendation-model-training-on-a-single-gpu/


Colossal-AI has successfully used a heterogeneous training strategy to increase the number of NLP model training parameters capacity by hundreds of times at the same hardware. And experiment results show that it only needs to keep 1~5% of the embedding parameters in the GPU, and is still able to maintain excellent end-to-end training speed.


