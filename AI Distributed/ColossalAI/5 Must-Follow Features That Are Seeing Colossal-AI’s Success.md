
https://medium.com/@hpcaitech/5-must-follow-features-that-are-seeing-colossal-ais-success-2d5361e27e4b


Colossal-AI, HPC-AI Tech’s flagship large-scale parallel AI training system, has become an overnight success. Recently, it was touted as one of the top trending projects on Github, against a backdrop of many projects that have as many as 10K stars.

![](https://miro.medium.com/v2/resize:fit:1400/0*BheRKCvQSon1oB5z)

Colossal-AI is an easy-to-use deep learning system that enables users to maximize the efficiency of AI deployments whilst drastically reducing costs. If you would like to learn more about the project, do check out our GitHub repository: [https://github.com/hpcaitech/ColossalAI](https://github.com/hpcaitech/ColossalAI)









