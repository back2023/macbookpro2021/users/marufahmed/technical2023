

Follow our latest tutorial to see how to implement use Colossal AI with Gradient Notebooks to train a ResNet34 classifier on a multi-GPU machine.

https://blog.paperspace.com/colossal-ai-resnet-multigpu/