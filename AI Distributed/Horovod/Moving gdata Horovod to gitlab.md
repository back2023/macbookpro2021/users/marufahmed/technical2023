2023.02.03
```bash
cd Horovod 
git init
...
Initialized empty Git repository in /g/data/wb00/admin/staging/Horovod/.git/
git add .
git commit -m "2023.02.03"
```

```bash
cd Horovod #existing_repo
#git remote add origin https://gitlab.com/back2023/gadi/g_/data/admin/staging/horovod.git
git remote add origin git@gitlab.com:back2023/gadi/g_/data/admin/staging/horovod.git
git branch -M main
git push -uf origin main
```
