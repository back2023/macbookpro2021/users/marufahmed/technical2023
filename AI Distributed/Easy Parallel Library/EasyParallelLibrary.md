
https://github.com/alibaba/EasyParallelLibrary
# Easy Parallel Library

## [](https://github.com/alibaba/EasyParallelLibrary#overview)Overview

Easy Parallel Library (EPL) is a general and efficient library for distributed model training.

- Usability - Users can implement different parallelism strategies with a few lines of annotations, including data parallelism, pipeline parallelism, tensor model parallelism, and their hybrids.
- Memory Efficient - EPL provides various memory-saving techniques, including gradient checkpoint, ZERO, CPU Offload, etc. Users are able to train larger models with fewer computing resources.
- High Performance - EPL provides an optimized communication library to achieve high scalability and efficiency.

For more information, you may [read the docs](https://easyparallellibrary.readthedocs.io/en/latest/).

EPL [Model Zoo](https://github.com/alibaba/FastNN) provides end-to-end parallel training examples.


