

https://github.com/kvfrans/jax-vqvae-vqgan


JAX implementation of VQVAE/VQGAN autoencoders (+FSQ). Adapted largely from [MaskGIT repo](https://github.com/google-research/maskgit). This code was tested on TPU-v3 pods. We can succesfully reproduce results from VQGAN and FSQ papers.

