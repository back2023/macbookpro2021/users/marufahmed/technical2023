

https://github.com/huiqu18/TDGAN-PyTorch

# TDGAN: Learn distributed GAN with Temporary Discriminators

[](https://github.com/huiqu18/TDGAN-PyTorch#tdgan-learn-distributed-gan-with-temporary-discriminators)

## Description

[](https://github.com/huiqu18/TDGAN-PyTorch#description)

This repository contains the Pytorch code for the paper:

Learn distributed GAN with Temporary Discriminators, ECCV2020. ([PDF](https://arxiv.org/pdf/2007.09221.pdf))

[Hui Qu1*](https://github.com/huiqu18), [Yikai Zhang1*](https://github.com/hazaiisme), [Qi Chang1*](https://github.com/tommy-qichang), Zhennan Yan2, Chao Chen3, and Dimitris Metaxas1 {hq43,yz422,qc58,dnm}@cs.rutgers.edu, [yanzhennan@sensetime.com](mailto:yanzhennan@sensetime.com), [chao.chen.cchen@gmail.com](mailto:chao.chen.cchen@gmail.com).

## Introduction

[](https://github.com/huiqu18/TDGAN-PyTorch#introduction)

In this work, we propose a method for training distributed GAN with sequential temporary discriminators. Our proposed method tackles the challenge of training GAN in the federated learning manner: How to update the generator with a flow of temporary discriminators? We apply our proposed method to learn a self-adaptive generator with a series of local discriminators from multiple data centers. We show our design of loss function indeed learns the correct distribution with provable guarantees. Our empirical experiments show that our approach is capable of generating synthetic data which is practical for real-world applications such as training a segmentation model.

