

https://viso.ai/deep-learning/generative-adversarial-networks-gan/


In the past few years, a new type of machine learning has taken the world by storm: Generative Adversarial Networks, or GANs. So what is a GAN, and why are those AI models to popular?

In this article, we’ll introduce GANs and explain why they’re so important. We’ll also give a brief overview of the following topics:

- What GANs are and how they work
- How to train Generative Adversarial Networks
- Popular real-world applications of GANs
- The biggest challenges of GANs
- Variants and alternatives
- Examples and tutorials

