
 https://medium.com/codex/parallel-gan-training-on-gpu-generate-your-own-images-9aa807042ef1

Generative Adversarial Networks (GANs) are an increasingly popular and very powerful form of computer vision deep learning, and they require a whole lot of compute to be done effectively and speedily. This is just the sort of use case where parallelization with Dask clusters can make a difference in your workflow, so let’s learn how to build a GAN and train it on a cluster!

We’re going to train this with PyTorch, and I’ll use Saturn Cloud to manage the cluster.

> _Disclaimer: I’m a Senior Data Scientist at Saturn Cloud — a platform enabling easy to use parallelization and scaling for Python with Dask. If you’d like to know more about Saturn Cloud, visit us at_ [_www.saturncloud.io_](https://www.saturncloud.io/)_._


