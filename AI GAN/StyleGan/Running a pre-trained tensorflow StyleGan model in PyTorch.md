
https://discuss.pytorch.org/t/running-a-pre-trained-tensorflow-stylegan-model-in-pytorch/42001?page=2


Yes! [@tom](https://discuss.pytorch.org/u/tom) and I transferred the pre-trained weights to PyTorch and created a notebook [here 878](https://github.com/lernapparat/lernapparat/blob/master/style_gan/pytorch_style_gan.ipynb). ![:slight_smile:](https://discuss.pytorch.org/images/emoji/apple/slight_smile.png?v=9 ":slight_smile:")

