


https://github.com/TencentARC/GFPGAN


## About

GFPGAN aims at developing Practical Algorithms for Real-world Face Restoration.


GFPGAN aims at developing a **Practical Algorithm for Real-world Face Restoration**.  
It leverages rich and diverse priors encapsulated in a pretrained face GAN (_e.g._, StyleGAN2) for blind face restoration.


