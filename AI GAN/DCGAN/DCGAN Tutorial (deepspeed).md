

https://www.deepspeed.ai/tutorials/gan/


If you haven’t already, we advise you to first read through the [Getting Started](https://www.deepspeed.ai/getting-started/) guide before stepping through this tutorial.

In this tutorial, we will port the DCGAN model to DeepSpeed using custom (user-defined) optimizers and a multi-engine setup!

