
https://github.com/ArthurFDLR/GANightSky

## About

🌌The Jupyter Notebook behind ThisNightSkyDoesNotExist - Train a StyleGan2-ADA on a custom image dataset scrapped from Instagram!

[arthurfindelair.com/thisnightskydoesnotexist/](https://arthurfindelair.com/thisnightskydoesnotexist/ "https://arthurfindelair.com/thisnightskydoesnotexist/")

## Install StyleGAN2-ADA on your Google Drive

StyleGAN2-ADA only works with Tensorflow 1. Run the next cell before anything else to make sure we’re using TF1 and not TF2.