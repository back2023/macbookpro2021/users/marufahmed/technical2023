

https://github.com/anh-nn01/Satellite-Imagery-to-Map-Translation-using-Pix2Pix-GAN-framework


## About

PyTorch Implementation of Pix2Pix framework to train a U-Net with Generative Adversarial Network to map Satellite Imagery to an equivalent Map.


# Satellite-Imagery-to-Map-Translation-using-Pix2Pix-GAN-framework

[](https://github.com/anh-nn01/Satellite-Imagery-to-Map-Translation-using-Pix2Pix-GAN-framework#satellite-imagery-to-map-translation-using-pix2pix-gan-framework)

PyTorch Implementation of Pix2Pix framework from scratch to train a U-Net with Generative Adversarial Network which translates Satellite Imagery to an equivalent Map.  
**Reference**: [https://arxiv.org/abs/1611.07004](https://arxiv.org/abs/1611.07004)


