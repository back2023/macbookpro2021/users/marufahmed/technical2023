
https://www.kevan.tv/articles/training-stylegan-with-a-custom-dataset/

Fascinated by the artistic opportunities of machine learning and the incredibly realistic images that can be generated, I decided to learn how to use it to create my own images. Here's the results of my exploration.

