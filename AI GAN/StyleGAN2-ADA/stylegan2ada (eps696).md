
https://github.com/eps696/stylegan2ada

## About

StyleGAN2-ada for practice


## Features

[](https://github.com/eps696/stylegan2ada#features)

- inference (image generation) in arbitrary resolution (finally with proper padding on both TF and Torch)
- **multi-latent inference** with split-frame or masked blending
- non-square aspect ratio support (auto-picked from dataset; resolution must be divisible by 2**n, such as 512x256, 1280x768, etc.)
- various conversion options (changing resolution/aspect, adding alpha channel, etc.) for pretrained models (for further finetuning)
- transparency (alpha channel) support (auto-picked from dataset)
- using plain image subfolders as conditional datasets
- **upd:** adaptive pseudo augmentation from [DeceiveD](https://github.com/EndlessSora/DeceiveD) (on by default)
- funky "digression" inference technique, ported from [Aydao](https://github.com/aydao/stylegan2-surgery)

Few operation formats ::

- Windows batch-files, described below (if you're on Windows with powerful GPU)
- local Jupyter notebook (for non-Windows platforms)
- [Colab notebook](https://colab.research.google.com/github/eps696/stylegan2ada/blob/master/StyleGAN2a_colab.ipynb) (max ease of use, requires Google drive)

Just in case, original [StyleGAN2-ada](https://github.com/NVlabs/stylegan2-ada-pytorch) charms:

- claimed to be up to 30% faster than original [StyleGAN2](https://github.com/NVlabs/stylegan2)
- has greatly improved training (requires 10+ times fewer samples than original [StyleGAN2](https://github.com/NVlabs/stylegan2))
- has lots of adjustable internal training settings
- works with plain image folders or zip archives (instead of custom datasets)
- should be easier to tweak/debug