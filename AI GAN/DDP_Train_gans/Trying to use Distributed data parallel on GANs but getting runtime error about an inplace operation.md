
https://stackoverflow.com/questions/66165366/trying-to-use-distributed-data-parallel-on-gans-but-getting-runtime-error-about


"
I encountered a similar error when trying to train a GAN with `DistributedDataParallel`. I noticed the problem was coming from `BatchNorm` layers in my discriminator.

Indeed, `DistributedDataParallel` synchronizes the batchnorm parameters at each forward pass ([see the doc](https://pytorch.org/docs/master/notes/ddp.html)), thereby modifying the variable inplace, which causes problems if you have multiple forward passes in a row.

Converting my `BatchNorm` layers to [`SyncBatchNorm`](https://pytorch.org/docs/stable/generated/torch.nn.SyncBatchNorm.html) did the trick for me:

```python
discriminator = torch.nn.SyncBatchNorm.convert_sync_batchnorm(discriminator)
discriminator = DPP(discriminator)
```

You probably want to do it anyway when using `DistributedDataParallel`.

Alternatively, if you don't want to use `SyncBatchNorm`, you can set the `broadcast_buffers` parameter to `False`, but I don't think you really want to do that, as it means your batch norm stats will not be synchronized among processes.

```python
discriminator = DPP(discriminator, device_ids=[rank], broadcast_buffers=False)
```
"