

https://github.com/imprasukjain/PROGAN/tree/main


# ProGAN - Progressive Growing of GANs


This repository contains the implementation of ProGAN (Progressive Growing of GANs), a state-of-the-art generative model architecture for generating high-quality images. ProGAN was first introduced in the paper ["Progressive Growing of GANs for Improved Quality, Stability, and Variation"](https://arxiv.org/abs/1710.10196) by Tero Karras et al. The Notebook included in this repo is strictly based on the above reseach paper

The ProGAN model is trained progressively, starting from a low resolution and gradually increasing the resolution during training. This progressive training process allows the model to learn finer details and produce high-resolution images.


