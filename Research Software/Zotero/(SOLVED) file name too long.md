

https://forums.zotero.org/discussion/78561/solved-file-name-too-long



I have Zotero on a Windows system and on a Linux system. Apparently, while using it on Windows, a file with a long name was added - NTFS can handle it, while EXT3 cannot.  
  
Thus, when attempting to synchronize Zotero on my Linux machine, I get this error:  
  
Component returned failure code: 0x80520011 (NS_ERROR_FILE_NAME_TOO_LONG) [nsIFile.create]


