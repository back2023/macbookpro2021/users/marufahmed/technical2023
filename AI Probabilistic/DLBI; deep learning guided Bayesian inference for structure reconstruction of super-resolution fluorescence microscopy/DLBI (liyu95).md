
https://github.com/liyu95/DLBI

## About

DLBI: Deep learning guided Bayesian inference for structure reconstruction of super-resolution fluorescence microscopy


# Sum up

**There are three modules in our framework. Our framework contains threes parts: data simulation, deep neural network and Bayesian inference.**

Here, the simulation and deep neural network implementation is openly authorization, and the third part is partially authorization. The first part is data simulation module, whose source can be find in folder fluorescent_simulation_module. The second part is the deep learning module, who can be find in folder deep_learning_module. The third part is the Bayesian inference module, users are encouraged to contact with the authors for further authorization.

If you think the related works here are helpful, please cite "DLBI: Deep learning guided Bayesian inference for structure reconstruction of super-resolution fluorescence microscopy" and "Live cell single molecule-guided Bayesian localization super resolution microscopy".




