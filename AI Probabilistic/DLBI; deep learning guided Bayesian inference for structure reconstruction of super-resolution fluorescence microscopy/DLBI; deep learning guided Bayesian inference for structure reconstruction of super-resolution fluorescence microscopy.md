
https://academic.oup.com/bioinformatics/article/34/13/i284/5045796
2018

## Abstract

Motivation

Super-resolution fluorescence microscopy with a resolution beyond the diffraction limit of light, has become an indispensable tool to directly visualize biological structures in living cells at a nanometer-scale resolution. Despite advances in high-density super-resolution fluorescent techniques, existing methods still have bottlenecks, including extremely long execution time, artificial thinning and thickening of structures, and lack of ability to capture latent structures.

Results

Here, we propose a novel deep learning guided Bayesian inference (DLBI) approach, for the time-series analysis of high-density fluorescent images. Our method combines the strength of deep learning and statistical inference, where deep learning captures the underlying distribution of the fluorophores that are consistent with the observed time-series fluorescent images by exploring local features and correlation along time-axis, and statistical inference further refines the ultrastructure extracted by deep learning and endues physical meaning to the final image. In particular, our method contains three main components. The first one is a simulator that takes a high-resolution image as the input, and simulates time-series low-resolution fluorescent images based on experimentally calibrated parameters, which provides supervised training data to the deep learning model. The second one is a multi-scale deep learning module to capture both spatial information in each input low-resolution image as well as temporal information among the time-series images. And the third one is a Bayesian inference module that takes the image from the deep learning module as the initial localization of fluorophores and removes artifacts by statistical inference. Comprehensive experimental results on both real and simulated datasets demonstrate that our method provides more accurate and realistic local patch and large-field reconstruction than the state-of-the-art method, the 3B analysis, while our method is more than two orders of magnitude faster.

Availability and implementation

The main program is available at [https://github.com/lykaust15/DLBI](https://github.com/lykaust15/DLBI)

Supplementary information

[Supplementary data](https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/bioinformatics/34/13/10.1093_bioinformatics_bty241/2/bioinformatics_34_13_i284_s1.pdf?Expires=1695462033&Signature=41ibgfBtHr~gyxy6VEhYmMMkoN7~hQYaIWjlWTzRij1cJruAmJ7YgzyYOIG8ZP0Ax4kKgsvrwP0HFeaLSwEDbcwwUGAF8NF9XdvA~zoDpeWUvIDCihnlG8xR6hHsm4V-JPme28S7~cY~NRwH3j5PhXEMLjFM0rD-e2x8CUvivc0zgrgISFVjN0vW-FXpCaGyZQUhcYls~m8wfA5NiApMzjNNDaqh~hVzrZ4upQdYkXQJyBYkEYtjXvdnsGGukI-vx6vQegPN6FEbRFcpPUebyHtCigxGCCigJvIHHce-aCdme~5m4VwWImIcgFLXrRl2PsQ7S-tJg7aVHy5e0GIPww__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA) are available at _Bioinformatics_ online.






