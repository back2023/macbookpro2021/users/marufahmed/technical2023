
https://github.com/shangqigao/BayeSR

## About

The official implementation of "Bayesian Image Super-Resolution with Deep Modeling of Image Statistics" via TensorFlow


# BayeSR

The official implementation of "[Bayesian Image Super-Resolution with Deep Modeling of Image Statistics](https://ieeexplore.ieee.org/document/9744488)" which has been accepted by _IEEE Transactions on Pattern Analysis and Machine Intelligence_.









