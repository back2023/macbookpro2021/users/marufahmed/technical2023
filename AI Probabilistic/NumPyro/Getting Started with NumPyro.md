
https://num.pyro.ai/en/stable/getting_started.html

# Getting Started with NumPyro[¶](https://num.pyro.ai/en/stable/getting_started.html#getting-started-with-numpyro "Permalink to this headline")

Probabilistic programming powered by [JAX](https://github.com/google/jax) for autograd and JIT compilation to GPU/TPU/CPU.

## What is NumPyro?[¶](https://num.pyro.ai/en/stable/getting_started.html#what-is-numpyro "Permalink to this headline")

NumPyro is a lightweight probabilistic programming library that provides a NumPy backend for [Pyro](https://github.com/pyro-ppl/pyro). We rely on [JAX](https://github.com/google/jax) for automatic differentiation and JIT compilation to GPU / CPU. NumPyro is under active development, so beware of brittleness, bugs, and changes to the API as the design evolves.





