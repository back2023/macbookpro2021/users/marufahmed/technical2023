
https://github.com/HIPS/firefly-monte-carlo


## About

Implementation of an algorithm for Markov chain Monte Carlo with data subsampling


# Firefly Monte Carlo : Exact MCMC with Subsets of Data

This package implements the Firefly Monte Carlo algorithm described [here](https://hips.seas.harvard.edu/files/maclaurin-firefly-uai-2014.pdf). To get started, check out the [toy data example](https://github.com/HIPS/firefly-monte-carlo/blob/master/examples/toy_dataset.py).



