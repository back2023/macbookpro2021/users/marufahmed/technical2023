
https://bbolker.github.io/math1mp/notes/week8.html


#### Ben Bolker

#### 31 October 2019

## Markov models

- In a **Markov model**, the future state of a system depends only on its current state (not on any previous states)
- Widely used: physics, chemistry, queuing theory, economics, genetics, mathematical biology, sports, …
- From the [Markov chain page on Wikipedia](https://en.wikipedia.org/wiki/Markov_chain):
    - Suppose that you start with $10, and you wager $1 on an unending, fair, coin toss indefinitely, or until you lose all of your money. If Xn�� represents the number of dollars you have after n� tosses, with X0=10�0=10, then the sequence {Xn:n∈ℕ}{��:�∈�} is a Markov process.
    - If I know that you have $12 now, then you will either have $11 or $13 after the next toss with equal probability
    - Knowing the history (that you started with $10, then went up to $11, down to $10, up to $11, and then to $12) doesn’t provide any more information
