
https://github.com/krasserm/bayesian-machine-learning


This repository is a collection of notebooks about _Bayesian Machine Learning_. The following links display some of the notebooks via [nbviewer](https://nbviewer.jupyter.org/) to ensure a proper rendering of formulas. Dependencies are specified in `requirements.txt` files in subdirectories.


