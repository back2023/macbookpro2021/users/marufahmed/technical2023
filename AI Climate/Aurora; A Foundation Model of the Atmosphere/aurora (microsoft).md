

https://github.com/microsoft/aurora/tree/main

2024.08.31
[README.md](https://github.com/microsoft/aurora/blob/main/README.md "README.md") 13 hours ago
[.gitignore](https://github.com/microsoft/aurora/blob/main/.gitignore ".gitignore") last week 

## About

Implementation of the Aurora model for atmospheric forecasting
[microsoft.github.io/aurora](https://microsoft.github.io/aurora "https://microsoft.github.io/aurora")


# Aurora: A Foundation Model of the Atmosphere

[![CI](https://github.com/microsoft/Aurora/actions/workflows/ci.yaml/badge.svg)](https://github.com/microsoft/Aurora/actions/workflows/ci.yaml) [![Documentation](https://camo.githubusercontent.com/c21574baf34c81b3651a5274c8b074471bc1e142a9516c0cfa75dea9223d93fe/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f646f63732d6c61746573742d626c75652e737667)](https://microsoft.github.io/aurora) [![Paper](https://camo.githubusercontent.com/1c19049b5f03d974b5b96b2a0f09a7faf51db61852f003633fa9af9aeb70c070/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f61725869762d323430352e31333036332d626c7565)](https://arxiv.org/abs/2405.13063)

Implementation of the Aurora model for atmospheric forecasting.

_The package currently includes the pretrained model and the fine-tuned version for high-resolution weather forecasting._ _We are working on the fine-tuned version for air pollution forecasting, which will be included in due time._

[Link to the paper on arXiv.](https://arxiv.org/abs/2405.13063)

[Please see the documentation for detailed instructions and more examples.](https://microsoft.github.io/aurora) You can also directly go to [a full-fledged example that runs the model on ERA5](https://microsoft.github.io/aurora/example_era5.html).


