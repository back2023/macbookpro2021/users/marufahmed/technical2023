
https://www.youtube.com/watch?v=ZGsqt1jZxK0&list=PLsj5XvRxIfBsidAL86ZXx8IJrp27MWe17&index=14



Implementation of the paper: "Aurora: A Foundation Model of the Atmosphere" in PyTorch: [Paper link](https://arxiv.org/abs/2405.13063)
2024.08.31
"[aurora_torch](https://github.com/kyegomez/Aurora/tree/main/aurora_torch "aurora_torch")" "last month"
"[.gitignore](https://github.com/kyegomez/Aurora/blob/main/.gitignore ".gitignore")" "2 months ago"



 