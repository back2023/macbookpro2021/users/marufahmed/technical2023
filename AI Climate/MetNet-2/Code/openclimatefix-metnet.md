[https://github.com/openclimatefix/metnet](https://github.com/openclimatefix/metnet)

### MetNet and MetNet-2
PyTorch Implementation of Google Research's MetNet for short term weather forecasting ([https://arxiv.org/abs/2003.12140](https://arxiv.org/abs/2003.12140)), inspired from [https://github.com/tcapelle/metnet_pytorch/tree/master/metnet_pytorch](https://github.com/tcapelle/metnet_pytorch/tree/master/metnet_pytorch)

MetNet-2 ([https://arxiv.org/pdf/2111.07470.pdf](https://arxiv.org/pdf/2111.07470.pdf)) is a further extension of MetNet that takes in a larger context image to predict up to 12 hours ahead, and is also implemented in PyTorch here.

