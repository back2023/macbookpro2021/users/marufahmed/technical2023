```bash
mkdir -p /scratch/fp0/mah900/metnet2_jax
cd /scratch/fp0/mah900/metnet2_jax
```
Google colab version
```bash
import torch
print(torch.version.cuda)
print(torch.__version__)
!nvcc --version
!python --version

11.6 
1.13.1+cu116 
nvcc: NVIDIA (R) Cuda compiler driver Copyright 
(c) 2005-2022 NVIDIA Corporation Built on Wed_Sep_21_10:33:58_PDT_2022 
Cuda compilation tools, release 11.8, V11.8.89 
Build cuda_11.8.r11.8/compiler.31833905_0
Python 3.9.16
```

2023.03.26 (Does not work, `XlaRuntimeError` error )
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/metnet2_jax -c pytorch -c nvidia \
python=3.9 pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.6  
```

Does not Install (coda PyTorch and Jax, together)
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/metnet2_jax -c pytorch -c nvidia \
python=3.9 pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.6  \
jaxlib=*=*cuda* jax cuda-nvcc -c conda-forge 
```

Source: https://github.com/google/jax#pip-installation-gpu-cuda
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/metnet2_jax
pip install --upgrade pip
pip install --upgrade "jax[cuda]" -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html

pip install jupyterlab --no-cache-dir
pip install ml_collections --no-cache-dir
pip install flax --no-cache-dir
```

Test
Source: https://discourse.pymc.io/t/set-up-environment-for-jax-sampling-with-gpu-support-in-pymc-v4/9516
```python
import jax
jax.default_backend()
jax.devices()
```

ARE
```bash
6
gpuvolta
1gpu
fp0
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00+gdata/rt52+gdata/hh5+scratch/fp0

cuda/11.6.1 cudnn/8.6.0-cuda11
/scratch/fp0/mah900/Miniconda2023
/scratch/fp0/mah900/env/metnet2_jax

```

Error 
```bash
XlaRuntimeError: INTERNAL: RET_CHECK failure (external/org_tensorflow/tensorflow/compiler/xla/service/gpu/gpu_compiler.cc:641) dnn != nullptr
```
Need to reinstall
https://github.com/google/jax/issues/14480

### Next, try (working)
```bash
rm -r /scratch/fp0/mah900/env/metnet2_jax
module purge
module load cuda/11.6.1 cudnn/8.6.0-cuda11
conda create -p /scratch/fp0/mah900/env/metnet2_jax python=3.9
conda activate /scratch/fp0/mah900/env/metnet2_jax
pip install --upgrade pip
pip install --upgrade "jax[cuda]" -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html
pip install flax --no-cache-dir
pip install ml_collections --no-cache-dir
pip install jupyterlab --no-cache-dir


```

### 2023.04.04
### Metnet (Pytorch) Install
https://github.com/openclimatefix/metnet
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/metnet2_jax
# Try install Pytorch Metnet
cd /scratch/fp0/mah900/
git clone https://github.com/openclimatefix/metnet.git
cd metnet
pip install -r requirements.txt --no-cache-dir
pip install -e . --no-cache-dir
```

### Test
```
import pandas as pd
import s3fs
import xarray
import datasets
```
All failed
### New installs
1. Datasets, huggingface: https://huggingface.co/docs/datasets/installation
```bash
conda install -c huggingface -c conda-forge datasets
```
2. s3fs (S3Fs is a Pythonic file interface to S3. It builds on top of [botocore](https://botocore.readthedocs.io/en/latest/).): 
   https://s3fs.readthedocs.io/en/latest/install.html
```bash
conda install s3fs -c conda-forge
```
3. 
```bash
conda install pandas

```
4. XArray: https://docs.xarray.dev/en/stable/getting-started-guide/installing.html
```bash
conda install -c conda-forge xarray dask netCDF4 bottleneck
```
5. Zarr: https://anaconda.org/conda-forge/zarr
```
conda install -c conda-forge zarr
```
6. cartopy:  https://scitools.org.uk/cartopy/docs/latest/installing.html
```bash
conda install -c conda-forge cartopy
```
7. matplotlib: https://matplotlib.org/stable/users/installing/index.html#third-party-distributions
```bash
conda install -c conda-forge matplotlib
```
8. GCSFS, A pythonic file-system interface to [Google Cloud Storage](https://cloud.google.com/storage/docs/): https://gcsfs.readthedocs.io/en/latest/
```bash
conda install -c conda-forge gcsfs
```
9. Pyresample,  a python package for resampling geospatial image data:  https://pyresample.readthedocs.io/en/latest/installation.html
```
conda install -c conda-forge pyresample
```
10. cartopy_offlinedata: https://anaconda.org/conda-forge/cartopy_offlinedata
```bash
conda install -c conda-forge cartopy_offlinedata
```


### 2023.04.18
Try Jupyter lab for downloading data
(No need anymore as internet is now available )
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/metnet2_jax
jupyter lab --no-browser --ip='*' --NotebookApp.token='' --NotebookApp.password='' --notebook-dir="/scratch/fp0/mah900/metnet2_jax"
```
  https://stackoverflow.com/questions/41159797/how-to-disable-password-request-for-a-jupyter-notebook-session
Port forwarding:
```bash
PORT=8891
NODE=gadi-login-02
ssh -v -N -L 8080:${NODE}:${PORT} mah900@gadi.nci.org.au
```

### 2023.04.26

ARE runs with internet now.
The MetNet code and dataset still has problems.
Only random data is used.
Stopping further work with MetNet, until more code/data is released. 