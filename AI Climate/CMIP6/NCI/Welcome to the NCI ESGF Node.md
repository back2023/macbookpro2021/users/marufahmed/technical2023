
https://esgf.nci.org.au/projects/esgf-nci/

The [Earth System Grid Federation](https://esgf.llnl.gov/) (ESGF) consists of federated data centres across the globe that enable access to the largest archive of climate data world-wide. This portal allows you to find, select and download data files from the federation.







