
https://github.com/ENES-Data-Space/notebooks/blob/main/Quick_Start_cartopy.ipynb

[**cartopy**](https://scitools.org.uk/cartopy/docs/latest/) is a Python package designed for geospatial data processing in order to produce maps and other geospatial data analyses. It makes use of the powerful `PROJ`, `NumPy` and `Shapely` libraries and includes a programmatic interface built on top of `matplotlib` for the creation of publication quality maps.

