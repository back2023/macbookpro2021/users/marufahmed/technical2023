
https://github.com/tpys/ai-models-fuxi/tree/main

# ai-models-fuxi

`ai-models-fuxi` is an [ai-models](https://github.com/ecmwf-lab/ai-models) plugin to run [Fudan's FuXi](https://github.com/tpys/FuXi.git).

FuXi: A cascade machine learning forecasting system for 15-day global weather forecast,arXiv preprint: 2306.12873, 2022. [https://arxiv.org/pdf/2306.12873.pdf](https://arxiv.org/pdf/2306.12873.pdf)
