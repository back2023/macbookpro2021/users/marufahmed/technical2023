
https://github.com/google/ai-weather-climate/tree/main

This repository contains code for ML applied to various avenues in the weather and climate domain.

This is an experimental research project and is not an officially supported Google project. You are welcome to use it, but we do not guarantee stability.

## [](https://github.com/google/ai-weather-climate/tree/main#description-of-subdirectories)Description of subdirectories

- `ai_weather_climate/beam`: contains data pipelines implemented in beam.




