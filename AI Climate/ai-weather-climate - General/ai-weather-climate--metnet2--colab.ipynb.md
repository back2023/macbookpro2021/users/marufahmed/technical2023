
https://github.com/google/ai-weather-climate/blob/main/metnet2/colab.ipynb


# MetNet-2 Model Skeleton

This colab provides the model code for [MetNet-2](https://ai.googleblog.com/2021/11/metnet-2-deep-learning-for-12-hour.html) as well as general code for preprocessing data.


