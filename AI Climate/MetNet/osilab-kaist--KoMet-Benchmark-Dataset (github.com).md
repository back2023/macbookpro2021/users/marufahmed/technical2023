
https://github.com/osilab-kaist/komet-benchmark-dataset


## KoMet.v1.1

# 🌧 KoMet-Benchmark-Dataset
This repository contains the data and code to reproduce all the analyses in the paper ([link](https://arxiv.org/abs/2206.15241)). If you need something immediately or find it confusing, please open a GitHub issue or email us. We recommend reading the paper, appendix, and below descriptions thoroughly before running the code. Future code modifications and official developments will take place here.

Paper: _"Benchmark Dataset for Precipitation Forecasting by Post-Processing the Numerical Weather Prediction."_, under review in the NeurIPS 22 Benchmark Dataset Track

## 🤖 Models
Currently, we support three models from the following papers:

- [U-Net: Convolutional Networks for Biomedical Image Segmentation, Ronneberger et al. 2015](http://arxiv.org/abs/1505.04597)
- [Convolutional LSTM Network: A Machine Learning Approach for Precipitation Nowcasting, Shi et al. 2015](https://arxiv.org/abs/1506.04214)
- [MetNet: A Neural Weather Model for Precipitation Forecasting, Sonderby et al. 2020](https://arxiv.org/abs/2003.12140)

You can load the model using the `set_model()` function in `utils.py`. Below is an example of initializing the MetNet model with various hyperparameters.
```python
from model.metnet import MetNet

model = MetNet(input_data=input_data,
               window_size=window_size,
               num_cls=num_classes,
               in_channels=in_channels,
               start_dim=start_dim,
               center_crop=False,
               center=None,
               pred_hour=1)
```

 

