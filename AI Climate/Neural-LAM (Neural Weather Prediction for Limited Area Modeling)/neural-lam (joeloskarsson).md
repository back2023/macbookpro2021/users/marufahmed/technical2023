

https://github.com/joeloskarsson/neural-lam

https://github.com/mllam/neural-lam

Neural-LAM is a repository of graph-based neural weather prediction models for Limited Area Modeling (LAM). The code uses [PyTorch](https://pytorch.org/) and [PyTorch Lightning](https://lightning.ai/pytorch-lightning). Graph Neural Networks are implemented using [PyG](https://pyg.org/) and logging is set up through [Weights & Biases](https://wandb.ai/).

The repository contains LAM versions of:

- The graph-based model from [Keisler (2022)](https://arxiv.org/abs/2202.07575).
- GraphCast, by [Lam et al. (2023)](https://arxiv.org/abs/2212.12794).
- The hierarchical model from [Oskarsson et al. (2023)](https://arxiv.org/abs/2309.17370).

For more information see our preprint: [_Graph-based Neural Weather Prediction for Limited Area Modeling_](https://arxiv.org/abs/2309.17370). If you use Neural-LAM in your work, please cite:

```
@article{oskarsson2023graphbased,
      title={Graph-based Neural Weather Prediction for Limited Area Modeling},
      author={Joel Oskarsson and Tomas Landelius and Fredrik Lindsten},
      year={2023},
      journal={arXiv preprint arXiv:2309.17370}
}
```

