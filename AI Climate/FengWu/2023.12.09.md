
```bash
cd /scratch/fp0/mah900/
git clone https://github.com/OpenEarthLab/FengWu.git
```

Curl download from the one drive
```bash
cd /scratch/fp0/mah900/FengWu/
qsub download-fengwu.sh
```

```bash
#!/bin/bash
#PBS -S /bin/bash
#PBS -q copyq
#PBS -l ncpus=1
#PBS -l jobfs=10GB
#PBS -l storage=gdata/wb00 
# scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00+scratch/vp91 
#PBS -l mem=20GB
#PBS -l walltime=00:30:00
#PBS -N cqp_FengWu
#PBS -P fp0

cd /g/data/wb00/admin/testing/FengWu

curl 'https://pjlab-my.sharepoint.cn/personal/chenkang_pjlab_org_cn/_layouts/15/download.aspx?SourceUrl=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments%2Ffengwu%5Fv1%2Eonnx' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7' \
  -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
  -H 'Connection: keep-alive' \
  -H 'Cookie: FedAuth=77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U1A+VjEzLDBoLmZ8bWVtYmVyc2hpcHx1cm4lM2FzcG8lM2Fhbm9uI2Q1ZDQ3OTk3NDBhMzY2MjdhYzYyMGFmYjhhMDNmZWQyNDQ0YWZjN2YxODYzNGQyNzhhMDNjNDllYmVlYTc5MGIsMCMuZnxtZW1iZXJzaGlwfHVybiUzYXNwbyUzYWFub24jZDVkNDc5OTc0MGEzNjYyN2FjNjIwYWZiOGEwM2ZlZDI0NDRhZmM3ZjE4NjM0ZDI3OGEwM2M0OWViZWVhNzkwYiwxMzM0NjU5MjA1MDAwMDAwMDAsMCwxMzM0NjY3ODE1MDI2MTQ1NzksMC4wLjAuMCwyNTgsMGNiZTg3NWQtOGNjMy00Mzg2LTgyODMtMGQ4MGJiZjIzZTA1LCwsMmEwNWY2YTAtNzA4MS0wMDAwLTE5OWMtMWM4OTk5YzQyZmRlLDJhMDVmNmEwLTcwODEtMDAwMC0xOTljLTFjODk5OWM0MmZkZSxnalY1OVp1RGRVS3BhU2h4VlhkQUxnLDAsMCwwLCwsLDI2NTA0Njc3NDM5OTk5OTk5OTksMCwsLCwsLCwwLCw3NDEsSi1aSXFsTk15eXR5TXNmcHpyNWI4RzU1aFpJLGRLbVRIZGVaSm16V29iUTdDdEg4WGtiWW1SSElVd2tybmU2b2kzQjVEV1JkZlhvdVJOU1ZVUXRucmFDNzlxbUxuOVVDVzQ2YzNBQTJrUjg0aFIrZ3d3RmdiZVBRWnZMdVNQenNBNWdJV01lY1FFdVFnd29IU2ZZb2FyYjBxcUUra0FoZXErR1ZDK3FXdXRCVHgvTXRWWWdGVzRPaC9RWjFYUGpTaE5oU29kTjBwTjFnakVxbEt3YlZ3dDBDbm1SRzNRd3FoTnlmOHdWQzV0ZUtOS3hsVUkzSGxFV0FXcjFSSkV3akJ5QkRONGFTbEdZN2ZVby9mWGFzZnRpMS91dzZHUkUrMTc4U2pYS0ZFblkzeE9ia0VCOWorY3dsQXJ5dkJ4TjRnRXNUZ1pGRmNLMTMwc29jSDU4Yjh2U0tQYWZaSHh1QkNTWUN0LzlmajZDMFR2QjgyZz09PC9TUD4=; MicrosoftApplicationsTelemetryDeviceId=77fb1486-4add-46a4-be73-32df3ef026ef; MSFPC=GUID=9da5fb3416214417bd141516aae6aa2d&HASH=9da5&LV=202302&V=4&LU=1677470532435; ai_session=mLfrdIcPXwc/EBe0KzGLQ2|1702118158013|1702118867165' \
  -H 'DNT: 1' \
  -H 'Referer: https://pjlab-my.sharepoint.cn/personal/chenkang_pjlab_org_cn/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments%2Ffengwu%5Fv1%2Eonnx&parent=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments&ga=1' \
  -H 'Sec-Fetch-Dest: iframe' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Service-Worker-Navigation-Preload: true' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36' \
  -H 'sec-ch-ua: "Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "macOS"' \
  --compressed  \
  --output fengwu_v1.onnx


curl 'https://pjlab-my.sharepoint.cn/personal/chenkang_pjlab_org_cn/_layouts/15/download.aspx?SourceUrl=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments%2Ffengwu%5Fv2%2Eonnx' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7' \
  -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
  -H 'Connection: keep-alive' \
  -H 'Cookie: FedAuth=77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U1A+VjEzLDBoLmZ8bWVtYmVyc2hpcHx1cm4lM2FzcG8lM2Fhbm9uI2Q1ZDQ3OTk3NDBhMzY2MjdhYzYyMGFmYjhhMDNmZWQyNDQ0YWZjN2YxODYzNGQyNzhhMDNjNDllYmVlYTc5MGIsMCMuZnxtZW1iZXJzaGlwfHVybiUzYXNwbyUzYWFub24jZDVkNDc5OTc0MGEzNjYyN2FjNjIwYWZiOGEwM2ZlZDI0NDRhZmM3ZjE4NjM0ZDI3OGEwM2M0OWViZWVhNzkwYiwxMzM0NjU5MjA1MDAwMDAwMDAsMCwxMzM0NjY3ODE1MDI2MTQ1NzksMC4wLjAuMCwyNTgsMGNiZTg3NWQtOGNjMy00Mzg2LTgyODMtMGQ4MGJiZjIzZTA1LCwsMmEwNWY2YTAtNzA4MS0wMDAwLTE5OWMtMWM4OTk5YzQyZmRlLDJhMDVmNmEwLTcwODEtMDAwMC0xOTljLTFjODk5OWM0MmZkZSxnalY1OVp1RGRVS3BhU2h4VlhkQUxnLDAsMCwwLCwsLDI2NTA0Njc3NDM5OTk5OTk5OTksMCwsLCwsLCwwLCw3NDEsSi1aSXFsTk15eXR5TXNmcHpyNWI4RzU1aFpJLGRLbVRIZGVaSm16V29iUTdDdEg4WGtiWW1SSElVd2tybmU2b2kzQjVEV1JkZlhvdVJOU1ZVUXRucmFDNzlxbUxuOVVDVzQ2YzNBQTJrUjg0aFIrZ3d3RmdiZVBRWnZMdVNQenNBNWdJV01lY1FFdVFnd29IU2ZZb2FyYjBxcUUra0FoZXErR1ZDK3FXdXRCVHgvTXRWWWdGVzRPaC9RWjFYUGpTaE5oU29kTjBwTjFnakVxbEt3YlZ3dDBDbm1SRzNRd3FoTnlmOHdWQzV0ZUtOS3hsVUkzSGxFV0FXcjFSSkV3akJ5QkRONGFTbEdZN2ZVby9mWGFzZnRpMS91dzZHUkUrMTc4U2pYS0ZFblkzeE9ia0VCOWorY3dsQXJ5dkJ4TjRnRXNUZ1pGRmNLMTMwc29jSDU4Yjh2U0tQYWZaSHh1QkNTWUN0LzlmajZDMFR2QjgyZz09PC9TUD4=; MicrosoftApplicationsTelemetryDeviceId=77fb1486-4add-46a4-be73-32df3ef026ef; MSFPC=GUID=9da5fb3416214417bd141516aae6aa2d&HASH=9da5&LV=202302&V=4&LU=1677470532435; ai_session=/tN28pksm9GHqSfpYVqV97|1702122832299|1702122833096' \
  -H 'DNT: 1' \
  -H 'Referer: https://pjlab-my.sharepoint.cn/personal/chenkang_pjlab_org_cn/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments%2Ffengwu%5Fv2%2Eonnx&parent=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments&ga=1' \
  -H 'Sec-Fetch-Dest: iframe' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Service-Worker-Navigation-Preload: true' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36' \
  -H 'sec-ch-ua: "Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "macOS"' \
  --compressed \
  --output fengwu_v2.onnx
```



# 2023.12.11

Plan: Data make script


# 2023.12.12

FengWu NCI script: `scratch/fp0/mah900/FengWu/FengWu_NCI.py`

Move statistics file to `wb00`:
```bash
mv /scratch/fp0/mah900/FengWu/data_std.npy /g/data/wb00/admin/testing/FengWu
mv /scratch/fp0/mah900/FengWu/data_mean.npy /g/data/wb00/admin/testing/FengWu
```

# 2023.12.13

Created a new gitlab backup repo
`https://gitlab.com/back2023/gadi/scratch/fp0/mah900/fengwu` 
# Initial Instructions form the site
---
# FengWu
#### The repository for this project is empty
You can get started by cloning the repository or start adding files to it with one of the following options.

##### Command line instructions
##### Git global setup
```
git config --global user.name "Maruf Ahmed"
git config --global user.email "nci.2022.06@gmail.com"
```

##### Create a new repository
```
git clone git@gitlab.com:back2023/gadi/scratch/fp0/mah900/fengwu.git
cd fengwu
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
```

##### Push an existing folder
```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:back2023/gadi/scratch/fp0/mah900/fengwu.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

##### Push an existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:back2023/gadi/scratch/fp0/mah900/fengwu.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
```

----

# Apply to FengWu (Working)
```
cd /scratch/fp0/mah900/FengWu
git remote add back2023 git@gitlab.com:back2023/gadi/scratch/fp0/mah900/fengwu.git
git push --set-upstream back2023 --all
git push --set-upstream back2023 --tags

```

Note: Had to increase Jobfs size to 300GB as 28 day prediction data is more thatn 50GB
```
61G	/jobfs/103996451.gadi-pbs/2022-01-01T00
```
So, have to delete after calculation and before the next run. 

Next plan: Write stat compare script
Implement weighted RMSE and ACC in Numpy, using spearate lat. 
And, install:  xskillscore: Metrics for verifying forecasts (https://xskillscore.readthedocs.io/en/v0.0.19/index.html)



# 2023.12.14

Next Plan: Reading all data for ACC is slow.
A cache algorithm have to be implemented. 


# 2023.12.15

Install `xskillscore` 
```bash 
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/NCI_ai_models

pip install xskillscore
...
Installing collected packages: threadpoolctl, llvmlite, scikit-learn, properscoring, numba, xhistogram, xskillscore
Successfully installed llvmlite-0.41.1 numba-0.58.1 properscoring-0.1 scikit-learn-1.3.2 threadpoolctl-3.2.0 xhistogram-0.3.2 xskillscore-0.0.24




```


Plan: Try reducing to three days (Working)
Next plan: implement a month cache data (much faster, about 5.25 times)

Note: Need to Do month by month (Not year)

Next Plan: Re-structure the code (Done-2023.12.16)
start breaking the files into three parts
```bash
/scratch/fp0/mah900/FengWu/FengWu_Inference.py
/scratch/fp0/mah900/FengWu/FengWu_stats.py
/scratch/fp0/mah900/FengWu/FengWu_util.py
```

# 2023.12.16

Done multiporcessing for stats calculation
Each Surface variable take about 67 sec.
while, Pressure level taking: 955 or 14.25 min
So, need to change the pressure level function so that it takes only one level at a time and 
then run all levels in parallel. (Done)

Next plan: need to change to process pool to limit the process number


# 2023.12.17

Plan: Input Dict for all var and level
Plan: Process pool implement. 

Plan tomorrow: Try one wee run, 
if successful the try one month with pbs script, see what happens (From a enclosed folder)


# 2023.12.18

json can not write tuple
```
TypeError: keys must be str, int, float, bool or None, not tuple
```
Try: install ultra-jason
Source: https://stackoverflow.com/a/56403092
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/NCI_ai_models
pip install ujson
...
Successfully installed ujson-5.9.0
[notice] A new release of pip is available: 23.3.1 -> 23.3.2
[notice] To update, run: pip install --upgrade pip
```
 json stores tuple as string. so, might change to string in the first place to be able to save. 


Next plan move every thing inside  a package

```bash
cd /scratch/fp0/mah900/FengWu/
mkdir FengWu
mv FengWu_Inference.py ./FengWu/inference.py
mv FengWu_pred_stat.py ./FengWu/pred_and_stat.py
mv FengWu_stats.py     ./FengWu/stats.py
mv FengWu_util.py      ./FengWu/util.py
touch               ./FengWu/__init__.py 
```


# 2023.12.21

Create pbs script (Working)
`scratch/fp0/mah900/FengWu/FengWu_a1_h12.pbs`

```bash
#!/bin/bash
#PBS -S /bin/bash
#PBS -q dgxa100
#PBS -l ncpus=16
#PBS -l ngpus=1
#PBS -l jobfs=500GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00+scratch/vp91+gdata/rt52
#PBS -l mem=500GB
#PBS -l walltime=12:00:00
#PBS -N FengWu_A1_h12
#PBS -P fp0

. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/NCI_ai_models
python /scratch/fp0/mah900/FengWu/fengwu/pred_stat.py

```

```bash
cd /scratch/fp0/mah900/FengWu/Stats_data
qsub ../FengWu_a1_h12.pbs
```

Note : 32 threads are too many, CPU cores are not showing 100%
So, setting to 20 in `scratch/fp0/mah900/FengWu/fengwu/stats.py`
```python
# line 21
N_PROC = 20 # 32 #16 #12
```


# 2023.12.22

Need to Change Month logic
1) Run First day of each month only
2) Run 4 months in year. from 2018 to 2022

Also, use seperate GPU and CPU queue for processing?
Could not implement
- Could not find multi queue option in the PBS
- Each date produces about 61 GB data, so storing them is not feasible in the moment

Next plan, add argparse in the python code, needed for the PBS Scripts (Done)

Next plan, add [xskillscore](https://xskillscore.readthedocs.io/en/v0.0.19/index.html) to the stats (Done, the probabilistic one's do not work )

After adding scores from the xskillscore, 3-day acc data take 35 min
for 7-day, 0:39:57

Try run 2019-22
```bash
cd /scratch/fp0/mah900/FengWu/Stats_data
qsub FengWu_A1_2018_01.pbs
qsub FengWu_A1_2018_05.pbs
qsub FengWu_A1_2018_07.pbs
qsub FengWu_A1_2018_12.pbs

qsub FengWu_A1_2019_01.pbs
qsub FengWu_A1_2019_05.pbs
qsub FengWu_A1_2019_07.pbs
qsub FengWu_A1_2019_12.pbs

qsub FengWu_A1_2020_01.pbs
qsub FengWu_A1_2020_05.pbs
qsub FengWu_A1_2020_07.pbs
qsub FengWu_A1_2020_12.pbs

qsub FengWu_A1_2021_01.pbs
qsub FengWu_A1_2021_05.pbs
qsub FengWu_A1_2021_07.pbs
qsub FengWu_A1_2021_12.pbs

qsub FengWu_A1_2022_01.pbs
qsub FengWu_A1_2022_05.pbs
qsub FengWu_A1_2022_07.pbs
qsub FengWu_A1_2022_12.pbs
```

Error-1: In the six hour ahead function, fixed 
File: `scratch/fp0/mah900/FengWu/fengwu/util.py`
```python
def six_hour_before_date(year, month, day, hour): 
    #print (year, month, day, hour)
    if hour >= 6:
        return year, month, day, hour-6
    
    hour = 24 - 6
    day -= 1
    if day <= 0:
        month -= 1              
        if month <= 0:
            month = 12
            year -= 1
        day = get_month_days(year, month)     
    #print (year, month, day, hour)
    return year, month, day, hour
```


# 2023.12.23
***
Only six tasks are running.
A100 is taking long, ~~may~~ have to switch to v100 for later models. 
More V100 nodes so it would be okay to run longer jobs. 
(Also, A100s may get released during the holiday period (????) have to see)
***
Okay, 12 hours gone, and 
only Six are running, rest will take too long.
Change them to V100 with 48 hours 

```bash
cd /scratch/fp0/mah900/FengWu/Stats_V100/
qsub FengWu_V1_2019_07.pbs
qsub FengWu_V1_2019_12.pbs 
qsub FengWu_V1_2020_01.pbs
qsub FengWu_V1_2020_05.pbs
qsub FengWu_V1_2020_07.pbs
qsub FengWu_V1_2020_12.pbs
qsub FengWu_V1_2021_01.pbs
qsub FengWu_V1_2021_05.pbs
qsub FengWu_V1_2021_07.pbs
qsub FengWu_V1_2021_12.pbs
qsub FengWu_V1_2022_01.pbs
qsub FengWu_V1_2022_05.pbs
qsub FengWu_V1_2022_07.pbs
qsub FengWu_V1_2022_12.pbs
```


- All started running within 20 mins 
- Use 2 V100 to increase the multiprocess pool number to 24 (???)
	- Plus point: Already using 190 GB mem, so the other GPU is unstable anyway 
	- But, are they on two cores, can multiprocessing can use them?
	- Can experiment with custom Jupyter notebook


# 2023.12.24

### Note
- All but three jobs are killed,  (So, three job is the limit)
	- Various disk error,
- So, next time run fewer jobs 
- One option is to run 4 v100 and see if can use all 48 CPUs to finish the task quicker. 

### Test with Jupyter
Note: 4 V100 not running, Now, running (Took 5 min longer to run)
3 V100 and 2 V100 relatively quick.  

- Try: 4 V100, CPU set 48
	- Top: Looks like using all 48 
	- Total time: 0:16:40.092304
	- 45/17=2.647 speed up
	- Two iterations, Total time: 0:32:20.048336 (31 -> 8.33h)
- (So, need to check for 3 V100, or just set pool proc to 36 )
	- Input 00: 0:15:36.031775 (!!!!) mem: 65886.2 used
	- Two loops: Total time: 0:31:06.805911
	
- How about 24 (Input 00: 0:18:02.490456) (Input 00: 0:16:55.814425)
- 24 processors but set to 30 (0:17:23.846288) (two loops time: 0:34:31.660879)

- Or, how about 69 
  
- Or, 96, but only 69 created,  mem: 145GB, Input 00: 0:16:12.191955
	- Two loops: Total time: 0:33:01.115806
	  
- Try 12 again, mem: 31642.6 used, 0:26:11.305006 (!!!!)
	- Two loops, Total time: 0:49:38.559325

#### Also Plan: Cupy-Xarray
Install 
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/NCI_ai_models
conda install cupy-xarray -c conda-forge

...
The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    cuda-version-11.8          |       h70ddcb2_2          20 KB  conda-forge
    cupy-12.3.0                |  py311h134e05b_0        40.0 MB  conda-forge
    cupy-xarray-0.1.3          |     pyhd8ed1ab_0          13 KB  conda-forge
    fastrlock-0.8.2            |  py311hb755f60_2          37 KB  conda-forge
    libstdcxx-ng-13.2.0        |       h7e041cc_3         3.7 MB  conda-forge
    psutil-5.9.7               |  py311h459d7ec_0         490 KB  conda-forge
    ------------------------------------------------------------
                                           Total:        44.2 MB
...
```
Error-1
Jupyter gets disconnected,  each time.

Try cmd, Error-1
```bash
python /scratch/fp0/mah900/FengWu/fengwu/pred_stat.py -y 2017 -m 1 
...
AttributeError: module 'psutil._psutil_posix' has no attribute 'getpagesize'

```
Try-1
```
pip install -U psutil
...
Requirement already satisfied: psutil in /scratch/fp0/mah900/env/NCI_ai_models/lib/python3.11/site-packages (5.9.7)
```
Source: https://github.com/ultralytics/ultralytics/issues/1030#issuecomment-1436124997
Try-2 (Working)
```
pip show psutil

DEPRECATION: Loading egg at /scratch/fp0/mah900/env/NCI_ai_models/lib/python3.11/site-packages/modulus-22.9-py3.11.egg is deprecated. pip 24.3 will enforce this behaviour change. A possible replacement is to use pip for package installation.. Discussion can be found at https://github.com/pypa/pip/issues/12330
Name: psutil
Version: 5.9.7
Summary: Cross-platform lib for process and system monitoring in Python.
Home-page: https://github.com/giampaolo/psutil
Author: Giampaolo Rodola
Author-email: g.rodola@gmail.com
License: BSD-3-Clause
Location: /scratch/fp0/mah900/env/NCI_ai_models/lib/python3.11/site-packages
Requires: 
Required-by: distributed, ipykernel, provenance, wandb, xserver


pip uninstall psutil
```

I also encountered this problem.
There are two solutions:  
1. Uninstall the resource  
2. Comment out the resouce code in process_collector.py and notebookapp.py. The results are as follows:
Source: https://github-com.translate.goog/jupyter/notebook/issues/5817?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en-US&_x_tr_pto=wapp

I have the same error ,after rename my `resource` module in the `PYTHONPATH` , it works right. check your PYTHONPATH, is there a resource module?
Source: https://stackoverflow.com/a/56769631

My problem was mamba installed graalvm's python for some reason, and that version of python does not have `resource.getpagesize`. Using conda and their version of python solves the issue for me.
Source: https://stackoverflow.com/a/75831405
***

Does not work with the Jupyter, but works with the CMD (!!!!)
```
python /scratch/fp0/mah900/FengWu/fengwu/pred_stat.py -y 2017 -m 1 
```

Tried Cupy-Xarray
Both Jupyter notebook and terminal failing 
So, trying ssh. 

```bash
ssh gadi-gpu-v100-0008.gadi.nci.org.au
cd /scratch/fp0/mah900/FengWu/
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/NCI_ai_models

python /scratch/fp0/mah900/FengWu/fengwu/pred_stat.py -y 2017 -m 1 
```

Error-1
```
  File "/scratch/fp0/mah900/FengWu/fengwu/stats.py", line 85, in stat_var_level_series
    era5_cache_data = era5_cache_data.cupy.as_cupy()
                      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  ...
  cupy_backends.cuda.api.runtime.CUDARuntimeError: cudaErrorInitializationError: initialization error                    
```
Try-1 (Working)
```python
ctx = multiprocessing.get_context('spawn')
pool = ctx.Pool(4)
```
Source: https://stackoverflow.com/a/54808510

Error-2
```
  File "/scratch/fp0/mah900/FengWu/fengwu/stats.py", line 109, in stat_var_level_series
    pearson_r_p = xs.pearson_r_p_value(pred_xarray, era5_ground_truth).to_numpy().item()
                  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
...
  TypeError: Implicit conversion to a NumPy array is not allowed. Please use `.get()` to construct a NumPy array explicitly.
...			   
```

Error-3
```
  File "/scratch/fp0/mah900/FengWu/fengwu/stats.py", line 424, in compute_weighted_rmse
    rmse = np.sqrt(((error)**2 * weights_lat).mean(mean_dims))
```

Try
```bash
        rmse = compute_weighted_rmse(pred_xarray , era5_ground_truth, mean_dims=xr.ALL_DIMS) #.to_numpy().item()

```
Error-4
```
...
  File "/scratch/fp0/mah900/FengWu/fengwu/stats.py", line 424, in compute_weighted_rmse
    rmse = np.sqrt(((error)**2 * weights_lat).mean(mean_dims))
    
...
TypeError: Unsupported type <class 'numpy.ndarray'>
```

Try another stats file for cupy
copy: `scratch/fp0/mah900/FengWu/fengwu/pred_stat.py`
Then, change numpy functions to cupy

Try (Not working)
```python
cupy.asarray(error.latitude)

...
AttributeError: 'ndarray' object has no attribute 'cupy'
```
Source: https://github.com/cupy/cupy/issues/2138#issuecomment-481933171
Try (Working)
```python
print ("cupy.deg2rad(error.latitude):", cupy.deg2rad(cupy.asanyarray(error.latitude)))
```
Source: https://github.com/cupy/cupy/issues/2138#issuecomment-482223041
- So, (Looks like ) the problem is that is cupy function return 'ndarray' object, which needs to be converted to cupy again (am I right ?)

Error-5
```
  File "/scratch/fp0/mah900/FengWu/fengwu/stats_cupy.py", line 437, in compute_weighted_rmse_cupy
    rmse = cupy.sqrt(((error)**2 * weights_lat).mean(mean_dims))
```
Try 
```python
rmse = cupy.sqrt(((  cupy.squeeze(error)  )**2 * weights_lat).mean(mean_dims))  
```
Error-6
```
ValueError: operands could not be broadcast together with shapes (721, 1440) (721,)
```

### Leave CuPy and use 24 CPUs
Cupy functions are taking longer to fix, just run 24 CPU instead for now
May later come back to cupy

Make a commit and move to 24 CPU code
Also, job outputs taking a lot of space, run them from wb00. 