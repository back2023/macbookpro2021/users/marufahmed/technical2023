
https://github.com/OpenEarthLab/FengWu
(Accessed: 2023.11.26 7:40 pm)
(Released last week: 17 Nov, 2023)

This repository presents the inference code and pre-trained model of FengWu, a deep learning-based weather forecasting model that pushes the skillful global weather forecasts beyond 10 days lead. The original version of FengWu has 37 vertical levels. To make it easier for real-time evaluation with operational analysis data, the pre-trained model released here accepts 13 vertical levels.  
If you are interested in the technique details, please refer to the arxiv version: [https://arxiv.org/abs/2304.02948](https://arxiv.org/abs/2304.02948).

If you have any questions, feel free to contact Dr. Lei Bai [bailei@pjlab.org.cn](mailto:bailei@pjlab.org.cn).






