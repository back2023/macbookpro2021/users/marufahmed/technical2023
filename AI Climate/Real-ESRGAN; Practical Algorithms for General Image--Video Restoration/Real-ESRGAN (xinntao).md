
https://github.com/xinntao/Real-ESRGAN/tree/master


Real-ESRGAN aims at developing **Practical Algorithms for General Image/Video Restoration**.  
We extend the powerful ESRGAN to a practical restoration application (namely, Real-ESRGAN), which is trained with pure synthetic data.

🌌 Thanks for your valuable feedbacks/suggestions. All the feedbacks are updated in [feedback.md](https://github.com/xinntao/Real-ESRGAN/blob/master/docs/feedback.md).

---

If Real-ESRGAN is helpful, please help to ⭐ this repo or recommend it to your friends 😊  
Other recommended projects:  
▶️ [GFPGAN](https://github.com/TencentARC/GFPGAN): A practical algorithm for real-world face restoration  
▶️ [BasicSR](https://github.com/xinntao/BasicSR): An open-source image and video restoration toolbox  
▶️ [facexlib](https://github.com/xinntao/facexlib): A collection that provides useful face-relation functions.  
▶️ [HandyView](https://github.com/xinntao/HandyView): A PyQt5-based image viewer that is handy for view and comparison  
▶️ [HandyFigure](https://github.com/xinntao/HandyFigure): Open source of paper figures







