
https://github.com/HansBambel/SmaAt-UNet

# SmaAt-UNet

Code for the Paper "SmaAt-UNet: Precipitation Nowcasting using a Small Attention-UNet Architecture" [Arxiv-link](https://arxiv.org/abs/2007.04417), [Elsevier-link](https://www.sciencedirect.com/science/article/pii/S0167865521000556?via%3Dihub)


The proposed SmaAt-UNet can be found in the model-folder under [SmaAt_UNet](https://github.com/HansBambel/SmaAt-UNet/blob/master/models/SmaAt_UNet.py).

**>>>IMPORTANT<<<**

The original Code from the paper can be found in this branch: [https://github.com/HansBambel/SmaAt-UNet/tree/snapshot-paper](https://github.com/HansBambel/SmaAt-UNet/tree/snapshot-paper)

The current master branch has since upgraded packages and was refactored. Since the exact package-versions differ the experiments may not be 100% reproducible.

If you have problems running the code, feel free to open an issue here on Github.

### Precipitation dataset

The dataset consists of precipitation maps in 5-minute intervals from 2016-2019 resulting in about 420,000 images.

The dataset is based on radar precipitation maps from the [The Royal Netherlands Meteorological Institute (KNMI)](https://www.knmi.nl/over-het-knmi/about).