

https://huggingface.co/hf-ai/OpenCastKit


This is an open-source solutions of global data-driven high-resolution weather forecasting, implemented and improved by [High-Flyer AI](https://www.high-flyer.cn/). It can compare with the ECMWF Integrated Forecasting System (IFS).

