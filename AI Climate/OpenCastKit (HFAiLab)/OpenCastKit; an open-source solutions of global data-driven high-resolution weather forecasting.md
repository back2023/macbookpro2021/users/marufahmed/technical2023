
https://github.com/HFAiLab/OpenCastKit/blob/master/README_en.md

English | [简体中文](https://github.com/HFAiLab/OpenCastKit/blob/master/README.md)

This is an open-source solutions of global data-driven high-resolution weather forecasting, implemented and improved by [High-Flyer AI](https://www.high-flyer.cn/). It can compare with the ECMWF Integrated Forecasting System (IFS).

The model weights trained on the ERA5 data from 1979-01 to 2022-12 are released at [Hugging Face repository](https://huggingface.co/hf-ai/OpenCastKit). You can also have a look at [HF-Earth](https://www.high-flyer.cn/hf-earth/), a daily updated demo of weather prediction.


## Training

The raw data is from the public dataset, [ERA5](https://www.ecmwf.int/en/forecasts/datasets/reanalysis-datasets/era5) . We can use the script `data_factory/convert_ear5_hourly.py` to fetch featuers and convert them into the high-performance sample data of [FFRecord](https://www.high-flyer.cn/blog/ffrecord/) format.

### [](https://github.com/HFAiLab/OpenCastKit/blob/master/README_en.md#fourcastnet-training)FourCastNet training

Run locally：

```shell
   python train_fourcastnet.py --pretrain-epochs 100 --fintune-epochs 40 --batch-size 4
```

We can conduct data-parallel training on the Yinghuo HPC:

```shell
   hfai python train_fourcastnet.py --pretrain-epochs 100 --fintune-epochs 40 --batch-size 4 -- -n 12 --name train_fourcastnet
```

### [](https://github.com/HFAiLab/OpenCastKit/blob/master/README_en.md#graphcast-training)GraphCast training

Run locally：

```shell
   python train_graphcast.py --epochs 200 --batch-size 2
```

We can conduct pipeline-parallel training on the Yinghuo HPC:

```shell
   hfai python train_graphcast.py --epochs 200 --batch-size 2 -- -n 32 --name train_graphcast
```

