
https://www.ecmwf.int/en/computing/software/ecmwf-web-api


The ECMWF Web API enables you to programmatically request and retrieve data via HTTP from the ECMWF data archive for use in your web, mobile, or desktop applications.

The data request is made using the ECMWF MARS scripting language and the data is received as NetCDF, GRIB or json, depending on the API service used.




