
[Stephan Rasp](https://agupubs.onlinelibrary.wiley.com/authored-by/Rasp/Stephan), [Nils Thuerey](https://agupubs.onlinelibrary.wiley.com/authored-by/Thuerey/Nils)

https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2020MS002405

Numerical weather prediction has traditionally been based on the models that discretize the dynamical and physical equations of the atmosphere. Recently, however, the rise of deep learning has created increased interest in purely data-driven medium-range weather forecasting with first studies exploring the feasibility of such an approach. To accelerate progress in this area, the WeatherBench benchmark challenge was defined. Here, we train a deep residual convolutional neural network (Resnet) to predict geopotential, temperature and precipitation at 5.625° resolution up to 5 days ahead. To avoid overfitting and improve forecast skill, we pretrain the model using historical climate model output before fine-tuning on reanalysis data. The resulting forecasts outperform previous submissions to WeatherBench and are comparable in skill to a physical baseline at similar resolution. We also analyze how the neural network creates its predictions and find that, for the case studies analyzed, the model has learned physically reasonable correlations. Finally, we perform scaling experiments to estimate the potential skill of data-driven approaches at higher resolutions.

## Key Points

- A large convolutional neural network is trained for the WeatherBench challenge
    
- Pretraining on climate model data improves skill and prevents overfitting
    
- The model sets a new state-of-the-art for data-driven medium-range forecasting


### Data-driven medium-range weather prediction with a Resnet pretrained on climate simulations: A new model for WeatherBench 

https://arxiv.org/pdf/2008.08626.pdf

Stephan Rasp1∗ , and Nils Thuerey1 
1Department of Informatics, Technical University of Munich, Germany 
Key Points: 
• A large convolutional neural network is trained for the WeatherBench challenge. 
• Pretraining on climate model data improves skill and prevents overfitting. 
• The model sets a new state-of-the art for data-driven medium-range forecasting.