
Plan: Copy existing file and test

### First model, the Pangu-Weather
```bash
cd /scratch/fp0/mah900/Pangu-Weather
cp Pangu-07.ipynb /scratch/fp0/mah900/NCI_ai_models/
# Rename `Pangu-07.ipynb` to `Pangu-07 (copy-2023.10.23).ipynb`
```

Notes: 
- Pangu-Weather is using GPU
- With 7day, 28 steps  (Increasing RMSE - so working)
	-  "2018", month = "9", day  = 1, hour = "0", roll_lon = int(1440/2)
- Next Try, 10 days, 40 steps  (Increasing RMSE - so working)
	-  "2018", month = "9", day  = 1, hour = "0", roll_lon = int(1440/2)
- Next Try 2022,  (Seems RMSE not increasing much  after 7 days)
	-  "2022", month = "1", day  = 1, hour = "0", 
```bash

2022-01-01T06:00:00.00
...
2022-01-11T00:00:00.00
...
40 40
```
- Next Try, 14 days, 56 steps. (Okay, RMSE goes flat after 7 day, 10 days and so on, but starts increasing slowly, later.  )
```bash
...
2022-01-15T00:00:00.00
(4, 721, 1440) (5, 13, 721, 1440)
Done
(4, 721, 1440) (5, 13, 721, 1440)
56 56
```
- Next Try 21 day, 84 steps, (RMSE starts to fall after 432 hours)
```bash
...
20220101-20220131
2022-01-22T00:00:00.00
(4, 721, 1440) (5, 13, 721, 1440)
Done
(4, 721, 1440) (5, 13, 721, 1440)
84 84
```
Error - Kernel restart during prediction. (Memory error) (Think about switch to 2 v100 node????)
Next Try, prediction first, done, 
Then, try loading ground truth. 
One GPU works after restart, visualisation is little unresponsive

- Next Try, 28 days, 112 steps (Okay, goes down, then up, but stay within a range. )
```bash
...
2022-01-29T00:00:00.00
(4, 721, 1440) (5, 13, 721, 1440)
Done
(4, 721, 1440) (5, 13, 721, 1440)
112 112
```
Kernel restart, during prediction.
Worked after restart
```
Done
(112, 5, 13, 721, 1440) (112, 4, 721, 1440)
2022-01-01T00:00:00.00 : 112 112
```

Next Plan, Try with different moths 

Error-1
```bash
---> 11     if (year%4 == 0 and year%100 != 0) or (year%400 == 0) :
...
TypeError: not all arguments converted during string formatting
```
- year is not int
Change code in the file `Pangu-07 (copy-2023.10.23).ipynb`, 
year is changed to int, and later string formatting is used during Xarray dataset opening. 
```python
    year  = int (year)
...
    msl_path = f'/g/data/rt52/era5/single-levels/reanalysis/msl/{year}/msl_era5_oper_sfc_{file_suffix}.nc'
    u10_path = f'/g/data/rt52/era5/single-levels/reanalysis/10u/{year}/10u_era5_oper_sfc_{file_suffix}.nc'
    v10_path = f'/g/data/rt52/era5/single-levels/reanalysis/10v/{year}/10v_era5_oper_sfc_{file_suffix}.nc'
    t2m_path = f'/g/data/rt52/era5/single-levels/reanalysis/2t/{year}/2t_era5_oper_sfc_{file_suffix}.nc'
...
    z_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/z/{year}/z_era5_oper_pl_{file_suffix}.nc'
    q_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/q/{year}/q_era5_oper_pl_{file_suffix}.nc'
    t_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/t/{year}/t_era5_oper_pl_{file_suffix}.nc'
    u_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/u/{year}/u_era5_oper_pl_{file_suffix}.nc'
    v_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/v/{year}/v_era5_oper_pl_{file_suffix}.nc'

```
- Error-2
```bash
KeyError: "not all values found in index 'time'. Try setting the `method` keyword argument (example: method='nearest')."
```
Solution (Try): 
- Need to change code so that change of month works. 

***
***
Add extra code from `scratch/fp0/mah900/ai_models_module/ai_models_era5.py`
```python
def is_leap_year (year):
    if year % 100 == 0 and year % 400 == 0 :
        return 1
    elif year % 4 == 0 :
        return 1
    else:
        return 0      

def get_month_days(year, month):
    month_days = [31,28,31,30,31,30,31,31,30,31,30,31]
    leap = 0
    if month == 2:
        leap = is_leap_year (year)
    return (month_days[month-1]+leap)       
```

***
Modify existing function. 
```python
# 2023.08.07 - Copy from Pangu-06
# 2023.10.23 - Copy from Pangu-07

def create_input_panguweather (year, month, day, hour, roll_lon): 

    m_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    month = int (month)
    year  = int (year)
    if month != 2 :
        m_total_days =  m_days[month-1]
    else :
        if (year%4 == 0 and year%100 != 0) or (year%400 == 0) :
            m_total_days =  m_days[month-1] + 1
        else :
            m_total_days =  m_days[month-1] 
    
    file_suffix = "{:d}{:02d}{:02d}-{:d}{:02d}{:02d}" .format(int(year), int(month), 1, int(year), int(month), int(m_total_days))
    date_time   = "{:d}-{:02d}-{:02d}T{:02d}:00:00.00".format(int(year), int(month), int(day), int(hour))
    print (file_suffix)
    print (date_time)
    
    msl_path = f'/g/data/rt52/era5/single-levels/reanalysis/msl/{year}/msl_era5_oper_sfc_{file_suffix}.nc'
    u10_path = f'/g/data/rt52/era5/single-levels/reanalysis/10u/{year}/10u_era5_oper_sfc_{file_suffix}.nc'
    v10_path = f'/g/data/rt52/era5/single-levels/reanalysis/10v/{year}/10v_era5_oper_sfc_{file_suffix}.nc'
    t2m_path = f'/g/data/rt52/era5/single-levels/reanalysis/2t/{year}/2t_era5_oper_sfc_{file_suffix}.nc'

    if (not os.path.exists(msl_path)) or (not os.path.exists(u10_path)) or (not os.path.exists(v10_path)) or  (not os.path.exists(t2m_path)):
        print ("Surface File(s) don't exist")
        return   

    msl_data = xr.open_dataset(msl_path)
    u10_data = xr.open_dataset(u10_path)
    v10_data = xr.open_dataset(v10_path)
    t2m_data = xr.open_dataset(t2m_path)

    surface1 =  msl_data['msl'].sel(time=date_time).values 
    surface2 =  u10_data['u10'].sel(time=date_time).values 
    surface3 =  v10_data['v10'].sel(time=date_time).values 
    surface4 =  t2m_data['t2m'].sel(time=date_time).values  

    surface1 = np.roll( surface1 , roll_lon)
    surface2 = np.roll( surface2 , roll_lon)
    surface3 = np.roll( surface3 , roll_lon)
    surface4 = np.roll( surface4 , roll_lon)
  
    NCI_surface  =  np.array([surface1, surface2, surface3, surface4], np.float32)
    print(NCI_surface.shape, end= " ")
   
    z_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/z/{year}/z_era5_oper_pl_{file_suffix}.nc'
    q_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/q/{year}/q_era5_oper_pl_{file_suffix}.nc'
    t_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/t/{year}/t_era5_oper_pl_{file_suffix}.nc'
    u_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/u/{year}/u_era5_oper_pl_{file_suffix}.nc'
    v_path = f'/g/data/rt52/era5/pressure-levels/reanalysis/v/{year}/v_era5_oper_pl_{file_suffix}.nc'

    if (not os.path.exists(z_path)) or (not os.path.exists(q_path)) or (not os.path.exists(t_path)) or (not os.path.exists(u_path)) or (not os.path.exists(v_path)):
        print ("Upper File(s) don't exist")
        return    

    z_data = xr.open_dataset(z_path)
    q_data = xr.open_dataset(q_path)
    t_data = xr.open_dataset(t_path)
    u_data = xr.open_dataset(u_path)
    v_data = xr.open_dataset(v_path)

    upper1 = z_data['z'].sel(time=date_time, level = [1000, 925, 850, 700, 600, 500, 400, 300, 250, 200, 150, 100, 50]).values 
    upper2 = q_data['q'].sel(time=date_time, level = [1000, 925, 850, 700, 600, 500, 400, 300, 250, 200, 150, 100, 50]).values 
    upper3 = t_data['t'].sel(time=date_time, level = [1000, 925, 850, 700, 600, 500, 400, 300, 250, 200, 150, 100, 50]).values 
    upper4 = u_data['u'].sel(time=date_time, level = [1000, 925, 850, 700, 600, 500, 400, 300, 250, 200, 150, 100, 50]).values 
    upper5 = v_data['v'].sel(time=date_time, level = [1000, 925, 850, 700, 600, 500, 400, 300, 250, 200, 150, 100, 50]).values 

    upper1 = np.roll( upper1, roll_lon)
    upper2 = np.roll( upper2, roll_lon)
    upper3 = np.roll( upper3, roll_lon)
    upper4 = np.roll( upper4, roll_lon)
    upper5 = np.roll( upper5, roll_lon)
 
    NCI_upper  = np.array([upper1, upper2, upper3, upper4, upper5] , np.float32)
    print(NCI_upper.shape)

    print("Done")
    return  date_time, NCI_surface, NCI_upper
```


```python
g_truth_range = 112 # 84 # 40 #28
g_t_surface   = [None for x in range(g_truth_range)]
g_t_upper     = [None for x in range(g_truth_range)]
g_t_date_times= [None for x in range(g_truth_range)]

day  = 1
hour = 6
year = 2022
month= 2
for i in range(g_truth_range):    
    g_t_date_times[i], g_t_surface[i], g_t_upper[i] = create_input(year = year, month = month,
                                                                    day = day, hour = hour, roll_lon = int(1440/2))
   
    print(g_t_surface[i].shape, g_t_upper[i].shape) 
    hour += 6
    if hour%24 == 0:
        hour = 0
        day += 1
    if day > get_month_days(year, month):
        month += 1
        day = 1
    if month > 12:    
        year += 1
        month = 1  

print ( len(g_t_surface), len(g_t_upper) )
```
***
```python
# N-days, 6 hours forecast, panguweather
# 6 hours forecast
#%%time
os.chdir('/g/data/wb00/admin/testing/Pangu-Weather/root')

def pangu_inference_6(input, input_surface, pred_range):
    
    model_24 = onnx.load('pangu_weather_24.onnx')
    model_6  = onnx.load('pangu_weather_6.onnx')
    # Set the behavier of onnxruntime
    options = ort.SessionOptions()
    options.enable_cpu_mem_arena = False
    options.enable_mem_pattern   = False
    options.enable_mem_reuse     = False
    # Increase the number for faster inference and more memory consumption
    options.intra_op_num_threads = 1
    # Set the behavier of cuda provider
    cuda_provider_options = {'arena_extend_strategy':'kSameAsRequested',}
    
    # Initialize onnxruntime session for Pangu-Weather Models
    ort_session_24 = ort.InferenceSession('pangu_weather_24.onnx', 
                                          sess_options=options, 
                                          providers=[('CUDAExecutionProvider', cuda_provider_options)])    
    ort_session_6  = ort.InferenceSession('pangu_weather_6.onnx', 
                                          sess_options=options, 
                                          providers=[('CUDAExecutionProvider', cuda_provider_options)])
    
    # Run the inference session
    a = [None for x in range(pred_range)]
    b = [None for x in range(pred_range)]
    input_24, input_surface_24 = input, input_surface   
    for i in range(pred_range):
        if (i+1) % 4 == 0:
            print (i+1, "24h") 
            output,   output_surface   = ort_session_24.run(None, {'input':input_24, 'input_surface':input_surface_24})
            input_24, input_surface_24 = output, output_surface
        else:
            print (i+1, "6h") 
            output, output_surface = ort_session_6.run(None, {'input':input, 'input_surface':input_surface})
        input, input_surface = output, output_surface
        a[i] = output
        b[i] = output_surface  
    
    all_output_upper   = np.stack(tuple(a[i] for i in range(pred_range)), axis = 0)
    all_output_surface = np.stack(tuple(b[i] for i in range(pred_range)), axis = 0)
    print("Done")
    print (all_output_upper.shape, all_output_surface.shape)

    return all_output_upper, all_output_surface
```

```python
# Run prediciton
input_date , input_surface, input_upper = create_input_panguweather(year = 2022, month = 2,
                                                day  = 1, hour = "0", roll_lon = int(1440/2))
#all_output_upper_3x24, all_output_surface_3x24 = pangu_inference_24(input_upper, input_surface, 3)
all_output_upper_40x6, all_output_surface_40x6 = pangu_inference_6(input_upper, input_surface, 112)
print (input_date,":", len (all_output_upper_40x6), len ( all_output_surface_40x6) )
```
***
***

- Next Try, Month Feb, (year = 2022, month = 2, day  = 1, hour = "0", roll_lon = int(1440/2)) (Run one 1 v100, after restart)
	- After 360-70 hours, RMSE fluctuate, goes down and up. 
- Next Try, Month March (year = 2022, month = 3, day  = 1, hour = "0", roll_lon = int(1440/2) (Run one 1 v100, after restart)
	- Increase until 408 hours, then fall until 504 hours, then within a narrow range 

###### Next Plan, Try making graphcast input





