

https://github.com/andregraubner/ClimateNet



## About

Climate Analytics using Deep Neural Networks in Python.


# ClimateNet

[](https://github.com/andregraubner/ClimateNet#climatenet)

ClimateNet is a Python library for deep learning-based Climate Science. It provides tools for quick detection and tracking of extreme weather events. We also expose models, data sets and metrics to jump-start your research.


