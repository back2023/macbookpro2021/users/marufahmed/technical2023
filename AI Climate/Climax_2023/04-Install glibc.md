
### Install 'GLIBC_2.29' (Not worked)
```bash
cd /scratch/fp0/mah900/ClimaX
wget -c https://ftp.gnu.org/gnu/glibc/glibc-2.29.tar.gz
 
tar -zxvf glibc-2.29.tar.gz

## Error 1
#cd glibc-2.29
#./configure --prefix=/scratch/fp0/mah900/software/glibc-2.29
cd /scratch/fp0/mah900/software/glibc-2.29
/scratch/fp0/mah900/ClimaX/glibc-2.29/configure --prefix=/scratch/fp0/mah900/software/glibc-2.29
# Error 2
make 
#make install
```

https://stackoverflow.com/a/73188338

##### Error 1
configure: error: you must configure in a separate build directory
##### Try (worked)
"In the specific case of `glibc` the folder where we **extract the project** and the folder where we generate our **configuration files** are diferent. So It's necessary to create a folder to hold our generated configuration files."
[build into a specified directory using the "prefix" option](https://stackoverflow.com/questions/51062293/how-do-i-build-into-a-specified-directory-using-the-prefix-option-of-configure)
https://stackoverflow.com/a/74538605

##### Error 2
```
 - < /scratch/z00/mah900/tmp/tmp7bof9wp2/test.c' returned non-zero exit status 1.
make[2]: *** [../Makerules:271: /scratch/fp0/mah900/software/glibc-2.29/tcb-offsets.h] Error 1
make[2]: Leaving directory '/scratch/fp0/mah900/ClimaX/glibc-2.29/csu'
make[1]: *** [Makefile:258: csu/subdir_lib] Error 2
make[1]: Leaving directory '/scratch/fp0/mah900/ClimaX/glibc-2.29'
make: *** [Makefile:9: all] Error 2
```

#### Try (Not Work)
```bash
rm -r /scratch/fp0/mah900/software/glibc-2.29/*
cd /scratch/fp0/mah900/ClimaX/glibc-2.29
mkdir glibc-2.29-build
cd glibc-2.29-build
../configure --prefix=/scratch/fp0/mah900/software/glibc-2.29
 
```
https://access.redhat.com/discussions/3244811
#### The same error
```bash
../sysdeps/nptl/pthread.h:744:47: error: argument 1 of type ‘struct __jmp_buf_tag *’ declared as a pointer [-Werror=array-parameter=]

  744 | extern int __sigsetjmp (struct __jmp_buf_tag *__env, int __savemask) __THROWNL;
```

#### Next try
use the option
```
--disable-werror
 ```
[glibc Missing From Older Linux Distributions](https://community.fortra.com/kb-nav/kb-article/?id=3cbb3322-1ecb-ec11-a7b5-0022480b2f00&redirect=false)
(**Last Modified On:** 5/5/2022)

```bash
# Clean dirs
rm -r /scratch/fp0/mah900/software/glibc-2.29/*
rm -r /scratch/fp0/mah900/ClimaX/glibc-2.29/glibc-2.29-build/*

# Then, continue as before
cd /scratch/fp0/mah900/ClimaX/glibc-2.29/glibc-2.29-build/
"../configure"  --disable-werror "--prefix=/scratch/fp0/mah900/software/glibc-2.29"
make
...
make install
...

# Eorror 1
#export LD_LIBRARY_PATH=/scratch/fp0/mah900/software/glibc-2.29/lib:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH="/scratch/fp0/mah900/software/glibc-2.29/lib:/usr/lib64/" 

```

##### Error 1
```
Inconsistency detected by ld.so: dl-call-libc-early-init.c: 37: _dl_call_libc_early_init: Assertion `sym != NULL' failed!
```

NOT working, all dir need to be cleaned, later

=================================================================

### Next , Try conda option

[Fixes for GLIBC errors when installing tensorflow or pytorch on older Red Hat or CentOS cluster environments](https://gist.github.com/michaelchughes/85287f1c6f6440c060c3d86b4e7d764b

#### 2.29 missing in the repo



# NOT going to work with this anymore
* Solve the `glibc` problem by using `pip` instead of `conda`

### Some more links that are not fully explored 
* ###### Install working tensorflow or pytorch via standard conda environment workflow.
  https://gist.github.com/michaelchughes/85287f1c6f6440c060c3d86b4e7d764b
*  [Error while importing Tensorflow in Python 2.7 in Ubuntu 12.04. 'GLIBC_2.17 not found'](https://stackoverflow.com/questions/33655731/error-while-importing-tensorflow-in-python-2-7-in-ubuntu-12-04-glibc-2-17-not)
* 'Some reported that their systems crashed after using this answer. So anyone who is trying this, please be careful and make sure you create backups before trying it.'
  [How to install a libc6 version >= 2.29?](https://askubuntu.com/questions/1143268/how-to-install-a-libc6-version-2-29) 
*  Some links to download glibc code and builds
  http://mirrors.edge.kernel.org/ubuntu/pool/main/g/glibc/
  https://packages.debian.org/sid/amd64/libc6/download
  https://answers.launchpad.net/ubuntu/+source/glibc/2.29-0ubuntu2/+build/16599428
  https://packages.ubuntu.com/focal/amd64/libc6/download
* 


  