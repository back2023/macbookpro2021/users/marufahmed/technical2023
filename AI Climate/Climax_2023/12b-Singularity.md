##### Idea:
Add prediction code to the scource code of my git fork. 
Build new container with prediction support

##### Setup: (2023.03.03)
```bash
 cd /scratch/fp0/mah900/
 git clone https://github.com/mahm1846/ClimaX_maruf.git
 cd ClimaX_maruf/
 git remote show origin
```
###### Folder path:
`/scratch/fp0/mah900/ClimaX_maruf`

### Changes 
###### 1. Copy and Create Config file `prediction.yaml`
```
cp global_forecast_climax.yaml gprediction.yaml
```

Chage  file `global_forecast_climax.yaml`
```bash
#- class_path: pytorch_lightning.callbacks.RichModelSummary
# init_args:
# max_depth: -1
#
#- class_path: pytorch_lightning.callbacks.RichProgressBar
```

###### 2. Copy and create singularity def file.  (2023.03.05)
```bash
cp /scratch/fp0/mah900/ClimaX_sing/docker/Singularity.2023.02.28.def. \
	/scratch/fp0/mah900/ClimaX_maruf/docker/Singularity.2023.03.05.def 
```
And, change code
```bash
# lines 33-44
\
git clone https://github.com/mahm1846/ClimaX_maruf.git && \
cd ClimaX_maruf && \
\
export PIP_NO_CACHE_DIR=off && \
. /opt/miniconda/etc/profile.d/conda.sh && \
conda update conda && \
conda env create -f docker/environment.yml && \
conda activate climaX && \
pip install -e . && \
conda clean -ay && \
echo ". /opt/miniconda/etc/profile.d/conda.sh" >> $SINGULARITY_ENVIRONMENT 
# apt-get autoremove && apt-get autoclean && conda clean -ay && \
```


###### 3. Change `environment.yml` file, add packages. 
`/scratch/fp0/mah900/ClimaX_maruf/docker/environment.yml`
```
Conda 
	- jupyterlab=3.6
pip
	#- --no-cache-dir
	- "rich[jupyter]"
```
Pip no cache environment file
(Does not work) Source: https://github.com/conda/conda/issues/6805#issuecomment-515682512

###### 4. Source dir 
Folder: `/scratch/fp0/mah900/ClimaX_maruf/src/climax`
Datamodule file: `/scratch/fp0/mah900/ClimaX_maruf/src/climax/global_forecast/datamodule.py`
```bash
# line, 66
self.lister_pred = list(dp.iter.FileLister(os.path.join(root_dir, "pred")))

# line, 73
self.pred_clim = self.get_climatology("pred", out_variables)

# line, 78
self.data_pred: Optional[IterableDataset] = None

# line, 172-191
       if not self.data_pred: 

           self.data_pred = IndividualForecastDataIter(
                Forecast(
                    NpyReader(
                        file_list=self.lister_pred,
                        start_idx=0,
                        end_idx=1,
                        variables=self.hparams.variables,
                        out_variables=self.hparams.out_variables,
                        shuffle=False,
                        multi_dataset_training=False,
                    ),
                    max_predict_range=self.hparams.predict_range,
                    random_lead_time=False,
                    hrs_each_step=self.hparams.hrs_each_step,
                ),
                transforms=self.transforms,
                output_transforms=self.output_transforms,
            )

# line 225
    def predict_dataloader(self):
        return DataLoader(
            self.data_pred,
            batch_size=self.hparams.batch_size,
            shuffle=False,
            drop_last=False,
            num_workers=self.hparams.num_workers,
            pin_memory=self.hparams.pin_memory,
            collate_fn=collate_fn,
        )     

```

Source Module file: `/scratch/fp0/mah900/ClimaX/src/climax/global_forecast/module.py`
Updated Module file: `/scratch/fp0/mah900/ClimaX_maruf/src/climax/global_forecast/module.py`
```bash
# line, 98
def set_pred_clim(self, clim):
	self.pred_clim = clim

# line, 118-125
    def predict_step(self, batch: Any, batch_idx: int):
        x, y, lead_times, variables, out_variables = batch

        loss_dict, pred = self.net.forward(x, y, lead_times, variables, out_variables, [lat_weighted_mse], lat=self.lat)
        loss_dict = loss_dict[0]
        loss = loss_dict["loss"]
        
        return x,y, pred
```


Prediction file: 
Copy:
```
cp /scratch/fp0/mah900/ClimaX/src/climax/global_forecast/predict_t.py /scratch/fp0/mah900/ClimaX_maruf/src/climax/global_forecast/predict.py
```
Update:
```bash
# lines, 40-48 
   # pred
    #x, y, predictions = 
    pred = cli.trainer.predict(cli.model, datamodule=cli.datamodule, return_predictions=True)
    path='/scratch/fp0/shared/climax_train_global/pred'
    import torch
    torch.save(pred[0][0], path+ '/x.pt')
    torch.save(pred[0][1], path+ '/y.pt')
    torch.save(pred[0][2], path+ '/preds.pt')
    ####################################
```


A quick check of the Conda jupyter version.
and, then Used  `jupyterlab=3.6`
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
cd /scratch/fp0/mah900/ClimaX

jupyter --version
Selected Jupyter core packages...
IPython          : 8.10.0
ipykernel        : 6.21.2
ipywidgets       : not installed
jupyter_client   : 8.0.2
jupyter_core     : 5.2.0
jupyter_server   : 2.3.0
jupyterlab       : 3.6.1
nbclient         : 0.7.2
nbconvert        : 7.2.9
nbformat         : 5.7.3
notebook         : 6.5.2
qtconsole        : not installed
traitlets        : 5.9.0
```



### Build
###### Commit and push
```bash
cd /scratch/fp0/mah900/ClimaX_maruf
git add .
git commit -m "2023.03.05"
git push origin main
```
Error-1
```bash
remote: Permission to mahm1846/ClimaX_maruf.git denied to maruf-anu.
fatal: unable to access 'https://github.com/mahm1846/ClimaX_maruf.git/': The requested URL returned error: 403
```
Try new Token:
Source: https://www.shellhacks.com/git-config-username-password-store-credentials/
```bash
git remote set-url origin https://mahm1846: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@github.com/mahm1846/ClimaX_maruf.git
```
(Working)

######  Remote build 
```bash
cd /scratch/fp0/mah900/ClimaX_maruf
module load singularity
export SINGULARITY_CACHEDIR=/tmp 
singularity -vv build  --remote ClimaX.sif docker/Singularity.2023.03.05.def
```
Error-1
```bash
ResolvePackageNotFound: 
  - scipy==1.10.0=py38h14f4228_0
```
Try-1
```
  - scipy==1.10
```
Reason: was still pulling old file from the Microsoft account
```bash
#line, 38
wget -v https://raw.githubusercontent.com/microsoft/ClimaX/main/docker/environment.yml && \
```
Solution: changed code above
Error-2
```bash
+ apt-get autoremove
Reading package lists...
Building dependency tree...
Reading state information...
The following packages will be REMOVED:
  libgd3 libnginx-mod-http-image-filter libnginx-mod-http-xslt-filter
  libnginx-mod-mail libnginx-mod-stream libxpm4 libxslt1.1
0 upgraded, 0 newly installed, 7 to remove and 0 not upgraded.
After this operation, 1633 kB disk space will be freed.
Do you want to continue? [Y/n] Abort.
FATAL:   While performing build: while running engine: exit status 1
FATAL:   While performing build: build image size <= 0
```
Try-1
removed autoclean above
```
# apt-get autoremove && apt-get autoclean && conda clean -ay && \
```
Error-3
```bash
ERROR: Invalid requirement: --no-cache-dir           
__main__.py: error: no such option: --no-cache-dir
```
Removed the line 
```
#- --no-cache-dir
```

Error-4
```bash
+ PS1=(climaX) Singularity> 
+ export PATH=/opt/miniconda/envs/climaX/bin:/opt/miniconda/condabin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ export CONDA_PREFIX=/opt/miniconda/envs/climaX
+ export CONDA_SHLVL=1
+ export CONDA_DEFAULT_ENV=climaX
+ export CONDA_PROMPT_MODIFIER=(climaX) 
+ export CONDA_EXE=/opt/miniconda/bin/conda
+ export _CE_M=
+ export _CE_CONDA=
+ export CONDA_PYTHON_EXE=/opt/miniconda/bin/python
...
INFO:    Uploading 8558305280 bytes
...
FATAL:   Unable to push image to library: sending file did not succeed: upload image blob failed: Patch "https://harbor.sylabs.io/v2/2005gre/remote-builds/rb-6404471ac4b9bfc24f01fd30/blobs/uploads/7c39ca5f-2915-44c2-b7ba-b0c91857a084?_state=N-m9zkQnJL1hFaxwSH64tnLFFcPkZECJd-LYg1YWALN7Ik5hbWUiOiIyMDA1Z3JlL3JlbW90ZS1idWlsZHMvcmItNjQwNDQ3MWFjNGI5YmZjMjRmMDFmZDMwIiwiVVVJRCI6IjdjMzljYTVmLTI5MTUtNDRjMi1iN2JhLWIwYzkxODU3YTA4NCIsIk9mZnNldCI6NTIzNDQ5MTM5MiwiU3RhcnRlZEF0IjoiMjAyMy0wMy0wNVQwODoyMjoxMloifQ%3D%3D": http2: Transport: cannot retry err [http2: Transport received Server's graceful shutdown GOAWAY] after Request.Body was written; define Request.GetBody to avoid this error (500 Internal Server Error)

FATAL:   While performing build: build image size <= 0
```


###### Next try, add `--no-cache-dir` (NOT worked)
```
#line, 42
pip install --no-cache-dir -e . && \

Selec multi line, Mac: ⌥ Opt+⌘ Cmd+↑/↓
add : --no-cache-dir
```
source: https://stackoverflow.com/a/30039968

###### Next build try
```bash
screen
cd /scratch/fp0/mah900/ClimaX_maruf
module load singularity
export SINGULARITY_CACHEDIR=/tmp 
singularity -vv build  --remote ClimaX.sif docker/Singularity.2023.03.05.def
```
Output
```bash
INFO:    Calculating SIF image checksum
INFO:    Uploading image to library...
WARNING: Skipping container verification
INFO:    Uploading 8558309376 bytes
INFO:    Image uploaded successfully.
INFO:    Build complete: ClimaX.sif
```

###### Import Test [[05-Test Import]]
Error-1
```
Singularity> from torchdata import _torchdata as _torchdata
from: can't read /var/mail/torchdata
```
Reason: Did not start python
https://stackoverflow.com/questions/16069816/getting-python-error-from-cant-read-var-mail-bio
Backup: Download from the `sylab.io` site 
```bash
singularity pull library://2005gre/remote-builds/rb-64045c84c4b9bfc24f01fd34
```

All import success. 


### Singularity open ondemand, try idea (NOT work)
```
module load singularity &&  /scratch/fp0/mah900/ClimaX_maruf/ClimaX.sif && . /opt/miniconda/

singularity shell /scratch/fp0/mah900/ClimaX_maruf/ClimaX.sif
```
Does not work with ARE

[[12c-singularity-Native command]]


### Another Try, build (2023.03.06-5:00 pm)
Add IPython and env variables
```bash
Bootstrap: docker
From: mcr.microsoft.com/azureml/openmpi4.1.0-cuda11.3-cudnn8-ubuntu20.04:latest
Stage: build

%post
# tags: https://catalog.ngc.nvidia.com/orgs/nvidia/containers/pytorch/tags?quick-deploy=false

DEBIAN_FRONTEND=noninteractive TZ=Australia/ACT \
apt-get update && apt-get install -y --allow-downgrades --allow-change-held-packages --no-install-recommends \
build-essential \
cmake \
g++-7 \
git \
gpg \
curl \
vim \
wget \
ca-certificates \
libjpeg-dev \
libpng-dev \
librdmacm1 \
libibverbs1 \
ibverbs-providers \
openssh-client \
openssh-server \
libsm6 \
libxext6 \
ffmpeg \
libfontconfig1 \
libxrender1 \
libgl1-mesa-glx && \
apt-get clean && rm -rf /var/lib/apt/lists/* && \
\
git clone https://github.com/mahm1846/ClimaX_maruf.git && \
cd ClimaX_maruf && \
\
export PIP_NO_CACHE_DIR=1 && \
. /opt/miniconda/etc/profile.d/conda.sh && \
conda update conda && \
conda env create -f docker/environment.yml && \
conda activate climaX && \
pip install --no-cache-dir -e . && \
conda clean -ay && \
echo ". /opt/miniconda/etc/profile.d/conda.sh" >> $SINGULARITY_ENVIRONMENT && \
python -m ipykernel install --name=climaX

# apt-get autoremove && apt-get autoclean && conda clean -ay && \

%environment
export PS1='(climaX) Singularity> '
export PATH=/opt/miniconda/envs/climaX/bin:/opt/miniconda/condabin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export PATH=/opt/miniconda/envs/climaX/bin/:$PATH
export CONDA_PREFIX=/opt/miniconda/envs/climaX
export CONDA_SHLVL=1
export CONDA_DEFAULT_ENV=climaX
export CONDA_PROMPT_MODIFIER='(climaX) '
export CONDA_EXE=/opt/miniconda/bin/conda
export _CE_M=''
export _CE_CONDA=''
export CONDA_PYTHON_EXE=/opt/miniconda/bin/python

%runscript
exec /bin/bash "$@"
%startscript
exec /bin/bash "$@"

```

```bash
cd /scratch/fp0/mah900/ClimaX_maruf
module load singularity
export SINGULARITY_CACHEDIR=/tmp 
singularity -vv build  --remote ClimaX.sif docker/Singularity.2023.03.05.def
```

Output: conda env related
```bash
+ conda activate climaX
+ local cmd=activate
+ __conda_activate activate climaX
+ [ -n  ]
+ local ask_conda
+ PS1=Singularity>  __conda_exe shell.posix activate climaX
+ /opt/miniconda/bin/conda shell.posix activate climaX
+ ask_conda=PS1='(climaX) Singularity> '
export PATH='/opt/miniconda/envs/climaX/bin:/opt/miniconda/condabin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
export CONDA_PREFIX='/opt/miniconda/envs/climaX'
export CONDA_SHLVL='1'
export CONDA_DEFAULT_ENV='climaX'
export CONDA_PROMPT_MODIFIER='(climaX) '
export CONDA_EXE='/opt/miniconda/bin/conda'
export _CE_M=''
export _CE_CONDA=''
export CONDA_PYTHON_EXE='/opt/miniconda/bin/python'
. "/opt/miniconda/envs/climaX/etc/conda/activate.d/esmpy-activate.sh"
. "/opt/miniconda/envs/climaX/etc/conda/activate.d/libblas_mkl_activate.sh"
+ eval PS1='(climaX) Singularity> '
export PATH='/opt/miniconda/envs/climaX/bin:/opt/miniconda/condabin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
export CONDA_PREFIX='/opt/miniconda/envs/climaX'
export CONDA_SHLVL='1'
export CONDA_DEFAULT_ENV='climaX'
export CONDA_PROMPT_MODIFIER='(climaX) '
export CONDA_EXE='/opt/miniconda/bin/conda'
export _CE_M=''
export _CE_CONDA=''
export CONDA_PYTHON_EXE='/opt/miniconda/bin/python'
. "/opt/miniconda/envs/climaX/etc/conda/activate.d/esmpy-activate.sh"
. "/opt/miniconda/envs/climaX/etc/conda/activate.d/libblas_mkl_activate.sh"
+ PS1=(climaX) Singularity> 
+ export PATH=/opt/miniconda/envs/climaX/bin:/opt/miniconda/condabin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ export CONDA_PREFIX=/opt/miniconda/envs/climaX
+ export CONDA_SHLVL=1
+ export CONDA_DEFAULT_ENV=climaX
+ export CONDA_PROMPT_MODIFIER=(climaX) 
+ export CONDA_EXE=/opt/miniconda/bin/conda
+ export _CE_M=
+ export _CE_CONDA=
+ export CONDA_PYTHON_EXE=/opt/miniconda/bin/python
+ . /opt/miniconda/envs/climaX/etc/conda/activate.d/esmpy-activate.sh
+ [ -n  ]
+ [ -f /opt/miniconda/envs/climaX/lib/esmf.mk ]
+ export ESMFMKFILE=/opt/miniconda/envs/climaX/lib/esmf.mk
+ . /opt/miniconda/envs/climaX/etc/conda/activate.d/libblas_mkl_activate.sh
+ export CONDA_MKL_INTERFACE_LAYER_BACKUP=
+ export MKL_INTERFACE_LAYER=LP64,GNU

...

+ echo . /opt/miniconda/etc/profile.d/conda.sh
+ python -m ipykernel install --name=climaX
Installed kernelspec climaX in /usr/local/share/jupyter/kernels/climax

...

WARNING: Skipping container verification
INFO:    Uploading 8528306176 bytes
INFO:    Image uploaded successfully.
INFO:    Build complete: ClimaX.sif
```

Working with Climax kernel insalled. (2023.03.06-7:02)

### Vistualization modules missing
Not found in cond environment file, either
```bash 
# Both missing.
import matplotlib
import holoviews as hv
``` 
Have to do the build again. 
Also, Delete other sif files than the last working one.

### 2023.03.06 - 9:00 pm
Added `matplotlib` and `holoviews` to the environment file 
`/scratch/fp0/mah900/ClimaX_maruf/docker/environment.yml`
```
lines, 193-194
- matplotlib
- "holoviews[recommended]"
```
Build again
```bash
cd /scratch/fp0/mah900/ClimaX_maruf
module load singularity
export SINGULARITY_CACHEDIR=/tmp 
singularity -vv build  --remote ClimaX.sif docker/Singularity.2023.03.05.def
...
WARNING: Skipping container verification
INFO:    Uploading 8571699200 bytes
INFO:    Image uploaded successfully.
INFO:    Build complete: ClimaX.sif
```
