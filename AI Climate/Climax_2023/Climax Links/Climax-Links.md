github
https://github.com/microsoft/ClimaX

ClimaX home (actually inside github)
https://microsoft.github.io/ClimaX/

Paper 
https://arxiv.org/pdf/2301.10343.pdf

Web
Microsoft & UCLA Introduce ClimaX: A Foundation Model for Climate and Weather Modelling
https://medium.com/syncedreview/microsoft-ucla-introduce-climax-a-foundation-model-for-climate-and-weather-modelling-5b3d7304981d

https://syncedreview.com/2023/01/30/microsoft-ucla-introduce-climax-a-foundation-model-for-climate-and-weather-modelling/