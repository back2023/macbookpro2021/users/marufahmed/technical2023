
# Tensorboard

Pytorch lighting
Use the logging dir only, 
version and chkpt are sorted out by Tensorboard
```bash
/g/data/wb00/admin/staging/climax_train_global/
```

#### First, try
```bash
# Start
[mah900@gadi-login-03 staging]$ 
tensorboard --logdir=/g/data/wb00/admin/staging/climax_train_global/ --host localhost --port 6006
# Port forward
ssh -L 6006:gadi-login-03:6006 mah900@gadi.nci.org.au
```
#### Error
Port forwarding did not work
Issue with the host?? Local or a server, must be explicitly stated. 
Tensorboard is ***sensitive to hostname***
https://stackoverflow.com/questions/40106949/unable-to-open-tensorboard-in-browser

#### Next, try (Working)
So, currect hostname is important
```bash
# Start
[mah900@gadi-login-03 staging]$ 
tensorboard --logdir=/g/data/wb00/admin/staging/climax_train_global/ --host gadi-login-03 --port 6007
# Port forward
ssh -v -N -L 6007:gadi-login-03:6007 mah900@gadi.nci.org.au
```
(There is already a page in NCI confluence about tensorboard)


### PBS Script for Tensorboard
```bash
# Copy
cp c1_h12.pbs /scratch/fp0/mah900/ClimaX/tensorboard.pbs

#!/bin/bash
#PBS -S /bin/bash
#PBS -q normal
#PBS -l ncpus=1
#PBS -l jobfs=1GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
#PBS -l mem=1GB
#PBS -l walltime=04:00:00
#PBS -N TB_pbs
#PBS -P fp0
LOG=${LOG_DIR}
#LOG=/g/data/wb00/admin/staging/climax_train_global
HOST=`hostname`

PORT=16006
echo -ne "ssh " 
#echo "-v -N -L ${PORT}:${HOST}:${PORT} ${USER}@gadi.nci.org.au" > ~/port_forward.txt
echo "-v -N -L ${PORT}:${HOST}:${PORT} ${USER}@gadi.nci.org.au" > ${PBS_O_WORKDIR}/port_forward.txt 
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
echo ${LOG_DIR}
tensorboard --logdir="${LOG}" --host ${HOST} --port ${PORT}
# tensorboard --logdir=g/data/wb00/admin/staging/climax_train_global --host ${HOST} --port ${PORT} # does not work either # Error in the path
# tensorboard --logdir="g/data/wb00/admin/staging/climax_train_global" --host ${HOST} --port ${PORT} # does not work either # Error in the path
```
Source: https://opus.nci.org.au/display/Help/Useful+PBS+Environment+Variables
##### Error -1
` line 18: tensorboard: command not found`
##### Error-2
```bash
### No dashboards are active for the current data set.
Probable causes:
-   You haven’t written any data to your event files.
-   TensorBoard can’t find your event files.
```

##### Run
```bash
# qsub -F "LOG_DIR=/g/data/wb00/admin/staging/climax_train_global/" tensorboard.pbs # doesn not work
# qsub -v LOG_DIR='/g/data/wb00/admin/staging/climax_train_global/'  tensorboard.pbs # doesn not work
qsub -v LOG_DIR="/g/data/wb00/admin/staging/climax_train_global"  tensorboard.pbs # WORKED after correcting path and adding disk
qsub tensorboard.pbs # doesnt not work, variable inside script, either. 
qsub -v LOG_DIR="/g/data/wb00/admin/testing/NCI_Climax/Train_Global" tensorboard.pbs
```
Log directory does not work inside the script
if works on cmd but not in script. 
~~Have to look later, after prediction results~~

#### Reason:
* Was error in the log path : `g/data/...`  (Correct is `/g/data/...`)
* Aso, did not load the file in pbs disk: `storage=scratch/fp0+scratch/z00` 
(correct is `scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00`)
Solved




