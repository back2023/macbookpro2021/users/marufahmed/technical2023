Code continued From [[06b-Prediction]]

~~Add clean code from: `/scratch/fp0/mah900/ClimaX/src/climax/` to : `/scratch/fp0/mah900/ClimaX_sing/src/climax/`~~
Start with th previous file (`Climax`), will move to new location later. 

Start with 1 GPU
Run  global `predict_t.py`
```bash
qsub -I ~/conda/g1_h8.pbs
...
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
cd /scratch/fp0/mah900/ClimaX
export OUTPUT_DIR=/scratch/fp0/mah900/climax_train_global

time python \
	src/climax/global_forecast/predict_t.py \
	--config 'configs/global_forecast_climax.yaml' \
	--trainer.num_nodes=1 \
	--trainer.strategy=ddp --trainer.devices=1 \
    --trainer.max_epochs=0 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --data.num_workers=1 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5
```
######  Error-1
```bash
File "src/climax/global_forecast/predict_t.py", line 38, in main
    cli.trainer.test(cli.model, datamodule=cli.datamodule, ckpt_path="best")
...
  File "/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/pytorch_lightning/trainer/connectors/checkpoint_connector.py", line 155, in _set_ckpt_path
    raise ValueError(
ValueError: `.test(ckpt_path="best")` is set but `ModelCheckpoint` is not configured to save the best model.
```
Try (Working)
```bash
# comment off
#cli.trainer.test(cli.model, datamodule=cli.datamodule, ckpt_path="best")
```
###### Error-2
```bash
ValueError: An invalid dataloader was passed to `Trainer.predict(dataloaders=...)`. Either pass the dataloader to the `.predict()` method OR implement `def predict_dataloader(self):` in your LightningModule/LightningDataModule.
```
Reason: incorrect loader name for prediction, corrected.
```bash
# In 'datamodule.py', line 226
def predict_dataloader(self):
```
###### Error-3
```bash
  File "/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/torch/serialization.py", line 211, in __init__
    super(_open_file, self).__init__(open(name, mode))
FileNotFoundError: [Errno 2] No such file or directory: '/scratch/fp0/shared/climax_train_global/pred/x-4.pt'
```
Solution: create the folder beforehand 

###### Prediction folder
works with just one file (64 samples)
```bash
# /g/data/wb00/admin/staging/weatherbench/5.625deg_npz/pred
ls
2018_0.npz  climatology.npz
```

###### Without fit prediction
Working without train, but need to comapre the output.
```
# line 34-35
# fit() runs the training
#cli.trainer.fit(cli.model, datamodule=cli.datamodule)
```

##### MPI works without extra parameter passing


###### To do
1. Set New singularity remote account.
2. Need to jupyterlab, too (not yet in singularity)
3. Add `pip install "rich[jupyter]"` to file
4. Change the `module.py` and `datamodule.py` to add prediction code
5. Add prediction config file with rich progress bar
6. Build and test the container. 