

#####  Check folder and nc files 
```
tree -L 1  /g/data/wb00/admin/staging/weatherbench/5.625deg_npz/
ls -alh  /g/data/wb00/admin/staging/weatherbench/5.625deg_npz/
```

##### npz and npy files inspect
```python
python
from numpy import load
npz_file = '/g/data/wb00/admin/staging/weatherbench/5.625deg_npz/train/1979_0.npz'
with load(npz_file) as data:
	print(list(data.files))
['lsm', 'orography', 'lat2d', 't2m', 'u10', 'v10', 'tisr', 'tp', 'z_50', 'z_250', 'z_500', 'z_600', 'z_700', 'z_850', 'z_925', 'z_1000', 'u_50', 'u_250', 'u_500', 'u_600', 'u_700', 'u_850', 'u_925', 'u_1000', 'v_50', 'v_250', 'v_500', 'v_600', 'v_700', 'v_850', 'v_925', 'v_1000', 't_50', 't_250', 't_500', 't_600', 't_700', 't_850', 't_925', 't_1000', 'r_50', 'r_250', 'r_500', 'r_600', 'r_700', 'r_850', 'r_925', 'r_1000', 'q_50', 'q_250', 'q_500', 'q_600', 'q_700', 'q_850', 'q_925', 'q_1000']
```
https://stackoverflow.com/questions/31368710/how-to-open-an-npz-file

#### To XArray
```python
import numpy as np
import xarray as xr
npz_file = '/g/data/wb00/admin/staging/weatherbench/5.625deg_npz/train/1980_0.npz'

data = np.load(npz_file)
list(data.keys())
lsm = data.f.lsm
lsm.shape
(1095, 1, 32, 64)

lat2d=data.f.lat2d
lat2d.shape
(1095, 1, 32, 64)
...

npy_file = '/g/data/wb00/admin/staging/weatherbench/5.625deg_npz/lat.npy'
data = np.load(npy_file)
list(data)
[-87.1875, -81.5625, -75.9375, -70.3125, -64.6875, -59.0625, -53.4375, -47.8125, -42.1875, -36.5625, -30.9375, -25.3125, -19.6875, -14.0625, -8.4375, -2.8125, 2.8125, 8.4375, 14.0625, 19.6875, 25.3125, 30.9375, 36.5625, 42.1875, 47.8125, 53.4375, 59.0625, 64.6875, 70.3125, 75.9375, 81.5625, 87.1875]

npy_file = '/g/data/wb00/admin/staging/weatherbench/5.625deg_npz/lon.npy'
data = np.load(npy_file)
list(data)
[0.0, 5.625, 11.25, 16.875, 22.5, 28.125, 33.75, 39.375, 45.0, 50.625, 56.25, 61.875, 67.5, 73.125, 78.75, 84.375, 90.0, 95.625, 101.25, 106.875, 112.5, 118.125, 123.75, 129.375, 135.0, 140.625, 146.25, 151.875, 157.5, 163.125, 168.75, 174.375, 180.0, 185.625, 191.25, 196.875, 202.5, 208.125, 213.75, 219.375, 225.0, 230.625, 236.25, 241.875, 247.5, 253.125, 258.75, 264.375, 270.0, 275.625, 281.25, 286.875, 292.5, 298.125, 303.75, 309.375, 315.0, 320.625, 326.25, 331.875, 337.5, 343.125, 348.75, 354.375]

```
https://tristansalles.github.io/EnviReef/5-xarray/basic.html

### Processing start (2023.02.16)
###### Start in a GPU or CPU node

##### 1. Clean previous files
```bash
rm -r /g/data/wb00/admin/staging/weatherbench/5.625deg_npz/
```
##### 2. processing
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
cd /scratch/fp0/mah900/ClimaX

#check file structure
tree -L 1 /g/data/wb00/admin/staging/weatherbench/5.625deg
# Not worked, this time ??

time python src/data_preprocessing/nc2np_equally_era5.py \
    --root_dir /g/data/wb00/admin/staging/weatherbench/5.625deg \
    --save_dir /g/data/wb00/admin/staging/weatherbench/5.625deg_npz \
    --start_train_year 1979 --start_val_year 2016 \
    --start_test_year 2017 --end_year 2019 --num_shards 8

[mah900@gadi-gpu-v100-0093 ClimaX]$
...
real 25m22.111s
user 4m10.250s
sys 14m23.029s

```

#### Check output folder
```bash
tree -L 1  /g/data/wb00/admin/staging/weatherbench/5.625deg_npz/
/g/data/wb00/admin/staging/weatherbench/5.625deg_npz/
├── lat.npy
├── lon.npy
├── normalize_mean.npz
├── normalize_std.npz
├── test
├── train
└── val
```

#### Check file content

##### npz and npy files inspect
```python
python
from numpy import load
npz_file = '/g/data/wb00/admin/staging/weatherbench/5.625deg_npz/train/1979_0.npz'
with load(npz_file) as data:
	print(list(data.files))
['lsm', 'orography', 'lat2d', 't2m', 'u10', 'v10', 'tisr', 'tp', 'z_50', 'z_250', 'z_500', 'z_600', 'z_700', 'z_850', 'z_925', 'z_1000', 'u_50', 'u_250', 'u_500', 'u_600', 'u_700', 'u_850', 'u_925', 'u_1000', 'v_50', 'v_250', 'v_500', 'v_600', 'v_700', 'v_850', 'v_925', 'v_1000', 't_50', 't_250', 't_500', 't_600', 't_700', 't_850', 't_925', 't_1000', 'r_50', 'r_250', 'r_500', 'r_600', 'r_700', 'r_850', 'r_925', 'r_1000', 'q_50', 'q_250', 'q_500', 'q_600', 'q_700', 'q_850', 'q_925', 'q_1000']
... 
['land_sea_mask', 'orography', 'lattitude', '2m_temperature', '10m_u_component_of_wind', '10m_v_component_of_wind', 'toa_incident_solar_radiation', 'total_precipitation', 'geopotential_50', 'geopotential_250', 'geopotential_500', 'geopotential_600', 'geopotential_700', 'geopotential_850', 'geopotential_925', 'geopotential_1000', 'u_component_of_wind_50', 'u_component_of_wind_250', 'u_component_of_wind_500', 'u_component_of_wind_600', 'u_component_of_wind_700', 'u_component_of_wind_850', 'u_component_of_wind_925', 'u_component_of_wind_1000', 'v_component_of_wind_50', 'v_component_of_wind_250', 'v_component_of_wind_500', 'v_component_of_wind_600', 'v_component_of_wind_700', 'v_component_of_wind_850', 'v_component_of_wind_925', 'v_component_of_wind_1000', 'temperature_50', 'temperature_250', 'temperature_500', 'temperature_600', 'temperature_700', 'temperature_850', 'temperature_925', 'temperature_1000', 'relative_humidity_50', 'relative_humidity_250', 'relative_humidity_500', 'relative_humidity_600', 'relative_humidity_700', 'relative_humidity_850', 'relative_humidity_925', 'relative_humidity_1000', 'specific_humidity_50', 'specific_humidity_250', 'specific_humidity_500', 'specific_humidity_600', 'specific_humidity_700', 'specific_humidity_850', 'specific_humidity_925', 'specific_humidity_1000']
# All names changed


import numpy as np
npz_file = '/g/data/wb00/admin/staging/weatherbench/5.625deg_npz/train/1980_0.npz'

with np.load(npz_file) as data:
	list(data.keys())
... 
['land_sea_mask', 'orography', 'lattitude', '2m_temperature', '10m_u_component_of_wind', '10m_v_component_of_wind', 'toa_incident_solar_radiation', 'total_precipitation', 'geopotential_50', 'geopotential_250', 'geopotential_500', 'geopotential_600', 'geopotential_700', 'geopotential_850', 'geopotential_925', 'geopotential_1000', 'u_component_of_wind_50', 'u_component_of_wind_250', 'u_component_of_wind_500', 'u_component_of_wind_600', 'u_component_of_wind_700', 'u_component_of_wind_850', 'u_component_of_wind_925', 'u_component_of_wind_1000', 'v_component_of_wind_50', 'v_component_of_wind_250', 'v_component_of_wind_500', 'v_component_of_wind_600', 'v_component_of_wind_700', 'v_component_of_wind_850', 'v_component_of_wind_925', 'v_component_of_wind_1000', 'temperature_50', 'temperature_250', 'temperature_500', 'temperature_600', 'temperature_700', 'temperature_850', 'temperature_925', 'temperature_1000', 'relative_humidity_50', 'relative_humidity_250', 'relative_humidity_500', 'relative_humidity_600', 'relative_humidity_700', 'relative_humidity_850', 'relative_humidity_925', 'relative_humidity_1000', 'specific_humidity_50', 'specific_humidity_250', 'specific_humidity_500', 'specific_humidity_600', 'specific_humidity_700', 'specific_humidity_850', 'specific_humidity_925', 'specific_humidity_1000']
```


