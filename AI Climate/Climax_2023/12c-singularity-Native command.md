### modulesfile (2023.03.06)

```bash
cd /scratch/fp0/shared/Modules/modulefiles 
mkdir climaX
cp gpustat/1 climaX/
```


```
module use /scratch/fp0/shared/Modules/modulefiles/
module load climaX
```


```bash
cd  /scratch/fp0/shared/Modules/modulefiles/climaX/ 
mkdir libexec bin
mv /scratch/fp0/mah900/ClimaX_maruf/ClimaX.sif /scratch/fp0/shared/Modules/modulefiles/climaX/libexec/ 
```
`nano libexec/run.sh`
```bash
#!/bin/bash
export SINGULARITY_BINDPATH="/data"
dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
img="ClimaX.sif"
cmd=$(basename "$0")
arg="$@"
echo running: singularity exec "${dir}/${img}" $cmd $arg
#singularity exec "${dir}/${img}" $cmd $arg
```

path
```bash
export PATH=$PATH:/scratch/fp0/shared/Modules/modulefiles/climaX/bin
```

`nano 1`
```bash
#%Module

proc ModulesHelp { } {
   puts stderr "This module adds Climax to your path "
}

module-whatis "This module adds Climax to your path\n"

prepend-path BASEDIR  "/scratch/fp0/shared/Modules/modulefiles/climaX/bin"
prepend-path PATH     "/scratch/fp0/shared/Modules/modulefiles/climaX/bin"
module load singularity
```

```
module use /scratch/fp0/shared/Modules/modulefiles
module load climaX
```
Error-1
```
module load climaX
max size for a Tcl value (2147483647 bytes) exceeded
```
Reason: `ClimaX.sif` file is too large.  
Solution: Move the file out of the folder
Changed run.sh file
```bash
#!/bin/bash
export SINGULARITY_BINDPATH="/g/data/wb00,/scratch/fp0/,/tmp"
dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
dir="/scratch/fp0/shared/Modules/modulefiles"
img="ClimaX.sif"
cmd=$(basename "$0")
arg="$@"
echo running: singularity exec "${dir}/${img}" $cmd $arg
singularity exec --nv "${dir}/${img}" $cmd $arg
```

### ARE

Error-1
```
---------------------------------------------------------------------------
ModuleNotFoundError                       Traceback (most recent call last)
Input In [1], in <cell line: 1>()
----> 1 from torchdata import _torchdata as _torchdata
      2 import holoviews as hv
      3 def RMSELoss(yhat,y):

ModuleNotFoundError: No module named 'torchdata'
```

Python is pointing to another env:
```bash
import sys
for line in sys.path:
    print (line)
...
/home/900/mah900
/home/900/mah900
/home/900/mah900/ondemand/data/sys/dashboard/batch_connect/sys/jupyter/ncigadi/output/75726aee-b01e-4cfb-81f0-e6166432066c/lib/python3
/scratch/fp0/mah900/env/dataloader/lib/python39.zip
/scratch/fp0/mah900/env/dataloader/lib/python3.9
/scratch/fp0/mah900/env/dataloader/lib/python3.9/lib-dynload

/home/900/mah900/.local/lib/python3.9/site-packages
/scratch/fp0/mah900/env/dataloader/lib/python3.9/site-packages

echo $PYTHONPATH
:/home/900/mah900/ondemand/data/sys/dashboard/batch_connect/sys/jupyter/ncigadi/output/75726aee-b01e-4cfb-81f0-e6166432066c/lib/python3
```

Gather path data
Conda
```bash
/scratch/fp0/mah900/env/climax/bin:/scratch/fp0/mah900/Miniconda2023/condabin:/home/900/mah900/.local/bin:/home/900/mah900/bin:/opt/pbs/default/bin:/opt/nci/bin:/opt/bin:/opt/Modules/v4.3.0/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin
```
Singularity
```bash
Singularity> echo $PATH
/opt/miniconda/condabin:/opt/miniconda/envs/climaX/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

>>> import sys
>>> for line in sys.path:
...     print (line)
... 

/opt/miniconda/envs/climaX/lib/python38.zip
/opt/miniconda/envs/climaX/lib/python3.8
/opt/miniconda/envs/climaX/lib/python3.8/lib-dynload
/home/900/mah900/.local/lib/python3.8/site-packages
/opt/miniconda/envs/climaX/lib/python3.8/site-packages
/ClimaX_maruf/src
```


Have to be chnaged in the modulefile ???
```
#%Module
proc ModulesHelp { } {
   puts stderr "This module adds gpustat to your path "
}
module-whatis "This module adds gpustat to your path\n"
module load singularity
prepend-path  BASEDIR    /scratch/fp0/shared/Modules/modulefiles/climaX/
prepend-path  PATH       /scratch/fp0/shared/Modules/modulefiles/climaX/bin
prepend-path  PATH       /opt/miniconda/envs/climaX/bin/ 
prepend-path  PATH       /home/900/mah900/.local/lib/python3.8/site-packages 
setenv        PYTHONPATH /opt/miniconda/envs/climaX/lib/python3.8

```

TRY running a juputer server, se it works or not (Not worked)
later,
Try building singularity again with PYTHONPATH, env variables, and ClimaX kernel installed.
Modified file in [[12b-Singularity]] on section 'Another Try, build (2023.03.06-5:00)''
(Working)

###### Next, update module file 
updating is done on [[12b-Singularity]] , section: Another Try, build (2023.03.06-5:00)
Working