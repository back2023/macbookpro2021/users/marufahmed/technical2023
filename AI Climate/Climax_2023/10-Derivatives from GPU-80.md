`/scratch/fp0/mah900/ClimaX/pbs_scripts`

### `global_train_8.pbs`
`cp global_train_80.pbs global_train_8.pbs`
```bash
#!/bin/bash
#PBS -S /bin/bash
#PBS -q gpuvolta
#PBS -l ncpus=96
#PBS -l ngpus=8
#PBS -l jobfs=800GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
#PBS -l mem=760GB
#PBS -l walltime=02:00:00
#PBS -N Glob_G_8
#PBS -P fp0
export WORLD_SIZE=$PBS_NGPUS  #1
export MASTER_ADDR=$(head -n 1 $PBS_NODEFILE | uniq )  #2
export MASTER_PORT=10001   #3
export TOT_NODES=$PBS_NNODES    #4
export GPU_PER_NODE=4  #5
export MAX_EPOCHS=3  #6
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global
export CONFIG_PATH='/scratch/fp0/mah900/ClimaX/configs/global_forecast_climax.yaml'  
export ROOT_DIR='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' 
export PRETRAIN_PATH='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt'  
export PROG_BAR='True'

N_LIST=''
while IFS= read -r line ; do
         N_LIST+="${line::-16}:1," 
done < <(cat $PBS_NODEFILE | uniq)       
N_LIST=${N_LIST::-1}
echo ${N_LIST}

module load openmpi
mpirun -np ${PBS_NNODES} \
    -H ${N_LIST} \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    -x WORLD_SIZE -x MASTER_ADDR -x MASTER_PORT \
    -x TOT_NODES -x GPU_PER_NODE -x MAX_EPOCHS -x OUTPUT_DIR -x PBS_NODEFILE \
    -x CONFIG_PATH -x ROOT_DIR -x PRETRAIN_PATH -x PROG_BAR \
    /scratch/fp0/mah900/ClimaX/global_mn_80.sh
```

###### Output observations
With `5.625deg_npz_120'` and 8 GPUs train ended in 6 min.
Are some of the shards remain unused???
So, shard has to match number of GPUs??? (lodged a discussion: https://github.com/microsoft/ClimaX/discussions/7)
```bash
real    6m5.569s
user    3m21.305s
sys     0m42.456s  

real    6m5.920s
user    3m11.594s
sys     0m51.556s
```
Run again with `5.625deg_npz` (8 shards) and is Working 
Stick with 8 for the momoent till I get some more info

Output
```bash
======================================================================================
                  Resource Usage on 2023-02-25 06:01:31:
   Job Id:             73953952.gadi-pbs
   Project:            fp0
   Exit Status:        0
   Service Units:      269.44
   NCPUs Requested:    96                     NCPUs Used: 96
                                           CPU Time Used: 09:21:36
   Memory Requested:   760.0GB               Memory Used: 207.24GB
   NGPUs Requested:    8                 GPU Utilisation: 0%
                                         GPU Memory Used: 0B
   Walltime requested: 02:00:00            Walltime Used: 00:56:08
   JobFS requested:    800.0GB                JobFS used: 179.05KB
======================================================================================
```

### `global_train_1.pbs`
`cp global_train_8.pbs global_train_1.pbs`
```bash
#!/bin/bash
#PBS -S /bin/bash
#PBS -q gpuvolta
#PBS -l ncpus=12
#PBS -l ngpus=1
#PBS -l jobfs=400GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
#PBS -l mem=380GB
#PBS -l walltime=08:00:00
#PBS -N Glob_G_1
#PBS -P fp0
export WORLD_SIZE=$PBS_NGPUS  #1
export MASTER_ADDR=$(head -n 1 $PBS_NODEFILE | uniq )  #2
export MASTER_PORT=10001   #3
export TOT_NODES=$PBS_NNODES    #4
export GPU_PER_NODE=1  #5
export MAX_EPOCHS=3  #6
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global
export CONFIG_PATH='/scratch/fp0/mah900/ClimaX/configs/global_forecast_climax.yaml'  
export ROOT_DIR='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' 
export PRETRAIN_PATH='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt'  
export PROG_BAR='True'

N_LIST=''
while IFS= read -r line ; do
         N_LIST+="${line::-16}:1," 
done < <(cat $PBS_NODEFILE | uniq)       
N_LIST=${N_LIST::-1}
echo ${N_LIST}

module load openmpi
mpirun -np ${PBS_NNODES} \
    -H ${N_LIST} \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    -x WORLD_SIZE -x MASTER_ADDR -x MASTER_PORT \
    -x TOT_NODES -x GPU_PER_NODE -x MAX_EPOCHS -x OUTPUT_DIR -x PBS_NODEFILE \
    -x CONFIG_PATH -x ROOT_DIR -x PRETRAIN_PATH -x PROG_BAR \
    /scratch/fp0/mah900/ClimaX/global_mn_80.sh
```

### `global_train_4.pbs`
`cp global_train_8.pbs global_train_1.pbs`

```bash
#!/bin/bash
#PBS -S /bin/bash
#PBS -q gpuvolta
#PBS -l ncpus=48
#PBS -l ngpus=4
#PBS -l jobfs=400GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
#PBS -l mem=380GB
#PBS -l walltime=04:00:00
#PBS -N Glob_G_4
#PBS -P fp0
export WORLD_SIZE=$PBS_NGPUS  #1
export MASTER_ADDR=$(head -n 1 $PBS_NODEFILE | uniq )  #2
export MASTER_PORT=10002   #3
export TOT_NODES=$PBS_NNODES    #4
export GPU_PER_NODE=4  #5
export MAX_EPOCHS=3  #6
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global
export CONFIG_PATH='/scratch/fp0/mah900/ClimaX/configs/global_forecast_climax.yaml'  
export ROOT_DIR='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' 
export PRETRAIN_PATH='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt'  
export PROG_BAR='True'

N_LIST=''
while IFS= read -r line ; do
         N_LIST+="${line::-16}:1," 
done < <(cat $PBS_NODEFILE | uniq)       
N_LIST=${N_LIST::-1}
echo ${N_LIST}

module load openmpi
mpirun -np ${PBS_NNODES} \
    -H ${N_LIST} \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    -x WORLD_SIZE -x MASTER_ADDR -x MASTER_PORT \
    -x TOT_NODES -x GPU_PER_NODE -x MAX_EPOCHS -x OUTPUT_DIR -x PBS_NODEFILE \
    -x CONFIG_PATH -x ROOT_DIR -x PRETRAIN_PATH -x PROG_BAR \
    /scratch/fp0/mah900/ClimaX/global_mn_80.sh
```
### `global_train_16.pbs`
`cp global_train_8.pbs global_train_16.pbs`

```bash
#!/bin/bash
#PBS -S /bin/bash
#PBS -q gpuvolta
#PBS -l ncpus=192
#PBS -l ngpus=16
#PBS -l jobfs=400GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
#PBS -l mem=1500GB
#PBS -l walltime=02:00:00
#PBS -N Glob_G16
#PBS -P fp0
export WORLD_SIZE=$PBS_NGPUS  #1
export MASTER_ADDR=$(head -n 1 $PBS_NODEFILE | uniq )  #2
export MASTER_PORT=10002   #3
export TOT_NODES=$PBS_NNODES    #4
export GPU_PER_NODE=4  #5
export MAX_EPOCHS=3  #6
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global
export CONFIG_PATH='/scratch/fp0/mah900/ClimaX/configs/global_forecast_climax.yaml'  
export ROOT_DIR='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' 
export PRETRAIN_PATH='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt'  
export PROG_BAR='True'

N_LIST=''
while IFS= read -r line ; do
         N_LIST+="${line::-16}:1," 
done < <(cat $PBS_NODEFILE | uniq)       
N_LIST=${N_LIST::-1}
echo ${N_LIST}

module load openmpi
mpirun -np ${PBS_NNODES} \
    -H ${N_LIST} \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    -x WORLD_SIZE -x MASTER_ADDR -x MASTER_PORT \
    -x TOT_NODES -x GPU_PER_NODE -x MAX_EPOCHS -x OUTPUT_DIR -x PBS_NODEFILE \
    -x CONFIG_PATH -x ROOT_DIR -x PRETRAIN_PATH -x PROG_BAR \
    /scratch/fp0/mah900/ClimaX/global_mn_80.sh
```

