
#### Try, 2023.02.17-3:35
##### Try screen recording

```bash
qsub -I /home/900/mah900/conda/g4_h4.pbs
...
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
cd /scratch/fp0/mah900/ClimaX

script --t=time_global.2023.02.17.txt -a script_global.2023.02.17.txt



```
https://www.redhat.com/sysadmin/record-terminal-script-scriptreplay
https://www.linuxshelltips.com/record-linux-terminal-sessions/