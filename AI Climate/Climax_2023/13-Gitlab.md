2023.02.02
```bash
cd existing_repo
#git remote add origin https://gitlab.com/back2023/gadi/scratch/fp0/mah900/climax.git
git remote add origin git@gitlab.com:back2023/gadi/scratch/fp0/mah900/climax.git
git branch -M main
git push -uf origin main
```

Error-1
```bash
$ git push -uf origin main
remote: You are not allowed to push code to this project.
fatal: unable to access 'https://gitlab.com/back2023/gadi/scratch/fp0/mah900/climax.git/': The requested URL returned error: 403
```
Solution: Use SSH instead of http
Source: https://stackoverflow.com/a/7771927
First commit 2023.02.02
```bash
git add .
git commit -m "First commit 2023.02.02"
git push origin main
```
