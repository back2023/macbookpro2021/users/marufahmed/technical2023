
Received info from Imitaz

*** 
```
From: Tung Nguyen <ductungnguyen1997@gmail.com>
Sent: Friday, 13 October 2023 10:03 AM
To: Imtiaz Dharssi <imtiaz.dharssi@bom.gov.au>
Subject: Re: Seminar on ClimaX weather and climate model? [SEC=OFFICIAL]
 
Hi Imtiaz,

Yes, that works for me. I'm sending you my bio as well as the title and the abstract of the talk.

Bio: Tung Nguyen is a 3rd year PhD student in Computer Science at UCLA, advised by Aditya Grover. His research interest lies in the intersection of Decision making, Sequence modeling, Generative Modeling, and their applications to Tackling Climate Change.
Title: ClimaX: A foundation model for weather and climate
Abstract: Most state-of-the-art approaches for weather and climate modeling are based on physics-informed numerical models of the atmosphere. These approaches aim to model the non-linear dynamics and complex interactions between multiple variables, which are challenging to approximate. Additionally, many such numerical models are computationally intensive, especially when modeling the atmospheric phenomenon at a fine-grained spatial and temporal resolution. Recent data-driven approaches based on machine learning instead aim to directly solve a downstream forecasting or projection task by learning a data-driven functional mapping using deep neural networks. However, these networks are trained using curated and homogeneous climate datasets for specific spatiotemporal tasks, and thus lack the generality of numerical models. We develop and demonstrate ClimaX, a flexible and generalizable deep learning model for weather and climate science that can be trained using heterogeneous datasets spanning different variables, spatio-temporal coverage, and physical groundings. ClimaX extends the Transformer architecture with novel encoding and aggregation blocks that allow effective use of available compute while maintaining general utility. ClimaX is pre-trained with a self-supervised learning objective on climate datasets derived from CMIP6. The pretrained ClimaX can then be fine-tuned to address a breadth of climate and weather tasks, including those that involve atmospheric variables and spatio-temporal scales unseen during pretraining. Compared to existing data-driven baselines, we show that this generality in ClimaX results in superior performance on benchmarks for weather forecasting and climate projections, even when pretrained at lower resolutions and compute budgets. Source code is available at https://github.com/microsoft/ClimaX.

Best,
Tung Nguyen
```
***

Meeting detail
```
From: Roseanna McKay (she/her) <roseanna.mckay@bom.gov.au>
Sent: Friday, 13 October 2023 11:30 AM
To: Christian Stassen (he/him) <Christian.Stassen@bom.gov.au>; Justin Peter <justin.peter@bom.gov.au>; Haifeng Zhang <Haifeng.Zhang@bom.gov.au>; Bureau Science Seminars Admin <bureau-science-seminars-admin@bom.gov.au>; Bureau Science Seminars <bureau-science-seminars@bom.gov.au>; ductungnguyen1997@gmail.com <ductungnguyen1997@gmail.com>; Imtiaz Dharssi <imtiaz.dharssi@bom.gov.au>
Subject: Seminar by Tung Nguyen, Tuesday 17 October, 10am (online only) [SEC=OFFICIAL]
When: Tuesday, 17 October 2023 10:00 AM-11:00 AM.
Where: Microsoft Teams Meeting
 
Microsoft Teams meeting

Join on your computer, mobile app or room device
Click here to join the meeting
Meeting ID: 497 184 854 14
Passcode: 3ZCDBT
Download Teams | Join on the web
Disclaimer: This meeting is hosted by the Bureau of Meteorology. You must join with your full name and follow instruction given by the host. Disclosure or unauthorised use of content from this meeting may be a serious criminal offence.
Learn more | Meeting options
__________________________

Tuesday, 17 October 2023, 10 -11 am 

 MS Teams Software only (use the link above)  

ClimaX: A foundation model for weather and climate

Tung Nguyen (UCLA)
```

*** 
### Microsoft Teams meeting
Join on your computer, mobile app or room device
[Click here to join the meeting](https://teams.microsoft.com/l/meetup-join/19%3ameeting_Yzg0NjE2YTEtYTJjYy00Njc2LWIxYTItNjU3M2FmMmIzMTRh%40thread.v2/0?context=%7b%22Tid%22%3a%22d1ad7db5-97dd-4f2b-816e-50d663b7bb94%22%2c%22Oid%22%3a%22415dce97-7c99-404c-be43-c4e2fbc22e91%22%7d)

Meeting ID: 497 184 854 14  
Passcode: 3ZCDBT

