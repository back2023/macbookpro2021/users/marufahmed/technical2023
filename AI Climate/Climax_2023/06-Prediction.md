1. Start with regional, it will take less time 
2. First add predict after train.
3. Next, will try with out train

Make a copy from the train.py file
```
cp train.py predict_t.py
```

Allocate one gpu and try
```
qsub -I /home/900/mah900/conda/g1_h8.pbs
cd /scratch/fp0/mah900/ClimaX/src/climax/regional_forecast
```

```
# Added
# 2023.02.21
predictions = trainer.predict(dataloaders=test_dataloader)
```

Code to run the script (from )
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
cd /scratch/fp0/mah900/ClimaX

time python src/climax/regional_forecast/predict_t.py \
	--config configs/regional_forecast_climax.2023.02.17.yaml \
    --trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir=/g/data/wb00/admin/staging/weatherbench/5.625deg_npz \
    --data.region="NorthAmerica" \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5

```
Error 
```bash
RuntimeError: CUDA out of memory.
```
Reason: command was not complete, `\` was missing after region. Hence, comand was split into two and error

Next, try 4 GPUs
```bash
qsub -I /home/900/mah900/conda/g4_h4.pbs
```

```bash
from pytorch_lightning import LightningModule

cli.model = LightningModule.load_from_checkpoint("/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")
```
Error
```bash
RuntimeError: Error(s) in loading state_dict for LightningModule:
Unexpected key(s) in state_dict: "net.pos_embed",
```

Error
```bash    
	raise MisconfigurationException(
lightning_lite.utilities.exceptions.MisconfigurationException: `model` must be provided to `trainer.predict()` when it hasn't been passed in a previous run
```


Error
```bash
    raise MisconfigurationException("`Trainer.predict` requires `forward` method to run.")
lightning_lite.utilities.exceptions.MisconfigurationException: `Trainer.predict` requires `forward` method to run.
```






### Global script
```bash

export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global
time python \
	/g/data/wb00/admin/staging/ClimaX/src/climax/global_forecast/train.py \
	--config '/g/data/wb00/admin/staging/ClimaX/configs/global_forecast_climax.yaml' \
	--trainer.num_nodes=1 \
	--trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --data.num_workers=1 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5
```


Next      (2022.02.21)
1. create a folder with one file and set prediction path
2. prediction_step has to concate all the preds, and implement pred_end to save (lookup in the doc for functions to implement)
3. check difference between 1 an 0 iter.
   If same then good
   otherwise, trainer or model save point investigate.
4. For each batch concatenet x,y, pred and at the end return all three for comaring


# 2023.02.22

##### Switch to regional script
Start with regional as the global is still working 
1. Make a backup copy of the `module.py` and `datamodule` files
2. Add code, pred dataloader and save code.
3. make a pred folder and copy one file 

Create the pred folder
```
mkdir -p /g/data/wb00/admin/staging/weatherbench/5.625deg_npz/pred
cp test/2018_7.npz pred/
```

Back up `datamodule.py`  & `module.py`
```
cd /scratch/fp0/mah900/ClimaX/src/climax/regional_forecast
cp datamodule.py datamodule.py.backup_2023.02.22
cp module.py module.py.backup.2023.02.22
```
### Modify `datamodule.py` 
```python
# line 81
 self.lister_pred = list(dp.iter.FileLister(os.path.join(root_dir, "pred")))
 
# line 88
 self.pred_clim = self.get_climatology("pred", out_variables)
 
# line 93
 self.data_pred: Optional[IterableDataset] = None

# line 197-216
 self.data_pred = IndividualForecastDataIter(
	Forecast(
		NpyReader(
			file_list=self.lister_pred,
				start_idx=0,
				end_idx=1,
				variables=self.hparams.variables,
				out_variables=self.hparams.out_variables,
				shuffle=False,
				multi_dataset_training=False,
		),
		max_predict_range=self.hparams.predict_range,
		random_lead_time=False,
		hrs_each_step=self.hparams.hrs_each_step,
	),
	transforms=self.transforms,
	output_transforms=self.output_transforms,
	region_info=region_info
)

# line 248 -257
 def predict_dataloader(self):
	return DataLoader(
		self.data_pred,
		batch_size=self.hparams.batch_size,
		shuffle=False,
		drop_last=False,
		num_workers=self.hparams.num_workers,
		pin_memory=self.hparams.pin_memory,
		collate_fn=collate_fn_regional,
	)

```


### In the  `module.py` ,  add three class variables for x, y, and predict
```python
#line 57-59
	self.x_store = []
	self.y_store = []
	self.pred_store = []

#line 125-145

	################
	#2023.02.21
	def predict_step(self, batch: Any, batch_idx: int):
		x, y, lead_times, variables, out_variables, region_info = batch
		#metrics=[lat_weighted_mse_val, lat_weighted_rmse, lat_weighted_acc],
		loss_dict, preds = self.net.forward(
			x,
			y,
			lead_times,
			variables,
			out_variables,
			[lat_weighted_mse],
			lat=self.lat,
			region_info=region_info
		)
		#preds = 'a'
		x_store.append(x)
		y_store.append(y)
		preds_store.append(preds)
		return x_store,y_store,preds_store
	#################
```

`predict_t.py`
```python
# line 38-67
# 2023.02.21
from pytorch_lightning import LightningModule
from climax.regional_forecast.arch import RegionalClimaX
#model = RegionalForecastModule(net=RegionalClimaX)
#, pretrained_path="/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")
#cli.model = LightningModule.load_from_checkpoint("/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")
#cli.model.load_from_checkpoint("/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")

#cli.trainer.fit(cli.model, datamodule=cli.datamodule, ckpt_path="/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")

#model.set_denormalization(mean_denorm, std_denorm)
#model.set_lat_lon(*cli.datamodule.get_lat_lon())
#model.set_pred_range(cli.datamodule.hparams.predict_range)
#model.set_val_clim(cli.datamodule.val_clim)
#model.set_test_clim(cli.datamodule.test_clim)

#model = LightningModule.load_from_checkpoint("/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")

# fit() runs the training
cli.trainer.fit(cli.model, datamodule=cli.datamodule)
x, y, predictions = cli.trainer.predict(cli.model, datamodule=cli.datamodule, return_predictions=True)
# test the trained model
#cli.trainer.test(cli.model, datamodule=cli.datamodule, ckpt_path="best")

#print (type(predictions))
#print (type(predictions[0]))
path='/g/data/wb00/admin/staging/climax_train_regional/test'
torch.save(predictions, path+ '/preds-3.pt')
torch.save(x,path+ '/x-3.pt')
torch.save(y,path+ '/y-3.pt')

####################################
####################################
```

##### Regional run - 4 GPUs
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
cd /scratch/fp0/mah900/ClimaX
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_regional 

time python src/climax/regional_forecast/predict_t.py \
	--config configs/regional_forecast_climax.2023.02.17.yaml \
    --trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir=/g/data/wb00/admin/staging/weatherbench/5.625deg_npz \
    --data.region="NorthAmerica" \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5
```

###### Error 
```bash
FileNotFoundError: [Errno 2] No such file or directory: '/g/data/wb00/admin/staging/weatherbench/5.625deg_npz/pred/climatology.npz'
```
Solution
```bash
cd /g/data/wb00/admin/staging/weatherbench/5.625deg_npz/
cp test/climatology.npz pred/
```
Working
Next, try on the global data

### Try on global data again
Back up `datamodule.py`  & `module.py` and make `predict_t.py` file
```bash
cd /scratch/fp0/mah900/ClimaX/src/climax/global_forecast/
cp datamodule.py datamodule.py.backup_2023.02.22
cp module.py module.py.backup.2023.02.22

cp train.py predict_t.py
```

#### Now, add code like the above regional section
`datamodule.py`
```python
#line 61
self.lister_pred = list(dp.iter.FileLister(os.path.join(root_dir, "pred")))

#line 69
self.pred_clim = self.get_climatology("pred", out_variables)

#line 74
self.data_pred: Optional[IterableDataset] = None

# line 168-186
self.data_pred = IndividualForecastDataIter(
	Forecast(
		NpyReader(
			file_list=self.lister_pred,
			start_idx=0,
			end_idx=1,
			variables=self.hparams.variables,
			out_variables=self.hparams.out_variables,
			shuffle=False,
			multi_dataset_training=False,
		),
		max_predict_range=self.hparams.predict_range,
		random_lead_time=False,
		hrs_each_step=self.hparams.hrs_each_step,
	),
	transforms=self.transforms,
	output_transforms=self.output_transforms
)

# line 220-229
def predict_dataloader(self):
	return DataLoader(
		self.data_pred,
		batch_size=self.hparams.batch_size,
		shuffle=False,
		drop_last=False,
		num_workers=self.hparams.num_workers,
		pin_memory=self.hparams.pin_memory,
		collate_fn=collate_fn,
	)
```

`module.py`
```python
# line 57
self.x_store = []
self.y_store = []
self.pred_store = []

# line 98
def set_pred_clim(self, clim):
	self.pred_clim = clim

#line 107-110
#print (x.shape, y.shape )
#loss_dict, _ = self.net.forward(x, y, lead_times, variables, out_variables, [lat_weighted_mse], lat=self.lat)
loss_dict, preds = self.net.forward(x, y, lead_times, variables, out_variables, [lat_weighted_mse], lat=self.lat)
#print (preds.shape)

#line 124-144
################
#2023.02.21
def predict_step(self, batch: Any, batch_idx: int):
	x, y, lead_times, variables, out_variables = batch
	#metrics=[lat_weighted_mse_val, lat_weighted_rmse, lat_weighted_acc],
	loss_dict, preds = self.net.forward(
		x,
		y,
		lead_times,
		variables,
		out_variables,
		[lat_weighted_mse],
		lat=self.lat
	)
	#preds = 'a'
	x_store.append(x)
	y_store.append(y)
	preds_store.append(preds)
	return x_store,y_store,preds_store
#################

```

`predict_t.py`
```python
# line 32
cli.model.set_pred_clim(cli.datamodule.pred_clim)

#line 39-45
# pred
x, y, predictions = cli.trainer.predict(cli.model, datamodule=cli.datamodule, return_predictions=True)
path='/g/data/wb00/admin/staging/climax_train_global/test'
torch.save(predictions, path + '/preds-3.pt')
torch.save(x,path + '/x-3.pt')
torch.save(y,path + '/y-3.pt')
####################################
```

Run  global `predict_t.py`
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
cd /scratch/fp0/mah900/ClimaX
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global

time python \
	src/climax/global_forecast/predict_t.py \
	--config 'configs/global_forecast_climax.2023.02.16.yaml' \
	--trainer.num_nodes=1 \
	--trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=0 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --data.num_workers=1 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5
```

###### Error-1
```bash
jsonargparse.util.ParserError: Parser key "model.net":
  Import path climax.arch.ClimaX does not correspond to a subclass of <class 'climax.regional_forecast.arch.RegionalClimaX'>
```
Reason: wrong value in the script
```
#src/climax/regional_forecast/predict_t.py \
src/climax/regional_global/predict_t.py \
```
###### Error-2
```bash
  File "/scratch/fp0/mah900/ClimaX/src/climax/global_forecast/datamodule.py", line 185, in setup
    region_info=region_info
NameError: name 'region_info' is not defined
```
Reason: line 185, `region_info=region_info`. It is now removed

###### Error-3
```bash
  File "/scratch/fp0/mah900/ClimaX/src/climax/global_forecast/datamodule.py", line 227, in predict_dataloader
    collate_fn=collate_fn_regional,
NameError: name 'collate_fn_regional' is not defined
```
Reason: no regional fn. Corrected: `collate_fn=collate_fn,`
##### Error-4
```bash
  File "src/climax/global_forecast/predict_t.py", line 41, in main
    x, y, predictions = cli.trainer.predict(cli.model, datamodule=cli.datamodule, return_predictions=True)
ValueError: not enough values to unpack (expected 3, got 0)
```


##### Error-5
```bash
    return self.model.predict_step(*args, **kwargs)
  File "/scratch/fp0/mah900/ClimaX/src/climax/global_forecast/module.py", line 140, in predict_step
    x_store.append(x)
NameError: name 'x_store' is not defined
```

##### Error-6
```bash
  File "/scratch/fp0/mah900/ClimaX/src/climax/regional_forecast/predict_t.py", line 60, in main
    x, y, predictions = cli.trainer.predict(cli.model, datamodule=cli.datamodule, return_predictions=True)
ValueError: not enough values to unpack (expected 3, got 0)
```
Reason: was running regional cmmand, instead of global

##### Error-7
```bash
  File "/scratch/fp0/mah900/ClimaX/src/climax/global_forecast/module.py", line 146, in predict_step
    self.preds_store.append(preds)
  File "/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/torch/nn/modules/module.py", line 1207, in __getattr__
    raise AttributeError("'{}' object has no attribute '{}'".format(
AttributeError: 'GlobalForecastModule' object has no attribute 'preds_store'
```
Reason: mispelled as 'pred_store'


#####  Error-8
```bash
  File "/scratch/fp0/mah900/ClimaX/src/climax/global_forecast/predict_t.py", line 49, in <module>
    main()
  File "/scratch/fp0/mah900/ClimaX/src/climax/global_forecast/predict_t.py", line 41, in main
    x, y, predictions = cli.trainer.predict(cli.model, datamodule=cli.datamodule, return_predictions=True)
```
There was not enough files in the pred folder. 
Some run after coping 4 files instead of 1. copying another 4. (Total 8)
```
cd test
cp 2018_0.npz  2018_2.npz  2018_4.npz  2018_6.npz 2018_1.npz  2018_3.npz  2018_5.npz  2018_7.npz ../pred
```
Reason. 
Predictor returns a touple at the end, not three variables as coded in the predict_step function.
`batch_idx` matches touple len, that means lightning returned all togather. 
That means pytorch lightining accumalated everything. 

Next, continued on [[06b-Prediction]]