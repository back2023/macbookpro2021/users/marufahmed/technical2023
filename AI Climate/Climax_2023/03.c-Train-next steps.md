Carried on from [[03.b-Train]]

# 1. Set config parameters from cmd
#### Try 1
Use full path
set parameters from the cmd
```bash
qsub -I /home/900/mah900/conda/g4_h4.pbs
...
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
#cd /scratch/fp0/mah900/ClimaX

time python /scratch/fp0/mah900/ClimaX/src/climax/global_forecast/train.py \
	--config '/scratch/fp0/mah900/ClimaX/configs/global_forecast_climax.2023.02.16.yaml' \
	--trainer.default_root_dir='/g/data/wb00/admin/staging/climax_train_global' \
	--trainer.num_nodes=1 \
	--trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5
```

#### Error.1
```
OSError: [Errno 30] Read-only file system: '/logs'
```

### Try 2 (Working)
```bash
# global_forecast_climax.2023.02.16.yaml
# line 7
# default_root_dir: ${OUTPUT_DIR} # Did not work
default_root_dir: ${oc.env:OUTPUT_DIR} # Working

export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global

time python /scratch/fp0/mah900/ClimaX/src/climax/global_forecast/train.py \
	--config '/scratch/fp0/mah900/ClimaX/configs/global_forecast_climax.2023.02.16.yaml' \
	--trainer.num_nodes=1 \
	--trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5
```

##### Error.1
https://legacy.docs.greatexpectations.io/en/latest/guides/how_to_guides/configuring_data_contexts/how_to_use_a_yaml_file_or_environment_variables_to_populate_credentials.html 
```bash
omegaconf.errors.InterpolationKeyError: Interpolation key 'OUTPUT_DIR' not found
    full_key: trainer.default_root_dir
    object_type=dict
```
##### Solution
In the config file, use `default_root_dir: ${oc.env:OUTPUT_DIR}` , not `default_root_dir: ${OUTPUT_DIR}`

##### Output observations
```bash
...
Loading pre-trained checkpoint from: /g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt
...
...
/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/pytorch_lightning/callbacks/model_checkpoint.py:606: UserWarning: Checkpoint directory /g/data/wb00/admin/staging/climax_train_global/checkpoints exists and is not empty.
  rank_zero_warn(f"Checkpoint directory {dirpath} exists and is not empty.")
...
...
/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/pytorch_lightning/trainer/connectors/data_connector.py:224: PossibleUserWarning: 
The dataloader, val_dataloader 0, does not have many workers which may be a bottleneck. Consider increasing the value of the `num_workers` argument` (try 96 which is the number of cpus on this machine) in the `DataLoader` init to improve performance.
 ...
 ...
 Loaded model weights from checkpoint at /g/data/wb00/admin/staging/climax_train_global/checkpoints/epoch_000.ckpt
/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/pytorch_lightning/trainer/connectors/data_connector.py:224: PossibleUserWarning: The dataloader, test_dataloader 0, does not have many workers which may be a bottleneck. Consider increasing the value of the `num_workers` argument` (try 96 which is the number of cpus on this machine) in the `DataLoader` init to improve performance.
  rank_zero_warn(
/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/pytorch_lightning/utilities/data.py:85: UserWarning: Trying to infer the 
`batch_size` from an ambiguous collection. The batch size we found is 12. To avoid any miscalculations, use `self.log(..., 
batch_size=batch_size)`.
  warning_cache.warn(
...
...
real 37m23.377s
user 33m47.227s
sys 3m30.707s
```
* Increased workers from next runs

# 2. Clone code in `g/data/wb00/admin/staging`  (Working)
* Make a fresh clone in `g/data/wb00/admin/staging` 
  Run the train and config script from this location, but using my conda env.
* Tried: working without any change to the config file
* From observation above: increase workers by `--data.num_workers=96`  # does work
  Only  `--data.num_workers=1 works

```bash
cd /g/data/wb00/admin/staging/
git clone https://github.com/microsoft/ClimaX.git
...
Updating files: 100% (52/52), done.

. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global

time python \
	/g/data/wb00/admin/staging/ClimaX/src/climax/global_forecast/train.py \
	--config '/g/data/wb00/admin/staging/ClimaX/configs/global_forecast_climax.yaml' \
	--trainer.num_nodes=1 \
	--trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --data.num_workers=1 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5
```
* (Working for 4 GPUs)

#### Output Observation ( with `--data.num_workers=1`)
```
...
  rank_zero_warn(
/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/pytorch_lightning/utilities/data.py:85: UserWarning: Trying to infer the 
`batch_size` from an ambiguous collection. The batch size we found is 16. To avoid any miscalculations, use `self.log(..., 
batch_size=batch_size)`.
...
...
[W reducer.cpp:1251] Warning: find_unused_parameters=True was specified in DDP constructor, but did not find any unused parameters in the forward pass. This flag results in an extra traversal of the autograd graph every iteration,  which can adversely affect performance. If your model indeed never has any unused parameters in the forward pass, consider turning this flag off. Note that this warning may be a false positive if your model has flow control causing later iterations to have unused parameters. (function operator())
...
...
real 34m26.345s
user 33m1.827s
sys 3m21.730s

```


#### Error with `--data.num_workers=96`
```bash
  warnings.warn(_create_warning_msg(
/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/torch/utils/data/dataloader.py:563: UserWarning: This DataLoader will create 96 
worker processes in total. Our suggested max number of worker in current system is 48, which is smaller than what this DataLoader is going to
create. Please be aware that excessive worker creation might get DataLoader running slow or even freeze, lower the worker number to avoid 
potential slowness/freeze if necessary.
```

#### Error with `--data.num_workers=48` and any other greater than 1
```bash
  File "/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/pytorch_lightning/callbacks/progress/rich_progress.py", line 358, in 
on_sanity_check_end
    assert self.val_sanity_progress_bar_id is not None
AssertionError
```

##### Try: 
* chnaging to  `--data.num_workers=48`  # The same error
* chnaging to  --data.num_workers=12 # The same error
* chnaging to  --data.num_workers=8  #  Based on data processing, The same error
* chnaging to  --data.num_workers=4  #  For 4 GPUs, The same error
*  chnaging to  --data.num_workers=1  # Becuse, all other failed. This one is working

# 3. Multi-node (2023.02.18)
### Try 1, 8 nodes (Working)

```bash
qsub -I /home/900/mah900/conda/g8_h4.pbs
...
Job ID               Username Queue    Jobname    SessID NDS TSK Memory Time  S Time
-------------------- -------- -------- ---------- ------ --- --- ------ ----- - -----
73052413.gadi-pbs    mah900   gpuvolt* G8_H4      614839   2  96  600gb 04:00 R 00:00
   gadi-gpu-v100-0084/0*48+gadi-gpu-v100-0086/0*48


# Rank 0
ssh gadi-gpu-v100-0084
export MASTER_PORT=10001
export MASTER_ADDR=gadi-gpu-v100-0084
export WORLD_SIZE=8 
export NODE_RANK=0 

# Rank 1
ssh gadi-gpu-v100-0086
export MASTER_PORT=10001  
export MASTER_ADDR=gadi-gpu-v100-0084
export WORLD_SIZE=8 
export NODE_RANK=1

# Both machine
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global

time python \
	/g/data/wb00/admin/staging/ClimaX/src/climax/global_forecast/train.py \
	--config '/g/data/wb00/admin/staging/ClimaX/configs/global_forecast_climax.yaml' \
	--trainer.num_nodes=2 \
	--trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --data.num_workers=1 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5
```
##### Error-1
```
  File "/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/lightning_lite/utilities/device_parser.py", line 136, in _sanitize_gpu_ids
    raise MisconfigurationException(
lightning_lite.utilities.exceptions.MisconfigurationException: You requested gpu: [0, 1, 2, 3, 4, 5, 6, 7]
 But your machine only has: [0, 1, 2, 3]
```
###### Solution: 
Device is local device not global/world size
Change `--trainer.strategy=ddp --trainer.devices=8 ` to `--trainer.strategy=ddp --trainer.devices=4` 
`trainer.num_nodes=2` X `trainer.devices=4` = 8 = `world size`
Source: https://pytorch-lightning.readthedocs.io/en/stable/clouds/cluster_intermediate_1.html#setup-the-training-script

#### Output observation
```bash
...
  rank_zero_warn(
/scratch/fp0/mah900/env/climax/lib/python3.8/site-packages/pytorch_lightning/utilities/data.py:85: UserWarning: Trying to infer the `batch_size` 
from an ambiguous collection. The batch size we found is 14. To avoid any miscalculations, use `self.log(..., batch_size=batch_size)`.
...
...
real 19m26.919s
user 17m25.687s
sys 1m58.034s
```


### Try 2, 8 nodes-script (Working)
Use a script to run multiple command with mpiexe
source: https://stackoverflow.com/questions/52583517/can-i-use-mpiexec-to-run-the-same-executable-with-different-command-line-argumen

```bash
# /g/data/wb00/admin/staging/ClimaX/global_8.sh
# chmod 775 global_8.sh
set -x
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global

time python \
	/g/data/wb00/admin/staging/ClimaX/src/climax/global_forecast/train.py \
	--config '/g/data/wb00/admin/staging/ClimaX/configs/global_forecast_climax.yaml' \
	--trainer.num_nodes=2 \
	--trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --data.num_workers=1 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5

```


Output
```bash
real 17m28.134s
user 16m32.800s
sys 1m47.018s
```

### Next, try mpi (None worked)
```bash
##################
module load openmpi/4.1.4
# mpiexec -n 2 /g/data/wb00/admin/staging/ClimaX/global_8.sh  # Error both run on the first node
# mpiexec -np 2 -N 1 --pernode /g/data/wb00/admin/staging/ClimaX/global_8.sh # Forgot io use Host
# mpiexec -np 2 --host gadi-gpu-v100-0084:1,gadi-gpu-v100-0086:1 /g/data/wb00/admin/staging/ClimaX/global_8.sh # Not working
# mpirun -np 2 --bynode -host gadi-gpu-v100-0084,gadi-gpu-v100-0086 /g/data/wb00/admin/staging/ClimaX/global_8.sh # Not working

#mpirun -np 2 \
#    -H gadi-gpu-v100-0084:1,gadi-gpu-v100-0086:1 \
#    -bind-to none -map-by slot \
#    --pernode -N 1 \
#    /g/data/wb00/admin/staging/ClimaX/global_8.sh   
```

##### Error-1
MPI failed (but scripts run on both nodes, works)
Have to specify how many process per node (https://www.open-mpi.org/doc/v3.0/man1/mpiexec.1.php)
```bash
mpiexec was unable to start the specified application as it encountered an
error:

Error code: -125
Error name: The specified application failed to start
```

### MPI, Next Try (Not running on the othe node)
After adding shebang at the top
```bash
 mpirun -np 2 \
    -H gadi-gpu-v100-0084:1,gadi-gpu-v100-0086:1 \
     /g/data/wb00/admin/staging/ClimaX/global_8.sh
```
#### Error
```bash
 Error:             Exec format error
```
#### Solution
Add shebang to the scrtipt, 
```bash
#!/usr/bin/env bash

# rest of script
# ... 
```
Source: https://stackoverflow.com/questions/52622280/error-when-using-mpirun-with-a-shell-script

### MPI, Next try - display allocation (not worked)
```
mpiexec -np 2 -H gadi-gpu-v100-0084:1,gadi-gpu-v100-0086:1 \
    --map-by node --pernode \
    --display-allocation hostname 
mpiexec -np 2 -H gadi-gpu-v100-0084:1,gadi-gpu-v100-0086:1 \
    --display-allocation hostname 

# Not work	
# mpirun -np 2 \
#    -H gadi-gpu-v100-0084:1,gadi-gpu-v100-0086:1 \
#    --map-by node -pernode 1 \
#    /bin/bash /g/data/wb00/admin/staging/ClimaX/global_8.sh 

mpiexec -np 2 -H gadi-gpu-v100-0084:1,gadi-gpu-v100-0086:1 \
    --map-by node --pernode \
	/g/data/wb00/admin/staging/ClimaX/global_8.sh 
 

```
#### Problem
when mpi is run from other node than the PBS allocation?
Binding problem.

##### MPI resource allocation check
https://www.ibm.com/docs/en/smpi/10.2?topic=command-mpirun-options

### MPI, Next Try (2023.02.19)
##### Try from headnode
Note: MPI does not run from the head node wtithout loading the module
defult PBS mpi hangs
```bash
qsub -I /home/900/mah900/conda/g8_h4.pbs

# mpiexec hang on the leading node, when no argument is given
 [mah900@gadi-gpu-v100-0036 ~]$ mpiexec

# Version
[mah900@gadi-gpu-v100-0036 ~]$ mpiexec --version
pbs_version = 2021.1.3.20220217134230

# Nodes
gadi-gpu-v100-0036/0*48+gadi-gpu-v100-0039/0*48

# This hangs, too
mpiexec -np 2 -H gadi-gpu-v100-0036:1,gadi-gpu-v100-0039:1  hostname

# Works 
$  mpiexec -np 2 -H gadi-gpu-v100-0036:1,gadi-gpu-v100-0039:1  hostname
gadi-gpu-v100-0036.gadi.nci.org.au
gadi-gpu-v100-0039.gadi.nci.org.au

```


```bash
# Does Not work
mpirun -np 2 \
    -H gadi-gpu-v100-0036:1,gadi-gpu-v100-0039:1 \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    /g/data/wb00/admin/staging/ClimaX/global_8.sh 
# Does Not work
mpirun -np 2 \
    -H gadi-gpu-v100-0036:1,gadi-gpu-v100-0039:1 \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    /g/data/wb00/admin/staging/ClimaX/global_8.sh 

```
The same problem all are running on the first node 


### MPI, Next Test
Make to test file to see if they are running on differnt nodes or not?
```bash
#tesh.sh file
echo `hostname`
#set -v 
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global
echo $CONDA_PREFIX
echo `hostname`
```

```bash
mpirun -np 2 \
    -H gadi-gpu-v100-0028:1,gadi-gpu-v100-0084:1 \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    /bin/bash /scratch/fp0/mah900/ClimaX/test.sh
```
So, both running on differnt noded but display on the first node.
MPI was running on two different nodes along???
That means pytorch lighting setup problem???
That means they cloud not find each other and env varialbes have to be set within the shell???
That was the problem along

#### Next plan, (Not woked)
Head nodes write all nodes to a file 
Then, all nodes read and comare with thier respective hostnames to find the rank.
The script runs only once in first node.
```bash
# Store all node names, run in the first node
cat $PBS_NODEFILE | uniq > /g/data/wb00/admin/staging/ClimaX/cls_nodes
# cat $PBS_NODEFILE | uniq > /tmp/cls_node # do not work 

# Read and find rank 
# test2.sh
# chmod 775 test2.sh
i=0
HOST=`hostname`
RANK=-1
cat /g/data/wb00/admin/staging/ClimaX/cls_nodes | while read line || [[ -n $line ]];
do
   echo -ne "${i}. "  
   echo $line  
   if [[ "$line" == "$HOST" ]]; then
	   RANK=$i
	   echo "matched"
   fi	   
   i=$((i+1))
done
echo $HOST
echo $RANK
```
Does not work, because do loop runs inside a sub-shell. 
Need to change the code. 
Source: https://unix.stackexchange.com/questions/402750/modify-global-variable-in-while-loop

#### Working
```bash
i=0
HOST=`hostname`
RANK=-1
while read line; do
   #echo -ne "${i}. "  
   #echo $line  
   if [[ "$line" == "$HOST" ]]; then
	   RANK=$i
   fi	   
   i=$((i+1))
done < /g/data/wb00/admin/staging/ClimaX/cls_nodes 
#echo $HOST
#echo $RANK
export NODE_RANK=${RANK}
export WORLD_SIZE=8 
export MASTER_ADDR=$(head -n 1 /g/data/wb00/admin/staging/ClimaX/cls_nodes)
export MASTER_PORT=10001

echo "${NODE_RANK} ${WORLD_SIZE} ${MASTER_ADDR}  ${MASTER_PORT}"
 
```
Fed txt file in to the loop directly
https://unix.stackexchange.com/questions/530979/cat-file-in-for-loop-keeping-format-of-data-on-1-line-once-in-loop

#### MPI run (working)
```bash
# Nodes
# gadi-gpu-v100-0084/0*48+gadi-gpu-v100-0114/0*48

mpirun -np 2 \
    -H gadi-gpu-v100-0084:1,gadi-gpu-v100-0114:1 \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    /bin/bash /scratch/fp0/mah900/ClimaX/test2.sh
```


### Next try, Script  update
A lot of work, have to be done later.
```bash
# Run only once on 
cd /g/data/wb00/admin/staging/ClimaX
cat $PBS_NODEFILE | uniq > ./cls_nodes

# Script (updated)
#################################################################
#!/usr/bin/env bash
#set -v
cd /g/data/wb00/admin/staging/ClimaX
i=0
HOST=`hostname`
RANK=-1
while read line; do
   if [[ "$line" == "$HOST" ]]; then
	   RANK=$i
   fi	   
   i=$((i+1))
done < ./cls_nodes    
#done < /g/data/wb00/admin/staging/ClimaX/cls_nodes 
export NODE_RANK=${RANK}
export WORLD_SIZE=8 
export MASTER_ADDR=$(head -n 1 ./cls_nodes)
export MASTER_PORT=10001

echo "${NODE_RANK} ${WORLD_SIZE} ${MASTER_ADDR}  ${MASTER_PORT}"
echo "=========================================================="
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax
export OUTPUT_DIR=/g/data/wb00/admin/staging/climax_train_global

time python \
	/g/data/wb00/admin/staging/ClimaX/src/climax/global_forecast/train.py \
	--config '/g/data/wb00/admin/staging/ClimaX/configs/global_forecast_climax.yaml' \
	--trainer.num_nodes=2 \
	--trainer.strategy=ddp --trainer.devices=4 \
    --trainer.max_epochs=1 \
    --data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
    --data.predict_range=72 \
    --data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
    --data.batch_size=16 \
    --data.num_workers=1 \
    --model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
    --model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
    --model.weight_decay=1e-5

```
###### NCI PBS variables
https://opus.nci.org.au/display/Help/Useful+PBS+Environment+Variables

### MPI (Working)
```bash
# Before mpi run, to save node names
cd /g/data/wb00/admin/staging/ClimaX
cat $PBS_NODEFILE | uniq > ./cls_nodes
# Nodes
# gadi-gpu-v100-0084/0*48+gadi-gpu-v100-0114/0*48
module load openmpi
mpirun -np 2 \
    -H gadi-gpu-v100-0084:1,gadi-gpu-v100-0114:1 \
    -bind-to none --map-by slot \
    -x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
    -mca pml ob1 -mca btl ^openib \
    /g/data/wb00/admin/staging/ClimaX/global_8.sh
```

#### Output observations
```bash
...
----------------------------------------------------------------------------------------------------
distributed_backend=nccl
All distributed processes registered. Starting with 8 processes
----------------------------------------------------------------------------------------------------
...
...
gadi-gpu-v100-0084:685711:685761 [2] NCCL INFO Connected all trees
...
...
LOCAL_RANK: 0 - CUDA_VISIBLE_DEVICES: [0,1,2,3]
LOCAL_RANK: 0 - CUDA_VISIBLE_DEVICES: [0,1,2,3]
LOCAL_RANK: 3 - CUDA_VISIBLE_DEVICES: [0,1,2,3]
LOCAL_RANK: 2 - CUDA_VISIBLE_DEVICES: [0,1,2,3]
LOCAL_RANK: 2 - CUDA_VISIBLE_DEVICES: [0,1,2,3]
LOCAL_RANK: 3 - CUDA_VISIBLE_DEVICES: [0,1,2,3]
LOCAL_RANK: 1 - CUDA_VISIBLE_DEVICES: [0,1,2,3]
LOCAL_RANK: 1 - CUDA_VISIBLE_DEVICES: [0,1,2,3]
...
...
real 20m11.228s
user 17m40.372s
sys 1m54.412s
  
real 20m6.770s
user 17m39.106s
sys 1m43.867s

# Second run
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO Trees [0] -1/-1/-1->1->0 [1] -1/-1/-1->1->0
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Trees [0] 3/6/-1->2->-1 [1] 3/-1/-1->2->6
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Channel 00/02 :    0   3   6   5   4   7   2   1
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Trees [0] 0/-1/-1->3->2 [1] 0/-1/-1->3->2
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO Setting affinity for GPU 1 to 1c718f
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Channel 01/02 :    0   3   6   5   4   7   2   1
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Setting affinity for GPU 2 to 1c71,8f000000
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Setting affinity for GPU 3 to 1c71,8f000000
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Trees [0] 1/-1/-1->0->3 [1] 1/-1/-1->0->3
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Setting affinity for GPU 0 to 1c718f
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Trees [0] 7/-1/-1->6->2 [1] 7/2/-1->6->-1
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Trees [0] 4/-1/-1->7->6 [1] 4/-1/-1->7->6
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO Trees [0] 5/-1/-1->4->7 [1] 5/-1/-1->4->7
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Setting affinity for GPU 2 to 1c71,8f000000
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Setting affinity for GPU 3 to 1c71,8f000000
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO Setting affinity for GPU 0 to 1c718f
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO Trees [0] -1/-1/-1->5->4 [1] -1/-1/-1->5->4
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO Setting affinity for GPU 1 to 1c718f
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Channel 00 : 0[3d000] -> 3[b2000] via P2P/IPC
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Channel 01 : 0[3d000] -> 3[b2000] via P2P/IPC
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO Channel 00 : 4[3d000] -> 7[b2000] via P2P/IPC
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO Channel 01 : 4[3d000] -> 7[b2000] via P2P/IPC
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO Channel 00 : 1[3e000] -> 0[3d000] via P2P/IPC
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO Channel 00 : 5[3e000] -> 4[3d000] via P2P/IPC
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO Channel 01 : 1[3e000] -> 0[3d000] via P2P/IPC
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO Channel 01 : 5[3e000] -> 4[3d000] via P2P/IPC
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 00 : 7[b2000] -> 2[b1000] [receive] via NET/IB/0
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 00 : 3[b2000] -> 6[b1000] [receive] via NET/IB/0
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Channel 00 : 7[b2000] -> 2[b1000] [send] via NET/IB/0
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Channel 00 : 3[b2000] -> 6[b1000] [send] via NET/IB/0
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 01 : 7[b2000] -> 2[b1000] [receive] via NET/IB/0
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 01 : 3[b2000] -> 6[b1000] [receive] via NET/IB/0
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Channel 01 : 7[b2000] -> 2[b1000] [send] via NET/IB/0
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Channel 01 : 3[b2000] -> 6[b1000] [send] via NET/IB/0
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO Connected all rings
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO Channel 00 : 4[3d000] -> 5[3e000] via P2P/IPC
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Connected all rings
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO Channel 01 : 4[3d000] -> 5[3e000] via P2P/IPC
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Channel 00 : 0[3d000] -> 1[3e000] via P2P/IPC
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Connected all rings
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Channel 01 : 0[3d000] -> 1[3e000] via P2P/IPC
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Connected all rings
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 00 : 2[b1000] -> 1[3e000] via P2P/IPC
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 01 : 2[b1000] -> 1[3e000] via P2P/IPC
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 00 : 6[b1000] -> 5[3e000] via P2P/IPC
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 01 : 6[b1000] -> 5[3e000] via P2P/IPC
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Connected all rings
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Connected all rings
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 00 : 2[b1000] -> 3[b2000] via P2P/IPC
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO Connected all rings
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 01 : 2[b1000] -> 3[b2000] via P2P/IPC
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 00 : 6[b1000] -> 7[b2000] via P2P/IPC
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO Connected all rings
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 01 : 6[b1000] -> 7[b2000] via P2P/IPC
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Channel 00 : 3[b2000] -> 0[3d000] via P2P/IPC
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Channel 01 : 3[b2000] -> 0[3d000] via P2P/IPC
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Channel 00 : 7[b2000] -> 4[3d000] via P2P/IPC
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Channel 01 : 7[b2000] -> 4[3d000] via P2P/IPC
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO Connected all trees
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO threadThresholds 8/8/64 | 64/8/64 | 8/8/512
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO 2 coll channels, 2 p2p channels, 1 p2p channels per peer
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO Connected all trees
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO threadThresholds 8/8/64 | 64/8/64 | 8/8/512
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO 2 coll channels, 2 p2p channels, 1 p2p channels per peer
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO Connected all trees
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Channel 00 : 3[b2000] -> 2[b1000] via P2P/IPC
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO threadThresholds 8/8/64 | 64/8/64 | 8/8/512
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO 2 coll channels, 2 p2p channels, 1 p2p channels per peer
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Channel 01 : 3[b2000] -> 2[b1000] via P2P/IPC
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO Connected all trees
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO threadThresholds 8/8/64 | 64/8/64 | 8/8/512
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO 2 coll channels, 2 p2p channels, 1 p2p channels per peer
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Channel 00 : 7[b2000] -> 6[b1000] via P2P/IPC
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Channel 01 : 7[b2000] -> 6[b1000] via P2P/IPC
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 00 : 6[b1000] -> 2[b1000] [receive] via NET/IB/0
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 00 : 2[b1000] -> 6[b1000] [receive] via NET/IB/0
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 01 : 6[b1000] -> 2[b1000] [receive] via NET/IB/0
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 01 : 2[b1000] -> 6[b1000] [receive] via NET/IB/0
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 00 : 2[b1000] -> 6[b1000] [send] via NET/IB/0
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 00 : 6[b1000] -> 2[b1000] [send] via NET/IB/0
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Channel 01 : 2[b1000] -> 6[b1000] [send] via NET/IB/0
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Channel 01 : 6[b1000] -> 2[b1000] [send] via NET/IB/0
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO Connected all trees
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO threadThresholds 8/8/64 | 64/8/64 | 8/8/512
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO 2 coll channels, 2 p2p channels, 1 p2p channels per peer
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO Connected all trees
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO threadThresholds 8/8/64 | 64/8/64 | 8/8/512
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO 2 coll channels, 2 p2p channels, 1 p2p channels per peer
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO Connected all trees
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO threadThresholds 8/8/64 | 64/8/64 | 8/8/512
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO 2 coll channels, 2 p2p channels, 1 p2p channels per peer
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO Connected all trees
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO threadThresholds 8/8/64 | 64/8/64 | 8/8/512
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO 2 coll channels, 2 p2p channels, 1 p2p channels per peer
gadi-gpu-v100-0084:687626:687679 [2] NCCL INFO comm 0x14703c0030d0 rank 2 nranks 8 cudaDev 2 busId b1000 - Init COMPLETE
gadi-gpu-v100-0084:687631:687677 [3] NCCL INFO comm 0x146a940030d0 rank 3 nranks 8 cudaDev 3 busId b2000 - Init COMPLETE
gadi-gpu-v100-0084:687620:687673 [0] NCCL INFO comm 0x145fdc0030d0 rank 0 nranks 8 cudaDev 0 busId 3d000 - Init COMPLETE
gadi-gpu-v100-0084:687624:687678 [1] NCCL INFO comm 0x1483700030d0 rank 1 nranks 8 cudaDev 1 busId 3e000 - Init COMPLETE
gadi-gpu-v100-0084:687620:687620 [0] NCCL INFO Launch mode Parallel
gadi-gpu-v100-0114:3156335:3156380 [2] NCCL INFO comm 0x1479c00030d0 rank 6 nranks 8 cudaDev 2 busId b1000 - Init COMPLETE
gadi-gpu-v100-0114:3156339:3156381 [3] NCCL INFO comm 0x1489cc0030d0 rank 7 nranks 8 cudaDev 3 busId b2000 - Init COMPLETE
gadi-gpu-v100-0114:3156327:3156378 [0] NCCL INFO comm 0x1516780030d0 rank 4 nranks 8 cudaDev 0 busId 3d000 - Init COMPLETE
gadi-gpu-v100-0114:3156333:3156379 [1] NCCL INFO comm 0x14f8b00030d0 rank 5 nranks 8 cudaDev 1 busId 3e000 - Init COMPLETE

real 17m15.728s
user 16m36.367s
sys 1m37.130s

real 17m16.132s
user 16m26.962s
sys 1m49.382s
```


###### Other ideas, NOT tried
Next try: use two mpi commands
https://www.ibm.com/support/pages/how-run-multiple-mpi-executables-different-thread-counts-using-spectrum-mpi
Next, try use a loop
 
 

