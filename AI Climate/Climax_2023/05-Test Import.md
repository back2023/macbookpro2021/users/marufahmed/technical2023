
### Test climax install 

#### Start the python shell
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climaX_test
python
```

#### Inside the python shell
```python
from torchdata import _torchdata as _torchdata
```

### ClimaX/src/climax/global_forecast
`global_forecast/datamodule.py`
```python
import os
from typing import Optional

import numpy as np
import torch
import torchdata.datapipes as dp
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, IterableDataset
from torchvision.transforms import transforms

from climax.pretrain.datamodule import collate_fn
from climax.pretrain.dataset import (
	Forecast,
	IndividualForecastDataIter,
	NpyReader,
	ShuffleIterableDataset,
)
```

`global_forecast/module.py`
```python
from typing import Any

import torch
from pytorch_lightning import LightningModule
from torchvision.transforms import transforms

from climax.arch import ClimaX
from climax.utils.lr_scheduler import LinearWarmupCosineAnnealingLR
from climax.utils.metrics import (
	lat_weighted_acc,
	lat_weighted_mse,
	lat_weighted_mse_val,
	lat_weighted_rmse,
)
from climax.utils.pos_embed import interpolate_pos_embed
```

`global_forecast/train.py`
```python
import os
from climax.global_forecast.datamodule import GlobalForecastDataModule
from climax.global_forecast.module import GlobalForecastModule
from pytorch_lightning.cli import LightningCLI
```

### ClimaX/src/climax/pretrain

`pretrain/dataset.py`
```python
import math
import os
import random

import numpy as np
import torch
from torch.utils.data import IterableDataset
```
`pretrain/train.py`
```python
import os

from pytorch_lightning.cli import LightningCLI

from climax.pretrain.datamodule import MultiSourceDataModule
from climax.pretrain.module import PretrainModule
```

### ClimaX/tree/main/src/climax/regional_forecast
`pretrain/datamodule.py`
```python
import os
from typing import Optional

import numpy as np
import torch
import torchdata.datapipes as dp
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, IterableDataset
from torchvision.transforms import transforms

from climax.pretrain.dataset import (
	Forecast,
	IndividualForecastDataIter,
	NpyReader,
	ShuffleIterableDataset,
)
from climax.utils.data_utils import get_region_info
```
###### Error
```python
>>> from climax.utils.data_utils import get_region_info
Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
ImportError: cannot import name 'get_region_info' from 'climax.utils.data_utils' (/scratch/fp0/mah900/ClimaX/src/climax/utils/data_utils.py)
```


`regional_forecast/train.py`
```python
import os
from climax.regional_forecast.datamodule import RegionalForecastDataModule
from climax.regional_forecast.module import RegionalForecastModule
from pytorch_lightning.cli import LightningCLI
```
###### Error
```python
>>> from climax.regional_forecast.datamodule import RegionalForecastDataModule
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'climax.regional_forecast'
>>> from climax.regional_forecast.module import RegionalForecastModule
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'climax.regional_forecast'
```
##### Check
`regional_forecast` is actually missing (2023.02.15)
```bash
ls -lh /scratch/fp0/mah900/ClimaX/src/climax
total 32K
-rw-r--r-- 1 mah900 fp0 8.8K Feb  9 10:39 arch.py
drwxr-sr-x 3 mah900 fp0 4.0K Feb 10 18:06 global_forecast
-rw-r--r-- 1 mah900 fp0   73 Feb  9 10:39 __init__.py
drwxr-sr-x 3 mah900 fp0 4.0K Feb 15 16:54 pretrain
drwxr-sr-x 2 mah900 fp0 4.0K Feb 15 16:59 __pycache__
drwxr-sr-x 3 mah900 fp0 4.0K Feb 10 11:15 utils
```
##### Further, check github
https://github.com/microsoft/ClimaX/pull/3
**[tung-nd]** commented  yesterday ' add regional forecasting'
added on: `commit af4dcaf3f28a8e4ad8c711f4772ca52bea9769b2`
https://github.com/microsoft/ClimaX/pull/3/commits/af4dcaf3f28a8e4ad8c711f4772ca52bea9769b2
So, it was not in the original repo, but added yersterday??
Then, need to pull again, the latest version (2022.02.15)

##### Update:
New Dir added and files are modified in the latest commit.  (2022.02.16)
No error now.

### ClimaX/tree/main/src/climax/utils
`utils/lr_scheduler.py`
```python
import math
import warnings
from typing import List

from torch.optim import Optimizer
from torch.optim.lr_scheduler import _LRScheduler
```

### ClimaX/src/climax/arch.py
`climax/arch.py`
```python
from functools import lru_cache

import numpy as np
import torch
import torch.nn as nn
from timm.models.vision_transformer import Block, PatchEmbed, trunc_normal_

from climax.utils.pos_embed import (
	get_1d_sincos_pos_embed_from_grid,
	get_2d_sincos_pos_embed,
)
```

### ClimaX/src/data_preprocessing
`data_preprocessing/nc2np_equally_era5.py`
```python
import glob
import os

import click
import numpy as np
import xarray as xr
from tqdm import tqdm

from climax.utils.data_utils import DEFAULT_PRESSURE_LEVELS, NAME_TO_VAR
```
