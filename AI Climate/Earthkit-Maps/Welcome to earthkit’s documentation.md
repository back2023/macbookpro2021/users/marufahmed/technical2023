
https://earthkit.readthedocs.io/en/latest/

**earthkit** is a new open-source Python project led by ECMWF, providing powerful tools for speeding up weather and climate science workflows by simplifying data access, processing, analysis, visualisation and much more.

**earthkit** offers **multiple interoperable** [software components](https://earthkit.readthedocs.io/en/latest/components_overview.html#components) built on top of well-established open-source Python libraries like numpy, pandas and matplotlib. earthkit also integrates and leverages the robust and operations-ready software stack that is familiar to ECMWF production systems (e.g. [ecCodes](https://confluence.ecmwf.int/display/ECC/ecCodes+Home), FDB, etc).

The interface of [earthkit components](https://earthkit.readthedocs.io/en/latest/components_overview.html#components) is designed to be high-level in order to provide common tools to support activities across ECMWF and beyond. The design of the components also takes **scalability** into account, so researchers can enjoy efficiency whilst providing easier transfer to operations.

**earthkit** is still under development but some components are already available in beta through PyPI and their GitHub repositories.





