

Nowcasting is weather forecasting on a very short term mesosacle period upto 6 hours. The goal is to give precise and timely prediction of precipitation, storm structure, hail potential, etc. in a local region over a short time period (eg., 0-6 hours). These predictions are useful for producing rainfall, storms, hails, etc alerts, providing weather guidance for airports, etc.

Weather Radar’s reflectivity is used by scientists to detect precipitation, evaluate storm structure, determine hail potential, etc. Sequence of radar reflectivity over a region for some time duration has spatiotemporal nature. Weather nowcasting is a spatiotemporal sequence forecasting problem with the sequence of past reflectivity maps as input and the sequence of future reflectivity maps as output.

The LSTM encoder-decoder framework provides a general framework for sequence-to-sequence learning problems. I have implemented Convolutional LSTM Encoder-Decoder Network [1] for weather forecasting with the sequences being maps of radar reflectivity.

## [](https://github.com/iamrakesh28/Deep-Learning-for-Weather-and-Climate-Science#weather-forecasting-using-nexrad)Weather Forecasting using NEXRAD

The Next Generation Weather Radar (NEXRAD) [3] system currently comprises 160 sites throughout the United States and select overseas locations. NEXRAD detects precipitation and atmospheric movement or wind. It returns data which when processed can be displayed in a mosaic map which shows patterns of precipitation and its movement. NEXRAD Level-II (Base) data include the original three meteorological base data quantities: reflectivity, mean radial velocity, and spectrum width. Data is collected from the radar sites usually at the interval of 4, 5, 6 or 10 minutes depending upon the volume coverage. Radar Data can be accessed at [https://www.ncdc.noaa.gov/nexradinv/](https://www.ncdc.noaa.gov/nexradinv/).

reflectivity is expressed in dBZ. Higher value of reflectivity tells heavy precipiation or hail at that place and lower value tells light precipiation. For examples, 65 dBZ means extremely heavy precipitation (410 mm per hour, but likely hail), 50 dBZ means heavy precipitation (51 mm per hour), 35 dBZ tells moderate precipitation of 6.4 mm per hour [2], and so on. So, reflectivity component from the Level-II data can be used for weather forecasting for short duration.