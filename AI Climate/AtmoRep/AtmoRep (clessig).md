

https://github.com/clessig/atmorep/


This repository contains the source code for the [AtmoRep](https://www.atmorep.org/) models for large scale representation learning of atmospheric dynamics as well as links to the pre-trained models and the required model input data.

The pre-print for the work is available on ArXiv: [https://arxiv.org/abs/2308.13280](https://arxiv.org/abs/2308.13280).


