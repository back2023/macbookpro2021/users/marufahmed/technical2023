
https://confluence.ecmwf.int/display/CKB/What+are+GRIB+files+and+how+can+I+read+them


## Introduction

The **GRIB** file format is designed for storing and distributing weather data. GRIB files are widely used in meteorological applications ([Wikipedia article](https://en.wikipedia.org/wiki/GRIB), including a list of software to read GRIB files).

GRIB stands for "General Regularly distributed Information in Binary form" and is a [WMO](https://old.wmo.int/extranet/pages/index_en.html) (World Meteorological Organisation) standard format for archiving and exchanging gridded data. GRIB is a binary format, and the data is packed to increase storage efficiency. GRIB messages are often concatenated together to form a GRIB file. GRIB files usually have the extension .grib, .grb or .gb.

Currently there are two different coding standards: **GRIB edition 1** (commonly referred to as [GRIB1](https://old.wmo.int/extranet/pages/prog/www/WMOCodes/Guides/GRIB/GRIB1-Contents.html)) and **GRIB edition 2** ([GRIB2](https://old.wmo.int/extranet/pages/prog/www/WMOCodes.html)). The major differences are in the structure of the messages; in GRIB2, several variables are defined with more precision (e.g. in GRIB1, latitudes and longitudes are in milli-degrees while in GRIB2, they are in micro-degrees). Also in GRIB2, longitude values must lie between 0 and 360 degrees), the encoding of the parameter is very different, and in GRIB2 the description of the data is template/table based. Note that a GRIB file can contain a mix of GRIB1 and GRIB2 messages.

Please see the WMO "[Introduction to GRIB Edition 1 and GRIB Edition 2](https://old.wmo.int/extranet/pages/prog/www/WMOCodes/Guides/GRIB/Introduction_GRIB1-GRIB2.pdf)" documentation for further details. 

The ECMWF model (the Integrated Forecasting System, IFS) currently outputs model-level fields in GRIB2 while pressure and surface level outputs are produced in GRIB1. For example,**ERA-Interim** (a climate reanalysis dataset provided by ECMWF) is produced in the GRIB edition 1 format. The ERA-Interim data is then made available for download in its native GRIB format.

## GRIB conversion to netCDF

In some cases, data is also available in NetCDF format as the result of the conversion of the GRIB file to NetCDF. Note that due to this conversion, not all the information in the GRIB file will be included in the NetCDF version, and his is particularly true for the GRIB file metadata. As a result, care should be taken when using these files. At this time, the NetCDF format is not formally supported by ECMWF.


  
## How to read GRIB files
[Metview](https://confluence.ecmwf.int/display/METV/Metview) is a software tool from ECMWF which allows users to read, process and visualise GRIB 1 and GRIB 2 data (see [Metview](https://confluence.ecmwf.int/display/METV/Metview) documentation).

Common GUI tools for reading and visualising GRIB files are:

- [Panoply](http://www.giss.nasa.gov/tools/panoply/) by NASA
- [MetView](https://confluence.ecmwf.int/display/METV) by ECMWF
- [Integrated Data Viewer (IDV)](http://www.unidata.ucar.edu/software/idv/) by UCAR unidata
- [zyGRIB](http://www.zygrib.org/)
- [ArcGIS](http://desktop.arcgis.com/en/arcmap/10.3/manage-data/raster-and-images/multidimensional-raster-types.htm) by ESRI

For users comfortable with command line tools:

- [ecCodes](https://confluence.ecmwf.int/display/ECC/ecCodes+Home) by ECMWF  
    - Supports GRIB to NetCDF conversion (grib_to_netcdf -o netcdf_file grib_file)
    - Supports GRIB to JSON (`grib_dump -j <gribfile>` )
    - See tools and code examples for [Fortran, Python and C](https://confluence.ecmwf.int/display/ECC/API+examples)
- [CDO info operators](https://code.zmaw.de/projects/cdo/embedded/index.html#x1-390002.1) (info, infon, sinfo, sinfon)
- [NCO](http://nco.sourceforge.net/)
- [GrADS](http://cola.gmu.edu/grads/)
- [wgrib2](http://www.cpc.ncep.noaa.gov/products/wesley/wgrib2/), only for GRIB edition 2, wgrib2 cannot decode GRIB edition 1 files
- [IDL](http://www.exelisvis.co.uk/ProductsServices/IDL.aspx)
- the [GDAL library](http://www.gdal.org/frmt_grib.html), with limited support for coordinate systems, georeferencing and metadata. In particular irregular (non-cartesian) coordinate systems might not be decoded correctly.

More tools are listed in [this Wikipedia article](https://en.wikipedia.org/wiki/GRIB#Software).







