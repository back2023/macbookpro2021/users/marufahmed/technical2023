
https://confluence.ecmwf.int/display/ECC/grib_get_keys

# [grib_get_keys](https://confluence.ecmwf.int/display/ECC/grib_get_keys)
- Created by [Sandor Kertesz](https://confluence.ecmwf.int/display/~cgr), last modified by [Shahram Najm](https://confluence.ecmwf.int/display/~masn) on [Dec 28, 2022](https://confluence.ecmwf.int/pages/diffpagesbyversion.action?pageId=46600858&selectedPageVersions=17&selectedPageVersions=18 "Show changes")
### Description

This example shows: _how to get values using keys from GRIB messages_

### Source code - - [Python](https://confluence.ecmwf.int/display/ECC/grib_get_keys#Python)
https://confluence.ecmwf.int/display/ECC/grib_get_keys#Python

```python
# (C) Copyright 2005- ECMWF.
#
# This software is licensed under the terms of the Apache Licence Version 2.0
# which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
#
# In applying this licence, ECMWF does not waive the privileges and immunities
# granted to it by virtue of its status as an intergovernmental organisation
# nor does it submit to any jurisdiction.
 
import traceback
import sys
 
from eccodes import *
 
INPUT = '../../data/reduced_latlon_surface.grib1'
VERBOSE = 1  # verbose error reporting
 
 
def example():
    f = open(INPUT, 'rb')
 
    keys = [
        'Ni',
        'Nj',
        'latitudeOfFirstGridPointInDegrees',
        'longitudeOfFirstGridPointInDegrees',
        'latitudeOfLastGridPointInDegrees',
        'longitudeOfLastGridPointInDegrees',
    ]
 
    while 1:
        gid = codes_grib_new_from_file(f)
        if gid is None:
            break
 
        for key in keys:
            try:
                print('  %s: %s' % (key, codes_get(gid, key)))
            except KeyValueNotFoundError as err:
                # Full list of exceptions here:
                #   https://confluence.ecmwf.int/display/ECC/Python+exception+classes
                print('  Key="%s" was not found: %s' % (key, err.msg))
            except CodesInternalError as err:
                print('Error with key="%s" : %s' % (key, err.msg))
 
        print('There are %d values, average is %f, min is %f, max is %f' % (
            codes_get_size(gid, 'values'),
            codes_get(gid, 'average'),
            codes_get(gid, 'min'),
            codes_get(gid, 'max')
        ))
 
        codes_release(gid)
 
    f.close()
 
 
def main():
    try:
        example()
    except CodesInternalError as err:
        if VERBOSE:
            traceback.print_exc(file=sys.stderr)
        else:
            sys.stderr.write(err.msg + '\n')
 
        return 1
 
 
if __name__ == "__main__":
    sys.exit(main())
```



# Trace the location of `gribapi.py`

```python
import traceback
import sys
 
from eccodes import *

codes_grib_new_from_file("")
...
TypeError
...
File /scratch/fp0/mah900/env/ai-models/lib/python3.10/site-packages/gribapi/gribapi.py:402

```
