
https://confluence.ecmwf.int/display/ECC/GRIB+Parameters+Database


The GRIB Parameter database can be found [here.](http://codes.ecmwf.int/grib/param-db)

This database lists all GRIB parameters known to ecCodes with their Name, Short Name, Units, Parameter ID (unique) and available file formats.  
The corresponding ecCodes keys are: `name`, `shortName`, `units` and `paramId`.


