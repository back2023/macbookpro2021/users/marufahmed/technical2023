
https://confluence.ecmwf.int/display/CEMS/GRIB+to+NetCDF+conversion


```python
import xarray as xr
import argparse
import sys
 
def convert(input, output):
 
    try:
        ds = xr.open_dataset(
                        input,
                        engine='cfgrib',
                        backend_kwargs={'indexpath':''}
                        )
 
    except FileNotFoundError:
        sys.exit("File was not found : {}".format(input))
 
    ds.to_netcdf(output)
 
```
