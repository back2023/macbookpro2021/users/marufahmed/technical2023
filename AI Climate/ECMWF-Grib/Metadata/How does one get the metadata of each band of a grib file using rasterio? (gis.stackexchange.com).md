
https://gis.stackexchange.com/questions/422737/how-does-one-get-the-metadata-of-each-band-of-a-grib-file-using-rasterio


```python
import rasterio 

with rasterio.open(input_file) as src:
    print(src.tags(band_number))
```

