
https://stackoverflow.com/questions/15432587/converting-netcdf-to-grib2


```python
import iris

cubes = iris.load('input.nc')       # each variable in the netcdf file is a cube
iris.save(cubes[0],'output.grib2')  # save a specific variable to grib 
```






