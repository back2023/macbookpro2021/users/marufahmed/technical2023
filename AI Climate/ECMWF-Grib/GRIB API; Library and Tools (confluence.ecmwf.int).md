
https://confluence.ecmwf.int/display/OPTR/GRIB+API%3A+Library+and+Tools


09:15 [GRIB API Introduction](https://confluence.ecmwf.int/download/attachments/45745418/gribapi1_intro_2015.pdf?version=1&modificationDate=1446205573800&api=v2) - Shahram Najm  
- GRIB editions 1 and 2: structure and differences  
- GRIB API design and concepts

11:00 [GRIB API: Tools](https://confluence.ecmwf.int/download/attachments/45745418/grib_tools.pdf?version=1&modificationDate=1446201249244&api=v2) - Carsten Maass  
- Introduction to the command-line tools
- Obtaining information about the grib_api installation with grib_info
- Inspecting the content of GRIB files with grib_count,  grib_ls and grib_dump
[Solutions to grib_dump and grib_ls practical](https://confluence.ecmwf.int/pages/viewpage.action?pageId=45757288)


12:15 [GRIB API: Keys](https://confluence.ecmwf.int/download/attachments/45745418/gribapi2_keys_2015.pdf?version=1&modificationDate=1446205511554&api=v2) - Shahram Najm  
- Coded and computed keys
- Namespaces
- Examples of keys

14:00 [GRIB API: Tools](https://confluence.ecmwf.int/download/attachments/45745418/grib_tools.pdf?version=1&modificationDate=1446201249244&api=v2) - Paul Dando  
- Using grib_get
- Printing data values with grib_get_data
- Comparing the content of GRIB messages with grib_compare


09:00 GRIB API: [Fortran, C, Python APIs - Basics](https://confluence.ecmwf.int/download/attachments/45745418/grib_api_1_2015.pdf?version=2&modificationDate=1424875912184&api=v2) - Dominique Lucas, Xavi Abellan, Cristian Simarro  
- Overview of the API
- Using the API functions for decoding with Fortran, C and Python
-  [Practicals](https://confluence.ecmwf.int/download/attachments/45745418/grib_api_1_practicals_2015.pdf?version=1&modificationDate=1424773693638&api=v2)


14:00 [GRIB API: Advanced tools](https://confluence.ecmwf.int/download/attachments/45745418/grib_api_advanced_tools.pdf?version=1&modificationDate=1425310059178&api=v2) - Paul Dando  

- Basic features of grib_filter
- Conversion from GRIB to NetCDF with grib_to_netcdf


09:00 GRIB API: [Fortran, C, python APIs - Advanced features](https://confluence.ecmwf.int/download/attachments/45745418/grib_api_2_2015.pdf?version=2&modificationDate=1424961181228&api=v2) - Dominique Lucas, Xavi Abellan, Cristian Simarro  

- Use of the indexing routines
- Encoding GRIB messages and using GRIB samples  
- [Practicals](https://confluence.ecmwf.int/download/attachments/45745418/grib_api_2_practicals_2015.pdf?version=1&modificationDate=1424773777359&api=v2)

14:00 GRIB API: [Python interface](https://confluence.ecmwf.int/download/attachments/45745418/python-grib_2015.pdf?version=1&modificationDate=1424941969442&api=v2) - Xavi Abellan  

- A short introduction to Python and its use at ECMWF
- The Python interface to grib_api for decoding, encoding and indexed access to GRIB messages

6:00 [GRIB API: Advanced topics I](https://confluence.ecmwf.int/download/attachments/45745418/gribapi3_adv1_2015.pdf?version=1&modificationDate=1446205411069&api=v2) - Shahram Najm
- Simple packing
- Constant fields
- Bitmap fields
- Multi fields

09:00 [GRIB API: Advanced topics II](https://confluence.ecmwf.int/download/attachments/45745418/gribapi4_adv2_2015.pdf?version=2&modificationDate=1446205389075&api=v2) - Shahram Najm

- Parameter database
- Conversion from GRIB 1 to GRIB 2

11:00 [GRIB API: Advanced topics II](https://confluence.ecmwf.int/download/attachments/45745418/gribapi4_adv2_2015.pdf?version=2&modificationDate=1446205389075&api=v2) - Shahram Najm  

- Local adaptation - how to define your own local parameters

12:15 [Quiz](https://confluence.ecmwf.int/download/attachments/45745418/gribapi5_quiz_2015.pdf?version=1&modificationDate=1446205372222&api=v2)


