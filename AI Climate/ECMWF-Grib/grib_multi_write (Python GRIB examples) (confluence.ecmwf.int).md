
### Description

This example shows: _how to create a multi field message in memory and write it in a file. The multi field messages can be created only in GRIB edition 2._


### Source code - Python
https://confluence.ecmwf.int/display/ECC/grib_multi_write#Python

```python
import traceback
import sys
 
from eccodes import *
 
INPUT = '../../data/sample.grib2'
OUTPUT = 'out.mw.grib'
VERBOSE = 1  # verbose error reporting
 
 
def example():
    fin = open(INPUT, 'rb')
    fout = open(OUTPUT, 'wb')
 
    gid = codes_grib_new_from_file(fin)
 
    mgid = codes_grib_multi_new()
 
    for step in range(12, 132, 12):
        codes_set(gid, "step", step)
        codes_grib_multi_append(gid, 4, mgid)
 
    codes_grib_multi_write(mgid, fout)
 
    codes_grib_multi_release(mgid)
    codes_release(gid)
    fin.close()
    fout.close()
 
 
def main():
    try:
        example()
    except CodesInternalError as err:
        if VERBOSE:
            traceback.print_exc(file=sys.stderr)
        else:
            sys.stderr.write(err.msg + '\n')
 
        return 1
 
 
if __name__ == "__main__":
    sys.exit(main())
```
