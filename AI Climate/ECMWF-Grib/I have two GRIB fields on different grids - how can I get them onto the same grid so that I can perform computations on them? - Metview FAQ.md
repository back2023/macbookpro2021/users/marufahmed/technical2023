
https://confluence.ecmwf.int/pages/viewpage.action?pageId=143038050



```python
import metview as mv
 
t = mv.retrieve(param='t', grid=[0.5, 0.5], area=[50, 10, 80, 40])
lsm = mv.retrieve(param='lsm', levtype='sfc', grid=[0.2, 0.2])
 
regridded_lsm = mv.regrid(data=lsm,
                          grid_definition_mode='template',
                          template_data=t)
```

