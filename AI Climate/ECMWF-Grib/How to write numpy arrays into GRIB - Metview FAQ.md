
https://confluence.ecmwf.int/display/UDOC/How+to+write+numpy+arrays+into+GRIB+-+Metview+FAQ

## Step-by-step guide

Suppose the we have a GRIB file, and we want to use some NumPy routines to manipulate the data values, and then write the result back to a GRIB file. The following Python code shows how to do this:

```python
import metview as mv
import numpy as np
 
g = mv.read("mydata.grib")[0] # work on the first field only
vals = np.reciprocal(g.values())
g = g.set_values(vals)
mv.write('recip.grib', g)
 
```