
https://confluence.ecmwf.int/display/METV/Processing+Data

**Metview's documentation is now on readthedocs!**
https://metview.readthedocs.io/en/latest/tutorials/data_analysis_and_vis/processing_data.html
# Overview

One of Metview's most powerful features is its data processing ability. Data from various sources can be combined and manipulated using high- or low-level commands.

