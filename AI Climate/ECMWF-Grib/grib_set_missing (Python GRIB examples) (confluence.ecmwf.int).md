
### Description

This example shows: _how to set a key as missing._

### Source code
https://confluence.ecmwf.int/display/ECC/grib_set_missing#Python