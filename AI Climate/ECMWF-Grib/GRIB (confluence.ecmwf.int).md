
## What is GRIB?

**GRIB** (**GRIdded Binary** or **General Regularly-distributed Information in Binary form)** is a common data format used in meteorology and [standardised](https://codes.wmo.int/_grib2) by the World Meteorological Organisation.

- There are two versions of GRIB: GRIB edition 1 (GRIB1) and edition 2 (GRIB2).
- CEMS-Flood data is available in **GRIB2**.
- It is archieved in GRIB2 in the **M**eteorological **A**rchival and **R**etrieval **S**ystem (MARS) at ECMWF, see the [MARS user documentation](https://confluence.ecmwf.int/display/UDOC/MARS+user+documentation) for further information.
- Further information on the data format is available in the [What are GRIB files](https://confluence.ecmwf.int/display/CKB/What+are+GRIB+files+and+how+can+I+read+them) page on the Copernicus Knowledge Base.

## Working with GRIB2 files

There are 2 principal ways to work with GRIB2 files:

      1. [ecCodes](https://confluence.ecmwf.int/display/ECC/What+is+ecCodes)

A library developed at ECMWF consisting of a set of tools to code and decode GRIBs, as well as other formats. The eccodes command-line utilities are commonly used by the meteorology community. Please consult the Copernicus Knowledge Base page [How to read GRIB files](https://confluence.ecmwf.int/display/CKB/What+are+GRIB+files+and+how+can+I+read+them) for an overview of the commands available.

      2. [CFGRIB](https://github.com/ecmwf/cfgrib)

A Python interface to the eccodes library. It is one of the supported backends of [Xarray](http://xarray.pydata.org/en/stable/). Please consult the [Open CEMS-Flood data page](https://confluence.ecmwf.int/display/CEMS/Open+CEMS-Flood+Data) for example of using CFGRIB with CEMS-Flood data.



