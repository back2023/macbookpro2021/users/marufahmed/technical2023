
https://herbie.readthedocs.io/en/latest/user_guide/grib2.html


- GRIB stands for “gridded binary” and is an international standard for meteorological data. There is an old standard GRIB, and a new standard GRIB2. In these documents, when I refer to GRIB I really mean the new GRIB2 standard.

- Yes, GRIB is notoriously difficult to work with and has a steep learning curve for those unfamiliar with the format. I won’t discuss here the good, bad, and ugly of GRIB frankly because I’m not an expert and probably will say something wrong. However, even seasoned meteorologists complain about GRIB. Since complaining won’t fix the problem, I choose to embrace it because, for now, NWP data is widely distributed as GRIB2.

- [Wikipedia: GRIB](https://en.wikipedia.org/wiki/GRIB)
- [ECMWF: What are GRIB files and how can I read them?](https://confluence.ecmwf.int/display/CKB/What+are+GRIB+files+and+how+can+I+read+them)


## Python Tools[](https://herbie.readthedocs.io/en/latest/user_guide/grib2.html#python-tools "Link to this heading")

There are two key python packages for reading GRIB2 files. Both can be installed via conda-forge.

- **pygrib** is what I started to learn and still use sometimes. | [Video Demo](https://youtu.be/yLoudFv3hAY) | [pygrib GitHub](https://github.com/jswhit/pygrib) |
    
- **cfgrib** works well reading GRIB2 data as xarray datasets. Make sure you have the latest version (>0.9.8) | [cfgrib GitHub](https://github.com/ecmwf/cfgrib)


