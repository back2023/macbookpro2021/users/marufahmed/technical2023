
https://github.com/google/weather-tools


# weather-tools

Apache Beam pipelines to make weather data accessible and useful.

## Introduction

This project contributes a series of command-line tools to make common data engineering tasks easier for researchers in climate and weather. These solutions were born out of the need to improve repeated work performed by research teams across Alphabet.

The first tool created was the weather downloader (`weather-dl`). This makes it easier to ingest data from the European Center for Medium Range Forecasts (ECMWF). `weather-dl` enables users to describe very specifically what data they'd like to ingest from ECMWF's catalogs. It also offers them control over how to parallelize requests, empowering users to [retrieve data efficiently](https://github.com/google/weather-tools/blob/main/Efficient-Requests.md). Downloads are driven from a [configuration file](https://github.com/google/weather-tools/blob/main/Configuration.md), which can be reviewed (and version-controlled) independently of pipeline or analysis code.

We also provide two additional tools to aid climate and weather researchers: the weather mover (`weather-mv`) and the weather splitter (`weather-sp`). These CLIs are still in their alpha stages of development. Yet, they have been used for production workflows for several partner teams.

We created the weather mover (`weather-mv`) to load geospatial data from cloud buckets into [Google BigQuery](https://cloud.google.com/bigquery). This enables rapid exploratory analysis and visualization of weather data: From BigQuery, scientists can load arbitrary climate data fields into a Pandas or XArray dataframe via a simple SQL query.

The weather splitter (`weather-sp`) helps normalize how archival weather data is stored in cloud buckets: Whether you're trying to merge two datasets with overlapping variables — or, you simply need to [open Grib data from XArray](https://github.com/ecmwf/cfgrib/issues/2), it's really useful to split datasets into their component variables.


