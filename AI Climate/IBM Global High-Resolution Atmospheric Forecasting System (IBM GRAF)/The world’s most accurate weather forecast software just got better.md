
IBM Global High-Resolution Atmospheric Forecasting System (IBM GRAF) helps democratize weather forecasts so people, businesses and governments—anywhere—can make better decisions.

https://www.ibm.com/weather/industries/cross-industry/graf


