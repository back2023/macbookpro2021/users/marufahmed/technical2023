

https://glossary.ametsoc.org/wiki/Climatological_forecast?__cf_chl_tk=fCtx_OvChQlo4tblss.TvUf8u4qu1zqU96Xl1Z4LUuY-1722559191-0.0.1.1-3924


# climatological forecast

Jump to:[navigation](https://glossary.ametsoc.org/wiki/Climatological_forecast?__cf_chl_tk=fCtx_OvChQlo4tblss.TvUf8u4qu1zqU96Xl1Z4LUuY-1722559191-0.0.1.1-3924#mw-navigation), [search](https://glossary.ametsoc.org/wiki/Climatological_forecast?__cf_chl_tk=fCtx_OvChQlo4tblss.TvUf8u4qu1zqU96Xl1Z4LUuY-1722559191-0.0.1.1-3924#p-search)

A forecast based solely upon the climatological [statistics](https://glossary.ametsoc.org/wiki/Statistics "Statistics") for a region rather than the dynamical implications of the current conditions.

Climatological forecasts are often used as a baseline for evaluating the performance of [weather](https://glossary.ametsoc.org/wiki/Weather_forecast "Weather forecast") and [climate forecasts](https://glossary.ametsoc.org/wiki/Climate_forecasts "Climate forecasts").


