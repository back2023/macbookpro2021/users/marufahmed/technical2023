
https://github.com/snap-stanford/GraphGym
# GraphGym

GraphGym is a platform for designing and evaluating Graph Neural Networks (GNN). GraphGym is proposed in _[Design Space for Graph Neural Networks](https://arxiv.org/abs/2011.08843)_, Jiaxuan You, Rex Ying, Jure Leskovec, **NeurIPS 2020 Spotlight**.

Please also refer to [PyG](https://www.pyg.org/) for a tightly integrated version of GraphGym and PyG.

### [](https://github.com/snap-stanford/GraphGym#highlights)Highlights

**1. Highly modularized pipeline for GNN**

- **Data:** Data loading, data splitting
- **Model:** Modularized GNN implementation
- **Tasks:** Node / edge / graph level GNN tasks
- **Evaluation:** Accuracy, ROC AUC, ...

**2. Reproducible experiment configuration**

- Each experiment is _fully described by a configuration file_

**3. Scalable experiment management**

- Easily launch _thousands of GNN experiments in parallel_
- _Auto-generate_ experiment analyses and figures across random seeds and experiments.

**4. Flexible user customization**

- Easily _register your own modules_ in [`graphgym/contrib/`](https://github.com/snap-stanford/GraphGym/blob/master/graphgym/contrib), such as data loaders, GNN layers, loss functions, etc.

