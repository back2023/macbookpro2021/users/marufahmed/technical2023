
### Try loading ERA5 data
```
msl_path='/g/data/rt52/era5/single-levels/reanalysis/msl'
msl_path+='/2022/msl_era5_oper_sfc_20220101-20220131.nc'
msl_data = xr.open_dataset(msl_path)
msl_data
```
Error-1
```bash 
ImportError: /g/data/wb00/admin/testing/conda/pangu_weather/lib/python3.9/site-packages/netCDF4/../../../libnetcdf.so.19: undefined symbol: H5Pset_fapl_ros3
```

Try
```bash
#1
import h5py; print(h5py.version.info)
...
ModuleNotFoundError: No module named 'h5py'
#2 (Installed h5py, but Not worked)
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /g/data/wb00/admin/testing/conda/pangu_weather
module load cuda/11.6.1 cudnn/8.2.2-cuda11.4 
pip uninstall h5py
pip install --no-cache-dir h5py
...
Requirement already satisfied: numpy>=1.17.3 in /home/900/mah900/.local/lib/python3.9/site-packages (from h5py) (1.22.4)
Installing collected packages: h5py
Successfully installed h5py-3.9.0
```
 Source #1: https://github.com/h5py/h5py/issues/1880#issuecomment-824110475
 Source #2: https://stackoverflow.com/a/73420441
 Next, try
```bash
conda uninstall h5py
...
PackagesNotFoundError: The following packages are missing from the target environment:
  - h5py

conda uninstall hdf5
pip uninstall h5py
pip install --no-cache-dir h5py
```

Error-2
```bash
AttributeError: module 'netCDF4' has no attribute 'Dataset'
```
Try (Not working)
```bash
pip uninstall netCDF4
WARNING: Skipping netCDF4 as it is not installed.

pip install netCDF4
...
Installing collected packages: cftime, netCDF4
Successfully installed cftime-1.6.2 netCDF4-1.6.4
```

Try (Not worked)
```
pip uninstall netCDF4
conda uninstall xarray
conda uninstall xarray dask bottleneck
pip uninstall xarray dask netCDF4 bottleneck
pip uninstall xarray dask netCDF4 bottleneck
...
conda install -c conda-forge xarray dask netCDF4 bottleneck
```
 Source: https://docs.xarray.dev/en/stable/getting-started-guide/installing.html#instructions

Try (Working, `netCDF4=1.5` )
```bash
conda uninstall xarray dask netCDF4 bottleneck
conda install netCDF4=1.5 # Found Dataset
conda uninstall netCDF

conda install -c conda-forge xarray dask netCDF4=1.5 bottleneck
```

Make a copy for NCI data.
```

```