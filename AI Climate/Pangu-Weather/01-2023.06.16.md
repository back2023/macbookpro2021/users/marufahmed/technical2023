Source: https://github.com/198808xc/Pangu-Weather/tree/b5850da258ee738bfda6e61138b436842305f0ec

### Dir structure
```bash
mkdir  /g/data/wb00/admin/testing/Pangu-Weather/
cd /g/data/wb00/admin/testing/Pangu-Weather/
mkdir root
mkdir input_data output_data

```

### Download from google drive (Working)
Source: https://medium.com/@acpanjan/download-google-drive-files-using-wget-3c2c025a8b99
```bash
# Data
#Code
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=${FILEID}' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=${FILEID}" -O ${FILENAME} && rm -rf /tmp/cookies.txt

cd /g/data/wb00/admin/testing/Pangu-Weather/root

# The 1-hour model (pangu_weather_1.onnx)
https://drive.google.com/file/d/1fg5jkiN_5dHzKb-5H9Aw4MOmfILmeY-S/view?usp=share_link

FILEID=1fg5jkiN_5dHzKb-5H9Aw4MOmfILmeY-S 
FILENAME=pangu_weather_1.onnx

# The 3-hour model (pangu_weather_3.onnx): 
https://drive.google.com/file/d/1EdoLlAXqE9iZLt9Ej9i-JW9LTJ9Jtewt/view?usp=share_link

FILEID=1EdoLlAXqE9iZLt9Ej9i-JW9LTJ9Jtewt
FILENAME=pangu_weather_3.onnx

# The 6-hour model (pangu_weather_6.onnx):
https://drive.google.com/file/d/1a4XTktkZa5GCtjQxDJb_fNaqTAUiEJu4/view?usp=share_link

FILEID=1a4XTktkZa5GCtjQxDJb_fNaqTAUiEJu4 
FILENAME=pangu_weather_6.onnx 

# The 24-hour model (pangu_weather_24.onnx):
https://drive.google.com/file/d/1lweQlxcn9fG0zKNW8ne1Khr9ehRTI6HP/view?usp=share_link

FILEID=1lweQlxcn9fG0zKNW8ne1Khr9ehRTI6HP 
FILENAME=pangu_weather_24.onnx 


cd input_data

# input_surface.npy:
https://drive.google.com/file/d/1pj8QEVNpC1FyJfUabDpV4oU3NpSe0BkD/view?usp=share_link

FILEID=1pj8QEVNpC1FyJfUabDpV4oU3NpSe0BkD
FILENAME=input_surface.npy

# input_upper.npy
https://drive.google.com/file/d/1--7xEBJt79E3oixizr8oFmK_haDE77SS/view?usp=share_link

FILEID=1--7xEBJt79E3oixizr8oFmK_haDE77SS
FILENAME=input_upper.npy


# Code
cd /g/data/wb00/admin/testing/Pangu-Weather/root

wget https://raw.githubusercontent.com/198808xc/Pangu-Weather/main/inference_gpu.py
wget https://raw.githubusercontent.com/198808xc/Pangu-Weather/main/inference_cpu.py
wget https://raw.githubusercontent.com/198808xc/Pangu-Weather/main/inference_iterative.py

```

### Tree
```bash
/g/data/wb00/admin/testing/Pangu-Weather

└── root
    ├── inference_cpu.py
    ├── inference_gpu.py
    ├── inference_iterative.py
    ├── input_data
    │   ├── input_surface.npy
    │   └── input_upper.npy
    ├── output_data
    ├── pangu_weather_1.onnx
    ├── pangu_weather_24.onnx
    ├── pangu_weather_3.onnx
    └── pangu_weather_6.onnx

```

### Install
```bash
cd /scratch/fp0/mah900/
git clone https://github.com/198808xc/Pangu-Weather.git
cd Pangu-Weather/


. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /g/data/wb00/admin/testing/conda/pangu_weather python=3.9

conda activate /g/data/wb00/admin/testing/conda/pangu_weather
module load cuda/11.6.1 cudnn/8.2.2-cuda11.4
cd /scratch/fp0/mah900/Pangu-Weather/
#pip install -r requirement_gpu.txt
pip install -r requirements_gpu.txt 
...
Successfully installed coloredlogs-15.0.1 flatbuffers-23.5.26 humanfriendly-10.0 mpmath-1.3.0 onnx-1.12.0 onnxruntime-gpu-1.14.0 protobuf-3.20.1 sympy-1.12

#Jupyter Lab (Does not launch)
conda install -c conda-forge jupyterlab
python -m pip install -U matplotlib

#Retry with pip (2023.06.19)
conda remove jupyterlab
pip install jupyterlab
...
Installing collected packages: webencodings, mistune, json5, fastjsonschema, websocket-client, webcolors, uri-template, traitlets, tornado, tomli, tinycss2, soupsieve, sniffio, send2trash, rfc3986-validator, rfc3339-validator, requests, pyzmq, pyyaml, python-json-logger, pyrsistent, pygments, pycparser, prometheus-client, platformdirs, pandocfilters, overrides, nest-asyncio, MarkupSafe, jupyterlab-pygments, jsonpointer, importlib-metadata, fqdn, exceptiongroup, defusedxml, debugpy, bleach, babel, attrs, async-lru, terminado, jupyter-core, jsonschema, comm, cffi, beautifulsoup4, arrow, anyio, nbformat, jupyter-server-terminals, jupyter-client, isoduration, argon2-cffi-bindings, nbclient, ipykernel, argon2-cffi, nbconvert, jupyter-events, jupyter-server, notebook-shim, jupyterlab-server, jupyter-lsp, jupyterlab

Successfully installed MarkupSafe-2.1.3 anyio-3.7.0 argon2-cffi-21.3.0 argon2-cffi-bindings-21.2.0 arrow-1.2.3 async-lru-2.0.2 attrs-23.1.0 babel-2.12.1 beautifulsoup4-4.12.2 bleach-6.0.0 cffi-1.15.1 comm-0.1.3 debugpy-1.6.7 defusedxml-0.7.1 exceptiongroup-1.1.1 fastjsonschema-2.17.1 fqdn-1.5.1 importlib-metadata-6.7.0 ipykernel-6.23.2 isoduration-20.11.0 json5-0.9.14 jsonpointer-2.4 jsonschema-4.17.3 jupyter-client-8.2.0 jupyter-core-5.3.1 jupyter-events-0.6.3 jupyter-lsp-2.2.0 jupyter-server-2.6.0 jupyter-server-terminals-0.4.4 jupyterlab-4.0.2 jupyterlab-pygments-0.2.2 jupyterlab-server-2.23.0 mistune-2.0.5 nbclient-0.8.0 nbconvert-7.5.0 nbformat-5.9.0 nest-asyncio-1.5.6 notebook-shim-0.2.3 overrides-7.3.1 pandocfilters-1.5.0 platformdirs-3.6.0 prometheus-client-0.17.0 pycparser-2.21 pygments-2.15.1 pyrsistent-0.19.3 python-json-logger-2.0.7 pyyaml-6.0 pyzmq-25.1.0 requests-2.31.0 rfc3339-validator-0.1.4 rfc3986-validator-0.1.1 send2trash-1.8.2 sniffio-1.3.0 soupsieve-2.4.1 terminado-0.17.1 tinycss2-1.2.1 tomli-2.0.1 tornado-6.3.2 traitlets-5.9.0 uri-template-1.2.0 webcolors-1.13 webencodings-0.5.1 websocket-client-1.6.0

```
Error-1
Jupyterlab is hanging,
Try, remove the Conda Jupiterlab and install with pip. 
Now working.
Seems Conda Jupyter install was the problem. 
Remove conda package: https://www.freecodecamp.org/news/how-to-remove-a-package-in-anaconda/
Install Pip Jupyter: https://jupyter.org/install



### Inference
```bash
qsub -I /home/900/mah900/conda/a1_h4.pbs
...
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /g/data/wb00/admin/testing/conda/pangu_weather
module load cuda/11.6.1 cudnn/8.2.2-cuda11.4

cd /g/data/wb00/admin/testing/Pangu-Weather/root
python inference_gpu.py
mv output_data/ output_data_24

mkdir output_data
python inference_iterative.py
mv output_data/ output_data_7
# Have add two file save lines.

```

### 2023.06.19
Install Xarray
Source: https://docs.xarray.dev/en/stable/getting-started-guide/installing.html
```bash
conda install -c conda-forge xarray dask netCDF4 bottleneck
```

Error-2
```bash
----> 7 import matplotlib.pyplot as plt
...
ModuleNotFoundError: No module named 'importlib_resources'

#Try (Working)
pip install importlib-resources
```
Source: https://blog.finxter.com/fixed-modulenotfounderror-no-module-named-importlib-resources/

Install cartopy (Working)
```
conda install -c conda-forge cartopy
```
Source: https://scitools.org.uk/cartopy/docs/latest/installing.html

Error-3
```bash
ImportError: Using image transforms requires either pykdtree or scipy.

#Try (Working)
python -m pip install scipy
 
```
Source: https://scipy.org/install/


