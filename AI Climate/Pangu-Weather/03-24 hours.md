Need to fill up

Storing Two arrays returned from a function has problem saving the values. Array dimensions are not preserve. So intermediate variables are used. 
`Pangu-04`
```python
for i in range(3):
  output, output_surface = ort_session_24.run(None, {'input':input_3d, 'input_surface':input_surface_3d})  
  input_3d, input_surface_3d = output, output_surface
  a[i] = output
  b[i] = output_surface  
```

### Error 
```python
SyntaxError: invalid syntax (2658875363.py, line 14)
```
Related to function declaration. 
A `:` operator is used out of place. 
That is, Either not used after a function declaration or Used after a function call