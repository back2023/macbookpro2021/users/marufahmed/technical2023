
Plan:
1) Move data to dk92
2) Change data dir in code and 
   Add ground truth return function and 
   build another package and
   test on the env
3) panguweather change 'tmp' folder
4) Rebuild module in the dk92 with the new nci-ai-models
   
5) Add info to opus, make four sub pages for four models
6) Add some video to opus

No write permission to dk92
```
echo $PROJECT 
sudo -u otheruser test -r /g/data/dk92
 
test -r /g/data/dk92; echo "$?"
 
touch -c /g/data/dk92 2>&1 | grep 'Permission denied'
 
```

###### Try copy data (Working)
Move to `/g/data/pp66/data`
```bash
cd /g/data/wb00/admin/testing/ai-models/
mkdir /g/data/pp66/data
cp -r /g/data/wb00/admin/testing/ai-models /g/data/pp66/data
```

```bash
 cd /scratch/fp0/mah900/nci-datasci-env/
 git checkout ai-models/2023.11.14
 git pull --all
 cd /scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14
 cp install.sh update.sh
```

```bash
# nano +166 -c update.sh
echo "Copy ai-model files"
mkdir -p /g/data/dk92/data 
cp -r /g/data/pp66/data/ai-models /g/data/dk92/data 
```

```bash
git add .
git commit -a -m "2022.11.15-1"
git push origin ai-models/2023.11.14
...
https://git.nci.org.au/nci-rei/specialised-env/datasci/nci-datasci-env/-/merge_requests/new?merge_request%5Bsource_branch%5D=ai-models%2F2023.11.14
...
```
File copy worked
```bash
ls /g/data/dk92/data/
ai-models
```

***
### Code change

###### Assets dir path change
`/scratch/fp0/mah900/ai-models/src/nci_ai_models/nci_ai_models_predict.py`: 33
```python
        #self.assets_dir = '/g/data/wb00/admin/testing/ai-models'
        self.assets_dir = '/g/data/dk92/data/ai-models'
        if Path(self.assets_dir).is_dir():
            LOG.info ("Assets dir: " + assets_dir)
        else:    
            sys.exit(f"Assets dir NOT FOUND ({self.assets_dir})")
```

###### Ground truth data
`/scratch/fp0/mah900/ai-models/src/nci_ai_models/nci_ai_models_plot.py` 
```python
    def get_ground_truth_data (self, dir_prefix, var_name, pred_data, levels=None):    
        sel_datetime_list = np.datetime_as_string(pred_data['valid_time'].values )

        if isinstance(sel_datetime_list, (np.str_) ) or len(sel_datetime_list) < 2 :
            sys.exit("There must be at least two datetimes")        

        ground_truth = nci_ai_models_data.data().get_datetime_list_var_data (dir_prefix, sel_datetime_list,  var_name, levels = levels )
        return ground_truth
```
###### Re-Build package
```bash
cd /scratch/fp0/mah900/ai-models/
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/ai-models 
# project.toml
# version = "0.0.8"
# version = "0.0.9"
# version = "0.0.10"
python3 -m build
python3 -m twine upload --repository testpypi dist/*
#https://test.pypi.org/project/nci-ai-models/0.0.8/
#https://test.pypi.org/project/nci-ai-models/0.0.9/
https://test.pypi.org/project/nci-ai-models/0.0.10/

# Test
pip uninstall nci-ai-models
#pip install -i https://test.pypi.org/simple/ nci-ai-models==0.0.8
#pip install -i https://test.pypi.org/simple/ nci-ai-models==0.0.9
pip install -i https://test.pypi.org/simple/ nci-ai-models==0.0.10
```

###### Update files and Install CI/CD 
```bash
cd /scratch/fp0/mah900/nci-datasci-env/
git checkout ai-models/2023.11.14
git pull --all
cd /scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14
```

```bash
# file: /scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14/build-2023.11.09.sh
# 32
#PANGU_TMP_DIR='/g/data/wb00/admin/testing/ai-models/tmp/ai-models-panguweather'
PANGU_TMP_DIR='/tmp/ai-models-panguweather'
# 39
echo "nci_ai_models install ..."
pip install -i https://test.pypi.org/simple/ nci-ai-models==0.0.9

# file: /scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14/update.sh
# 166
#echo "Copy ai-model files"
#mkdir -p /g/data/dk92/data
#cp -r /g/data/pp66/data/ai-models /g/data/dk92/data
```

```bash
mv update.sh ~update.sh
git add .
git commit -a -m "2023.11.15-2"
git push origin ai-models/2023.11.14
...
https://git.nci.org.au/nci-rei/specialised-env/datasci/nci-datasci-env/-/merge_requests/new?merge_request%5Bsource_branch%5D=ai-models%2F2023.11.14
```

###### Update CI/CD
```bash
cd /scratch/fp0/mah900/nci-datasci-env/
git checkout ai-models/2023.11.14
git pull --all
cd /scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14

mv ~update.sh update.sh
# file: ai-models/2023.11.14/build-2023.11.09.sh
# 40 
pip install -i https://test.pypi.org/simple/ nci-ai-models==0.0.10

git add .
git commit -a -m "2023.11.15-3"
git push origin ai-models/2023.11.14
...
 https://git.nci.org.au/nci-rei/specialised-env/datasci/nci-datasci-env/-/merge_requests/new?merge_request%5Bsource_branch%5D=ai-models%2F2023.11.14
```

Note: build does not run during update.
Still the previous version is installed, update does not work changing build code
```
nci-ai-models                 0.0.9
```

Try installing again (Working )
```bash
...
https://git.nci.org.au/nci-rei/specialised-env/datasci/nci-datasci-env/-/merge_requests/new?merge_request%5Bsource_branch%5D=ai-models%2F2023.11.14
...
create modulefile in /g/data/pp66/apps/modulefiles/ai-models/2023.11.14
...
```

Next Try, updating in the dk92
```
...
mv ~update.sh update.sh
# File: /scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14/update.sh
# 3
#APP_ROOT_DIR='/g/data/pp66/apps'
APP_ROOT_DIR='/g/data/dk92/apps'

...
git commit -a -m "2023.11.15-5"
git push origin ai-models/2023.11.14
...
https://git.nci.org.au/nci-rei/specialised-env/datasci/nci-datasci-env/-/merge_requests/new?merge_request%5Bsource_branch%5D=ai-models%2F2023.11.14
...
create module directory  /g/data/dk92/apps/modulefiles/ai-models
create modulefile in  /g/data/dk92/apps/modulefiles/ai-models/2023.11.14
...
```



Test
```
module use /g/data/pp66/apps/modulefiles
module av ai-models
```


# 2023.11.16

Plan:
1) make notebooks 14a,b,c,d copies and upload to nci github
2) Change the get ground truth function name
3) Nove the assets dir checking code to the run function.
4) Re Install CI/CD


10.00
### Change module location in dk92
Update file updata:
`/scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14/update.sh`
```bash
#MODULE_ROOT_DIR=${APP_ROOT_DIR}/modulefiles
MODULE_ROOT_DIR=/g/data/dk92/apps/Modules/modulefiles
```

Update - CI/CD
```bash
cd /scratch/fp0/mah900/nci-datasci-env/
git checkout ai-models/2023.11.14
git pull --all
cd /scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14

git add .
git commit -a -m "2023.11.16-1"
git push origin ai-models/2023.11.14
...
https://git.nci.org.au/nci-rei/specialised-env/datasci/nci-datasci-env/-/merge_requests/new?merge_request%5Bsource_branch%5D=ai-models%2F2023.11.14
...
create_module_file
create module directory  /g/data/dk92/apps/Modules/modulefiles/ai-models
create modulefile in  /g/data/dk92/apps/Modules/modulefiles/ai-models/2023.11.14
```

Check apps and module location
```
ls -lh /g/data/dk92/apps/
drwxrws---+ 3 wp25_ci dk92   4.0K Nov 16 03:35 ai-models
...
ls -lh /g/data/dk92/apps/Modules/modulefiles/
drwxrws---+ 2 wp25_ci dk92   4.0K Nov 16 10:25 ai-models
...
```


###### Notebooks to github
Copy
```bash
cd /scratch/fp0/mah900/ai-models/
mkdir -p nci_ai_models
cp notebooks/AI-Models-14-a.ipynb nci_ai_models/AI-Models-fourcastnet.ipynb
cp notebooks/AI-Models-14-b.ipynb nci_ai_models/AI-Models-fourcastnet_v2.ipynb
cp notebooks/AI-Models-14-c.ipynb nci_ai_models/AI-Models-graphcast.ipynb
cp notebooks/AI-Models-14-d.ipynb nci_ai_models/AI-Models-panguweather.ipynb
```
Git init
```bash
/scratch/fp0/mah900/ai-models/nci_ai_models
git init -b main
git add .
git commit -a -m "1st commit-2023.11.16"

git remote add origin https://github.com/maruf-anu/NCI-AI-Models.git
git remote show origin
#git pull origin main
#Does not work
wget https://raw.githubusercontent.com/maruf-anu/NCI-AI-Models/main/LICENSE
wget https://raw.githubusercontent.com/maruf-anu/NCI-AI-Models/main/README.md
git add .
git commit -a -m "2023.11.16-2"
# remote: Permission to maruf-anu/NCI-AI-Models.git denied to mahm1846.
```
Source: https://gist.github.com/c0ldlimit/4089101

Does not work
```bash
#nano .git/config
        #url = https://github.com/maruf-anu/NCI-AI-Models.git
        url = ssh://git@github.com/maruf-anu/NCI-AI-Models.git
git push origin main        
```
Does not work
```
git remote set-url origin https://yourusername@github.com/user/repo.git
https://youruser:password@github.com/user/repo.git

```
Source: https://stackoverflow.com/a/11368701
Error
```bash
error: failed to push some refs to 'https://github.com/github.com/maruf-anu/NCI-AI-Models.git'
```
Try
```
git pull --rebase origin main
git push origin main
```
Error
```bash
remote: Not Found
fatal: repository 'https://github.com/github.com/maruf-anu/NCI-AI-Models.git/' not found
				   https://github.com/maruf-anu/NCI-AI-Models.git
```
Try
```
 git remote -v
origin https://maruf-anu: ... @github.com/github.com/maruf-anu/NCI-AI-Models.git (fetch)
origin https://maruf-anu: ... @github.com/github.com/maruf-anu/NCI-AI-Models.git (push)

git remote rm origin 
git remote add origin https://maruf-anu: ... @github.com/maruf-anu/NCI-AI-Models.git 

git pull --rebase origin main
...
Successfully rebased and updated refs/heads/main.

git push origin main
remote: Support for password authentication was removed on August 13, 2021.
...
```
Source: https://stackoverflow.com/a/24114760

Try
```
git remote rm origin 
git remote add origin https://maruf-anu@github.com/maruf-anu/NCI-AI-Models.git 

```
Try 
From your GitHub account, 
go to **Settings** → **Developer Settings** → **Personal Access Token** → **Generate New Token** (Give your password) → **Fillup the form** → click **Generate token** → **Copy the generated Token**
(Worked)
```
git push origin main
Password for 'https://maruf-anu@github.com': <token>
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
...
```
Source: https://stackoverflow.com/a/68781050


###### Change code

`/scratch/fp0/mah900/ai-models/src/nci_ai_models/nci_ai_models_predict.py`: 38
```python
    def run(self, dir_prefix, ai_model_name, year, month, day, hour, lead_time, assets_dir = None): 
    
        if assets_dir != None and assets_dir != "":
            self.assets_dir = assets_dir

        if Path(self.assets_dir).is_dir():
            LOG.info ("Assets dir: " + self.assets_dir)
        else:    
            sys.exit(f"Assets dir NOT FOUND ({self.assets_dir})")
```

`/scratch/fp0/mah900/ai-models/src/nci_ai_models/nci_ai_models_predict.py`: 123
```python
    #def run(self, ai_model_name, in_file, pred_dir, lead_time): 
    def run_date_time(self, dir_prefix, ai_model_name, date_time_lead , assets_dir = None): 
           
        year      = int (date_time_lead.children[0].value.strftime("%Y %m %d").split() [0] )
        month     = int (date_time_lead.children[0].value.strftime("%Y %m %d").split() [1] )
        day       = int (date_time_lead.children[0].value.strftime("%Y %m %d").split() [2] )
        hour      = int (date_time_lead.children[1].value [:2] )
        lead_time = int (date_time_lead.children[2].value )        
        
        return self.run(dir_prefix, ai_model_name, year, month, day, hour, lead_time)
```



`/scratch/fp0/mah900/ai-models/src/nci_ai_models/nci_ai_models_plot.py`: 540
```python
    def get_nci_era5_by_date (self, dir_prefix, var_name, pred_data, levels=None):    
        sel_datetime_list = np.datetime_as_string(pred_data['valid_time'].values )

        #if isinstance(sel_datetime_list, (np.str_) ) or len(sel_datetime_list) < 2 :
        #    sys.exit("There must be at least two datetimes")        

        ground_truth = nci_ai_models_data.data().get_datetime_list_var_data (dir_prefix, sel_datetime_list,  var_name, levels = levels )
        return ground_truth
```
`/scratch/fp0/mah900/ai-models/src/nci_ai_models/nci_ai_models_plot.py`: 785
```python
    #def get_nci_era5_by_date (self, dir_prefix, var_name, pred_data, levels=None): 
    def get_nci_era5 (self, dir_prefix, pl_name_levels, pred_data):   

        var_name = str(pl_name_levels.children[0].value)
        try:
            levels = list(pl_name_levels.children[1].value)
        except:  
            levels = None  
        return self.get_nci_era5_by_date (dir_prefix, var_name, pred_data, levels)
```

Build package
```bash
# version = "0.0.11"
...
Successfully built nci_ai_models-0.0.11.tar.gz and nci_ai_models-0.0.11-py3-none-any.whl
...
https://test.pypi.org/project/nci-ai-models/0.0.11/
...
pip uninstall nci-ai-models
... 
pip install -i https://test.pypi.org/simple/ nci-ai-models==0.0.11
```
Fix error, and build again (Working)
```
version = "0.0.12"
...
https://test.pypi.org/project/nci-ai-models/0.0.12/
...
pip install -i https://test.pypi.org/simple/ nci-ai-models==0.0.12
```

Test with ARE
```bash
/scratch/fp0/mah900/Miniconda2023/
/scratch/fp0/mah900/env/ai-models
```

CI/CD
Update
```bash
# file: /scratch/fp0/mah900/nci-datasci-env/ai-models/2023.11.14/build-2023.11.09.sh
# 39 (Update nci-ai-models version)
echo "nci_ai_models install ..."
pip install -i https://test.pypi.org/simple/ nci-ai-models==0.0.12
```
Install
```bash
...
mv update.sh ~update.sh
git add .
git commit -a -m "2023.11.17-1"
git push origin ai-models/2023.11.14
...
https://git.nci.org.au/nci-rei/specialised-env/datasci/nci-datasci-env/-/merge_requests/new?merge_request%5Bsource_branch%5D=ai-models%2F2023.11.14
...
create modulefile in /g/data/pp66/apps/modulefiles/ai-models/2023.11.14
```
Update
```bash
...
mv ~update.sh update.sh
...
https://git.nci.org.au/nci-rei/specialised-env/datasci/nci-datasci-env/-/merge_requests/new?merge_request%5Bsource_branch%5D=ai-models%2F2023.11.14
...
create modulefile in /g/data/dk92/apps/Modules/modulefiles/ai-models/2023.11.14
```

End for today
***
