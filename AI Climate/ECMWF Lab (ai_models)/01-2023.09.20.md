
### Start page
https://github.com/ecmwf-lab/ai-models

### Installation
- A 10-day forecast can take several hours on a CPU but only around one minute on a modern GPU.
- install `ai-models` in a [conda](https://docs.conda.io/en/latest/) environment and 
	- install the CUDA libraries in that environment 


### Running the models
- To run model, make sure it has been installed: `ai-models <model-name>`
- By default, the model will be run for a 10-day lead time (240 hours), using yesterday's 12Z analysis from ECMWF's MARS archive.
- To produce a 15 days forecast, use the `--lead-time HOURS` option.
- You can change the other defaults using the available command line options, as described below.
 
#### Assets
- The AI models rely on weights and other assets created during training. 
	- The first time you run a model, you will need to download the trained weights and any additional required assets.
	- You can provide a different directory to store the assets
	- For better organisation of the assets directory, you can use the `--assets-sub-directory` option. This option will store the assets of each model in its own subdirectory within the specified assets directory.

### Input data

- The models require input data (initial conditions) to run. 
	- You can provide the input data using different sources
- From MARS
	- By default, `ai-models` use yesterday's 12Z analysis from ECMWF, fetched from the Centre's MARS archive using the [ECMWF WebAPI](https://www.ecmwf.int/en/computing/software/ecmwf-web-api). 
	- You will need an ECMWF account to access that service.
- From the CDS
	- You can start the models using ERA5 (ECMWF Reanalysis version 5) data for the [Copernicus Climate Data Store (CDS)](https://cds.climate.copernicus.eu/). 
	- You will need to create an account on the CDS. 
	- The data will be downloaded using the [CDS API](https://cds.climate.copernicus.eu/api-how-to).
	- To access the CDS, simply add `--input cds` on the command line.
	- Please note that ERA5 data is added to the CDS with a delay, so you will also have to provide a date with `--date YYYYMMDD`.
- From a GRIB file
	- If you have input data in the GRIB format, you can provide the file using the `--file` option
	- The GRIB file can contain more fields than the ones required by the model. 
		- The `ai-models` command will automatically select the necessary fields from the file.
- To find out the list of fields needed by a specific model as initial conditions, use the following command: `ai-models --fields <model-name>`

### Output
- By default, the model output will be written in GRIB format in a file called `<model-name>.grib`. 
	- You can change the file name with the option `--path <file-name>`. 
	- If the path you specify contains placeholders between `{` and `}`, multiple files will be created based on the [eccodes](https://confluence.ecmwf.int/display/ECC) keys.


### Command line options
It has the following options:
- `--help`: Displays this help message.
- `--models`: Lists all installed models.
- `--debug`: Turns on debug mode. This will print additional information to the console.



### 2023.09.20
### Install, Plan 1
Follow the steps on the page: https://github.com/ecmwf-lab/ai-models/tree/main

```bash
cd /scratch/fp0/mah900/
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/ai-models python=3.10
conda activate /scratch/fp0/mah900/env/ai-models
conda install cudatoolkit
pip install ai-models

...
Successfully installed MarkupSafe-2.1.3 ai-models-0.2.5 attrs-23.1.0 branca-0.6.0 cdsapi-0.6.1 certifi-2023.7.22 cffi-1.15.1 cfgrib-0.9.10.4 cftime-1.6.2 charset-normalizer-3.2.0 click-8.1.7 climetlab-0.17.2 cloudpickle-2.2.1 dask-2023.9.2 eccodes-1.6.0 ecmwf-api-client-1.6.3 ecmwf-opendata-0.2.0 ecmwflibs-0.5.3 entrypoints-0.4 filelock-3.12.4 findlibs-0.0.5 fsspec-2023.9.1 gputil-1.4.0 idna-3.4 imageio-2.31.3 importlib-metadata-6.8.0 jinja2-3.1.2 locket-1.0.0 magics-1.5.8 markdown-3.4.4 multiurl-0.2.1 netcdf4-1.6.4 numpngw-0.1.2 numpy-1.26.0 packaging-23.1 pandas-2.1.0 partd-1.4.0 pdbufr-0.11.0 pillow-10.0.1 pycparser-2.21 pyodc-1.3.0 python-dateutil-2.8.2 pytz-2023.3.post1 pyyaml-6.0.1 requests-2.31.0 six-1.16.0 termcolor-2.3.0 toolz-0.12.0 tqdm-4.66.1 tzdata-2023.3 urllib3-2.0.5 xarray-2023.8.0 zipp-3.17.0

```

```bash
pip install ai-models-fourcastnetv2
...
Successfully installed ai-models-fourcastnetv2-0.0.1 cmake-3.27.5 lit-16.0.6 mpmath-1.3.0 networkx-3.1 nvidia-cublas-cu11-11.10.3.66 nvidia-cuda-cupti-cu11-11.7.101 nvidia-cuda-nvrtc-cu11-11.7.99 nvidia-cuda-runtime-cu11-11.7.99 nvidia-cudnn-cu11-8.5.0.96 nvidia-cufft-cu11-10.9.0.58 nvidia-curand-cu11-10.2.10.91 nvidia-cusolver-cu11-11.4.0.1 nvidia-cusparse-cu11-11.7.4.91 nvidia-nccl-cu11-2.14.3 nvidia-nvtx-cu11-11.7.91 ruamel.yaml-0.17.32 ruamel.yaml.clib-0.2.7 sympy-1.12 torch-2.0.1 torch_harmonics-0.6.2 triton-2.0.0 typing-extensions-4.8.0
```


### Next try, assets 
```bash
ai-models --download-assets --assets-sub-directory --assets /g/data/wb00/admin/testing/ai-models ai-models-fourcastnetv2
...
ai-models: error: argument MODEL: invalid choice: 'ai-models-fourcastnetv2' (choose from 'fourcastnetv2-small')

# Try again
$ ai-models --download-assets --assets-sub-directory --assets /g/data/wb00/admin/testing/ai-models fourcastnetv2-small

2023-09-20 22:30:27,060 INFO Writing results to fourcastnetv2-small.grib.
2023-09-20 22:30:27,064 INFO Downloading /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/weights.tar
2023-09-20 22:30:27,064 INFO Downloading https://get.ecmwf.int/repository/test-data/ai-models/fourcastnetv2/small/weights.tar
2023-09-20 22:40:21,485 INFO Downloading /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/global_means.npy
2023-09-20 22:40:21,487 INFO Downloading https://get.ecmwf.int/repository/test-data/ai-models/fourcastnetv2/small/global_means.npy
2023-09-20 22:40:23,648 INFO Downloading /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/global_stds.npy
2023-09-20 22:40:23,648 INFO Downloading https://get.ecmwf.int/repository/test-data/ai-models/fourcastnetv2/small/global_stds.npy
2023-09-20 22:40:25,505 INFO Loading /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/global_means.npy                                
2023-09-20 22:40:25,562 INFO Loading /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/global_stds.npy
2023-09-20 22:40:25,564 INFO Loading surface fields from MARS

An API key is needed to access this dataset. Please visit
https://www.ecmwf.int/user/login/sso to register or sign-in
then visit https://api.ecmwf.int/v1/key/ to retrieve you API key.

API url:

```



Login and 
Visited: `https://api.ecmwf.int/v1/key/` 
**** 

|   |   |
|---|---|
|Your registered email:|maruf.ahmed@anu.edu.au|

|   |   |
|---|---|
|Your API key:|72043cf773a88a730862bda0a87b4f8a (valid until Sept. 19, 2024, 12:44 p.m.)|

|   |   |
|---|---|
|Content of $HOME/.ecmwfapirc|{<br>    "url"   : "https://api.ecmwf.int/v1",<br>    "key"   : "72043cf773a88a730862bda0a87b4f8a",<br>    "email" : "maruf.ahmed@anu.edu.au"<br>}|

```bash
# Try again
ai-models --download-assets --assets-sub-directory --assets /g/data/wb00/admin/testing/ai-models fourcastnetv2-small

...
ecmwfapi.api.APIException: "ecmwf.API error 1: User 'maruf.ahmed@anu.edu.au' has no access to services/mars"
```
Source: https://confluence.ecmwf.int/display/UDOC/ecmwf.API+error+1%3A+User+has+no+access+to+services+mars+-+Web+API+FAQ
- Your username is not authorised to use the [Access MARS](https://confluence.ecmwf.int/display/WEBAPI/Access+MARS) service.

### Next try, Input
```bash
ai-models --assets /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/ --input cds --date 20220101 --time 0000 --lead-time 6 fourcastnetv2-small

2023-09-20 23:08:53,324 INFO Writing results to fourcastnetv2-small.grib.
2023-09-20 23:08:53,324 INFO Loading /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/global_means.npy
2023-09-20 23:08:53,341 INFO Loading /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/global_stds.npy
2023-09-20 23:08:53,357 INFO Loading surface fields from CDS
...
2023-09-20 23:08:57,768 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-single-levels
2023-09-20 23:08:57,768 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-single-levels
...
2023-09-20 23:09:20,772 INFO Downloading https://download-0021.copernicus-climate.eu/cache-compute-0021/cache/data5/adaptor.mars.internal-1695215352.0830624-26679-4-4f0bf2cb-ba1c-4684-84de-bc6e7a40735d.grib to /g/data/wb00/admin/staging/climetlab-mah900/cds-retriever-5c16014596cdd1d29eccf03ac73a98f3e5d4aa70a597e61b7cf71b12e096f08e.cache.tmp (15.8M)

2023-09-20 23:09:20,772 INFO Downloading https://download-0021.copernicus-climate.eu/cache-compute-0021/cache/data5/adaptor.mars.internal-1695215352.0830624-26679-4-4f0bf2cb-ba1c-4684-84de-bc6e7a40735d.grib to /g/data/wb00/admin/staging/climetlab-mah900/cds-retriever-5c16014596cdd1d29eccf03ac73a98f3e5d4aa70a597e61b7cf71b12e096f08e.cache.tmp (15.8M)
...
2023-09-20 23:09:44,513 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-pressure-levels
2023-09-20 23:09:44,513 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-pressure-levels
...
2023-09-20 23:10:19,164 INFO Downloading https://download-0002-clone.copernicus-climate.eu/cache-compute-0002/cache/data4/adaptor.mars.internal-1695215413.9155552-4000-2-fa7ca264-bf21-499c-9f94-ac5380a03346.grib to /g/data/wb00/admin/staging/climetlab-mah900/cds-retriever-38a2bdb458c228574c9ed9413e84e0fe826fca84a846c73a30a588ae4fcee97f.cache.tmp (128.7M)

2023-09-20 23:10:19,164 INFO Downloading https://download-0002-clone.copernicus-climate.eu/cache-compute-0002/cache/data4/adaptor.mars.internal-1695215413.9155552-4000-2-fa7ca264-bf21-499c-9f94-ac5380a03346.grib to /g/data/wb00/admin/staging/climetlab-mah900/cds-retriever-38a2bdb458c228574c9ed9413e84e0fe826fca84a846c73a30a588ae4fcee97f.cache.tmp (128.7M)
...
2023-09-20 23:10:52,403 INFO Using device 'CPU'. The speed of inference depends greatly on the device.
2023-09-20 23:11:43,095 INFO Model initialisation: 2 minutes 49 seconds
2023-09-20 23:11:43,100 INFO Starting inference for 1 steps (6h).
2023-09-20 23:12:34,573 INFO Done 1 out of 1 in 51 seconds (6h), ETA: 51 seconds.
2023-09-20 23:12:34,581 INFO Elapsed: 51 seconds.
2023-09-20 23:12:34,583 INFO Average: 51 seconds per step.
2023-09-20 23:12:34,702 INFO Total time: 3 minutes 44 seconds.

ls -lth  fourcastnetv2-small.grib
-rw-r--r-- 1 mah900 z00 145M Sep 20 23:12 fourcastnetv2-small.grib
```

### Next try, visualisation

```bash
# Try Xarray and Grib
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/ai-models
python
>>> import xarray as xr
>>> import cfgrib
#>>> ds = xr.load_dataset("/home/900/mah900/fourcastnetv2-small.grib", engine="cfgrib")
>>> grib_data = cfgrib.open_datasets("/home/900/mah900/fourcastnetv2-small.grib")
#>>> ds = xr.load_dataset("/home/900/mah900/fourcastnetv2-small.grib", engine="cfgrib", backend_kwargs={'indexpath': ''})
>>> ds = xr.load_dataset("/home/900/mah900/fourcastnetv2-small.grib", engine="pynio")
# Worked
>>> ds = xr.load_dataset("/home/900/mah900/fourcastnetv2-small.grib", engine="cfgrib",backend_kwargs={'filter_by_keys':{'typeOfLevel': 'heightAboveGround','level':2}})

```
Source: https://github.com/ecmwf/cfgrib/issues/263#issuecomment-1540838485


# 2023.09.21

### Change CDS cache dir

```bash
nano -c /home/900/mah900/.climetlab/settings.yaml
# Line 10
#cache-directory: /g/data/wb00/admin/staging/climetlab-mah900
cache-directory: /tmp/climetlab-mah900

# Remote cache 
rm -r /g/data/wb00/admin/staging/climetlab-mah900

```

### Install JupyterLab
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/ai-models

conda install -c conda-forge jupyterlab
```
Source: https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html

### Try ARE
```

Walltime (hours): 1
Queue: gpuvolta
Compute Size: 1gpu
Project: fp0
Storage: gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00+gdata/rt52+gdata/hh5+scratch/fp0+gdata/rr3+scratch/vp91

Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda2023
Conda environment: /scratch/fp0/mah900/env/ai-models

```

### ARE Test

##### Error-1
Relative import not found
```python
ImportError: attempted relative import with no known parent package
```
An alternative is to avoid using relative imports, and just use...
```python
#Change
from .mymodule import myfunction
# to
from mypackage.mymodule import as_int
```
Source: https://stackoverflow.com/questions/16981921/relative-imports-in-python-3 


##### Error-2
```bash
/scratch/fp0/mah900/env/ai-models/lib/python3.10/site-packages/tqdm/auto.py:21: TqdmWarning: IProgress not found. Please update jupyter and ipywidgets. See [https://ipywidgets.readthedocs.io/en/stable/user_install.html](https://ipywidgets.readthedocs.io/en/stable/user_install.html)
  from .autonotebook import tqdm as notebook_tqdm
```
Source: https://github.com/CosmiQ/solaris/issues/392#issuecomment-759238485

`/scratch/fp0/mah900/env/ai-models/lib/python3.10/site-packages/tqdm/auto.py` : 21
```bash
# solved this by changing (Working)
from .autonotebook import tqdm as notebook_tqdm
# to
from tqdm import tqdm as notebook_tqdm
```

##### Error-3
```bash
ModuleNotFoundError: No module named 'ipywidgets'
```
Source: 
Try 
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/ai-models

pip install ipywidgets
...
Successfully installed ipywidgets-8.1.1 jupyterlab-widgets-3.0.9 widgetsnbextension-4.0.9
```
Source: https://stackoverflow.com/questions/34364681/no-module-named-ipywidgets-error-when-running-ipyhon-widgets

Test progress bar
```python
from ipywidgets import IntProgress
from IPython.display import display

progress_bar = IntProgress(min=0, max=100)
display(progress_bar)

for i in range(100):
    progress_bar.value = i
```
Source: https://saturncloud.io/blog/importerror-iprogress-not-found-please-update-jupyter-and-ipywidgets-although-it-is-installed/#step-5-test-the-iprogress-widget


### Next, Try assets
```bash
# Below does not work 
# ai-models --download-assets --assets-sub-directory --assets /g/data/wb00/admin/testing/ai-models fourcastnetv2-latest
!ai-models --download-assets --assets-sub-directory --assets /g/data/wb00/admin/testing/ai-models fourcastnetv2-small
```

### Next, Run
```bash
! ai-models --assets /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/ --input cds --date 20220101 --time 0000 --lead-time 6 fourcastnetv2-small
```

### Visualization
```python
import xarray as xr

ds = xr.load_dataset("fourcastnetv2-small.grib", engine="cfgrib",backend_kwargs={'filter_by_keys':{'typeOfLevel': 'heightAboveGround','level':2}})
```

### Install matplotlib, cartopy
```bash
# Does not work from within JupyterLab
$ echo $CONDA_PREFIX
/scratch/fp0/mah900/env/ai-models
pip install matplotlib
...
# Error
# ModuleNotFoundError: No module named 'matplotlib'
```
Next, try from cmd (Working)
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/ai-models
pip install matplotlib
pip install cartopy
```

Jupyter file can not be saved

`/scratch/fp0/mah900/ai-models/AI-Models-01.ipynb`

```python
# ai-models
import datetime
import json
import logging
import os
import sys
import time
from collections import defaultdict
from functools import cached_property

import entrypoints
from climetlab.utils.humanize import seconds
from multiurl import download

from ai_models.checkpoint import peek
from ai_models.inputs import get_input
from ai_models.outputs import get_output
from ai_models.stepper import Stepper

import logging
from functools import cached_property
import climetlab as cml

import argparse
import logging
import os
import shlex
import sys
from ai_models.inputs import available_inputs
from ai_models.model import Timer, available_models, load_model
from ai_models.outputs import available_outputs

import os
import pickle
import zipfile
from typing import Any

import datetime
import json
import os
import sys
import sysconfig

import logging
import time
from climetlab.utils.humanize import seconds

```


```python
#ai-models-fourcastnetv2

from ai_models_fourcastnetv2.fourcastnetv2.sfnonet import FourierNeuralOperatorNet

import torch
from torch import nn

from functools import partial
from collections import OrderedDict
from copy import Error, deepcopy
from re import S
from numpy.lib.arraypad import pad
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.fft
from torch.nn.modules.container import Sequential
from torch.utils.checkpoint import checkpoint, checkpoint_sequential
from torch.cuda import amp
from typing import Optional
import math

from ai_models_fourcastnetv2.fourcastnetv2.contractions import *
from ai_models_fourcastnetv2.fourcastnetv2.activations import *


from functools import partial
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.checkpoint import checkpoint

# from apex.normalization import FusedLayerNorm

from torch.utils.checkpoint import checkpoint

# helpers
from ai_models_fourcastnetv2.fourcastnetv2.layers import (
    trunc_normal_,
    DropPath,
    MLP,
)
from ai_models_fourcastnetv2.fourcastnetv2.layers import (
    SpectralAttentionS2,
    SpectralConvS2,
)
from ai_models_fourcastnetv2.fourcastnetv2.layers import (
    SpectralAttention2d,
    SpectralConv2d,
)

import torch_harmonics as harmonics

# to fake the sht module with ffts
from ai_models_fourcastnetv2.fourcastnetv2.layers import RealFFT2, InverseRealFFT2

from ai_models_fourcastnetv2.fourcastnetv2.contractions import *

# from .fourcastnetv2 import activations
from ai_models_fourcastnetv2.fourcastnetv2.activations import *

import logging
import os

import numpy as np
import torch
from ai_models.model import Model
import datetime

import ai_models_fourcastnetv2.fourcastnetv2 as nvs

```



```python
!ai-models --fields fourcastnetv2-small

!ai-models --download-assets --assets-sub-directory --assets /g/data/wb00/admin/testing/ai-models fourcastnetv2-s
!ai-models --download-assets --assets-sub-directory --assets /g/data/wb00/admin/testing/ai-models fourcastnetv2-latest

! ai-models --assets /g/data/wb00/admin/testing/ai-models/fourcastnetv2-small/ --input cds --date 20220101 --time 0000 --lead-time 60 fourcastnetv2-small

! ls -htl fourcastnetv2-small.grib

```

```python
import xarray as xr
import matplotlib.pyplot as plt
ds = xr.load_dataset("fourcastnetv2-small.grib",engine = 'cfgrib')
 
#ds = xr.load_dataset("fourcastnetv2-small.grib", engine="cfgrib",backend_kwargs={'filter_by_keys':{'typeOfLevel': 'heightAboveGround','level':2}})
ds 
```


```python
from matplotlib import animation
from ipywidgets import IntProgress
from IPython.display import display
import time

field = "t"
progress_bar = IntProgress(min=0, max= len(ds.to_array()[0]) - 1 )
columns = 2
rows    = 1

fig, axes = plt.subplots(nrows=rows, ncols=columns, figsize=(columns*9, rows*5))

for idx, ax in enumerate(axes.flat):
    ax.set_xticks([])
    ax.set_yticks([])  

fig.add_subplot(rows, columns, 1)
im1 = None
#im1 = plt.imshow( ds.to_array()[0][0], aspect='auto',  cmap='Spectral_r' )
fig.add_subplot(rows, columns, 2)
#im2 = plt.imshow( ds.to_array()[0][0], aspect='auto',  cmap='Spectral_r' )
im2 = plt.imshow( ds[field][0], aspect='auto',  cmap='Spectral_r' )
axes[0].set_title("Ground Truth")
axes[1].set_title("Prediction  ")

plt.subplots_adjust(hspace=0.01)      
fig.tight_layout()  

def init():
    pass

i = 0
def animate(i):
    axes[0].set_title("Ground Truth")
    axes[1].set_title("Prediction  ")
    #im1.set_data( ds.to_array()[0][i] )
    #im2.set_data( ds.to_array()[0][i]  )
    im2.set_data( ds[field][i]  )
    progress_bar.value = i
    return im1, im2    

anim = animation.FuncAnimation(fig, animate, init_func=init, frames=len(ds.to_array()[0]), interval=400)    

```

```python
from IPython.display import HTML
display(progress_bar)
HTML(anim.to_jshtml())
```
