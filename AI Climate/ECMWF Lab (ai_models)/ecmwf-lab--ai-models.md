
https://github.com/ecmwf-lab/ai-models


# ai-models

The `ai-models` command is used to run AI-based weather forecasting models. These models need to be installed independently.

## [](https://github.com/ecmwf-lab/ai-models#prerequisites)Prerequisites

Before using the `ai-models` command, ensure you have the following prerequisites:

- Python 3.10 (it may work with different versions, but it has not been tested with 3.10).
- An ECMWF and/or CDS account for accessing input data (see below for more details).
- A computed with a GPU for optimal performance (strongly recommended).


See [ai-models-panguweather](https://github.com/ecmwf-lab/ai-models-panguweather), [ai-models-fourcastnet](https://github.com/ecmwf-lab/ai-models-fourcastnet), [ai-models-fourcastnetv2](https://github.com/ecmwf-lab/ai-models-fourcastnetv2) and [ai-models-graphcast](https://github.com/ecmwf-lab/ai-models-graphcast) for more details about these models.


