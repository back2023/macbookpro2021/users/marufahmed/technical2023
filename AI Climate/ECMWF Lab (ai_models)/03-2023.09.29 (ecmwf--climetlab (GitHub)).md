
# ecmwf/climetlab (GitHub)

## climetlab/readers/grib/codes.py

#### Line 32: def get_messages_positions(path):

An iterator function, return fields one by one 

```python
path = '/scratch/fp0/mah900/ai-models/test_template_3.grib'
for f in climetlab.readers.grib.codes.get_messages_positions(path):
    print (f)

(0, 2076588)
(2076588, 2076588)
(4153176, 2076588)
(6229764, 2076588)
(8306352, 2076588)
(10382940, 2076588)
...
```

### Line 105: class CodesHandle:

```python

class CodesHandle:

    def __init__(self, handle, path, offset):
        self.handle = handle
        self.path = path
        self.offset = offset


	def set_values(self, values):
	        assert self.path is None, "Only cloned handles can have values changed"
	        eccodes.codes_set_values(self.handle, values.flatten())
	        # This is writing on the GRIB that something has been modified (255=unknown)
	        eccodes.codes_set_long(self.handle, "generatingProcessIdentifier", 255)
	
	def set_multiple(self, values):
	        assert self.path is None, "Only cloned handles can have values changed"
	        eccodes.codes_set_key_vals(self.handle, values)

	def set_long(self, name, value):
	        try:
	            assert self.path is None, "Only cloned handles can have values changed"
	            eccodes.codes_set_long(self.handle, name, value)
	        except Exception as e:
	            LOG.error("Error setting %s=%s", name, value)
	            LOG.exception(e)

    def write(self, f):
        eccodes.codes_write(self.handle, f)




def write(self, f):
        eccodes.codes_write(self.handle, f)
    
```



### Line 263: class ReaderLRUCache(dict):

	def __init__(self, size)


### Line 292: class CodesReader:

	def __init__(self, path):


### Line 321: class GribField(Base):

```python

class GribField(Base):
    def __init__(self, path, offset, length):
        self.path = path
        self._offset = offset
        self._length = length
        self._handle = None
        self._values = None
        self._cache = {}
```  

Returns the handle for the GribField
```python
# Code, line: 330
    @property
    def handle(self):
        if self._handle is None:
            assert self._offset is not None
            self._handle = CodesReader.from_cache(self.path).at_offset(self._offset)
        return self._handle
        
# Example
ds = climetlab.load_source( "file", '/scratch/fp0/mah900/ai-models/test_template_3.grib')
template = ds [0]
template.handle   

<climetlab.readers.grib.codes.CodesHandle at 0x1517abc26da0>
```
***

```python
# Code, Line: 337
    @property
    def values(self):
        if self._values is None:
            self._values = self.handle.get("values")
        return self._values

# Example
template.values 
#or
template.handle.get("values") 

array([1000.        , 1000.16699219, 1000.33300781, ...,  991.84375   ,
        991.68359375,  991.52246094])
```
***

```python
# Code, line: 343
    @property
    def offset(self):
        if self._offset is None:
            self._offset = int(self.handle.get("offset"))
        return self._offset

# Example
template.offset 

0
```
***
```python
# Code, line: 349
    @property
    def shape(self):
        Nj = missing_is_none(self.handle.get("Nj"))
        Ni = missing_is_none(self.handle.get("Ni"))
        if Ni is None or Nj is None:
            n = self.handle.get("numberOfDataPoints")
            return (n,)  # shape must be a tuple
        return (Nj, Ni)

# Example
template.shape

(721, 1440)
```
***
 
```python
# Code, line: 358
    def plot_map(self, backend):
        backend.bounding_box(
            north=self.handle.get("latitudeOfFirstGridPointInDegrees"),
            south=self.handle.get("latitudeOfLastGridPointInDegrees"),
            west=self.handle.get("longitudeOfFirstGridPointInDegrees"),
            east=self.handle.get("longitudeOfLastGridPointInDegrees"),
        )
        backend.plot_grib(self.path, self.handle.get("offset"))

# Example
template.plot_map(matplotlib.pyplot) 
...
AttributeError: module 'matplotlib.pyplot' has no attribute 'bounding_box'
```
***


```python
# Code, line: 367
    @call_counter
    def to_numpy(self, reshape=True, dtype=None):
        values = self.values
        if reshape:
            values = values.reshape(self.shape)
        if dtype is not None:
            values = values.astype(dtype)
        return values

# Example
template.to_numpy()

array([[1000.        , 1000.16699219, 1000.33300781, ...,  996.16210938,
         996.00195312,  995.84179688],
... 
...	   
```

***
```python
# Code, line: 376
    def __repr__(self):
        return "GribField(%s,%s,%s,%s,%s,%s)" % (
            self.handle.get("shortName"),
            self.handle.get("levelist"),
            self.handle.get("date"),
            self.handle.get("time"),
            self.handle.get("step"),
            self.handle.get("number"),
        )

# Example
template

GribField(10u,None,20220101,0,0,0)
```
***
```python
template.handle

<climetlab.readers.grib.codes.CodesHandle at 0x1517ab9cfeb0>
```
***
```python
# Code, line: 386
    def _grid_definition(self):
        return dict(
            north=self.handle.get("latitudeOfFirstGridPointInDegrees"),
            south=self.handle.get("latitudeOfLastGridPointInDegrees"),
            west=self.handle.get("longitudeOfFirstGridPointInDegrees"),
            east=self.handle.get("longitudeOfLastGridPointInDegrees"),
            south_north_increment=self.handle.get("jDirectionIncrementInDegrees"),
            west_east_increment=self.handle.get("iDirectionIncrementInDegrees"),
        )

# Example
template._grid_definition()

{'north': 90.0,
 'south': -90.0,
 'west': 0.0,
 'east': 359.75,
 'south_north_increment': 0.25,
 'west_east_increment': 0.25}
```
***

```python
# Code, line: 396
    def field_metadata(self):
        m = self._grid_definition()

        for n in (
            "shortName",
            "units",
            "paramId",
            "level",
            "typeOfLevel",
            "marsClass",
            "marsStream",
            "marsType",
            "number",
            "stepRange",
            "param",
            "long_name",
            "standard_name",
            "levelist",
        ):
            p = self.handle.get(n)
            if p is not None:
                m[n] = str(p)
        m["shape"] = self.shape
        return m

# Example
template.field_metadata()

{'north': 90.0,
 'south': -90.0,
 'west': 0.0,
 'east': 359.75,
 'south_north_increment': 0.25,
 'west_east_increment': 0.25,
 'shortName': '10u',
 'units': 'm s**-1',
 'paramId': '165',
 'level': '0',
 'typeOfLevel': 'surface',
 'marsClass': 'ea',
 'marsStream': 'oper',
 'marsType': 'an',
 'number': '0',
 'stepRange': '0',
 'param': '165.128',
 'shape': (721, 1440)}
```

***
```python
# Code, line: 421
    @property
    def resolution(self):
        grid_type = self["gridType"]

        if grid_type == "reduced_gg":
            return self["gridName"]

        if grid_type == "regular_ll":
            x = self["DxInDegrees"]
            y = self["DyInDegrees"]
            assert x == y, (x, y)
            return x

        raise ValueError(f"Unknown gridType={grid_type}")

 
template.resolution

0.25
```

***

```python
# Code, line:436
    def datetime(self):
        date = self.handle.get("date")
        time = self.handle.get("time")
        return datetime.datetime(
            date // 10000,
            date % 10000 // 100,
            date % 100,
            time // 100,
            time % 100,
        )

# Example
template.datetime()

datetime.datetime(2022, 1, 1, 0, 0)
```
***

```python
# Code, line: 447
    def valid_datetime(self):
        date = self.handle.get("validityDate")
        time = self.handle.get("validityTime")
        assert 0 <= time <= 2400, (date, time)
        assert isinstance(date, int), (date, time)

        date = str(date)
        time = f"{time:04d}"
        assert len(date) == 8, (date, time)
        assert len(time) == 4, (date, time)

        return datetime.datetime(
            int(date[0:4]),
            int(date[4:6]),
            int(date[6:8]),
            int(time[0:2]),
            int(time[2:4]),
        )
# Example    
>>>template.valid_datetime()
datetime.datetime(2022, 1, 1, 0, 0)

```
***

```python
# Example
    def to_datetime_list(self):
        return [self.valid_datetime()]

# Example
>>> template.to_datetime_list()
[datetime.datetime(2022, 1, 1, 0, 0)]
```

```python
# Code, line: 469
    def to_bounding_box(self):
        return BoundingBox(
            north=self.handle.get("latitudeOfFirstGridPointInDegrees"),
            south=self.handle.get("latitudeOfLastGridPointInDegrees"),
            west=self.handle.get("longitudeOfFirstGridPointInDegrees"),
            east=self.handle.get("longitudeOfLastGridPointInDegrees"),
        )

# Example
>>> template.to_bounding_box()
BoundingBox(north=90,west=0,south=-90,east=359.75)
```
***

```python
# Code, line: 477
    def _attributes(self, names):
        result = {}
        for name in names:
            result[name] = self.handle.get(name)
        return result

# Example
>>> template_ds[0]._attributes('shortName')

{'s': None,
 'h': None,
 'o': None,
 'r': None,
 't': None,
 'N': None,
 'a': None,
 'm': None,
 'e': None}
```

*** 

```python
# Code, line: 483
    def _get(self, name):
        """Private, for testing only"""
        # paramId is renamed as param to get rid of the
        # additional '.128' (in climetlab/scripts/grib.py)
        if name == "param":
            name = "paramId"
        return self.handle.get(name)

# Example
>>> template_ds[0]._get('shortName')
>>>'10u'
>>> template_ds[0]._get('typeOfLevel')
>>> 'surface'

```

***

```python
# Code, line: 491
    def metadata(self, name):
        if name == DATETIME:
            date = self.metadata("validityDate")
            time = self.metadata("validityTime")
            return datetime.datetime(
                date // 10000,
                date % 10000 // 100,
                date % 100,
                time // 100,
                time % 100,
            ).isoformat()

        if name == "param":
            name = "shortName"

        if name == "_param_id":
            name = "paramId"

        if name == "level" and self[name] == 0:
            return None

        return self[name]

# Example
>>> template.metadata('shortName')
'10u'
```
***

```python
# Code, line: 535
    @property
    def data(self):
        return self.handle.get_data()

# Example 
>>> template_ds[0].data
>>> ({'lat': 90.0, 'lon': 0.0, 'value': 0.0674896240234375},
 {'lat': 90.0, 'lon': 0.25, 'value': 0.0674896240234375},
 {'lat': 90.0, 'lon': 0.5, 'value': 0.0674896240234375},
...
```
***

```python
# Code, line 542
    def write(self, f):
        f.write(self.handle.read_bytes(self._offset, self._length))
```



```python
# Code, line: 573
    def grid_points(self):
        import numpy as np

        data = self.data
        lat = np.array([d["lat"] for d in data])
        lon = np.array([d["lon"] for d in data])

        return lat, lon

# Example
>>> template.grid_points()
(array([ 90.,  90.,  90., ..., -90., -90., -90.]),
 array([0.0000e+00, 2.5000e-01, 5.0000e-01, ..., 3.5925e+02, 3.5950e+02,
        3.5975e+02]))
```



## climetlab/readers/grib/fieldset.py

#### Line 25: class FieldSetMixin(PandasMixIn, XarrayMixIn, PytorchMixIn, TensorflowMixIn):


## climetlab/readers/grib/output.py

#### Line 41: class Combined:

```python
class Combined:
    def __init__(self, handle, metadata):
        self.handle = handle
        self.metadata = metadata
```

### Line 56: 
```python
class GribOutput:


```

***
```python 
    def write(
        self,
        values,
        check_nans=False,
        metadata={},
        template=None,
        **kwargs,
    ):



...
		 compulsary = ("date", ("param", "paramId", "shortName"))
		
...
        if template is None:
            handle = self.handle_from_metadata(values, metadata, compulsary)
        else:
            handle = template.handle.clone()
            
...
		 self.update_metadata(handle, metadata, compulsary)

...

        metadata = {
            k: v for k, v in sorted(metadata.items(), key=lambda x: order(x[0]))
        }

        LOG.debug("GribOutput.metadata %s", metadata)

        for k, v in metadata.items():
            handle.set(k, v)

        handle.set_values(values)

        file, path = self.f(handle)
        handle.write(file)

        return handle, path
```
***


```python
    def close(self):
        for f in self._files.values():
            f.close()

```
***

Line: 150
```python
	 def update_metadata(self, handle, metadata, compulsary):
		# TODO: revisit that logic
		combined = Combined(handle, metadata)

        if "step" in metadata:
            if combined["type"] == "an":
                metadata["type"] = "fc"

        if "time" in metadata:  # TODO, use a normalizer
            try:
                time = int(metadata["time"])
                if time < 100:
                    metadata["time"] = time * 100
            except ValueError:
                pass
        
        if "time" not in metadata and "date" in metadata:
            date = metadata["date"]
            metadata["time"] = date.hour * 100 + date.minute

        if "date" in metadata:
            if isinstance(metadata["date"], datetime.datetime):
                date = metadata["date"]
                metadata["date"] = date.year * 10000 + date.month * 100 + date.day
            else:
                metadata["date"] = int(metadata["date"])

...

        if "levelist" in metadata:
            metadata.setdefault("levtype", "pl")

        if "param" in metadata:
            param = metadata.pop("param")
            try:
                metadata["paramId"] = int(param)
            except ValueError:
                metadata["shortName"] = param
            


```

```python
handle = template.handle.clone()
compulsary = ("date", ("param", "paramId", "shortName"))
metadata = {'time':6}
output = cml.new_grib_output('/scratch/fp0/mah900/ai-models/test_template_3.grib')

output.update_metadata(handle, metadata, compulsary) 
# NO effect
```

***



```python 
    def handle_from_metadata(self, values, metadata, compulsary):
        from .codes import CodesHandle  # Lazy loading of eccodes
    
```

***

```python
def new_grib_output(*args, **kwargs):
    return GribOutput(*args, **kwargs)
```


## 2023.10.01
## climetlab/readers/grib/reader.py



