
https://typeset.io/papers/rainbench-towards-global-precipitation-forecasting-from-2xucbeq7we


TL;DR: This paper introduces RainBench, a new multi-modal benchmark dataset for data-driven precipitation forecasting that includes simulated satellite data, a selection of relevant meteorological data from the ERA5 reanalysis product, and IMERG precipitation data.


