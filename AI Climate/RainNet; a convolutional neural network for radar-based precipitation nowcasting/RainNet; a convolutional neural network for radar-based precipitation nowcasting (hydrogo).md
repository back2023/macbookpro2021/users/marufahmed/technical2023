

## Brief description

Here we introduce RainNet -- a convolutional neural network for radar-based precipitation nowcasting. RainNet was trained to predict continuous precipitation intensities at a lead time of five minutes, using several years of quality-controlled weather radar composites provided by the German Weather Service (DWD).

The source code of the RainNet model written using [_Keras_](https://keras.io/) functional API is in the file `rainnet.py`.

The pretrained instance of `keras` `Model` for RainNet, as well as RainNet's pretrained weights are available on Zenodo:

[![DOI](https://camo.githubusercontent.com/969c3c3f6a6748af64697724f9937a1a29da42faa8187ae20c711643b6b7fc93/68747470733a2f2f7a656e6f646f2e6f72672f62616467652f444f492f31302e353238312f7a656e6f646f2e333633303432392e737667)](https://doi.org/10.5281/zenodo.3630429)


https://github.com/hydrogo/rainnet


