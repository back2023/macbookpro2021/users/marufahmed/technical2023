
https://spiral.imperial.ac.uk/bitstream/10044/1/93422/5/Quart%20J%20Royal%20Meteoro%20Soc%20-%202021%20-%20Clare%20-%20Combining%20distribution%E2%80%90based%20neural%20networks%20to%20predict%20weather%20forecast.pdf


### Abstract 

The success of deep learning techniques over the last decades has opened up a new avenue of research for weather forecasting. Here, we take the novel approach of using a neural network to predict full probability density functions at each point in space and time rather than a single output value, thus producing a probabilistic weather forecast. This enables the calculation of both uncertainty and skill metrics for the neural network predictions, and overcomes the common difficulty of inferring uncertainty from these predictions. This approach is data-driven and the neural network is trained on the WeatherBench dataset (processed ERA5 data) to forecast geopotential and temperature 3 and 5 days ahead. Data exploration leads to the identification of the most important input variables. In order to increase computational efficiency, several neural networks are trained on small subsets of these variables. The outputs are then combined through a stacked neural network, the first time such a technique has been applied to weather data. Our approach is found to be more accurate than some coarse numerical weather prediction models and as accurate as more complex alternative neural networks, with the added benefit of providing key probabilistic information necessary for making informed weather forecasts.

