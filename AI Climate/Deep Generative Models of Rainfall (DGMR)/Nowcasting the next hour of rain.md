

Our lives are dependent on the weather. At any moment in the UK, according to [one study](https://www.bbc.com/future/article/20151214-why-do-brits-talk-about-the-weather-so-much), one third of the country has talked about the weather in the past hour, reflecting the importance of weather in daily life. Amongst weather phenomena, rain is especially important because of its influence on our everyday decisions. Should I take an umbrella? How should we route vehicles experiencing heavy rain? What safety measures do we take for outdoor events? Will there be a flood?


https://www.deepmind.com/blog/nowcasting-the-next-hour-of-rain
