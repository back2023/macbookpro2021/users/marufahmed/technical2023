

[DeepMind](https://deepmind.com/) open-sourced a dataset and trained model snapshot for [Deep Generative Models of Rainfall](https://deepmind.com/blog/article/nowcasting) (DGMR), an AI system for short-term precipitation forecasts. In evaluations conducted by 58 expert meteorologists comparing it to other existing methods, DGMR was ranked first in accuracy and usefulness in 89% of test cases.


https://www.infoq.com/news/2021/12/deepmind-weather-forecasting/


