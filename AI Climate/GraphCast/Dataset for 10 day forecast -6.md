
https://github.com/google-deepmind/graphcast/issues/6

Hi, thanks for your reply.

Those inputs are only provided as dataset samples to replicate training in the paper which only goes to 3 days.

For producing 10 day forecasts, I would recommend to obtain data directly from ERA5, which you may be able to obtain via [WeatherBench2](https://weatherbench2.readthedocs.io/en/latest/data-guide.html).

Hope this helps!
