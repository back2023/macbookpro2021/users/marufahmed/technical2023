
https://developer.nvidia.com/blog/develop-physics-informed-machine-learning-models-with-graph-neural-networks/



Modulus also includes the GraphCast architecture proposed in [GraphCast: Learning Skillful Medium-Range Global Weather Forecasting](https://arxiv.org/pdf/2212.12794.pdf). GraphCast is a novel GNN-based architecture for global weather forecasting. It significantly improves on some existing models by effectively capturing spatio-temporal relationships in weather data. The weather data is modeled as a graph, where nodes represent Earth grid cells. This graph-based representation enables the model to capture both local and non-local dependencies in the data.

