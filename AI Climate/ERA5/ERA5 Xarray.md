 
### How to extract ERA5 data at different pressure levels from a 4 dimension (4D) dataset 
https://confluence.ecmwf.int/display/CUSF/How+to+extract+ERA5+data+at+different+pressure+levels+from+a+4+dimension+%284D%29+dataset

import xarray and read the data: 
```bash
import xarray as xr

#read the data:
ds = xr.open_dataset('/path/to/data/ERA5_2000_03.nc')
```

Select area using sel() method (note that latitudes are decreasing in your dataset this is why you put 40 then 30):
```bash
ds_area  = ds.sel(latitude=slice( 40 , 30 ),longitude=slice(- 45 , 10 )) 
```
Select one point also using sel() method:
```bash
one_point = ds.sel(latitude=37, longitude=-40)
```
This will select the area or point for he whole dataset if you have more variables. You can do the same thing for each individual dataarray, for example something like this:
```bash
one_point_o3 = ds.o3.sel(latitude=``37``, longitude=-``40``)`
```
You can use sel() method to filter any dimension, not only latitude and longitude, but time and level in this case, as well.
If you want to calculate mean between 2 levels, you can do something like this:
```
o3_400_450_mean = one_point_o3.o3.sel(level=slice(450,500)).mean('level')
```
I am sure there are different ways to get all of this done.



