
### ERA5 Climate Gridded Data
Download and use the  ECMWF ERA5 from the [AWS Public Dataset Program](https://registry.opendata.aws/ecmwf-era5/).
https://docs.digitalearthafrica.org/en/latest/sandbox/notebooks/Datasets/Climate_Data_ERA5_AWS.html

### ERA5 Data on S3 via AWS Public Dataset Program
https://github.com/planet-os/notebooks/blob/master/aws/era5-pds.md

https://github.com/planet-os/notebooks/blob/master/aws/era5-s3-via-boto.ipynb

### Accessing ERA5 Data on S3
https://notebook.community/planet-os/notebooks/api-examples/era5-s3-via-boto


### Processing ERA5 data in NetCDF Format
To work with the ECMWF ERA5 at the AWS Public Dataset Program
Utilizes Amazon SageMaker & AWS Fargate with a Jupyter notebook and Dask cluster.
https://nbviewer.org/github/awslabs/amazon-asdi/blob/main/examples/dask/notebooks/era5.ipynb


