

A full archive of ERA5 reanalysis data has been made available as a free to access public data set on [AWS](https://registry.opendata.aws/ecmwf-era5/). To aid effiecent cloud access to this data the provider has reproduced the native netcdf variables as Zarr stores. While this is convenient for the end user the burden of processing and the cost duplicating the original data is significant.


https://gist.github.com/rsignell-usgs/0f05f37902056036128c8c26bbebe14d






