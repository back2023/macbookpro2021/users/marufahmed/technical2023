### Download data  form - API generator. 
1. Use the two pages below to select parameters, date, hours, etc.
2. At the end of the page, a button to generate the API 

1. ERA5 hourly data on **pressure levels** from 1940 to present
https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels?tab=form

2. ERA5 hourly data on **single levels** from 1940 to present
https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form