

## **Basics**

**NetCDF** (Network Common Data Form) is a set of software libraries and self-describing, machine-independent data formats that support the creation, access, and sharing of array-oriented scientific data. NetCDF is commonly used to store and distribute scientific data. The NetCDF software was developed at the Unidata Program Center in Boulder, Colorado, USA ([Unidata NetCDF Factsheet](http://www.unidata.ucar.edu/publications/factsheets/current/factsheet_netcdf.pdf); Also see [Wikipedia article](https://en.wikipedia.org/wiki/NetCDF)). NetCDF files usually have the extension .nc.

To read NetCDF files there are tools with a graphical interface like Matlab, IDL, ArcGIS, [NCView](http://cirrus.ucsd.edu/~pierce/software/ncview/quick_intro.html), [Xconv](https://ncas-cms.github.io/um-training/post-processing.html) and developer (programming) tools like the [Unidata NetCDF4 module for Python](http://unidata.github.io/netcdf4-python/) and [Xarray](http://xarray.pydata.org/en/stable/index.html). Please see your preferred tools' documentation for further information regarding NetCDF support.

For climate and forecast data stored in NetCDF format there are several (non-mandatory) metadata conventions which can be used (such as the [CF Convention)](http://cfconventions.org/). CF compliant metadata in NetCDF files can be accessed by several tools, including [Metview](https://confluence.ecmwf.int/display/METV/Metview), [NCView](http://cirrus.ucsd.edu/~pierce/software/ncview/quick_intro.html), [Xconv](https://ncas-cms.github.io/um-training/post-processing.html) .

The latest version of the NetCDF format is NetCDF 4 (aka NetCDF _enhanced_, introduced in 2008), but NetCDF 3 (NetCDF _classic)_ is also still used.

NetCDF files can be converted [to ASCII or text](http://www.unidata.ucar.edu/software/netcdf/docs/faq.html#How-do-I-convert-netCDF-data-to-ASCII-or-text) see the following link for more details: [How to convert NetCDF to CSV](https://confluence.ecmwf.int/display/CKB/How+to+convert+NetCDF+to+CSV) , although please be aware that this can lead to large amounts of output.



https://confluence.ecmwf.int/display/CKB/What+are+NetCDF+files+and+how+can+I+read+them


