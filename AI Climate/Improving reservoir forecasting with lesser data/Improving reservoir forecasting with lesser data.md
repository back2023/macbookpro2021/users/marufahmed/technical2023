
In this work, I studied how knowledge of reservoir physics can help improve data-based models for reservoir prediction. This concept can be generalized to other data-science and Machine Learning areas where we have access to analytical equations that can provide strong prior to the model. My research extends the work of [1]. But this repository only contains the my simple Pytorch based implementation of [1], which uses the PINN developed in [PINN](https://github.com/nanditadoloi/PINN).


https://github.com/nanditadoloi/PIML



