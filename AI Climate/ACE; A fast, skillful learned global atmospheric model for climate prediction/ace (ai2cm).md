

https://github.com/ai2cm/ace




### 2. Download data and checkpoint

These are available via a public [requester pays](https://cloud.google.com/storage/docs/requester-pays) Google Cloud Storage bucket. The checkpoint can be downloaded with:
```
gsutil -u YOUR_GCP_PROJECT cp gs://ai2cm-public-requester-pays/2023-11-29-ai2-climate-emulator-v1/checkpoints/ace_ckpt.tar .
```

Download the 10-year validation data (approx. 190GB; can also download a portion only, but it is required to download enough data to span the desired prediction period):
```
gsutil -m -u YOUR_GCP_PROJECT cp -r gs://ai2cm-public-requester-pays/2023-11-29-ai2-climate-emulator-v1/data/repeating-climSST-1deg-netCDFs/validation .
```

