

https://blog.allenai.org/stable-and-fast-ace-the-ai2-climate-emulator-a2da5abe9cb5


## ACE: a stable and fast climate emulator

Here we present [ACE (AI2 Climate Emulator)](https://doi.org/10.48550/arXiv.2310.02074), a neural network-based atmospheric model. ACE uses the Spherical Fourier Neural Operator architecture and is trained to emulate an existing physics-based atmospheric model to predict global outputs every 6 hours. ACE runs stably for at least 100 years and can simulate a decade in one hour of wall clock time, nearly 100 times faster and using 100 times less energy than the reference atmospheric model.

ACE replicates the near-surface climatology of the reference physics-based model better and runs faster than a 2x coarser but otherwise identical model. Finally, ACE is framed so that precise evaluation conservation of mass and moisture is possible, and we find that moisture for a given vertical model column is very nearly conserved across individual timesteps.


