
https://github.com/jweyn/DLWP-CS/tree/master

## About

Deep learning models for global weather prediction on a cubed sphere

```
# DLWP-CS: Deep Learning Weather Prediction

#### [](https://github.com/jweyn/DLWP-CS/tree/master#dlwp-cs-is-a-python-project-containing-data-processing-and-model-building-tools-for-predicting-the-gridded-atmosphere-using-deep-convolutional-neural-networks-applied-to-a-cubed-sphere)DLWP-CS is a Python project containing data-processing and model-building tools for predicting the gridded atmosphere using deep convolutional neural networks applied to a cubed sphere.
```


<iframe src="https://github.com/jweyn/DLWP-CS/tree/master" style="width:100%; height:300px;" ></iframe>


