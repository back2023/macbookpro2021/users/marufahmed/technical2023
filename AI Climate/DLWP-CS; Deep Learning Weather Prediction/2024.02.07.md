
Install env
https://github.com/jweyn/DLWP-CS/tree/master
Plan: 
- Use the NCI-ml module for already install packages
- Create a Conda env for the rest

### Git Download
```bash
cd /scratch/fp0/mah900/
git clone https://github.com/jweyn/DLWP-CS.git
```

### Install
```bash
module use /g/data/dk92/apps/Modules/modulefiles
module load NCI-ai-ml/23.10

. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/dlwp  

conda activate /scratch/fp0/mah900/env/dlwp
which python
/g/data/dk92/apps/NCI-ai-ml/23.10/bin/python

# bash: GenerateCSMesh: command not found
# ModuleNotFoundError: No module named 'pyspharm'
# ModuleNotFoundError: No module named 'pygrib'
conda install -c conda-forge tempest-remap
pip install pygrib pyspharm
...
Installing collected packages: pyspharm, pygrib
Successfully installed pygrib-2.1.5 pyspharm-1.0.9
```

### ARE
```
Storage: gdata/fp0+gdata/dk92+gdata/z00+gdata/rt52+scratch/fp0+gdata/wb00+gdata/pp66+scratch/vp91

Module directories: /g/data/dk92/apps/Modules/modulefiles
Modules: NCI-ai-ml/23.10

Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda2023
Conda environment:  /scratch/fp0/mah900/env/dlwp

```

Asking for a password
Try-1 (Does not work)
```
Session ID -> config.py -> c.NotebookApp.password
```
Try-2 (Working)
```
Session ID -> connection.yml ->
# [connection.yml]
host: gadi-gpu-v100-0112.gadi.nci.org.au
port: 34427
password: ZABoReqCILYDUyZP
```
Next Try: ~~install JupyterLab in the conda env. ~~


Test imports
```python
import warnings
import os
NOTEBOOK_PATH = "/scratch/fp0/mah900/DLWP-CS" 
warnings.filterwarnings('ignore')
os.environ["TF_ENABLE_ONEDNN_OPTS"] = "0"
os.chdir (f'{NOTEBOOK_PATH}')
import DLWP

# barotropic
from __future__ import (absolute_import, division, print_function)  #noqa
from datetime import timedelta
import math
import numpy as np
from DLWP.barotropic.pyspharm_transforms import TransformsEngine
from DLWP.barotropic.model import BarotropicModel, BarotropicModelPsi
```


# 2024.02.07

### Error: `multi_gpu_model` is replaced
```
ImportError: cannot import name 'multi_gpu_model' from 'keras.utils' (/opt/conda/envs/mlenv/lib/python3.9/site-packages/keras/utils/__init__.py)
```
Reason:
https://discuss.tensorflow.org/t/multi-gpu-model/11778
Tried
https://github.com/minimaxir/textgenrnn/issues/238

Import is In the file : `scratch/fp0/mah900/DLWP-CS/DLWP/model/models.py` (Does not work)
```
#from tensorflow.keras.utils import multi_gpu_model
#from tensorflow.python.keras.utils.multi_gpu_utils import multi_gpu_model
#from keras.utils import multi_gpu_model
```

Error, also, in: `/scratch/fp0/mah900/DLWP-CS/DLWP/util.py`
```
---> 20 from tensorflow.keras.utils import multi_gpu_model
```

### Check older version of NCI-ai-ml to see if tf before 2.4 exists. 
```bash
module use /g/data/dk92/apps/Modules/modulefiles
module av NCI
```
No, the earliest version is `NCI-ai-ml/22.08`
has `tensorflow-gpu            2.8.2`


Import check
```python
import warnings
import os
NOTEBOOK_PATH = "/scratch/fp0/mah900/DLWP-CS" 
warnings.filterwarnings('ignore')
os.environ["TF_ENABLE_ONEDNN_OPTS"] = "0"
os.chdir (f'{NOTEBOOK_PATH}')
import DLWP

import os
import warnings
import itertools as it
import numpy as np
import netCDF4 as nc
import pandas as pd
import xarray as xr
from datetime import datetime
from copy import deepcopy


# barotropic
from __future__ import (absolute_import, division, print_function)  #noqa
from datetime import timedelta
import math
import numpy as np
from DLWP.barotropic.pyspharm_transforms import TransformsEngine
from DLWP.barotropic.model import BarotropicModel, BarotropicModelPsi

#data
from DLWP.data.cfsr import CFSReanalysis, CFSReforecast
from DLWP.data.era5 import ERA5Reanalysis
try:
    from urllib.request import urlopen
except ImportError:
    from urllib import urlopen
try:
    import pygrib
except ImportError:
    warnings.warn("module 'pygrib' not found; processing of raw CFS data unavailable.")
    
# Model
from DLWP.model.models import DLWPNeuralNet, DLWPFunctional
from DLWP.model.generators import DataGenerator, SeriesDataGenerator, ArrayDataGenerator, tf_data_generator
from DLWP.model.preprocessing import Preprocessor
from DLWP.model.extensions import TimeSeriesEstimator, SeriesDataGeneratorWithInference, ArrayDataGeneratorWithInference

from DLWP.model.models_torch import DLWPTorchNN
    
from DLWP.model.models import DLWPNeuralNet, DLWPFunctional
from DLWP.model.models_torch import DLWPTorchNN
from DLWP.model.generators import DataGenerator, SeriesDataGenerator, ArrayDataGenerator
from DLWP.util import insolation    
    
from tensorflow.keras.utils import Sequence
from DLWP.util import delete_nan_samples, insolation, to_bool

from tensorflow.keras import models
# ImportError: cannot import name 'multi_gpu_model' from 'tensorflow.keras.utils'
#from tensorflow.keras.utils import multi_gpu_model

from DLWP.model.generators import DataGenerator, SeriesDataGenerator, ArrayDataGenerator
from DLWP import util
   
from DLWP import util

try:
    import torch
    import torch.nn as nn
    import torch.nn.functional as F
    import torch.optim as optim

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

except ImportError:
    warnings.warn('DLWPTorchNN is not available because PyTorch is not installed.')

from DLWP.util import to_bool, insolation    

# plot
from DLWP.plot.plot_functions import *
from DLWP.plot import util

from DLWP.util import remove_chars

from matplotlib.colors import LinearSegmentedColormap, ListedColormap

#remap
from DLWP.remap.cubesphere import CubeSphereRemap
import subprocess
import warnings
from DLWP.remap.base import _BaseRemap

# custom.py
import tensorflow as tf
from tensorflow.compat.v1 import keras as tfk
from tensorflow.keras.callbacks import Callback, EarlyStopping
from tensorflow.keras.layers import ZeroPadding2D, ZeroPadding3D, LocallyConnected2D, Lambda, Layer
from tensorflow.keras.losses import mean_absolute_error, mean_squared_error
from tensorflow.python.keras.utils import conv_utils
from tensorflow.python.keras.engine.base_layer import InputSpec
from tensorflow.keras import activations, initializers, regularizers, constraints
import numpy as np

# util.py
import pickle
import random
import re
import tempfile
from importlib import import_module
from copy import copy
import numpy as np
import pandas as pd
from tensorflow.keras import models as keras_models
#from tensorflow.keras.utils import multi_gpu_model


```
Works for all except for `from tensorflow.keras.utils import multi_gpu_model` statement

**Next plan**: 
use the https://github.com/jweyn/DLWP-CS/blob/master/environment.yml, to build a conda env.
See, if that works. 
(**First try** if notebooks can be run in the already install env.
If not then delete the env and create a new from yaml file)


# 2024.02.13

Plan: Try loading NCI weather-bench data.

Make a copy of the notebooks 
```
cp -r /scratch/fp0/mah900/DLWP-CS/Tutorials/  /scratch/fp0/mah900/DLWP-CS/NCI-Tutorials
```
ARE Location: `/scratch/fp0/mah900/DLWP-CS/NCI-Tutorials`

### Changes in notebooks:

### 1. `/scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/1 - Downloading and processing ERA5.ipynb`
1) Stop the download from ECMWF
`era.retrieve` is used to download and process the ERA5 data.
Turn off the cell in: `/scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/1 - Downloading and processing ERA5.ipynb`
```python
#era.retrieve(variables, levels, years=years, hourly=3,
#             request_kwargs={'grid': [2., 2.]}, verbose=True, delete_temporary=True)
```
In the file: `/scratch/fp0/mah900/DLWP-CS/DLWP/data/era5.py:411`, the `open(self, **dataset_kwargs)` function is downloading the data.
Instead, to use NCI ERA5, set the Dataset variable directly
```python
import glob
print (variables)
print (years)
all_files = []

for year in years:
    all_files += [ f'{DATAPATH}/{variables[0]}/{variables[0]}_{year}_{res}.nc' ]
    all_files += [ f'{DATAPATH}/{variables[1]}/{variables[1]}_{year}_{res}.nc' ]
#print (*all_files, sep="\n") 
Dataset = xr.open_mfdataset(all_files, chunks={'time': 10}, parallel = True).sel(level=500)
#print(Dataset)  
Dataset = Dataset.isel(time=slice(0, None, 3))
#print(raw_data)  
era.Dataset = Dataset
era.dataset_dates = self.Dataset['time']
print(era.Dataset)
```

2) Levels have to be added to the surface level variables
In file: `scratch/fp0/mah900/DLWP-CS/DLWP/data/era5.py`, the `_process_temp_file(self, file_name, level):404` function sets level. 
It is called from the ` _fetch(self, request, file_name, verbose)` function.
So code need to be changed
~~Load two datasets, increase level and merge~~
Only the pressure level is processed, so adding a list should work 
```
 Dataset = xr.open_mfdataset(all_files, chunks={'time': 10}, parallel = True).sel(level=[500])
```

3) 
Error:
```bash
--> 528         n_sample, n_var, n_level, n_lat, n_lon = (len(all_dates), len(variables), len(levels),    529                                                   ds.dims[lat_dim], ds.dims[lon_dim])
...
TypeError: object of type 'NoneType' has no len()
```
Reason: 
`dataset_dates` has to be set 
File: `scratch/fp0/mah900/DLWP-CS/DLWP/data/era5.py:422`
```
self.dataset_dates = self.Dataset['time']
```
So, add
```
era.dataset_dates = era.Dataset['time']
```

`/scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/1 - Downloading and processing ERA5.ipynb` is working

### 2.`/scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/2 - Remapping to the cubed sphere.ipynb`

1) Change dir code
```python
import warnings
import os
NOTEBOOK_PATH = "/scratch/fp0/mah900/DLWP-CS" 
warnings.filterwarnings('ignore')
os.environ["TF_ENABLE_ONEDNN_OPTS"] = "0"
os.chdir (f'{NOTEBOOK_PATH}')
```
Also, commented off
```python
#import os
#os.chdir(os.pardir)
```

2. Error
```
CubeSphereRemap: generating offline forward map...
/opt/conda/envs/mlenv/bin/GenerateRLLMesh --lat 91 --lon 180 --file outLL.g --lat_begin 90 --lat_end -90 --out_format Netcdf4

...
/scratch/fp0/mah900/DLWP-CS/DLWP/remap/cubesphere.py in generate_offline_maps(self, lat, lon, res, map_name, inverse_map_name, inverse_lat, remove_meshes, in_np)
    117             if self.verbose:
    118                 print(' '.join(cmd))
--> 119             subprocess.check_output(cmd)
    120         except subprocess.CalledProcessError as e:
    121             print('An error occurred while generating the lat-lon mesh.')

...
FileNotFoundError: [Errno 2] No such file or directory: '/opt/conda/envs/mlenv/bin/GenerateRLLMesh'
```
Reason: `GenerateRLLMesh` path has to be corrected in `CubeSphereRemap` in `DLWP.remap`
The install in:`/scratch/fp0/mah900/env/dlwp/bin/GenerateRLLMesh`, is not found
Now, in `scratch/fp0/mah900/DLWP-CS/DLWP/remap/cubesphere.py` file, class `CubeSphereRemap(_BaseRemap):`, initializer `__init__(self, path_to_remapper=None, to_netcdf4=True, verbose=True)` has a option to set the `TempestRemap executables` path
```
:param path_to_remapper: str: path to the TempestRemap executables
```

Try using that one (Working)
```python
#csr = CubeSphereRemap()
csr = CubeSphereRemap(path_to_remapper='/scratch/fp0/mah900/env/dlwp/bin/')
```

3. Error
```bash
CubeSphereRemap: applying forward map...
/scratch/fp0/mah900/env/dlwp/bin/ApplyOfflineMap --in_data /home/disk/wave2/jweyn/Data/ERA5/tutorial_z500_t2m.nc.nocoord --out_data /home/disk/wave2/jweyn/Data/ERA5/temp.nc --map map_LL91x180_CS48.nc --var predictors
...
Cannot open source data file "/home/disk/wave2/jweyn/Data/ERA5/tutorial_z500_t2m.nc.nocoord"
```
~~Need to check, `/scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/1 - Downloading and processing ERA5.ipynb` again.~~
The file path is wrong, just set the correct path. (Working)

4. 
Error
```
CubeSphereRemap: applying forward map...
/scratch/fp0/mah900/env/dlwp/bin/ApplyOfflineMap --in_data /g/data/wb00/admin/testing/DLWP/Data/ERA5/tutorial_z500_t2m.nc.nocoord --out_data /g/data/wb00/admin/testing/DLWP/Data/ERA5/temp.nc --map map_LL91x180_CS48.nc --var predictors
...
Done\nProcessing "/g/data/wb00/admin/testing/DLWP/Data/ERA5/tutorial_z500_t2m.nc.nocoord"\n..predictors\n....EXCEPTION (src/OfflineMap.cpp, Line 1804) Error: Source variable "predictors" has inconsistent dimension size for this map in dimension "lat".  Expected 91, found 64.
```
Is it because  of the difference in resolution???
"Since we used 2 degree data, we have 91 latitude points and 180 longitude points"
Okay, the cell above has 
```python
csr.generate_offline_maps(lat=91, lon=180, res=48, inverse_lat=True)
```
Whereas, NCI weather-bench has 64 on the lat. 
So, try (Working)
```python
csr.generate_offline_maps(lat=64, lon=128, res=48, inverse_lat=True)
```


### 3. `scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/3 - Training a DLWP-CS model.ipynb`

1. Error
```bash
---------------------------------------------------------------------------
AttributeError                            Traceback (most recent call last)
/jobfs/108083344.gadi-pbs/ipykernel_2173086/2106408161.py in <cell line: 4>()
      2 
      3 input_names = ['main_input'] + ['solar_%d' % i for i in range(1, integration_steps)]
----> 4 tf_train_data = tf_data_generator(generator, batch_size=batch_size, input_names=input_names)
      5 tf_val_data = tf_data_generator(val_generator, input_names=input_names)
...

/scratch/fp0/mah900/DLWP-CS/DLWP/model/generators.py in generate(self, samples)
    874             samples = np.arange(self._n_sample, dtype=np.int)
    875         else:
--> 876             samples = np.array(samples, dtype=np.int)
    877         n_sample = len(samples)
    878
...

AttributeError: module 'numpy' has no attribute 'int'.
`np.int` was a deprecated alias for the builtin `int`. To avoid this error in existing code, use `int` by itself. Doing this will not modify any behavior and is safe. When replacing `np.int`, you may wish to use e.g. `np.int64` or `np.int32` to specify the precision. If you wish to review your current use, check the release note link for additional information.
The aliases was originally deprecated in NumPy 1.20; for more details and guidance see the original release note at:
    [https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations](https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations)
      
```
Try (Working)
```python
 np.int_
```
Source: https://github.com/WongKinYiu/yolov7/issues/1280#issuecomment-1584851356
Changed four occurrences in: `scratch/fp0/mah900/DLWP-CS/DLWP/model/generators.py`

2. 
```bash
--------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
/jobfs/108083344.gadi-pbs/ipykernel_2173600/1009637842.py in <cell line: 25>()
     23 }
     24 skip_connections = 'unet' in cnn_model_name.lower()
---> 25 conv_2d_1 = CubeSphereConv2D(base_filter_number, 3, **conv_kwargs)
     26 conv_2d_1_2 = CubeSphereConv2D(base_filter_number, 3, **conv_kwargs)
     27 conv_2d_1_3 = CubeSphereConv2D(base_filter_number, 3, **conv_kwargs)

/scratch/fp0/mah900/DLWP-CS/DLWP/custom.py in __init__(self, filters, kernel_size, strides, padding, data_format, dilation_rate, activation, use_bias, flip_north_pole, independent_north_pole, kernel_initializer, bias_initializer, kernel_regularizer, bias_regularizer, activity_regularizer, kernel_constraint, bias_constraint, **kwargs)
    860         self.bias_constraint = constraints.get(bias_constraint)
    861         self.rank = 3
--> 862         self.input_spec = InputSpec(ndim=self.rank + 2)
    863 
    864         self.equatorial_kernel = None
...

TypeError: Layer input_spec must be an instance of InputSpec. Got: InputSpec(ndim=5)
		
```
Try-1 (Working)
In File: `scratch/fp0/mah900/DLWP-CS/DLWP/custom.py:17`
```python
#from tensorflow.python.keras.engine.base_layer import InputSpec
from tensorflow.keras.layers import InputSpec
```
Explanation:
```
Anything under `tf.python.*` is private, intended for development only, rather than for public use.  
Importing from tensorflow.python or any other modules (including import tensorflow_core...) is not supported, and can break unannounced.So, it is suggested not to use anything with tf.python.*.

You can use `tf.keras.layers.InputSpec` instead of `tensorflow.python.keras.engine.input_spec`. Please find the gist of working code [here](https://colab.sandbox.google.com/gist/synandi/b7fa1ca299d9901f650e8a8bdf55c992/59863.ipynb). Thank you!
```
Source: https://github.com/tensorflow/tensorflow/issues/59863#issuecomment-1455567587
Example in Colab: https://colab.sandbox.google.com/gist/synandi/b7fa1ca299d9901f650e8a8bdf55c992/59863.ipynb

### 4. `scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/4 - Predicting with a DLWP-CS model.ipynb`

1. Change in Import
```python
from datetime import datetime
import warnings
import os
NOTEBOOK_PATH = "/scratch/fp0/mah900/DLWP-CS" 
warnings.filterwarnings('ignore')
os.environ["TF_ENABLE_ONEDNN_OPTS"] = "0"
os.chdir (f'{NOTEBOOK_PATH}')
#import os
#os.chdir(os.pardir)
```

2. Download checkpoints
```bash
cd /g/data/wb00/admin/testing/DLWP
wget 'https://api.ngc.nvidia.com/v2/models/nvidia/modulus/modulus_dlwp_cubesphere/versions/v0.2/files/dlwp_cubesphere.zip'
unzip dlwp_cubesphere.zip

```
Source: https://catalog.ngc.nvidia.com/orgs/nvidia/teams/modulus/models/modulus_dlwp_cubesphere
No `pkl` file

3. Okay: `pkl` file is created in `/g/data/wb00/admin/testing/DLWP/Data`
Now, the Dirs look 
```python
#root_directory = '/home/disk/wave2/jweyn/Data'
root_directory  = f'/g/data/wb00/admin/testing/DLWP/Data'
predictor_file = os.path.join(root_directory, 'ERA5', 'tutorial_z500_t2m_CS.nc')
scale_file = os.path.join(root_directory, 'ERA5', 'tutorial_z500_t2m.nc')

#model = os.path.join(root_directory, 'dlwp-cs_tutorial')
model = '/g/data/wb00/admin/testing/DLWP/Data/dlwp-cs_tutorial'
map_files = ('map_LL91x180_CS48.nc', 'map_CS48_LL91x180.nc')

print('root_directory:', root_directory)
print('predictor_file:', predictor_file)
print('scale_file: ', scale_file)
print('model:      ', model)
```

4. Error
```
FileNotFoundError: [Errno 2] No such file or directory: '/opt/conda/envs/mlenv/bin/ApplyOfflineMap' 
```
Try (Working)
```python
#csr = CubeSphereRemap(to_netcdf4=True)
csr = CubeSphereRemap(to_netcdf4=True, 
                      path_to_remapper='/scratch/fp0/mah900/env/dlwp/bin/')
```


# 2024.02.14

Plan: Try loading BARRA data.

ARE has to include the path (`gdata/ob53`)
```bash
gdata/fp0+gdata/dk92+gdata/z00+gdata/rt52+scratch/fp0+gdata/wb00+gdata/pp66+scratch/vp91+gdata/ob53
```

Path
```bash
cd /g/data/ob53/BARRA2/output/reanalysis/AUS-11/BOM/ERA5/historical/hres/BARRA-R2/v1/
```

### BARRA parameters


| ob53 | Parameter |
| ---- | ---- |
| hus10 | Specific Humidity |
| mrfso | Soil Frozen Water Content |
| mrfsol | Frozen Water Content of Soil Layer |
`/g/data/ob53/BARRA2/output/reanalysis/AUS-11/BOM/ERA5/historical/hres/BARRA-R2/v1/1hr`
***

| ob53 | long_namer | standard_name |  |
| ---- | ---- | ---- | ---- |
| CAPE | Convective Available Potential Energy | atmosphere_convective_available_potential_energy_wrt_surface |  |
| CIN | Convective Inhibition | atmosphere_convective_inhibition_wrt_surface |  |
| clh |   High Level Cloud Fraction | cloud_area_fraction_in_atmosphere_layer |  |
| clivi | Ice Water Path | atmosphere_mass_content_of_cloud_ice |  |
| cll | Low Level Cloud Fraction | cloud_area_fraction_in_atmosphere_layer |  |
| clm | Mid Level Cloud Fraction | cloud_area_fraction_in_atmosphere_layer |  |
| clt | Total Cloud Cover Percentage | cloud_area_fraction |  |
| clwvi | Condensed Water Path | atmosphere_mass_content_of_cloud_condensed_water |  |
| evspsblpot | Potential Evapotranspiration | water_potential_evaporation_flux |  |
| hfls | Surface Upward Latent Heat Flux | surface_upward_latent_heat_flux |  |
| hfss | Surface Upward Sensible Heat Flux | surface_upward_sensible_heat_flux |  |
| hurs | Near-Surface Relative Humidity | relative_humidity |  |
| hus1000, hus200 , hus300, hus400,  hus500, hus600, hus700, hus850, hus925, hus950 | Specific Humidity | specific_humidity |  |
|  |  |  |  |
| huss | Near-Surface Specific Humidity | specific_humidity |  |
| mrfsos | Frozen Water Content in Upper Portion of Soil Column | frozen_water_content_of_soil_layer |  |
| mrsos | Moisture in Upper Portion of Soil Column | mass_content_of_water_in_soil_layer |  |
| omega500 | Downward Air Velocity in pressure coordinates | downward_air_velocity |  |
| pr | Precipitation | precipitation_flux |  |
| prc | Convective Precipitation | convective_precipitation_flux |  |
| prsn | Snowfall Flux | snowfall_flux |  |
| prw | Water Vapor Path | atmosphere_mass_content_of_water_vapor |  |
| ps | Surface Air Pressure | surface_air_pressure |  |
| psl | Sea Level Pressure | air_pressure_at_mean_sea_level |  |
| rlds | Surface Downwelling Longwave Radiation | surface_downwelling_longwave_flux_in_air |  |
| rldscs | Surface Downwelling Clear-Sky Longwave Radiation | surface_downwelling_longwave_flux_in_air_assuming_clear_sky |  |
| rlus | Surface Upwelling Longwave Radiation | surface_upwelling_longwave_flux_in_air |  |
| rluscs | Surface Upwelling Clear-Sky Longwave Radiation | surface_upwelling_longwave_flux_in_air_assuming_clear_sky |  |
| rlut | TOA Outgoing Longwave Radiation | toa_outgoing_longwave_flux |  |
| rlutcs | TOA Outgoing Clear-Sky Longwave Radiation | toa_outgoing_longwave_flux_assuming_clear_sky |  |
| rsds | Surface Downwelling Shortwave Radiation | surface_downwelling_shortwave_flux_in_air |  |
| rsdscs | Surface Downwelling Clear-Sky Shortwave Radiation | surface_downwelling_shortwave_flux_in_air_assuming_clear_sky |  |
| rsdsdir | Surface Direct Downwelling Shortwave Radiation | surface_direct_downwelling_shortwave_flux_in_air |  |
| rsdt | TOA Incident Shortwave Radiation | toa_incoming_shortwave_flux |  |
| rsus | Surface Upwelling Shortwave Radiation | surface_upwelling_shortwave_flux_in_air |  |
| rsuscs | Surface Upwelling Clear-Sky Shortwave Radiation | surface_upwelling_shortwave_flux_in_air_assuming_clear_sky |  |
| rsut | TOA Outgoing Shortwave Radiation | toa_outgoing_shortwave_flux |  |
| rsutcs | TOA Outgoing Clear-Sky Shortwave Radiation | toa_outgoing_shortwave_flux_assuming_clear_sky |  |
| sfcWind | Near-Surface Wind Speed | wind_speed |  |
| ta1000, ta100m, ta1500m, ta150m, ta200, ta200m, ta250m, ta300, ta400, ta500, ta50m, ta600, ta700, ta850, ta925, ta950 | Air Temperature | air_temperature |  |
|  |  |  |  |
| tas | Near-Surface Air Temperature | air_temperature |  |
| tasmax | Hourly Maximum Near-Surface Air Temperature | air_temperature |  |
| tasmean | Hourly Mean Near-Surface Air Temperature | air_temperature |  |
| tasmin | Hourly Minimum Near-Surface Air Temperature | air_temperature |  |
| ts | Surface Temperature | surface_temperature |  |
| ua1000, ua100m, ua1500m, ua150m, ua200, ua200m, ua250m, ua300, ua400, ua500, ua50m, ua600, ua700, ua850, ua925 | Eastward Wind | eastward_wind |  |
|  |  |  |  |
| uas | Eastward Near-Surface Wind | eastward_wind |  |
| uasmax | Eastward Near-Surface Wind | eastward_wind |  |
| uasmean | Eastward Near-Surface Wind | eastward_wind |  |
| va1000, va100m, va1500m, va150m, va200, va200m, va250m, va300, va400, va500, va50m, va600, va700, va850, va925 | Northward Wind | northward_wind |  |
|  |  |  |  |
| vas | Northward Near-Surface Wind | northward_wind |  |
| vasmax | Northward Near-Surface Wind | northward_wind |  |
| vasmean | Northward Near-Surface Wind | northward_wind |  |
| wa1000, wa200, wa300, wa400, wa500, wa600, wa700, wa850, wa925 | Upward Air Velocity | upward_air_velocity |  |
| wsgsmax | Hourly Maximum Near-Surface Wind Speed of Gust | wind_speed_of_gust |  |
| zg1000, zg200, zg300, zg400, zg500, zg600, zg700, zg850, zg925 | Geopotential Height | geopotential_height |  |
|  |  |  |  |
| zmla |   Height of Boundary Layer | atmosphere_boundary_layer_thickness |  |


### 1. Problem: Variables have to be set manually 
```python
era.set_variables(variables)
```
Function does not work. 
In File: `scratch/fp0/mah900/DLWP-CS/DLWP/data/era5.py:204`
```python
self.dataset_variables = sorted(list(variables))
```
Converted code, in notebook: `/scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/1 - Downloading and processing BARRA.ipynb`
```python
era.dataset_variables = sorted(list(variables))
```

### 2. Problem: Surface and Pressure level  merging together.

```bash
# Dataset1
Frozen({'time': 2191, 'lat': 646, 'lon': 1082, 'bnds': 2})
# Dataset2
Frozen({'time': 2191, 'lat': 646, 'lon': 1082, 'bnds': 2})
# Merged (Wrong lat and lon)
Frozen({'time': 2191, 'lat': 1266, 'lon': 2121, 'bnds': 2})
```
#### Solution:
To avoid this use the `join='override'` option 
```python
all_files = []

for year in years:
    all_files += glob.glob (fr'{DATAPATH}/{variables[0]}/v20231001/*{year}*' )
    all_files += glob.glob (fr'{DATAPATH}/{variables[1]}/v20231001/*{year}*' )
    
Dataset = xr.open_mfdataset(all_files, chunks={'time': 10}, parallel = True, join='override') 
era.Dataset = Dataset
```

### 3. Variables
Used variables: `zg500`  `ts`

### 4. Date time generation correctly
In the Notebook: `scratch/fp0/mah900/DLWP-CS/NCI-Tutorials/3 - Training a DLWP-CS model BARRA.ipynb`
Date time generations has to match the input data.
```python
train_set = list(pd.date_range('2013-01-01 12:00', '2014-12-31 12:00', freq='1D'))
validation_set = list(pd.date_range('2015-01-01 12:00', '2016-12-31 12:00', freq='1D'))
```


# 2024.02.16

Error-1
```
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
/jobfs/108307996.gadi-pbs/ipykernel_3244111/3801474404.py in <cell line: 4>()
      2 
      3 # Select the samples from the initialization dates. The first "time" input to the model is actually one time step earlier
----> 4 samples = np.array([int(np.where(val_generator.ds['sample'] == s)[0]) for s in initialization_dates]) \
      5     - io_time_steps + 1
      6 time_series = estimator.predict(num_forecast_hours // dt, samples=samples, verbose=1)

/jobfs/108307996.gadi-pbs/ipykernel_3244111/3801474404.py in <listcomp>(.0)
      2 
      3 # Select the samples from the initialization dates. The first "time" input to the model is actually one time step earlier
----> 4 samples = np.array([int(np.where(val_generator.ds['sample'] == s)[0]) for s in initialization_dates]) \
      5     - io_time_steps + 1
      6 time_series = estimator.predict(num_forecast_hours // dt, samples=samples, verbose=1)

TypeError: only size-1 arrays can be converted to Python scalars
```

Try
```
#dates = pd.date_range('2017-01-01', '2018-12-31', freq='7D')
dates = pd.date_range('2017-01-01 12:00', '2018-12-31 12:00', freq='7D')

```

Error-2
```
---------------------------------------------------------------------------
IndexError                                Traceback (most recent call last)
/jobfs/108307996.gadi-pbs/ipykernel_3244111/3801474404.py in <cell line: 6>()
      4 samples = np.array([int(np.where(val_generator.ds['sample'] == s)[0]) for s in initialization_dates]) \
      5     - io_time_steps + 1
----> 6 time_series = estimator.predict(num_forecast_hours // dt, samples=samples, verbose=1)
...

/scratch/fp0/mah900/DLWP-CS/DLWP/model/generators.py in <listcomp>(.0)
    500                     insol.append(
    501                         np.concatenate(
--> 502                             [self.insolation_da.values[samples + self._interval * (self._input_time_steps * s + n),    503                                                        np.newaxis, np.newaxis] for n in range(self._input_time_steps)],
    504                             axis=1

IndexError: index 730 is out of bounds for axis 0 with size 730
```

Try-1 
Try two data sets from ~~3hr~~/
1hr has both : `['zg500', 'ts']`

Kernel died, have to use more RAM in ARE.
Try-2: Dask + 191G, still Out of memory 
```
distributed.worker.memory - WARNING - Worker is at 92% memory usage. Pausing worker.  Process memory: 14.71 GiB -- Worker memory limit: 15.92 GiB
distributed.worker.memory - WARNING - Worker is at 93% memory usage. Pausing worker.  Process memory: 14.89 GiB -- Worker memory limit: 15.92 GiB
distributed.worker.memory - WARNING - Worker is at 93% memory usage. Pausing worker.  Process memory: 14.86 GiB -- Worker memory limit: 15.92 GiB
```

Try-3
Dask + 382G, 
Note: Data loaded but kernel restart afterwards 
```python
Dataset = xr.open_mfdataset(all_files, chunks={'time': 10}, parallel = True, join='override') 
Dataset = Dataset.isel(time=slice(0, None, 3)).load()

Mem: 352.86 GB
CPU times: user 4min 27s, sys: 16min 25s, total: 20min 52s
Wall time: 28min 12s
```
Try (Working - about 113G used)
```python
Dataset = Dataset.isel(time=slice(0, None, 3)) #.load()
```

Error
```bash
2024-02-16 20:30:14.892285: W tensorflow/tsl/framework/bfc_allocator.cc:485] Allocator (GPU_0_bfc) ran out of memory trying to allocate 112.50MiB (rounded to 117964800)requested by op model/cube_sphere_padding2d_1/concat_118
If the cause is memory fragmentation maybe the environment variable 'TF_GPU_ALLOCATOR=cuda_malloc_async' will improve the situation.
```
Try: Restart and run again.  (Working)

# 2024.02.17


# 2024.02.26


# 2024.02.29

Plan make a seperate folder for the BARRA and commit

BARRA
```bash
cd /scratch/fp0/mah900/DLWP-CS
mkdir NCI-Tutorials-BARRA
cd NCI-Tutorials
mv '1 - Downloading and processing BARRA.ipynb' '2 - Remapping to the cubed sphere BARRA.ipynb' '3 - Training a DLWP-CS model BARRA.ipynb' '4 - Predicting with a DLWP-CS model BARRA.ipynb' ../NCI-Tutorials-BARRA
```
BARPA
```bash
cp -r NCI-Tutorials-BARRA NCI-Tutorials-BARPA 
cd NCI-Tutorials-BARPA
mv '1 - Downloading and processing BARRA.ipynb'      '1 - Downloading and processing BARPA.ipynb' 
mv '2 - Remapping to the cubed sphere BARRA.ipynb'   '2 - Remapping to the cubed sphere BARPA.ipynb'
mv '3 - Training a DLWP-CS model BARRA.ipynb'        '3 - Training a DLWP-CS model BARPA.ipynb'
mv '4 - Predicting with a DLWP-CS model BARRA.ipynb' '4 - Predicting with a DLWP-CS model BARPA.ipynb'
```

Commit
```bash
cd /scratch/fp0/mah900/DLWP-CS
git add .
git commit -m "2024.02.29"

```

---
New project created
https://gitlab.com/back2023/gadi/scratch/fp0/mah900/dlwp-cs
Project 'DLWP-CS' was successfully created.
# DLWP-CS

#### The repository for this project is empty
You can get started by cloning the repository or start adding files to it with one of the following options.

##### Command line instructions
You can also upload existing files from your computer using the instructions below.

##### Git global setup
```bash
git config --global user.name "Maruf Ahmed"
git config --global user.email "nci.2022.06@gmail.com"
```
##### Create a new repository
```bash
git clone git@gitlab.com:back2023/gadi/scratch/fp0/mah900/dlwp-cs.git
cd dlwp-cs
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
```
##### Push an existing folder
```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:back2023/gadi/scratch/fp0/mah900/dlwp-cs.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

##### Push an existing Git repository
```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:back2023/gadi/scratch/fp0/mah900/dlwp-cs.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
```
---

First push
```
git remote add back2023 git@gitlab.com:back2023/gadi/scratch/fp0/mah900/dlwp-cs.git
git push --set-upstream back2023 --all
git push --set-upstream back2023 --tags
```

Error
```bash
remote: Resolving deltas: 100% (860/860), done.
remote: GitLab: You are attempting to check in one or more blobs which exceed the 100.0MiB limit:
remote: 
remote: - 0f06bf7896ba3eacc5f50558b691b0c7aa3ca49f (295 MiB)
remote: - f31d7d554cb99b055fdd926b0b684ef21298ef81 (295 MiB)
remote: To resolve this error, you must either reduce the size of the above blobs, or utilize LFS.
remote: You may use "git ls-tree -r HEAD | grep $BLOB_ID" to see the file path.
remote: Please refer to https://gitlab.com/help/user/free_push_limit and
remote: https://gitlab.com/help/administration/settings/account_and_limit_settings
remote: for further information.
To gitlab.com:back2023/gadi/scratch/fp0/mah900/dlwp-cs.git
 ! [remote rejected] master -> master (pre-receive hook declined)
error: failed to push some refs to 'gitlab.com:back2023/gadi/scratch/fp0/mah900/dlwp-cs.git'
```

Try-1
```
Open your project > Settings > Repository and go to "Protected branches", find "master" branch into the list and click "Unprotect" and try again.
```
Source: https://stackoverflow.com/a/28832644
No protected branches. 

Try-2
Exclude files over 100mb
```bash
# .gitignore file already exist.
nano -c .gitignore
# Line 115
# 2023.02.29
*.nc
```

```shell
git rm --cached *.nc
git add .
git commit -m "2024.02.29 - nc files remove"

git push --set-upstream back2023 --all
git push --set-upstream back2023 --tags

```
 Source: https://docs.github.com/en/get-started/getting-started-with-git/ignoring-files
To see all files, use
```bash
tree -a 
```
Git cache has objects that are bigger than 100mb.
```bash
git rm -r --cached .
git gc

git add .
git commit -m "2024.02.29 - git cache clearede"
git push --set-upstream back2023 --all
```
Source: https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-gc.html
Source: https://stackoverflow.com/questions/41863484/clear-git-local-cache 
The same error

find the file
```bash
find . -name "*0f06bf7896ba3eacc5f50558b691b0c7aa3ca49f*"
```
Does not work

Next, Try finding blob
```bash
git cat-file --batch-all-objects --batch-check | grep commit

git cat-file --batch-all-objects --batch-check | grep 0f06bf7896ba3eacc5f50558b691b0c7aa3ca49f

git cat-file --batch-all-objects --batch-check | grep blob
 
```
Source: https://stackoverflow.com/questions/73434864/file-associated-with-a-dangling-blob

Try
```bash
git reflog expire --expire=now --all
git gc --aggressive --prune=now
```
Source: https://stackoverflow.com/questions/31855439/git-delete-a-blob

git log inspect
```bash
[mah900@gadi-login-02 DLWP-CS]$ git log 
commit c2a129daf2043953b42298dfc4647bb96d4cdf8f (HEAD -> master)
Author: Maruf Ahmed <maruf.ahmed@anu.edu.au>
Date:   Thu Feb 29 23:07:36 2024 +1100

    2024.02.29 - nc files remove

commit 0727fa0e582420abd9ca8debe1bfe421a3f76667
Author: Maruf Ahmed <maruf.ahmed@anu.edu.au>
Date:   Thu Feb 29 22:25:35 2024 +1100

    2024.02.29

commit fd15a11a54dedb51a87d176a741294ed0a8e76bd (origin/master, origin/HEAD)
Author: Jonathan Weyn <33530169+jweyn@users.noreply.github.com>
Date:   Sat Oct 3 10:00:55 2020 -0700

    Add JAMES link in README

```

Try
```
git reset --hard HEAD~2
```

So, Again
```bash
cd /scratch/fp0/mah900/DLWP-CS
mkdir NCI-Tutorials-BARRA
cd NCI-Tutorials
mv '1 - Downloading and processing BARRA.ipynb' '2 - Remapping to the cubed sphere BARRA.ipynb' '3 - Training a DLWP-CS model BARRA.ipynb' '4 - Predicting with a DLWP-CS model BARRA.ipynb' ../NCI-Tutorials-BARRA

cd ..
cp -r NCI-Tutorials-BARRA NCI-Tutorials-BARPA 
cd NCI-Tutorials-BARPA
mv '1 - Downloading and processing BARRA.ipynb'      '1 - Downloading and processing BARPA.ipynb' 
mv '2 - Remapping to the cubed sphere BARRA.ipynb'   '2 - Remapping to the cubed sphere BARPA.ipynb'
mv '3 - Training a DLWP-CS model BARRA.ipynb'        '3 - Training a DLWP-CS model BARPA.ipynb'
mv '4 - Predicting with a DLWP-CS model BARRA.ipynb' '4 - Predicting with a DLWP-CS model BARPA.ipynb'
```
**All files gone (Disaster)
Next time be-careful with HARD reset
```
nano -c .gitignore
# Line 115
# 2023.02.29
*.nc
```

```
 git add .
 git commit -m "2024.02.29 - all deleted "
```
***

# 2024.03.04

Rebuild in the next page [2024-03-04]






