

https://github.com/NVIDIA/earth2studio

Earth2Studio is a Python-based package designed to get users up and running with AI weather and climate models _fast_. Our mission is to enable everyone to build, research and explore AI driven meteorology.

**- Earth2Studio Documentation -**

[Install](https://nvidia.github.io/earth2studio/userguide/about/install.html) | [User-Guide](https://nvidia.github.io/earth2studio/userguide/) | [Examples](https://nvidia.github.io/earth2studio/examples/) | [API](https://nvidia.github.io/earth2studio/modules/)

