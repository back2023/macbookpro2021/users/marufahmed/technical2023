
https://blog.research.google/2023/08/weatherbench-2-benchmark-for-next.html

To this end, we are announcing [WeatherBench 2](https://arxiv.org/abs/2308.15560) (WB2), a benchmark for the next generation of data-driven, global weather models. WB2 is an update to the [original benchmark](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2020MS002203) published in 2020, which was based on initial, lower-resolution ML models. The goal of WB2 is to accelerate the progress of data-driven weather models by providing a trusted, reproducible framework for evaluating and comparing different methodologies. The [official website](https://sites.research.google/weatherbench) contains scores from several state-of-the-art models (at the time of writing, these are [Keisler (2022)](https://arxiv.org/abs/2202.07575), an early graph neural network, Google DeepMind’s [GraphCast](https://arxiv.org/abs/2212.12794) and Huawei's [Pangu-Weather](https://www.nature.com/articles/s41586-023-06185-3), a transformer-based ML model). In addition, forecasts from ECMWF’s high-resolution and ensemble forecasting systems are included, which represent some of the best traditional weather forecasting models.


