
https://sites.research.google/weatherbench/


## Participating models

Missing an existing model or have a new model that you'd like to see added? Check out [this guide](https://weatherbench2.readthedocs.io/en/latest/submit.html) for how to participate or submit a [GitHub issue](https://github.com/google-research/weatherbench2/issues). Please also refer to the [FAQ](https://sites.research.google/weatherbench/faq/) for more detailed information.

|Model / Dataset|Source|Method|Type|Initial conditions|Horizontal resolution **|
|---|---|---|---|---|---|
|[ERA5](https://www.ecmwf.int/en/forecasts/dataset/ecmwf-reanalysis-v5#:~:text=ERA5%20is%20the%20fifth%20generation,land%20and%20oceanic%20climate%20variables.)|ECMWF|Physics-based|Reanalysis||0.25°|
|[IFS HRES](https://www.ecmwf.int/en/forecasts/documentation-and-support/medium-range-forecasts)|ECMWF|Physics-based|Forecast (deterministic)|Operational|0.1°|
|[IFS ENS](https://www.ecmwf.int/en/forecasts/documentation-and-support/medium-range-forecasts)|ECMWF|Physics-based|Forecast (50 member ensemble)|Operational|0.2° *|
|[ERA5 forecasts](https://apps.ecmwf.int/data-catalogues/era5/?type=fc&class=ea&stream=oper&expver=1)|ECMWF|Physics-based|Hindcast (deterministic)|ERA5|0.25°|
|[Keisler (2022)](https://arxiv.org/abs/2202.07575)|Ryan Keisler|ML-based|Forecast (deterministic)|ERA5|1°|
|[Pangu-Weather](https://www.nature.com/articles/s41586-023-06185-3)|Huawei|ML-based|Forecast (deterministic)|ERA5|0.25°|
|[GraphCast](https://arxiv.org/abs/2212.12794)|Google DeepMind|ML-based|Forecast (deterministic)|ERA5|0.25°|

 


