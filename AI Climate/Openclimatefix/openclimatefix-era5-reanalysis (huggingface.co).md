

https://huggingface.co/datasets/openclimatefix/era5-reanalysis


This repo contains converted ECMWF ERA5 reanalysis files for both hourly atmospheric and land variables from Jan 2014 to October 2022. The data has been converted from the downloaded NetCDF files into Zarr using Xarray. Each file is 1 day of reanalysis, and so has 24 timesteps at a 0.25 degree grid resolution. All variables in the reanalysis are included here.


