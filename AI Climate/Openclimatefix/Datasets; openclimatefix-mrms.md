
### Dataset Summary

Multi-Radar/Multi-System Precipitation Rate Radar data for 2016-2022. This data contains precipitation rate values for the continental United States.


https://huggingface.co/datasets/openclimatefix/mrms/tree/main/data