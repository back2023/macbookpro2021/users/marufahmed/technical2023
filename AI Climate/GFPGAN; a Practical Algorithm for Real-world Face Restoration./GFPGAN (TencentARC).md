
https://github.com/TencentARC/GFPGAN


## About

GFPGAN aims at developing Practical Algorithms for Real-world Face Restoration.


GFPGAN aims at developing a **Practical Algorithm for Real-world Face Restoration**.  
It leverages rich and diverse priors encapsulated in a pretrained face GAN (_e.g._, StyleGAN2) for blind face restoration.

❓ Frequently Asked Questions can be found in [FAQ.md](https://github.com/TencentARC/GFPGAN/blob/master/FAQ.md).

🚩 **Updates**

- ✅ Add [RestoreFormer](https://github.com/wzhouxiff/RestoreFormer) inference codes.
- ✅ Add [V1.4 model](https://github.com/TencentARC/GFPGAN/releases/download/v1.3.0/GFPGANv1.4.pth), which produces slightly more details and better identity than V1.3.
- ✅ Add **[V1.3 model](https://github.com/TencentARC/GFPGAN/releases/download/v1.3.0/GFPGANv1.3.pth)**, which produces **more natural** restoration results, and better results on _very low-quality_ / _high-quality_ inputs. See more in [Model zoo](https://github.com/TencentARC/GFPGAN#european_castle-model-zoo), [Comparisons.md](https://github.com/TencentARC/GFPGAN/blob/master/Comparisons.md)
- ✅ Integrated to [Huggingface Spaces](https://huggingface.co/spaces) with [Gradio](https://github.com/gradio-app/gradio). See [Gradio Web Demo](https://huggingface.co/spaces/akhaliq/GFPGAN).
- ✅ Support enhancing non-face regions (background) with [Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN).
- ✅ We provide a _clean_ version of GFPGAN, which does not require CUDA extensions.
- ✅ We provide an updated model without colorizing faces.

---

If GFPGAN is helpful in your photos/projects, please help to ⭐ this repo or recommend it to your friends. Thanks😊 Other recommended projects:  
▶️ [Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN): A practical algorithm for general image restoration  
▶️ [BasicSR](https://github.com/xinntao/BasicSR): An open-source image and video restoration toolbox  
▶️ [facexlib](https://github.com/xinntao/facexlib): A collection that provides useful face-relation functions  
▶️ [HandyView](https://github.com/xinntao/HandyView): A PyQt5-based image viewer that is handy for view and comparison



