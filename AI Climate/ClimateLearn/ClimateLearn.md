
**ClimateLearn** is a Python library for accessing state-of-the-art climate data and machine learning models in a standardized, straightforward way. This library provides access to multiple datasets, a zoo of baseline approaches, and a suite of metrics and visualizations for large-scale benchmarking of statistical downscaling and temporal forecasting methods.

https://climatelearn.readthedocs.io/en/latest/index.html


# Metrics

https://climatelearn.readthedocs.io/en/latest/user-guide/metrics.html


# Tasks and Datasets

https://climatelearn.readthedocs.io/en/latest/user-guide/tasks_and_datasets.html#era5-dataset


