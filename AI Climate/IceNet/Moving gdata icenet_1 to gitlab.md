2023.03.03
```bash
git init
...
Initialized empty Git repository in /g/data/wb00/admin/staging/icenet_1/.git/
git add .
```
Error-1. hang, diagnosis. 
```
git gc
git fsck
```
https://stackoverflow.com/questions/43319180/unresponsive-git-status-diff-add-hanging
Try
```
rm -rf .git/

# add 
# .gitignore
data/
```
Reason:
folder contains data of 2.7G, that is what causing the problem
Will use git ignore to avoid the data. 
```
git commit -m "2023.03.03"
```

```bash
cd   #existing_repo
#git remote add origin https://gitlab.com/back2023/gadi/g_/data/admin/staging/icenet_1.git
git remote add origin git@gitlab.com:back2023/gadi/g_/data/admin/staging/icenet_1.git
git branch -M main 
git push -uf origin main
```

