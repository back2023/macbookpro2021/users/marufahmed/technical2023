
https://developer.nvidia.com/blog/modeling-earths-atmosphere-with-spherical-fourier-neural-operators/

Machine learning-based weather prediction has emerged as a promising complement to traditional numerical weather prediction (NWP) models. Models such as [NVIDIA FourCastNet](https://github.com/NVlabs/FourCastNet) have demonstrated that the computational time for generating weather forecasts can be reduced from hours to mere seconds, a significant improvement to current NWP-based workflows.






