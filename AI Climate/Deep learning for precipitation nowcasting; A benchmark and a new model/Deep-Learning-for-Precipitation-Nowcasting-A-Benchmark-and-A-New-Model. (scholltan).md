
# About

Source code of the paper [Deep learning for precipitation nowcasting: A benchmark and a new model](http://papers.nips.cc/paper/7145-deep-learning-for-precipitation-nowcasting-a-benchmark-and-a-new-model)

https://github.com/scholltan/Deep-Learning-for-Precipitation-Nowcasting-A-Benchmark-and-A-New-Model.



