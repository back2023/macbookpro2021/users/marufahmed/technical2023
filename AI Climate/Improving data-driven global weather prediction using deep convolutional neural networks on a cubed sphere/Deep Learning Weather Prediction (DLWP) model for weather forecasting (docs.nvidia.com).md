
https://docs.nvidia.com/deeplearning/modulus/modulus-core/examples/weather/dlwp/readme.html

This example is an implementation of the [DLWP Cubed-sphere](https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2021MS002502) model. The DLWP model can be used to predict the state of the atmosphere given a previous atmospheric state. You can infer a 320-member ensemble set of six-week forecasts at 1.4° resolution within a couple of minutes, demonstrating the potential of AI in developing near real-time digital twins for weather prediction

## [Problem overview](https://docs.nvidia.com/deeplearning/modulus/modulus-core/examples/weather/dlwp/readme.html#problem-overview)

The goal is to train an AI model that can emulate the state of the atmosphere and predict global weather over a certain time span. The Deep Learning Weather Prediction (DLWP) model uses deep CNNs for globally gridded weather prediction. DLWP CNNs directly map u(t) to its future state u(t+Δt) by learning from historical observations of the weather, with Δt set to 6 hr





