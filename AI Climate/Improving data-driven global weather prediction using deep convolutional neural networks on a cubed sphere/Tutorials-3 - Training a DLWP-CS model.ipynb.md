

https://github.com/jweyn/DLWP-CS/blob/master/Tutorials/3%20-%20Training%20a%20DLWP-CS%20model.ipynb


## Training a DLWP-CS model

Now we use the data processed in the previous two notebooks to train a convolutional neural network on weather data mapped to the cubed sphere. We will construct the same convolutional neural network with the cubed sphere as in _Weyn et al. (2020)_, with the exception of having only two variables (Z500 and T2) instead of their four, and without the constant land-sea mask and topography data. This will seem like a fairly involved example but much simpler constructions are also possible using the `DLWPNeuralNet` class instead of the functional Keras API. I also highly recommend having this model train on a GPU with at least 4 GB of video memory.



