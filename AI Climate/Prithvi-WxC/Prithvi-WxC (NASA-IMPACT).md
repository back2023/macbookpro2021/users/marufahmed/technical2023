
https://github.com/NASA-IMPACT/Prithvi-WxC/tree/main


##### 2024.09.24

Note: Initial releaser is last month
Rest 3/2/1 days ago

|   |   |   |
|---|---|---|
|[PrithviWxC](https://github.com/NASA-IMPACT/Prithvi-WxC/tree/main/PrithviWxC "PrithviWxC")|[Removed unused option docstring](https://github.com/NASA-IMPACT/Prithvi-WxC/commit/0dbcf082b910ba2ed5d8ba276c35718e6b506ab9 "Removed unused option docstring<br>Signed-off-by: Will Trojak <w.trojak@ibm.com>")|3 days ago|
|[docs](https://github.com/NASA-IMPACT/Prithvi-WxC/tree/main/docs "docs")|[Update installation.rst](https://github.com/NASA-IMPACT/Prithvi-WxC/commit/e283e1bec0e2977fcd580cff5b51093511b026d5 "Update installation.rst")|3 days ago|
|[examples](https://github.com/NASA-IMPACT/Prithvi-WxC/tree/main/examples "examples")|[Update PrithviWxC_rollout.ipynb](https://github.com/NASA-IMPACT/Prithvi-WxC/commit/8ca32e2b7e0f1800fbf1e1f145d0ac23194059c3 "Update PrithviWxC_rollout.ipynb<br>pull model, climatology and data from hugging face")|2 days ago|
|[.gitignore](https://github.com/NASA-IMPACT/Prithvi-WxC/blob/main/.gitignore ".gitignore")|[Removed DS_Store and added to gitignore](https://github.com/NASA-IMPACT/Prithvi-WxC/commit/fa1344ccc894cc9d950a1567bde65cf81c9fb467 "Removed DS_Store and added to gitignore<br>Signed-off-by: Will Trojak <w.trojak@ibm.com>")|3 days ago|
|[.pre-commit-config.yaml](https://github.com/NASA-IMPACT/Prithvi-WxC/blob/main/.pre-commit-config.yaml ".pre-commit-config.yaml")|[Core model](https://github.com/NASA-IMPACT/Prithvi-WxC/commit/943a6ccfa3a0a63aa8d964eae37fa779f8069f33 "Core model<br>Signed-off-by: Will Trojak <w.trojak@ibm.com>")|5 days ago|
|[LICENSE](https://github.com/NASA-IMPACT/Prithvi-WxC/blob/main/LICENSE "LICENSE")|[Initial commit](https://github.com/NASA-IMPACT/Prithvi-WxC/commit/f69933c8311b708b8dc61b3b3abf218b04559a59 "Initial commit")|last month|
|[README.md](https://github.com/NASA-IMPACT/Prithvi-WxC/blob/main/README.md "README.md")|[citation update](https://github.com/NASA-IMPACT/Prithvi-WxC/commit/a5caa6ca354300325704c9dba46aa6f13fc9beaa "citation update")|yesterday|
|[pyproject.toml](https://github.com/NASA-IMPACT/Prithvi-WxC/blob/main/pyproject.toml "pyproject.toml")|[Updated to include huggingface downloads](https://github.com/NASA-IMPACT/Prithvi-WxC/commit/f5626c9edd02f0bd0b8e25ce512e657250b3eeb0 "Updated to include huggingface downloads<br>Signed-off-by: Will Trojak <w.trojak@ibm.com>")|3 days ago|


