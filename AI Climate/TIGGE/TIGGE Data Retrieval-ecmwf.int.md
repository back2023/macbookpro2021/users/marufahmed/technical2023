

In order to retrieve data from this server, you first have to accept the [licence and terms of use](https://apps.ecmwf.int/datasets/data/tigge/licence/).

https://apps.ecmwf.int/datasets/data/tigge/levtype=sfc/type=cf/