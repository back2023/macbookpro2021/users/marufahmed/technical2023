
THORPEX is a 10-year international research and development programme to accelerate improvements in the accuracy of one-day to two-week high impact weather forecasts  
for the benefit of society, the economy and the environment.

THORPEX establishes an organizational framework that addresses weather research and forecast problems whose solutions will be accelerated through international collaboration among academic institutions, operational forecast centres and users of forecast products.

https://community.wmo.int/en/wwrp-thorpex

