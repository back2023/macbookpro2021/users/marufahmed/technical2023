
The THORPEX Interactive Grand Global Ensemble (TIGGE) is a key component of the THORPEX project, which provides operational global ensemble forecast data quasi-operationally (2-day delay). The TIGGE portals provide the TIGGE data freely only for research and education purposes. For details, visit [the WMO THORPEX website](http://www.wmo.int/pages/prog/arep/wwrp/new/thorpex_new.html) or [the TIGGE website](http://tigge.ecmwf.int/).  
  
The TIGGE Museum is operated for a promotion of utilization of the TIGGE data by [Dr. Mio Matsueda](http://gpvjma.ccs.hpcc.jp/~mio/index.html) (University of Tsukuba). Forecast products in the TIGGE Museum are updated every day with a 2- or 3-day delay, and are available for non-commercial use.

http://gpvjma.ccs.hpcc.jp/TIGGE/


### 01get_TIGGE_dat.sh
http://gpvjma.ccs.hpcc.jp/TIGGE/TIGGE_sample/01get_TIGGE_data.sh.txt


