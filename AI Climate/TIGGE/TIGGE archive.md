
**The TIGGE dataset consists of ensemble forecast data from thirteen global NWP centres, starting from October 2006. TIGGE was established as a key component of [THORPEX](https://community.wmo.int/wwrp-thorpex): a World Weather Research Programme to accelerate the improvements in the accuracy of 1-day to 2 week high-impact weather forecasts for the benefit of humanity. Although the 10-year THORPEX program ended at the end of 2014, TIGGE continued for another 5 years. Currently another stage of TIGGE archive has already been confirmed for further 4 years until the end of 2023.**

https://confluence.ecmwf.int/display/TIGGE

