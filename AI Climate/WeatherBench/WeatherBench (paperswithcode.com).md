
https://paperswithcode.com/dataset/weatherbench

# WeatherBench

Introduced by Rasp et al. in [WeatherBench: A benchmark dataset for data-driven weather forecasting](https://paperswithcode.com/paper/weatherbench-a-benchmark-dataset-for-data)

A benchmark dataset for data-driven medium-range weather forecasting, a topic of high scientific interest for atmospheric and computer scientists alike.


