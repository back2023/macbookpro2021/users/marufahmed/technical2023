
```bash
cp -r pbs_SR pbs_SR_deg




```









### 2023.06.21
```bash
cd /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/2.8125deg
$ qsub /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_wb_tcc.pbs
88487917.gadi-pbs
$ qsub -W depend=afterany:88487917.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_wb_u.pbs
88495301.gadi-pbs
$ qsub -W depend=afterany:88495301.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_wb_u10.pbs
88495556.gadi-pbs
$ qsub -W depend=afterany:88495556.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_wb_v.pbs
88495625.gadi-pbs
$ qsub -W depend=afterany:88495625.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_wb_v10.pbs
88495898.gadi-pbs
$ qsub -W depend=afterany:88495898.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_wb_vo.pbs
88495934.gadi-pbs
qsub -W depend=afterany:88495934.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_wb_z.pbs
88495961.gadi-pbs

```

Error-1
```bash
Traceback (most recent call last):
  File "/scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_regrid_deg.py", line 87, in <module>
    inspect_data = xr.open_mfdataset(in_dir)  
  File "/scratch/fp0/mah900/env/weatherbench/lib/python3.10/site-packages/xarray/backends/api.py", line 948, in open_mfdataset
    raise OSError("no files to open")
OSError: no files to open
```
Solution: 
V10 path has to be set back to `rt52`.
(Delete the previous converted files first)
`/scratch/fp0/mah900/WeatherBench/pbs_SR_deg/config_10m_v_component_of_wind.yml`
```bash
in_dir_prefix: '/g/data/rt52/era5/single-levels/reanalysis/10v'
#in_dir_prefix: '/g/data/wb00/admin/testing/cds_era5/single-levels/reanalysis/10v'
```

Re-run
```bash
#Remove first
rm -r /g/data/wb00/admin/testing/NCI_weatherbench/2.8125deg/10m_v_component_of_wind
# Re-submit
cd /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/2.8125deg/
qsub -W depend=afterany:88495961.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_wb_v10.pbs
88572450.gadi-pbs
```
 
Error-2
```bash
#vo data error
# 1981 have to be downloaded. 
Traceback (most recent call last):
  File "/scratch/fp0/mah900/WeatherBench/pbs_SR_deg/SR_regrid_deg.py", line 87, in <module>
    inspect_data = xr.open_mfdataset(in_dir)  
  File "/scratch/fp0/mah900/env/weatherbench/lib/python3.10/site-packages/xarray/backends/api.py", line 1011, in open_mfdataset
    combined = combine_by_coords(
  File "/scratch/fp0/mah900/env/weatherbench/lib/python3.10/site-packages/xarray/core/combine.py", line 976, in combine_by_coords
    concatenated = _combine_single_variable_hypercube(
  File "/scratch/fp0/mah900/env/weatherbench/lib/python3.10/site-packages/xarray/core/combine.py", line 623, in _combine_single_variable_hypercube
    combined_ids, concat_dims = _infer_concat_order_from_coords(list(datasets))
  File "/scratch/fp0/mah900/env/weatherbench/lib/python3.10/site-packages/xarray/core/combine.py", line 112, in _infer_concat_order_from_coords
    raise ValueError(
ValueError: Coordinate variable level is neither monotonically increasing nor monotonically decreasing on all datasets

```

### tisr
```bash
cd /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/tisr
qsub -W depend=afterany:88572450.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/tisr/SR_wb_tisr.pbs
88595348.gadi-pbs
```

### tp
```bash
cd /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/tp 
qsub -W depend=afterany:88595348.gadi-pbs /scratch/fp0/mah900/WeatherBench/pbs_SR_deg/tp/SR_wb_tp.pbs
88595707.gadi-pbs
```