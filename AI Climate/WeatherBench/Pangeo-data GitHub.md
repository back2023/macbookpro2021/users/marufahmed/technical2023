https://github.com/pangeo-data/WeatherBench
2023.03.09
```bash
cd /scratch/fp0/mah900/
git clone https://github.com/pangeo-data/WeatherBench.git

#In envronmetn file 
#name: weatherbench
...
#  - tensorflow>=2.0
- pip. # Warning-1
```


######  Create Conda env
```bash
cd /scratch/fp0/mah900/WeatherBench
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda env create -p /scratch/fp0/mah900/env/weatherbench -f environment.yml
```
Warning-1
```bash
Warning: you have pip-installed dependencies in your environment file, but you do not list pip itself as one of your conda dependencies.  Conda may not use the correct pip to install your packages, and they may end up in the wrong place.  Please add an explicit pip dependency.  I'm adding one for you, but still nagging you.
```
Error-1
```bash
Solving environment: \ Killed
```
Logged off and tried again, worked

###### Activate
```bash
cd /scratch/fp0/mah900/WeatherBench
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench
```

Install Jupyter lab
```bash
conda install -c conda-forge jupyterlab
```

Allocate CPU and run notebooks
```bash
qsub -I ~/conda/c12_h12.pbs
```

###### ARE
```bash
12
normalbw
xlarge
fp0
#scratch/fp0+gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00+gdata/rt52+gdata/hh5
# Storage updated at 2023.05.15
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00+gdata/rt52+gdata/hh5+scratch/fp0+gdata/rr3

/scratch/fp0/mah900/Miniconda2023/
/scratch/fp0/mah900/env/weatherbench
```



Error-3 
```
ModuleNotFoundError: No module named 'cartopy'
```
Try
https://github.com/SciTools/cartopy/issues/1528#issuecomment-616606302
```
conda install cartopy -c conda-forge
```


### Install MPI4py (Already installed)
https://mpi4py.readthedocs.io/en/stable/install.html
```bash
cd /scratch/fp0/mah900/WeatherBench
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

module load openmpi/4.1.4
python -m pip install --no-cache-dir mpi4py

# Requirement already satisfied: mpi4py in /scratch/fp0/mah900/env/weatherbench/lib/python3.10/site-packages (3.1.4)

# Test
mpiexec -n 2 python -m mpi4py.bench helloworld
# Pass
```

### Install yaml
```
conda install -c conda-forge pyyaml
```

### Install Dask MPI
https://mpi.dask.org/en/stable/install.html
```
conda install dask-mpi -c conda-forge
#test fail
pip install dask_mpi --upgrade --no-cache-dir 
# Similar
```
 
