
New multiprocessing yearly script to replace the error years.

```bash
mkdir /scratch/fp0/mah900/WeatherBench/pbs_mp
cd    /scratch/fp0/mah900/WeatherBench/pbs_mp
```

### 2023.05.25
```bash
cd    /scratch/fp0/mah900/WeatherBench/pbs_mp
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

python MP_regrid_a_year.py #/scratch/fp0/mah900/WeatherBench/pbs_SR/config_10m_v_component_of_wind.yml 10
# worked
python MP_regrid_a_year.py /scratch/fp0/mah900/WeatherBench/pbs_SR/config_10m_v_component_of_wind.yml 37

```

### 2023.05.26
```bash
cd    /scratch/fp0/mah900/WeatherBench/pbs_mp
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

## Did not improve
python MP_regrid_a_year.py /scratch/fp0/mah900/WeatherBench/pbs_SR/config_temperature.yaml 34

```
### 2023.05.27
Error: Multiprocessing hangs if too many Xarray files are open.
Tried: moving the code to `__main__`  section, did not work.
Tried: setting `spawn` , did not work

Reason: Run out of memory, 
Solution: Use a node with more memory and use chunks to reduce mem. 
Used: `chunks={'time':20}`


### 2023.05.28

```bash

. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

# 2m_temperature (None changed)
1980, 1995, 2005, 2007
cd /g/data/wb00/admin/testing/NCI_weatherbench/5.625deg/2m_temperature
rm 2m_temperature_1980_5.625deg.nc 2m_temperature_1995_5.625deg.nc 2m_temperature_2005_5.625deg.nc 2m_temperature_2007_5.625deg.nc

cd    /scratch/fp0/mah900/WeatherBench/pbs_mp
python MP_regrid_a_year.py /scratch/fp0/mah900/WeatherBench/pbs_SR/config_2m_temperature.yml 42

python MP_regrid_a_year.py /scratch/fp0/mah900/WeatherBench/pbs_SR/config_2m_temperature.yml 36

python MP_regrid_a_year.py /scratch/fp0/mah900/WeatherBench/pbs_SR/config_2m_temperature.yml 53

python MP_regrid_a_year.py /scratch/fp0/mah900/WeatherBench/pbs_SR/config_2m_temperature.yml 6


# specific_humidity
1996
# temperature
1979
# v_component_of_wind
1996
```

### 2023.05.30
year 1974, is missing from the `config_10m_v_component_of_wind` folder.
Try again
```bash
cd  /scratch/fp0/mah900/WeatherBench/pbs_mp
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench
(Seems worked)
python MP_regrid_a_year.py /scratch/fp0/mah900/WeatherBench/pbs_SR/config_10m_v_component_of_wind.yml 57
```
Next, run Climax preprocessing again. 

### Error: preprocessing
10m_v, year 2003 (that was re-done) still causing problem (nan value)
1. Did not work with the 12 threads??? Join at the end did not work? ??
See if 1974 worked?? (try from 1959 to 2002)
Otherwise, Try: rewrite the load, using mf_load function.
Otherwise, Try: downloading the year. 

2.   Try: 1959 to 2002: did not work. 
3.   Try: 1975 to 2002: work.
4.   Try: 1974 to 2002: did not work. 
5.   Try: 1959 to 1973: Worked
So, problem is in year 1974 and 2003

### 2023.05.31

Try: `SR_regrid.py` (To check if the multi processing is working or not)

```bash
# Try year 2003


cd  /scratch/fp0/mah900/WeatherBench/pbs_mp
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

rm /g/data/wb00/admin/testing/NCI_weatherbench/5.625deg/10m_v_component_of_wind/10m_v_component_of_wind_2003_5.625deg.nc

python /scratch/fp0/mah900/WeatherBench/pbs_SR/SR_regrid.py "/scratch/fp0/mah900/WeatherBench/pbs_SR/config_10m_v_component_of_wind.yml" "37"
...
[Wed 31 16:37:31]  End rank: 37,  Out file: /g/data/wb00/admin/testing/NCI_weatherbench/5.625deg/10m_v_component_of_wind/10m_v_component_of_wind_2003_5.625deg.nc    ( 188m, 31s)

# Still error
#{'10m_v_component_of_wind': array([nan], dtype=float32),
```
Next, try: Need to download the year


### Downloaded the 10v
Try re-gridding again
```bash
# Try 2003
rm /g/data/wb00/admin/testing/NCI_weatherbench/5.625deg/10m_v_component_of_wind/10m_v_component_of_wind_2003_5.625deg.nc

cd  /scratch/fp0/mah900/WeatherBench/pbs_mp
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench
# Not worked
python MP_regrid_a_year.py /scratch/fp0/mah900/WeatherBench/pbs_SR/config_10m_v_component_of_wind.yml 37 

```

### Reason: using the wrong dir 
All up dates were done in:  `/g/data/wb00/admin/testing/NCI_weatherbench/`
But, Climax preprocessing is reading from `/g/data/wb00/admin/testing/NCI_Climax/`

So, update
```bash
cp /g/data/wb00/admin/testing/NCI_weatherbench/5.625deg/10m_v_component_of_wind/10m_v_component_of_wind_2003_5.625deg.nc /g/data/wb00/admin/testing/NCI_Climax/5.625deg/10m_v_component_of_wind/

cp /g/data/wb00/admin/testing/NCI_weatherbench/5.625deg/10m_v_component_of_wind/10m_v_component_of_wind_1974_5.625deg.nc /g/data/wb00/admin/testing/NCI_Climax/5.625deg/10m_v_component_of_wind/ 

```




