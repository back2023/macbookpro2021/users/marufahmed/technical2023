
https://github.com/raspstephan/WeatherBench


## About

WARNING! This is my working fork! For the actual repo please go to
[github.com/pangeo-data/WeatherBench](https://github.com/pangeo-data/WeatherBench "https://github.com/pangeo-data/WeatherBench")

WARNING: This is my fork. Please go to [https://github.com/pangeo-data/WeatherBench](https://github.com/pangeo-data/WeatherBench) for the official repo.



  
Stephan Raspra
spstephan

https://github.com/raspstephan

Contributed to [google-research/weatherbench2](https://github.com/google-research/weatherbench2), [google/weather-tools](https://github.com/google/weather-tools), [raspstephan/raspstephan.github.io](https://github.com/raspstephan/raspstephan.github.io) and 1 other repository

