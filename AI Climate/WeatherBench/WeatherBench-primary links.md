
## 1. Pangeo-data (2023.03.08)
https://github.com/pangeo-data/WeatherBench
1. Regridding is **the process of interpolating from one grid resolution to a different grid resolution**. This could involve temporal, vertical or spatial ('horizontal') interpolations. However, most commonly, regridding refers to spatial interpolation

### Data processing - raw ERA5 archive
##### `src` directory
1. Download monthly files from the ERA5 reanalysis archive (`src/download.py`)
2. Regrid the raw data to the required resolutions (`src/regrid.py`)

#### Particular variables
##### `Snakemake` and `scripts` dir
1. Because downloading the data can take a long time (several weeks), the workflow is wrapped in [Snakemake](https://snakemake.readthedocs.io/).
2. See `Snakefile` and the configuration files for each variable in `scripts/config_ {variable}.yml`.
   These files can be modified if additional variables are required.
3. To execute Snakemake for a particular variable type : `snakemake -p -j 4 all --configfile scripts/config_toa_incident_solar_radiation.yml`. 

#### Constants 
1. In addition to the time-dependent fields, the constant fields were downloaded and processed using `scripts /download_and_regrid_constants.sh`

### Data processing - CMIP historical climate model data.
1. Use the Snakemake file in `snakemake_configs_CMIP`.
2. Here, we downloaded data from the `MIP-ESM-HR` model. To download other models, search for the download links on the CMIP website and modify the scripts accordingly.


## 2. climetlab
https://climetlab.readthedocs.io/en/0.11.7/examples/11-weatherbench.html



## 3. Stephan Rasp (2023.03.08)
###### WeatherBench: A benchmark dataset for data-driven weather forecasting
https://raspstephan.github.io/blog/weatherbench/#
1. Used the best available data of past weather, the [ERA5 reanalysis archive](https://www.ecmwf.int/en/forecasts/datasets/reanalysis-datasets/era5), and pre-processed it to make it easy to use.
2. The [Github repository](https://github.com/pangeo-data/WeatherBench) contains scripts to download and process data with your own settings.
3. We chose 14 meteorological variables that we thought are most important for a good weather prediction, plus several constant fields like topography.
4. download the weatherbench [data](https://mediatum.ub.tum.de/1524895)

## 4. Anonymous Github
https://anonymous.4open.science/repository/6bb0b0c0-e929-4fa0-a7b7-a42361bcf7dd/README.md
