
Install CDS ( in the previous env) (Working)
Source: https://cds.climate.copernicus.eu/api-how-to
```bash
cd /scratch/fp0/mah900/WeatherBench
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench
pip install cdsapi
```

### 2023.05.28
Install resource usages
Source: https://github.com/jupyter-server/jupyter-resource-usagef
```bash
cd /scratch/fp0/mah900/WeatherBench
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

conda install -c conda-forge jupyter-resource-usage
```