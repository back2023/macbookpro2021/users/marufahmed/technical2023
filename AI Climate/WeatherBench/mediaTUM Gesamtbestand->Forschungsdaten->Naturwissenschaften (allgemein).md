
https://mediatum.ub.tum.de/1524895

###   Title: WeatherBench: A benchmark dataset for data-driven weather forecasting

https://mediatum.ub.tum.de/1524895


### Links:
[Source code on GitHub](https://github.com/pangeo-data/WeatherBench) and [preprint of paper on arxiv.org](https://arxiv.org/abs/2002.00469)
### Key words: 
Reanalysis; ERA5; machine learning
### Technical remarks:
- [View and download (5.8 TB total, 315 files)](https://dataserv.ub.tum.de/index.php/s/m1524895)  
- The data server also offers [downloads with FTP](ftp://m1524895:m1524895@dataserv.ub.tum.de/)  
- The data server also offers downloads with rsync (password m1524895):  
`rsync rsync://m1524895@dataserv.ub.tum.de/m1524895/`