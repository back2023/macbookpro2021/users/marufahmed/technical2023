
# rain-nowcasting-using-deep-learning

This repository aims at expliciting the training procedure of the deep learning model presented in the paper "Merging radar rain images and wind predictions in a deep learning model applied to rain nowcasting". The paper aims at training a neural network to forecast precipitations at short and mid-term horizon by combining rain radar data to predictions of wind speed made by weather forecast models.


https://github.com/VincentBouget/rain-nowcasting-with-fusion-of-rainfall-and-wind-data-article

