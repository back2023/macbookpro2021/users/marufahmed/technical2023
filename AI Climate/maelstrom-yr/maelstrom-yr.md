https://github.com/metno/maelstrom-yr

A dataset plugin for climetlab for the dataset maelstrom-yr. Check out this [notebook](https://github.com/metno/maelstrom-yr/blob/main/notebooks/demo_yr.ipynb).