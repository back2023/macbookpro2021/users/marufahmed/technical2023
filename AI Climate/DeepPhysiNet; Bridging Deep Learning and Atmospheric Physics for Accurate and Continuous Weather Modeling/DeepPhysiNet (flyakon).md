

https://github.com/flyakon/DeepPhysiNet


## About

Implement code of paper "DeepPhysiNet: Bridging Deep Learning and Atmospheric Physics for Accurate and Continuous Weather Modeling"


## Introduction

[](https://github.com/flyakon/DeepPhysiNet#introduction)

This repository is the code implementation of the paper [DeepPhysiNet: Bridging Deep Learning and Atmospheric Physics for Accurate and Continuous Weather Modeling](https://arxiv.org/abs/2401.0412).

The current branch has been tested under PyTorch 2.x and CUDA 12.1, supports Python 3.7+, and is compatible with most CUDA versions.


