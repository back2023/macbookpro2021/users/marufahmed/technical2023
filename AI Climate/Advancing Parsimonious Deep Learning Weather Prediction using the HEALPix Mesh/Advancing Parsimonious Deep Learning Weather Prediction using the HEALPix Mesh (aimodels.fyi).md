
https://www.aimodels.fyi/papers/arxiv/advancing-parsimonious-deep-learning-weather-prediction-using

## Overview

- Presents a deep learning weather prediction model called DLWP-HPX that can forecast 7 atmospheric variables with 3-hour resolution for up to one year
- Uses a coarser resolution and fewer variables than state-of-the-art ML weather forecast models, yet its skill is only about 1 day behind those models and the top numerical weather prediction model
- Reports several model design improvements, including switching to the HEALPix mesh and using gated recurrent units (GRUs) in the U-Net architecture
- Can generate realistic, long-term atmospheric simulations that respect seasonal trends

## Plain English Explanation

The research paper introduces a new deep learning weather prediction model called DLWP-HPX. This model is capable of forecasting 7 key atmospheric variables, such as temperature and wind speed, with high temporal resolution (every 3 hours) for up to a full year into the future.

Remarkably, the DLWP-HPX model achieves this level of performance using fewer input variables and a coarser spatial resolution compared to existing state-of-the-art [machine learning weather forecast models](https://aimodels.fyi/papers/arxiv/pangu-weather-scalable-ai-driven-weather-forecasting) and [numerical weather prediction models](https://aimodels.fyi/papers/arxiv/validating-deep-learning-weather-forecast-models-recent). This means the model is able to make accurate predictions with a simpler and more efficient architecture.

The researchers report several key innovations in the model design that contribute to its strong performance. This includes switching from a cubed sphere to a HEALPix mesh, which allows for more consistent, location-invariant convolution kernels that can effectively propagate weather patterns globally. They also inverted the typical U-Net architecture and incorporated gated recurrent units (GRUs) at each level of the U-Net hierarchy.

Importantly, the model can generate realistic, long-term atmospheric simulations that respect seasonal trends, without losing spectral power over time. This means the model can be run autoregressively for hundreds of steps into the future to produce plausible future weather states.



