

https://github.com/CognitiveModeling/dlwp-hpx



## Getting Started


Create an environment with pyhton installed, e.g., with `conda create -n dlwp-hpx python` and activate it with `conda activate dlwp-hpx`.

Install `dlwp-hpx` package with `pip install .`, favorably in a dedicated environment (as shown above). Then cd into `src/dlwp-hpx`

Verify installation with `python remap/healpix.py`, which projects the exemplary `data/era5_z500.nc` file from the equirectangular LatLon convention of shape [...,𝐻,𝑊] (with 𝐻 the latitudes and 𝑊 the longitudes) into the HEALPix mesh of shape [...,𝐹,𝐻,𝑊], where 𝐹=12 the number of faces and 𝐻=𝑊 the side length of the squares.


