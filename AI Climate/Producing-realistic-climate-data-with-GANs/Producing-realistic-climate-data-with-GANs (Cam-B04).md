https://github.com/Cam-B04/Producing-realistic-climate-data-with-GANs

Associated repository to the article Producing realistic climate data (10.5281/zenodo.4436274)

## Notebooks

In the notebooks the code used to create the figures in the article is available. These notebooks run on the reduced dataset, consequently the figures and the results will be less accurate due to the small sample size.

However, the saved model of the GAN Generator used for the article can be found in the folder `model`.


