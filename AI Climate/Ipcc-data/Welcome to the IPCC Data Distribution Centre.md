

Welcome to the Data Distribution Centre (DDC) of the Intergovernmental Panel on Climate Change (IPCC). The DDC provides a transparent, traceable, stable and accessible archive for the climate, socio-economic and environmental data and scenarios used in the reports and key figures produced by the IPCC.

You can use the search bar above to explore the datasets made available in the data catalogue at this stage. This mainly consists of data associated with the Sixth Assessment Report (AR6)

Our old site continues to offer a wealth of data from previous assessment reports that will be migrated into the data catalogue over time. Access to this site will be maintained during this transition process.

Key Links include
- GCM Input data for previous reports available [here](http://ipcc-data.org/sim/gcm_monthly/)
- Socioeconomic data and scenarios (including AR4 and AR5 observed impacts) available [here](http://sedac.ipcc-data.org/)

For more information about the IPCC DCC, please visit our [About](https://ipcc-data.org/about.html) page.


https://ipcc-data.org/

