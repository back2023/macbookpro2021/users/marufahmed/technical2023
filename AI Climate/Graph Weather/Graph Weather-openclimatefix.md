
PyTorch implementation of Ryan Keisler's 2022 "Forecasting Global Weather with Graph Neural Networks" paper ([https://arxiv.org/abs/2202.07575](https://arxiv.org/abs/2202.07575))


https://github.com/openclimatefix/graph_weather


