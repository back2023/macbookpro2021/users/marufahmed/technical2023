

https://github.com/tpys/FuXi-S2S


## FuXi-S2S

[](https://github.com/tpys/FuXi-S2S#fuxi-s2s)

This is the official repository for the FuXi-S2S paper.

A machine learning model that outperforms conventional global subseasonal forecast models


