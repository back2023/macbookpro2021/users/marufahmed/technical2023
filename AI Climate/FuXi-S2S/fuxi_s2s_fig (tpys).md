

https://github.com/tpys/fuxi_s2s_fig


This the zip file which includes all the relevant raw data from each figure in the main manuscript and in the Supplementary Information for the FuXi-S2S paper.

A machine learning model that outperforms conventional global subseasonal forecast models

by Lei Chen, Xiaohui Zhong, Hao Li, Jie Wu, Bo Lu, Deliang Chen, Shangping Xie, Libo Wu, Qingchen Chao, Chensen Lin, Zixin Hu, Yuan Qi

