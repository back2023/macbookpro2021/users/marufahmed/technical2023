
https://hific.github.io/

## About

We combine Generative Adversarial Networks with learned compression to obtain a state-of-the-art generative lossy compression system. In the paper, we investigate normalization layers, generator and discriminator architectures, training strategies, as well as perceptual losses. In a user study, we show that our method is **preferred to previous state-of-the-art approaches even if they use more than 2× the bitrate.**