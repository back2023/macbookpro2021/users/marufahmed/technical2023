
https://www.personio.com/hr-lexicon/performance-development-review-pdr/


A performance development review (or PDR) is considered to be a critical part of a mature [performance management cycle](https://www.personio.com/hr-lexicon/performance-management-cycles/). But, what is it, how does it work and how does it help your employees grow in their role — moreover, how does it help your organisation succeed? Let’s get into it.


