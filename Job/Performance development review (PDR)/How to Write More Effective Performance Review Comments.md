
https://lattice.com/library/how-to-write-more-effective-performance-review-comments

While [performance reviews](https://lattice.com/library/performance-review-examples-the-essential-guide-for-managers) aim to accomplish a lot of things, one of their most important objectives is to allow managers to [share feedback](https://lattice.com/library/managers-here-s-how-to-give-feedback) with employees. But like any form of feedback, performance review comments must be carefully written to ensure this vital guidance lands well with employees and gives them a clear understanding of what they need to be doing going forward.

Effective performance review comments not only recognize employee strengths and accomplishments but also share helpful advice employees can use to grow and improve. Here’s how to write more effective performance review comments that can re-engage and reinvigorate employees who are eager for feedback and direction.







