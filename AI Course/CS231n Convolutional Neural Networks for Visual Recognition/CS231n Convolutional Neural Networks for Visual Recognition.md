
https://cs231n.github.io/


These notes accompany the Stanford CS class [CS231n: Convolutional Neural Networks for Visual Recognition](http://cs231n.stanford.edu/). For questions/concerns/bug reports, please submit a pull request directly to our [git repo](https://github.com/cs231n/cs231n.github.io).

