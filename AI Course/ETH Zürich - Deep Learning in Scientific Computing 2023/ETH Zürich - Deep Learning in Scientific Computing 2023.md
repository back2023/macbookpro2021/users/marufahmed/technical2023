***
# ETH Zürich DLSC: Course Introduction
<iframe width="1416" height="800" src="https://www.youtube.com/embed/y6wHpRzhhkA?si=UP5gFq6FHhBkh4Nk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
***
# ETH Zürich DLSC: Introduction to Deep Learning Part 1
<iframe width="1416" height="800" src="https://www.youtube.com/embed/q8bAXSBRz3k?si=uuq_mC1WxMlp2ghT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

***
# ETH Zürich DLSC: Introduction to Deep Learning Part 2
<iframe width="1416" height="800" src="https://www.youtube.com/embed/GWjnFVIGwIg?si=y7yxL74bkGPzz2NQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

***
# ETH Zürich DLSC: Physics-Informed Neural Networks - Introduction
width="1416" height="800"
<iframe width="1416" height="800"  src="https://www.youtube.com/embed/Oh1nhCNlqjg?si=zHqCfBySShIK6I-t" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

***
# ETH Zürich DLSC: Physics-Informed Neural Networks - Applications
width="1416" height="800"
<iframe width="1416" height="800" src="https://www.youtube.com/embed/IDIv92Z6Qvc?si=K_HkaIqbf_fxf4zS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
***
# 6. ETH Zürich DLSC: Physics-Informed Neural Networks - Limitations and Extensions
width="1416" height="800"
<iframe width="1416" height="800" src="https://www.youtube.com/embed/FAfVbrufiZM?si=v6CjWuPoiJO_1FhC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

