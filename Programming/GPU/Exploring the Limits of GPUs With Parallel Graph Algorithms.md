https://arxiv.org/abs/1002.4482

Exploring the Limits of GPUs With Parallel Graph Algorithms ∗ (2010)
https://arxiv.org/pdf/1002.4482.pdf