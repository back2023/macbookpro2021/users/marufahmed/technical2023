

The inclusion of NVSHMEM as an experimental back-end in Kokkos allows Kokkos programmers to easily scale their algorithms to multiple GPUs in distributed-memory environments. In this talk we show early results from parallelizing a sparse matrix-vector multiplication on multiple GPUs, discuss the support of important NVSHMEM APIs, and give a brief insight into the current development state.



https://www.nvidia.com/en-us/on-demand/session/supercomputing2020-sc2008/






