

https://stackoverflow.com/questions/35479600/name-is-not-defined-in-a-list-comprehension-with-multiple-loops


You have the order of your loops mixed up; they are considered nested from left to right, so `for r in a[g]` is the _outer_ loop and executed first. Swap out the loops:

```python
print [r['n'] for g in good for r in a[g]]
```

Now `g` is defined for the next loop, `for r in a[g]`, and the expression no longer raises an exception:

```python
>>> a={
...   1: [{'n': 1}, {'n': 2}],
...   2: [{'n': 3}, {'n': 4}],
...   3: [{'n': 5}],
... }
>>> good = [1,2]
>>> [r['n'] for g in good for r in a[g]]
[1, 2, 3, 4]
```


