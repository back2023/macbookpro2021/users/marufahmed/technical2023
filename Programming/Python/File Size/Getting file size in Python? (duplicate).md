

https://stackoverflow.com/questions/6591931/getting-file-size-in-python



Use [`os.path.getsize(path)`](https://docs.python.org/library/os.path.html#os.path.getsize) which will

> Return the size, in bytes, of _path_. Raise [`OSError`](https://docs.python.org/library/exceptions.html#OSError) if the file does not exist or is inaccessible.

```python
import os
os.path.getsize('C:\\Python27\\Lib\\genericpath.py')
```

Or use [`os.stat(path).st_size`](https://docs.python.org/3/library/os.html#os.stat)

```python
import os
os.stat('C:\\Python27\\Lib\\genericpath.py').st_size 
```

Or use [`Path(path).stat().st_size`](https://docs.python.org/library/pathlib.html#pathlib.Path.stat) (Python 3.4+)

```python
from pathlib import Path
Path('C:\\Python27\\Lib\\genericpath.py').stat().st_size
```


