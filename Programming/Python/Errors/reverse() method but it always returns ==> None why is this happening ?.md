


```
my_list = [1, 2, 3, 4, 5]
reverse_list = list(reversed(my_list))
print(reverse_list)
```


```

my_list = [1, 2, 3, 4, 5]
reverse_list = my_list[::-1]
print(reverse_list)

```

https://teamtreehouse.com/community/hi-i-tried-the-reverse-method-but-it-always-returns-none-why-is-this-happening
