
https://www.geeksforgeeks.org/how-to-locate-a-particular-module-in-python/


- In this article, we will see how to locate a particular module in [Python](https://www.geeksforgeeks.org/python-programming-language/). Locating a module means finding the directory from which the module is imported. When we import a module the Python interpreter searches for the module in the following manner:
	- First, it searches for the module in the current directory.
	- If the module isn’t found in the current directory, Python then searches each directory in the shell variable [PYTHONPATH](https://www.geeksforgeeks.org/pythonpath-environment-variable-in-python/). The [PYTHONPATH](https://www.geeksforgeeks.org/pythonpath-environment-variable-in-python/) is an environment variable, consisting of a list of directories.
	- If that also fails python checks the installation-dependent list of directories configured at the time Python is installed.

- The [sys.path](https://www.geeksforgeeks.org/sys-path-in-python/) contains the list of the current directory, PYTHONPATH, and the installation-dependent default. We will discuss how to use this and other methods to locate the module in this article.


