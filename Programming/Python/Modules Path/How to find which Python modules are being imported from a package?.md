
https://www.tutorialspoint.com/How-to-find-which-Python-modules-are-being-imported-from-a-package


## Using List Comprehension

```python
import sys 
output = [module.__name__ for module in sys.modules.values() if module]
output = sorted(output) 
print('The list of imported Python modules are :',output)
```


## Using pip freeze command

```bash
C:\Users\Lenovo>pip freeze
```


## Using dir() method
```python
module = dir() 
print('The list of imported Python modules are :',module)
```


## Using inspect.getmember() and a Lambda


```python
import inspect 
import os 
modules = inspect.getmembers(os) results = filter(lambda m: inspect.ismodule(m[1]), modules) 
for o in results: 
	print('The list of imported Python modules are :',o)
```


## Using sys module

```python
from datetime import datetime 
import sys 
print (sys.modules.keys())
```