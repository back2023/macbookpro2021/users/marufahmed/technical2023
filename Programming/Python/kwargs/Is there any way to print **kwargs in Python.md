
https://stackoverflow.com/questions/39623889/is-there-any-way-to-print-kwargs-in-python


```python
def mixAndMatch(*args, **kwargs):
    print(f' Args: {args}' )
    print(f' Kwargs: {kwargs}' )

mixAndMatch('one', 'two', arg3 = 'three', arg4 = 'four’)


>>>
Args: ('one', 'two')
Kwargs: {'arg3': 'three', 'arg4': 'four'}
```


