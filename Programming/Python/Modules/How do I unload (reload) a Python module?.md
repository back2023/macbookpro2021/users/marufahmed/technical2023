
https://stackoverflow.com/questions/437589/how-do-i-unload-reload-a-python-module


TL;DR:

Python ≥ 3.4: `importlib.reload(module)`


```python
import importlib
importlib.reload(module)
```