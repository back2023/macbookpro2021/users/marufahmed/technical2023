
https://github.com/ClimateGlobalChange/tempestremap


 
<iframe src="https://github.com/ClimateGlobalChange/tempestremap" style="width:100%; height:500px;" ></iframe>
 
# TempestRemap

Author: Paul Ullrich Email: [paullrich@ucdavis.edu](mailto:paullrich@ucdavis.edu)

## Overview

TempestRemap is a conservative, consistent and monotone remapping package for arbitrary grid geometry with support for finite volumes and finite elements. There is still quite a bit of work to be done, but any feedback is appreciated on the software in its current form.

If you choose to use this software in your work, please cite our papers:

Paul A. Ullrich and Mark A. Taylor, 2015: Arbitrary-Order Conservative and Consistent Remapping and a Theory of Linear Maps: Part 1. Mon. Wea. Rev., 143, 2419–2440, doi: 10.1175/MWR-D-14-00343.1

Paul A. Ullrich, Darshi Devendran and Hans Johansen, 2016: Arbitrary-Order Conservative and Consistent Remapping and a Theory of Linear Maps, Part 2. Mon. Weather Rev., 144, 1529-1549, doi: 10.1175/MWR-D-15-0301.1.



### Embed page markdown
```
<iframe src="https://codepen.io/team/codepen/embed/preview/PNaGbb" style="width:100%; height:300px;" ></iframe>
```
Source: https://css-tricks.com/embedded-content-in-markdown/

