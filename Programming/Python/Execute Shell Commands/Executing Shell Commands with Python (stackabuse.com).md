
https://stackabuse.com/executing-shell-commands-with-python/




| os.system|subprocess.run|subprocess.Popen |
|---|---|---|---|
|Requires parsed arguments|no|yes|yes|
|Waits for the command|yes|yes|no|
|Communicates with stdin and stdout|no|yes|yes|
|Returns|return value|object|object|








