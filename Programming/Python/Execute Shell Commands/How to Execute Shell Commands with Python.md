
https://janakiev.com/blog/python-shell-commands/


## Table of Contents
- [Using the os Module](https://janakiev.com/blog/python-shell-commands/#using-the-os-module)
- [Using the subprocess Module](https://janakiev.com/blog/python-shell-commands/#using-the-subprocess-module)
- [Conclusion](https://janakiev.com/blog/python-shell-commands/#conclusion)
- [Resources](https://janakiev.com/blog/python-shell-commands/#resources)



# Using the `os` Module
- The first and the most straight forward approach to run a shell command is by using [os.system()](https://docs.python.org/3/library/os.html#os.system):
```python
import os
os.system('ls -l')
```

- Note, that if you run this function in Jupyter notebook, you won’t have an output inline. Instead you the inline output will be the return code of the executed programm (`0` for successful and `-1` for unsuccessful). 
- You will find the output in the command line where you have started Jupyter notebook.

- Next, the [os.popen()](https://docs.python.org/3/library/os.html#os.popen) command opens a pipe from or to the command line. 
- This means that we can access the stream within Python. 
- This is useful since you can now get the output as a variable:
```python
import os
stream = os.popen('echo Returned output')
output = stream.read()
output
```
When you use the `.read()` function, you will get the whole output as one string. You can also use the `.readlines()` function, which splits each line (including a trailing `\n`).


# Using the `subprocess` Module

- The [subprocess](https://docs.python.org/3/library/subprocess.html#module-subprocess) module provides more powerful facilities for spawning new processes and retrieving their results; using that module is preferable to using this function.
- The main function you want to keep in mind if you use Python >= 3.5 is [subprocess.run()](https://docs.python.org/3/library/subprocess.html#subprocess.run)
- The [subprocess.Popen()](https://docs.python.org/3/library/subprocess.html#subprocess.Popen) class is responsible for the creation and management of the executed process. 
	- In contrast to the previous functions, this class executes only a single command with arguments as a list.
```python
import subprocess
process = subprocess.Popen(['echo', 'More output'],
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
stdout, stderr = process.communicate()
stdout, stderr
```

- You’ll notice that we set `stdout` and `stderr` to [subprocess.PIPE](https://docs.python.org/3/library/subprocess.html#subprocess.PIPE). This is a special value that indicates to `subprocess.Popen` that a pipe should be opened that you can then read with the `.communicate()` function. 
- It is also possible to use a file object as with:
```python
with open('test.txt', 'w') as f:
    process = subprocess.Popen(['ls', '-l'], stdout=f)
```

- When you run `.communicate()`, it will wait until the process is complete. 
- However if you have a long program that you want to run and you want to continuously check the status in realtime while doing something else, you can do this like here:
```python
process = subprocess.Popen(['ping', '-c 4', 'python.org'], 
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

while True:
    output = process.stdout.readline()
    print(output.strip())
    # Do something else
    return_code = process.poll()
    if return_code is not None:
        print('RETURN CODE', return_code)
        # Process has finished, read rest of the output 
        for output in process.stdout.readlines():
            print(output.strip())
        break
```

- You can use the `.poll()` function to check the return code of the process. It will return `None` while the process is still running. 
- To get the output, you can use `process.stdout.readline()` to read a single line.
- Conversely, when you use `process.stdout.readlines()`, it reads all lines and it also waits for the process to finish if it has not finished yet.

- Also note, that you won’t need quotations for arguments with spaces in between like `'\"More output\"'`. 
- If you are unsure how to tokenize the arguments from the command, you can use the [shlex.split()](https://docs.python.org/3/library/shlex.html#shlex.split) function:
```python
import shlex
shlex.split("/bin/prog -i data.txt -o \"more data.txt\"")
```


- You have also the [subprocess.call()](https://docs.python.org/3/library/subprocess.html#subprocess.call) function to your disposal which works like the `Popen` class, but it waits until the command completes and gives you the return code as in `return_code = subprocess.call(['echo', 'Even more output'])`. 
	- The recommended way however is to use [subprocess.run()](https://docs.python.org/3/library/subprocess.html#subprocess.run) which works since Python 3.5.
	- It has been added as a simplification of `subprocess.Popen`. 
	- The function will return a [subprocess.CompletedProcess](https://docs.python.org/3/library/subprocess.html#subprocess.CompletedProcess) object:
```python
process = subprocess.run(['echo', 'Even more output'], 
                         stdout=subprocess.PIPE, 
                         universal_newlines=True)
process
```

```python
CompletedProcess(args=['echo', 'Even more output'], returncode=0, stdout='Even more output\n')
```

You can now find the resulting output in this variable:
```python
process.stdout

'Even more output\n'
```


- Similar to `subprocess.call()` and the previous `.communicate()` function, it will wait untill the process is completed. 
- Finally, here is a more advanced example on how to access a server with ssh and the `subprocess` module:
```python
import subprocess

ssh = subprocess.Popen(["ssh", "-i .ssh/id_rsa", "user@host"],
                        stdin =subprocess.PIPE,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                        universal_newlines=True,
                        bufsize=0)
 
# Send ssh commands to stdin
ssh.stdin.write("uname -a\n")
ssh.stdin.write("uptime\n")
ssh.stdin.close()

# Fetch output
for line in ssh.stdout:
    print(line.strip())
```
- Here you can see how to write input to the process. In this case you need to set the `bufsize=0` in order to have unbuffered output. 
- After you are finished writing to the `stdin`, you need to close the connection.