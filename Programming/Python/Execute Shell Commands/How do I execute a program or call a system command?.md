
https://stackoverflow.com/questions/89228/how-do-i-execute-a-program-or-call-a-system-command

How do I call an external command within Python as if I had typed it in a shell or command prompt?
***

Use [`subprocess.run`](https://docs.python.org/library/subprocess.html#subprocess.run):

```python
import subprocess

subprocess.run(["ls", "-l"]) 
```

- Another common way is [`os.system`](https://docs.python.org/library/os.html#os.system) but you shouldn't use it because it is unsafe if any parts of the command come from outside your program or can contain spaces or other special characters, 
- also `subprocess.run` is generally more flexible (you can get the [`stdout`](https://docs.python.org/library/subprocess.html#subprocess.CompletedProcess.stdout), [`stderr`](https://docs.python.org/library/subprocess.html#subprocess.CompletedProcess.stderr), the ["real" status code](https://docs.python.org/library/subprocess.html#subprocess.CompletedProcess.returncode), better [error handling](https://docs.python.org/library/subprocess.html#subprocess.CalledProcessError), etc.). 
- Even the [documentation for `os.system`](https://docs.python.org/library/os.html#os.system) recommends using `subprocess` instead.

***

Here is a summary of ways to call external programs, including their advantages and disadvantages:
1. [`os.system`](https://docs.python.org/3/library/os.html#os.system) passes the command and arguments to your system's shell. 
   - This is nice because you can actually run multiple commands at once in this manner and set up pipes and input/output redirection.
```python
os.system("some_command < input_file | another_command > output_file")  
```
- However, while this is convenient, you have to manually handle the escaping of shell characters such as spaces, et cetera. 
- On the other hand, this also lets you run commands which are simply shell commands and not actually external programs.

2. [`os.popen`](https://docs.python.org/3/library/os.html#os.popen) will do the same thing as `os.system` except that it gives you a file-like object that you can use to access standard input/output for that process.
   - There are 3 other variants of popen that all handle the i/o slightly differently. 
   - If you pass everything as a string, then your command is passed to the shell; 
   - if you pass them as a list then you don't need to worry about escaping anything.
```python
print(os.popen("ls -l").read())
```

3. [`subprocess.Popen`](https://docs.python.org/3/library/subprocess.html#subprocess.Popen). This is intended as a replacement for `os.popen`, but has the downside of being slightly more complicated by virtue of being so comprehensive.
```python
print subprocess.Popen("echo Hello World", shell=True, stdout=subprocess.PIPE).stdout.read()
```
instead of
```python
print os.popen("echo Hello World").read()
```
- but it is nice to have all of the options there in one unified class instead of 4 different popen functions. See [the documentation](https://docs.python.org/3/library/subprocess.html#popen-constructor).

4. [`subprocess.call`](https://docs.python.org/3/library/subprocess.html#subprocess.call). This is basically just like the `Popen` class and takes all of the same arguments, but it simply waits until the command completes and gives you the return code.
```python
return_code = subprocess.call("echo Hello World", shell=True)
```

5.  [`subprocess.run`](https://docs.python.org/3/library/subprocess.html#subprocess.run). Python 3.5+ only. Similar to the above but even more flexible and returns a [`CompletedProcess`](https://docs.python.org/3/library/subprocess.html#subprocess.CompletedProcess) object when the command finishes executing.

6.  `os.fork`, `os.exec`, `os.spawn` are similar to their C language counterparts, but I don't recommend using them directly.

- The `subprocess` module should probably be what you use.

***

```python
import subprocess

p = subprocess.Popen('ls', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
for line in p.stdout.readlines():
    print line,
retval = p.wait()
```
