
https://github.com/nschloe/tuna

tuna is a modern, lightweight Python profile viewer inspired by [SnakeViz](https://github.com/jiffyclub/snakeviz). It handles runtime and import profiles, has minimal dependencies, uses [d3](https://d3js.org/) and [bootstrap](https://getbootstrap.com/), and avoids [certain](https://github.com/jiffyclub/snakeviz/issues/111) [errors](https://github.com/jiffyclub/snakeviz/issues/112) present in SnakeViz (see below) and is faster, too.


