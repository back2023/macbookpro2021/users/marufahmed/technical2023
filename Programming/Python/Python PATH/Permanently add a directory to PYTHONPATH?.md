
https://stackoverflow.com/questions/3402168/permanently-add-a-directory-to-pythonpath


If you're using bash (on a Mac or GNU/Linux distro), add this to your `~/.bashrc`

```python
export PYTHONPATH="${PYTHONPATH}:/my/other/path"
```


