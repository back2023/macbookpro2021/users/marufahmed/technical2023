
https://realpython.com/add-python-to-path/

You may need to add Python to `PATH` if you’ve installed Python, but typing `python` on the command line doesn’t seem to work. You may be getting a message saying that the term `python` isn’t recognized, or you may end up with the wrong version of Python running.

A common fix for these problems is adding Python to the `PATH` [environment variable](https://en.wikipedia.org/wiki/Environment_variable). In this tutorial, you’ll learn how to add Python to `PATH`. You’ll also learn about what `PATH` is and why `PATH` is vital for programs like the command line to be able to find your Python installation.

**Note:** A [path](https://en.wikipedia.org/wiki/Path_(computing)) is the address of a file or folder on your hard drive. The `PATH` environment variable, also referred to as just `PATH` or _Path_, is a list of paths to directories that your operating system keeps and uses to find executable scripts and programs.

The steps that you’ll need to take to add something to `PATH` will depend significantly on your operating system (OS), so be sure to skip to the relevant section if you’re only interested in this procedure for one OS.

Note that you can use the following steps to add any program to `PATH`, not just Python.




