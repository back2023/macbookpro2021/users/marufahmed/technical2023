
https://pythonprogramming.org/automatically-building-python-package-using-github-actions/

Are you tired of manually building and deploying Python packages to `PyPi`? In this post, we are going to talk about the solution in detail. By the end of the post, you will not have to build your package again.






