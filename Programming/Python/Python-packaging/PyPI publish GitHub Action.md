
https://github.com/marketplace/actions/pypi-publish

This action allows you to upload your [Python distribution packages](https://packaging.python.org/glossary/#term-Distribution-Package) in the `dist/` directory to PyPI. This text suggests a minimalistic usage overview. For more detailed walkthrough check out the [PyPA guide](https://packaging.python.org/guides/publishing-package-distribution-releases-using-github-actions-ci-cd-workflows/).

If you have any feedback regarding specific action versions, please leave comments in the corresponding [per-release announcement discussions](https://github.com/pypa/gh-action-pypi-publish/discussions/categories/announcements).






