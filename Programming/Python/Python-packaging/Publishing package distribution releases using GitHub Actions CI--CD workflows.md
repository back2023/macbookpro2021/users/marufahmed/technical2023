
https://packaging.python.org/en/latest/guides/publishing-package-distribution-releases-using-github-actions-ci-cd-workflows/


[GitHub Actions CI/CD](https://github.com/features/actions) allows you to run a series of commands whenever an event occurs on the GitHub platform. One popular choice is having a workflow that’s triggered by a `push` event. This guide shows you how to publish a Python distribution whenever a tagged commit is pushed. It will use the [pypa/gh-action-pypi-publish GitHub Action](https://github.com/marketplace/actions/pypi-publish) for publishing. It also uses GitHub’s [upload-artifact](https://github.com/actions/upload-artifact) and [download-artifact](https://github.com/actions/download-artifact) actions for temporarily storing and downloading the source packages.

**Attention**
This guide _assumes_ that you already have a project that you know how to build distributions for and _it lives on GitHub_. This guide also avoids details of building platform specific projects. If you have binary components, check out [cibuildwheel](https://packaging.python.org/en/latest/key_projects/#cibuildwheel)’s GitHub Action examples.



