
https://docs.github.com/en/github-ae@latest/packages/learn-github-packages/publishing-a-package

## [About published packages](https://docs.github.com/en/github-ae@latest/packages/learn-github-packages/publishing-a-package#about-published-packages)

You can help people understand and use your package by providing a description and other details like installation and usage instructions on the package page. GitHub AE provides metadata for each version, such as the publication date, download activity, and recent versions. For an example package page, see [@Codertocat/hello-world-npm](https://github.com/Codertocat/hello-world-npm/packages/10696?version=1.0.1).

You can publish packages in an internal repository (internal packages) to share with everyone on your enterprise, or in a private repository (private packages) to share with collaborators or an organization. A repository can be connected to more than one package. To prevent confusion, make sure the README and description clearly provide information about each package.






