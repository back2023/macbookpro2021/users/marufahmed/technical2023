
https://realpython.com/what-is-pip/


### Running `pip` as a Module

- When you run your system `pip` directly, the command itself doesn’t reveal which Python version `pip` belongs to.
- This unfortunately means that you could use `pip` to install a package into the site-packages of an old Python version without noticing. 
- To prevent this from happening, you can run `pip` as a Python module:
```python
python3 -m pip
```
- Notice that you use `python3 -m` to run `pip`. The `-m` switch tells Python to run a module as an executable of the `python3` interpreter.



### Using a Custom Package Index
- By default, `pip` uses PyPI to look for packages. 
	- But `pip` also gives you the option to define a custom package index.
	
- Using `pip` with a custom index can be helpful when the PyPI domain is blocked on your network or if you want to work with packages that aren’t publicly available. 
- Sometimes system administrators also create their own internal package index to better control which package versions are available to `pip` users on the company’s network.
- A custom package index must comply with [PEP 503 – Simple Repository API](https://www.python.org/dev/peps/pep-0503/) to work with `pip`.
- Any custom index that follows the same API can be targeted with the `--index-url` option. 
	- Instead of typing `--index-url`, you can also use the `-i` shorthand.
- For example, to install the [`rptree`](https://realpython.com/directory-tree-generator-python/) tool from the [TestPyPI](https://test.pypi.org/) package index, you can run the following command:
```bash
(venv) C:\> python -m pip install -i https://test.pypi.org/simple/ rptree
```

- If you need to use an alternative index permanently, then you can set the `index-url` option in the `pip` [configuration file](https://pip.pypa.io/en/stable/topics/configuration/).
- This file is called `pip.conf`, and you can find its location by running the following command:
```bash
(venv) C:\> python -m pip config list -vv
```


### Installing Packages From Your GitHub Repositories
- `pip` also provides the option to install packages from a [GitHub repository](https://realpython.com/python-git-github-intro/).
```bash
(venv) C:\> python -m pip install git+https://github.com/realpython/rptree
```
- With the `git+https` scheme, you can point to a Git repository that contains an installable package.

### Installing Packages in Editable Mode to Ease Development
- When working on your own package, installing it in an editable mode can make sense.
-  A typical workflow is to first clone the repository and then use `pip` to install it as an editable package in your environment:
```bash
$ git clone https://github.com/realpython/rptree
$ cd rptree
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ python3 -m pip install -e .
```
- The `-e` option is shorthand for the `--editable` option. When you use the `-e` option with `pip install`, you tell `pip` that you want to install the package in editable mode. 
- Instead of using a package name, you use a dot (`.`) to point `pip` to the current directory.
- When you install a package in editable mode, you’re creating a link in the site-packages to the local project path:
```bash
 ~/rptree/venv/lib/python3.10/site-packages/rptree.egg-link
```


### Pinning Requirements
- Running `pip help` shows that there’s a `freeze` command that outputs the installed packages in requirements format.


### Fine-Tuning Requirements
- Open `requirements.txt` in your favorite [editor](https://realpython.com/python-ides-code-editors-guide/) and turn the equality operators (`==`) into greater than or equal to operators (`>=`)
- You can change the [comparison operator](https://realpython.com/python-operators-expressions/#comparison-operators) to `>=` to tell `pip` to install an exact or greater version that has been published.


## Exploring Alternatives to `pip`

Here are some other package management tools that are available for Python:

|Tool|Description|
|---|---|
|[Conda](https://conda.io/en/latest/)|**Conda** is a package, dependency, and environment manager for many languages, including Python. It comes from [Anaconda](https://www.anaconda.com/), which started as a data science package for Python. Consequently, it’s widely used for data science and [machine learning applications](https://realpython.com/python-windows-machine-learning-setup/). Conda operates its own [index](https://repo.continuum.io/) to host compatible packages.|
|[Poetry](https://python-poetry.org/)|**Poetry** will look very familiar to you if you’re coming from [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) and [npm](https://www.npmjs.com/). Poetry goes beyond [package management](https://realpython.com/dependency-management-python-poetry/), helping you build distributions for your applications and libraries and deploying them to PyPI.|
|[Pipenv](https://github.com/pypa/pipenv)|**Pipenv** is another package management tool that merges virtual environment and package management in a single tool. [Pipenv: A Guide to the New Python Packaging Tool](https://realpython.com/pipenv-guide/) is a great place to start learning about Pipenv and its approach to package management.|






