

https://timothybramlett.com/How_to_create_a_Python_Package_with___init__py.html


## What is a Python package?

A Python [package](http://programmers.stackexchange.com/questions/111871/module-vs-package) is simply an organized collection of python modules. A python [module](https://docs.python.org/3/tutorial/modules.html) is simply a single python file.

