
https://python-packaging.readthedocs.io/en/latest/dependencies.html

- Fortunately, setuptools makes it easy for us to specify those dependencies (assuming they are packaged correctly) and automatically install them when our packages is installed.
- We can add some formatting spice to the **funniest** joke with [Markdown](http://pypi.python.org/pypi/Markdown/).
- In `text.py`:
```python
from markdown import markdown

def joke():
    return markdown(u'Wenn ist das Nunst\u00fcck git und Slotermeyer?'
                    u'Ja! ... **Beiherhund** das Oder die Flipperwaldt '
                    u'gersput.')
```
- Now our package depends on the `markdown` package. To note that in `setup.py`, we just add an `install_requires` keyword argument:
```python
from setuptools import setup

setup(name='funniest',
      version='0.1',
      description='The funniest joke in the world',
      url='http://github.com/storborg/funniest',
      author='Flying Circus',
      author_email='flyingcircus@example.com',
      license='MIT',
      packages=['funniest'],
      install_requires=[
          'markdown',
      ],
      zip_safe=False)
```

- To prove this works, we can run `python setup.py develop` again, and we’ll see: `$ python setup.py develop` 
- When we publish this to PyPI, calling `pip install funniest` or similar will also install `markdown`.

## Packages Not On PyPI
- Sometimes you’ll want to use packages that are properly arranged with setuptools, but aren’t published to PyPI. 
- In those cases, you can specify a list of one or more `dependency_links` URLs where the package can be downloaded, along with some additional hints, and setuptools will find and install the package correctly.
- For example, if a library is published on GitHub, you can specify it like:
```python
setup(
    ...
    dependency_links=['http://github.com/user/repo/tarball/master#egg=package-1.0']
    ...
)
```
  