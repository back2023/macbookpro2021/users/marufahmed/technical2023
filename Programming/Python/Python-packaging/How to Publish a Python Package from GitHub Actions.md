
https://www.seanh.cc/2022/05/21/publishing-python-packages-from-github-actions/


A simple way to use [GitHub Actions](https://github.com/actions) to build your Python package, bump the version number, and publish it to [GitHub releases](https://github.com/seanh/gha-python-packaging-demo/releases) and [PyPI.org](https://pypi.org/project/gha-python-packaging-demo/) all with a single click of a button in the web interface.






