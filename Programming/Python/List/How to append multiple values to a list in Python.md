
https://stackoverflow.com/questions/20196159/how-to-append-multiple-values-to-a-list-in-python

You can use the [sequence method `list.extend`](https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types) to extend the list by multiple values from any kind of iterable, being it another list or any other thing that provides a sequence of values.

So you can use `list.append()` to append _a single_ value, and `list.extend()` to append _multiple_ values.