
https://stackoverflow.com/questions/17856928/how-to-terminate-process-from-python-using-pid

"Using the awesome [`psutil`](https://github.com/giampaolo/psutil) library it's pretty simple:

```python
p = psutil.Process(pid)
p.terminate()  #or p.kill()
```

If you don't want to install a new library, you can use the `os` module:

```python
import os
import signal

os.kill(pid, signal.SIGTERM) #or signal.SIGKILL 
```

See also the [`os.kill` documentation](https://docs.python.org/3/library/os.html#os.kill)."