
Multi-dimensional arrays with broadcasting and lazy computing.

# Introduction[¶](https://xtensor.readthedocs.io/en/latest/index.html#introduction "Permalink to this heading")

_xtensor_ is a C++ library meant for numerical analysis with multi-dimensional array expressions.

_xtensor_ provides
- an extensible expression system enabling **lazy broadcasting**.
- an API following the idioms of the **C++ standard library**.
- tools to manipulate array expressions and build upon _xtensor_.

Containers of _xtensor_ are inspired by [NumPy](http://www.numpy.org/), the Python array programming library. **Adaptors** for existing data structures to be plugged into the expression system can easily be written.

In fact, _xtensor_ can be used to **process NumPy data structures in-place** using Python’s [buffer protocol](https://docs.python.org/3/c-api/buffer.html). For more details on the NumPy bindings, check out the [xtensor-python](https://github.com/xtensor-stack/xtensor-python) project. Language bindings for R and Julia are also available.

_xtensor_ requires a modern C++ compiler supporting C++14. The following C++ compilers are supported:
- On Windows platforms, Visual C++ 2015 Update 2, or more recent
- On Unix platforms, gcc 4.9 or a recent version of Clang


https://xtensor.readthedocs.io/en/latest/index.html