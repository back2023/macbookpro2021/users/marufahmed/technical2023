
This tutorial will introduce how to utilize `map-by` option to deal with many complex scenarios such as running a hybrid MPI program (mixture of OpenMP and MPI).

https://nekodaemon.com/2021/02/05/Understanding-MPI-map-by-and-bind-to-option/