

This chapter describes the options to the mpirun command that are used for distributed resource management, and provides instructions for each resource manager. It contains the following sections:

- [mpirun Options for Third-Party Resource Manager Integration](https://docs.oracle.com/cd/E19923-01/820-6793-10/ExecutingBatchPrograms.html#50524185_88974)

- [Running Parallel Jobs in the PBS Environment](https://docs.oracle.com/cd/E19923-01/820-6793-10/ExecutingBatchPrograms.html#50524185_85531)

- [Running Parallel Jobs in the Sun Grid Engine Environment](https://docs.oracle.com/cd/E19923-01/820-6793-10/ExecutingBatchPrograms.html#50524185_58140)

---

mpirun Options for Third-Party Resource Manager Integration

ORTE is compatible with a number of other launchers, including rsh/ssh, Sun Grid Engine, and PBS.

---
|**Note -** Open MPI itself supports other third-party launchers supported by Open MPI, such as SLURM and Torque. However, these launchers are currently not supported in Sun HPC ClusterTools software. To use these other third-party launchers, you must download the Open MPI source, compile, and link with the libraries for the launchers.|

---

https://docs.oracle.com/cd/E19923-01/820-6793-10/ExecutingBatchPrograms.html