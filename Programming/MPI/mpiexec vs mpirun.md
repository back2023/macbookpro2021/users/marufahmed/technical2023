

`mpiexec` is defined in the MPI standard (well, the recent versions at least) and I refer you to those (your favourite search engine will find them for you) for details.

`mpirun` is a command implemented by many MPI implementations. It has never, however, been standardised and there have always been, often subtle, differences between implementations. For details see the documentation of the implementation(s) of your choice.

And yes, they are both used to launch MPI programs, these days `mpiexec` is generally preferable because it is standardised.

https://stackoverflow.com/questions/25287981/mpiexec-vs-mpirun#25289126

