

I am using mpirun (Open MPI) 4.0.0, install from a source(./configure->make->make install)  
and running on centos 7.4

The problem is , i use mpirun to execute mdtest on 2 nodes, but it is working for np 2,and not working for np 4,the command i was running is :  
[root@node2 mdtest]# mpirun --allow-run-as-root --mca btl_openib_allow_ib 1 -host node2:100,node3:100 -np 4 mdtest -C -r -d /mnt/mdtest1 -i 3 -I 1000 -z 2 -b 8 -L -u

I got the following output:  
-- started at 11/19/2018 20:38:15 --

mdtest-1.9.3 was launched with 4 total task(s) on 1 node(s)  
Command line used: mdtest "-C" "-r" "-d" "/mnt/mdtest1" "-i" "3" "-I" "1000" "-z" "2" "-b" "8" "-L" "-u"  
Path: /mnt  
FS: 1.6 TiB Used FS: 0.0% Inodes: 0.0 Mi Used Inodes: -nan%

4 tasks, 292000 files/directories

Anyone knows why this happen???


https://github.com/open-mpi/ompi/issues/6094