
https://machinelearningmastery.com/a-gentle-introduction-to-the-jacobian/


In the literature, the term _Jacobian_ is often interchangeably used to refer to both the Jacobian matrix or its determinant. 

Both the matrix and the determinant have useful and important applications: in machine learning, the Jacobian matrix aggregates the partial derivatives that are necessary for backpropagation; the determinant is useful in the process of changing between variables.

In this tutorial, you will review a gentle introduction to the Jacobian. 

After completing this tutorial, you will know:

- The Jacobian matrix collects all first-order partial derivatives of a multivariate function that can be used for backpropagation.
- The Jacobian determinant is useful in changing between variables, where it acts as a scaling factor between one coordinate space and another.




