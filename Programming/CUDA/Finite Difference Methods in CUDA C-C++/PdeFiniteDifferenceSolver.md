https://pmontalb.github.io/PdeFiniteDifferenceSolver/


C++ manager class for PdeFiniteDifferenceKernels API. The low level calls are managed in the namespace _pde::detail DeviceManager_, whereas the high level infrastructure is delegated to the particular solver type.

Only linear hyperbolic and parabolic PDEs are supported (up to 3D). The exposed implementation is through:

- _AdvectionDiffusionSolver1D, AdvectionDiffusionSolver2D_
- _WaveEquationSolver1D, WaveEquationSolver2D_

These solvers are implemented with the Curiously Recurring Template Pattern (CRTP), useful for delegating the members data type at compile time.