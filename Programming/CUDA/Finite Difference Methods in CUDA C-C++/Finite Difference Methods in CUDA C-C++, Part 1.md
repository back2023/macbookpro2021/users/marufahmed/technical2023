
https://developer.nvidia.com/blog/finite-difference-methods-cuda-cc-part-1/

In the [previous CUDA C/C++ post](https://developer.nvidia.com/blog/parallelforall/efficient-matrix-transpose-cuda-cc/ "An Efficient Matrix Transpose in CUDA C/C++") we investigated how we can use shared memory to optimize a matrix transpose, achieving roughly an order of magnitude improvement in effective bandwidth by using shared memory to coalesce global memory access. The topic of today’s post is to show how to use shared memory to enhance data reuse in a finite difference code. In addition to shared memory, we will also discuss constant memory, which is a cached read-only memory optimized for uniform access across threads in a block (or warp).



