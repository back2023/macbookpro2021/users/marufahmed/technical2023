
https://arxiv.org/pdf/2306.07795.pdf


Abstract Optimal usage of the memory system is a key element of fast GPU algorithms. Unfortunately many common algorithms fail in this regard despite exhibiting great regularity in memory access patterns. In this paper we propose efficient kernels to permute the elements of an array. We handle a class of permutations known as Bit Matrix Multiply Complement (BMMC) permutations, for which we design kernels of speed comparable to that of a simple array copy. This is a first step towards implementing a set of array combinators based on these permutations. CCS Concepts: • Computing methodologies→Massively parallel algorithms; Parallel programming languages; • Software and its engineering → Software performance.



