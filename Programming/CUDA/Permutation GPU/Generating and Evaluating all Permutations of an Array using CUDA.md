
https://sites.google.com/site/cudapermutations

**The Problem:**

How to evaluate all N! permutations of an array against a test function in the least amount of time.

**Why this matters**:

While for many brute force/exhaustive search problems there exists better(quicker) solutions than examining each possibility, that is not always the case.

For such situation where there is no other approach, then the implementation of such an algorithm and the hardware which executes that algorithm are the determining factors in the overall running time.