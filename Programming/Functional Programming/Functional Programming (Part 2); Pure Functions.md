
https://blog.bitsrc.io/functional-programming-part-2-pure-functions-85491f3d7190


**_This article is a part of a series that talks about “Functional Programming”_**

In the [previous article](https://blog.bitsrc.io/functional-programming-part-1-first-class-functions-791103984dfb) in this series we discussed one of the most important features in a programming language that supports Functional style; First-Class functions. In this article we’ll be talking about Pure functions. Which every FP concept should be built on.

# Table of contents

- What are pure functions
- Characteristics of a pure function
- Why impure functions are bad
- Why pure functions are good
- 4096-Cores
- Honorship responsibility
- Recap
- Conclusion






