
https://web.ma.utexas.edu/CNA/NMC5/nmc5-C.html
 
Fifth Edition  
Ward Cheney & David Kincaid  
Sample C Codes


In the following table, each line/entry contains the program file name, the page number where it can be found in the textbook, and a brief description. Click on the program name to display the source code, which can be downloaded.

|   |   |   |
|---|---|---|
|**Chapter 1: Introduction**|   |   |
|[first.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP01/first.c)|6-7|First programming experiment|
|[double_first.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP01/double_first.c)|6-7|First programming experiment (doulbe precision version)|
|[pi.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP01/pi.c)|8|Simple code to illustrate double precision|
|**Chapter 2: Number Representation and Errors**|   |   |
|[xsinx.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP02/xsin.c)|77-79|Example of programming f(x) = x - sinx carefully|
|**Chapter 3: Locating Roots of Equations**|   |   |
|[bisection.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP03/bisection.c)|94-95|Bisection method|
|[rec_bisection.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP03/reec_bisection.c)|95-96|Recursive version of bisection method|
|[newton.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP03/newton.c)|106-107|Sample Newton method|
|[secant.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP03/secant.c)|127|Secant method|
|**Chapter 4: Interpolation and Numerical Differentiation**|   |   |
|[coef.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP04/coef.c)|152-155|Newton interpolation polynomial at equidistant pts|
|[deriv.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP04/deriv.c)|185-186|Derivative by center differences/Richardson extrapolation|
|**Chapter 5: Numerical Integration**|   |   |
|[sums.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP05/sums.c)|200|Upper/lower sums experiment for an integral|
|[trapezoid.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP05/trapezoid.c)|207|Trapezoid rule experiment for an integral|
|[romberg.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP05/romberg.c)|223-224|Romberg arrays for three separate functions|
|**Chapter 6: More on Numerical Integration**|   |   |
|[rec_simpson.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP06/rec_simpson.c)|241|Adaptive scheme for Simpson's rule|
|**Chapter 7: Systems of Linear Equations**|   |   |
|[ngauss.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP07/ngaus.c)|270-271|Naive Gaussian elimination to solve linear systems|
|[gauss.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP07/gauss.c)|285-287|Gaussian elimination with scaled partial pivoting|
|[tri.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP07/tri.c)|301-302|Solves tridiagonal systems|
|[penta.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP07/penta.c)|204|Solves pentadiagonal linear systems|
|**Chapter 8: More on Systems of Linear Equations**|   |   |
|**Chapter 9: Approximation by Spline Functions**|   |   |
|[spline1.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP09/spline1.c)|385|Interpolates table using a first-degree spline function|
|[spline3.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP09/spline3.c)|404-406|Natural cubic spline function at equidistant points|
|[spline2.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP09/spline2.c)|427-428|Interpolates table using a quadratic B-spline function|
|[schoenberg.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP09/schoenberg.c)|430-431|Interpolates table using Schoenberg's process|
|**Chapter 10: Ordinary Differential Equations**|   |   |
|[euler.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP10/euler.c)|448-449|Euler's method for solving an ODE|
|[taylor.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP10/taylor.c)|451|Taylor series method (order 4) for solving an ODE|
|[rk4.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP10/rk4.c)|462-463|Runge-Kutta method (order 4) for solving an IVP|
|[rk45.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP10/rk45.c)|472-473|Runge-Kutta-Fehlberg method for solving an IVP|
|[mainrk45.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP10/mainrk45.c)|474|Runge-Kutta-Fehlberg method for solving an IVP (main program)|
|[rk45ad.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP10/rk45ad.c)|474|Adaptive Runge-Kutta-Fehlberg method|
|**Chapter 11: Systems of Ordinary Differential Equations**|   |   |
|[taylorsys.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP11/taylorsys.c)|489-491|Taylor series method (order 4) for systems of ODEs|
|[rk4sys.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP11/rk4sys.c)|491-493, 496|Runge-Kutta method (order 4) for systems of ODEs|
|[amrk.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP11/amrk.c)|510-513|Adams-Moulton method for systems of ODEs|
|[amrkad.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP11/amrkad.c)|513|Adaptive Adams-Moulton method for systems of ODEs|
|**Chapter 12: Smoothing of Data and the Method of Least Squares**|   |   |
|**Chapter 13: Monte Carlo Methods and Simulation**|   |   |
|[test_random.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/test_random.c)|5652-563|Example to compute, store, and print random numbers|
|[coarse_check.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/coarse_check.c)|564|Coarse check on the random-number generator|
|[double_integral.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/double_integral.c)|574-575|Volume of a complicated 3D region by Monte Carlo|
|[volume_region.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/volume_region.c)|575-576|Numerical value of integral over a 2D disk by Monte Carlo|
|[cone.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/cone.c)|576-576|Ice cream cone example|
|[loaded_die.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/loaded_die.c)|581|Loaded die problem simulation|
|[birthday.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/birthday.c)|583|Birthday problem simulation|
|[needle.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/needle.c)|584|Buffon's needle problem simulation|
|[two_die.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/two_die.c)|585|Two dice problem simulation|
|[shielding.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP13/shielding.c)|586-587|Neutron shielding problem simulation|
|**Chapter 14: Boundary Value Problems for Ordinary Differential Equations**|   |   |
|[bvp1.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP14/bvp1.c)|602-603|Boundary value problem solved by discretization technique|
|[bvp2.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP14/bvp2.c)|605-606|Boundary value problem solved by shooting method|
|**Chapter 15: Partial Differential Equations**|   |   |
|[parabolic1.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP15/parabolic1.c)|618-619|Parabolic partial differential equation problem|
|[parabolic2.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP15/parabolic.c)|620-621|Parabolic PDE problem solved by Crank-Nicolson method|
|[hyperbolic.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP15/hyperbolic.c)|633-634|Hyperbolic PDE problem solved by discretization|
|[seidel.c](http://www.ma.utexas.edu/CNA/cheney-kincaid/Ccode/CHP15/seidel.c)|642-645|Elliptic PDE solved by discretization/ Gauss-Seidel method|
|**Chapter 16: Minimization of Functions**|   |   |
|**Chapter 17: Linear Programming**|   |   |

Addditional programs can be found at the textbook's anonymous ftp site:

[**ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/**](ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/)



## Numerical Mathematics and Computing,  
5th Edition

by [Ward Cheney](http://www.ticam.utexas.edu/People/Faculty/CHENEY.html) and [David Kincaid](http://www.cs.utexas.edu/users/kincaid/)

https://web.ma.utexas.edu/CNA/NMC5/




## Sample Computer Codes
https://web.ma.utexas.edu/CNA/NMC5/sample.html

Software associated with our book is freely available on the Internet for educational use by students. Files are available based on the pseudocode in the textbook written in Matlab, Maple, and Mathematica as well as in a variety of programming languages such as Fortran 90/77, C/C++, and Java.

List of Sample Computer Codes:

- **[Matlab codes](http://www.ma.utexas.edu/CNA/NMC5/nmc5-matlab.html)**
    
- **[Maple codes](http://www.ma.utexas.edu/CNA/NMC5/nmc5-maple.html)**
    
- **[Mathematica codes](http://www.ma.utexas.edu/CNA/NMC5/nmc5-mathematica.html)**
    
- **[Fortran 90 codes](http://www.ma.utexas.edu/CNA/NMC5/nmc5-fortran90.html)**
    
- **[Fortran 77 codes](http://www.ma.utexas.edu/CNA/NMC5/nmc5-fortran77.html)**
    
- **[C codes](http://www.ma.utexas.edu/CNA/NMC5/nmc5-C.html)**
    
- **[C++ codes](http://www.ma.utexas.edu/CNA/NMC5/nmc5-Cpp.html)**
    
- **[More C++ codes](ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/Cpp/)**
    
- **[Java codes](ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/Java/)**
    
- **[More Java codes](ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/java/)**
    
- **[computer plots](ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/plots/)**
    
- **[transparencies files](ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/slides/)**
    

These file and a variety of other files may be accessed directly by visiting our ftp Web site:

**[ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/](ftp://ftp.ma.utexas.edu/pub/cheney-kincaid/)**