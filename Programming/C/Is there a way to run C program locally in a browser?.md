

[Emscripten](https://emscripten.org/) and [WASM](https://en.wikipedia.org/wiki/WebAssembly) are the two popular solutions here.

Don't expect great performance, but you should then be able to link it up with a little bit of JavaScript, CSS and HTML for your code editing and console views.

If you're okay with running a server, then you can use this [Jupyter Notebook](https://jupyter.org/) kernel: [https://github.com/jupyter-xeus/xeus-cling](https://github.com/jupyter-xeus/xeus-cling)

Here's an example in WASM, with no server: [https://github.com/tbfleming/cib](https://github.com/tbfleming/cib)


https://stackoverflow.com/questions/61925125/is-there-a-way-to-run-c-program-locally-in-a-browser









