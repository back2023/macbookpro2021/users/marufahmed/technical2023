Copied from path: `/home/900/mah900/Technical/Programming/C/C-Jupyter-Xues.md`

### Install

### Try 1: (For Python) NOT
```
#https://xeus-python.readthedocs.io/en/latest/installation.html
#
#. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
#conda create -p /scratch/fp0/mah900/env/xeus-python
#conda activate /scratch/fp0/mah900/env/xeus-python
#
#conda install xeus-python jupyterlab -c conda-forge
```
  

### Try 2: For C ??? NOT
```

#https://xeus.readthedocs.io/en/latest/installation.html
#
#. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
#conda activate /scratch/fp0/mah900/env/xeus-python
#conda install xeus-zmq -c conda-forge
```
  

### Try 3:
https://github.com/jupyter-xeus/xeus-cling
https://xeus-cling.readthedocs.io/en/latest/

```
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/xeus-cling
conda activate /scratch/fp0/mah900/env/xeus-cling

conda install xeus-cling -c conda-forge
conda install jupyterlab -c conda-forge
```  
  

### ARE
https://are.nci.org.au/
```
8
normalbw
small
fp0
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

/scratch/fp0/mah900/Miniconda2023/
/scratch/fp0/mah900/env/xeus-cling
```