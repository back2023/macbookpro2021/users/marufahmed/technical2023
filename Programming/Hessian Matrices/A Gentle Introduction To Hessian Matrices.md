
https://machinelearningmastery.com/a-gentle-introduction-to-hessian-matrices/

Last Updated on March 16, 2022

Hessian matrices belong to a class of mathematical structures that involve second order derivatives. They are often used in machine learning and data science algorithms for optimizing a function of interest. 

In this tutorial, you will discover Hessian matrices, their corresponding discriminants, and their significance. All concepts are illustrated via an example.

After completing this tutorial, you will know:

- Hessian matrices
- Discriminants computed via Hessian matrices
- What information is contained in the discriminant









