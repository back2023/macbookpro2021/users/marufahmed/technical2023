
https://flax.readthedocs.io/en/latest/guides/flax_fundamentals/flax_basics.html

Plan: use `NCI_ai_models` to practise flex notebooks 
### ARE (Flex)
```bash
gpuvolta
1gpu
gdata/fp0+gdata/dk92+gdata/z00+gdata/rt52+scratch/fp0+gdata/wb00+gdata/pp66+scratch/vp91

Python or Conda virtual environment base: "/scratch/fp0/mah900/Miniconda2023" 
Conda environment: "/scratch/fp0/mah900/env/NCI_ai_models"
```
Note: ARE launches but Jupyter does not work. 
Try reinstalling Jupyter
Did not work, new env install at [2024.01.13]
Now, using the new env.

New Directory for notebooks 
```
mkdir /scratch/fp0/mah900/flex
cd /scratch/fp0/mah900/flex
```

