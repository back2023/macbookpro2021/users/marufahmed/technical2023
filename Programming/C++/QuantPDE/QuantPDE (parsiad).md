
https://github.com/parsiad/QuantPDE

A high-performance, open-source, header-only C++(>=11) library for pricing derivatives.

More generally, QuantPDE can also be used to solve [Hamilton-Jacobi-Bellman](https://en.wikipedia.org/wiki/Hamilton%E2%80%93Jacobi%E2%80%93Bellman_equation) (HJB) equations and quasi-variational inequalities (HJBQVI).


