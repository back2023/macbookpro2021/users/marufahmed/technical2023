
Technology trends require software/hardware co-design for HPC systems. Open-standard software programming models enable this co-design and allows us to support initiatives such as European Processor Initiative (EPI) supporting RISC-V® processors as accelerators. Indeed, the need for distributed and heterogeneous programming models is so urgent that we now have many examples to draw from. By focusing on open ISO languages such as C++ with the addition of heterogeneous/distributed offloads to support the newest accelerator hardware, this workshop aims to isolate and unify the learnings from the many different models for the Performance, Portability, and Productivity equation.

This will be the 4th DHPCC++ workshop (previously held 2017-19) with a focus on heterogeneous programming models for C and C++, covering all the programming models that have been designed to support heterogeneous programming in C++. It is held as part of [Euro-Par 22](https://2022.euro-par.org/) in Glasgow, Scotland 22-26 August 2022.


https://sycl.tech/events/22/03/24/distributed-heterogeneous-programming-in-cpp.html






