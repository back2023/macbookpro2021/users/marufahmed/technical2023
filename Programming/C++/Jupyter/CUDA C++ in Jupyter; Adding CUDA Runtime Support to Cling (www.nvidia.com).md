
https://www.nvidia.com/en-us/on-demand/session/gtcsj20-s21588/
 
Axel Huebl, Lawrence Berkeley National Laboratory
Simeon Ehrig, Helmholtz-Zentrum Dresden-Rossendorf


Jupyter Notebooks are omnipresent in the modern scientist's and engineer's toolbox just as CUDA C++ is in accelerated computing. We present the first implementation of a CUDA C++ enabled read-eval-print-loop (REPL) that allows to interactively "script" the popular CUDA C++ runtime syntax in Notebooks. With our novel implementation, based on CERN's C++ interpreter Cling, the modern CUDA C++ developer can work as interactively and productively as (I)Python developers while keeping all the benefits of the vast C++ computing and library ecosystem coupled with first-class performance.



