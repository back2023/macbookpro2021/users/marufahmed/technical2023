
https://github.com/kingoflolz/mesh-transformer-jax


# able of contents

1. [Mesh Transformer JAX](https://github.com/kingoflolz/mesh-transformer-jax#mesh-transformer-jax)
    1. [Updates](https://github.com/kingoflolz/mesh-transformer-jax#updates)
2. [Pretrained Models](https://github.com/kingoflolz/mesh-transformer-jax#pretrained-models)
    1. [GPT-J-6B](https://github.com/kingoflolz/mesh-transformer-jax#gpt-j-6b)
        1. [Links](https://github.com/kingoflolz/mesh-transformer-jax#links)
        2. [Acknowledgments](https://github.com/kingoflolz/mesh-transformer-jax#acknowledgments)
        3. [License](https://github.com/kingoflolz/mesh-transformer-jax#license)
        4. [Model Details](https://github.com/kingoflolz/mesh-transformer-jax#model-details)
        5. [Zero-Shot Evaluations](https://github.com/kingoflolz/mesh-transformer-jax#zero-shot-evaluations)
3. [Architecture and Usage](https://github.com/kingoflolz/mesh-transformer-jax#architecture-and-usage)
    1. [Fine-tuning](https://github.com/kingoflolz/mesh-transformer-jax#fine-tuning)
    2. [JAX Dependency](https://github.com/kingoflolz/mesh-transformer-jax#jax-dependency)
4. [TODO](https://github.com/kingoflolz/mesh-transformer-jax#todo)

# [](https://github.com/kingoflolz/mesh-transformer-jax#mesh-transformer-jax)Mesh Transformer JAX

A haiku library using the `xmap`/`pjit` operators in JAX for model parallelism of transformers.

The parallelism scheme is similar to the [original Megatron-LM](https://arxiv.org/abs/1909.08053), which is efficient on TPUs due to the high speed 2d mesh network. There is also an experimental model version which implements [ZeRo style sharding](https://arxiv.org/abs/1910.02054).

This library is designed for scalability up to approximately 40B parameters on TPUv3s, beyond which different parallelism strategies should be used. See other implementations such as [GPT-NeoX](https://github.com/EleutherAI/gpt-neox) or [DeepSpeed](https://github.com/microsoft/DeepSpeed) for that.

One future direction for research is integrating this codebase with [swarm-jax](https://github.com/kingoflolz/swarm-jax), to achieve further scalability with pipeline parallelism.

 