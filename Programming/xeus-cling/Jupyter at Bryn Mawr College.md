
|   |   |   |   |
|---|---|---|---|
|**Public notebooks:** [/services/public/dblank](https://jupyter.brynmawr.edu/services/public/dblank) / [CS240 Computer Organization](https://jupyter.brynmawr.edu/services/public/dblank/CS240%20Computer%20Organization) / [2017-Fall](https://jupyter.brynmawr.edu/services/public/dblank/CS240%20Computer%20Organization/2017-Fall) / [Notebooks](https://jupyter.brynmawr.edu/services/public/dblank/CS240%20Computer%20Organization/2017-Fall/Notebooks)|   |   |   |

# The C Programming Language[](https://jupyter.brynmawr.edu/services/public/dblank/CS240%20Computer%20Organization/2017-Fall/Notebooks/The%20C%20Programming%20Language.ipynb#The-C-Programming-Language)

For these examples, I will make a subdirectory called cstuff. There are four ways to run shell commands:

1. `! command` in a cell
2. `%%shell` at top of cell
3. Open a Terminal
4. Put an IFRAME in a notebook

https://jupyter.brynmawr.edu/services/public/dblank/CS240%20Computer%20Organization/2017-Fall/Notebooks/The%20C%20Programming%20Language.ipynb

