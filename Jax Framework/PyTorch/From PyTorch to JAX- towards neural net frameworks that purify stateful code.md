
2020-03-09

[JAX](https://github.com/google/jax), Google's now-over-a-year-old Python library for machine learning and other numerical computing describes itself as “Composable transformations of Python+NumPy programs: differentiate, vectorize, JIT to GPU/TPU, and more”—and while that definition is certainly fitting, it is a bit intimidating. I would describe JAX as numpy, but on GPU, and then move on to the one feature we will be most concerned with in this post: its autodifferentiation capability, i.e., how to get gradients of some loss function your code computes with respect to you input parameters. If you haven't heard of JAX at all, I can recommend [Skye Wanderman-Milne's talk at NeurIPS](https://slideslive.com/38923687/jax-accelerated-machinelearning-research-via-composable-function-transformations-in-python) on JAX (or check out [the corresponding slides](https://program-transformations.github.io/slides/NeurIPS_workshop_JAX_talk.pdf)). It's a cool framework with cool ideas!


https://sjmielke.com/jax-purify.htm