
## Motivation

Running XGBoost with **custom loss functions can greatly increase** classification/regression **performance** in certain applications. Being able to quickly test many different loss functions is key in time-critical research environments. Thus, _manual differentiation_ is not always feasible (and sometimes even prone to human errors, or numerical instability).

_Automatic differentiation_ allows us to automatically get the derivatives of a function, given its calculation. It does so by representing our function as a composition of functions with known derivatives, requiring zero effort from the developer’s side.

We will start with a quick introduction, clarifying our problem. Then, we will dive into the implementation of automatic differentiation with PyTorch and JAX and integrate it with XGBoost. Finally, we will perform run-time benchmarks and show that **JAX is ~10x faster than PyTorch** for this application.


https://towardsdatascience.com/jax-vs-pytorch-automatic-differentiation-for-xgboost-10222e1404ec









