

I’ve been looking into deep learning libraries recently and JAX seemed interesting, but as far as I could tell no one had actually benchmarked it against PyTorch, the de facto standard. So I decided to implement the same model in both and compare. Here’s the top level summary: **PyTorch gets 1.11 iterations per second and JAX gets 1.24it/s (12% better)** on a Google Colab notebook with a P100. In addition, **JAX is more memory efficient, the PyTorch model OOMs with more than 62 examples at a time and JAX can get up to 79** (at 1.01it/s, or 79.79 examples per second vs PyTorch’s 68.82 with the smaller batch size).

Meanwhile TPUs are kind of absurd. [Torch on XLA theoretically exists](https://pytorch.org/xla/release/1.9/index.html#), but I don’t know of anyone who’s actually gotten it to work. When I was testing it my code _segfaulted_. TPUs work very smoothing with JAX though. I was accepted in the [TPU Research Cloud](https://sites.research.google/trc/) (formerly TFRC), and a TPUv3-8 can run through 2,591 examples per second with a batch size of 3,032.

http://www.echonolan.net/posts/2021-09-06-JAX-vs-PyTorch-A-Transformer-Benchmark.html