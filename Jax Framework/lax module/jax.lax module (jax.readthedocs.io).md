
https://jax.readthedocs.io/en/latest/jax.lax.html

[`jax.lax`](https://jax.readthedocs.io/en/latest/jax.lax.html#module-jax.lax "jax.lax") is a library of primitives operations that underpins libraries such as [`jax.numpy`](https://jax.readthedocs.io/en/latest/jax.numpy.html#module-jax.numpy "jax.numpy"). Transformation rules, such as JVP and batching rules, are typically defined as transformations on [`jax.lax`](https://jax.readthedocs.io/en/latest/jax.lax.html#module-jax.lax "jax.lax") primitives.

Many of the primitives are thin wrappers around equivalent XLA operations, described by the [XLA operation semantics](https://www.tensorflow.org/xla/operation_semantics) documentation. In a few cases JAX diverges from XLA, usually to ensure that the set of operations is closed under the operation of JVP and transpose rules.

Where possible, prefer to use libraries such as [`jax.numpy`](https://jax.readthedocs.io/en/latest/jax.numpy.html#module-jax.numpy "jax.numpy") instead of using [`jax.lax`](https://jax.readthedocs.io/en/latest/jax.lax.html#module-jax.lax "jax.lax") directly. The [`jax.numpy`](https://jax.readthedocs.io/en/latest/jax.numpy.html#module-jax.numpy "jax.numpy") API follows NumPy, and is therefore more stable and less likely to change than the [`jax.lax`](https://jax.readthedocs.io/en/latest/jax.lax.html#module-jax.lax "jax.lax") API.







