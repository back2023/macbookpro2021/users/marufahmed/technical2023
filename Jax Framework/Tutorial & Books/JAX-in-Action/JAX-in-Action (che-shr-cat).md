
https://github.com/che-shr-cat/JAX-in-Action

# "Deep Learning with JAX" book

This is a repository accompanying the "[Deep Learning with JAX](https://www.manning.com/books/deep-learning-with-jax)" book (originally called "JAX in Action"). Feel free to use the promotional code **au35sap** for 35% discount for this book and any other Manning's product.


