
https://github.com/deep-learning-indaba/indaba-pracs-2022/blob/main/practicals/Introduction_to_ML_using_JAX.ipynb


© Deep Learning Indaba 2022. Apache License 2.0.

**Authors:** Kale-ab Tessera

**Reviewers:** Javier Antoran, James Allingham, Ruan van der Merwe, Sebastian Bodenstein, Laurence Midgley, Joao Guilherme and Elan van Biljon.

**Introduction:**

In this tutorial, we will learn about JAX, a new machine learning framework that has taken deep learning research by storm! JAX is praised for its speed, and we will learn how to achieve these speedups, using core concepts in JAX, such as automatic differentiation (`grad`), parallelization (`pmap`), vectorization (`vmap`), just-in-time compilation (`jit`), and more. We will then use what we have learned to implement Linear Regression effectively while learning some of the fundamentals of optimization.

**Topics:**

Content: `Numerical Computing` , `Supervised Learning`  
Level: `Beginner`

**Aims/Learning Objectives:**

- Learn the basics of JAX and its similarities and differences with NumPy.
- Learn how to use JAX transforms - `jit`, `grad`, `vmap`, and `pmap`.
- Learn the basics of optimization and how to implement effective training procedures using [Haiku](https://github.com/deepmind/dm-haiku) and [Optax](https://github.com/deepmind/optax).

**Prerequisites:**

- Basic knowledge of [NumPy](https://github.com/numpy/numpy).
- Basic knowledge of [functional programming](https://en.wikipedia.org/wiki/Functional_programming).

