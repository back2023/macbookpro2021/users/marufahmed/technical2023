https://medium.com/nlplanet/a-quick-intro-to-jax-with-examples-c6e8cc65c3c1

NumPy on accelerators, JIT compilation, XLA, optimized kernels, and automatic differentiation.