

https://notes.quantecon.org/submission/622ed4daf57192000f918c61

# Dynamic Programming on the GPU: A Tutorial for Economists


Parallelization on GPUs is one of the major trends of modern scientific computing.

In this notebook we examine a simple implementation of dynamic programming on the GPU using Python and the [Google JAX library](https://jax.readthedocs.io/en/latest/index.html).

This notebook is intended for readers who are familiar with the basics of dynamic programming and want to learn about the JAX library and working on the GPU.

The notebook is part of the [QuantEcon](https://quantecon.org/) project.

From our timing on Google Colab with a Tesla P100 GPU, the JAX based Bellman operator is

- over 3000 times faster than a Numba-based JIT-compiled single-threaded version, and
- almost 1500 times faster than a NumPy-based multithreaded version.

(The Numba-based version uses a similar compiler to Julia and, for this kind of problem, runs at a similar speed to compiled C and Fortran code.)

