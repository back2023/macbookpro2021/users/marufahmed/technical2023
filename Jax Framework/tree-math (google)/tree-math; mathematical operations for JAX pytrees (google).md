
https://github.com/google/tree-math


## About

Mathematical operations for JAX pytrees

# tree-math: mathematical operations for JAX pytrees

tree-math makes it easy to implement numerical algorithms that work on [JAX pytrees](https://jax.readthedocs.io/en/latest/pytrees.html), such as iterative methods for optimization and equation solving. It does so by providing a wrapper class `tree_math.Vector` that defines array operations such as infix arithmetic and dot-products on pytrees as if they were vectors.



