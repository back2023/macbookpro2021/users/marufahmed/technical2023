
https://pennylane.ai/qml/demos/tutorial_jax_transformations


JAX is an incredibly powerful scientific computing library that has been gaining traction in both the physics and deep learning communities. While JAX was originally designed for classical machine learning (ML), many of its transformations are also useful for quantum machine learning (QML), and can be used directly with PennyLane.






