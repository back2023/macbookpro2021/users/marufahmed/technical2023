
https://mpi4jax.readthedocs.io/en/latest/index.html


`mpi4jax` enables zero-copy, multi-host communication of [JAX](https://jax.readthedocs.io/) arrays, even from jitted code and from GPU memory.

## But why? 

The JAX framework [has great performance for scientific computing workloads](https://github.com/dionhaefner/pyhpc-benchmarks), but its [multi-host capabilities](https://jax.readthedocs.io/en/latest/jax.html#jax.pmap) are still limited.

With `mpi4jax`, you can scale your JAX-based simulations to _entire CPU and GPU clusters_ (without ever leaving `jax.jit`).

In the spirit of differentiable programming, `mpi4jax` also supports differentiating through some MPI operations.





