
We show how to train an ensemble of CNNs on the MNIST dataset, where the size of the ensemble is equal to the number of available devices. In short, this change be described as:

- make a number of functions parallel using [`jax.pmap()`](https://jax.readthedocs.io/en/latest/jax.html#jax.pmap),  
- split the random seed to obtain different parameter initialization,
- replicate the inputs and unreplicate the outputs where necessary,
- average probabilities across devices to compute the predictions.

In this HOWTO we omit some of the code such as imports, the CNN module, and metrics computation, but they can be found in the [MNIST example](https://github.com/google/flax/blob/main/examples/mnist/train.py).


https://flax.readthedocs.io/en/latest/guides/ensembling.html






