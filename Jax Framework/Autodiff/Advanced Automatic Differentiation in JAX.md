
https://colab.research.google.com/github/google/jax/blob/main/docs/jax-101/04-advanced-autodiff.ipynb


_Authors: Vlatimir Mikulik & Matteo Hessel_

Computing gradients is a critical part of modern machine learning methods. This section considers a few advanced topics in the areas of automatic differentiation as it relates to modern machine learning.

While understanding how automatic differentiation works under the hood isn't crucial for using JAX in most contexts, we encourage the reader to check out this quite accessible [video](https://www.youtube.com/watch?v=wG_nF1awSSY) to get a deeper sense of what's going on.

[The Autodiff Cookbook](https://colab.research.google.com/corgiredirector?site=https%3A%2F%2Fjax.readthedocs.io%2Fen%2Flatest%2Fnotebooks%2Fautodiff_cookbook.html) is a more advanced and more detailed explanation of how these ideas are implemented in the JAX backend. It's not necessary to understand this to do most things in JAX. However, some features (like defining [custom derivatives](https://colab.research.google.com/corgiredirector?site=https%3A%2F%2Fjax.readthedocs.io%2Fen%2Flatest%2Fnotebooks%2FCustom_derivative_rules_for_Python_code.html)) depend on understanding this, so it's worth knowing this explanation exists if you ever need to use them.


