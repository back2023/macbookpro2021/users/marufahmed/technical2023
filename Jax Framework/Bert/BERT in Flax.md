
# Archived: see [https://github.com/nikitakit/sabertooth](https://github.com/nikitakit/sabertooth) instead

# [](https://github.com/nikitakit/flax_bert#bert-in-flax)BERT in Flax

_**NOTE: This implementation is work in progress!**_

An implementation of BERT in JAX+Flax. Runs on CPU, GPU, and TPU.


https://github.com/nikitakit/flax_bertgl


