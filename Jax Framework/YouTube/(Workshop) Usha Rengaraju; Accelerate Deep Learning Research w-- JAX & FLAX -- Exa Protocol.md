
https://www.youtube.com/watch?v=jb5TkZBCp3U

Join Usha Rengaraju, Chief of Research at Exa Protocol, as she unveils the power of JAX in accelerating deep learning research. Discover how to build efficient neural networks with JAX/FLAX, leveraging Python+NumPy familiarity and hardware accelerator speed. Dive into function transformations like automatic differentiation, batching, and parallelization. Master JAX features in this comprehensive workshop to boost your machine learning projects. Don't miss out on unlocking the potential of JAX for your research!









