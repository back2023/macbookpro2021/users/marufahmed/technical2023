
https://www.youtube.com/watch?v=QF8IWXhIL9o

Session presented at Big Mountain Data and Dev Conference(www.utahgeekevents.com) 

Speakers:Nick Doiron 
Level:Intermediate 
Track:Nick Doiron 
Room:Room 2 

Description:JAX is a library similar to NumPy, redesigned by Google for GPUs and TPUs. A wave of new ML frameworks, based on JAX, are looking for a chance to become an alternative to PyTorch and TensorFlow. In this session, I try out three JAX frameworks, explaining the steps to solving Kaggle's flower classification challenge with each one.


