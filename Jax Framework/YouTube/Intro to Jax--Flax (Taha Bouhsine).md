
https://www.youtube.com/watch?v=EZLNsChZlzU

Streamed live on 17 Aug 2022 [MENA Digital Days 2022 - Week #32 GDE Series](https://www.youtube.com/playlist?list=PLlqOXLg-GOqfvCTP_zY0W_99odTBTtxz5)

`#Session#` 
Intro to Jax/Flax (Taha Bouhsine) 
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 

[#MENADD](https://www.youtube.com/hashtag/menadd)
[#MENADigitalDays2022](https://www.youtube.com/hashtag/menadigitaldays2022) [#ml](https://www.youtube.com/hashtag/ml) 

» WebSite: [http://bit.ly/mena-dd](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbGpYVHc3TGhxdWlxNElDSVVpamtkSnMxTG5Od3xBQ3Jtc0tuWVJjbUE3R0xfbFVFQTUtekpSRGZtN0FKTlFWZzQ3NldPVnZaM0c3Sko5OWtuWlVubEowbFh2aTNOOXQwclBZNE1oa3NocGFYRFZnelNnR0xET0trbXNJdjFqZkxRTDB1bThDZ0JQenQ0akdpeXpwUQ&q=http%3A%2F%2Fbit.ly%2Fmena-dd&v=EZLNsChZlzU) 
» All sessions are recorded and will remain online after the streaming ends. You can watch it later anytime.    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) / gdgmena](https://youtube.com/GDGMENA)   

`# Contact #`  

» Twitter ⇒ Google Lead [https://twitter.com/abid_salim](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbVFaR1hqTE5qMExPcVhnc2k5bUgwaEhIbEpiUXxBQ3Jtc0ttcXlLcTJreC02VTB6Qm01OFlValp0RGFvZ1FNUlZ1UFBMaDFlT2tCSnNkazEwU3JISGlOZEJ6ckxwa3pTVkJ2X3ZVMTc3dzh0MzBjb2FJQmZiT05mTnJhSThKeGFMeUFzdXZXNHlrTFNRaVljQVYtMA&q=https%3A%2F%2Ftwitter.com%2Fabid_salim&v=EZLNsChZlzU) && [https://twitter.com/gdgmena](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbWF0OWU5cHBxZ2hWcUhkUWdkeEVPaE16dkJ6UXxBQ3Jtc0ttdmhVbWppNFByNU1wRi1nY201MXJ4WGlNbFFfWjA1NzVxNjQ2NnRySVNpa3RwSmxzdllNdExDakh1cUZqNlBRdDNXQWtsRVlzejJpMzFCNXpzNHAtMkRxWFA2cXhWbFlLS3hTdEg2VjJqZzB6ajZ1bw&q=https%3A%2F%2Ftwitter.com%2Fgdgmena&v=EZLNsChZlzU) » Facebook ⇒ [https://fb.com/MENAGDG](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbWxuSDB3Rld3Qk5mS280d3l0TDRodXJ0Sm11QXxBQ3Jtc0tta1drX05Sa3pFNEZ1ZG5uYllRa3hzNlZFdFRlY1o4ZjVVeEU4aHRpejFzNFhHcE1QemdqdElDUHVaeHRkRVprQkw3YW5vMlhzNklLblYyeVFFd2VqUm1VdTdrZEpGc2EyalFmeGpuenZ2OV9jd2p0aw&q=https%3A%2F%2Ffb.com%2FMENAGDG&v=EZLNsChZlzU) 
» Instagram ⇒ [https://instagram.com/gdgmena](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa3FfamxEOFZPck1kWWdmTjFIU0NjeURhTHcxQXxBQ3Jtc0tuR19VOExaeDY3OUxYQU9ZVmlJcFNEVHFnbFNYN1Q1WE9zaXd4YXQ4NHdYOXA0NWM1NnB3OF9BSmpva2tWRWFLekZBbTZEQUhlVExFSVNKZklTS3hoVUJmblFxbkQtZUJmVmJHTmNVWXVGbjZKT3Y0dw&q=https%3A%2F%2Finstagram.com%2Fgdgmena&v=EZLNsChZlzU) 

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
[#Google](https://www.youtube.com/hashtag/google) Programs 
» Join the DSC/Developer Student clubs’ leads ⇒ [http://goo.gle/dsc-leads](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbVR1a2lJQTJKTlFxRlFmQWVvNHlabl90OGdlZ3xBQ3Jtc0tsWkRfWThUTVRUQW1tcmtLX05ESHdEX2hhVmluY1N3WUNvSFRZZTJVaEJkR1BfWTZQeGlhV2ZYWUtxOV9GZHZjU2M2ZDA1MmU1b2VzTktrc004bVE0T1FYcjRrZmIwZEJuenl2NkV5ZE05dWJ3TUd6WQ&q=http%3A%2F%2Fgoo.gle%2Fdsc-leads&v=EZLNsChZlzU) 
» Join the GDG team ⇒[https://developers.google.com/community](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbWp5Nms4ZHFyUzJQeC10Y3VqRndnV0hKd3ZJQXxBQ3Jtc0tsLTcwZVJxaVRTYzFzOVFXQXdoMDJ3R3BpSzlKclhZM3laZ0hxVUt5YjFTQjMxcGVSd3RNX3RjVjdaN3M4NTFseGhqRTBuUWc0cmM4dEdETV9SYVhYcEc5VmZZUHh3MGRFRXpJem1GbUZZTnIzcVdVbw&q=https%3A%2F%2Fdevelopers.google.com%2Fcommunity&v=EZLNsChZlzU) 
» Join the Women Techmakers programs ⇒ [https://womentechmakers.com/communities](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbjV5a3AycVVmdWdycGJKLWxOeVpHbG1ISzhqUXxBQ3Jtc0trbUNSNW5LWHo0TjJXVnY0VldnVTZFci1zVnhnQUZjMk5zaVA5dklhRjF2Q3B1LWNkUHB0M1FrQ3lmY2JxYkRjbmhGMlBkTEZrVktHWTd2NTY0RjhvYWNoN2VDLUJMNEhUT0xHeW5FSUxaWlpiZmdnMA&q=https%3A%2F%2Fwomentechmakers.com%2Fcommunities&v=EZLNsChZlzU) » Apply to the Google for Startups MENA program ⇒ g.co/AcceleratorMENA

 

