
https://www.youtube.com/watch?v=phJZyr9H9C0

Live coding with JAX and Flax. In this episode we will explore Flax's new APIs to support parallelism. 

Repo: [https://github.com/cgarciae/jax-streams](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbGtFZVlBN0dGQUxzVmllT1lFcTY0TFBMY0R5d3xBQ3Jtc0tuMllHMFpTOFlIWmM3TkFiRHgwN3FmQndhXzV0S3p5UXprNERDS1M1Q1Z5WFMxUWM0XzNOVnBxdHM1NS04RHF5cEdmaHpHYVFGYngwVl9mclJWZTh2eWFHQUJ6dlNHX3NsSG15TS1oWFVSaUpIMEU5cw&q=https%3A%2F%2Fgithub.com%2Fcgarciae%2Fjax-streams&v=phJZyr9H9C0)


