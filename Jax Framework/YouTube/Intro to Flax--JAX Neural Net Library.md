
https://www.youtube.com/watch?v=HOlQzrn84A4


This is an intro to the famous FLAX library from Google Brain. It is super fast than the other libraries that are presently available. The recent PaLM, the 540B param Language Model, is also trained using JAX. So, let's explore this library together. 

Flax - [https://github.com/google/flax](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbVhzQWphRjBrRnRqYV83aUhkTVg4MDkwdkJLUXxBQ3Jtc0tsRzliR202bFlaMl8zakhtMFIyaGR5bFRmZUMtdXU4cm1zVGVNTnJNUzNVdmY1d0Rqa1hndS1fX09kSjlHZjBzWVA0UGZ6Q2JTeHViQjBEcVRRNmNGa3MxazAxeWZsN1BxdWtNOVljN3lpd2dWMy13aw&q=https%3A%2F%2Fgithub.com%2Fgoogle%2Fflax&v=HOlQzrn84A4) 
Flax Documentation - [https://flax.readthedocs.io/en/latest/](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbkp2QUZmejRUSFZRTVFFeERjTkx6RkpEQTVKQXxBQ3Jtc0trSGdfMG1NemFZdzV2ZEloNlFudGxndEE0dkZHemJEZEV2WmVJTFlhQXpMcUdZc2pidDQ4M0tSczIyakpRcVVBMHRvZFhaM3NxOXNfYnRPTTRQbTBZemhJWVNLbGlNMTRsa0dHYWN6RUtYUGJXSWNGZw&q=https%3A%2F%2Fflax.readthedocs.io%2Fen%2Flatest%2F&v=HOlQzrn84A4) 
HuggingFace - [https://huggingface.co/](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa0o0TF96UDg0MFpPNGNCc095TnpkelE5WkdLZ3xBQ3Jtc0ttOGgtb2g1T2lWM3ZaWlhFWURrZGFiSFFySkdGWWE3MGVxNEFFMGUxcVVrSHpQUzNxeDFaT0xIMDJiRVRRTjY1aXg0N1BiY1dHcFN5UGVFYnBLVnZFZ0tGU1pyelZaRXl0R3kyRUZKQ0NJeWlqZGpoMA&q=https%3A%2F%2Fhuggingface.co%2F&v=HOlQzrn84A4) 
My fav JAX based NN library is Haiku by DeepMind - [https://github.com/deepmind/dm-haiku](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa3c3SWo1QTlIZkRraXByM29JVmpVQXE2M3p5Z3xBQ3Jtc0ttQVJkaUJkV0pOenY5MHZHb3ZweGFwb1FFc010WU5mN1plcmZFaXREM0hTeUE2aFpPb2pqbGdVU1l0S1A5SEN6U0NNQTRFT044ZFlvMzY4MzFESFBnVkhENm1tWERqVlRfcldsRFRHbHVjM3ppcnVLMA&q=https%3A%2F%2Fgithub.com%2Fdeepmind%2Fdm-haiku&v=HOlQzrn84A4) 

Follow Me here Twitter - [https://twitter.com/asvs_kartheek](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbmtBR09ZX2hCQkZrckxQQk04clNjTUY2ZU5NQXxBQ3Jtc0tta0I4dnotWnZ0OFpTYURZT1VSSGNrWVhLM21TOHZHaWYtc3ZWZTJtd2dEWklQbnBsZlF4SGRaaDdBQVpURGtLdi16S1RWMndUd1ZSZl8zYmVSVktJUlpreHJ4RXVYWF92ZFEyMWFJMEVFcndIWEo4cw&q=https%3A%2F%2Ftwitter.com%2Fasvs_kartheek&v=HOlQzrn84A4) 
GitHub - [https://github.com/asvskartheek](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbVRwWVc3ZTVDZjNERU9CSkJIMkF4WDdxQ2t1UXxBQ3Jtc0ttV3V5enN6ZU8yMUcwM3NOWGtJbi1Ub1dLVlRCV1lZMUNsakdMaFlqeWZEUTI3c3ZfcHRzU3hZbGtEUXhRNnVhSlFuR1RxckpvZGJ2T0RCYVE4cXdmOTgzZEM5TFJBNVNPZnRKVjF2cUVzalRkVnM0QQ&q=https%3A%2F%2Fgithub.com%2Fasvskartheek&v=HOlQzrn84A4)



