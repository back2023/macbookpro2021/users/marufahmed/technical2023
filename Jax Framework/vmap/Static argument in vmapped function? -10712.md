
https://github.com/google/jax/discussions/10712


"static" arguments in `jax.jit` are ones that are not traced. To make an argument untraced in `vmap` you can set its corresponding `in_axes` to `None`.





