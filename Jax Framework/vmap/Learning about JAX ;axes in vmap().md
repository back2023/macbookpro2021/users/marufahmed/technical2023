

https://jiayiwu.me/blog/2021/04/05/learning-about-jax-axes-in-vmap.html


In the recent weeks I’ve been learning about [JAX](https://jax.readthedocs.io/en/latest/), a Python library for machine learning developed by [Google Research](https://research.google/) team and [extensively used by Deepmind](https://deepmind.com/blog/article/using-jax-to-accelerate-our-research).

#### What is JAX and why is it different?

JAX uses [Numpy](https://www.nature.com/articles/s41586-020-2649-2) based API for numerical computation. It has three main features making it attractive for fast and simple deep learning model implementation:

- Auto-differentiation (a.k.a `grad` in JAX) for customized functions. This means when optimizing loss for a machine learning model, instead of having to derive gradient and code it yourself or using syntactic and semantic constraints, the `grad` function will just compute the gredient for you based on the loss function.
    
- Fast and efficient vectorization and parallelization (a.k.a `vmap` and `pmap` in JAX). Like [numpy vectorization](https://numpy.org/doc/stable/reference/generated/numpy.vectorize.html), it simplifies the coding process and speeding up computation drastically.
    
- Just-in-time (JIT) compilation (a.k.a `jit` in JAX). I think this is the main secret sauce for JAX to perform so much faster than other packages, take a look at the comparisons in [quick start on `jit`](https://jax.readthedocs.io/en/latest/notebooks/quickstart.html#using-jit-to-speed-up-functions) and [combine `jit` with `vmap`](https://jax.readthedocs.io/en/latest/notebooks/quickstart.html#auto-vectorization-with-vmap).
    

#### Resources to learn about JAX

[Tutorial: JAX 101](https://jax.readthedocs.io/en/latest/jax-101/index.html) from JAX docsfile is a great point to start, though I found a bit challenging to understand some examples as a beginner.

My mentor plus friend Eric Ma wrote a gradient-based machine learning course called [dl-workshop](https://github.com/ericmjl/dl-workshop), which uses JAX framework to illustrate machine learning concepts.

[Robert Tjarko Lange](https://roberttlange.github.io/year-archive/) has several blog posts on using JAX on deep learning based projects, while [Colin Raffel](https://colinraffel.com/) demonstrated key concepts of JAX in [You don’t know JAX](https://colinraffel.com/blog/you-don-t-know-jax.html) with code.



