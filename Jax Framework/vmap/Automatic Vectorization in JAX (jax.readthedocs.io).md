
https://jax.readthedocs.io/en/latest/jax-101/03-vectorization.html


In the previous section we discussed JIT compilation via the `jax.jit` function. This notebook discusses another of JAX’s transforms: vectorization via `jax.vmap`.
