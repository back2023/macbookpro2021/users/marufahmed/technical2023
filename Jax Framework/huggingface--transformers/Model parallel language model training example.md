
https://github.com/huggingface/transformers/tree/main/examples/research_projects/jax-projects/model_parallel

###  transformers/examples/research_projects/jax-projects/model_parallel/


The following example showcases how to train/fine-tune GPTNeo model with model parallelism using the JAX/Flax backend and the [`pjit`](https://jax.readthedocs.io/en/latest/jax.experimental.pjit.html) transformation.

> Note: The example is experimental and might have bugs. Also currently it only supports single V3-8.

The `partition.py` file defines the `PyTree` of `ParitionSpec` for the GPTNeo model which describes how the model will be sharded. The actual sharding is auto-matically handled by `pjit`. The weights are sharded across all local devices. To adapt the script for other models, we need to also change the `ParitionSpec` accordingly.



