

JAX (Just After eXecution) is a machine/deep learning library developed by DeepMind. All JAX operations are based on XLA or Accelerated Linear Algebra. Developed by Google, XLA is a domain-specific compiler for linear algebra that uses whole-program optimisations to accelerate computing. It makes [BERT’s](https://analyticsindiamag.com/a-complete-tutorial-on-masked-language-modelling-using-bert/) training speed faster by almost 7.3 times.

It is designed for high-performance numerical computing. JAX was launched in 2018 and is presently used by Alphabet subsidiary DeepMind. It is similar to the numerical computing library NumPy, another library for [Python](https://analyticsindiamag.com/8-free-python-courses-for-data-scientists-in-2021/) programming. Its API for numerical functions is based on NumPy.

In this article, we’ll explore different libraries and frameworks for reinforcement learning using JAX.


https://analyticsindiamag.com/the-jax-libraries-frameworks-for-reinforcement-learning/