
https://jax.readthedocs.io/en/latest/multi_process.html


This guide explains how to use JAX in environments such as GPU clusters and [Cloud TPU](https://cloud.google.com/tpu) pods where accelerators are spread across multiple CPU hosts or JAX processes. We’ll refer to these as “multi-process” environments.

This guide specifically focuses on how to use collective communication operations (e.g. [`jax.lax.psum()`](https://jax.readthedocs.io/en/latest/_autosummary/jax.lax.psum.html#jax.lax.psum "jax.lax.psum") ) in multi-process settings, although other communication methods may be useful too depending on your use case (e.g. RPC, [mpi4jax](https://github.com/mpi4jax/mpi4jax)). If you’re not already familiar with JAX’s collective operations, we recommend starting with the [Parallel Evaluation in JAX](https://jax.readthedocs.io/en/latest/jax-101/06-parallelism.html) notebook. An important requirement of multi-process environments in JAX is direct communication links between accelerators, e.g. the high-speed interconnects for Cloud TPUs or [NCCL](https://developer.nvidia.com/nccl) for GPUs. These links allow collective operations to run across multiple processes’ worth of accelerators with high performance.

