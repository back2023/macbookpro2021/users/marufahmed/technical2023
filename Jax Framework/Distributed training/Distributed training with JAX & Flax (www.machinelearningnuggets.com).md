
https://www.machinelearningnuggets.com/distributed-training-with-jax-and-flax/


Training models on accelerators with [JAX](https://www.machinelearningnuggets.com/what-is-jax/) and Flax differs slightly from training with CPU. For instance, the data needs to be replicated in the different devices when using multiple accelerators. After that, we need to execute the training on multiple devices and aggregate the results. Flax supports TPU and GPU accelerators.  

In the last article, we saw how to [train models with the CPU](https://www.machinelearningnuggets.com/image-classification-with-jax-flax/). This article will focus on training models with Flax and [JAX](https://www.machinelearningnuggets.com/what-is-jax/) using GPUs and TPU.



