
https://github.com/pytorch/functorch


# functorch

[**Why functorch?**](https://github.com/pytorch/functorch#why-composable-function-transforms) | [**Install guide**](https://github.com/pytorch/functorch#install) | [**Transformations**](https://github.com/pytorch/functorch#what-are-the-transforms) | [**Documentation**](https://github.com/pytorch/functorch#documentation) | [**Future Plans**](https://github.com/pytorch/functorch#future-plans)

**This library is currently under heavy development - if you have suggestions on the API or use-cases you'd like to be covered, please open an github issue or reach out. We'd love to hear about how you're using the library.**

`functorch` is [JAX-like](https://github.com/google/jax) composable function transforms for PyTorch.

It aims to provide composable `vmap` and `grad` transforms that work with PyTorch modules and PyTorch autograd with good eager-mode performance.

In addition, there is experimental functionality to trace through these transformations using FX in order to capture the results of these transforms ahead of time. This would allow us to compile the results of vmap or grad to improve performance.




