
https://pytorch.org/functorch/stable/functorch.html

We’ve integrated functorch into PyTorch. As the final step of the integration, the functorch APIs are deprecated as of PyTorch 2.0. Please use the torch.func APIs instead and see the [migration guide](https://pytorch.org/docs/master/func.migrating.html) and [docs](https://pytorch.org/docs/master/func.html) for more details.





