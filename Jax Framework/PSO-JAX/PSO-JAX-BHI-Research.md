

# PSO-JAX

An accelerated Particle Swarm Optimization tool that uses [JAX](https://github.com/google/jax) key component. Here are the main benefits:

- Runs on Python.
- Supports multi-core CPUs, GPUs, and TPUs.
- Compatible with Colab environment.

## [](https://github.com/BHI-Research/PSO-JAX/tree/master/examples/knapspack#getting-started)Getting Started

### [](https://github.com/BHI-Research/PSO-JAX/tree/master/examples/knapspack#colab-example)Colab example

[![Open In Colab](https://camo.githubusercontent.com/84f0493939e0c4de4e6dbe113251b4bfb5353e57134ffd9fcab6b8714514d4d1/68747470733a2f2f636f6c61622e72657365617263682e676f6f676c652e636f6d2f6173736574732f636f6c61622d62616467652e737667)](https://colab.research.google.com/github/BHI-Research/PSO-JAX/blob/master/examples/knapspack/PSO-JAX-knapspack-example.ipynb)  

### [](https://github.com/BHI-Research/PSO-JAX/tree/master/examples/knapspack#local-example)Local example

In the case of Ubuntu 18.04

```
$ virtualenv --system-site-packages -p python3 ./venv-pso-jax
$ source venv-pso-jax/bin/activate
(venv-pso-jax) $ pip install --upgrade jax jaxlib # CPU-only version
(venv-pso-jax) $ pip install pyswarms
(venv-pso-jax) $ pip install --upgrade tensorflow
(venv-pso-jax) $ python PSO-JAX-knapspack-example.py

```

## [](https://github.com/BHI-Research/PSO-JAX/tree/master/examples/knapspack#usage)



https://github.com/BHI-Research/PSO-JAX/tree/master/examples/knapspack


