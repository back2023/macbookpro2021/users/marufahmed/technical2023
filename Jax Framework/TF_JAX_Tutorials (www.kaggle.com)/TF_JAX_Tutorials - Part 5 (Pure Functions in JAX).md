
https://www.kaggle.com/code/aakashnain/tf-jax-tutorials-part-5-pure-functions-in-jax


**Update - 23rd Dec, 2021**

We have completed the TF-JAX tutorials series. 10 notebooks that covers every fundamental aspect of both TensorFlow and JAX. Here are the links to the notebooks along with the Github repo details:

### TensorFlow Notebooks:[](https://www.kaggle.com/code/aakashnain/tf-jax-tutorials-part-5-pure-functions-in-jax#TensorFlow-Notebooks:)

- [TF_JAX_Tutorials - Part 1](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part1)
- [TF_JAX_Tutorials - Part 2](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part2)
- [TF_JAX_Tutorials - Part 3](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part3)

### JAX Notebooks:[](https://www.kaggle.com/code/aakashnain/tf-jax-tutorials-part-5-pure-functions-in-jax#JAX-Notebooks:)

- [TF_JAX_Tutorials - Part 4 (JAX and DeviceArray)](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part-4-jax-and-devicearray)
- [TF_JAX_Tutorials - Part 5 (Pure Functions in JAX)](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part-5-pure-functions-in-jax/)
- [TF_JAX_Tutorials - Part 6 (PRNG in JAX)](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part-6-prng-in-jax/)
- [TF_JAX_Tutorials - Part 7 (JIT in JAX)](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part-7-jit-in-jax)
- [TF_JAX_Tutorials - Part 8 (Vmap and Pmap)](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part-8-vmap-pmap)
- [TF_JAX_Tutorials - Part 9 (Autodiff in JAX)](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part-9-autodiff-in-jax)
- [TF_JAX_Tutorials - Part 10 (Pytrees in JAX)](https://www.kaggle.com/aakashnain/tf-jax-tutorials-part-10-pytrees-in-jax)

### Github Repo with all notebooks in one place[](https://www.kaggle.com/code/aakashnain/tf-jax-tutorials-part-5-pure-functions-in-jax#Github-Repo-with-all-notebooks-in-one-place)

[https://github.com/AakashKumarNain/TF_JAX_tutorials](https://github.com/AakashKumarNain/TF_JAX_tutorials)









