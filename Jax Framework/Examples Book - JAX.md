
https://the-examples-book.com/programming-languages/python/jax

## Overview & References

JAX is a Google research project built upon native Python and NumPy functions to improve machine research learning. The [official JAX page](https://github.com/google/jax) describes the core of the project as "an extensible system for composable function transformations," which means that JAX takes the dynamic form of Python functions and converts them to JAX-based functions that work with gradients, backpropogation, just-in-time compiling, and other JAX augmentations.


