
https://ericmjl.github.io/dl-workshop/


Thanks for stopping by to read this online book on differential programming!

## What you will learn

From a practical standpoint, this book will teach you the basics of how to use JAX, in particular the idioms and how they map onto what we might alrady know in Python.

From a more abstract standpoint, this book will give you practice with a more "functional" style of programming (in contrast to an object-oriented style or an imperative style).

My goal for you is to finish reading the book having the confidence to write differentiable numeric models of the world. The key operative word here being "differentiable" - you can calculate and evaluate the gradient of a model (written as a function) w.r.t. its parameters (which are passed in as inputs).

Along the way, you might see the connections between topics that you might be familiar with (Bayesian statistics, deep learning, and more) and differntial computing. If they pop out to you through this book and the examples in there, then I know you'll likely enjoy the thrill of seeing a new connection in your personal knowledge graph.


