

In JAX, we use the term _pytree_ to refer to a tree-like structure built out of container-like Python objects. Classes are considered container-like if they are in the pytree registry, which by default includes lists, tuples, and dicts. That is:

1. any object whose type is _not_ in the pytree container registry is considered a _leaf_ pytree;
    
2. any object whose type is in the pytree container registry, and which contains pytrees, is considered a pytree.
    

For each entry in the pytree container registry, a container-like type is registered with a pair of functions that specify how to convert an instance of the container type to a `(children, metadata)` pair and how to convert such a pair back to an instance of the container type. Using these functions, JAX can canonicalize any tree of registered container objects into tuples.


https://jax.readthedocs.io/en/latest/pytrees.html


