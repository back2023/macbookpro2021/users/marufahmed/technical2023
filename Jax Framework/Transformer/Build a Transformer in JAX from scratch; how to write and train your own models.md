
https://theaisummer.com/jax-transformer/

In this tutorial, we will explore how to develop a Neural Network (NN) with JAX. And what better model to choose than the [Transformer](https://theaisummer.com/transformer/). As JAX is growing in popularity, more and more developer teams are starting to experiment with it and incorporating it into their projects. Despite the fact that it lacks the maturity of Tensorflow or Pytorch, it provides some great features for building and training Deep Learning models.

For a solid understanding of JAX basics, check my [previous article](https://theaisummer.com/jax/) if you haven’t already. Also you can find the full code in our [Github repository](https://github.com/The-AI-Summer/JAX-examples).





https://github.com/The-AI-Summer/JAX-examples



