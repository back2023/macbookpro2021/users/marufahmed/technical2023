
[JAX](https://jax.readthedocs.io/en/latest/) is a [Python](https://www.machinelearningnuggets.com/python-for-data-science/) library offering high performance in machine learning with [XLA](https://www.tensorflow.org/xla) and Just In Time (JIT) compilation. Its API is similar to [NumPy’s](https://www.machinelearningnuggets.com/numpy-tutorial/) with a few differences. JAX ships with functionalities that aim to improve and increase speed in machine learning research. These functionalities include:

- Automatic differentiation
- Vectorization
- JIT compilation

Flax is a neural network library for JAX. This article will cover how to track JAX and Flax models with Comet.

Let’s get started.



https://heartbeat.comet.ml/tracking-jax-and-flax-models-with-comet-9adad64608da