

We will show how to convert PyTorch models to Flax. We will cover convolutions, fc layers, batch norm, and average pooling.


https://flax.readthedocs.io/en/latest/guides/convert_pytorch_to_flax.html


