
# Fourier Neural Operator (Flax)

This is an **unofficial** [Flax](https://modelzoo.co/model/fourier-neural-operator-flax) ([JAX](https://modelzoo.co/model/fourier-neural-operator-flax)) port of the [Fourier Neural Operator](https://arxiv.org/abs/2010.08895) developed by Zongyi Li et al.

Please visit [this repository](https://github.com/zongyi-li/fourier_neural_operator) to access the official PyTorch implementation of FNO form


https://modelzoo.co/model/fourier-neural-operator-flax
