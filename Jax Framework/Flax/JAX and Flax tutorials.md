
Looking for high-performance with deep learning networks? Look no further. **JAX** is here to help you.

JAX is a Python library offering high performance in machine learning with XLA and Just In Time (JIT) compilation. Its API is similar to NumPy’s, with a few differences. JAX ships with functionalities that aim to improve and increase speed in machine learning research. These functionalities include:

- Automatic differentiation
- Vectorization
- JIT compilation

We have provided various tutorials to get you started with JAX and Flax(the deep learning library for JAX). They include:

- [What is JAX?](https://www.machinelearningnuggets.com/what-is-jax/)
- [Flax vs. TensorFlow](https://www.machinelearningnuggets.com/flax-vs-tensorflow/)
- [JAX loss functions](https://www.machinelearningnuggets.com/jax-loss-functions/)
- [Activation functions in JAX and Flax](https://www.machinelearningnuggets.com/jax-flax-activation-functions/)
- [Optimizers in JAX and Flax](https://www.machinelearningnuggets.com/jax-flax-optimizers/)
- [How to load datasets in JAX using TensorFlow](https://www.machinelearningnuggets.com/how-to-load-dataset-in-jax-with-tensorflow/)
- [Building Convolutional Neural Networks in JAX and Flax](https://www.machinelearningnuggets.com/image-classification-with-jax-flax/)
- [Distributed training in JAX](https://www.machinelearningnuggets.com/distributed-training-with-jax-and-flax/)
- [Using TensorBoard in JAX and Flax](https://www.machinelearningnuggets.com/how-to-use-tensorboard-in-flax/)
- [LSTM in JAX & Flax](https://www.machinelearningnuggets.com/jax-flax-lstm/)
- [Elegy (High-level API for deep learning in JAX & Flax)](https://www.machinelearningnuggets.com/elegy-flax-jax/)
- [Transfer learning with JAX and Flax](https://www.machinelearningnuggets.com/resnet-flax/)



https://mwitiderrick.medium.com/jax-and-flax-tutorials-d3cfe8524133


