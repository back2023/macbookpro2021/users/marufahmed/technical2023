
https://www.assemblyai.com/blog/why-you-should-or-shouldnt-be-using-jax-in-2023/


Should you be using JAX in 2023? Check out our recommendations on using JAX for Deep Learning and more!


If you want to explore JAX's ecosystem, you can check out the links to some of these projects below:

- Deep Learning
    - DeepMind's [Haiku](https://github.com/deepmind/dm-haiku?ref=assemblyai.com) and [RLax](https://github.com/deepmind/rlax?ref=assemblyai.com)
    - Google's [Flax](https://github.com/google/flax?ref=assemblyai.com), [Trax](https://github.com/google/trax?ref=assemblyai.com), [Objax](https://github.com/google/objax?ref=assemblyai.com), and [Stax](https://github.com/google/jax/blob/main/jax/example_libraries/README.md?ref=assemblyai.com#neural-net-building-with-stax)
    - Google's [Scenic](https://github.com/google-research/scenic?ref=assemblyai.com)
    - [Elegy](https://github.com/poets-ai/elegy?ref=assemblyai.com)
- Studying Deep Learning
    - Google's [Neural Tangents](https://github.com/google/neural-tangents?ref=assemblyai.com) and [Spectral Density](https://github.com/google/spectral-density?ref=assemblyai.com)
- Scientific Simulation
    - Google's [JAX, M.D.](https://github.com/google/jax-md?ref=assemblyai.com) and [Computation-Thru-Dynamics](https://github.com/google-research/computation-thru-dynamics?ref=assemblyai.com)
    - [Time Machine](https://github.com/proteneer/timemachine?ref=assemblyai.com)
- Bayesian Methods
    - [NumPryo](https://github.com/pyro-ppl/numpyro?ref=assemblyai.com)
- Robotics and Control Systems
    - [Asynchronous Model-Predictive Control](https://arxiv.org/pdf/1907.03613.pdf?ref=assemblyai.com)




