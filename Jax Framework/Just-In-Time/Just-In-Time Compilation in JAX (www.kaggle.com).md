
https://www.kaggle.com/code/goktugguvercin/just-in-time-compilation-in-jax

Welcome to my JAX tutorial series. This is the fourth part of this series. As you notice, wherever you look at, you become encountered by something about JAX in machine learning domain, and naturally you become curious about what is really that JAX thing. I am here to explain and clarify your curiosity in the most detailed way. You can find the list of my tutorials below:

**JAX Tutorials:**

- [1. Introduction to JAX](https://www.kaggle.com/code/goktugguvercin/introduction-to-jax)
- [2. Gradients and Jacobians in JAX](https://www.kaggle.com/code/goktugguvercin/gradients-and-jacobians-in-jax)
- [3. Automatic Differentiation in JAX](https://www.kaggle.com/code/goktugguvercin/automatic-differentiation-in-jax)
- [4. Just-In-Time Compilation in JAX](https://www.kaggle.com/code/goktugguvercin/just-in-time-compilation-in-jax)


