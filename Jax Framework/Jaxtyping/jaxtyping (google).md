

Type annotations **and runtime type-checking** for:

1. shape and dtype of [JAX](https://github.com/google/jax) arrays; _(Now also supports PyTorch, NumPy, and TensorFlow!)_
2. [PyTrees](https://jax.readthedocs.io/en/latest/pytrees.html).


https://github.com/google/jaxtyping


