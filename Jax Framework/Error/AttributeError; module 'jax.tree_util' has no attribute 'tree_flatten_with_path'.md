

RuntimeError:  
As of JAX 0.4.0, JAX only supports TPU VMs, not the older Colab TPUs.

We recommend trying Kaggle Notebooks  
([https://www.kaggle.com/code](https://www.kaggle.com/code), click on "New Notebook" near the top) which offer  
TPU VMs. You have to create an account, log in, and verify your account to get  
accelerator support.

Source:
https://github.com/sanchit-gandhi/whisper-jax/issues/34#issuecomment-1519146158


