

https://github.com/AstroCGHA/ERA5-Reanalysis


Visualizing ECMWF's ERA5 Reanalysis from both Single and Pressure Levels and on hourly basis, at 0.25 x 0.25 degree. These dataset can be queried via NCAR/UCAR RDA ([https://rda.ucar.edu/](https://rda.ucar.edu/)), direct to THREDDS Server ([https://thredds.ucar.edu/thredds/catalog/catalog.html](https://thredds.ucar.edu/thredds/catalog/catalog.html)) or conviniently through CDS ([https://cds.climate.copernicus.eu/#!/home](https://cds.climate.copernicus.eu/#!/home)).

These notebooks that I created are used for my bachelor's thesis. However, it can be used, accordingly based on your preference i.e. location, time, etc. Thus, you can definitely make improvements out of it.

These notebooks include synoptic and mesoscale (e.g. convective and kinematic) parameters.

I've used Python ver. 3.9.x and the python packages/dependencies mostly used in these notebooks are the following;

- Cartopy,
- Matplotlib,
- MetPy (In my case, I'm using both 1.3 on Desktop and 1.5 on Laptop. Latest is better),
- NumPy, and
- Xarray

Make sure you have those!


