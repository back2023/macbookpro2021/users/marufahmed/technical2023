
https://github.com/pyvista/pyvista


## About

3D plotting and mesh analysis through a streamlined interface for the Visualization Toolkit (VTK)

PyVista is:

- _Pythonic VTK_: a high-level API to the [Visualization Toolkit](https://vtk.org/) (VTK)
- mesh data structures and filtering methods for spatial datasets
- 3D plotting made simple and built for large/complex data geometries

PyVista is a helper module for the Visualization Toolkit (VTK) that wraps the VTK library through NumPy and direct array access through a variety of methods and classes. This package provides a Pythonic, well-documented interface exposing VTK's powerful visualization backend to facilitate rapid prototyping, analysis, and visual integration of spatially referenced datasets.

This module can be used for scientific plotting for presentations and research papers as well as a supporting module for other mesh 3D rendering dependent Python modules; see Connections for a list of projects that leverage PyVista.

Share this project on X: [![tweet](https://camo.githubusercontent.com/716a9be0e73fe94bc665174a879f481aa6aa5762e7b0f0821fc506fbea68ccbb/68747470733a2f2f696d672e736869656c64732e696f2f747769747465722f75726c2e7376673f7374796c653d736f6369616c2675726c3d68747470253341253246253246736869656c64732e696f)](https://twitter.com/intent/tweet?text=Check%20out%20this%20project%20for%203D%20visualization%20in%20Python&url=https://github.com/pyvista/pyvista&hashtags=3D,visualization,Python,vtk,mesh,plotting,PyVista)

PyVista is a NumFOCUS affiliated project
