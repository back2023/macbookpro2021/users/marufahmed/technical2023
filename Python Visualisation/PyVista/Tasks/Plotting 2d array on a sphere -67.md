
https://github.com/pyvista/pyvista-support/issues/67


## Description

Does `pyvista` allow for "stretching" a 2D array over a sphere?  
(Related to [#52](https://github.com/pyvista/pyvista-support/issues/52), but simpler.)

I would like to create a 3D sphere with a 2D array as its surface. I tried creating a VTK sphere with specific resolution and then filling cells with flattened data, e.g.

