

https://discretize.simpeg.xyz/en/main/examples/plot_pyvista_laguna.html


The example demonstrates the how to use the VTK interface via the [pyvista library](http://docs.pyvista.org/) . To run this example, you will need to [install pyvista](http://docs.pyvista.org/getting-started/installation.html) .

- contributed by [@banesullivan](https://github.com/banesullivan)
    

Using the inversion result from the example notebook [plot_laguna_del_maule_inversion.ipynb](http://docs.simpeg.xyz/content/examples/20-published/plot_laguna_del_maule_inversion.html)

