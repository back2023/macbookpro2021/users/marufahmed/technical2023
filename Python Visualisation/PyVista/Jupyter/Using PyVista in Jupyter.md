
https://tutorial.pyvista.org/tutorial/00_jupyter/index.html

PyVista is designed to be used in Jupyter notebooks. This section of the tutorial will walk you through the basics of using PyVista in Jupyter notebooks and will be a reference guide for you when configuring PyVista to work in Jupyter.

