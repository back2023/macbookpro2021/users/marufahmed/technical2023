
[Kitware’s Trame](https://kitware.github.io/trame/index.html) is an open-source platform for creating interactive and powerful visual analytics applications. Based on Python, and leveraging platforms such as VTK, ParaView, and Vega, it is possible to create web-based applications in minutes.


