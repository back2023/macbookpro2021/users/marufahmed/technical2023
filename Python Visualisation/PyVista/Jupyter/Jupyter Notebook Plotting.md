
https://docs.pyvista.org/version/stable/user-guide/jupyter/index.html

Plot with `pyvista` interactively within a [Jupyter](https://jupyter.org/) notebook.

Note

We recommend using the Trame-based backed. See [Trame Jupyter Backend for PyVista](https://docs.pyvista.org/version/stable/user-guide/jupyter/trame.html#trame-jupyter).

## Supported Modules[](https://docs.pyvista.org/version/stable/user-guide/jupyter/index.html#supported-modules "Permalink to this heading")

The PyVista module supports a variety of backends when plotting within a jupyter notebook:

- Server and client-side rendering with PyVista streaming to the notebook through [trame](https://github.com/Kitware/trame/)
    
- Static images.
    

- [Trame Jupyter Backend for PyVista](https://docs.pyvista.org/version/stable/user-guide/jupyter/trame.html)

