
https://docs.pyvista.org/version/stable/


# Overview[](https://docs.pyvista.org/version/stable/#overview "Permalink to this heading")

PyVista is:

- _Pythonic VTK_: a high-level API to the [Visualization Toolkit](https://vtk.org/) (VTK)
- mesh data structures and filtering methods for spatial datasets
- 3D plotting made simple and built for large/complex data geometries

PyVista is a helper library for the Visualization Toolkit (VTK) that takes a different approach on interfacing with VTK through NumPy and direct array access. This package provides a Pythonic, well-documented interface exposing VTK’s powerful visualization backend to facilitate rapid prototyping, analysis, and visual integration of spatially referenced datasets.

This module can be used for scientific plotting for presentations and research papers as well as a supporting module for other mesh dependent Python modules.

Share this project on X: [![tweet](https://img.shields.io/twitter/url.svg?style=social&url=http%3A%2F%2Fshields.io)](https://twitter.com/intent/tweet?text=Check%20out%20this%20project%20for%203D%20visualization%20in%20Python&url=https://github.com/pyvista/pyvista&hashtags=3D,visualization,Python,vtk,mesh,plotting,PyVista)

Want to test-drive PyVista? Check out our live examples on MyBinder: [![Launch on Binder](https://static.mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/pyvista/pyvista-examples/master)


