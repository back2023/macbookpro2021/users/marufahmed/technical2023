
https://analyticsindiamag.com/hands-on-guide-to-pyvista-a-python-package-for-3d-plotting-and-mesh-analysis/


PyVista (formerly known as ‘vtki’) is a flexible helper module and a high-level API for the Visualization Toolkit (VTK). It is a streamlined interface for the VTK, enabling mesh analysis and plotting 3D figures using Python code. It was introduced by _C. Bane Sullivan_ and _Alexander A. Kaszynski_ in May 2019 ([research paper](https://joss.theoj.org/papers/10.21105/joss.01450)). 

Before going into the details of PyVista, let us have a brief overview of VTK.


