
https://github.com/pyvista/pyvista-support/issues/199


The `pyvista_ndarray` is a NumPy `ndarray`. We’ve simply subclassed it for some internal management of data between VTK and numpy.

Use those arrays in the same way as any numpy array.

You can iterate over those arrays the same way as any numpy array

Also. I’m going to transfer this to the support forum