

https://discourse.vtk.org/t/numpy-3d-array-into-vtk-data-types-for-volume-rendering/3455


[Bane Sullivan](https://discourse.vtk.org/u/banesullivan)

[Jun '20](https://discourse.vtk.org/t/numpy-3d-array-into-vtk-data-types-for-volume-rendering/3455/3 "Post date")

[PyVista 196](https://docs.pyvista.org/) makes this easy:

`import pyvista as pv  data = pv.wrap(my_3d_numpy_array) data.plot(volume=True) # Volume render`

See:

- [https://docs.pyvista.org/examples/00-load/create-uniform-grid.html#creating-a-uniform-grid 268](https://docs.pyvista.org/examples/00-load/create-uniform-grid.html#creating-a-uniform-grid)
- [https://docs.pyvista.org/examples/02-plot/volume.html#volume-rendering 282](https://docs.pyvista.org/examples/02-plot/volume.html#volume-rendering)
