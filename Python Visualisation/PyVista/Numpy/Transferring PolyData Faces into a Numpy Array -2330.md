
https://github.com/pyvista/pyvista/issues/2330


```python
from typing import List
from pyvista import _vtk, PolyData
from numpy import split, ndarray

def face_arrays(mesh: PolyData) -> List[ndarray]:
    cells = mesh.GetPolys()
    c = _vtk.vtk_to_numpy(cells.GetConnectivityArray())
    o = _vtk.vtk_to_numpy(cells.GetOffsetsArray())
    return split(c, o[1:-1])
```