
https://fangohr.github.io/vacancies/master-project.html


We are looking for physics students with interest in programming, data analysis & visualisation, and software engineering who want to do their Masters project with us (Einarbeitungsprojekt, Vorbereitungsprojekt, Master-Arbeit).

- Task: contribute to data analysis framework for 3d and 4d simulation results
    
- Expectations:
    
    - programming skills and interest/desire to improve those
    - Python experience, knowledge of any of the tools listed below is useful
- Likely tools you will be using include:
    
    - git
    - Python
    - py.test - we like testing our software
    - Jupyter notebooks
    - numpy & xarray - dealing with n-dimensional arrays of data
    - scipy & pandas - scientific computing and data science tools
    - matplotlib & holoviews - 2d plots for parameter space exploration ([interactive holoviews example](https://octopus-code.gitlab.io/postopus/notebooks/holoviews_with_postopus.html#Generating-a-holoviews-Image))
    - PyVista - interactive 3d plots in notebooks
-

