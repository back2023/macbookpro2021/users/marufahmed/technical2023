
https://github.com/pyvista/pyvista-xarray/tree/main
# PyVista xarray

[![PyPI](https://camo.githubusercontent.com/5fd5b96df2db65454ceba377ba9b68dee18f45ce530c0d85b93e789c2b807703/68747470733a2f2f696d672e736869656c64732e696f2f707970692f762f707976697374612d7861727261792e7376673f6c6f676f3d707974686f6e266c6f676f436f6c6f723d7768697465)](https://pypi.org/project/pyvista-xarray/) [![codecov](https://camo.githubusercontent.com/2c33a82033589c0162da0512ed2adb0328916ba9949519fd0cf69ad062b98cf3/68747470733a2f2f636f6465636f762e696f2f67682f707976697374612f707976697374612d7861727261792f6272616e63682f6d61696e2f67726170682f62616467652e7376673f746f6b656e3d34425344565630574f47)](https://codecov.io/gh/pyvista/pyvista-xarray) [![MyBinder](https://camo.githubusercontent.com/581c077bdbc6ca6899c86d0acc6145ae85e9d80e6f805a1071793dbe48917982/68747470733a2f2f6d7962696e6465722e6f72672f62616467655f6c6f676f2e737667)](https://mybinder.org/v2/gh/pyvista/pyvista-xarray/HEAD)

xarray DataArray accessors for PyVista to visualize datasets in 3D

## [](https://github.com/pyvista/pyvista-xarray#-usage)🚀 Usage

You must `import pvxarray` in order to register the `DataArray` accessor with xarray. After which, a `pyvista` namespace of accessors will be available.


Try on MyBinder: [https://mybinder.org/v2/gh/pyvista/pyvista-xarray/HEAD](https://mybinder.org/v2/gh/pyvista/pyvista-xarray/HEAD)

