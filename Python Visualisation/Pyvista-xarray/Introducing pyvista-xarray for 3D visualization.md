

https://discourse.pangeo.io/t/introducing-pyvista-xarray-for-3d-visualization/2561


I’ve recently been working on [`pyvista-xarray` 37](https://github.com/pyvista/pyvista-xarray) to create a `DataArray` accessor that builds a direct line of interoperability between xarray and VTK/PyVista for 3D visualization.

This package is still in its early stages and I’d love to solicit as much feedback as possible from the xarray community about the types of data and ways in which you all would like to see this package improve to address 3D visualization needs.

Additionally, this package provides a DataSet IO backend for reading VTK files with xarray’s `open_dataset` function.

Please open an issue for any feedback you have or any new features you’d like to see!

