
https://anaconda.org/anaconda/xorg-x11-server-xvfb-cos6-x86_64


### Description

Xvfb (X Virtual Frame Buffer) is an X server that is able to run on machines with no display hardware and no physical input devices. Xvfb simulates a dumb framebuffer using virtual memory. Xvfb does not open any devices, but behaves otherwise as an X display. Xvfb is normally used for testing servers.

