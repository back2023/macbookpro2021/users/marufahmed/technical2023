

https://github.com/cgoldberg/xvfbwrapper


## About xvfbwrapper:

xvfbwrapper is a python module for controlling virtual displays with Xvfb.

---

## [](https://github.com/cgoldberg/xvfbwrapper#what-is-xvfb)What is Xvfb?:

Xvfb (X virtual framebuffer) is a display server implementing the X11 display server protocol. It runs in memory and does not require a physical display. Only a network layer is necessary.

Xvfb is useful for running acceptance tests on headless servers.

---

## [](https://github.com/cgoldberg/xvfbwrapper#install-xvfbwrapper-from-pypi)Install xvfbwrapper from PyPI:

```shell
pip install xvfbwrapper
```

---

## [](https://github.com/cgoldberg/xvfbwrapper#system-requirements)System Requirements:

- X11 Windowing System
- Xvfb (sudo apt-get install xvfb, yum install xorg-x11-server-Xvfb, etc)
- Python 2.7 or 3.4+

