
Here is a demo of how to use PyVista to visualize some data in netCDF files! Can easily be modified to use `xarray` instead.



Then open up the Jupyter notebook to get something like the following. FYI: change the `notebook=True` arguments for the `Plotter` class to `False` to have a pop-out, interactive scene.
 