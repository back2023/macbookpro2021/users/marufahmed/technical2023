
https://github.com/giannisdravilas/Weather-Plotting
# Weather-Plotting

🌀❄️🌩️ This repository contains some examples for creating 2d and 3d weather plots using matplotlib and cartopy libraries in python3.  
Every directory also contains samples depicting the result of the provided code.

**geopHeight3d**

Creates a 3d plot of the Geopotential Height on the level of 500 hPa on a given date, according to ICON-EU forecasting model's real-time data provided by the DWD. The z axis has a reduced scale and limits for a better representation of the 3d surface. A basemap using cartopy, is also used, combined with a simple contour plot on level 0, producing a projection of the data on a simple x,y layer.