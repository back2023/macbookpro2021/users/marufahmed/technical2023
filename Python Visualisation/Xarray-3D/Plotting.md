
https://docs.xarray.dev/en/latest/user-guide/plotting.html


For more extensive plotting applications consider the following projects:

- [Seaborn](https://seaborn.pydata.org/): “provides a high-level interface for drawing attractive statistical graphics.” Integrates well with pandas.
    
- [HoloViews](https://holoviews.org/) and [GeoViews](https://geoviews.org/): “Composable, declarative data structures for building even complex visualizations easily.” Includes native support for xarray objects.
    
- [hvplot](https://hvplot.pyviz.org/): `hvplot` makes it very easy to produce dynamic plots (backed by `Holoviews` or `Geoviews`) by adding a `hvplot` accessor to DataArrays.
    
- [Cartopy](https://scitools.org.uk/cartopy/docs/latest/): Provides cartographic tools.

