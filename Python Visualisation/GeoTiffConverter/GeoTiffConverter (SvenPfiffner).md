
https://github.com/SvenPfiffner/GeoTiffConverter


## About

This Python package and web UI allow you to seamlessly convert GeoTiff data into 3D meshes, making geospatial data visualization and analysis a breeze. Whether you're working with elevation maps, terrain models, or any other geospatial information stored in GeoTiff files, this tool simplifies the process of turning raw data into 3D visualizations.



