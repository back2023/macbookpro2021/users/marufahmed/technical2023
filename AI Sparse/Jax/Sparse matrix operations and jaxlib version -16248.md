
https://github.com/google/jax/discussions/16248


I am hoping to do sparse matrix operations on some data because I am finding that dense matrix operations are taking too long. In particular, I want to use the `jax.experimental.sparse.linalg.spsolve()` because I am inverting a large, sparse matrix using `jax.numpy.linalg.solve()` and finding that it takes prohibitively long. I tried following the nice documentation here for using sparse matrices in JAX: [https://jax.readthedocs.io/en/latest/jax.experimental.sparse.html#module-jax.experimental.sparse]

But I found that only the CSR data structure can be used with `spsolve()`. For one, it seems like the BCOO data structure is better supported -- will BCOO become useable in `spsolve()` any time soon?

Furthermore, I'm having trouble even getting `spsolve()` to run using GPU. I had to install a specific (and old) version of jaxlib a while back in order to be compatible with the CUDA drivers on my computing server. However, I get an error message saying `ValueError: spsolve requires jaxlib version 86 or above.` But I can't update my jaxlib version or I'll lose compatibility with my GPUs.

I made a new conda environment to test this out, using the most recent versions of jaxlib and jax, and sure enough I can now call `spsolve()`, but I get the warning that `No GPU/TPU found, falling back to CPU. (Set TF_CPP_MIN_LOG_LEVEL=0 and rerun for more info.)`

Any ideas on what I can try here? It seems like a very tricky spot to be in where my CUDA drivers are too old to be used with `spsolve()`, and my current implementation without sparse operations is looking to be too slow to be used in practice.







