
https://jax.readthedocs.io/en/latest/jax.experimental.sparse.html

The [`jax.experimental.sparse`](https://jax.readthedocs.io/en/latest/jax.experimental.sparse.html#module-jax.experimental.sparse "jax.experimental.sparse") module includes experimental support for sparse matrix operations in JAX. It is under active development, and the API is subject to change. The primary interfaces made available are the [`BCOO`](https://jax.readthedocs.io/en/latest/_autosummary/jax.experimental.sparse.BCOO.html#jax.experimental.sparse.BCOO "jax.experimental.sparse.BCOO") sparse array type, and the [`sparsify()`](https://jax.readthedocs.io/en/latest/_autosummary/jax.experimental.sparse.sparsify.html#jax.experimental.sparse.sparsify "jax.experimental.sparse.sparsify") transform.

## Batched-coordinate (BCOO) sparse matrices[](https://jax.readthedocs.io/en/latest/jax.experimental.sparse.html#batched-coordinate-bcoo-sparse-matrices "Permalink to this heading")

The main high-level sparse object currently available in JAX is the [`BCOO`](https://jax.readthedocs.io/en/latest/_autosummary/jax.experimental.sparse.BCOO.html#jax.experimental.sparse.BCOO "jax.experimental.sparse.BCOO"), or _batched coordinate_ sparse array, which offers a compressed storage format compatible with JAX transformations, in particular JIT (e.g. [`jax.jit()`](https://jax.readthedocs.io/en/latest/_autosummary/jax.jit.html#jax.jit "jax.jit")), batching (e.g. [`jax.vmap()`](https://jax.readthedocs.io/en/latest/_autosummary/jax.vmap.html#jax.vmap "jax.vmap")) and autodiff (e.g. [`jax.grad()`](https://jax.readthedocs.io/en/latest/_autosummary/jax.grad.html#jax.grad "jax.grad")).









