
https://www.scaler.com/topics/pytorch/pytorch-pruning/


## Introduction

Among many bottlenecks and/or challenges that developers are faced with when deploying very large deep neural models in production, one challenge is their massive size. for instance, GPT-3, which is a prompt-based language model used in natural language processing for several tasks, contains as high as 175 billion parameters which are a massive 700 GB when operating in floating point precision with 32 bits (which is the standard of deep learning libraries like PyTorch).

This also means that for models with such a high number of parameters, a large number of vector and matrix calculations need to be performed to generate predictions when input data is fed to the models. This, yet again, means very high inference times from the model that pose latency and throughput issues in production.

To be able to avoid these challenges, there are several model compression and optimization techniques that aim to reduce the model size without compromising much on the performance front - model pruning is one such technique that can substantially speed up model inference time by reducing the size of the model.

This article discusses model pruning in terms of what it is, certain caveats of it along with the benefits it brings, and also demonstrates how to implement PyTorch Pruning techniques to prune our deep learning models.





