
https://olegpolivin.medium.com/experiments-in-neural-network-pruning-in-pytorch-c18d5b771d6d


# Introduction

The aim of this article is to write down and describe my effort to dive into pruning methods for neural networks. It reflects the knowledge I gained and which I share for a discussion. It is not a tutorial or a teaching material: I am in the process of discovering this field myself.

# Key takeaways

What I discovered is that on one hand there is an active research in the field. On the other hand, doing pruning in practice is not a well-established field. There are methods that implement pruning in PyTorch, but they do not lead to faster inference time or memory savings. The reason for that is that sparse operations are not currently supported in PyTorch (version 1.7), and so just assigning weights, neurons or channels to zero does not lead to real neural network compression. Thus, experiments below in the **Results** section give _theoretical_ improvements and not real ones.

I also provide a different way to compress a neural network which is knowledge distillation.

All code could be found at [my github repository](https://github.com/olegpolivin/pruningExperiments).



