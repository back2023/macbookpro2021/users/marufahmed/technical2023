
https://pytorch.org/tutorials/intermediate/pruning_tutorial.html


https://colab.research.google.com/github/pytorch/tutorials/blob/gh-pages/_downloads/7126bf7beed4c4c3a05bcc2dac8baa3c/pruning_tutorial.ipynb

https://pytorch.org/tutorials/_downloads/7126bf7beed4c4c3a05bcc2dac8baa3c/pruning_tutorial.ipynb

https://github.com/pytorch/tutorials/blob/master/intermediate_source/pruning_tutorial.py


State-of-the-art deep learning techniques rely on over-parametrized models that are hard to deploy. On the contrary, biological neural networks are known to use efficient sparse connectivity. Identifying optimal techniques to compress models by reducing the number of parameters in them is important in order to reduce memory, battery, and hardware consumption without sacrificing accuracy. This in turn allows you to deploy lightweight models on device, and guarantee privacy with private on-device computation. On the research front, pruning is used to investigate the differences in learning dynamics between over-parametrized and under-parametrized networks, to study the role of lucky sparse subnetworks and initializations (“[lottery tickets](https://arxiv.org/abs/1803.03635)”) as a destructive neural architecture search technique, and more.

In this tutorial, you will learn how to use `torch.nn.utils.prune` to sparsify your neural networks, and how to extend it to implement your own custom pruning technique.










