
https://leimao.github.io/article/Neural-Networks-Pruning/

## Introduction

Neural networks have become extremely popular nowadays since they usually can “solve” complex artificial intelligence problems that other conventional models cannot. Currently, the convention is with more data and larger neural network, we could archive better accuracy from training the neural networks. GPT-33, the state-of-the-art language model, is made up of 175175 billion parameters (700700 GB for FP3232 precision). While these neural networks are doing impressively well, they are costly to run and not applicable for edge devices.

Sparse neural networks, comparing to dense neural network, have sparse weight and neuron tensors in the model. Therefore, the number of useful parameters in the model is smaller, and the model will be more suitable for running on edge devices. Recent studies show that [sparse neural networks could perform as good as its corresponding dense neural networks](https://arxiv.org/abs/1803.03635). Such sparse neural network could be found by pruning dense neural networks.

In this article post, I would like to elucidate the mathematics for pruning neural networks and discuss the general protocol for pruning neural works.









