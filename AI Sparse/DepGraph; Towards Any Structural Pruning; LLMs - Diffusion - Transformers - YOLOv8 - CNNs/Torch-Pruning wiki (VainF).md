
https://github.com/VainF/Torch-Pruning/wiki



# Home

Gongfan Fang edited this page last month · [7 revisions](https://github.com/VainF/Torch-Pruning/wiki/Home/_history)

###  Pages 7

- [Home](https://github.com/VainF/Torch-Pruning/wiki)
    
- [0. Paper List](https://github.com/VainF/Torch-Pruning/wiki/0.-Paper-List)
    
- [1. Installation](https://github.com/VainF/Torch-Pruning/wiki/1.-Installation)
    
- [2. Quick Start](https://github.com/VainF/Torch-Pruning/wiki/2.-Quick-Start)
    
- [3. DepGraph & Group](https://github.com/VainF/Torch-Pruning/wiki/3.-DepGraph-&-Group)
    
- [4. High‐level Pruners](https://github.com/VainF/Torch-Pruning/wiki/4.-High%E2%80%90level-Pruners)
    
- [Frequently Asked Questions](https://github.com/VainF/Torch-Pruning/wiki/Frequently-Asked-Questions)
    
 

