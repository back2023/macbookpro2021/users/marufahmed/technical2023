
https://github.com/VainF/Torch-Pruning

### Towards Any Structural Pruning

Torch-Pruning (TP) is a library for structural pruning with the following features:

- **General-purpose Pruning Toolkit:** TP enables structural pruning for a wide range of deep neural networks, including [Large Language Models (LLMs)](https://github.com/horseee/LLM-Pruner), [Diffusion Models](https://github.com/VainF/Diff-Pruning), [Yolov7](https://github.com/VainF/Torch-Pruning/blob/master/examples/yolov7), [yolov8](https://github.com/VainF/Torch-Pruning/blob/master/examples/yolov8), [Vision Transformers](https://github.com/VainF/Torch-Pruning/blob/master/examples/transformers), [Swin Transformers](https://github.com/VainF/Torch-Pruning/blob/master/examples/transformers#swin-transformers-from-hf-transformers), [BERT](https://github.com/VainF/Torch-Pruning/blob/master/examples/transformers#bert-from-hf-transformers), FasterRCNN, SSD, ResNe(X)t, ConvNext, DenseNet, ConvNext, RegNet, DeepLab, etc. Different from [torch.nn.utils.prune](https://pytorch.org/tutorials/intermediate/pruning_tutorial.html) that zeroizes parameters through masking, Torch-Pruning deploys an algorithm called **[DepGraph](https://openaccess.thecvf.com/content/CVPR2023/html/Fang_DepGraph_Towards_Any_Structural_Pruning_CVPR_2023_paper.html)** to remove parameters physically. Check our [Paper List](https://github.com/VainF/Torch-Pruning/wiki/0.-Paper-List) for more details.
- **[Examples](https://github.com/VainF/Torch-Pruning/blob/master/examples)**: Play around with off-the-shelf models from Huggingface Transformers, Timm, Torchvision, Yolo, etc.
- **[Benchmark](https://github.com/VainF/Torch-Pruning/blob/master/benchmarks)**: Reproduce the our results in the DepGraph paper.

For more technical details, please refer to our CVPR'23 paper:

> [**DepGraph: Towards Any Structural Pruning**](https://openaccess.thecvf.com/content/CVPR2023/html/Fang_DepGraph_Towards_Any_Structural_Pruning_CVPR_2023_paper.html)  
> _[Gongfan Fang](https://fangggf.github.io/), [Xinyin Ma](https://horseee.github.io/), [Mingli Song](https://person.zju.edu.cn/en/msong), [Michael Bi Mi](https://dblp.org/pid/317/0937.html), [Xinchao Wang](https://sites.google.com/site/sitexinchaowang/)_  
> _[Learning and Vision Lab](http://lv-nus.org/), National University of Singapore_

### [](https://github.com/VainF/Torch-Pruning#update)Update:

- 2023.09.06 [Pruning & Finetuning Examples for Vision Transformers, Swin Transformers, Bert](https://github.com/VainF/Torch-Pruning/blob/master/examples/transformers).
- 2023.07.19 🚀 Support LLaMA, LLaMA-2, Vicuna, Baichuan, Bloom in [LLM-Pruner](https://github.com/horseee/LLM-Pruner)
- 2023.05.20 🚀 [**LLM-Pruner: On the Structural Pruning of Large Language Models**](https://github.com/horseee/LLM-Pruner) [_[arXiv]_](https://arxiv.org/abs/2305.11627)
- 2023.05.19 [Structural Pruning for Diffusion Models](https://github.com/VainF/Diff-Pruning) [_[arXiv]_](https://arxiv.org/abs/2305.10924)
- 2023.04.15 [Pruning and Post-training for YOLOv7 / YOLOv8](https://github.com/VainF/Torch-Pruning/blob/master/benchmarks/examples)







