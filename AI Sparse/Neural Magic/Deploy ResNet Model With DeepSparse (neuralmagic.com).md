
https://neuralmagic.com/blog/sparsify-image-classification-models-faster-with-sparseml-and-deep-lake/


When training is complete, export and deploy the model using [DeepSparse](https://neuralmagic.com/deepsparse/). DeepSparse is an inference runtime that offers GPU-class performance on commodity CPUs. Use the `ModuleExporter` utility from SparseML to export the model to ONNX.






