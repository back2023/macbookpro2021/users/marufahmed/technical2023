
https://www.youtube.com/watch?v=vlhWmd75GPs


[NEURAL MAGIC](https://www.youtube.com/results?search_query=Neural+Magic&sp=EiG4AQHCARtDaElKdlQ3blItdDM0NGtSbnVGWjFWaXExX3M%253D)

Learn about deep learning model sparsification and how it helps you deliver SOTA inference performance on commodity CPUs. The video gives an overview of sparsification and its different techniques, including pruning, quantization, and knowledge distillation. Furthermore, the video introduces an approach called "compound sparsification" that combines all techniques together for best-in-class results. 

Related Videos: Intro to Neural Magic & Software-Delivered AI:    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Intro to Neural Magic & Software-Deli...](https://www.youtube.com/watch?v=Pw_Pi7tKcL8&t=0s)   
Intro to SparseML (Sparsification from Scratch):    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Intro to SparseML](https://www.youtube.com/watch?v=_RNOfnTPtMY&t=0s)  
Intro to DeepSparse Runtime:    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Intro to DeepSparse Runtime](https://www.youtube.com/watch?v=CfFookHaiZE&t=0s)   
Intro to SparseZoo:    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Intro to SparseZoo](https://www.youtube.com/watch?v=EnrhELrF2sI&t=0s)   
Intro to Sparse Transfer Learning:    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Use Sparse Transfer Learning to Creat...](https://www.youtube.com/watch?v=-kloARsNbe4&t=0s)   
Accelerate Image Classification Tasks With Sparsity and the DeepSparse Runtime:    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Accelerate Image Classification Tasks...](https://www.youtube.com/watch?v=qLEkVP-Ajr0&t=0s)   
Accelerate Image Segmentation Tasks With Sparsity and the DeepSparse Runtime:    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Accelerate Image Segmentation Tasks W...](https://www.youtube.com/watch?v=TLvVzHb42Wg&t=0s)   
Accelerate Object Detection Tasks With Sparsity and the DeepSparse Runtime:    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Accelerate Object Detection Tasks Wit...](https://www.youtube.com/watch?v=3e1P6XBZk5g&t=0s)   
Accelerate NLP Tasks With Sparsity and the DeepSparse Runtime:    [![](https://www.gstatic.com/youtube/img/watch/yt_favicon.png) • Accelerate NLP Tasks With Sparsity an...](https://www.youtube.com/watch?v=qHqu9p1A6wU&t=0s)  
