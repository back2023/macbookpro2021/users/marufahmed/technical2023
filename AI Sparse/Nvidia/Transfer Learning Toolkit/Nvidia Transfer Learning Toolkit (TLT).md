
https://github.com/giktech/transfer-learning-nvidia

The TLT is a convenience framework that's supposed to help speed up the development of custom DL video applications.

- [https://developer.nvidia.com/transfer-learning-toolkit](https://developer.nvidia.com/transfer-learning-toolkit)
- [https://devblogs.nvidia.com/accelerating-video-analytics-tlt/](https://devblogs.nvidia.com/accelerating-video-analytics-tlt/)
- [https://devblogs.nvidia.com/transfer-learning-toolkit-pruning-intelligent-video-analytics/](https://devblogs.nvidia.com/transfer-learning-toolkit-pruning-intelligent-video-analytics/)
- The [Getting Started Guide](https://github.com/MIDS-scaling-up/v2/blob/master/week13/hw/Transfer-Learning-Toolkit-Getting-Started-Guide-IVAOpenBeta.pdf) (also available in the authenticated area of the developer Nvidia portal)







