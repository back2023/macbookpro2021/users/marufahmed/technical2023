
https://docs.nvidia.com/metropolis/TLT/tlt-user-guide/


https://docs.nvidia.com/tao/tao-toolkit/text/overview.html

NVIDIA [TAO Toolkit](https://developer.nvidia.com/transfer-learning-toolkit) is a low-code AI toolkit built on TensorFlow and PyTorch, which simplifies and accelerates the model training process by abstracting away the complexity of AI models and the deep learning framework. With TAO, users can select one of 100+ pre-trained vision AI models from NGC and fine-tune and customize on their own dataset without writing a single line of code. The output of TAO is a trained model in ONNX format that can be deployed on any platform that supports ONNX.





