
https://blogs.nvidia.com/blog/2019/02/07/what-is-transfer-learning/


_Editor’s note: The name of the NVIDIA Transfer Learning Toolkit was changed to NVIDIA TAO Toolkit in August 2021. All references to the name have been updated in this blog._

You probably have a career. But hit the books for a graduate degree or take [online certificate courses](https://www.nvidia.com/en-us/deep-learning-ai/education/) by night, and you could start a new career building on your past experience.

Transfer learning is the same idea. This [deep learning](https://blogs.nvidia.com/blog/2015/02/19/deep-learning-2/) technique enables developers to harness a neural network used for one task and apply it to another domain.

Take image recognition. Let’s say that you want to [identify horses](https://blogs.nvidia.com/blog/2018/11/08/magic-ai-analytics-horse-intelligence-gpu/), but there aren’t any publicly available algorithms that do an adequate job. With transfer learning, you begin with an existing [convolutional neural network](https://blogs.nvidia.com/blog/2018/09/05/whats-the-difference-between-a-cnn-and-an-rnn/) commonly used for image recognition of other animals, and you tweak it to train with horses.








