
https://developer.nvidia.com/tao-toolkit


Eliminate the need for mountains of data and an army of data scientists as you create AI/machine learning models and speed up the development process with [transfer learning](https://blogs.nvidia.com/blog/2019/02/07/what-is-transfer-learning/) . This powerful technique instantly transfers learned features from an existing neural network model to a new customized one.  
  
The open-source NVIDIA TAO Toolkit, built on TensorFlow and PyTorch, uses the power of transfer learning while simultaneously simplifying the model training process and optimizing the model for inference throughput on practically any platform. The result is an ultra-streamlined workflow. Take your own models or pre-trained models, adapt them to your own real or synthetic data, then optimize for inference throughput. All without needing AI expertise or large training datasets.








