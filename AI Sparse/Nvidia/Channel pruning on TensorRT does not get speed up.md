
https://forums.developer.nvidia.com/t/channel-pruning-on-tensorrt-does-not-get-speed-up/181825


Hi,  
Request you to share the model, script, profiler and performance output if not shared already so that we can help you better.  
Alternatively, you can try running your model with trtexec command.  
[https://github.com/NVIDIA/TensorRT/tree/master/samples/opensource/trtexec 2](https://github.com/NVIDIA/TensorRT/tree/master/samples/opensource/trtexec)

While measuring the model performance, make sure you consider the latency and throughput of the network inference, excluding the data pre and post-processing overhead.  
Please refer below link for more details:  
[https://docs.nvidia.com/deeplearning/tensorrt/archives/tensorrt-722/best-practices/index.html#measure-performance 8](https://docs.nvidia.com/deeplearning/tensorrt/archives/tensorrt-722/best-practices/index.html#measure-performance)

Thanks!


[@OnePieceOfDeepLearning](https://forums.developer.nvidia.com/u/onepieceofdeeplearning),

Yes, it’s usually better if the number of channels is multiple of 8 (for fp16) or 32 (for INT8).







