
https://pub.aimind.so/a-guide-for-pruning-neural-networks-for-complete-beginners-6ad8830741f4

This article goes hand in hand with my previous article on [Quantization](https://medium.com/ai-mind-labs/quantization-guide-for-complete-beginners-ac4555cf295f), in the theme of Model Compression in Deep Learning.


In this article, we will demystify ‘Pruning’ and guide you through the process of pruning models from scratch. I strongly encourage you to explore our comprehensive notebook on [Google Colab](https://drive.google.com/file/d/1ifjwvTXy0yxuTPD6fJrRuLODcRyG5tE_/view?usp=sharing) or [Github](https://github.com/FrancoisPorcher/awesome-medium-tutorials/tree/main/0003%20-%20pruning) for a hands-on experience with quantization.










