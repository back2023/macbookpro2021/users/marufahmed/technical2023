


Google AI has released an [open-source](https://arxiv.org/pdf/2304.14082.pdf) pruning and sparse training library for machine learning researchers called JaxPruner. The library was created to provide a comprehensive toolkit that would make it easier for researchers to quickly develop and assess sparsity concepts against various dynamic benchmarks.


If you’re interested in seeing for yourself, you can find more documentation via its [GitHub repository](https://github.com/google-research/jaxpruner).







