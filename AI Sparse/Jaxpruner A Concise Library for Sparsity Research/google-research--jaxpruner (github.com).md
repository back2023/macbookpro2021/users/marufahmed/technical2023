
https://github.com/google-research/jaxpruner


Paper: [arxiv.org/abs/2304.14082](https://arxiv.org/abs/2304.14082)

## [](https://github.com/google-research/jaxpruner#introduction)Introduction

_JaxPruner_, an open-source JAX-based pruning and sparse training library for machine learning research. _JaxPruner_ aims to accelerate research on sparse neural networks by providing concise implementations of popular pruning and sparse training algorithms with minimal memory and latency overhead. Algorithms implemented in _JaxPruner_ use a common API and work seamlessly with the popular optimization library Optax, which, in turn, enables easy integration with existing JAX based libraries. We demonstrate this ease of integration by providing examples in three different codebases: [scenic](https://github.com/google-research/scenic), [t5x](https://github.com/google-research/t5x) and [dopamine](https://github.com/google/dopamine) and [fedjax](https://github.com/google/fedjax).

We believe a sparsity library in Jax has the potential to accelerate sparsity research. This is because:

- Functional nature of jax makes it easy to modify parameters and masks.
- Jax is easy debug.
- Jax libraries and their usage in research is increasing.
- For further motivation read [why Deepmind uses jax here](https://www.deepmind.com/blog/using-jax-to-accelerate-our-research).

There are exciting developments for accelerating sparsity in neural networks (K:N sparsity, CPU-acceleration, activation sparsity) and various libraries aim to enable such acceleration (todo). _JaxPruner_ focuses mainly on accelerating algorithms research for sparsity. We mock sparsity by using binary masks and use dense operations for simulating sparsity. In the longer run, we also plan to provide integration with the [jax.experimental.sparse](https://jax.readthedocs.io/en/latest/jax.experimental.sparse.html), aim to reduce the memory footprint of our models.

_JaxPruner_ has 3 tenets:

- **Easy Integration**: requires minimal changes to use.
- **Research First**: provides strong baselines and is easy to modify.
- **Minimal Overhead**: runs as fast as (dense) baseline.









