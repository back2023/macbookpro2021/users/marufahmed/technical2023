
https://kelvins.esa.int/proba-v-super-resolution/data/

We collected satellite data from the PROBA-V mission of the European Space Agency from **74** hand-selected regions around the globe at different points in time. 
- The data is composed of radiometrically and geometrically corrected Top-Of-Atmosphere (TOA) reflectances for the RED and NIR spectral bands at **300m** and **100m** resolution in Plate Carrée projection. 
- The **300m** resolution data is delivered as **128x128** grey-scale pixel images, 
- the **100m** resolution data as **384x384** grey-scale pixel images. 
- The bit-depth of the images is **14**, but they are saved in a **16-bit .png**-format (which makes them look relatively dark if opened in typical image viewers).

Each image comes with a **quality map**, indicating which pixels in the image are concealed (i.e. clouds, cloud shadows, ice, water, missing, etc) and which should be considered clear. For an image to be included in the dataset, at least **75%** of its pixels have to be clear for 100m resolution images, and **60%** for 300m resolution images. Each data-point consists of exactly **one** 100m resolution image and **several** 300m resolution images from **the same scene**. In total, the dataset contains **1450** scenes, which are split into **1160** scenes for training and **290** scenes for testing. On average, each scene comes with **19** different low resolution images and always with at least **9**. We expect you to submit a **384x384** image for each of the **290** test-scenes, for which we will not provide a high resolution image.
