
https://github.com/BinaLab/FloodNet-Supervised_v1.0


# FloodNet Dataset

[](https://github.com/BinaLab/FloodNet-Supervised_v1.0#floodnet-dataset)

## Overview

[](https://github.com/BinaLab/FloodNet-Supervised_v1.0#overview)

Frequent, and increasingly severe, natural disasters threaten human health, infrastructure, and natural systems. The provision of accurate, timely, and understandable information has the potential to revolutionize disaster management. For quick response and recovery on a large scale, after a natural disaster such as a hurricane, access to aerial images is critically important for the response team. The emergence of small unmanned aerial systems (UAS) along with inexpensive sensors presents the opportunity to collect thousands of images after each natural disaster with high flexibility and easy maneuverability for rapid response and recovery. Moreover, UAS can access hard-to-reach areas and perform data collection tasks that can be unsafe for humans if not impossible. Despite all these advancements and efforts to collect such large datasets, analyzing them and extracting meaningful information remains a significant challenge in scientific communities.


