
https://data.csiro.au/collection/csiro:56372?_st=browse&_str=31&_si=14&browseType=kw&browseValue=Machine%20learning


Collection description

Wild-Places is a large-scale LiDAR dataset for inter and intra-run place recognition in unstructured natural environments. The data was collected in two environments at the Karawatha and Venman walking trails in Brisbane, Australia over fourteen months, allowing for research into both long and short-term revisits. We release sub-maps produced from a global point cloud for four sequences in both environments for a total of ~67K submaps, with accurate 6DoF poses and timestamps for each submap.

