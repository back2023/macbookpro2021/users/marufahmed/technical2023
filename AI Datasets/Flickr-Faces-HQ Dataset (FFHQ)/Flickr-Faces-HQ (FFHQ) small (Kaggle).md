
https://www.kaggle.com/datasets/tommykamaz/faces-dataset-small/code

This is a small version of FFHQ with 3143 photos.  
Flickr-Faces-HQ (FFHQ) is an image dataset containing high-quality images of human faces. It is provided by NVIDIA under the Creative Commons BY-NC-SA 4.0 license. It offers 70,000 PNG images at 1024×1024 resolution that display diverse ages, ethnicities, image backgrounds, and accessories like hats and eyeglasses.

