
https://cds.climate.copernicus.eu/cdsapp#!/dataset/derived-utci-historical?tab=overview

This dataset provides a complete historical reconstruction for a set of indices representing human thermal stress and discomfort in outdoor conditions. This dataset, also known as ERA5-HEAT (Human thErmAl comforT) represents the current state-of-the-art for bioclimatology data record production.

The dataset is organised around two main variables:

- the mean radiant temperature (MRT)
- the universal thermal climate index (UTCI)

These variables describe how the human body experiences atmospheric conditions, specifically air temperature, humidity, ventilation and radiation.

The dataset is computed using the ERA5 reanalysis from the European Centre for Medium-Range Forecasts (ECMWF). ERA5 combines model data with observations from across the world to provide a globally complete and consistent description of the Earth’s climate and its evolution in recent decades. ERA5 is regarded as a good proxy for observed atmospheric conditions.

