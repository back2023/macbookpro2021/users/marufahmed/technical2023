
https://lmdb.readthedocs.io/en/release/#installation-unix

This is a universal Python binding for the [LMDB ‘Lightning’ Database](http://lmdb.tech/). Two variants are provided and automatically selected during install: a [CFFI](https://cffi.readthedocs.io/en/release-0.5/) variant that supports [PyPy](http://www.pypy.org/) and all versions of CPython >=2.7, and a C extension that supports CPython >=2.7 and >=3.4. Both variants provide the same interface.

LMDB is a tiny database with some excellent properties:

- Ordered map interface (keys are always lexicographically sorted).
- Reader/writer transactions: readers don’t block writers, writers don’t block readers. Each environment supports one concurrent write transaction.
- Read transactions are extremely cheap.
- Environments may be opened by multiple processes on the same host, making it ideal for working around Python’s [GIL](http://wiki.python.org/moin/GlobalInterpreterLock).
- Multiple named databases may be created with transactions covering all named databases.
- Memory mapped, allowing for zero copy lookup and iteration. This is optionally exposed to Python using the [`buffer()`](https://docs.python.org/2.7/library/functions.html#buffer "(in Python v2.7)") interface.
- Maintenance requires no external process or background threads.
- No application-level caching is required: LMDB fully exploits the operating system’s buffer cache.




