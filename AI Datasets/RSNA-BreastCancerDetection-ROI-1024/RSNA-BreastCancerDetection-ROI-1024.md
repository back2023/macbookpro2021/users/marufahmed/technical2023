
https://www.kaggle.com/datasets/aryamaanthakur/rsna-breast-cancer-detection-roi-1024/data


## About Dataset

This dataset contains the Region of Interest extracted from breast X-ray images ([https://www.kaggle.com/competitions/rsna-breast-cancer-detection/data](https://www.kaggle.com/competitions/rsna-breast-cancer-detection/data)). 1024 is the length of longest size of image.


