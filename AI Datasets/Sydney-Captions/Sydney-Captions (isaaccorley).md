
https://huggingface.co/datasets/isaaccorley/Sydney-Captions


### How to download the rows directly with the[API](https://huggingface.co/docs/datasets-server)

```bash
curl -X GET \
     "https://datasets-server.huggingface.co/rows?dataset=isaaccorley%2FSydney-Captions&config=default&split=train&offset=0&length=100"


```



### How to use from the

[Datasets](https://huggingface.co/docs/hub/datasets-usage)library
```python
from datasets import load_dataset

ds = load_dataset("isaaccorley/Sydney-Captions")
```