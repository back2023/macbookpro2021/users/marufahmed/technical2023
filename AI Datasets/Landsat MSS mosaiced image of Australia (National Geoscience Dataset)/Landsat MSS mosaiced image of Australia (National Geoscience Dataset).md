

https://ecat.ga.gov.au/geonetwork/srv/api/records/a05f7892-b08e-7506-e044-00144fdd4fa6


These datasets are a subset of the mosaic of Australian Landsat MSS images of Australia. ldsatmss.bil is a 500m pixel Lambert Conformal Conic projected dataset. ldsatmssdd.bil is a 0.0048 degree geographic projected dataset. NOTE The original image is located in the corporate storage system at /d/geo/store/data/image/landsat_mss/aus_mosaic. In this directory there is extensive documentation (OVERVIEW.TXT) which describes all contents.


