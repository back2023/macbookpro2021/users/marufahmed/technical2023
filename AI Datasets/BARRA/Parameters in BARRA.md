
http://www.bom.gov.au/research/projects/reanalysis/BARRA_Parameters_Master_List_July_2019.v1.pdf


The following parameters comprise the Bureau of Meteorology Atmospheric high-resolution Regional Reanalysis of Australia (BARRA) dataset.

For information on the model system, current data availability and access see the project webpage: http://www.bom.gov.au/research/projects/reanalysis/

BARRA parameters are mostly hourly frequency except for 'spec' stream parameters which are 10 minute frequency 

