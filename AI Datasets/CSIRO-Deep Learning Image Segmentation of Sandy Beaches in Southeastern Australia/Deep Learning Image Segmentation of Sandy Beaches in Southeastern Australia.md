
https://data.csiro.au/collection/csiro:62849


Collection description

The collection includes beach coastlines from Southeastern Australia, specifically Victoria and New South Wales, used to train an image segmentation model using the U-Net deep learning architecture for mapping sandy beaches. The dataset contains polygons that represent the outline or extent of the raster images and polygons drawn by citizen-scientists. Additionally, we provide the trained model itself, which can be utilized for further evaluation or refined through fine-tuning. The resulting predictions are also available in Shapefiles format, which can be loaded to NationalMap. This collection supplements the publication: Regional-Scale Image Segmentation of Sandy Beaches: Comparison of Training and Prediction Across Two Extensive Coastlines in Southeastern Australia (Yong et al.)

Access

The metadata and files (if any) are available to the public.

Lineage

The training dataset of citizen science-drawn beach outlines and polygons was sourced from OpenStreetMap (OSM) https://www.openstreetmap.org/). Tiled images along the coast were sourced from Microsoft Bing imagery to process new beach outlines, as it is also one of the main sources of imagery used for drawing features in OSM. Note, the original OSM data was licensed ODbL and should be considered when using the processed dataset, which required a Creative Commons Licence to be published in this portal. CC-BY was identified as the most suitable license in the portal to align with ODbL. The saved deep learning model was trained on the dataset using a U-Net architecture, which is used to generate the predicted maps.


