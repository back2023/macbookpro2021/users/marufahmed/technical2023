
https://data.csiro.au/collection/63019

Collection description

The code collection contains a Python package to train an image segmentation model using the U-Net deep learning architecture for mapping sandy beaches. This collection supplements the publication: Regional-Scale Image Segmentation of Sandy Beaches: Comparison of Training and Prediction Across Two Extensive Coastlines in Southeastern Australia (Yong et al.)


