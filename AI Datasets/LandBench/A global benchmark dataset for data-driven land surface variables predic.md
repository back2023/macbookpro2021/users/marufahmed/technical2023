
https://data.tpdc.ac.cn/en/data/fde9782e-a3a9-49bf-9a85-a892885954ad


- Temporal resolutionDaily
- Spatial resolution0.5° - 1°
- Sharing wayOpen access
- Size427.54 GB
- Data time range
    
    1979-01-01 — 2020-12-31
    
- Metadata update time2023-05-04


Datasets Summary

We provide a set of benchmark data LandBench1.0, used for data driven land surface variables prediction.The data set is based on ERA5 - land, ERA5, SoilGrid, the SMSC and MODIS data sets generated and provided 0.5, 1, 2, and 4 degrees resolution daily global data.Data preprocessing, convenient to use in a data-driven model.We also provide a benchmark data set kit, open access is available at https://github.com/2023ATAI/Landbench.This toolkit promoted the existing method to achieve, a new prediction model of development and utilization of the unified evaluation index, the kit also includes used in high resolution land surface variables predicted global address mapping technology.Benchmark data set for the purpose of computer and the earth system is to encourage cooperation between scientists, in order to develop effective land surface variables prediction based on machine learning data driven model.

