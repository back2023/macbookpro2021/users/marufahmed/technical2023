
https://github.com/csiro-robotics/WildScenes


## About

[IJRR2024 ACCEPTED] The official repository for the WildScenes: A Benchmark for 2D and 3D Semantic Segmentation in Natural Environments

[csiro-robotics.github.io/WildScenes/](https://csiro-robotics.github.io/WildScenes/ "https://csiro-robotics.github.io/WildScenes/")


