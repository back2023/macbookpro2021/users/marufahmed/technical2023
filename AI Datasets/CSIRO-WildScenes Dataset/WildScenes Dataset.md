
https://data.csiro.au/collection/csiro:61541?_st=browse&_str=2&_si=1&browseType=kw&browseValue=Semantic%20Segmentation

Collection description

WildScenes is a large-scale 2D and 3D semantic segmentation dataset containing both labelled images and lidar point clouds, in natural environments. The data was collected from two natural environments in Brisbane, Australia across multiple revisits. Our release includes 2D images, 2D annotated images, 3D point cloud submaps, 3D annotated point cloud submaps, alongside accurate 6-DoF poses.




