
https://www.tensorflow.org/datasets/catalog/div2k

DIV2K dataset: DIVerse 2K resolution high quality images as used for the challenges @ NTIRE (CVPR 2017 and CVPR 2018) and @ PIRM (ECCV 2018)

```python
FeaturesDict({    
	'hr': Image(shape=(None, None, 3), dtype=uint8),    
	'lr': Image(shape=(None, None, 3), dtype=uint8),
})
```

- **Feature documentation**:

|Feature|Class|Shape|Dtype|Description|
|---|---|---|---|---|
||FeaturesDict||||
|hr|Image|(None, None, 3)|uint8||
|lr|Image|(None, None, 3)|uint8||

