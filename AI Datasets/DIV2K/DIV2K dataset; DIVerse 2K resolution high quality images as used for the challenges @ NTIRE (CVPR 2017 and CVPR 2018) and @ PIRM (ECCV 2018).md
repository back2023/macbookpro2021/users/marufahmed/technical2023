
https://data.vision.ee.ethz.ch/cvl/DIV2K/

## Data overview

We are making available a large newly collected dataset -**DIV2K**- of RGB images with a large diversity of contents.

The DIV2K dataset is divided into:

- train data: starting from 800 high definition high resolution images we obtain corresponding low resolution images and provide both high and low resolution images for 2, 3, and 4 downscaling factors
- validation data: 100 high definition high resolution images are used for genereting low resolution corresponding images, the low res are provided from the beginning of the challenge and are meant for the participants to get online feedback from the validation server; the high resolution images will be released when the final phase of the challenge starts.
- test data: 100 diverse images are used to generate low resolution corresponding images; the participants will receive the low resolution images when the final evaluation phase starts and the results will be announced after the challenge is over and the winners are decided.

## Data structure

DIV2K dataset has the following structure:

1000 2K resolution images divided into: 800 images for training, 100 images for validation, 100 images for testing

For each challenge Track (with **1. bicubic** or **2. unknown** downgrading operators) we have:

- the high resolution images: `0001.png, 0002.png, ..., 1000.png`
- the downscaled images:
    - `YYYYx2.png` for downscaling factor x2; where YYYY is the image ID
    - `YYYYx3.png` for downscaling factor x3; where YYYY is the image ID
    - `YYYYx4.png` for downscaling factor x4; where YYYY is the image ID

