
https://mmlab.ie.cuhk.edu.hk/projects/CelebA.html


## News

**2021-09-10** Another related dataset, [CelebA-Dialog](https://github.com/yumingj/Talk-to-Edit) has been released.  

**2020-07-10** Two related datasets, [CelebAMask-HQ](https://github.com/switchablenorms/CelebAMask-HQ) and [CelebA-Spoof](https://github.com/Davidzhangyuanhan/CelebA-Spoof), have been released. 

**2016-07-29** If Dropbox is not accessible, please download the dataset using [Google Drive](https://drive.google.com/drive/folders/0B7EVK8r0v71pWEZsZE9oNnFzTm8?resourcekey=0-5BR16BdXnb8hVj6CNHKzLg&usp=sharing) or [Baidu Drive](https://pan.baidu.com/s/1CRxxhoQ97A5qbsKO7iaAJg) (password: rp0s).

## Details

  

**CelebFaces Attributes Dataset (CelebA)** is a large-scale face attributes dataset with more than **200K** celebrity images, each with **40** attribute annotations. The images in this dataset cover large pose variations and background clutter. CelebA has large diversities, large quantities, and rich annotations, including

- **10,177** number of **identities**,
    
- **202,599** number of **face images**, and
    
- **5 landmark locations**, **40 binary attributes** annotations per image.
    

The dataset can be employed as the training and test sets for the following computer vision tasks: face attribute recognition, face recognition, face detection, landmark (or facial part) localization, and face editing & synthesis.


