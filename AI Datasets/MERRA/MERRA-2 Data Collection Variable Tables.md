

https://disc.gsfc.nasa.gov/information/documents?title=MERRA-2%20Data%20Collection%20Variable%20Tables


This document consists of tables of the variable list for MERRA-2 data collections.  Please read more information regarding the algorithm in the [MERRA-2 File Specification](https://gmao.gsfc.nasa.gov/pubs/docs/Bosilovich785.pdf),   [FAQs and publications on the MERRA-2 project page](https://gmao.gsfc.nasa.gov/reanalysis/MERRA-2/docs/).

