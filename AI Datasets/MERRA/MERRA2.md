
http://climate-cms.wikis.unsw.edu.au/MERRA2#MERRA2_at_NCI

The Modern Era Retrospective-Analysis for Research and Applications (MERRA) is a NASA reanalysis integrating satellite observations using a major new version of the Goddard Earth Observing System Data Assimilation System Version 5 (GEOS-5).

MERRA focuses on historical analyses of the hydrological cycle on a broad range of weather and climate time scales covering the modern satellite era from 1979 to present. The state-of-the-art GEOS-5 data assimilation system includes many modern observing systems (such as EOS) in a climate framework Specifically, the GEOS-DAS Version 5 implements Incremental Analysis Updates (IAU) to slowly adjust the model states toward the observed state.

MERRA-2 is produced with the GMAO/GEOS-5 Data Assimilation System Version 5.12.4 and its native resolution is 0.5° x 0.625° x 72 hybrid sigma/pressure levels. Hourly data intervals are used for two-dimensional products, while 3-hourly intervals are used for three-dimensional products. These may be on the model’s native 72-layer vertical grid or at 42 pressure surfaces extending to 0.1 hPa. MERRA-2 spans from 1979 to present (with about 5 months lag).

The 42 pressure levels are: 1000, 975, 950, 925, 900, 875, 850, 825, 800, 775, 750, 725, 700, 650, 600, 550, 500, 450, 400, 350, 300, 250, 200, 150,100, 70, 50, 40, 30, 20,10, 7, 5, 4, 3, 2, 1,0.7, 0.5, 0.4, 0.3, 0.1 hPa The 72 native grid levels are:

Information retrieved from [NCAR climate re-analysis guide](https://climatedataguide.ucar.edu/climate-data/nasa-merra) and [GEOS-CHEM MERRA-2 wiki](http://wiki.seas.harvard.edu/geos-chem/index.php/MERRA-2). The data was downloaded from the [GES DISC data store](https://disc.sci.gsfc.nasa.gov/information/glossary/581b59c65fa71421a68e64fa/merra-2?tags=mission) where information on all the MERRA-2 products is available.