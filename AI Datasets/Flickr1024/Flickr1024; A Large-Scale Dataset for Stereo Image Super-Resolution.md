

https://yingqianwang.github.io/Flickr1024/


**_Flickr1024 is a large-scale stereo image dataset which consists of 1024 high-quality image pairs and covers diverse senarios. Details of this dataset can be found in our [published paper](http://openaccess.thecvf.com/content_ICCVW_2019/papers/LCI/Wang_Flickr1024_A_Large-Scale_Dataset_for_Stereo_Image_Super-Resolution_ICCVW_2019_paper.pdf). Although the Flickr1024 dataset was originally developed for stereo image SR (click [here](https://github.com/YingqianWang/Stereo-Image-SR) for an overview), it was also used for many other tasks such as reference-based SR, stereo matching, and stereo image denoising._**

