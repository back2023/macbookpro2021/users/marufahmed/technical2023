
https://data.csiro.au/collection/csiro:55630


The LIB-HSI dataset contains hyperspectral reflectance images and their corresponding RGB images of building façades in a light industrial environment. The dataset also contains pixel-level annotated images for each hyperspectral/RGB image. The LIB-HSI dataset was created to develop deep learning methods for segmenting building facade materials. 

The images were captured using a Specim IQ hyperspectral camera. The hyperspectral images are in the ENVI format. There are 393 training images, 45 validation images and 75 test images.