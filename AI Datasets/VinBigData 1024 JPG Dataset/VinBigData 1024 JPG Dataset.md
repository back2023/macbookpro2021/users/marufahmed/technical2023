
https://www.kaggle.com/datasets/sunghyunjun/vinbigdata-1024-jpg-dataset/data

1024px JPG X-ray images dataset converted from original dataset of the competition [VinBigData Chest X-ray Abnormalities Detection](https://www.kaggle.com/c/vinbigdata-chest-xray-abnormalities-detection).

The code for reading X-ray images is from:  
[https://www.kaggle.com/raddar/convert-dicom-to-np-array-the-correct-way](https://www.kaggle.com/raddar/convert-dicom-to-np-array-the-correct-way)  
[https://www.kaggle.com/raddar/vinbigdata-competition-jpg-data-3x-downsampled](https://www.kaggle.com/raddar/vinbigdata-competition-jpg-data-3x-downsampled)

Following code was used:


