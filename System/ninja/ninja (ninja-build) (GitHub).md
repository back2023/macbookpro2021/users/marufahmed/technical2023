
https://github.com/ninja-build/ninja


# Ninja

Ninja is a small build system with a focus on speed. [https://ninja-build.org/](https://ninja-build.org/)

See [the manual](https://ninja-build.org/manual.html) or `doc/manual.asciidoc` included in the distribution for background and more details.

Binaries for Linux, Mac and Windows are available on [GitHub](https://github.com/ninja-build/ninja/releases). Run `./ninja -h` for Ninja help.

Installation is not necessary because the only required file is the resulting ninja binary. However, to enable features like Bash completion and Emacs and Vim editing modes, some files in misc/ must be copied to appropriate locations.

If you're interested in making changes to Ninja, read [CONTRIBUTING.md](https://github.com/ninja-build/ninja/blob/master/CONTRIBUTING.md) first.

