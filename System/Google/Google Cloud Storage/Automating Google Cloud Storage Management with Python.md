https://medium.com/google-cloud/automating-google-cloud-storage-management-with-python-92ba64ec8ea8

* Firstly, we need to **create a Service Account (SA)** which will be used to interact with Cloud Storage via the API.
	* Login to your [Google Cloud Management Console](https://console.cloud.google.com/), go to IAM & Admin and choose the [_Service Accounts_](https://console.cloud.google.com/iam-admin/serviceaccounts) tab. From there, click on _Create Service Account_.
	* Choose a service account name. Finally, we need to give access rights to our SA
* Now that we’ve created the SA, we need to generate its respective key.
	* Then go to the _Keys_ tab, click on _Add Key > Create New Key_ and select _JSON_ as key type.
	* This will download a JSON file to your PC
	* Now that we have gathered the API key of our Cloud Storage SA, we can start making API calls to Cloud Storage.
* Before we can make any API calls, we need to create an environment variable
```python
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = r"/path/to/credentials/project-name-123456.json"
```