https://stackoverflow.com/questions/42555142/downloading-a-file-from-google-cloud-storage-inside-a-folder

```python
from google.cloud import storage

# Initialise a client
storage_client = storage.Client("[Your project name here]")
# Create a bucket object for our bucket
bucket = storage_client.get_bucket(bucket_name)
# Create a blob object from the filepath
blob = bucket.blob("folder_one/foldertwo/filename.extension")
# Download the file to a destination
blob.download_to_filename(destination_file_name)
```

You can also use `.download_as_string()` but as you're writing it to a file anyway downloading straight to the file may be easier.