https://serverfault.com/questions/1001184/how-to-get-a-download-url-for-files-in-google-cloud-storage

You can get the downloadable link replacing the values of the following string:
```bash
https://storage.googleapis.com/{bucket.name}/{blob.name}
```
 