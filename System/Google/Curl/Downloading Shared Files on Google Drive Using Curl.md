
https://gist.github.com/tanaikech/f0f2d122e05bf5f971611258c22c110f


```bash
#!/bin/bash
fileid="### file id ###"
filename="MyFile.csv"
html=`curl -c ./cookie -s -L "https://drive.google.com/uc?export=download&id=${fileid}"`
curl -Lb ./cookie "https://drive.google.com/uc?export=download&`echo ${html}|grep -Po '(confirm=[a-zA-Z0-9\-_]+)'`&id=${fileid}" -o ${filename}
```
Source: https://gist.github.com/tanaikech/f0f2d122e05bf5f971611258c22c110f#updated-at-february-17-2022