

### _How to use [Singularity](https://sylabs.io/guides/latest/user-guide/)!_

This is an introductory workshop on Singularity. It was originally taught by [David Godlove](https://github.com/GodloveD) at the [NIH HPC](https://hpc.nih.gov/), but the content has since been adapted to a general audience. For more information about the topics covered here, see the following:

- [Singularity Home](https://sylabs.io/singularity/)
- [Singularity on GitHub](https://github.com/singularityware/singularity)
- [Singularity on Google Groups](https://groups.google.com/a/lbl.gov/forum/#!forum/singularity)
- [Singularity at the NIH HPC](https://hpc.nih.gov/apps/singularity.html)
- [Docker documentation](https://docs.docker.com/)
- [Singularity Container Services](https://cloud.sylabs.io/home)
- [Docker Hub](https://hub.docker.com/)


https://github.com/Singularity-tutorial/Singularity-tutorial.github.io



