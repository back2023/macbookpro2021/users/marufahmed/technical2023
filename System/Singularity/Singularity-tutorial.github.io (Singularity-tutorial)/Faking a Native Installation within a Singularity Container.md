


How would you like to install an app with one more more commands inside of a Singularity container and then just forget it's in a container and use it like any other app?

In the last example, we took a step in that direction by creating a `runscript` to accept input and output, but we still had to set an environment variable and our container was not actually using the cowsay program as written. Not to mention, it gave us zero access to the `fortune` and `lolcat` programs. In other words, it could only do what our `runscript` told it to do.

In this section, we are going to look at a simple, flexible method for creating containerized app installations that you can "set and forget". This is the same method that Biowulf staff members use to install containerized applications on the NIH HPC systems. As of today, around 100 applications are installed like this, and most of them are used all the time by scientists who may never know or care that the app they depend on is actually running inside of a container!


https://github.com/Singularity-tutorial/Singularity-tutorial.github.io/tree/master/07-fake-installation


