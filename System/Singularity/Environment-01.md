in the `%post` section of your definition file.

```bash
Bootstrap: library
From: default/alpine

%post
    echo 'export VARIABLE_NAME=variable_value' >>$SINGULARITY_ENVIRONMENT
```
https://docs.sylabs.io/guides/latest/user-guide/environment_and_metadata.html

Variables set in the `%post` section through `$SINGULARITY_ENVIRONMENT` take precedence over those added via `%environment`.
https://docs.sylabs.io/guides/latest/user-guide/definition_files.html

