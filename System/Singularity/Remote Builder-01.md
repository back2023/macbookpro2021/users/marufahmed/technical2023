## Cloud Library
https://docs.sylabs.io/guides/latest/user-guide/cloud_library.html#remote-builder
1. The Sylabs Cloud also provides a [Remote Builder](https://docs.sylabs.io/guides/latest/user-guide/cloud_library.html#remote-builder), allowing you to build containers on a secure remote service.
    This is convenient so that you can build containers on systems where you do not have root privileges.

#### 1. Make an Account
1. Goto  [https://cloud.sylabs.io/library](https://cloud.sylabs.io/library)
2. Used: patreontest2019@gmail
#### 2. Creating a Access token
1.  Go to: [https://cloud.sylabs.io/](https://cloud.sylabs.io/)
2. Token created: 2023Feb
```bash   eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGguc3lsYWJzLmlvL3Rva2VuIiwic3ViIjoiNjNmOWNiYzM0MzBjYTYxYTFhMjcxY2UxIiwiZXhwIjoxNjc5OTA3MzI4LCJpYXQiOjE2NzczMTUzMjgsImp0aSI6IjYzZjljZDAwNDJlMmNhYWJkMTA2MTQyNiJ9.C6lb5nWCfIRG26mbpgBAIQawKrIKnIeFlMqY9D1aU_dxcmCeqylHY4YgaSPiO4gbQE66O9Zbz8X5XkhvcRGWNyJtaijy_FobwwA_dTj-pQlpHHQJ9EDI7o7klXO37gitYGw_GhS--wavs99G1Ss_SGCI9OEMO4twLYOkyx706dADtos-HkUCiAx3GIACLCHTshUonxuUQ52GhkHpQGVQZ96rK9UPyPAQO7NVhYW2RR6aMQvD-4WJ4DCmUgVtywvcFW0VXBCD_UuyZrgUSqcdIP8mSAO-6Z3ISMDQFeoPJjx07_8BzkZmvLzpKQ1gkKa9Y7CcV3py07awxcGfgbYwZA
 ```
3. 
```bash
singularity remote login
An access token is already set for this remote. Replace it? [N/y]y
Generate an access token at https://cloud.sylabs.io/auth/tokens, and paste it here.
Token entered will be hidden for security.
Access Token: 
INFO:    Access Token Verified!
INFO:    Token stored in /home/900/mah900/.singularity/remote.yaml
```

## Remote Endpoints
https://docs.sylabs.io/guides/latest/user-guide/endpoint.html
```
singularity remote status

singularity remote list
```


## Build Environment
https://docs.sylabs.io/guides/latest/user-guide/build_env.html

1. The cache is created at `$HOME/.singularity/cache` by default. The location of the cache can be changed by setting the `SINGULARITY_CACHEDIR` environment variable.
    ```
    export SINGULARITY_CACHEDIR=/tmp/user/temporary-cache
	```
2. You can safely delete these directories, or content within them. SingularityCE will re-create any directories and data that are needed in future runs.
3. `singularity cache clean` to reset the cache to a clean, empty state.
4. `singularity cache list`

## Build a Container
https://docs.sylabs.io/guides/latest/user-guide/build_a_container.html

`build` can produce containers in two different formats that can be specified as follows.
1.   compressed read-only **Singularity Image File (SIF)** format suitable for production (default)
2.   writable **(ch)root directory** called a sandbox for interactive development ( `--sandbox` option)

##### `sudo`
In this case we’re running the `singularity build` with `sudo` because installing software with `apt-get` requires the privileges of the `root` user. By default, when you run SingularityCE you are the same user inside the container as outside on the host, so becoming `root` on the host with `sudo` ensures we can `apt-get` as `root` in the container build.

If you aren’t able, or don’t wish to use `sudo` when building a container, SingularityCE offers `--remote` builds, 
a `--fakeroot` mode, and 
limited unprivileged builds with `proot`.

##### `--remote` builds
[`Singularity Container Services <https://cloud.sylabs.io/`__](https://docs.sylabs.io/guides/latest/user-guide/build_a_container.html#id2) and [Singularity Enterprise](https://sylabs.io/singularity-enterprise/) provide a ‘Remote Build Service’. This service can perform a container build, as the root user, inside a secure single-use virtual machine.

Remote builds do not have the system requirements of `--fakeroot` builds, or the limitations of unprivileged `proot` builds. They are a convenient way to build SingularityCE containers on systems where `sudo` rights are not available.

### Singularity and Docker
1. You cannot build a singularity container _directly_ from a Dockerfile
   https://stackoverflow.com/a/60316979
2. You can transform a Dockerfile into a singularity recipe or vise-versa using Singularity Python.
   https://stackoverflow.com/a/66499169

### Singularity Python
https://github.com/singularityhub/singularity-cli
1. Install
   https://singularityhub.github.io/singularity-cli/install
```bash 
pip3 install spython
```
2. Singularity Python Recipes
```bash
# Write to a file
spython recipe Dockerfile Singularity.snowflake
# Debug
spython --debug recipe Dockerfile

```
3. 





