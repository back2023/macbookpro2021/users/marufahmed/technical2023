https://hpc.nih.gov/apps/singularity.html


_Extreme Mobility of Compute_

Singularity containers let users run applications in a Linux environment of their choosing.

Possible uses for Singularity on Biowulf:

- Run an application that was built for a different distribution of Linux than the host OS.
- Reproduce an environment to run a workflow created by someone else.
- Run a series of applications (a 'pipeline') that includes applications built on different platforms.
- Run an application from [Docker Hub](https://hub.docker.com/) on Biowulf without actually installing anything.

**Web sites**

- [Singularity home](https://www.sylabs.io/singularity/)
- [Singularity Documentation](https://sylabs.io/guides/latest/user-guide/)
- [Singularity on GitHub](https://github.com/sylabs/singularity)
- [Singularity on Google groups](https://groups.google.com/a/lbl.gov/forum/#!forum/singularity)
- [Docker Hub](https://hub.docker.com/)
- [Singularity Container Services](https://cloud.sylabs.io/home)

**Additional Learning Resources**

- Class taught by NIH HPC staff:
    - [Materials](https://github.com/NIH-HPC/Singularity-Tutorial/tree/2020-03-10)
    - [Day 1 recording](https://youtu.be/3Yg7XI39H4U)
    - [Day 2 recording](https://youtu.be/yi82PC--F2U)
- [Singularity Basics videos by Sylabs](https://www.youtube.com/playlist?list=PL052H4iYGzysewYEelldGPOgKRJkxd5zp)
- [NIH HPC Singularity Tutorial](https://singularity-tutorial.github.io/)




