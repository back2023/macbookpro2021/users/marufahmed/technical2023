
Inside Dockerfile add these 2 lines to disable both pip version check and cache.

```python
FROM python:3.6.10

ARG PIP_DISABLE_PIP_VERSION_CHECK=1
ARG PIP_NO_CACHE_DIR=1

RUN pip3 install -r requirements.txt
```
https://stackoverflow.com/questions/46288847/how-to-suppress-pip-upgrade-warning/60270281#60270281

```bash
PIP_NO_CACHE_DIR=off
```
https://github.com/pypa/pip/issues/2897