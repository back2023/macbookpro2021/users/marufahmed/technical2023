
### **Overview** 

This long awaited feature allows copying local files into remote builds via the _%files_ section of the Singularity def file the same way as you would if you were building locally with singularity build.

No special command-line arguments or other user actions are required: simply add a _%files_ section to your DEF file to be used with remote build and any referenced files will be automatically copied to the image.

Remote build files support is included in SingularityCE v3.10 (and later), `scs-build` 0.7.4 (and later), and will be back-ported to SingularityPRO v3.9 for an upcoming release.

### **scs-build (Singularity Container Services)**

`scs-build` is the go-to utility for integrating Singularity builds into a CI/CD pipeline, such as GitHub Actions, GitLab, Jenkins, CircleCI, and others. It is the same code used in SingularityCE and SingularityPRO to call out to a remote build service. There are currently two means to support the remote building of a SIF image, either in Sylabs Singularity Enterprise or in Sylabs Singularity Container Services ([cloud.sylabs.io](https://sylabs.io/2022/06/supporting-local-files-in-a-singularity-remote-build/cloud.sylabs.io)).

It is imperative that the Docker command-line arguments are properly formed to mimic the Singularity build from being invoked on the local system. This ensures the current local directory is mirrored in the `scs-build` container environment.


https://sylabs.io/2022/06/supporting-local-files-in-a-singularity-remote-build/