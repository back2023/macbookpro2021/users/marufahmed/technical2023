

There are [different ways](https://docs.sylabs.io/guides/3.0/user-guide/quick_start.html#runcontainer) in which you can run Singularity containers. If you use commands like `run`, `exec` and `shell` to interact with processes in the container, you are running Singularity containers in the foreground. Singularity, also lets you run containers in a “detached” or “daemon” mode which can run different services in the background. A “service” is essentially a process running in the background that multiple different clients can use. For example, a web server or a database. To run services in a Singularity container one should use _instances_. A container instance is a persistent and isolated version of the container image that runs in the background.


https://docs.sylabs.io/guides/3.0/user-guide/running_services.html