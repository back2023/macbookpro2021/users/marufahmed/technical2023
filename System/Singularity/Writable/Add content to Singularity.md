```
By default Singularity containers mounted as read-only volumes, which means you won't be able to add content or install software _(even as a privileged user)_, save for default or system-mounted paths. In order to add content you must run your Singularity command with the `--writable` flag.
```

https://notebook.community/zebulasampedro/singularity-jupyter-demo/managing-custom-jupyter-envs-with-singularity