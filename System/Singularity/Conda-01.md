Miniconda install link:  
https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

Install conda in singularity
https://stackoverflow.com/a/61232125/18844497
```
cd /scratch/fp0/mah900/conda_sin
nano conda_sing.def

Bootstrap: library
From: ubuntu:18.04
Stage: build

%post

apt-get update && apt-get -y upgrade
apt-get -y install \
build-essential \
wget \
bzip2 \
ca-certificates \
libglib2.0-0 \
libxext6 \
libsm6 \
libxrender1 \
git
rm -rf /var/lib/apt/lists/*
apt-get clean
#Installing Anaconda 3 
#wget -c https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh
#/bin/bash Anaconda3-2020.02-Linux-x86_64.sh -bfp /usr/local
wget -c https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
/bin/bash Miniconda3-latest-Linux-x86_64.sh -bfp /usr/local
#Conda configuration of channels from .condarc file
#conda config --file /.condarc --add channels defaults
#conda config --file /.condarc --add channels conda-forge
conda update conda
#List installed environments
conda list
```
Remote build
```
singularity -vv build --remote conda_sing.sif conda_sing.def
```
(Working)

### Ubuntu 20.04 -conda-def
```
Bootstrap: library
From: ubuntu:20.04
Stage: build
 
%post

apt-get update && apt-get -y upgrade
DEBIAN_FRONTEND=noninteractive TZ=Australia/ACT \
apt-get -y install \
build-essential wget bzip2 ca-certificates \
libglib2.0-0 libxext6 libsm6  libxrender1 \
git
rm -rf /var/lib/apt/lists/*
apt-get clean
wget -c https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
/bin/bash Miniconda3-latest-Linux-x86_64.sh -bfp /usr/local
conda update conda
conda info
conda list
```
Error-1
```bash
E: Unable to locate package libxrender2
```
Error-2
```bash
debconf: falling back to frontend: Readline
Configuring tzdata
------------------

Please select the geographic area in which you live. Subsequent configuration
```
Try-1 (Not worked)
https://serverfault.com/questions/949991/how-to-install-tzdata-on-a-ubuntu-docker-image
```ymal
%environment
	export DEBIAN_FRONTEND=noninteractive
	export TZ=Australia/ACT
```
Try-2
```bash
DEBIAN_FRONTEND=noninteractive TZ=Australia/ACT \
```

Error-3
```bash
2023-02-26 22:38:16 (146 MB/s) - 'Miniconda3-latest-Linux-x86_64.sh' saved [74403966/74403966]

--2023-02-26 22:38:16--  http://miniconda.sh/
Resolving miniconda.sh (miniconda.sh)... failed: Name or service not known.
wget: unable to resolve host address 'miniconda.sh'
```
Solution: wget save name option error. 

Error-4
```bash
+ Miniconda3-latest-Linux-x86_64.sh -bfp /usr/local
/.post.script: 11: Miniconda3-latest-Linux-x86_64.sh: not found
```
Solution: add `/bin/bash`
```bash
/bin/bash Miniconda3-latest-Linux-x86_64.sh -bfp /usr/local
```


### Azure Conda Recipe
```bash
Bootstrap: library
#From: mcr.microsoft.com/azureml/openmpi4.1.0-cuda11.3-cudnn8-ubuntu20.04:latest
From: ubuntu:20.04
Stage: build

%post
apt-get update && apt-get -y upgrade
DEBIAN_FRONTEND=noninteractive TZ=Australia/ACT \
apt-get -y install \
build-essential \
wget \
bzip2 \
ca-certificates \
libglib2.0-0 \
libxext6 \
libsm6 \
libxrender1 \
git
rm -rf /var/lib/apt/lists/*
apt-get clean

wget -c https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
/bin/bash Miniconda3-latest-Linux-x86_64.sh -bfp /opt/conda/
. /opt/conda/etc/profile.d/conda.sh
echo ". /opt/conda/etc/profile.d/conda.sh" >> $SINGULARITY_ENVIRONMENT
conda update conda
#List installed environments
conda list

```



### Activate Conda inside

```bash
. /opt/conda/etc/profile.d/conda.sh
conda activate myenv
```
https://stackoverflow.com/questions/59877249/activating-a-conda-environment-in-a-singularity-recipe

```bash
# set to whatever your conda path is, I usually install to /opt
echo ". /opt/conda/etc/profile.d/conda.sh" >> $SINGULARITY_ENVIRONMENT
echo "conda activate pcgr" >> $SINGULARITY_ENVIRONMENT
```
https://stackoverflow.com/questions/56446195/activate-conda-environment-on-execution-of-singularity-container-in-nextflow

### Links 
Setting a default Conda environment
https://github.com/apptainer/singularity/issues/5075

