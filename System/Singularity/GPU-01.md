### GPU Support (NVIDIA CUDA & AMD ROCm)
https://docs.sylabs.io/guides/latest/user-guide/gpu.html

To use the `--nv` flag to run a CUDA application inside a container you must ensure that:
1. The host has a working installation of the NVIDIA GPU driver, and a matching version of the basic NVIDIA/CUDA libraries.
2. The NVIDIA libraries are in the system’s library search path.
3. The application inside your container was compiled for a CUDA version, and device capability level, that is supported by the host card and driver.

### Multiple GPUs
By default, SingularityCE makes all host devices available in the container.