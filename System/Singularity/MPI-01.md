### SingularityCE and MPI applications
https://docs.sylabs.io/guides/latest/user-guide/mpi.html

* The most popular way of executing MPI applications installed in a SingularityCE container is to rely on the MPI implementation available on the host. This is called the _Host MPI_ or the _Hybrid_ model since both the MPI implementations provided by system administrators (on the host) and in the containers will be used.
* Another approach is to only use the MPI implementation available on the host and not include any MPI in the container. This is called the _Bind_ model since it requires to bind/mount the MPI version available on the host into the container.

### Hybrid model
-  you will call `mpiexec` or a similar launcher on the `singularity` command itself. The MPI process outside of the container will then work in tandem with MPI inside the container
- The MPI in the container must be compatible with the version of MPI available on the host.

```bash
 mpirun -n <NUMBER_OF_RANKS> singularity exec <PATH/TO/MY/IMAGE> </PATH/TO/BINARY/WITHIN/CONTAINER>
```

### Troubleshooting
1. If your containers run N rank 0 processes: it is likely that the MPI stack used to launch the containerized application is not compatible with, or cannot communicate with, the MPI stack in the container.
2. If your container starts processes of different ranks, but fails with communications errors there may also be a version incompatibility, or interconnect libraries may not be available or configured properly with the MPI stack in the container.