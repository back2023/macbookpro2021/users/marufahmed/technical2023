

https://www.resilio.com/blog/benefits-of-object-storage-vs-file-storage


## What is object storage?

Object storage is a method of data storage that emerged in the mid-1990s as researchers foresaw that existing storage methods would eventually start to show their limitations in certain scenarios. True to its name, object storage treats data as discrete units, or objects, that are accompanied by metadata and a universally unique identifier (UUID). This unstructured data resides in a flat (as opposed to tiered) address space called a storage pool. Object storage is also known for its compatibility with cloud computing, due to its unlimited scalability and faster data retrieval.

Today, as data comes to underpin everything we do, the adoption of object storage systems has increased. It’s common in data centers and popular cloud-based platforms, such as Google cloud storage or Amazon cloud storage, and has become the de facto standard in several enterprise use cases.


