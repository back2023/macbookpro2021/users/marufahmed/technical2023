

Publishing a package requires a flow from the author’s source code to an end user’s Python environment. The steps to achieve this are:
- Have a source tree containing the package. This is typically a checkout from a version control system (VCS).
- Prepare a configuration file describing the package metadata (name, version and so forth) and how to create the build artifacts. For most packages, this will be a `pyproject.toml` file, maintained manually in the source tree.
- Create build artifacts to be sent to the package distribution service (usually PyPI); these will normally be a [source distribution (“sdist”)](https://packaging.python.org/en/latest/glossary/#term-Source-Distribution-or-sdist) and one or more [built distributions (“wheels”)](https://packaging.python.org/en/latest/glossary/#term-Built-Distribution). These are made by a build tool using the configuration file from the previous step. Often there is just one generic wheel for a pure Python package.
- Upload the build artifacts to the package distribution service.

- At that point, the package is present on the package distribution service.


## The source tree

The source tree contains the package source code, usually a checkout from a VCS.

## The configuration file

- The configuration file depends on the tool used to create the build artifacts. The standard practice is to use a `pyproject.toml` file in the [TOML format](https://github.com/toml-lang/toml).
- At a minimum, the `pyproject.toml` file needs a `[build-system]` table specifying your build tool. There are many build tools available, including but not limited to [flit](https://packaging.python.org/en/latest/key_projects/#flit), [hatch](https://packaging.python.org/en/latest/key_projects/#hatch), [pdm](https://packaging.python.org/en/latest/key_projects/#pdm), [poetry](https://packaging.python.org/en/latest/key_projects/#poetry), [setuptools](https://packaging.python.org/en/latest/key_projects/#setuptools), [trampolim](https://pypi.org/project/trampolim/), and [whey](https://pypi.org/project/whey/).

- The particular build tool you choose dictates what additional information is required in the `pyproject.toml` file. For example, you might specify:
	- a `[project]` table containing project [Core Metadata](https://packaging.python.org/en/latest/specifications/core-metadata/) (name, version, author and so forth); see [Declaring project metadata](https://packaging.python.org/en/latest/specifications/declaring-project-metadata/) for more detail
	- a `[tool]` table containing tool-specific configuration options

## Build artifacts

### The source distribution (sdist)

A source distribution contains enough to install the package from source in an end user’s Python environment. As such, it needs the package source, and may also include tests and documentation. These are useful for end users wanting to develop your sources, and for end user systems where some local compilation step is required (such as a C extension).

The [build](https://packaging.python.org/en/latest/key_projects/#build) package knows how to invoke your build tool to create one of these:
```
python3 -m build --sdist source-tree-directory
```
Or, your build tool may provide its own interface for creating an sdist.


### The built distributions (wheels)

- A built distribution contains only the files needed for an end user’s Python environment. 
- No compilation steps are required during the install, and the wheel file can simply be unpacked into the `site-packages` directory.

- A pure Python package typically needs only one “generic” wheel. 
- A package with compiled binary extensions needs a wheel for each supported combination of Python interpreter, operating system, and CPU architecture that it supports. If a suitable wheel file is not available, tools like [pip](https://packaging.python.org/en/latest/key_projects/#pip) will fall back to installing the source distribution.

The [build](https://packaging.python.org/en/latest/key_projects/#build) package knows how to invoke your build tool to create one of these:
```bash
python3 -m build --wheel source-tree-directory
```


## Upload to the package distribution service[](https://packaging.python.org/en/latest/flow/#upload-to-the-package-distribution-service "Permalink to this headline")

The [twine](https://packaging.python.org/en/latest/key_projects/#twine) tool can upload build artifacts to PyPI for distribution, using a command like:

```bash
twine upload dist/package-name-version.tar.gz dist/package-name-version-py3-none-any.whl
```
Or, your build tool may provide its own interface for uploading.


## Download and install[](https://packaging.python.org/en/latest/flow/#download-and-install "Permalink to this headline")

Now that the package is published, end users can download and install the package into their Python environment. Typically this is done with [pip](https://packaging.python.org/en/latest/key_projects/#pip), using a command like:

```bash
python3 -m pip install package-name
```