
https://choosealicense.com/

An open source license protects contributors and users. Businesses and savvy developers won’t touch a project without this protection.


https://choosealicense.com/licenses/apache-2.0/
# Apache License 2.0

A permissive license whose main conditions require preservation of copyright and license notices. Contributors provide an express grant of patent rights. Licensed works, modifications, and larger works may be distributed under different terms and without source code.

|Permissions|Conditions|Limitations|
|---|---|---|
|-  Commercial use<br>-  Distribution<br>-  Modification<br>-  Patent use<br>-  Private use|-  License and copyright notice<br>-  State changes|-  Liability<br>-  Trademark use<br>-  Warranty|

