
https://packaging-guide.openastronomy.org/en/latest/minimal.html
©2019, OpenAstronomy Developers.

- We will take a look at the minimal set of files you will need to create an installable Python package. 
- Once you have set these up, your package directory should look like:
```
├── LICENCE
├── my_package
│   └── __init__.py
├── pyproject.toml
├── setup.py
├── MANIFEST.in
└── README.rst
```
- where `my_package` is the name of your package. 

## `LICENSE`
- Python ecosystem use the [3-clause BSD license](https://opensource.org/licenses/BSD-3-Clause) and we recommend following this or using the [MIT license](https://opensource.org/licenses/MIT) unless you have a good reason not to.
  
- To include the license in your package, 
	- create a file called LICENSE and paste the license text into it, 
	- making sure that you update the copyright year, authors, and any other required fields

## `README.rst`

- We recommend using the [reStructuredText (rst)](http://docutils.sourceforge.net/rst.html) format for your README as this will ensure that the README gets rendered well online, e.g. on [GitHub](https://github.com/) or [GitLab](https://gitlab.com/) and on [PyPI](https://pypi.org/).

## `my_package/__init__.py`

- Python code for your package should live in a sub-directory that has the name of the Python module you want your users to import. 
- This module name should be a valid Python variable name, so cannot start with numbers and cannot include hyphens.

- Once you have created this directory, the first file to create in it should be a file called `__init__.py` 
	- which will be the first code to be run when a user imports your package.


## `pyproject.toml`

- At a minimum, this file should contain 
	- the `[project]` table (defined by [PEP621](https://peps.python.org/pep-0621/)) and 
	- the `[build-system]` table (defined by [PEP518](https://peps.python.org/pep-0518/)).

### `[project]`

```bash
[project]
name = "my-package"
description = "My package description"
readme = "README.rst"
authors = [
    { name = "Your Name", email = "your@email.com" }
]
license = { text = "BSD 3-Clause License" }
dependencies = [
    "numpy",
    "astropy>=3.2",
]
dynamic = ["version"]

[project.urls]
homepage = "https://link-to-your-project"
```

- The `name` field is the name your package will have on PyPI. 
	- It is not necessarily the same as the module name, 
	- so in this case we’ve set the package name to `my-package` even though the module name is `my_package`.
	- However, aside from the case where the package name has a hyphen and the module name has an underscore, we strongly recommend making the package and the module name the same to avoid confusion.

- Note that the version of the package is **not** explicitly defined in the file above, (rather, defined as `dynamic`), 
	- because we are using the [setuptools_scm](https://pypi.org/project/setuptools-scm/) package to automatically retrieve the latest version from Git tags. 
	- However, if you choose to not use that package, you can explicitly set the version in the `[project]` section (and remove it from the `dynamic` list):
```bash
[project]
version = "0.12"
```
	  
- Finally, the `dependencies` section is important since it is where you will be declaring the dependencies for your package. 
	- The cleanest way to do this is to specify one package per line, as shown above.	  
	  
- A complete list of keywords in `[project]` can be found in the [Python packaging documentation](https://packaging.python.org/en/latest/specifications/declaring-project-metadata/#declaring-project-metadata).


### `[build-system]`




## `MANIFEST.in`




