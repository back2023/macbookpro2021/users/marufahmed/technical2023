
https://github.com/google-deepmind/tree

## About

tree is a library for working with nested data structures

# Tree

`tree` is a library for working with nested data structures. In a way, `tree` generalizes the builtin `map` function which only supports flat sequences, and allows to apply a function to each "leaf" preserving the overall structure.








