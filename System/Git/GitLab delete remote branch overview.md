
https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-to-perform-a-GitLab-delete-remote-branch-operation-on-a-repository-successfully


## GitLab delete remote branch overview

A developer can follow these steps to have GitLab delete a remote branch of a feature branch on the client side:
1. Open a Terminal window in the gitlab-made-easy repository on the client machine;
2. Switch to the master branch via the ‘git checkout’ command;
3. Delete the branch locally;
4. [Push to origin](https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-to-git-push-GitLab-commits-to-origin) with the –delete flag; and
5. Verify local and remote tracking branches are deleted with a ‘branch listing’ command.


##   Remote GitLab branch removal commands

```
git checkout master
git branch -d fun_feature
git push origin --delete fun_feature
git branch --all
```

