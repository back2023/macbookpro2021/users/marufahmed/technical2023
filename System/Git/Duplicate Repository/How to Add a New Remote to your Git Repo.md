

https://articles.assembla.com/en/articles/1136998-how-to-add-a-new-remote-to-your-git-repo

To add a new remote, use the **git remote add** command on the terminal, in the directory your repository is stored at.

The git remote add command takes two arguments:
- A unique remote name, for example, “my_awesome_new_remote_repo”
- A remote URL, which you can find on the _Source_ sub-tab of your Git repo
```
#set a new remote  
  
git remote add my_awesome_new_remote_repo git@git.assembla.com:portfolio/space.space_name.git  
  
  
#Verify new remote  
  
git remote -v  
  
> my_awesome_new_remote_repo  git@git.assembla.com:portfolio/space.space_name.git (fetch)  
> my_awesome_new_remote_repo  git@git.assembla.com:portfolio/space.space_name.git (push)

```

To push your changes into your remote repo execute the git push `<remote> <branch>` command:
```
git push <your_remote_name>  
  
#git push my_awesome_new_remote_repo
```
 