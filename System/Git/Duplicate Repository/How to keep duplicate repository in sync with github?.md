

https://serverfault.com/questions/1095806/how-to-keep-duplicate-repository-in-sync-with-github


One option is clone as bare repository and fetch the changes from original repo whenever there are changes.

```
git clone --mirror https://primary_repo_url/primary_repo.git
git fetch origin
```

Then, add the other new repo as another remote and push it to that repo whenever there is changes to sync.

```
cd primary_repo.git
git remote add --mirror=fetch secondary https://secondary_repo_url/secondary_repo.git

git push secondary --all
```

For more details and other options, I followed this [link](https://www.opentechguides.com/how-to/article/git/177/git-sync-repos.html#:%7E:text=The%20secondary%20repo%20will%20now%20have%20all%20the,origin%20%23%20git%20push%20secondary%20--all%20Reverse%20Sync)

