

https://stackoverflow.com/questions/849308/how-can-i-pull-push-from-multiple-remote-locations


**Doing this manually is no longer necessary**, with modern versions of `git`! See [**Malvineous**](https://stackoverflow.com/users/308237/malvineous)'s solution, [below](https://stackoverflow.com/a/12795747/6908282).

Reproduced here:

```
git remote set-url origin --push --add <a remote>
git remote set-url origin --push --add <another remote>
```

---


Since git 1.8 (October 2012) you are able to do this from the command line:

```
git remote set-url origin --push --add user1@repo1
git remote set-url origin --push --add user2@repo2
git remote -v
```

Then `git push` will push to user1@repo1, then push to user2@repo2.
https://stackoverflow.com/a/12795747
---
