


https://stackoverflow.com/questions/66839948/whats-the-point-of-the-prefix-line-in-conda-environment-file-when-creating-a-ne


t specifies the directory to put the environment in.

Straight from the [documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#specifying-a-location-for-an-environment):

> You can control where a conda environment lives by providing a path to a target directory when creating the environment. For example, the following command will create a new environment in a subdirectory of the current working directory called envs:
> 
> ```python
> conda create --prefix ./envs jupyterlab=0.35 matplotlib=3.1 numpy=1.16
> ```

Or, you could have checked:

```python
conda env create --help
```

And it shows:

```python
  -p PATH, --prefix PATH
                        Full path to environment location (i.e. prefix).
```


