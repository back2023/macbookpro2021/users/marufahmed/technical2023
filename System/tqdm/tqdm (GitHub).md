
`tqdm` derives from the Arabic word _taqaddum_ (تقدّم) which can mean "progress," and is an abbreviation for "I love you so much" in Spanish (_te quiero demasiado_).

Instantly make your loops show a smart progress meter - just wrap any iterable with `tqdm(iterable)`, and you're done!

https://github.com/tqdm/tqdm#hooks-and-callbacks

