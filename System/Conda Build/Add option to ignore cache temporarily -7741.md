https://github.com/conda/conda/issues/7741

### Cleam
`conda clean --all --force-pkgs-dirs` works fine but is a total overkill.
https://github.com/conda/conda/issues/7741#issuecomment-728157745

### Tmp cache
Wouldn't a less destructive workaround be to temporarily mask the other `pkgs_dirs` by pointing `CONDA_PKGS_DIRS` to an empty directory? For example,

```bash
CONDA_PKGS_DIRS=$(mktemp -d) conda install -c /path/to/some/local/channel mypkg
```
This won't cache the installed package(s). If that was needed, then one could also move the stuff from the temp cache to the original afterward
https://github.com/conda/conda/issues/7741#issuecomment-780339451

