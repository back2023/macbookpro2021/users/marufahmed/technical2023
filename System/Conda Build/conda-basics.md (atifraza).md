
https://gist.github.com/atifraza/b1a92ae7c549dd011590209f188ed2a0




## Creating an environment file


If the environment is intended to be created in the `./env` sub-directory, then the `name` property should be set to `null`. The following file snippet shows such an example and also includes version numbers for the packages.

```yaml
name: null

dependencies:
  - ipython=7.13
  - matplotlib=3.1
  - pandas=1.0
  - python=3.6
  - scikit-learn=0.22
  - pip=20.0
    - tensorflow=1.13
```

