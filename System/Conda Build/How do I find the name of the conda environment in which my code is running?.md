
https://stackoverflow.com/questions/36539623/how-do-i-find-the-name-of-the-conda-environment-in-which-my-code-is-running


You want `$CONDA_DEFAULT_ENV` or `$CONDA_PREFIX`:

```python
$ source activate my_env
(my_env) $ echo $CONDA_DEFAULT_ENV
my_env

(my_env) $ echo $CONDA_PREFIX
/Users/nhdaly/miniconda3/envs/my_env

$ source deactivate
$ echo $CONDA_DEFAULT_ENV  # (not-defined)

$ echo $CONDA_PREFIX  # (not-defined)
```

In python:

```python
import os
print(os.environ['CONDA_DEFAULT_ENV'])
```

for the absolute entire path which is usually more useful:

```python
Python 3.9.0 | packaged by conda-forge | (default, Oct 14 2020, 22:56:29) 
[Clang 10.0.1 ] on darwin
import os; print(os.environ["CONDA_PREFIX"])
/Users/miranda9/.conda/envs/synthesis
```