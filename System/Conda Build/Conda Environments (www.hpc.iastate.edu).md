https://www.hpc.iastate.edu/guides/virtual-environments/conda-environments

## Overview

- [Conda](https://conda.io/) is a software package manager for data science that allows unprivileged (non-administrative) Linux or MacOS users to search, fetch, install, upgrade, use, and manage supported open-source software packages and programming languages/libraries/environments (primarily Python and R, but also others such as Perl, Java, and Julia) in a directory they have write access to. 
- Conda allows users to create reproducible scientific software environments, including outside of ISU clusters.


## Installing Software

- Home Directory - **Note:** some Conda packages (with dependencies) can take gigabytes of storage space. Since home directory quotas are low, it is not recommended to install conda packages and envs in the home directory.


### Managing Conda Cache

- The default location for Conda to cache files is the user's home directory, which can rapidly fill and cause issues. 
	- This behavior can be changed by setting the **pkgs_dirs** entry in the **.condarc** file, or 
	- setting the **CONDA_PKGS_DIRS** environment variable.

- First, to see the current cache directory, issue: 
```bash
conda info
```

- Editing/creating the **pkgs_dirs** entry in the **.condarc** file will change the cache directory
- Another method to adjust the cache directory is by setting the CONDA_PKGS_DIRS environment variable. To do this, issue:
```bash
#Export the variable:  
export CONDA_PKGS_DIRS=/path/to/desired/cache/directory

#Confirm change with conda info:  
conda info
```

### Pip Installs in a Conda Environment


While performing pip installs, consider adding --no-cache-dir to avoid filling the home directory with cached packages. An install command would look like:
```bash
 python3 -m pip install <package> --no-cache-dir
```