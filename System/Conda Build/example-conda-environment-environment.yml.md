
https://github.com/binder-project/example-conda-environment/blob/master/environment.yml


```yaml
name: example-environment

dependencies:
  - python=3.4
  - numpy
  - toolz
  - matplotlib
  - dill
  - pandas
  - partd
  - bokeh
  - pip:
    - git+https://github.com/blaze/dask.git#egg=dask[complete]
```


