
https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html



## Creating an environment from an `environment.yml` file

Create the environment from the `environment.yml` file:
```bash
conda env create -f environment.yml
```

## Specifying a location for an environment


- An additional benefit of creating your project’s environment inside a subdirectory is that you can then use the same name for all your environments.
-  If you keep all of your environments in your `envs` folder, you’ll have to give each environment a different name.
	```bash
	conda create --prefix ./envs jupyterlab=3.2 matplotlib=3.5 numpy=1.21
	```
 

There are a few things to be aware of when placing conda environments outside of the default `envs` folder.
	1. Conda can no longer find your environment with the `--name` flag. You’ll generally need to pass the `--prefix` flag along with the environment’s full path to find the environment.
	2. Specifying an install path when creating your conda environments makes it so that your command prompt is now prefixed with the active environment’s absolute path rather than the environment’s name.




## Building identical conda environments


- You can use explicit specification files to build an identical conda environment on the same operating system platform, either on the same machine or on a different machine.
	- Run `conda list --explicit` to produce a spec list.
	- To create this spec list as a file in the current working directory, run:
	```bash
	conda list --explicit > spec-file.txt
	```
	- An explicit spec file is not usually cross platform, and therefore has a comment at the top such as `# platform: osx-64` showing the platform where it was created.
	- Conda does not check architecture or dependencies when installing from a spec file. To ensure that the packages work correctly, make sure that the file was created from a working environment, and use it on the same architecture, operating system, and platform, such as linux-64 or osx-64.



##  Activating an environment

### Nested activation

- By default, `conda activate` will deactivate the current environment before activating the new environment and reactivate it when deactivating the new environment.
- 



### Creating an environment file manually

```yaml
name: stats2
channels:
  - javascript
dependencies:
  - python=3.9
  - bokeh=2.4.2
  - conda-forge::numpy=1.21.*
  - nodejs=16.13.*
  - flask
  - pip
  - pip:
    - Flask-Testing
```


- You can exclude the default channels by adding `nodefaults` to the channels list.
```
channels:
  - javascript
  - nodefaults
```
- This is equivalent to passing the `--override-channels` option to most `conda` commands.

- Adding `nodefaults` to the channels list in `environment.yml` is similar to removing `defaults` from the [channels list](https://conda.io/projects/conda/en/latest/user-guide/configuration/use-condarc.html#config-channels) in the `.condarc` file.