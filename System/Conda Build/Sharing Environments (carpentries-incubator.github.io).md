
https://carpentries-incubator.github.io/introduction-to-conda-for-data-scientists/04-sharing-environments/index.html







### Updating an environment

```bash
 conda env update --prefix ./env --file environment.yml  --prune
```


## Rebuilding a Conda environment from scratch

- To rebuild a Conda environment from scratch you can pass the `--force` option to the `conda env create` command which will remove any existing environment directory before rebuilding it using the provided environment file.

```bash
conda env create --prefix ./env --file environment.yml --force
```


