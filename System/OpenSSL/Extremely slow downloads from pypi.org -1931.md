
https://github.com/pypi/support/issues/1931


### TLS Debug / IPv6 (If available)

```shell
$ echo -n | openssl s_client -6 -connect pypi.org:443

```

```shell
$ echo -n | openssl s_client -6 -connect files.pythonhosted.org:443

```

