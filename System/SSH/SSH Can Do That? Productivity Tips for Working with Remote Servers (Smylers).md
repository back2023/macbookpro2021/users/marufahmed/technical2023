
https://blogs.perl.org/users/smylers/2011/08/ssh-productivity-tips.html

### Multiple Connections

### Copying Files

### Repeated Connections

### Don’t Type Passwords

### Don’t Type Full Hostnames

### Hostname Aliases

### Don’t Type Usernames

### Onward Connections

### Resilient Connections

### Restarting Connections

### Persistent Remote Processes

### Jumping Through Servers

### Escaping from Networks

### Defeating Web Proxies

### Gui Applications and Remote Windows

### Operating on Remote Files Locally

### Editing Remote Files with Vim

### Editing Remote Files with Emacs

### Connecting to Remote Services with Local Apps

### Forwarding Specific Ports

### Setting Hostnames for Forwarded Connections

### Using Sshuttle to Forward Connections

### Avoiding Delays

### Faster Connections

