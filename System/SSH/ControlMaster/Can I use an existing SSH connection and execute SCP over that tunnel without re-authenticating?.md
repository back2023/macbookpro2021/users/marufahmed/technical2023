

Yes, session sharing is possible: `man ssh_config` and search for `ControlMaster` and/or check [here](http://blogs.perl.org/users/smylers/2011/08/ssh-productivity-tips.html) and [here](https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Multiplexing). Is this what you are looking for?


https://stackoverflow.com/questions/15949316/can-i-use-an-existing-ssh-connection-and-execute-scp-over-that-tunnel-without-re


