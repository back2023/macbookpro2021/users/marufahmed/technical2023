
https://support.zoom.us/hc/en-us/articles/6618841634957-Zoom-error-on-macOS-You-are-unable-to-connect-to-Zoom


## How to uninstall Zoom

1. From your desktop, click **Go** then **Applications**.
2. Right-click on **zoom.us** then click **Show package contents**.
3. Open the **Contents** folder then **Frameworks**.
4. Double-click on **ZoomUninstaller** and wait for the uninstallation to complete.

## How to reinstall Zoom

1. Visit our [Download Center](https://zoom.us/download).
2. Under **Zoom Client for Meetings**, click **Download**.
3. Double-click the downloaded file. It is typically saved to your Downloads folder.
4. Once the installer opens, click **Continue**.
5. Choose the destination for installation:
    - **Install for all users of this computer**  
        **Note**: This requires administrator credentials to install for all users on the device.
    - **Install only for me**
6. Click **Continue**.
7. (Optional) If you want to change the destination for installation, choose **Change Install Location**.
8. Click **Install**.
9. (Optional) If you choose **Install for all users of this computer**, enter the administrator credentials for the device.
10. Click **Install Software**.
11. Once the installation is complete, click **Close**.
