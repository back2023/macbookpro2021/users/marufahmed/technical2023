### Install
Source: https://cloud.google.com/storage/docs/gsutil_install#linux
```
mkdir /scratch/fp0/mah900/gsutil
cd /scratch/fp0/mah900/gsutil
curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-424.0.0-linux-x86_64.tar.gz
 
tar -xf google-cloud-cli-424.0.0-linux-x86_64.tar.gz

./google-cloud-sdk/install.sh 
Do you want to help improve the Google Cloud CLI (y/N)?  N
Do you want to continue (Y/n)?  n

./google-cloud-sdk/bin/gcloud init
You must log in to continue. Would you like to log in (Y/n)?  Y
Would you like to create one? (Y/n)?  n

/scratch/fp0/mah900/gsutil/google-cloud-sdk/bin/gsutil
```

### Access public data
https://cloud.google.com/storage/docs/access-public-data#command-line
```
cd /g/data/wb00/admin/testing/metnet/

/scratch/fp0/mah900/gsutil/google-cloud-sdk/bin/gsutil ls -r gs://public-datasets-eumetsat-solar-forecasting/satellite/EUMETSAT/SEVIRI_RSS/v3/eumetsat_seviri_hrv_uk.zarr

/scratch/fp0/mah900/gsutil/google-cloud-sdk/bin/gsutil -m cp -r gs://public-datasets-eumetsat-solar-forecasting/satellite/EUMETSAT/SEVIRI_RSS/v3/eumetsat_seviri_hrv_uk.zarr .

...
/ [173.6k/173.6k files][107.0 GiB/107.0 GiB] 100% Done  37.6 MiB/s ETA 00:00:00 
Operation completed over 173.6k objects/107.0 GiB.

```