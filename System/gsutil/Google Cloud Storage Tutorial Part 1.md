
https://medium.com/google-cloud/google-cloud-storage-tutorial-part-1-aee81f9d3247



## Working with the gsutil tool

```bash
#Create a bucket:
gsutil mb gs://gwbucket
#Copy some files to the bucket:
gsutil cp *.jpg gs://gwbucket
#List the files in the bucket:
gsutil ls -l gs://gwbucket
#Copy a file from our bucket back to the local /tmp directory
gsutil cp gs://gwbucket/sunset.jpg /tmp
#Delete the files:
gsutil rm gs://gwbucket/*
#Remove the bucket:
gsutil rb gs://gwbucket
```


Here’s another example using [rsync](https://cloud.google.com/storage/docs/gsutil/commands/rsync) and [versioning](https://medium.com/p/aee81f9d3247/edit):

```bash
#Create a new bucket:
gsutil mb gs://gwnewbucket
#Turn on versioning for the bucket
gsutil versioning set on gs://gwnewbucket
#rsync the current directory to our new bucket
#Adding -m to run multiple parallel processes (speed boost)
gsutil -m rsync -r -d . gs://gwnewbucket
#List all of the files in the bucket:
gsutil ls -lr gs://gwnewbucket
```