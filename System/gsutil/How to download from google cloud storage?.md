
https://stackoverflow.com/questions/18794522/how-to-download-from-google-cloud-storage



Download gcloud sdk first and then write

```
gsutil cp -r gs://bucketname local_folder_path

```
Source: https://stackoverflow.com/a/61383084



 

The right click method didn't work for me, here is what worked:

**Step 1:** install gsutil on your local machine: [https://cloud.google.com/storage/docs/gsutil_install?hl=fr](https://cloud.google.com/storage/docs/gsutil_install?hl=fr)

**Step 2:** copy files from bucket to your local machine running the command: `gsutil cp [-r if repository] gs://[BUCKET_NAME]/[OBJECT_NAME] [OBJECT_DESTINATION_IN_LOCAL]`
Source: https://stackoverflow.com/a/55825439

