
https://stackoverflow.com/questions/58145462/what-is-the-difference-between-gcloud-and-gsutil




The gsutil command is used only for Cloud Storage.

With the gcloud command, you can interact with other Google Cloud products like the App Engine, Google Kubernetes Engine etc. You can have a look [here](https://cloud.google.com/sdk/gcloud/) and [here](https://cloud.google.com/storage/docs/gsutil) for more info.
Source: https://stackoverflow.com/a/58147001




**The gsutil** is a Python application that lets you access Google Cloud Storage from the command line. You can use gsutil to do a wide range of bucket and object management tasks, including:

Creating and deleting buckets. Uploading, downloading, and deleting objects. Listing buckets and objects. Moving, copying, and renaming objects. Editing object and bucket ACLs.

**The gcloud** command-line interface is the primary CLI tool to create and manage Google Cloud resources. You can use this tool to perform many common platform tasks either from the command line or in scripts and other automations.

For example, you can use the gcloud CLI to create and manage:

Google Compute Engine virtual machine instances and other resources, Google Cloud SQL instances, Google Kubernetes Engine clusters, Google Cloud Dataproc clusters and jobs, Google Cloud DNS managed zones and record sets, Google Cloud Deployment manager deployments.
Source: https://stackoverflow.com/a/63006519


**"gcloud"** can create and manage **Google Cloud resources** while **"gsutil"** cannot do so.

**"gsutil"** can manipulate **buckets**, **bucket's objects** and **bucket ACLs** on **GCS(Google Cloud Storage)** while **"gcloud"** cannot do so.
Source: https://stackoverflow.com/a/72153944