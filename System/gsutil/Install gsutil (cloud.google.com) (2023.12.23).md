
https://cloud.google.com/storage/docs/gsutil_install

### There is an previous install from 2023.03.31 [Gsutil-01]
(Forgot about it before installing this one)

- Simply installing gsutil gives you immediate read and/or write access to public data. 
- Authenticating to the Cloud Storage service gives you read and/or write access to protected data that has been shared with you. 
- Enabling billing gives you the ability to create and manage your own buckets.

- you can specify the full path when running Cloud Storage gsutil (for example, `/home/users/joan/gsutil/gsutil ls`).

### Follow the instructions for your operating system to install gsutil as a part of the Google Cloud CLI:

1. Linux 64-bit (x86_64)
```
curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-458.0.1-linux-x86_64.tar.gz
tar -xf google-cloud-cli-458.0.1-linux-x86_64.tar.gz
```
- Optional: To replace an existing installation, remove the existing `google-cloud-sdk` directory and then extract the archive to the same location.

```bash
./google-cloud-sdk/install.sh --help
./google-cloud-sdk/install.sh --usage-reporting false
...
Do you want to continue (Y/n)? n
```


## Setting Up Credentials to Access Protected Data

- In order to access protected data or write to a protected bucket, you need to set up credentials (authenticate).
- To establish access, run the command [`gcloud init`](https://cloud.google.com/sdk/gcloud/reference/init) and follow the instructions provided in the command line, which include logging into your user account.
- Note that you likely already performed this setup if you followed the [installation steps](https://cloud.google.com/storage/docs/gsutil_install#sdk-install) above.
- If you ran `gcloud init` previously, when you run the command again you are asked if you want to re-initialize the configuration or create a new one. For more information, see [Initialize the Google Cloud CLI](https://cloud.google.com/sdk/docs/initializing#initialize_the).

```bash
 conda deactivate
./google-cloud-sdk/bin/gsutil help
./google-cloud-sdk/bin/gcloud init
...
 ./google-cloud-sdk/bin/gcloud init

Welcome! This command will take you through the configuration of gcloud.

Settings from your current configuration [default] are:
core:
  account: bsc98065@gmail.com
  disable_usage_reporting: 'True'

Pick configuration to use:
 [1] Re-initialize this configuration [default] with new settings 
 [2] Create a new configuration
 
...
Network diagnostic detects and fixes local network connection issues.
Checking network connection...done.                                                                        Reachability Check passed.
Network diagnostic passed (1/1 checks passed).

...
You are logged in as: [bsc98065@gmail.com].

WARNING: Listing available projects failed: There was a problem refreshing your current auth tokens: ('invalid_grant: Bad Request', {'error': 'invalid_grant', 'error_description': 'Bad Request'})
Please run:

  $ gcloud auth login

to obtain new credentials.

If you have already logged in with a different account, run:

  $ gcloud config set account ACCOUNT

to select an already authenticated account to use.
Enter project ID you would like to use: fuxi-2023-12

...
Created a default .boto configuration file at [/home/900/mah900/.boto]. See this file and
[https://cloud.google.com/storage/docs/gsutil/commands/config] for more
information about configuring Google Cloud Storage.
Your Google Cloud SDK is configured and ready to use!

* Commands that require authentication will use bsc98065@gmail.com by default
* Commands will reference project `fuxi-2023-12` by default
Run `gcloud help config` to learn how to change individual settings

...
```

To re-initialize
```
./google-cloud-sdk/bin/gcloud init
```
Source: https://cloud.google.com/sdk/docs/initializing


#### Troubleshooting
- Run the command `gsutil version -l` and check the value for `using cloud sdk`. 
	- If `False`, your system is using the stand-alone version of gsutil when you run commands. 
	- It's recommended that you remove the stand-alone version of gsutil from your system; however, you can alternatively authenticate using [`gsutil config -a` or `gsutil config -e`](https://cloud.google.com/storage/docs/gsutil/commands/config).

***

```bash
./google-cloud-sdk/bin/gsutil version -l
gsutil version: 5.27
checksum: 5cf9fcad0f47bc86542d009bbe69f297 (OK)
boto version: 2.49.0
python version: 3.11.6 (main, Dec 13 2023, 05:00:39) [Clang 17.0.1 ]
OS: Linux 4.18.0-477.27.1.el8.nci.x86_64
multiprocessing available: True
using cloud sdk: True
pass cloud sdk credentials to gsutil: True
config path(s): /home/900/mah900/.boto, /home/900/mah900/.config/gcloud/legacy_credentials/bsc98065@gmail.com/.boto
gsutil path: /scratch/fp0/mah900/ace/google-cloud-sdk/bin/gsutil
compiled crcmod: True
installed via package manager: False
editable install: False
shim enabled: False
```
Source: https://cloud.google.com/storage/docs/gsutil#builtinhelp

```
 ./google-cloud-sdk/bin/gsutil ls
Your credentials are invalid. Please run
$ gcloud auth login
```
Source: https://linuxcommandlibrary.com/man/gsutil

```
./google-cloud-sdk/bin/gcloud auth login
Go to the following link in your browser:
...
Enter authorization code:
...
You are now logged in as [bsc98065@gmail.com].
Your current project is [fuxi-2023-12].  You can change this setting by running:
  $ gcloud config set project PROJECT_ID
```