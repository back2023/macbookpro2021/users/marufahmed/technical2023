https://docs.github.com/en/authentication/connecting-to-github-with-ssh/checking-for-existing-ssh-keys?platform=linux


1. Enter `ls -al ~/.ssh` to see if existing SSH keys are present.
    ```shell
    $ ls -al ~/.ssh
    # Lists the files in your .ssh directory, if they exist
    ```

2. Check the directory listing to see if you already have a public SSH key. By default, the filenames of supported public keys for GitHub are one of the following.
	- _id_rsa.pub_
	- _id_ecdsa.pub_
	- _id_ed25519.pub_
