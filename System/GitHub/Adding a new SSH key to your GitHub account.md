
https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account?platform=linux&tool=webui


## Prerequisites 
1. Check for existing SSH keys. For more information, see "[Checking for existing SSH keys](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/checking-for-existing-ssh-keys)."

2. 1. Generate a new SSH key and add it to your machine's SSH agent. For more information, see "[Generating a new SSH key and adding it to the ssh-agent](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)."


## Adding a new SSH key to your account

1. Copy the SSH public key to your clipboard.
   - If your SSH public key file has a different name than the example code, modify the filename to match your current setup. When copying your key, don't add any newlines or whitespace
   ```bash
   $ cat ~/.ssh/id_ed25519.pub
	# Then select and copy the contents of the id_ed25519.pub file
	# displayed in the terminal to your clipboard
   ```
 
  - **Tip:** Alternatively, you can locate the hidden `.ssh` folder, open the file in your favorite text editor, and copy it to your clipboard.
2. In the upper-right corner of any page, click your profile photo, then click **Settings**.
3. In the "Access" section of the sidebar, click  **SSH and GPG keys**.
4. 1. Click **New SSH key** or **Add SSH key**.

   