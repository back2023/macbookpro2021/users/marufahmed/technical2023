

https://stackoverflow.com/questions/25411366/git-repo-says-its-up-to-date-after-pull-but-files-are-not-updated


Try this:

```
 git fetch --all
 git reset --hard origin/master
```
Explanation:
`git fetch` downloads the latest from remote without trying to merge or rebase anything.