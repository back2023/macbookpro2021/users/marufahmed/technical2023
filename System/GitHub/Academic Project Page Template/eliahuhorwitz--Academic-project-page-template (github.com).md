
https://github.com/eliahuhorwitz/Academic-project-page-template


## About

A project page template for academic papers. Demo at [https://eliahuhorwitz.github.io/Academic-project-page-template/](https://eliahuhorwitz.github.io/Academic-project-page-template/)

# Academic Project Page Template

This is an academic paper project page template.

Example project pages built using this template are:

- [https://www.vision.huji.ac.il/deepsim/](https://www.vision.huji.ac.il/deepsim/)
- [https://www.vision.huji.ac.il/3d_ads/](https://www.vision.huji.ac.il/3d_ads/)
- [https://www.vision.huji.ac.il/ssrl_ad/](https://www.vision.huji.ac.il/ssrl_ad/)
- [https://www.vision.huji.ac.il/conffusion/](https://www.vision.huji.ac.il/conffusion/)






