
### [Version specifiers]
(https://peps.python.org/pep-0440/#version-specifiers)

A version specifier consists of a series of version clauses, separated by commas. For example:
 `~= 0.9, >= 1.0, != 1.3.4.*, < 2.0`

The comparison operator determines the kind of version clause:
-   `~=`: [Compatible release](https://peps.python.org/pep-0440/#compatible-release) clause
-   `==`: [Version matching](https://peps.python.org/pep-0440/#version-matching) clause
-   `!=`: [Version exclusion](https://peps.python.org/pep-0440/#version-exclusion) clause
-   `<=`, `>=`: [Inclusive ordered comparison](https://peps.python.org/pep-0440/#inclusive-ordered-comparison) clause
-   `<`, `>`: [Exclusive ordered comparison](https://peps.python.org/pep-0440/#exclusive-ordered-comparison) clause
-   `===`: [Arbitrary equality](https://peps.python.org/pep-0440/#arbitrary-equality) clause.