An alternative solution to passing `--no-cache-dir` is to set environment variable `PIP_NO_CACHE_DIR` to `1`:
```
export PIP_NO_CACHE_DIR=1
```
https://github.com/conda/conda/issues/6805#issuecomment-809454842

