
https://pip.pypa.io/en/stable/reference/build-system/

When dealing with installable source distributions of a package, pip does not directly handle the build process for the package. This responsibility is delegated to “build backends” -- also known as “build systems”. This means that pip needs an interface, to interact with these build backends.

There are two main interfaces that pip uses for these interactions:

[`pyproject.toml` based](https://pip.pypa.io/en/stable/reference/build-system/pyproject-toml/)

Standards-backed interface, that has explicit declaration and management of build dependencies.

[`setup.py` based](https://pip.pypa.io/en/stable/reference/build-system/setup-py/)

Legacy interface, that we’re working to migrate users away from. Has no good mechanisms to declare build dependencies.

Details on the individual interfaces can be found on their dedicated pages, linked above. This document covers the nuances around which build system interface pip will use for a project, as well as details that apply to all the build system interfaces that pip may use.


