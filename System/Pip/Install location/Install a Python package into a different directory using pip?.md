
https://stackoverflow.com/questions/2915471/install-a-python-package-into-a-different-directory-using-pip


The [--target](https://pip.pypa.io/en/latest/cli/pip_install/#cmdoption-t) switch is the thing you're looking for:

```python
pip install --target d:\somewhere\other\than\the\default package_name
```

But you still need to add `d:\somewhere\other\than\the\default` to `PYTHONPATH` to actually use them from that location.

> **-t, --target <dir>**  
>  Install packages into <dir>. By default this will not replace existing files/folders in <dir>.  
>  Use --upgrade to replace existing packages in <dir> with new versions.

Source: https://stackoverflow.com/a/19404371

