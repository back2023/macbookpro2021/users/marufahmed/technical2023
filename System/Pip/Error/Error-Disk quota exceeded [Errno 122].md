
```
ERROR: Could not install packages due to an OSError: [Errno 122] Disk quota exceeded
```

### Reason
Default location of pip install is home dir and it is full. 

### solution
Do not use the `--user`  option. 

#### Try
1. Change Pip target location 
```bash
PIP_TARGET=/path/to/pip/dir
```
 https://stackoverflow.com/questions/24174821/how-to-change-default-install-location-for-pip
 
2. Through the terminal/ command line
   ```
   pip install -target=<target_dir> <package_name>
   ```
https://codeigo.com/python/change-pip-install-location
 

#### Install torchdata
```Bash
# Make a new dir
mkdir /scratch/fp0/mah900/env/pip-packages

pip install -target=/scratch/fp0/mah900/env/pip-packages --user torchdata
#ERROR: Can not combine '--user' and '--target'
#So, --user option selets local dir ??

pip install torchdata # working, do not user the --user option
```
