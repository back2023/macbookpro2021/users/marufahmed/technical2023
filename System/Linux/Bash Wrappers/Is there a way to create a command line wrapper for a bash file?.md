

Create a file named `run` with the contents:

```bash
#!/bin/sh
exec /pathtoscript/script.sh "$@"
```

Place the file in your path and set the executable bit.

### What is "your path"?

At the command line, type:

```bash
echo $PATH
```

You will see a colon-separated list of directories. These are the directories that the shell searches when looking for a command to run. They are collectively called the path. You will want to place your new file `run` in any one of those directories.

### Alternate approach for interactive work

If you just want `run` to work when you are working interactively, you can create an alias:

```bash
alias run=/pathtoscript/script.sh
```

If you want this alias saved permanently, put that line in the `.bashrc` in your home directory.


https://superuser.com/questions/789275/is-there-a-way-to-create-a-command-line-wrapper-for-a-bash-file


