

https://stackoverflow.com/questions/537942/how-to-list-running-screen-sessions


To list all of the screen sessions for a user, run the following command as that user:

```bash
screen -ls
```

To see all screen sessions on a specific machine you can do:

```bash
ls -laR /var/run/screen/
```

