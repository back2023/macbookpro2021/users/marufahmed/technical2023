
https://unix.stackexchange.com/questions/52313/how-to-get-execution-time-of-a-script-effectively



```bash
# Reset BASH time counter
SECONDS=0
    # 
    # do stuff
    # 
ELAPSED="Elapsed: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"
```
 