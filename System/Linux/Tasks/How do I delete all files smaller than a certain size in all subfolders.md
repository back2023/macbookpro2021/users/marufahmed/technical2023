https://superuser.com/questions/644272/how-do-i-delete-all-files-smaller-than-a-certain-size-in-all-subfolders

```bash
find . -type f -name "*.tif" -size -160k -delete
```

```bash
find some/dir -type f -name "*.tif" -size -160k -delete
```

```bash
find /absolute/path -type f -name "*.tif" -size -160k -delete
```