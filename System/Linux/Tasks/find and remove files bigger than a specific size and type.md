
https://unix.stackexchange.com/questions/287629/find-and-remove-files-bigger-than-a-specific-size-and-type


```bash
find -type f \( -name "*zip" -o -name "*tar" -o -name "*gz" \) -size +1M -delete
```

- the `\( \)` construct allows to group different filename patterns
- by using `-delete` option, we can avoid piping and troubles with `xargs` See [this](https://unix.stackexchange.com/questions/131766/why-does-my-shell-script-choke-on-whitespace-or-other-special-characters), [this](https://unix.stackexchange.com/questions/24954/when-is-xargs-needed) and [this](https://unix.stackexchange.com/questions/90886/how-can-i-find-files-and-then-use-xargs-to-move-them)
- `./` or `.` is optional when using `find` command for current directory



```bash
find -type f \( -name "Update*" \) -size +100k 

find -type f \( -name "Update*" \) -size +100k -delete

```

