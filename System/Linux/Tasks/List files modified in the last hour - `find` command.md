

```
$ find . -mtime -1
```
-   the `.` is the search path
-   `-mtime` time parameter
-   `-1` list files modified in the last 24 hours
https://coderwall.com/p/lspgjq/list-files-modified-in-the-last-hour-find-command