
https://www.geeksforgeeks.org/traceroute-command-in-linux-with-examples/

**traceroute** command in Linux prints the route that a packet takes to reach the host. This command is useful when you want to know about the route and about all the hops that a packet takes.

