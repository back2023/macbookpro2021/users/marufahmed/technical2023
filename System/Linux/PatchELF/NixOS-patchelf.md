
PatchELF is a simple utility for modifying existing ELF executables and libraries. In particular, it can do the following:

- Change the dynamic loader ("ELF interpreter") of executables:
    
    ```shell
    $ patchelf --set-interpreter /lib/my-ld-linux.so.2 my-program
    ```
    
- Change the `RPATH` of executables and libraries:
    
    ```shell
    $ patchelf --set-rpath /opt/my-libs/lib:/other-libs my-program
    ```
    
- Shrink the `RPATH` of executables and libraries:
    
    ```shell
    $ patchelf --shrink-rpath my-program
    ```

https://github.com/NixOS/patchelf