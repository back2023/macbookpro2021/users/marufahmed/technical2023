
https://superuser.com/questions/332610/where-is-the-temporary-directory-in-linux


`TMPDIR` This variable shall represent a pathname of a directory made available for programs that need a place to create temporary files.

If the `$TMPDIR` environment variable is set, use that path, and neither use `/tmp/` nor `/var/tmp/` directly.

