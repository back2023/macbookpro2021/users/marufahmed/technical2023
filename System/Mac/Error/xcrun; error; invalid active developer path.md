
```shell
$ git status

xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun
```

 
Found the fix for the problem [here](http://tips.tutorialhorizon.com/2015/10/01/xcrun-error-invalid-active-developer-path-library-developer-commandline-tools-missing-xcrun/).
```shell
xcode-select --install
```
Source: https://stackoverflow.com/questions/32893412/command-line-tools-not-working-os-x-el-capitan-sierra-high-sierra-mojave/32894314#32894314


```shell
xcode-select --install
```
If the above alone doesn't do it, then also run:
```shell
sudo xcode-select --reset
```
Source: https://apple.stackexchange.com/questions/254380/why-am-i-getting-an-invalid-active-developer-path-when-attempting-to-use-git-a