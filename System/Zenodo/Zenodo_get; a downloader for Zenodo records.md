

https://zenodo.org/records/1261813


This is a small tool for downloading large Zenodo records which contain many files.

It can generate URL list (for other download managers), download the files, and  check md5 checksums. 

Installation:  One could use this source directly, but there is also a pypi package which is easier to use:

```
pip install zenodo-get
```

For more information, feature requests or bug reports, please visit GitLab: [https://gitlab.com/dvolgyes/zenodo_get](https://gitlab.com/dvolgyes/zenodo_get)


