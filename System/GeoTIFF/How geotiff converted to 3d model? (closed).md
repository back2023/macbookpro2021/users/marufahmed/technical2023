
https://gis.stackexchange.com/questions/242526/how-geotiff-converted-to-3d-model


A quick search on the internet, I found that there are many tutorials available to show how to import raster DEM in Blender:

1. [How to Download and Import a Real World Terrain in blender (Youtube)](https://www.youtube.com/watch?v=I7YhIzQ_7DE)
2. [BLENDER TUTORIAL](https://somethingaboutmaps.wordpress.com/2014/01/01/blender-tutorial/)
3. [Preparing Your DEM](http://katiekowalsky.me/posts/2015/12/03/Preparing-DEM.html)

