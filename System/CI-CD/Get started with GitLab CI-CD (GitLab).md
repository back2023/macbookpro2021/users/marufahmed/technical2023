
https://docs.gitlab.com/ee/ci/


## Common terms

### `.gitlab-ci.yml` file 
https://docs.gitlab.com/ee/ci/#the-gitlab-ciyml-file

- To use GitLab CI/CD, 
	- you start with a `.gitlab-ci.yml` file at the root of your project which contains the configuration for your CI/CD pipeline. 
	- This file follows the YAML format and 
	- has its own special syntax.

- You can name this file anything you want, but `.gitlab-ci.yml` is the most common name.
  
- In the `.gitlab-ci.yml` file, you can define:
	- 
	- The location to deploy your application to.
	- Whether you want to run the scripts automatically or trigger any of them manually.


### Runners

- Runners are the agents that run your jobs. 
	- These agents can run on physical machines or virtual instances.
- In your `.gitlab-ci.yml` file, you can specify a container image you want to use when running the job. 
	- The runner loads the image, clones your project and runs the job either locally or in the container.
- If you use GitLab.com, SaaS runners on Linux, Windows, and macOS are already available for use. 
	- And you can register your own runners on GitLab.com if you’d like.
- If you don’t use GitLab.com, you can:
	- Register runners or use runners already registered for your self-managed instance.
	- Create a runner on your local machine.


### Pipelines 

- Pipelines are made up of jobs and stages:
	- **Jobs** define what you want to do. 
		- For example, test code changes, or deploy to a staging environment.
	- Jobs are grouped into **stages**. Each stage contains at least one job. 
		- Typical stages might be `build`, `test`, and `deploy`.

### CI/CD variables 

- CI/CD variables help you customize jobs by making values defined elsewhere accessible to jobs. 
	- They can be hard-coded in your `.gitlab-ci.yml` file, project settings, or dynamically generated
