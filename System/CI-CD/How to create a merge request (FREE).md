
https://apps.risksciences.ucla.edu/gitlab/help/user/project/merge_requests/creating_merge_requests.md

This document describes the several ways to create a merge request.

- When you start a new merge request, regardless of the method, you are taken to the [**New Merge Request** page](#new-merge-request-page) to fill it with information about the merge request.

- If you push a new branch to GitLab, also regardless of the method, you can click the [**Create Merge Request**](#create-merge-request-button) button and start a merge request from there.

### Create Merge Request button

- Once you have pushed a new branch to GitLab, visit your repository in GitLab and to see a call-to-action at the top of your screen from which you can click the button **Create Merge Request**.
  
- You can also see the **Create merge request** button in the top-right of the:
	- **Project** page.
	- **Repository > Files** page.
	- **Merge Requests** page.



### New merge request from your local environment

