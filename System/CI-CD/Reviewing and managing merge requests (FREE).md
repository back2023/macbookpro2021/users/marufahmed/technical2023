
https://apps.risksciences.ucla.edu/gitlab/help/user/project/merge_requests/reviewing_and_managing_merge_requests.md 

### View project merge requests

View all the merge requests in a project by navigating to **Project > Merge Requests**.





