
https://nouveau.freedesktop.org/

The **nouveau** project aims to build high-quality, free/libre software drivers for [nVidia cards](https://nouveau.freedesktop.org/CodeNames.html). “Nouveau” [_nuvo_] is the French word for “new”. Nouveau is composed of a Linux kernel KMS driver (nouveau), Gallium3D drivers in Mesa, and the Xorg DDX (xf86-video-nouveau). The kernel components have also been ported to [NetBSD](https://nouveau.freedesktop.org/NetBSD.html).

## Current Status

- 2D/3D acceleration supported on all GPUs (except for [GA10x](https://nouveau.freedesktop.org/CodeNames.html#nv170familyampere)); see [FeatureMatrix](https://nouveau.freedesktop.org/FeatureMatrix.html) for details.
- Video decoding acceleration supported on most pre-Maxwell cards; see [VideoAcceleration](https://nouveau.freedesktop.org/VideoAcceleration.html) for details.
- Support for manual performance level selection (also known as "reclocking") on [GM10x Maxwell](https://nouveau.freedesktop.org/CodeNames.html#nv110familymaxwell), [Kepler](https://nouveau.freedesktop.org/CodeNames.html#nve0familykepler) and [Tesla G94-GT218](https://nouveau.freedesktop.org/CodeNames.html#nv50familytesla) GPUs. Available in `/sys/kernel/debug/dri/0/pstate`
- Little hope of reclocking becoming available for [GM20x](https://nouveau.freedesktop.org/CodeNames.html#nv110familymaxwell) and newer GPUs as firmware now needs to be signed by NVIDIA to have the necessary access.

## News

- Jan, 2021: [GA10x](https://nouveau.freedesktop.org/CodeNames.html#nv170familyampere) kernel mode setting support merged in Linux 5.11.
- Jan, 2019: [TU10x](https://nouveau.freedesktop.org/CodeNames.html#nv160familyturing) acceleration support (with redistributable signed firmware) merged in Linux 5.6.
- Jan, 2019: Support for Turing merged into Linux 5.0.
- Nov, 2018: Support for HDMI 2.0 high-speed clocks merged into Linux 4.20 (GM200+ hardware only).
- Aug, 2018: Support for Volta merged into Linux 4.19.

