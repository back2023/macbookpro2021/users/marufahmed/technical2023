
https://godatadriven.com/blog/a-practical-guide-to-using-setup-py/

- _Note:_ nowadays the use of `setup.py` is discouraged in favour of `pyproject.toml` together with `setup.cfg`. 
	- Find out how to use those [here](https://godatadriven.com/blog/a-practical-guide-to-setuptools-and-pyproject-toml/).


- You could tell python where to look for the package by setting the `PYTHONPATH` environment variable or adding the path to `sys.path`, but that is far from ideal: 
	- it would require different actions on different platforms, and the path you need to set depends on the location of your code.  
- A much better way is to install your package using a `setup.py` and `pip`, 
	- since `pip` is the standard way to install all other packages, and 
	- it is bound it work the same on all platforms.

- 
