
https://github.com/raspstephan/raspstephan.github.io


Minimal Mistakes is a flexible two-column Jekyll theme, perfect for building personal sites, blogs, and portfolios. As the name implies, styling is purposely minimalistic to be enhanced and customized by you 😄.


### Remote theme method

Remote themes are similar to Gem-based themes, but do not require `Gemfile` changes or whitelisting making them ideal for sites hosted with GitHub Pages.

To install:

1. Create/replace the contents of your `Gemfile` with the following:
    
    ```ruby
    source "https://rubygems.org"
    
    gem "github-pages", group: :jekyll_plugins
    ```
    
2. Add `jekyll-include-cache` to the `plugins` array of your `_config.yml`.
    
3. Fetch and update bundled gems by running the following [Bundler](http://bundler.io/) command:
    
    ```shell
    bundle
    ```
    
4. Add `remote_theme: "mmistakes/minimal-mistakes@4.15.1"` to your `_config.yml` file. Remove any other `theme:` or `remote_theme:` entry.

