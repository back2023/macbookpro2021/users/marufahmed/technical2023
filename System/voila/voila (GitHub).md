

Rendering of live Jupyter notebooks with interactive widgets.

## [](https://github.com/voila-dashboards/voila#introduction)Introduction

Voilà turns Jupyter notebooks into standalone web applications.

Unlike the usual HTML-converted notebooks, each user connecting to the Voilà tornado application gets a dedicated Jupyter kernel which can execute the callbacks to changes in Jupyter interactive widgets.

- By default, Voilà disallows execute requests from the front-end, preventing execution of arbitrary code.
- By default, Voilà runs with the `strip_sources` option, which strips out the input cells from the rendered notebook.


https://github.com/voila-dashboards/voila


