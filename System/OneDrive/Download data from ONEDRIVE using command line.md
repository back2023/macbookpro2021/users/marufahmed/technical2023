

https://sushantag9.medium.com/download-data-from-onedrive-using-command-line-d27196a676d9


- Open the URL in either of the browser.  
- Open Developer options using Ctrl+Shift+I.  
- Go to Network tab.  
- Now click on download. Saving file isn’t required. We only need the network activity while browser requests the file from the server.  
- A new entry will appear which would look like “download.aspx?…”.  
- Right click on that and `Copy → Copy as cURL`.  
- Paste the copied content directly in the terminal and append ‘**--output file.extension**’ to save the content in **file.extension** since terminal isn’t capable of showing binary data.

