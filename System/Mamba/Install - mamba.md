
### Warning
Installing mamba into any other environment than `base` is not supported.
```
conda install mamba -n base -c conda-forge
```

