
https://community.atlassian.com/t5/Confluence-questions/insert-bitbucket-content-directly-into-confluence-document/qaq-p/1316916


You may find [Include Bitbucket for Confluence](https://marketplace.atlassian.com/apps/1212529/include-bitbucket-for-confluence-markdown-asciidoc?hosting=cloud&tab=overview) useful for your use case where you want to embed a Bitbucket file into Confluence and it is kept in sync to ensure you always have the latest version. 

You can simply **copy the URL of the Bitbucket file** and **paste it into the Confluence page**.

In the following example we use this Bitbucket URL of a `.js` file: [https://bitbucket.org/atlassian/atlassian-connect-express/src/master/lib/index.js](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/lib/index.js)

Then paste the URL in the Confluence page as shown in the video and the macro will automatically be applied to embed the file.

