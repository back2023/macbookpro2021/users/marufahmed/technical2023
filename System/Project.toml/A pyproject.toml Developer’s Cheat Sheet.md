
https://betterprogramming.pub/a-pyproject-toml-developers-cheat-sheet-5782801fb3ed

- [PEP 517](https://peps.python.org/pep-0517/) proposes a standard way to define alternative build systems for Python projects.
- And thanks to [PEP 518](https://peps.python.org/pep-0518/), developers can use `pyproject.toml` configuration files to set up the projects' build requirements.
  
- PEP 517 also introduces the concepts of build front and backend. 
	- According to the specification, a build frontend is a tool that takes arbitrary code and builds wheels from it. 
	- The actual building is done by the build backend.
	  
- 
