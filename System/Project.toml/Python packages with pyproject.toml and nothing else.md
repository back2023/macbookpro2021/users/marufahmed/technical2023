
https://til.simonwillison.net/python/pyproject
Created 2023-07-07T21:40:34-07:00, updated 2023-10-03T13:53:37-07:00 · [History](https://github.com/simonw/til/commits/main/python/pyproject.md) · [Edit](https://github.com/simonw/til/blob/main/python/pyproject.md)

- `pyproject.toml` is the new (or not so new, [the PEP](https://peps.python.org/pep-0621/) is dated June 2020) standard for Python packaging metadata.

- how to package a project with a single `pyproject.toml` file, using just `pip` and `build` to install and build that package.
  
- (Note that the approach described in this document likely only works for pure Python packages. If your package includes any binary compiled dependencies you likely need to use a different approach.)

- Here's the simplest `pyproject.toml` file I could get to work:
```bash
[project]
name = "demo-package"
version = "0.1"
```

- I put this in a folder called `/tmp/demo-package` and then added a file to that folder called `demo_package.py`
  
- Working with packages there are really just two main things to do with them. 
	- Install them in "editable" development mode using `pip install -e` and 
	- Build packages for distribution using `python -m build`.

- Both of those commands now work on a folder containing just a `pyproject.toml` file, with no `setup.py` or `setup.cfg` or any of the other old packaging files!


## It defaults to setuptools
https://til.simonwillison.net/python/pyproject#user-content-it-defaults-to-setuptools

- a `pyproject.toml` file without a `[build-system]` section defaults to using [setuptools](https://setuptools.pypa.io/). 
- Effectively it behaves the same as if you had added this block to the file:
```toml
[build-system]
requires = ["setuptools"]
build-backend = "setuptools.build_meta"
```
- If you want to be explicit you can add that section - it does no harm


## Editable mode with pip install  
https://til.simonwillison.net/python/pyproject#user-content-editable-mode-with-pip-install

- Then I used `pip install -e` to install an editable version of my new, minimal package:
```shell
pip install -e /tmp/demo-package 
```

- Can now import my package and run that function:
```python
>>> import demo_package
```
Can edit the `demo_package.py`

## Building a package with python -m build
https://til.simonwillison.net/python/pyproject#user-content-building-a-package-with-python--m-build

- These days the [build](https://pypi.org/project/build/) package is the recommended way to do that.
- Can install that using:
```bash
python3 -m pip install build
```

- Then run it in the `/tmp/demo-package` folder like this:
```shell
python3 -m build
```

These two files were created in `/tmp/demo-package/dist`:

```shell
ls dist
demo-package-0.1.tar.gz
demo_package-0.1-py3-none-any.whl
```

## It finds the Python files for you 
https://til.simonwillison.net/python/pyproject#user-content-it-finds-the-python-files-for-you

- The default behaviour now is to find all `*.py` files and all `*/*.py` files and include those - 
	- but to exclude common patterns such as `tests/` and `docs/` and `tests.py` and `test_*.py`.
- This behaviour is defined by `setuptools`. 
	- The [Automatic Discovery](https://setuptools.pypa.io/en/latest/userguide/package_discovery.html#automatic-discovery) section of the `setuptools` documentation describes these rules in detail.



## Adding metadata

```
[project]
name = "demo-package"
version = "0.1"
description = "This is a demo package"
readme = "README.md"
requires-python = ">=3.8"
```

- Note also the `requires-python = ">=3.8"` line to specify the minimum supported Python version.

- Can also include an author, a homepage URL and project URLs for things like my issue tracker in my projects.
```toml
[project]
name = "demo-package"
version = "0.1"
description = "This is a demo package"
readme = "README.md"
authors = [{name = "Simon Willison"}]
license = {text = "Apache-2.0"}
classifiers = [
    "Development Status :: 4 - Beta"
]

[project.urls]
Homepage = "https://github.com/simonw/demo-package"
Changelog = "https://github.com/simonw/demo-package/releases"
Issues = "https://github.com/simonw/demo-package/issues"
```

- Here's [a list](https://pypi.org/classifiers/) of the available `classifiers`.

## Adding dependencies

```toml
[project]
# ...
dependencies = [
    "httpx"
]
```

- These can use version specifiers, for example `httpx>=0.18.0` or `httpx~=0.18`.

## Test dependencies

- Those can be added in a section like this:
```toml
[project.optional-dependencies]
test = ["pytest"]
```

- I added that to my `/tmp/demo-package/pyproject.toml` file, then ran this in my elsewhere virtual environment:
```shell
pip install -e '/tmp/demo-package[test]'
```

## Package data
https://til.simonwillison.net/python/pyproject#user-content-package-data

- The build script will automatically find all Python files, 
	- but if you have other assets such as static CSS ond JavaScript, or templates with a `.html` extension, you need to specify package data.
- This works for adding everything in the `demo_package/static/` and `demo_package/templates/` directories:
```toml
[tool.setuptools.package-data]
demo_package = ["static/*", "templates/*"]
```

## Command-line scripts
https://til.simonwillison.net/python/pyproject#user-content-command-line-scripts

- I often build tools which include command-line scripts. 
	- These can be defined by adding a `scripts=` section to the `[project]` block, like this:
```toml
[project]
# ... previous configuration
scripts = { demo_package_hello = "demo_package:say_hello" }
```

- Or use this alternative syntax (here borrowed from my [db-build](https://github.com/simonw/db-build) Click app):
```toml
[project.scripts]
db-build = "db_build.cli:cli"
```
-Now run this again:
```shell
pip install -e '/tmp/demo-package'
```
- Typing `demo_package_hello` runs that function:
```shell
demo_package_hello
```
- We can see how that works with the following commands:
```shell
type demo_package_hello

demo_package_hello is /private/tmp/my-new-environment/venv/bin/demo_package_hello
...
```
 

## Entry points for Pluggy plugins 
https://til.simonwillison.net/python/pyproject#user-content-entry-points-for-pluggy-plugins

- I use [Pluggy](https://pluggy.readthedocs.io/) to provide a plugins mechanism for my [Datasette](https://datasette.io/) and [LLM](https://llm.datasette.io/) projects.
  
- Plugins require entry points to be configured in their packaging. 
	- The recipe for doing that for an LLM plugin (with [this feature](https://github.com/simonw/llm/issues/53)) looks like this:
```toml
[project.entry-points.llm]
markov = "llm_markov"
```

- For a [Datasette plugin](https://docs.datasette.io/en/stable/writing_plugins.html) that would look like this:
```toml
[project.entry-points.datasette]
cluster_map = "datasette_cluster_map"
```

## These packages work with pipx



## pip and build both depend on tomli [#](https://til.simonwillison.net/python/pyproject#user-content-pip-and-build-both-depend-on-tomli)

- One thing that puzzled me about this: 
	- TOML support was only added to the Python standard library in Python 3.11 - how come the `pip` and `build` packages are able to use it?
	- It turns out `pip` vendors [tomli](https://github.com/hukkin/tomli):
	- [https://github.com/pypa/pip/tree/23.1.2/src/pip/_vendor/tomli](https://github.com/pypa/pip/tree/23.1.2/src/pip/_vendor/tomli)

- And `build` lists it as a dependency for versions of Python prior to 3.11:
	- [https://github.com/pypa/build/blob/0.10.0/pyproject.toml#L40](https://github.com/pypa/build/blob/0.10.0/pyproject.toml#L40)
```toml
dependencies = [
  # ...
  'tomli >= 1.1.0; python_version < "3.11"',
]
```



