
https://packaging.python.org/en/latest/specifications/declaring-project-metadata/#declaring-project-metadata

- There are two kinds of metadata: _static_ and _dynamic_. 
	- Static metadata is specified in the `pyproject.toml` file directly and cannot be specified or changed by a tool 
		- (this includes data _referred_ to by the metadata, e.g. the contents of files referenced by the metadata). 
	- Dynamic metadata is listed via the `dynamic` key (defined later in this specification) and represents metadata that a tool will later provide.

- The keys defined in this specification MUST be in a table named `[project]` in `pyproject.toml`. 
	- No tools may add keys to this table which are not defined by this specification.
	- For tools wishing to store their own settings in `pyproject.toml`, they may use the `[tool]` table as defined in the [build dependency declaration specification](https://packaging.python.org/en/latest/specifications/declaring-build-dependencies/#declaring-build-dependencies). 
	- The lack of a `[project]` table implicitly means the [build backend](https://packaging.python.org/en/latest/glossary/#term-Build-Backend) will dynamically provide all keys.
	  
- The only keys required to be statically defined are:
	- `name`
    
- The keys which are required but may be specified _either_ statically or listed as dynamic are:
	- `version`
	  
- All other keys are considered optional and may be specified statically, listed as dynamic, or left unspecified.
	- The complete list of keys allowed in the `[project]` table are:
	- `authors`
	- `classifiers`
	- `dependencies`
	- `description`
	- `dynamic`
	- `entry-points`
	- `gui-scripts`
	- `keywords`
	- `license`
	- `maintainers`
	- `name`
	- `optional-dependencies`
	- `readme`
	- `requires-python`
	- `scripts`
	- `urls`
	- `version`





## Example
https://packaging.python.org/en/latest/specifications/declaring-project-metadata/#example

```toml
[project]
name = "spam"
version = "2020.0.0"
description = "Lovely Spam! Wonderful Spam!"
readme = "README.rst"
requires-python = ">=3.8"
license = {file = "LICENSE.txt"}
keywords = ["egg", "bacon", "sausage", "tomatoes", "Lobster Thermidor"]
authors = [
  {name = "Pradyun Gedam", email = "pradyun@example.com"},
  {name = "Tzu-Ping Chung", email = "tzu-ping@example.com"},
  {name = "Another person"},
  {email = "different.person@example.com"},
]
maintainers = [
  {name = "Brett Cannon", email = "brett@python.org"}
]
classifiers = [
  "Development Status :: 4 - Beta",
  "Programming Language :: Python"
]

dependencies = [
  "httpx",
  "gidgethub[httpx]>4.0.0",
  "django>2.1; os_name != 'nt'",
  "django>2.0; os_name == 'nt'",
]

# dynamic = ["version", "description"]

[project.optional-dependencies]
gui = ["PyQt5"]
cli = [
  "rich",
  "click",
]

[project.urls]
Homepage = "https://example.com"
Documentation = "https://readthedocs.org"
Repository = "https://github.com/me/spam.git"
Changelog = "https://github.com/me/spam/blob/master/CHANGELOG.md"

[project.scripts]
spam-cli = "spam:main_cli"

[project.gui-scripts]
spam-gui = "spam:main_gui"

[project.entry-points."spam.magical"]
tomatoes = "spam:main_tomatoes"
```






