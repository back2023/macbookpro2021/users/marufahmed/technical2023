
- Python packaging can be described in pyproject.toml alone per [PEP 621](https://peps.python.org/pep-0621/). 
- These packages are installable in live developer mode:
```sh
python -m pip install -e .
```

- Or via PyPI like any other Python package.

- Putting all package metadata into pyproject.toml instead of setup.py gives benefits
  
- This is an example of a minimal pyproject.toml that works all alone, 
	- no other metadata files required, except perhaps MANIFEST.in for advanced cases.
	- The `__version__` is contained in file `mypkg/__init__.py` as Python code:

```python
__version__ = "1.2.3"
```

pyproject.toml:

```toml
[build-system]
requires = ["setuptools>=61.0.0", "wheel"]
build-backend = "setuptools.build_meta"

[project]
name = "mypkg"
description = "My really awesome package."
keywords = ["random", "cool"]
classifiers = ["Development Status :: 5 - Production/Stable",
 "Environment :: Console",
 "Intended Audience :: Science/Research",
 "Operating System :: OS Independent",
 "Programming Language :: Python :: 3",
]
requires-python = ">=3.7"
dynamic = ["version", "readme"]

[tool.setuptools.dynamic]
readme = {file = ["README.md"], content-type = "text/markdown"}
version = {attr = "mypkg.__version__"}
```

- **PEP8 checking** via `flake8` is configured in `.flake8`:

```toml
[flake8]
max-line-length = 100
exclude = .git,__pycache__,doc/,docs/,build/,dist/,archive/
per-file-ignores =
  __init__.py:F401
```

- **MANIFEST.in** is used to [specify external files installed](https://packaging.python.org/guides/using-manifest-in/#manifest-in-commands).

- **Classifiers** are optional and help projects indexing in PyPI and search engines. 
	- Classifiers must be from the [official classifier trove](https://pypi.python.org/pypi?%3Aaction=list_classifiers) or they will fail when [uploading a package to PyPI](https://www.scivision.dev/easy-upload-to-pypi).

- Python can easily [import Fortran code using f2py](https://www.scivision.dev/tags/f2py). See this f2py example [setup.py](https://github.com/scivision/lowtran/).