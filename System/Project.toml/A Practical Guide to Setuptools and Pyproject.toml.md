
https://godatadriven.com/blog/a-practical-guide-to-setuptools-and-pyproject-toml/
Rogier van der Geer / 18 March, 2022


## Wrap up

- If you want to make use of Setuptools and `pyproject.toml` to build your package, this should provide you with a good starting point:
#### `pyproject.toml`

```toml
[build-system]
requires = ["setuptools"]
build-backend = "setuptools.build_meta"
```

#### `setup.cfg`

```toml
[metadata]
name = example
version = attr: example.__version__
author = You
author_email = your@email.address
url = https://godatadriven.com/blog/a-practical-guide-to-setuptools-and-pyproject-toml
description = Example package description
long_description = file: README.md
long_description_content_type = text/markdown
keywords = example, setuptools
license = BSD 3-Clause License
classifiers =
    License :: OSI Approved :: BSD License
    Programming Language :: Python :: 3

[options]
packages = find:
zip_safe = True
include_package_data = True
install_requires =
    pandas == 1.4.1
    PyYAML >= 6.0
    typer

[options.entry_points]
console_scripts = 
    my-example-utility = example.example_module:main

[options.extras_require]
notebook = jupyter>=1.0.0, matplotlib
dev = 
    black==22.1.0
    flake8==4.0.1

[options.package_data]
example = data/schema.json, *.txt
* = README.md
```

- Now build your project by running `pip -m build . --wheel`, or 
- do an editable install with `pip install -e .`




#### A `src/` layout

- Many people prefer placing their python packages in a `src/` folder in their project directory, which means having a project structure like this:
```
/path/to/example/project/
├── src/                            Source dir.
│   └── example/                    Python package directory.
│       ├── __init__.py             This makes the directory a package.
│       └── example_module.py       Example module.
├── pyproject.toml                  Definition of build process of the package.
├── README.md                       README with info of the project.
└── setup.cfg                       Configuration details of the python package.
```

- You can do so by adding the following options to your package configuration:
```toml
[options]
package_dir=
    =src

[options.packages.find]
where=src
```