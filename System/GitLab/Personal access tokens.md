
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

Personal access tokens can be an alternative to [OAuth2](https://docs.gitlab.com/ee/api/oauth2.html) and used to:
- Authenticate with the [GitLab API](https://docs.gitlab.com/ee/api/rest/index.html#personalprojectgroup-access-tokens).
- Authenticate with Git using HTTP Basic Authentication.

- The ability to create personal access tokens without expiry was [deprecated](https://gitlab.com/gitlab-org/gitlab/-/issues/369122) in GitLab 15.4 and [removed](https://gitlab.com/gitlab-org/gitlab/-/issues/392855) in GitLab 16.0.


## Create a personal access token
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

You can create as many personal access tokens as you like.
1. On the left sidebar, select your avatar.
2. Select **Edit profile**.
3. On the left sidebar, select **Access Tokens**.
4. Select **Add new token**.
5. Enter a name and expiry date for the token.
    - The token expires on that date at midnight UTC.
    - If you do not enter an expiry date, the expiry date is automatically set to 365 days later than the current date.
    - By default, this date can be a maximum of 365 days later than the current date.
6. Select the [desired scopes](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-token-scopes).
7. Select **Create personal access token**.

- Your new personal access token
	(**Make sure you save it - you won't be able to access it again.**)
	- Save it in lastpass

## Using a personal access token on the command line
https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#using-a-personal-access-token-on-the-command-line

- Once you have a personal access token, you can enter it instead of your password when performing Git operations over HTTPS.
- 
```shell
$ git clone https://HOSTNAME/USERNAME/REPO.git
Username: YOUR_USERNAME
Password: YOUR_PERSONAL_ACCESS_TOKEN
```

- Personal access tokens can only be used for HTTPS Git operations. If your repository uses an SSH remote URL, you will need to [switch the remote from SSH to HTTPS](https://docs.github.com/en/get-started/getting-started-with-git/managing-remote-repositories#switching-remote-urls-from-ssh-to-https).
- If you are not prompted for your username and password, your credentials may be cached on your computer. 
	- You can [update your credentials in the Keychain](https://docs.github.com/en/get-started/getting-started-with-git/updating-credentials-from-the-macos-keychain) to replace your old password with the token.
	  
- Instead of manually entering your personal access token for every HTTPS Git operation, you can cache your personal access token with a Git client. 
	- Git will temporarily store your credentials in memory until an expiry interval has passed. 
	- You can also store the token in a plain text file that Git can read before every request. 
	- For more information, see "[Caching your GitHub credentials in Git](https://docs.github.com/en/get-started/getting-started-with-git/caching-your-github-credentials-in-git)."