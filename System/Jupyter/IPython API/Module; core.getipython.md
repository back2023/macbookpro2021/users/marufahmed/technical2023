
https://ipython.readthedocs.io/en/stable/api/generated/IPython.core.getipython.html

Simple function to call to get the current InteractiveShell instance

## 1 Function[¶](https://ipython.readthedocs.io/en/stable/api/generated/IPython.core.getipython.html#function "Link to this heading")

IPython.core.getipython.get_ipython()[¶](https://ipython.readthedocs.io/en/stable/api/generated/IPython.core.getipython.html#IPython.core.getipython.get_ipython "Link to this definition")

Get the global InteractiveShell instance.

Returns None if no InteractiveShell instance is registered.

