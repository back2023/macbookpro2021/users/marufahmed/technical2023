

https://forums.fast.ai/t/ipygpulogger-gpu-logger-for-jupyter-ipython-memory-usage-and-exec-time/33964

# Moved

ipygpulogger got integrated into [ipyexperiments 5](https://github.com/stas00/ipyexperiments). Discussion forum is here: [IPyExperiments: Getting the most out of your GPU RAM in jupyter notebook](https://forums.fast.ai/t/ipyexperiments-getting-the-most-out-of-your-gpu-ram-in-jupyter-notebook/30145/1)