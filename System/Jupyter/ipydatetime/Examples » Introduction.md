
https://ipydatetime.readthedocs.io/en/latest/examples/introduction.html
# Introduction[¶](https://ipydatetime.readthedocs.io/en/latest/examples/introduction.html#Introduction "Permalink to this heading")

This package is meant to supply widgets for picking a time and a datetime, using the default HTML5 input controls. For a date-onpy input, use the one in ipywidgets core (`ipywidgets.DatePicker`).