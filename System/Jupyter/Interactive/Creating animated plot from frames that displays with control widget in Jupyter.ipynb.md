

https://gist.github.com/fomightez/d862333d8eefb94a74a79022840680b1


## Creating animated plot from frames that displays with control widget in Jupyter

This notebook shows animations made in the style covered [here](http://louistiao.me/posts/notebooks/embedding-matplotlib-animations-in-jupyter-as-interactive-javascript-widgets/). Learned of from [this Stack0verflow post](https://stackoverflow.com/q/70761535/8508004) seeking help.

**Important benefit of this approach**: These animations are interactive and playable in static renderings of notebooks such as those displayed via [nbviewer](https://nbviewer.jupyter.org/). (GitHub's viewer currently doesn't support this; you must use [nbviewer](https://nbviewer.jupyter.org/).) _No active kernel is needed once the notebook as been run once and saved._