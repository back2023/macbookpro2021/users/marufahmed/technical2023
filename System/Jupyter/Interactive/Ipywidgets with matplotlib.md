
https://kapernikov.com/ipywidgets-with-matplotlib/


Published on: March 31, 2020

This tutorial gives a brief introduction into using ipywidgets in Jupyter Notebooks. Ipywidgets provide a set of building blocks for graphical user interfaces that are powerful, yet easy to use.


A simple use case could be adding some basic controls to a plot for interactive data exploration. On the other side of the spectrum, we can combine widgets together to build full-fledged graphical user interfaces.

Here, we first introduce the `interact` function, which is a convenient way to quickly create suitable widgets to control functions. Second, we look into specific widgets and stack them together to build a basic gui application.


