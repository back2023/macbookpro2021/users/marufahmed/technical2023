

https://blog.jupyter.org/a-gallery-of-voil%C3%A0-examples-a2ce7ef99130


**Voilà** is one of the [latest addition to the Jupyter ecosystem](https://blog.jupyter.org/and-voil%C3%A0-f6a2c08a4a93) and can be used to turn notebooks into standalone applications and dashboards.

Today we are pleased to introduce the Voilà Gallery, a collection of examples built with Voilà.


