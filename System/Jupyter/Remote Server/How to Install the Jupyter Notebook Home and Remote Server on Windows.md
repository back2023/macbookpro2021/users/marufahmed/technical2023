
https://levelup.gitconnected.com/how-to-install-the-jupyter-notebook-home-and-remote-server-on-windows-9bfcf39ee2a7


## Summary:

This article installs the Jupyter Notebook home and public server. It installs the requirements, configures the server settings, security and protection, and firewall permissions. It also schedules the server to run at startup, sets up the port forwarding and static ip address, and accesses the server using the private and public ip address from the same wifi and a different wifi.





