

https://people.duke.edu/~ccc14/sta-663-bootstrap/IPythonNotebookIntroduction.html


The IPython notebook is an interactive, web-based environment that allows one to combine code, text and graphics into one unified document. All of the lectures in this course have been developed using this tool. In this lecture, we will introduce the notebook interface and demonstrate some of its features.


