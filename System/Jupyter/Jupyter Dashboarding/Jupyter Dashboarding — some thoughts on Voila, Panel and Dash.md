

https://medium.com/informatics-lab/jupyter-dashboarding-some-thoughts-on-voila-panel-and-dash-b84df9c9482f



# Jupyter Dashboarding Workshop

Back in June, I attended the four-day Jupyter Dashboarding Workshop in Paris. A big thanks to [Sylvain](https://github.com/SylvainCorlay), [Pascal](https://github.com/pbugnion) and the rest of the team, it was a great event. Also a big thank you to all who lent a hand to me over the week but especially [Philipp](https://github.com/philippjfr) and [Yuvi](https://github.com/yuvipanda).

There were lots of great conversations, ideas and demos, and I’ve got loads of ideas buzzing around my head as a result. I hope to write many of them up in due course, but I’m going to start with a discussion of the three key libraries in the Dashboarding space.

**Caveat:** I know relatively little about the three technologies I’m going to talk about so please take with a pinch of salt, and your thought and corrections are welcome in the comments!


