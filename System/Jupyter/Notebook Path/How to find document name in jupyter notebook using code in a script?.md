

https://stackoverflow.com/questions/58723723/how-to-find-document-name-in-jupyter-notebook-using-code-in-a-script



You can do this using [https://github.com/AaronWatters/jp_proxy_widget](https://github.com/AaronWatters/jp_proxy_widget)

Here is the code

```python
import jp_proxy_widget

nb_name = None

def save_name(name):
    global nb_name
    nb_name = name

get_name = jp_proxy_widget.JSProxyWidget()
get_name.js_init("""
var name = IPython.notebook.notebook_name;
element.html("The name of the notebook is" + name);
save_name(name);
""", save_name=save_name)
get_name
```