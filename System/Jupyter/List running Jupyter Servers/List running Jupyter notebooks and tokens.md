
https://stackoverflow.com/questions/46551551/list-running-jupyter-notebooks-and-tokens

You can now just run `jupyter notebook list` in the terminal to get the running jupyter sessions with tokens.
(Does Not work with Jupyter Lab)
```python
> ipython
[1] : system("jupyter" "notebook" "list")
Out[1]: 
['Currently running servers:','http://localhost:8895/token=067470c5ddsadc54153ghfjd817d15b5d5f5341e56b0dsad78a :: /u/user/dir']
```
Source: https://stackoverflow.com/a/46551552


```python
$ jupyter server list
```
Source: https://stackoverflow.com/a/67030093



