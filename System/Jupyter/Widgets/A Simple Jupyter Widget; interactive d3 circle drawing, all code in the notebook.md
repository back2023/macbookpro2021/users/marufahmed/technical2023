

https://github.com/paul-shannon/jupyter-widget-demo-all-in-notebook


Wishing to become proficient at creating interactive widgets for Jupyter notebooks and labs, I started out by studying the documentation offered by the jupyter/ipywidgets project:

- [https://github.com/ipython/ipywidgets](https://github.com/ipython/ipywidgets) (the project's README is helpful)
- [http://ipywidgets.readthedocs.io/en/latest/examples/Widget%20Custom.html](http://ipywidgets.readthedocs.io/en/latest/examples/Widget%20Custom.html) (provides well-documented incremental examples)

Next, as suggested by those docs, I looked at three more advanced examples. The ipywidgets README says:

---

Examples of custom widget libraries built upon ipywidgets are

- [bqplot](https://github.com/bloomberg/bqplot) a 2d data visualization library enabling custom user interactions.
- [pythreejs](https://github.com/jovyan/pythreejs) a Jupyter - Three.js wrapper, bringing Three.js to the notebook.
- [ipyleaflet](https://github.com/ellisonbg/ipyleaflet) a leaflet widget for Jupyter.
