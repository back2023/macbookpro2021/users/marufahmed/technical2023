

https://stackoverflow.com/questions/20186344/importing-an-ipynb-file-from-another-ipynb-file/43615015#43615015


It is really simple in newer Jupyter:

```python
%run MyOtherNotebook.ipynb
```

[See here for details](https://docs.qubole.com/en/latest/user-guide/notebooks-and-dashboards/notebooks/jupyter-notebooks/running-jupy-notebooks.html#running-a-jupyter-notebook-from-another-jupyter-notebook).

Official docs: [`%run` IPython magic command](https://ipython.readthedocs.io/en/stable/interactive/magics.html#magic-run)




