

https://stackoverflow.com/questions/32538758/nameerror-name-get-ipython-is-not-defined



`get_ipython` is available only if the IPython module was imported that happens implicitly if you run ipython shell (or Jupyter notebook).

If not, the import will fail, but you can still import it explicitly with:

```python
from IPython import get_ipython
```

