
https://matplotlib.org/ipympl/index.html

`ipympl` enables using the interactive features of matplotlib in Jupyter Notebooks, Jupyter Lab, Google Colab, VSCode notebooks.

Matplotlib requires a live Python kernel to have interactive plots so by default the outputs on this page will not be interactive. To try things out yourself you can either use [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/matplotlib/ipympl/stable?urlpath=retro/notebooks/examples/ipympl.ipynb) or make these docs interactive by clicking on the rocket icon in the top right of the page.


