
# ipywidgets

August 08, 2023

[ipywidgets](https://ipywidgets.readthedocs.io/en/7.7.0/) are visual elements that allow users to specify parameter values in notebook cells. You can use ipywidgets to make your Databricks Python notebooks interactive.

The ipywidgets package includes over [30 different controls](https://ipywidgets.readthedocs.io/en/7.7.0/examples/Widget%20List.html), including form controls such as sliders, text boxes, and checkboxes, as well as layout controls such as tabs, accordions, and grids. Using these elements, you can build graphical user interfaces to interface with your notebook code.

https://docs.databricks.com/en/notebooks/ipywidgets.html