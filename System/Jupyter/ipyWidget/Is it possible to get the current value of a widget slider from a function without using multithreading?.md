
Inside a modern ipython/ipykernel, you’ll pretty much always be inside an asyncio-compatible event loop. If you block, though, then things stop flowing.

`Output` is kind of an implementation nightmare, with each frontend being slightly different. However, `display(..., display_id=True).update` works great.

https://discourse.jupyter.org/t/is-it-possible-to-get-the-current-value-of-a-widget-slider-from-a-function-without-using-multithreading/15524