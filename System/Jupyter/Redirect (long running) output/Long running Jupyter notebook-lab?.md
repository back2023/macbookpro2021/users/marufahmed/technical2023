https://stackoverflow.com/questions/57664547/long-running-jupyter-notebook-lab



**EDIT** (after clarification):

You can use some Jupyter magic to continue running the cell after you close the browser or laptop, and then print the output after you return. Here's how that's done:

```python
%%capture stored_output

import time
time.sleep(30)
print("Hi")
```

After returning, run the following:

```python
stored_output.show()
# Hi
```


