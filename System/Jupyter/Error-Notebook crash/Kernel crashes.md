

https://github.com/microsoft/vscode-jupyter/wiki/Kernel-crashes

Jupyter Kernels can crash for a number of reasons (incorrectly installed or incompatible packages, unsupported OS or version of Python, etc) and at different points of execution phases in a notebook. The two distinct phases are [Starting a Kernel](https://github.com/microsoft/vscode-jupyter/wiki/Kernel-crashes/_edit#cannot-run-a-single-cell-kernel-fails-to-start) for the first time and [Running a cell](https://github.com/microsoft/vscode-jupyter/wiki/Kernel-crashes/_edit#some-cells-run-successfully-however-kernel-crashes-after-running-another-cell) after a kernel has been started. This page categorizes failures in to the above two categories to aid users in identifying causes for kernel crashes with the intention of being able to address those issues.

