

https://stackoverflow.com/questions/56206086/why-does-jupyter-notebook-server-keeps-crashing-when-ipynb-files-are-opened




Please **uninstall and re-install** all the below items:

- ipykernel
- ipython
- jupyter_client
- jupyter_core
- traitlets
- ipython_genutils

Additionally, if you're going to install with conda, follow below command.

`run conda clean -tipsy`

This command will clean up conda caches before you start.

Reference:

1. [https://github.com/jupyter/notebook/issues/1892](https://github.com/jupyter/notebook/issues/1892)

