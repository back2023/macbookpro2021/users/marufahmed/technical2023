https://stackoverflow.com/questions/18019477/how-can-i-play-a-local-video-in-my-ipython-notebook


(**updated 2019, removed unnecessarily costly method**)

Just do:

```python
from IPython.display import Video

Video("test.mp4")
```

If you get an error `No video with supported format or MIME type found`, just pass `embed=True` to the function: `Video("test.mp4", embed=True)`. In previous IPython versions, you may need to call the `from_file` method: `Video.from_file("test.mp4")`.

Or if you want to use the `HTML` element:

```python
from IPython.display import HTML

HTML("""
    <video alt="test" controls>
        <source src="test.mp4" type="video/mp4">
    </video>
""")
```