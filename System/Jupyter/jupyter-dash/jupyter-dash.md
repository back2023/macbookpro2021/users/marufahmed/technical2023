

https://github.com/plotly/jupyter-dash


## NOTICE: as of Dash v2.11, Jupyter support is built into the main Dash package.

[](https://github.com/plotly/jupyter-dash#notice-as-of-dash-v211-jupyter-support-is-built-into-the-main-dash-package)

The `jupyter-dash` package is no longer necessary, all of its functionality has been merged into `dash`. See [https://dash.plotly.com/dash-in-jupyter](https://dash.plotly.com/dash-in-jupyter) for usage details, and if you have any questions please join the discussion at [https://community.plotly.com/](https://community.plotly.com/)

The old readme is below for those still using the package, but `jupyter-dash` will not receive any further updates.


