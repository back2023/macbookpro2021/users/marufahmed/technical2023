
https://github.com/nteract/scrapbook


The **scrapbook** library records a notebook’s data values and generated visual content as "scraps". Recorded scraps can be read at a future time.

[See the scrapbook documentation](https://nteract-scrapbook.readthedocs.io/) for more information on how to use scrapbook.


