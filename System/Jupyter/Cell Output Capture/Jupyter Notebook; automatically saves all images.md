

https://stackoverflow.com/questions/38166236/jupyter-notebook-automatically-saves-all-images



You can use the [convert](http://nbconvert.readthedocs.io/en/latest/usage.html) option of Jupyter. The syntax is

```css
jupyter nbconvert --to FORMAT notebook.ipynb
```

The default is html, so it will convert your images to PNG files. You can also use a LaTeX conversion

```css
jupyter nbconvert --to latex notebook.ipynb
```

Alternatively, you can use the menu `File>>Download as`, and use Markdown, reSTructured Text or LaTeX there.

Regarding the format for publication, it depends on the journal you're aiming. My suggestion would be to export your images as SVG when you want to edit later. Inkscape can be used for this purpose), and they can be embedded in LibreOffice. Use PDF when you want them ready to go. Vector graphics are generally better for the type of graphics in your notebook. If you're planning to use MS Word, then you might want to convert the graphics from SVG to EMF as an intermediate step.