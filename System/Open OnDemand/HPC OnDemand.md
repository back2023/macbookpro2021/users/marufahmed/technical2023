
Open OnDemand (OOD or OoD) is a service providing access to HPC resources through a web browser. Manage your files, check the cluster job status, submit and monitor your own jobs, obtain a shell session, or choose from a set of "interactive" applications -- each provided through a browser tab.

https://sites.google.com/a/case.edu/hpcc/hpc-cluster/hpc-visual-access/hpc-ondemand