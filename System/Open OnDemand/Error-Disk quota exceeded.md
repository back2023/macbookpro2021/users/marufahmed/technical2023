
```bash
#### Failed to stage the template with the following error:

Disk quota exceeded @ dir_s_mkdir - /home/900/mah900/ondemand/data/sys/dashboard/batch_connect/sys/jupyter/ncigadi/output/d9ccb050-3538-430a-b05f-9e1e340048ce

-   The JupyterLab session data for this session can be accessed under the [staged root directory](https://are.nci.org.au/pun/sys/dashboard/files/fs/home/900/mah900/ondemand/data/sys/dashboard/batch_connect/sys/jupyter/ncigadi/output/d9ccb050-3538-430a-b05f-9e1e340048ce).
```

### Try 
checked  `~/.cache` folder

```
cd ~/.cache
# Sorted
du -hs * | sort -hr
# or, 
du -h --max-depth=1
```
https://superuser.com/questions/554319/display-each-sub-directory-size-in-a-list-format-using-one-line-command-in-bash

Removed pip http cache as there was no documenation about it.  (2023.02.12)
```
rm -r ~/.cache/pip/http/*
```
Open on demand is working now 

