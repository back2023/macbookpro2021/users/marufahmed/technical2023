
https://stackoverflow.com/questions/26487658/does-qsub-pass-command-line-arguments-to-my-script


On my platform the `-F` is not available. As a substitute `-v` helped:

```
qsub -v "var=value" script.csh
```

And then use the variable `var` in your script. See also the [documentation](http://docs.adaptivecomputing.com/suite/8-0/basic/help.htm#topics/torque/commands/qsub.htm#-v).