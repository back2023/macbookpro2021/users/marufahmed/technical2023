
Interactive jobs are used to manually run processes at the command line on the compute nodes. There are two ways to accomplish this:

- Request that the scheduler create a job whose resources matches the configuration for an existing PBS file.
- Request a number of resources explicitly.

In both cases the scheduler will create a job that, when started, will provide a shell prompt on the compute node associated with what would be MPI rank 0 (aka the head node).

To request an interactive session that matches the configuration of an existing PBS file, run the qsub command with the -I parameter.




https://www.niu.edu/crcd/current-users/getting-started/run-interactive-jobs.shtml