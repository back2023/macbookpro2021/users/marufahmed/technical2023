
https://hpc.research.uts.edu.au/pbs/nodes/

You can get an up-to-date summary of the nodes, queues and jobs by visiting the [HPC Status](https://hpc.research.uts.edu.au/status/) page. You may wish to obtain more detailed information though for use in your job scripts. You can do this by using the `pbsnodes` command.






