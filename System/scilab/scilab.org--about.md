
https://www.scilab.org/about
## What is Scilab ?

Scilab is free and open source software for numerical computation providing a powerful computing environment for engineering and scientific applications.

Scilab is released as open source under the [GPL License](https://www.gnu.org/licenses/gpl-3.0.en.html), and is [available for download](https://www.scilab.org/download) free of charge. Scilab is available under GNU/Linux, Mac OS X and Windows XP/Vista/7/8/10 (see [system requirements](https://www.scilab.org/download/system-requirements)).

## What does Scilab do ?

Scilab includes hundreds of mathematical functions. It has a high level programming language allowing access to advanced data structures, 2-D and 3-D graphical functions.

A large number of functionalities is included in Scilab:

- **Maths & Simulation**  
    For usual engineering and science applications including mathematical operations and data analysis.
- **2-D & 3-D Visualization**  
    Graphics functions to visualize, annotate and export data and many ways to create and customize various types of plots and charts.
- **Optimization**  
    Algorithms to solve constrained and unconstrained continuous and discrete optimization problems.
- **Statistics**  
    Tools to perform data analysis and modeling
- **Control Systems**  
    Standard algorithms and tools for control system study
- **Signal Processing**  
    Visualize, analyze and filter signals in time and frequency domains.
- **Application Development**  
    Increase Scilab native functionalities and manage data exchanges with external tools.
- **Xcos - Dynamic systems modeling**  
    Modeling mechanical systems, hydraulic circuits, control systems...