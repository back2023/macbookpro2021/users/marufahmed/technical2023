
https://github.com/2y7c3/Super-Resolution-Neural-Operator


This repository contains the official implementation for SRNO introduced in the following paper:

[**Super-Resolution Neural Operator**](https://arxiv.org/abs/2303.02584) (CVPR 2023)

Our code is based on Ubuntu 18.04, pytorch 1.10.2, CUDA 11.3 and python 3.9.


