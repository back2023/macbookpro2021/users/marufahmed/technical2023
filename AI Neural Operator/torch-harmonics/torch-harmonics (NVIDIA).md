https://github.com/NVIDIA/torch-harmonics
# torch-harmonics

[**Overview**](https://github.com/NVIDIA/torch-harmonics/tree/08108157eaee2182383f1e626a04584d732239e3#overview) | [**Installation**](https://github.com/NVIDIA/torch-harmonics/tree/08108157eaee2182383f1e626a04584d732239e3#installation) | [**More information**](https://github.com/NVIDIA/torch-harmonics/tree/08108157eaee2182383f1e626a04584d732239e3#more-about-torch-harmonics) | [**Getting started**](https://github.com/NVIDIA/torch-harmonics/tree/08108157eaee2182383f1e626a04584d732239e3#getting-started) | [**Contributors**](https://github.com/NVIDIA/torch-harmonics/tree/08108157eaee2182383f1e626a04584d732239e3#contributors) | [**Cite us**](https://github.com/NVIDIA/torch-harmonics/tree/08108157eaee2182383f1e626a04584d732239e3#cite-us) | [**References**](https://github.com/NVIDIA/torch-harmonics/tree/08108157eaee2182383f1e626a04584d732239e3#references)

[![tests](https://github.com/NVIDIA/torch-harmonics/actions/workflows/tests.yml/badge.svg)](https://github.com/NVIDIA/torch-harmonics/actions/workflows/tests.yml) [![pypi](https://camo.githubusercontent.com/a22d4703866ce82591959aaf291981d582ffde2948118c5d848c1b9c87729d98/68747470733a2f2f696d672e736869656c64732e696f2f707970692f762f746f7263685f6861726d6f6e696373)](https://pypi.org/project/torch_harmonics/)

## [](https://github.com/NVIDIA/torch-harmonics/tree/08108157eaee2182383f1e626a04584d732239e3#overview)Overview

torch-harmonics is a differentiable implementation of the Spherical Harmonic transform in PyTorch. It was originally implemented to enable Spherical Fourier Neural Operators (SFNO). It uses quadrature rules to compute the projection onto the associated Legendre polynomials and FFTs for the projection onto the harmonic basis. This algorithm tends to outperform others with better asymptotic scaling for most practical purposes.

torch-harmonics uses PyTorch primitives to implement these operations, making it fully differentiable. Moreover, the quadrature can be distributed onto multiple ranks making it spatially distributed.

torch-harmonics has been used to implement a variety of differentiable PDE solvers which generated the animations below. Moreover, it has enabled the development of Spherical Fourier Neural Operators (SFNOs) [1].