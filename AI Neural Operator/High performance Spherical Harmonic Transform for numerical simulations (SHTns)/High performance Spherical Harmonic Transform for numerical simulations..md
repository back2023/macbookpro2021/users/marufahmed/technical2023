
SHTns is a high performance library for Spherical Harmonic Transform written in C, aimed at numerical simulation (fluid flows, mhd, ...) in spherical geometries.  
Main features :

- **[blazingly fast](https://www2.atmos.umd.edu/~dkleist/docs/shtns/doc/html/bench.html)**
- distributed under the open source [CeCILL License](https://www2.atmos.umd.edu/~dkleist/docs/shtns/doc/html/license.html) (GPL compatible)
- both **scalar and [vector transforms](https://www2.atmos.umd.edu/~dkleist/docs/shtns/doc/html/vsh.html)**
- backward and forward (synthesis and analysis) functions
- flexible truncation
- spatial data can be stored in latitude-major or longitude-major order arrays.
- various conventions (normalization and Condon-Shortley phase)
- can be used from **Fortran, c/c++, and Python** programs
- a highly efficient Gauss algorithm working with **Gauss nodes** (based on Gauss-Legendre quadrature)
- an algorithm using DCT for **regular nodes** (based on a generalized Fejer quadrature)
- support for SSE2, SSE3 and **AVX** vectorization with gcc.
- **parallel transforms with OpenMP** (for Gauss grid only).
- synthesis (inverse transform) at any coordinate (not constrained to a grid) useful for rendering purposes.
- ability to choose the optimal spatial sizes for a given spherical harmonic truncation.
- **on-the-fly transforms** : saving memory and bandwidth, and can even be faster depending on architecture.
- accurate up to spherical harmonic degree l=16383 (at least).
- [rotation functions](https://www2.atmos.umd.edu/~dkleist/docs/shtns/doc/html/group__rotation.html) to rotate spherical harmonics (beta).
- [special spectral operator](https://www2.atmos.umd.edu/~dkleist/docs/shtns/doc/html/group__operators.html) functions that do not require a transform (multiply by cos(theta)...) .
- scalar transforms for complex spatial fields.
