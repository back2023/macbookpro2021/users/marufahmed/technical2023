
May 15, 2020 — The NVIDIA HPC SDK is a comprehensive suite of compilers and libraries enabling HPC developers to program the entire HPC platform from the GPU foundation to the CPU and through the interconnect. It is the only comprehensive, integrated SDK for programming accelerated computing systems.

https://www.hpcwire.com/off-the-wire/announcing-the-nvidia-hpc-sdk/

