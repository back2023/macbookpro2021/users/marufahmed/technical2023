

https://github.com/NVIDIA/trt-samples-for-hackathon-cn


This repository is aimed at NVIDIA TensorRT beginners and developers. We provide TensorRT-related learning and reference materials, code examples, and summaries of the annual TensorRT Hackathon competition information.


