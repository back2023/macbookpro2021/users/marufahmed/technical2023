


_The 3D simulation and collaboration platform now supports the NVIDIA A100 and H100 systems; Ecosystem Expands at HPC to Accelerate Million-X Scale Discovery from NVIDIA Modulus, NeuralVDB, and Index and Kitware’s Paraview_


https://biz.crast.net/nvidia-omniverse-opens-portals-for-scientists-to-explore-our-universe/


https://nvidianews.nvidia.com/news/nvidia-omniverse-scientific-computing



