

## [Abstract](https://docs.nvidia.com/deeplearning/performance/dl-performance-matrix-multiplication/index.html#abstract)

This guide describes matrix multiplications and their use in many deep learning operations. The trends described here form the basis of performance trends in fully-connected, convolutional, and recurrent layers, among others.






