
Bringing GPU-Accelerated Supercomputing to the NumPy Ecosystem


**NVIDIA cuNumeric** aspires to be a drop-in replacement library for NumPy, bringing distributed and accelerated computing on the NVIDIA platform to the Python community. Download the latest release of cuNumeric today.



## Legate

Legate is an abstraction layer which runs on top of a runtime system, together providing scalable implementations of popular domain-specific APIs. It provides an API similar to Apache Arrow but provides stronger guarantees about data coherence and synchronization to aid library developers. NVIDIA cuNumeric layers on top of Legate like other libraries.

  

Legate democratizes computing by making it possible for all programmers to leverage the power of large clusters of CPUs and GPUs by running the same code that runs on a desktop or a laptop at scale. Using this technology, computational and data scientists can develop and test programs on moderately sized datasets on local machines and then immediately scale up to larger datasets deployed on many nodes in the cloud or on a supercomputer without any code modifications.

  
[**Getting Started on Github**](https://nv-legate.github.io/legate.core/README.html)


https://developer.nvidia.com/cunumeric