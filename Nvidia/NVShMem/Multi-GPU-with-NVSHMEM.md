
# Multi GPU with NVSHMEM - lattice/quda GitHub Wiki

# Multi-GPU support with NVSHMEM

One systems with Infiniband Networks and CUDA 11 (or later) QUDA can use NVSHMEM for communication to reduce communication overheads and improve scaling.

_Note that this works on top of QMP/MPI and does not replace these._

_No changes are required in application that use QUDA_

https://github-wiki-see.page/m/lattice/quda/wiki/Multi-GPU-with-NVSHMEM

https://github.com/lattice/quda/wiki/Multi-GPU-with-NVSHMEM



