

[NVSHMEM](https://docs.nvidia.com/hpc-sdk/nvshmem/index.html) implements the [OpenSHMEM](http://openshmem.org/) parallel programming model for clusters of NVIDIA ® GPUs. The NVSHMEM Partitioned Global Address Space (PGAS) spans the memory across GPUs and includes an API for fine-grained GPU-GPU data movement from within a CUDA kernel, on CUDA streams, and from the CPU.

https://docs.nvidia.com/nvshmem/archives/nvshmem-113/api/docs/index.html