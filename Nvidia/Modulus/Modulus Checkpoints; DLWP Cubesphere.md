
https://catalog.ngc.nvidia.com/orgs/nvidia/teams/modulus/models/modulus_dlwp_cubesphere


#### Description

DLWP Cubesphere model checkpoint package. DLWP is a light-weight deep learning model for weather prediction that uses Cube-sphere grid.

## Details

This NGC asset is a DLWP Cubesphere model checkpoint package. Model checkpoint package refers to the set of artifacts needed to run inference using pre-trained model which includes the model checkpoint, set of sample inputs, inference script.

## Architecture

DLWP is a light-weight deep learning model for weather prediction that uses Cube-sphere grid. Re-implemented from: [https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2021MS002502](https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2021MS002502).

The DLWP model can be used to predict the state of the atmosphere given a previous atmospheric state. You can infer a 320-member ensemble set of six-week forecasts at 1.4° resolution within a couple of minutes, demonstrating the potential of AI in developing near real-time digital twins for weather prediction.

Please refer [Sub-Seasonal Forecasting With a Large Ensemble of Deep-Learning Weather Prediction Models](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2021MS002502) paper for additional details.

## Training

The model is trained on 7-channel subset of ERA5 Data that is mapped onto a cubed sphere grid with a resolution of 64x64 grid cells. The model uses years 1980-2015 for training, 2016-2017 for validation and 2018 for out of sample testing. The training scripts for the problem can be found at: [https://github.com/NVIDIA/modulus-launch/tree/main/examples/weather/dlwp](https://github.com/NVIDIA/modulus-launch/tree/main/examples/weather/dlwp)



