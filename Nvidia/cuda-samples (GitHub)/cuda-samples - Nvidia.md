

## Samples list

### [](https://github.com/NVIDIA/cuda-samples/tree/master#0-introduction)[0. Introduction](https://github.com/NVIDIA/cuda-samples/blob/master/Samples/0_Introduction/README.md)

Basic CUDA samples for beginners that illustrate key concepts with using CUDA and CUDA runtime APIs.

### [](https://github.com/NVIDIA/cuda-samples/tree/master#1-utilities)[1. Utilities](https://github.com/NVIDIA/cuda-samples/blob/master/Samples/1_Utilities/README.md)

Utility samples that demonstrate how to query device capabilities and measure GPU/CPU bandwidth.

### [](https://github.com/NVIDIA/cuda-samples/tree/master#2-concepts-and-techniques)[2. Concepts and Techniques](https://github.com/NVIDIA/cuda-samples/blob/master/Samples/2_Concepts_and_Techniques/README.md)

Samples that demonstrate CUDA related concepts and common problem solving techniques.

### [](https://github.com/NVIDIA/cuda-samples/tree/master#3-cuda-features)[3. CUDA Features](https://github.com/NVIDIA/cuda-samples/blob/master/Samples/3_CUDA_Features/README.md)

Samples that demonstrate CUDA Features (Cooperative Groups, CUDA Dynamic Parallelism, CUDA Graphs etc).

### [](https://github.com/NVIDIA/cuda-samples/tree/master#4-cuda-libraries)[4. CUDA Libraries](https://github.com/NVIDIA/cuda-samples/blob/master/Samples/4_CUDA_Libraries/README.md)

Samples that demonstrate how to use CUDA platform libraries (NPP, NVJPEG, NVGRAPH cuBLAS, cuFFT, cuSPARSE, cuSOLVER and cuRAND).

### [](https://github.com/NVIDIA/cuda-samples/tree/master#5-domain-specific)[5. Domain Specific](https://github.com/NVIDIA/cuda-samples/blob/master/Samples/5_Domain_Specific/README.md)

Samples that are specific to domain (Graphics, Finance, Image Processing).

### [](https://github.com/NVIDIA/cuda-samples/tree/master#6-performance)[6. Performance](https://github.com/NVIDIA/cuda-samples/blob/master/Samples/6_Performance/README.md)

Samples that demonstrate performance optimization.

### [](https://github.com/NVIDIA/cuda-samples/tree/master#7-libnvvm)[7. libNVVM](https://github.com/NVIDIA/cuda-samples/blob/master/Samples/7_libNVVM/README.md)

Samples that demonstrate the use of libNVVVM and NVVM IR.

## [](https://github.com/NVIDIA/cuda-samples/tree/master#dependencies)