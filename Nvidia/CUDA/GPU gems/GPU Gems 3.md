https://htmlpreview.github.io/?https://raw.githubusercontent.com/QianMo/GPU-Gems-Book-Source-Code/master/GPU-Gems-3-CD-Content/index.html

**Physics Simulation**

- [Ch. 29: Real-Time Rigid Body Simulation on GPUs](https://raw.githubusercontent.com/QianMo/GPU-Gems-Book-Source-Code/master/GPU-Gems-3-CD-Content/content/29)  
- [Ch. 31: Fast N-Body Simulation with CUDA](https://raw.githubusercontent.com/QianMo/GPU-Gems-Book-Source-Code/master/GPU-Gems-3-CD-Content/content/31)  
- [Ch. 33: Broad-Phase Collision Detection with CUDA](https://raw.githubusercontent.com/QianMo/GPU-Gems-Book-Source-Code/master/GPU-Gems-3-CD-Content/content/33)

**GPU Computing**

- [Ch. 36: AES Encryption and Decryption on the GPU](https://raw.githubusercontent.com/QianMo/GPU-Gems-Book-Source-Code/master/GPU-Gems-3-CD-Content/content/36)  
- [Ch. 37: Efficient Random Number Generation and Application Using CUDA](https://raw.githubusercontent.com/QianMo/GPU-Gems-Book-Source-Code/master/GPU-Gems-3-CD-Content/content/37)