
https://github.com/h2oai/h2o-tutorials/tree/master/cuda-workshop



https://github.com/h2oai/h2o-tutorials/blob/master/cuda-workshop/notebook/CUDA_101.ipynb
# Hands-On CUDA C/C++ 101

This is an introductory, hands-on, workshop for programming GPU devices using [CUDA](https://developer.nvidia.com/cuda-zone) C/C++. Even though we will be using a Jupyter Notebook to edit and run our code be adviced that none of the commands are actually executed here but rather on the server (be it one of the cloud instances or your local machine).

This hands-on was highly influenced by NVidia's workshops and tutorial including (but not only):

- [NVidia's qwiklab catalog](https://nvidia.qwiklab.com/catalog/)
- [CUDA by example](https://developer.nvidia.com/cuda-example)
- [CUDA C programming guide](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html)

Be sure to check them out for more in-depth information.


**[h2o-tutorials](https://github.com/h2oai/h2o-tutorials)**

https://github.com/h2oai/h2o-tutorials
# H2O Tutorials

This document contains tutorials and training materials for H2O-3. If you find any problems with the tutorial code, please open an issue in this repository.

For general H2O questions, please post those to [Stack Overflow using the "h2o" tag](http://stackoverflow.com/questions/tagged/h2o) or join the [H2O Stream Google Group](https://groups.google.com/forum/#!forum/h2ostream) for questions that don't fit into the Stack Overflow format.


https://github.com/h2oai/h2o-tutorials/blob/master/SUMMARY.md

# Summary

- [Introduction](https://github.com/h2oai/h2o-tutorials/blob/master/README.md)
- [What is H2O?](https://github.com/h2oai/h2o-tutorials/blob/master/WhatIsH2O.md)
- [Intro to Data Science](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/intro-to-datascience/README.md)
- [Building a Smarter Application](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/building-a-smarter-application/README.md)
- [Deep Learning](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/deeplearning/README.md)
- [GBM & Random Forest](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/gbm-randomforest/README.md)
- [GLM](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/glm/glm.md)
- [GLRM](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/glrm/glrm-tutorial.md)
- [AutoML](https://github.com/h2oai/h2o-tutorials/blob/master/h2o-world-2017/automl/README.md)
- [NLP with H2O](https://github.com/h2oai/h2o-tutorials/blob/master/h2o-world-2017/nlp/README.md)
- [Hive UDF POJO Example](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/hive_udf_template/hive_udf_pojo_template/README.md)
- [Hive UDF MOJO Example](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/hive_udf_template/hive_udf_mojo_template/README.md)
- [Ensembles: Stacking, Super Learner](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/ensembles-stacking/README.md)
- [Streaming](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/streaming/storm/README.md)
- [Sparkling Water](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/sparkling-water/README.md)
- [PySparkling](https://github.com/h2oai/h2o-tutorials/blob/master/tutorials/pysparkling/Chicago_Crime_Demo.md)
- [Resources](https://github.com/h2oai/h2o-tutorials/blob/master/resources.md)
