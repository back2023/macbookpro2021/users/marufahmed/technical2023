
I've been playing around with [Tensorflow.js](https://js.tensorflow.org/) for my PhD (see my [PhD Update blog post series](https://starbeamrainbowlabs.com/blog/article.php?article=posts/438-phd-update-6-the-road-ahead.html)), and I had some ideas that I wanted to test out on my own that aren't really related to my PhD. In particular, I've found [this blog post](https://karpathy.github.io/2015/05/21/rnn-effectiveness/) to be rather inspiring - where the author sets up a character-based recurrent neural network to generate text.

The idea of transcoding all those characters to numerical values and back seems like too much work and too complicated just for a quick personal project though, so my plan is to try and develop a byte-based network instead, in the hopes that I can not only teach it to generate text as in the blog post, but valid Unicode as well.

Obviously, I can't really use the University's resources ethically for this (as it's got nothing to do with my University work) - so since I got a new laptop recently with an Nvidia GeForce RTX 2060, I thought I'd try and use it for some machine learning instead.

The problem here is that Tensorflow.js requires _only_ CUDA 10.0, but since I'm running Ubuntu 20.10 with all the latest patches installed, I have CUDA 11.1. A quick search of the apt repositories on my system reveals nothing that suggests I can install older versions of CUDA alongside the newer one, so I had to devise another plan.

I discovered some months ago (while working with [Viper](http://hpc.wordpress.hull.ac.uk/home/) - my University's HPC - for my PhD) that you can actually extract - without `sudo` privileges - the contents of the CUDA `.run` installers. By then fiddling with your `PATH` and `LD_LIBRARY_PATH` environment variables, you can get any program you run to look for the CUDA libraries elsewhere instead of loading the default system libraries.

https://starbeamrainbowlabs.com/blog/article.php?article=posts%2F439-nvidia-multiple-cuda.html


