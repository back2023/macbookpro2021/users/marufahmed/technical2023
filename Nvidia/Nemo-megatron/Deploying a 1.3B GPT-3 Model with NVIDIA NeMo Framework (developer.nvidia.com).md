

https://developer.nvidia.com/blog/deploying-a-1-3b-gpt-3-model-with-nvidia-nemo-megatron/


[Large language models (](https://www.nvidia.com/en-us/deep-learning-ai/solutions/large-language-models/)[LLMs](https://www.nvidia.com/en-us/deep-learning-ai/solutions/large-language-models/)[)](https://www.nvidia.com/en-us/deep-learning-ai/solutions/large-language-models/) are some of the most advanced deep learning algorithms that are capable of understanding written language. Many modern LLMs are built using the transformer network introduced by Google in 2017 in the [Attention Is All You Need](https://arxiv.org/abs/1706.03762) research paper.

[NVIDIA NeMo framework](https://developer.nvidia.com/nemo/megatron?nvid=nv-int-tblg-133070#cid=dl28_nv-int-tblg_en-us) is an end-to-end GPU-accelerated framework for training and deploying transformer-based LLMs up to a trillion parameters. In September 2022, NVIDIA announced that [NeMo framework is now available in Open Beta](https://www.nvidia.com/en-us/on-demand/session/gtcfall22-a41200/?nvid=nv-int-tblg-881125), allowing you to train and deploy LLMs using your own data. With this announcement, several pretrained checkpoints have been uploaded to [HuggingFace](https://huggingface.co/), enabling anyone to deploy LLMs locally using GPUs.

This post walks you through the process of downloading, optimizing, and deploying a 1.3 billion parameter GPT-3 model using the NeMo framework. It includes [NVIDIA Triton Inference Server](https://developer.nvidia.com/nvidia-triton-inference-server?nvid=nv-int-tblg-596142#cid=dl05_nv-int-tblg_en-us), a powerful open-source, inference-serving software that can deploy a wide variety of models and serve inference requests on both CPUs and GPUs in a scalable manner.


