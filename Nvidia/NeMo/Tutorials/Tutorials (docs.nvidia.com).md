
https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/stable/starthere/tutorials.html



The best way to get started with NeMo is to start with one of our tutorials.

Most NeMo tutorials can be run on [Google’s Colab](https://colab.research.google.com/notebooks/intro.ipynb).

To run a tutorial:

1. Click the **Colab** link (see table below).
    
2. Connect to an instance with a GPU. For example, click **Runtime** > **Change runtime type** and select **GPU** for the hardware accelerator.





