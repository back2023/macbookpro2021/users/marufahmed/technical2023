

https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/v1.0.0/starthere/intro.html

[NVIDIA NeMo](https://github.com/NVIDIA/NeMo) is a toolkit for building new state-of-the-art conversational AI models. NeMo has separate collections for Automatic Speech Recognition (ASR), Natural Language Processing (NLP), and Text-to-Speech (TTS) models. Each collection consists of prebuilt modules that include everything needed to train on your data. Every module can easily be customized, extended, and composed to create new conversational AI model architectures.

