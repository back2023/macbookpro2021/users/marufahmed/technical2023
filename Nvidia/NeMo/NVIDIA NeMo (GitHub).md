
https://github.com/NVIDIA/NeMo



## **NVIDIA NeMo**

### [](https://github.com/NVIDIA/NeMo#introduction)Introduction

NVIDIA NeMo is a conversational AI toolkit built for researchers working on automatic speech recognition (ASR), text-to-speech synthesis (TTS), large language models (LLMs), and natural language processing (NLP). The primary objective of NeMo is to help researchers from industry and academia to reuse prior work (code and pretrained models) and make it easier to create new [conversational AI models](https://developer.nvidia.com/conversational-ai#started).

All NeMo models are trained with [Lightning](https://github.com/Lightning-AI/lightning) and training is automatically scalable to 1000s of GPUs. Additionally, NeMo Megatron LLM models can be trained up to 1 trillion parameters using tensor and pipeline model parallelism. NeMo models can be optimized for inference and deployed for production use-cases with [NVIDIA Riva](https://developer.nvidia.com/riva).

Getting started with NeMo is simple. State of the Art pretrained NeMo models are freely available on [HuggingFace Hub](https://huggingface.co/models?library=nemo&sort=downloads&search=nvidia) and [NVIDIA NGC](https://catalog.ngc.nvidia.com/models?query=nemo&orderBy=weightPopularDESC). These models can be used to transcribe audio, synthesize speech, or translate text in just a few lines of code.

We have extensive [tutorials](https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/stable/starthere/tutorials.html) that can all be run on [Google Colab](https://colab.research.google.com/).

For advanced users that want to train NeMo models from scratch or finetune existing NeMo models we have a full suite of [example scripts](https://github.com/NVIDIA/NeMo/tree/main/examples) that support multi-GPU/multi-node training.

For scaling NeMo LLM training on Slurm clusters or public clouds, please see the [NVIDIA NeMo Megatron Launcher](https://github.com/NVIDIA/NeMo-Megatron-Launcher). The NM launcher has extensive recipes, scripts, utilities, and documentation for training NeMo LLMs and also has an [Autoconfigurator](https://github.com/NVIDIA/NeMo-Megatron-Launcher#53-using-autoconfigurator-to-find-the-optimal-configuration) which can be used to find the optimal model parallel configuration for training on a specific cluster.

Also see our [introductory video](https://www.youtube.com/embed/wBgpMf_KQVw) for a high level overview of NeMo.





