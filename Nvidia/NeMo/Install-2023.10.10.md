
https://github.com/NVIDIA/NeMo

```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/nemo python=3.10
 ```
 Error-1
```bash
...
CondaHTTPError: HTTP 500 INTERNAL SERVER ERROR for url <https://public.dhe.ibm.com/ibmdl/export/pub/software/server/ibm-ai/conda/linux-64/current_repodata.json>
Elapsed: 00:11.846994 
...
```
Try
```bash
[mah900@gadi-login-03 ~]$ conda config --show-sources
==> /home/900/mah900/.condarc <==
ssl_verify: False
channels:
  - https://public.dhe.ibm.com/ibmdl/export/pub/software/server/ibm-ai/conda
  - defaults
```
Source: https://www.freecodecamp.org/news/what-is-a-channel-in-conda/

Try
```python
Change the order from `~/.condarc` so that `defaults` the first channel as
channels:
  - defaults
  - conda-forge
```
Source: https://stackoverflow.com/questions/48547046/resetting-conda-channel-priorities
Working 
```bash
channels:
  #- https://public.dhe.ibm.com/ibmdl/export/pub/software/server/ibm-ai/conda
  - defaults
ssl_verify: false
```

Next
```bash

conda activate /scratch/fp0/mah900/env/nemo
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia
conda install -c conda-forge libsndfile1 ffmpeg
pip install Cython
pip install nemo_toolkit['all']

# RNNT 
conda remove numba
pip uninstall numba
conda install -c conda-forge numba

# NeMo Megatron
cd /scratch/fp0/mah900/
git clone https://github.com/NVIDIA/apex.git apex_2023
cd apex_2023
git checkout 52e18c894223800cb611682dce27d88050edf1de
pip install -v --no-build-isolation --disable-pip-version-check --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" --global-option="--fast_layer_norm" --global-option="--distributed_adam" --global-option="--deprecated_fused_adam" ./

...
    File "/scratch/fp0/mah900/env/nemo/lib/python3.10/site-packages/setuptools/build_meta.py", line 335, in run_setup
      exec(code, locals())
    File "<string>", line 137, in <module>
    File "<string>", line 24, in get_cuda_bare_metal_version
  TypeError: unsupported operand type(s) for +: 'NoneType' and 'str'
...

# /scratch/fp0/mah900/apex_2023/setup.py
# line 86: print ("bare_metal_version:", bare_metal_version)
bare_metal_version: 11.8

# /scratch/fp0/mah900/apex_2023/setup.py: 23
# raw_output = subprocess.check_output([cuda_dir + "/bin/nvcc", "-V"], universal_newlines=True) 
# So, Try isntalling nvcc 
# (Worked)
conda install -c "nvidia/label/cuda-11.8.0" cuda-toolkit cuda-nvcc

pip install -v --no-build-isolation --disable-pip-version-check --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" --global-option="--fast_layer_norm" --global-option="--distributed_adam" --global-option="--deprecated_fused_adam" ./

...
  Building wheel for apex (pyproject.toml) ... done
  Created wheel for apex: filename=apex-0.1-py3-none-any.whl size=373030 sha256=25d2c11a344092580be256d56ca9f400d8980201356cdd4d057cd042e068f0f5
  Stored in directory: /scratch/z00/mah900/tmp/pip-ephem-wheel-cache-vv6b80s3/wheels/75/0f/90/1cca32e64d737a26b0bc45a63e74beb31ac0f57f86449e90c5
Successfully built apex
Installing collected packages: apex
Successfully installed apex-0.1

# cuda-nvprof
conda install -c nvidia cuda-nvprof=11.8
# packaging
pip install packaging

# Transformer Engine 
pip install --upgrade --no-cache git+https://github.com/NVIDIA/TransformerEngine.git@stable

# Error
...
      CMake Error at /half-root/usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:230 (message):
        Could NOT find CUDNN (missing: CUDNN_INCLUDE_DIR CUDNN_LIBRARY)
      Call Stack (most recent call first):
        /half-root/usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:594 (_FPHSA_FAILURE_MESSAGE)
        cmake/FindCUDNN.cmake:46 (find_package_handle_standard_args)
        CMakeLists.txt:24 (find_package)
...
        File "/scratch/z00/mah900/tmp/pip-req-build-e2prvv_q/setup.py", line 366, in _build_cmake
          raise RuntimeError(f"Error when running CMake: {e}")
      RuntimeError: Error when running CMake: Command '['/half-root/usr/bin/cmake', '-S', '/scratch/z00/mah900/tmp/pip-req-build-e2prvv_q/transformer_engine', '-B', '/scratch/z00/mah900/tmp/tmpecgsxr_x', '-DCMAKE_BUILD_TYPE=Release', '-DCMAKE_INSTALL_PREFIX=/scratch/z00/mah900/tmp/pip-req-build-e2prvv_q/build/lib.linux-x86_64-cpython-310', '-GNinja', '-Dpybind11_DIR=/scratch/fp0/mah900/env/nemo/lib/python3.10/site-packages/pybind11/share/cmake/pybind11']' returned non-zero exit status 1.
      [end of output]

# https://docs.nvidia.com/deeplearning/transformer-engine/user-guide/installation.html
# [Pre-requisites](https://github.com/NVIDIA/TransformerEngine#pre-requisites)
# - Linux x86_64
# - CUDA 11.8 or later
# - NVIDIA Driver supporting CUDA 11.8 or later
# - cuDNN 8.1 or later
# - For fused attention, CUDA 12.1 or later, NVIDIA Driver supporting CUDA 12.1 or later, and cuDNN 8.9 or later.
# So, try install cudnn (Does not work)
python -m pip install --no-cache-dir --upgrade nvidia-cudnn-cu11==8.6.0.163

# Try again
pip install --upgrade --no-cache git+https://github.com/NVIDIA/TransformerEngine.git@stable

# The same error 
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
      -- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
      -- Looking for pthread_create in pthreads
      -- Looking for pthread_create in pthreads - not found
      -- Looking for pthread_create in pthread
      -- Looking for pthread_create in pthread - found
      -- Found Threads: TRUE
      -- cudnn not found.
      -- cudnn_adv_infer not found.
      -- cudnn_adv_train not found.
      -- cudnn_cnn_infer not found.
      -- cudnn_cnn_train not found.
      -- cudnn_ops_infer not found.
      -- cudnn_ops_train not found.
      CMake Error at /half-root/usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:230 (message):
        Could NOT find CUDNN (missing: CUDNN_INCLUDE_DIR CUDNN_LIBRARY)
      Call Stack (most recent call first):
        /half-root/usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:594 (_FPHSA_FAILURE_MESSAGE)
        cmake/FindCUDNN.cmake:46 (find_package_handle_standard_args)
        CMakeLists.txt:24 (find_package)

# Next try 
# conda install -c conda-forge cudnn=8.4 # Not Install
# conda install -c conda-forge cudnn=8.3 # Not Install
python -m pip install --no-cache-dir --upgrade nvidia-cudnn-cu11==8.5.0.96

pip install ninja

# Try, remembered
mkdir -p $CONDA_PREFIX/etc/conda/activate.d
echo 'CUDNN_PATH=$(dirname $(python -c "import nvidia.cudnn;print(nvidia.cudnn.__file__)"))' >> $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh
echo 'export LD_LIBRARY_PATH=$CONDA_PREFIX/lib/:$CUDNN_PATH/lib:$LD_LIBRARY_PATH' >> $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh


# Next, Try
conda install -c anaconda cudnn
...
The following packages will be downloaded: 
    package                    |            build
    ---------------------------|-----------------
    ca-certificates-2023.08.22 |       h06a4308_0         130 KB  anaconda
    certifi-2023.7.22          |  py310h06a4308_0         155 KB  anaconda
    cudatoolkit-11.8.0         |       h6a678d5_0      1013.7 MB  anaconda
    cudnn-8.9.2.26             |         cuda11_0       647.8 MB  anaconda
    openssl-3.0.11             |       h7f8727e_2         5.2 MB  anaconda
    ------------------------------------------------------------
                                           Total:        1.63 GB


# Differernt error
...
 -- The CUDA compiler identification is NVIDIA 11.8.89
      -- The CXX compiler identification is GNU 8.5.0
      -- Detecting CUDA compiler ABI info
      -- Detecting CUDA compiler ABI info - done
      -- Check for working CUDA compiler: /scratch/fp0/mah900/env/nemo/bin/nvcc - skipped
      -- Detecting CUDA compile features
      -- Detecting CUDA compile features - done
      -- Detecting CXX compiler ABI info
      -- Detecting CXX compiler ABI info - done
      -- Check for working CXX compiler: /opt/nci/bin/c++ - skipped
      -- Detecting CXX compile features
      -- Detecting CXX compile features - done
      -- Found CUDAToolkit: /scratch/fp0/mah900/env/nemo/include (found version "11.8.89")
      -- Looking for C++ include pthread.h
      -- Looking for C++ include pthread.h - found
      -- Performing Test CMAKE_HAVE_LIBC_PTHREAD
      -- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
      -- Looking for pthread_create in pthreads
      -- Looking for pthread_create in pthreads - not found
      -- Looking for pthread_create in pthread
      -- Looking for pthread_create in pthread - found
      -- Found Threads: TRUE
      -- cudnn found at /scratch/fp0/mah900/env/nemo/lib/libcudnn.so.
      -- cudnn_adv_infer found at /scratch/fp0/mah900/env/nemo/lib/libcudnn_adv_infer.so.
      -- cudnn_adv_train found at /scratch/fp0/mah900/env/nemo/lib/libcudnn_adv_train.so.
      -- cudnn_cnn_infer found at /scratch/fp0/mah900/env/nemo/lib/libcudnn_cnn_infer.so.
      -- cudnn_cnn_train found at /scratch/fp0/mah900/env/nemo/lib/libcudnn_cnn_train.so.
      -- cudnn_ops_infer found at /scratch/fp0/mah900/env/nemo/lib/libcudnn_ops_infer.so.
      -- cudnn_ops_train found at /scratch/fp0/mah900/env/nemo/lib/libcudnn_ops_train.so.
      -- Found CUDNN: /scratch/fp0/mah900/env/nemo/include
      -- cuDNN: /scratch/fp0/mah900/env/nemo/lib/libcudnn.so
      -- cuDNN: /scratch/fp0/mah900/env/nemo/include
      -- Found Python: /scratch/fp0/mah900/env/nemo/bin/python3.10 (found version "3.10.13") found components: Interpreter Development Development.Module Development.Embed
      -- JAX support: OFF
      -- TensorFlow support: OFF
      -- Configuring done
      -- Generating done
      CMake Warning:
      
...
      ninja: build stopped: subcommand failed.
      Traceback (most recent call last):
        File "/scratch/fp0/mah900/env/nemo/lib/python3.10/site-packages/torch/utils/cpp_extension.py", line 2100, in _run_ninja_build
          subprocess.run(
        File "/scratch/fp0/mah900/env/nemo/lib/python3.10/subprocess.py", line 526, in run
          raise CalledProcessError(retcode, process.args,
      subprocess.CalledProcessError: Command '['ninja', '-v']' returned non-zero exit status 1.

# Test
'ninja' '-v'
# Try
pip install ninja
Requirement already satisfied: ninja in /scratch/fp0/mah900/env/nemo/lib/python3.10/site-packages (1.11.1.1)

#python -m pip install ninja  
# The same error

conda install -c conda-forge ninja
...
The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    ninja-1.10.2               |       h06a4308_5           8 KB
    ninja-base-1.10.2          |       hd09550d_5         109 KB
    ------------------------------------------------------------
                                           Total:         116 KB
# The same error
...
      ninja: build stopped: subcommand failed.
      Traceback (most recent call last):
        File "/scratch/fp0/mah900/env/nemo/lib/python3.10/site-packages/torch/utils/cpp_extension.py", line 2100, in _run_ninja_build
          subprocess.run(
        File "/scratch/fp0/mah900/env/nemo/lib/python3.10/subprocess.py", line 526, in run
          raise CalledProcessError(retcode, process.args,
      subprocess.CalledProcessError: Command '['ninja', '-v']' returned non-zero exit status 1.
...
ninja: error: loading 'build.ninja': No such file or directory

# ninja --version
# 1.10.2
# So Try
# /scratch/fp0/mah900/env/nemo/lib/python3.10/site-packages/torch/utils/cpp_extension.py: 2065
# Replace in line 2065
# # command = ['ninja', '-v']
command = ['ninja', '--version']


# New Error 
...
Building wheels for collected packages: transformer-engine, flash-attn
  Building wheel for transformer-engine (setup.py) ... error
  error: subprocess-exited-with-error
  
  × python setup.py bdist_wheel did not run successfully.
  │ exit code: 1
  ╰─> [208 lines of output]
      No CUDA runtime is found, using CUDA_HOME='/scratch/fp0/mah900/env/nemo'

...
      error: command '/opt/nci/bin/g++' failed with exit code 1
      [end of output]
  
  note: This error originates from a subprocess, and is likely not a problem with pip.
  ERROR: Failed building wheel for flash-attn
  Running setup.py clean for flash-attn
Failed to build transformer-engine flash-attn
ERROR: Could not build wheels for transformer-engine, flash-attn, which is required to install pyproject.toml-based projects
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/common.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/activation.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/attention.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/cast.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/gemm.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/misc.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/normalization.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/pybind.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/softmax.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/extensions/transpose.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/build/temp.linux-x86_64-cpython-310/scratch/z00/mah900/tmp/pip-req-build-b89qsnp5/transformer_engine/pytorch/csrc/ts_fp8_op.o: No such file or directory
      error: command '/opt/nci/bin/g++' failed with exit code 1
      [end of output]
  
  note: This error originates from a subprocess, and is likely not a problem with pip.
  ERROR: Failed building wheel for transformer-engine
  Running setup.py clean for transformer-engine
  Building wheel for flash-attn (setup.py) ... error
  error: subprocess-exited-with-error
  
  × python setup.py bdist_wheel did not run successfully.
  │ exit code: 1
  ╰─> [123 lines of output]
      No CUDA runtime is found, using CUDA_HOME='/scratch/fp0/mah900/env/nemo'

      
...
     /scratch/fp0/mah900/env/nemo/lib/python3.10/site-packages/torch/utils/cpp_extension.py:424: UserWarning: There are no g++ version bounds defined for CUDA version 11.8
        warnings.warn(f'There are no {compiler_name} version bounds defined for CUDA version {cuda_str_version}')
...
6_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim224_fp16_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim256_bf16_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim256_fp16_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim32_bf16_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim32_fp16_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim64_bf16_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim64_fp16_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim96_bf16_sm80.o: No such file or directory
      g++: error: /scratch/z00/mah900/tmp/pip-install-v32w1510/flash-attn_16f3cfc4b0da4fc3bd8153b65a9d1047/build/temp.linux-x86_64-cpython-310/csrc/flash_attn/src/flash_fwd_hdim96_fp16_sm80.o: No such file or directory
      error: command '/opt/nci/bin/g++' failed with exit code 1
      [end of output]
  
  note: This error originates from a subprocess, and is likely not a problem with pip.
  ERROR: Failed building wheel for flash-attn
  Running setup.py clean for flash-attn
Failed to build transformer-engine flash-attn
ERROR: Could not build wheels for transformer-engine, flash-attn, which is required to install pyproject.toml-based projects



# [Flash Attention](https://github.com/NVIDIA/NeMo#flash-attention)
pip install flash-attn
pip install triton==2.0.0.dev20221202

# [NLP inference UI](https://github.com/NVIDIA/NeMo#nlp-inference-ui)
pip install gradio==3.34.0


# Try again (Not Worked)
export FORCE_CUDA="1"

# Next Try (No install)
conda install cudatoolkit-dev=11.8 

# Next Try (Not worked)
NVTE_CUDA_INCLUDE_PATH=/scratch/fp0/mah900/env/nemo/include


# Next Try (Already installed)
pip install Cmake

# Next Try
export CUDA_HOME=$CONDA_PREFIX

# Next try download repo 
# Next offline install

```
Source: https://anaconda.org/conda-forge/libsndfile
Source: https://anaconda.org/conda-forge/ffmpeg

~~Source: https://pypi.org/project/ninja/ ~~
Source:https://anaconda.org/conda-forge/ninja


### 2023.10.11

```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/nemo 

# Next Try (Not work)
cd /scratch/fp0/mah900/
git clone https://github.com/NVIDIA/TransformerEngine.git
cd TransformerEngine/
pip install . --no-cache

# Next Try, (Not work)
pip install git+https://github.com/NVIDIA/TransformerEngine.git@main

# Next Try (Not work)
# Installation (from source)
rm -rf /scratch/fp0/mah900/TransformerEngine
git clone --branch stable --recursive https://github.com/NVIDIA/TransformerEngine.git
cd TransformerEngine
export NVTE_FRAMEWORK=pytorch   # Optionally set framework
pip install . --no-cache                   # Build and install
```
Source: https://docs.nvidia.com/deeplearning/transformer-engine/user-guide/installation.html

```bash
# Next Istalls
conda install -c conda-forge sox
pip install text-unidecode

# Jupyter
conda install -c conda-forge jupyterlab ipywidgets
```

```bash
# Next Try, offline install
pip download git+https://github.com/NVIDIA/TransformerEngine.git@stable -d /g/data/wb00/admin/testing/tmp

# Saved all nvidia_cudnn_cu12 files
...
Saved /g/data/wb00/admin/testing/tmp/transformer-engine-0.13.0+8eae4ce.zip
Saved /g/data/wb00/admin/testing/tmp/flash_attn-2.0.4.tar.gz
Saved /g/data/wb00/admin/testing/tmp/pydantic-2.4.2-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/pydantic_core-2.10.1-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/torch-2.1.0-cp310-cp310-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_cublas_cu12-12.1.3.1-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_cuda_cupti_cu12-12.1.105-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_cuda_nvrtc_cu12-12.1.105-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_cuda_runtime_cu12-12.1.105-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_cudnn_cu12-8.9.2.26-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_cufft_cu12-11.0.2.54-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_curand_cu12-10.3.2.106-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_cusolver_cu12-11.4.5.107-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_cusparse_cu12-12.1.0.106-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_nccl_cu12-2.18.1-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_nvtx_cu12-12.1.105-py3-none-manylinux1_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/triton-2.1.0-0-cp310-cp310-manylinux2014_x86_64.manylinux_2_17_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/annotated_types-0.6.0-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/typing_extensions-4.8.0-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/einops-0.7.0-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/filelock-3.12.4-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/fsspec-2023.9.2-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/Jinja2-3.1.2-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/networkx-3.1-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/ninja-1.11.1.1-py2.py3-none-manylinux1_x86_64.manylinux_2_5_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/packaging-23.2-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/sympy-1.12-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/MarkupSafe-2.1.3-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl
Saved /g/data/wb00/admin/testing/tmp/mpmath-1.3.0-py3-none-any.whl
Saved /g/data/wb00/admin/testing/tmp/nvidia_nvjitlink_cu12-12.2.140-py3-none-manylinux1_x86_64.whl
Successfully downloaded transformer-engine flash-attn pydantic pydantic-core torch nvidia-cublas-cu12 nvidia-cuda-cupti-cu12 nvidia-cuda-nvrtc-cu12 nvidia-cuda-runtime-cu12 nvidia-cudnn-cu12 nvidia-cufft-cu12 nvidia-curand-cu12 nvidia-cusolver-cu12 nvidia-cusparse-cu12 nvidia-nccl-cu12 nvidia-nvtx-cu12 triton annotated-types typing-extensions einops filelock fsspec jinja2 networkx ninja packaging sympy MarkupSafe mpmath nvidia-nvjitlink-cu12
```

Another Try with `nvidia-cudnn-cu11`
```bash
rm -rf /scratch/fp0/mah900/TransformerEngine
rm -rf /g/data/wb00/admin/testing/tmp/* 

pip download git+https://github.com/NVIDIA/TransformerEngine.git@stable \
nvidia-cudnn-cu11==8.6.0.163 tensorflow==2.13.* nvidia-tensorrt \
"jax[cuda11_local]" -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html \
-d /g/data/wb00/admin/testing/tmp

...


```


#### ARE-Nemo
```
12
gpuvolta
1gpu
fp0
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00+gdata/rt52+gdata/hh5+scratch/fp0+gdata/rr3+scratch/vp91

/scratch/fp0/mah900/Miniconda2023
/scratch/fp0/mah900/env/nemo
```