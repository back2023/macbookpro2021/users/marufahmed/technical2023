
https://developer.nvidia.com/nemo#nemo-megatron


NVIDIA NeMo™ is an end-to-end, cloud-native enterprise framework for developers to build, customize, and deploy generative AI models with billions of parameters.  
  
The NeMo framework provides an accelerated workflow for training with 3D parallelism techniques. It offers a choice of several customization techniques and is optimized for at-scale inference of models for language and image applications, with multi-GPU and multi-node configurations. NeMo makes generative AI model development easy, cost-effective, and fast for enterprises.






