

https://github.com/NVIDIA/modulus-launch


# Modulus Launch (Beta)

[![Project Status: Moved](https://camo.githubusercontent.com/41d421e5138b91b5911996ecd840b2cba74dbd35ad0203eba89978a0071ed49c/68747470733a2f2f7777772e7265706f7374617475732e6f72672f6261646765732f6c61746573742f6d6f7665642e737667)](https://www.repostatus.org/#moved) [![GitHub](https://camo.githubusercontent.com/cd6d510a3665dbabd93f11834b4fecb2950cc6a1d9c9c086d63fdc78e7b6b35c/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6963656e73652f4e56494449412f6d6f64756c75732d6c61756e6368)](https://github.com/NVIDIA/modulus-launch/blob/master/LICENSE.txt) [![Code style: black](https://camo.githubusercontent.com/d91ed7ac7abbd5a6102cbe988dd8e9ac21bde0a73d97be7603b891ad08ce3479/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f636f64652532307374796c652d626c61636b2d3030303030302e737667)](https://github.com/psf/black)

## [](https://github.com/NVIDIA/modulus-launch#rotating_light-this-repo-has-been-upstreamed-to-modulus-repository)🚨 This repo has been upstreamed to [Modulus](https://github.com/NVIDIA/modulus) repository

Modulus Launch is a PyTorch based deep-learning collection of training recipes and tools for creating physical surrogates. The goal of this repository is to provide a collection of deep learning training examples for different phenomena as starting points for academic and industrial applications. Additional information can be found in the [Modulus documentation](https://docs.nvidia.com/modulus/index.html#launch).

