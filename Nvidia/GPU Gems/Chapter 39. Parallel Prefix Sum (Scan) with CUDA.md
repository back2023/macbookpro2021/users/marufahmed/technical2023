

A simple and common parallel algorithm building block is the _all-prefix-sums_ operation. In this chapter, we define and illustrate the operation, and we discuss in detail its efficient implementation using NVIDIA CUDA. Blelloch (1990) describes all-prefix-sums as a good example of a computation that seems inherently sequential, but for which there is an efficient parallel algorithm. He defines the all-prefix-sums operation as follows:

> The all-prefix-sums operation takes a binary associative operator ![U2295.GIF](https://developer.nvidia.com/sites/all/modules/custom/gpugems/books/GPUGems3/elementLinks/U2295.GIF) with identity _I_, and an array of _n_ elements
> 
> |   |
> |---|
> |[_a_ 0, _a_ 1,..., _a_ _n_–1],|
> 
> and returns the array
> 
> |   |
> |---|
> |[_I_, _a_ 0, (_a_ 0 ![U2295.GIF](https://developer.nvidia.com/sites/all/modules/custom/gpugems/books/GPUGems3/elementLinks/U2295.GIF) _a_ 1),..., (_a_ 0 ![U2295.GIF](https://developer.nvidia.com/sites/all/modules/custom/gpugems/books/GPUGems3/elementLinks/U2295.GIF) _a_ 1 ![U2295.GIF](https://developer.nvidia.com/sites/all/modules/custom/gpugems/books/GPUGems3/elementLinks/U2295.GIF) ... ![U2295.GIF](https://developer.nvidia.com/sites/all/modules/custom/gpugems/books/GPUGems3/elementLinks/U2295.GIF) _a_ _n_–2)]|


https://developer.nvidia.com/gpugems/gpugems3/part-vi-gpu-computing/chapter-39-parallel-prefix-sum-scan-cuda