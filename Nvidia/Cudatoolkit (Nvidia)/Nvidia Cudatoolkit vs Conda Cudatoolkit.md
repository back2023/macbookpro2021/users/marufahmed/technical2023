
https://stackoverflow.com/questions/59529804/nvidia-cudatoolkit-vs-conda-cudatoolkit


If using anaconda to install tensorflow-gpu, yes it will install cuda and cudnn for you in same conda environment as tensorflow-gpu. All you need to install yourself is the latest nvidia-driver (so that it works with the latest CUDA level and all older CUDA levels you use.)

This has many advantages over the pip install tensorflow-gpu method:

1. Anaconda will always install the CUDA and CuDNN version that the TensorFlow code was compiled to use.
2. You can have multiple conda environments with different levels of TensorFlow, CUDA, and CuDNN and just use conda activate to switch between them.
3. You don't have to deal with installing CUDA and cuDNN manaually at the system wide level.

The disadvantage when compared to pip install tensorflow-gpu, is the latest version of tensorflow is added to pypi weeks before Anaconda is able to update the conda recipe and publish their builds of the latest TensorFlow version.