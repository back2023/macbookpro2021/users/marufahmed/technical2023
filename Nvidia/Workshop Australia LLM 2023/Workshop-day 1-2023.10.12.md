Attendance: 49

### Agenda 
Connecting to server
Nemp framework - repeat, a lot of you are from last workshop in august (?)
Question answering
Multitask promt
Megatron
Callenge 

****

### Bharat: 9:06:
All documents 
https://drive.google.com/drive/folders/1ifhqYAHhHOFS5DRkPwiE1DFkD05aauYq

August workshop covered language model
This is about Nemo. 
...
August workshop was more about Transformers

Spin up the server
```
8f01accfe9b84e56aa9d612abe13cad4
squeue --me
sbatch /bootcamp_scripts/NLP/sbatch-llm 8f01accfe9b84e56aa9d612abe13cad4
cat port_forwarding_command 
 
```


* Start from Memo fundamental notebook (Notebook 9)

### Breakout room - 
TA, Brad Stratton: Works in Canberra based AI, works in Digital Twin

Here Until, 10:45

Johan in the breakout room,  here to answer
How to add hugging face models to Nemo ?
Johan: Support tokenizer, not all, have plugins 
Johan: https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/main/_modules/nemo/collections/common/tokenizers/huggingface/auto_tokenizer.html

Does it support vector embedding?
Johan: it is coming

Cloud native?
Brad: Github repo from where their company deployed it. 
https://github.com/NVIDIA/NeMo

Difference between Hydra and Omega config?
Brad: interact with each other.

1st Break 

### Session: 11:00

Bert is the hello world of language models

### Breakout room: 11.15
Open until: 12.15

B. Notebook. [Question Answering](http://localhost:8888/files/jupyter_notebook/nemo/Question_Answering.ipynb?_xsrf=2%7C8aea328d%7Cea47971e8a2e67038339f743e902a1ac%7C1696999367)

Das, Abishek
"https://catalog.ngc.nvidia.com/orgs/nvidia/teams/nemo/models/stt_en_citrinet_512"
Can above framework can be tested in this container?
Johan: Will be complicated
NGC has models, get it from there: https://catalog.ngc.nvidia.com/orgs/nvidia/teams/nemo/models/stt_en_citrinet_512

Followint show the results path
print (exp_dir)

break until, 1 pm

***
12:20
Staff meeting is just cancelled
(I forgot, but good news)
***

### Session: 1.00 pm

Prompt Tuning/ P-Tuning 

### Breakout room: 1.20 pm
- Ends in 2.00

Notebook for this lab: C. [Prompt Tuning/P-Tuning](http://localhost:8888/files/jupyter_notebook/nemo/Multitask_Prompt_and_PTuning.ipynb?_xsrf=2%7C8aea328d%7Cea47971e8a2e67038339f743e902a1ac%7C1696999367)

P-tune or Prompt?
Johan: Always use P-tune, as it shows good results. 

Short/long answer parameter can be set. 

```python
  max_seq_length: 1024
  min_seq_length: 1
```

Johan: 15 min extended for everyone to finish 

Asking to the group:
P-tune is mainly by Nvidia, all links lead to Nvidia
No one other are talking about it.
Johan: Yes, working on P-tune a lot. 
Also, hugging face has an article. 
"https://huggingface.co/docs/peft/main/en/task_guides/ptuning-seq-classification"

Is ptuning is a type of PEFT?
Johan: yes
https://github.com/huggingface/peft

***

### Session: 2.27
Last Lab of the day on inference.
No slides, only lab

This jupyter is tricky, so listen carefully
First, Shutdown all kernels on the jupyter

Notebook: D. [NeMo Megatron-GPT 1.3B: Language Model Inferencing](http://localhost:8888/files/jupyter_notebook/nemo/demo.ipynb?_xsrf=2%7C8aea328d%7Cea47971e8a2e67038339f743e902a1ac%7C1696999367)

Megatron is a part of Nemo, dediacated for inference. 

### Open a terminal to launch the server (Working)

Codes are from the demo notebook: `/jupyter_notebook/nemo/demo.ipynb` 
section: Start The Server
```bash
cd /workspace/
git clone https://github.com/NVIDIA/NeMo.git

cd NeMo
pwd
/workspace/NeMO # make sure you are here
git checkout v1.17.0

# to change the port number goto config
NeMo/examples/nlp/language_modeling/conf/megatron_gpt_inference.yaml

line 32: port: #5555 # need to change here

# To find the port number to axis web cmda and cat the file `megatron_gpt_port`
# Copy the port number
# From axis web cmd get
bgnb2w2b@curiosity:~$ ls
megatron_gpt_port  nltk_data  port_forwarding_command  slurm-8095.out  slurm-8158.out  slurm-8210.out
bgnb2w2b@curiosity:~$ cat megatron_gpt_port 
Copy and paste to replace the Megatron-GPT client port in your notebook: 10366
# back to config file
line 32: port: 10366 #5555 # the port number for the inference server

# go back to terminal and directory
cd /workspace/NeMo/examples/nlp/language_modeling
# Server run command
python megatron_gpt_eval.py gpt_model_file=/workspace/source_code/nemo_gpt1.3B_fp16.nemo server=True tensor_model_parallel_size=1 trainer.devices=1

...
***************************
 * Serving Flask app 'nemo.collections.nlp.modules.common.text_generation_server'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:10366
 * Running on http://10.155.45.136:10366

```

### Breakout room: 2:45
Ends in 3:15

Q:  "story" variable purpose?
Johan: just to replace the string before, use to ask a new thing 

Q: How to validate LLM, any tools / techniques
Johan: No, just the best/good benchmark. 
Brad: they use different LLM to validate a LLM, but do not know how good the solution is that solution. 

Johan: https://huggingface.co/spaces/HuggingFaceH4/open_llm_leaderboard
Q: P-tune ...


### Sesson: 3.20 pm

Tommorow will discuss settings/config (???)

Summary 
Today Showed Nemo, a large framework. 
Have pre-train models in huggingface.
Also, integrating community models
Also, serving in Nvidia

Nemo Inform 
Nemo Guardrials - will be discussed tomorrow
Nemo SterrLM

Nemo can do 
Language model
text to image
image to image

Announcing NeMo SteerLM
It has Tune LLM

Everything is accsible by creating a Nvidia account

### Challenge 

Can be done today or tomorrow
Challene_cuad notebook

In the challenge we have Perform all steps shown so far in the workshop.
Its a end to end pipeline
If you can not finish by tomorrow,  can try later, the server will be up for a few days

Some one has a use-case and wanted to discuss it tomorrow as the chanllenge.
Though everyone has to bring a user-case.

Someone, is from Princeton, New Jersey and got the time zone wrong. 


### End 3:33





