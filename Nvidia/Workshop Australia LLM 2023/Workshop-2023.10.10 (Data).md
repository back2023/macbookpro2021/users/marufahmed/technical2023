

### Axis Security 
*** 

Tue 10-Oct-23 3:32 AM
"
Thanks for joining Axis Security, Maruf Ahmed.  
You were invited to join the RAPLAB HACKATHON workspace.  
  
Your Username is bgnb2w2b.  
[Set a password](https://protect-au.mimecast.com/s/WndYC91WPRTzWQjg7HEr1FS?domain=auth.axissecurity.com) for your account in order to log in to your User Portal.  
  
**Note:** This link is valid for 7 days.
"
<!--  Nvidia2023  -->
***
Cluster link:
https://axis-raplabhackathon.axisportal.io/apps
https://auth.axissecurity.com/
*** 

### 2023.10.11

Apoorva (NVIDIA)  [1:08 PM](https://australiallmbootcamp.slack.com/archives/C05UZ1TJMHQ/p1696990113236429)  
@channel  
Hi All,
Australia LLM Bootcamp Day 0 is coming up Today at 3PM AEDT!  
Please join via Zoom link: [https://us06web.zoom.us/j/89329532760?pwd=gkdPv6GDhRHaOmiaFYHqxkPYNSjeqn.1](https://us06web.zoom.us/j/89329532760?pwd=gkdPv6GDhRHaOmiaFYHqxkPYNSjeqn.1)
***


### 2023.10.11-3:10
Attendance beginning : 42
### Cluster access slides
Johan Barthelemy (NVIDIA)  [3:16 PM](https://australiallmbootcamp.slack.com/archives/C05VBLN0NQH/p1696997768462079)
https://australiallmbootcamp.slack.com/files/U05UZ3N8W83/F060P97LQJY/cluster_connection_day_1.pdf

```bash
# One time access key for Secure Shell
ssh ssh.axisapps.io -l 15d9ff99cd5f405ea7982eea425f40fe
```

```bash
squeue --me
#scancel
 
sbatch /bootcamp_scripts/NLP/sbatch-llm 15d9ff99cd5f405ea7982eea425f40fe
Submitted batch job 8095
squeue --me
# Wait for the portforwarding file, generated after the task starts.
# May need 8 minutes.
ls
...
megatron_gpt_port  port_forwarding_command  slurm-8095.out

cat port_forwarding_command 
ssh -L localhost:8888:dgx07:9002 ssh.axisapps.io -l 15d9ff99cd5f405ea7982eea425f40fe 

# If do not copy HASH correctly, then port_forwarding_command will not have hash
# Also, the output file will be in a few seconds instead of minutes

# If you have more than one job, then kill all before creating a new one.
# Kill all before leaving 
```

Now, run the port forwarding command
```bash
(base) marufahmed@Marufs-MacBook-Pro ~ % ssh -L localhost:8888:dgx07:9002 ssh.axisapps.io -l 15d9ff99cd5f405ea7982eea425f40fe
...
bgnb2w2b@curiosity:~$ 
```
Now, open in Browser.
**Note**: 
- Do not forward your mail to others, there are limited number of VMs.
- If you can run in Browser, then done for today

***

### Jupyter terminal 
PIP
```bash
Singularity> pip list
Package                       Version
----------------------------- --------------------------
absl-py                       1.4.0
accessible-pygments           0.0.4
aiofiles                      23.1.0
aiohttp                       3.8.4
aiosignal                     1.3.1
alabaster                     0.7.13
altair                        4.2.2
aniso8601                     9.0.1
antlr4-python3-runtime        4.9.3
anyio                         3.6.2
apex                          0.1
appdirs                       1.4.4
argon2-cffi                   21.3.0
argon2-cffi-bindings          21.2.0
asttokens                     2.2.1
astunparse                    1.6.3
async-timeout                 4.0.2
attrdict                      2.0.1
attrs                         22.2.0
audioread                     3.0.0
Babel                         2.12.1
backcall                      0.2.0
beautifulsoup4                4.11.2
black                         19.10b0
bleach                        6.0.0
blis                          0.7.9
boto3                         1.26.106
botocore                      1.29.106
braceexpand                   0.1.7
cachetools                    5.2.0
catalogue                     2.0.8
cdifflib                      1.2.6
certifi                       2022.12.7
cffi                          1.15.1
charset-normalizer            3.0.1
click                         8.0.2
cloudpickle                   2.2.0
cmake                         3.24.1.1
colorama                      0.4.6
comm                          0.1.2
commonmark                    0.9.1
confection                    0.0.4
contourpy                     1.0.6
cubinlinker                   0.2.2+2.g8e13447
cuda-python                   12.1.0rc1+1.g9e30ea2.dirty
cudf                          22.12.0
cugraph                       22.12.0
cugraph-dgl                   22.12.0
cugraph-service-client        22.12.0
cugraph-service-server        22.12.0
cuml                          22.12.0
cupy-cuda12x                  12.0.0a2
cycler                        0.11.0
cymem                         2.0.7
Cython                        0.29.33
dask                          2022.11.1
dask-cuda                     22.12.0
dask-cudf                     22.12.0
debugpy                       1.6.6
decorator                     5.1.1
defusedxml                    0.7.1
Distance                      0.1.3
distributed                   2022.11.1
docker-pycreds                0.4.0
docopt                        0.6.2
docutils                      0.19
editdistance                  0.6.2
einops                        0.6.0
entrypoints                   0.4
exceptiongroup                1.1.0
execnet                       1.9.0
executing                     1.2.0
expecttest                    0.1.3
faiss-cpu                     1.7.3
fastapi                       0.95.0
fastjsonschema                2.16.2
fastrlock                     0.8.1
fasttext                      0.9.2
ffmpy                         0.3.0
filelock                      3.9.0
Flask                         2.2.3
Flask-RESTful                 0.3.9
fonttools                     4.38.0
frozenlist                    1.3.3
fsspec                        2022.11.0
ftfy                          6.1.1
g2p-en                        2.1.0
gast                          0.4.0
gdown                         4.7.1
gitdb                         4.0.10
GitPython                     3.1.31
google-auth                   2.16.0
google-auth-oauthlib          0.4.6
gradio                        3.24.1
gradio_client                 0.0.7
graphsurgeon                  0.4.6
graphviz                      0.20.1
grpcio                        1.51.1
h11                           0.14.0
h5py                          3.8.0
HeapDict                      1.0.1
httpcore                      0.16.3
httpx                         0.23.3
huggingface-hub               0.13.3
hydra-core                    1.2.0
hypothesis                    5.35.1
idna                          3.4
ijson                         3.2.0.post0
imagesize                     1.4.1
importlib-metadata            5.1.0
importlib-resources           5.10.2
inflect                       6.0.2
iniconfig                     2.0.0
intel-openmp                  2021.4.0
ipadic                        1.0.0
ipykernel                     6.21.1
ipython                       8.9.0
ipython-genutils              0.2.0
ipywidgets                    8.0.6
isort                         5.12.0
itsdangerous                  2.1.2
jedi                          0.18.2
jieba                         0.42.1
Jinja2                        3.0.3
jiwer                         2.5.2
jmespath                      1.0.1
joblib                        1.2.0
json5                         0.9.11
jsonschema                    4.17.3
jupyter_client                8.0.2
jupyter_core                  5.2.0
jupyter-tensorboard           0.2.0
jupyterlab                    2.3.2
jupyterlab-pygments           0.2.2
jupyterlab-server             1.2.0
jupyterlab-widgets            3.0.7
jupytext                      1.14.4
kaldi-python-io               1.2.2
kaldiio                       2.18.0
kiwisolver                    1.4.4
kornia                        0.6.11
langcodes                     3.3.0
latexcodec                    2.0.1
Levenshtein                   0.20.9
librosa                       0.9.2
lightning-utilities           0.8.0
linkify-it-py                 1.0.3
llvmlite                      0.39.1
locket                        1.0.0
loguru                        0.6.0
lxml                          4.9.2
Markdown                      3.4.1
markdown-it-py                2.1.0
MarkupSafe                    2.1.1
marshmallow                   3.19.0
matplotlib                    3.6.2
matplotlib-inline             0.1.6
mdit-py-plugins               0.3.3
mdurl                         0.1.2
mecab-python3                 1.0.5
megatron-core                 0.2.0
mistune                       2.0.5
mkl                           2021.1.1
mkl-devel                     2021.1.1
mkl-include                   2021.1.1
mock                          5.0.1
mpmath                        1.2.1
msgpack                       1.0.4
multidict                     6.0.4
murmurhash                    1.0.9
nbclient                      0.7.2
nbconvert                     7.2.9
nbformat                      5.7.3
nemo-text-processing          0.1.7rc0
nemo-toolkit                  1.17.0
nest-asyncio                  1.5.6
networkx                      2.6.3
nltk                          3.8.1
notebook                      6.4.10
numba                         0.56.4+1.g772622d0d
numpy                         1.22.2
nvidia-dali-cuda110           1.22.0
nvidia-pyindex                1.0.9
nvtx                          0.2.5
oauthlib                      3.2.2
omegaconf                     2.2.3
onnx                          1.13.0
OpenCC                        1.1.6
opencv                        4.6.0
orjson                        3.8.9
packaging                     22.0
pandas                        1.5.2
pandocfilters                 1.5.0
pangu                         4.0.6.1
parameterized                 0.9.0
parso                         0.8.3
partd                         1.3.0
pathspec                      0.11.1
pathtools                     0.1.2
pathy                         0.10.1
pexpect                       4.8.0
pickleshare                   0.7.5
Pillow                        9.2.0
pip                           21.2.4
pkgutil_resolve_name          1.3.10
plac                          1.3.5
platformdirs                  3.0.0
pluggy                        1.0.0
ply                           3.11
polygraphy                    0.43.1
pooch                         1.6.0
portalocker                   2.7.0
preshed                       3.0.8
prettytable                   3.6.0
progress                      1.6
prometheus-client             0.16.0
prompt-toolkit                3.0.36
protobuf                      3.20.3
psutil                        5.9.4
ptxcompiler                   0.7.0+27.gd73915e
ptyprocess                    0.7.0
pure-eval                     0.2.2
pyannote.core                 5.0.0
pyannote.database             5.0.0
pyannote.metrics              3.2.1
pyarrow                       9.0.0
pyasn1                        0.4.8
pyasn1-modules                0.2.8
pybind11                      2.10.3
pybtex                        0.24.0
pybtex-docutils               1.0.2
pycocotools                   2.0+nv0.7.1
pycparser                     2.21
pydantic                      1.10.4
pydata-sphinx-theme           0.13.3
pydub                         0.25.1
Pygments                      2.14.0
pylibcugraph                  22.12.0
pylibcugraphops               22.12.0
pylibraft                     22.12.0
pynini                        2.1.5
pynvml                        11.4.1
pyparsing                     3.0.9
pypinyin                      0.48.0
pypinyin-dict                 0.5.0
pyrsistent                    0.19.3
PySocks                       1.7.1
pytest                        7.2.1
pytest-rerunfailures          11.0
pytest-runner                 6.0.0
pytest-shard                  0.1.2
pytest-xdist                  3.2.0
python-dateutil               2.8.2
python-hostlist               1.23.0
python-multipart              0.0.6
pytorch-lightning             1.9.4
pytorch-quantization          2.1.2
pytz                          2022.6
PyYAML                        5.4.1
pyzmq                         25.0.0
raft-dask                     22.12.0
rapidfuzz                     2.13.7
regex                         2022.10.31
requests                      2.28.2
requests-oauthlib             1.3.1
resampy                       0.4.2
rfc3986                       1.5.0
rich                          12.6.0
rmm                           22.12.0
rsa                           4.9
ruamel.yaml                   0.17.21
ruamel.yaml.clib              0.2.7
s3transfer                    0.6.0
sacrebleu                     2.3.1
sacremoses                    0.0.53
scikit-learn                  0.24.2
scipy                         1.6.3
seaborn                       0.12.1
semantic-version              2.10.0
Send2Trash                    1.8.0
sentence-transformers         2.2.2
sentencepiece                 0.1.97
sentry-sdk                    1.19.1
setproctitle                  1.3.2
setuptools                    65.5.1
shellingham                   1.5.0.post1
six                           1.16.0
smart-open                    6.3.0
smmap                         5.0.0
sniffio                       1.3.0
snowballstemmer               2.2.0
sortedcontainers              2.4.0
soundfile                     0.11.0
soupsieve                     2.3.2.post1
sox                           1.4.1
spacy                         3.5.0
spacy-legacy                  3.0.12
spacy-loggers                 1.0.4
Sphinx                        5.3.0
sphinx-book-theme             1.0.1
sphinx-copybutton             0.5.1
sphinx-glpi-theme             0.3
sphinxcontrib-applehelp       1.0.4
sphinxcontrib-bibtex          2.5.0
sphinxcontrib-devhelp         1.0.2
sphinxcontrib-htmlhelp        2.0.1
sphinxcontrib-jsmath          1.0.1
sphinxcontrib-qthelp          1.0.3
sphinxcontrib-serializinghtml 1.1.5
srsly                         2.4.5
stack-data                    0.6.2
starlette                     0.26.1
strings-udf                   22.12.0
sympy                         1.11.1
tabulate                      0.9.0
tbb                           2021.8.0
tblib                         1.7.0
tensorboard                   2.9.0
tensorboard-data-server       0.6.1
tensorboard-plugin-wit        1.8.1
tensorrt                      8.5.3.1
termcolor                     2.2.0
terminado                     0.17.1
text-unidecode                1.3
textdistance                  4.5.0
texterrors                    0.4.4
thinc                         8.1.7
threadpoolctl                 3.1.0
thriftpy2                     0.4.16
tinycss2                      1.2.1
tokenizers                    0.13.3
toml                          0.10.2
tomli                         2.0.1
toolz                         0.12.0
torch                         1.14.0a0+44dac51
torch-tensorrt                1.4.0.dev0
torchaudio                    0.14.0
torchmetrics                  0.11.4
torchvision                   0.15.0a0
tornado                       6.2
tqdm                          4.64.1
traitlets                     5.9.0
transformer-engine            0.5.0
transformers                  4.27.4
treelite                      3.0.1
treelite-runtime              3.0.1
triton                        2.0.0
typed-ast                     1.5.4
typer                         0.7.0
typing_extensions             4.4.0
uc-micro-py                   1.0.1
ucx-py                        22.12.0
uff                           0.6.9
urllib3                       1.26.13
uvicorn                       0.21.1
wandb                         0.14.1
wasabi                        1.1.1
wcwidth                       0.2.6
webdataset                    0.1.62
webencodings                  0.5.1
websockets                    11.0
Werkzeug                      2.2.2
wget                          3.2
wheel                         0.38.4
widgetsnbextension            4.0.7
wrapt                         1.15.0
xdoctest                      1.0.2
xgboost                       1.7.1
yarl                          1.8.2
youtokentome                  1.0.6
zict                          2.2.0
zipp                          3.11.0
WARNING: You are using pip version 21.2.4; however, version 23.2.1 is available.
You should consider upgrading via the '/usr/bin/python -m pip install --upgrade pip' command.
Singularity> 
```


```bash
Singularity> echo $CONDA_PREFIX

Singularity> which python3
/usr/bin/python3

Singularity> which nvcc --version
/usr/local/cuda/bin/nvcc

Singularity> nvcc --version
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2023 NVIDIA Corporation
Built on Fri_Jan__6_16:45:21_PST_2023
Cuda compilation tools, release 12.0, V12.0.140
Build cuda_12.0.r12.0/compiler.32267302_0
```

Download all
```bash
# Try to find path
Singularity> cat /home/bgnb2w2b/slurm-8095.out 
...
[I 21:34:33.365 LabApp] JupyterLab application directory is /usr/local/share/jupyter/lab
...
[I 21:34:33.377 LabApp] Serving notebooks from local directory: /workspace
...
```



*** 

Outside of the container, container can not access it
```bash
bgnb2w2b@curiosity:~$ cat /bootcamp_scripts/NLP/sbatch-llm
#!/bin/bash
#UPDATED 2023-07-30  JEREMYM
#SBATCH --partition=mig 
#SBATCH --ntasks=1
#SBATCH --nodes=1             # Max is 1
#SBATCH --gres=gpu:1          # Max is event specific
#SBATCH --cpus-per-gpu=4      # limit CPU cores 
#SBATCH --time=12:00:00

#getting the port and dgx node name
SERVER="`hostname`"
while
  PORT=$(shuf -n 1 -i 9000-9999)
  netstat -atun | grep -q "$PORT"
do
  continue
done

while
  GPT_PORT=$(shuf -n 1 -i 10000-11000)
  netstat -atun | grep -q "$GPT_PORT"
do
  continue
done

HASHNUMBER=$1
# Launch the Jupyter Notebook Server
rm -rf ~/port_forwarding_command
rm -rf ~/megatron_gpt_port
mkdir -p /bootcamp_workspaces/$USER
cp -n /bootcamp_cont/workspace-nlp-llm.tar /bootcamp_workspaces/$USER
cp -n /bootcamp_cont/nemo_llm_23_02.simg.tar /bootcamp_workspaces/$USER
cd /bootcamp_workspaces/$USER
tar --skip-old-files -xf workspace-nlp-llm.tar
tar --skip-old-files -xf nemo_llm_23_02.simg.tar
echo "ssh -L localhost:8888:${SERVER}:${PORT} ssh.axisapps.io -l ${HASHNUMBER} "> ~/port_forwarding_command
echo "Copy and paste to replace the Megatron-GPT client port in your notebook: ${GPT_PORT}"> ~/megatron_gpt_port
singularity run --nv --bind workspace-nlp-llm:/workspace nemo_llm_23_02.simg jupyter-lab --notebook-dir=/workspace --port=$PORT --ip=0.0.0.0 --no-browser --NotebookApp.token=""

```

Then, in the `/bootcamp_workspaces/$USER` folder
```bash
bgnb2w2b@curiosity:~$ ls -lth /bootcamp_workspaces/$USER
total 21G
-rw-r--r--  1 bgnb2w2b bgnb2w2b  21G Oct 10 21:22 nemo_llm_23_02.simg.tar
-rw-r--r--  1 bgnb2w2b bgnb2w2b 449M Oct 10 21:21 workspace-nlp-llm.tar
drwxrwxr-x  8 bgnb2w2b bgnb2w2b 4.0K Sep 27 01:51 workspace-nlp-llm
drwxr-xr-x 19 bgnb2w2b bgnb2w2b 4.0K Sep  6 08:44 nemo_llm_23_02.simg

```

***
Download zip and download the  workspace folder
```bash
# In Jupyter terminal
cd /home/bgnb2w2b/
Singularity> tar -zcvf workspace.tar.gz /workspace
tar: Removing leading `/' from member names
/workspace/
```

```bash
...
Singularity> ls
megatron_gpt_port  port_forwarding_command  slurm-8095.out  workspace.tar.gz
Singularity> mv workspace.tar.gz /workspace
```
Source: https://www.cyberciti.biz/faq/how-do-i-compress-a-whole-linux-or-unix-directory/
Now download from the Jupyter file browser
Downloaded to the location: `/Users/marufahmed/Downloads/workspace.tar.gz`

***
### Connect ot FileZilla (Worked)
```
https://axis-raplabhackathon.axisportal.io/apps
Click Bright 
Connect to Bright with One-Time Access Key --> Use a desktop SSH client --> One time access key for Secure Shell   

--> Connect with terminal
In your Terminal, paste the following SSH command:
ssh ssh.axisapps.io  -l bc0fd5f1e7a940368d53304491abafb2

--> Connect with SSH client
In your SSH client, fill in the following fields:
Hostname:
ssh.axisapps.io
Port:
22
Auto-login username:
2eb3fe8825644410910d98f6bee12c11

# On FileZilla fill above three fields
# Connect, and goto
/bootcamp_workspaces/bgnb2w2b
```

SSH man
```bash
ssh - OpenSSH SSH client (remote login program)
...
-l login_name 
Specifies the user to log in as on the remote machine. This also may be specified on a per-host basis in the configuration file.
```

Try rsync (Neither Work)
```bash
#rsync -v -a bc0fd5f1e7a940368d53304491abafb2@rssh.axisapps.io:/home/bgnb2w2b/slurm-8095.out /scratch/fp0/mah900/Australia_LLM_Bootcamp
#rsync -v -a 2eb3fe8825644410910d98f6bee12c11@rssh.axisapps.io:/home/bgnb2w2b/slurm-8095.out /scratch/fp0/mah900/Australia_LLM_Bootcamp
```
Source: https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories
Try SCP (None Work)
```bash
#scp -v bc0fd5f1e7a940368d53304491abafb2@rssh.axisapps.io:/home/bgnb2w2b/slurm-8095.out /scratch/fp0/mah900/Australia_LLM_Bootcamp
#scp -v 2eb3fe8825644410910d98f6bee12c11@rssh.axisapps.io:/home/bgnb2w2b/slurm-8095.out /scratch/fp0/mah900/Australia_LLM_Bootcamp
#scp -v bgnb2w2b@rssh.axisapps.io:/home/bgnb2w2b/slurm-8095.out /scratch/fp0/mah900/Australia_LLM_Bootcamp
```
Source: https://haydenjames.io/linux-securely-copy-files-using-scp/

Try shared connection/Control master (Not worked)
```bash
# Not work
ssh -f -N -M -S "$SSH_CONTROL_SOCKET" ssh.axisapps.io  -l bc0fd5f1e7a940368d53304491abafb2
muxserver_listen: link mux listener .OVSydlswHCsAJHP7 => : No such file or directory
```
Source: https://superuser.com/questions/1299180/cannot-scp-over-a-shared-connection

### Next Try,  Master Connection (Working)
```bash
# 
# 1. Open the master connection
SSHSOCKET=~/.ssh/myUsername@rssh.axisapps.io
ssh -M -f -N -o ControlPath=$SSHSOCKET ssh.axisapps.io  -l bc0fd5f1e7a940368d53304491abafb2

# -M instructs SSH to become the master, i.e. to create a master socket that will be used by the slave connections
# -f makes SSH to go into the background after the authentication
# -N tells SSH not to execute any command or to expect an input from the user; that's good because we want it only to manage and keep open the master connection and nothing else
# -o ControlPath=$SSHSOCKET - this defines the name to be used for the socket that represents the master connection; the slaves will use the same value to connect via it
# Thanks to -N and -f the SSH master connection will get out of the way but will stay open and such usable by subsequent ssh/scp invocations.

# 2. Open and close other connections without re-authenticating as you like
ssh -o ControlPath=$SSHSOCKET ssh.axisapps.io  -l bc0fd5f1e7a940368d53304491abafb2 "ls"

scp -o ControlPath=$SSHSOCKET ssh.axisapps.io:/home/bgnb2w2b/slurm-8095.out /scratch/fp0/mah900/Australia_LLM_Bootcamp  

# 3. Close the master connection
# And it's better to close it properly beause otherwise the socket file would not be deleted and would prevent the master connection from opening in the future.
ssh -S $SSHSOCKET -O exit ssh.axisapps.io

# This time time we use -S instead of  "-o ControlPath=" because we intend to use the socket for controlling the master SSH instance
# -O <command name> is used to send a command to the master SSH instance; the commands allowed are "exit" and "check"
ssh -S $SSHSOCKET -O check ssh.axisapps.io
```
Source: https://blog.jakubholy.net/2010/09/10/ssh-magic-authorize-only-once-for-multiple-sshscp-invocations/


### Try a New Container (Working)
```
91cb652cb482472daeb128e979b962d4
sbatch /bootcamp_scripts/NLP/sbatch-llm 91cb652cb482472daeb128e979b962d4
cat port_forwarding_command 
ssh -L localhost:8888:dgx07:9535 ssh.axisapps.io -l 91cb652cb482472daeb128e979b962d4 
```




### 2023.10.12

 list files from `https://axis-raplabhackathon.axisportal.io/SshClient`
```bash
# https://axis-raplabhackathon.axisportal.io/SshClient
bgnb2w2b@curiosity:~$ ls -lh /bootcamp_workspaces/$USER
total 21G
drwxr-xr-x 19 bgnb2w2b bgnb2w2b 4.0K Sep  6 08:44 nemo_llm_23_02.simg
-rw-r--r--  1 bgnb2w2b bgnb2w2b  21G Oct 10 21:22 nemo_llm_23_02.simg.tar
drwxrwxr-x  9 bgnb2w2b bgnb2w2b 4.0K Oct 11 20:41 workspace-nlp-llm
-rw-r--r--  1 bgnb2w2b bgnb2w2b 449M Oct 10 21:21 workspace-nlp-llm.tar
```

### Try download singularity file and data folder (Working)
```bash
qsub -I /home/900/mah900/conda/cqp_h2.pbs
qsub: job 97820118.gadi-pbs ready

$ SSHSOCKET=~/.ssh/bgnb2w2b@curiosity
$ ssh -M -f -N -o ControlPath=$SSHSOCKET ssh.axisapps.io  -l 8f01accfe9b84e56aa9d612abe13cad4
$ ssh -S $SSHSOCKET -O check ssh.axisapps.io
Master running (pid=819403)

# /bootcamp_workspaces/bgnb2w2b/nemo_llm_23_02.simg.tar
$ scp -o ControlPath=$SSHSOCKET ssh.axisapps.io:/bootcamp_workspaces/bgnb2w2b/nemo_llm_23_02.simg.tar /scratch/fp0/mah900/Australia_LLM_Bootcamp  
nemo_llm_23_02.simg.tar                                                                                       100%   20GB   9.1MB/s   38:09 
# /bootcamp_workspaces/bgnb2w2b/workspace-nlp-llm.tar
$ scp -o ControlPath=$SSHSOCKET ssh.axisapps.io:/bootcamp_workspaces/bgnb2w2b/workspace-nlp-llm.tar /scratch/fp0/mah900/Australia_LLM_Bootcamp  
workspace-nlp-llm.tar                                                                                         100%  448MB   8.9MB/s   00:50

ssh -S $SSHSOCKET -O exit ssh.axisapps.io
```

Try, re-establish after  connection was closed before ssh exit (Working)
```bash
SSHSOCKET=~/.ssh/bgnb2w2b@curiosity
ssh -M -f -N -o ControlPath=$SSHSOCKET ssh.axisapps.io  -l 8f01accfe9b84e56aa9d612abe13cad4
# Error
ControlSocket /home/900/mah900/.ssh/bgnb2w2b@curiosity already exists, disabling multiplexing

# Try
$ rm /home/900/mah900/.ssh/bgnb2w2b@curiosity
$ ssh -M -f -N -o ControlPath=$SSHSOCKET ssh.axisapps.io  -l 8f01accfe9b84e56aa9d612abe13cad4
$ ssh -S $SSHSOCKET -O check ssh.axisapps.io
Master running (pid=3125371)
$ ssh -S $SSHSOCKET -O exit ssh.axisapps.io
Exit request sent.
```


### Try Running
```bash
cd /scratch/fp0/mah900/Australia_LLM_Bootcamp
# Try running the singularity 
tar --skip-old-files -xf workspace-nlp-llm.tar
tar --skip-old-files -xf nemo_llm_23_02.simg.tar

module load singularity 
SERVER="`hostname`"
PORT=$(shuf -n 1 -i 9000-9999)
singularity run --nv --bind workspace-nlp-llm:/workspace nemo_llm_23_02.simg jupyter-lab --notebook-dir=/workspace --port=$PORT --ip=0.0.0.0 --no-browser --NotebookApp.token=""
FATAL:   configuration disallows users from running sandbox containers

```
Error
```bash
FATAL:   configuration disallows users from running sandbox containers
```

Next plan, look for the singularity recipe or use inspect to see the installed programs
Try `inspect` (Not much, but version: `nvcr.io/nvidia/nemo:23.02` )
```bash
cd /scratch/fp0/mah900/Australia_LLM_Bootcamp
module load singularity 
$ singularity inspect nemo_llm_23_02.simg
Author: Tosin
org.label-schema.build-arch: amd64
org.label-schema.build-date: Wednesday_6_September_2023_8:44:43_PDT
org.label-schema.schema-version: 1.0
org.label-schema.usage.singularity.deffile.bootstrap: docker
org.label-schema.usage.singularity.deffile.from: nvcr.io/nvidia/nemo:23.02
org.label-schema.usage.singularity.version: 3.8.5
```

Try `Curiosity`  cluster files
```
# Doesn't work
sudo su
[sudo] password for bgnb2w2b:

ls /bootcamp_cont/
ls /bootcamp_sc
```



### 2023.10.13

```bash
bgnb2w2b@curiosity:~$ cat  /bootcamp_scripts/nemo_guardrails/nemo_sbatch
#!/bin/bash
#UPDATED 2023-08-17  Mozhgank
#SBATCH --partition=mig
#SBATCH --ntasks=1
#SBATCH --nodes=1             # Max is 1
#SBATCH --gres=gpu:1          # Max is event specific
##SBATCH --cpus-per-gpu=4     # limit CPU cores
#SBATCH --time=8:00:00

#getting the port and dgx node name
SERVER="`hostname`"
#SNL=$SLURM_NODELIST
#SJG=$SLURM_JOB_GPUS
#CVD=$CUDA_VISIBLE_DEVICES

#A=$(id -u)
#PORT=$(( 8000 +  $A  ))

while
  PORT=$(shuf -n 1 -i 9000-9999)
  netstat -atun | grep -q "$PORT"
do
  continue
done

while
  PORT2=$(shuf -n 1 -i 10200-11200)
  netstat -atun | grep -q "$PORT2"
do
  continue
done

HASHNUMBER=$1
# Launch the Jupyter Notebook Server
rm -rf ~/port_forwarding_command
rm -rf ~/port_forwarding_command_jupyter
mkdir -p /bootcamp_workspaces/$USER

echo "Copying the Singularity SandBox"
#cp -n /bootcamp_cont/nemog/NeMo-Guardrails.tar /bootcamp_workspaces/$USER
cp -n /bootcamp_cont/nemog/nemo_sandbox.simg.tar /bootcamp_workspaces/$USER
cd /bootcamp_workspaces/$USER


echo "Writing archived files from the SandBox"
#tar --skip-old-files -xf NeMo-Guardrails.tar
tar --skip-old-files -xf nemo_sandbox.simg.tar


echo "Writing ports to output file"
echo "ssh ssh.axisapps.io -L localhost:8888:${SERVER}:${PORT} -L localhost:8889:${SERVER}:${PORT2} -l ${HASHNUMBER}"> ~/port_forwarding_command

#singularity run --nv --bind NeMo-Guardrails:/workspace nemo_sandbox.simg  jupyter-lab --notebook-dir=/workspace --port=$PORT --ip=0.0.0.0 --no-browser --NotebookApp.token=""

#singularity run --nv nemo_sandbox.simg  jupyter-lab --notebook-dir=/workspace --port=$PORT --ip=0.0.0.0 --no-browser --NotebookApp.token=""

singularity run --nv --bind /bootcamp_workspaces/$USER/nemo_sandbox.simg/workspace:/workspace nemo_sandbox.simg  jupyter-lab --notebook-dir=/workspace --port=$PORT --ip=0.0.0.0 --no-browser --NotebookApp.token=""

#nemoguardrails server --port=$PORT
bgnb2w2b@curiosity:~$ 

```