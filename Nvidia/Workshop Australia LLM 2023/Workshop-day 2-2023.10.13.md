
  
### Manjot Singh  [3:23 AM](https://australiallmbootcamp.slack.com/archives/C05UZ1TJMHQ/p1697127810523719)  
@channel Hello everyone, please find all recordings of the event here: [https://drive.google.com/drive/folders/1iYVB0hM8dARSzn2BqBfnhZB75_J_4IYi?usp=sharing](https://drive.google.com/drive/folders/1iYVB0hM8dARSzn2BqBfnhZB75_J_4IYi?usp=sharing)

### Session: 9.05
Attendence: 38-39

Todays commands 
`https://auth.axissecurity.com/idp/3fdee141d2974d2da2a30220466e8e27/?RelayState=2e674eb3877548df9dbf5b4d38dfa4f1`
```bash

Different commands today
Two ports forwarding one for jupyter and one guradrail

d712eb6f981a46a6a796607f74159fc8

sbatch /bootcamp_scripts/nemo_guardrails/nemo_sbatch d712eb6f981a46a6a796607f74159fc8

cat port_forwarding_command 
ssh ssh.axisapps.io -L localhost:8888:dgx07:9034 -L localhost:8889:dgx07:10513 -l d712eb6f981a46a6a796607f74159fc8

Need a API key for the Guradrail
Given in the breakout room
Guradrail local port 8889

export OPENAI_API_KEY=‘{add your key}’
cd /workspace/NeMo-Guardrails/ && nemoguardrails server --port=<your port number>

```

Why are we doing this
Guardrail
uneithical /ethical answers

Hallucination 

Toxicity 
Bias

Guardrail is Still alpha 

jail break 

Cona

### Breakout room: 9:25
George Clark TA

Each room will have a own key
(waiting for copy to enable in the Zoom chat)
```
export OPENAI_API_KEY=‘{add your key}’
cd /workspace/NeMo-Guardrails/ && nemoguardrails server --port=10513

 
```

### Session: 10.00

LLM --> Colang

Three ways to run Guardrail
web
cli
Hugging face Dali

Python

10.40: 15 min break

### Session: 10.55

KIll the guard-rail 
and run a new command 

Can use web, but cli gives debug info. 

### Breakout room: 11.17

Johan Barthelemy (NVIDIA)  [11:14 AM](https://australiallmbootcamp.slack.com/archives/C05UJFHBPF1/p1697156095317399)  
nemoguardrails chat --config=/workspace/NeMo-Guardrails/examples/topical_rail/ --verbose
[11:16](https://australiallmbootcamp.slack.com/archives/C05UJFHBPF1/p1697156210190359)
[https://github.com/NVIDIA/NeMo-Guardrails/blob/main/docs/user_guide/colang-language-syntax-guide.md](https://github.com/NVIDIA/NeMo-Guardrails/blob/main/docs/user_guide/colang-language-syntax-guide.md)


```bash
# Try
nemoguardrails chat --config=/workspace/NeMo-Guardrails/examples/topical_rail/ --verbose
...
__root__
  Did not find openai_api_key, please add an environment variable `OPENAI_API_KEY` which contains it, or pass  `openai_api_key` as a named parameter. (type=value_error)

# Try again  
#export OPENAI_API_KEY='sk-c9IfHzTa9HJAAIksUoaT3BlbkFjplZt56II88zATppgz1pt' # key error
nemoguardrails chat --config=/workspace/NeMo-Guardrails/examples/topical_rail/ --verbose
...
 ERROR:nemoguardrails.actions.action_dispatcher:Error Incorrect API key provided: sk-c9IfH**************************************z1pt. You can find your API key at https://platform.openai.com/account/api-keys. while execution generate_user_intent

# Next Try (Workibg)
export OPENAI_API_KEY='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'
nemoguardrails chat --config=/workspace/NeMo-Guardrails/examples/topical_rail/ --verbose


```
Ends at 11:50

### Session: 11.50

Colang Model - Config

LLMChains 

Prompt engineering 

Grounding rail

Groundrail demo videos 

Break  between 12:30 and 1:30

kill the chat bot and new one

Examples to work in the lab

.../exmples/grounding_rail/README.md

.../examples/sample_rails/

Challenge talk and lanuch both in 1.30

### Breakout room: 12.33 pm

Manjot Singh  [12:31 PM](https://australiallmbootcamp.slack.com/archives/C05UZ1TJMHQ/p1697160702545699)  
@channel Have a great lunch, see you all in one hour at 1:30 PM AEDT!


### Session: 1.30
Breakout room stopped
Also, already answering someone

First kill your existing jobs 
Use yesterday's command 
Start --> Challenge notebook
Look at the previous notebooks if require info. 
It is quad dataset not squid

Object is not to test, but to test out the application techniques that are discussed in the workshop.
***
Johan Barthelemy (NVIDIA)  [1:26 PM](https://australiallmbootcamp.slack.com/archives/C05VBMMTRDX/p1697164011334759)  
@channel Before doing the challenge, please remember to kill your previous using `scancel <job id>`
[1:27](https://australiallmbootcamp.slack.com/archives/C05VBMMTRDX/p1697164053578469)
Then you can use `sbatch /bootcamp_scripts/NLP/sbatch-llm <hash-code>` and go to [https://localhost:8888](https://localhost:8888/) and start the Challenge notebook
***
Barat:
Cluster will be available till Sunday.
Will be killed in Monday. other events will start on Monday
Links to github will be posted soon.
But, API keys you have to be done yourself
Johan:
Install is easy for Nemo and Gurad rail, possible with pip and NCG.

Breakout rooms open until 4:00

### Breakout room: 1.45
New container run 
```
e51c7e020f0042beaafdf7219f734db9
sbatch /bootcamp_scripts/NLP/sbatch-llm e51c7e020f0042beaafdf7219f734db9
ssh -L localhost:8888:dgx07:9750 ssh.axisapps.io -l e51c7e020f0042beaafdf7219f734db9 
```

Switched to room#4
Q: Multi-dimensional time series
Johan: Modulus has physics integrated.
Thermodynamic, and other simulations. 
Q: Doing Climate with Neural network, Postdoc in Princeton, not busy with publishing works. Mykhailo Rudko
Johan: https://www.nvidia.com/en-us/launchpad/ai/physics-informed-machine-learning-with-modulus/

Q: Any Knowlede extraction framework?
Johan: Nvidia is working with it. 
Now, use gathers and knowledge base. 
Outside Nemo, Guradrail you can use ...

Rika: material scientist, but not have experimental data. 
A Student is going through Journals corpus for data collection. 
What about Kaggle and Hugging-face. 
US govt released Covid papers, and Rika tried to see if anything can be done with that data. 
Rika: https://www.nature.com/articles/s41524-022-00784-w

Q: Modulus require multi GPU?
Johan: No, can be used on laptops 

Johan:  Solution will be shown at the end


### Session: 3.36
Attendec:18
One of the solutions

P tuning model config is set

```
######################### fill in the values ########################### 
config.model.task_templates = [
    {
        "taskname": "cuad",
        "prompt_template": "<|VIR|>",
        "total_virtual_tokens": 15,
        "virtual_token_splits": [15],
        "truncate_field": "context",
        "answer_only_loss": True,
        "answer_field": "answer",
    },
]
#######################################################################

```

Will be put in slack

```
mymodel = nemo_nlp.models.language_modeling.megatron_gpt_prompt_learning_model.MegatronGPTPromptLearningModel.restore_from(restore_path='/workspace/jupyter_notebook/nemo/nemo_experiments/p_tuning/2023-09-13_07-50-29/checkpoints/p_tuning.nemo',trainer=trainer)
```

Github repo will be posted soon. 

***

Johan Barthelemy (NVIDIA)  [3:58 PM](https://australiallmbootcamp.slack.com/archives/C05UZ1TJMHQ/p1697173135336639)  
[https://github.com/NVIDIA/NeMo-Guardrails/tree/main](https://github.com/NVIDIA/NeMo-Guardrails/tree/main)

Brad Stratton  [4:03 PM](https://australiallmbootcamp.slack.com/archives/C05UZ1TJMHQ/p1697173435773759)  
Hi everyone thanks for joining the bootcamp! The Open AI API keys for the guardrails testing will be deactivated at the end of today. If you wish to continue experimenting with guardrails you'll need to use your own personal Open AI API key. Have a good weekend :)


Note: Will try later this week, or later, first have to see the ai-models.



***
### 2023.10.17

Manjot Singh  [4:22 AM](https://australiallmbootcamp.slack.com/archives/C05UZ1TJMHQ/p1697476979980119)  
**@channel** Hello everyone, please find all recordings of the event here: [https://drive.google.com/drive/folders/1iYVB0hM8dARSzn2BqBfnhZB75_J_4IYi?usp=sharing](https://drive.google.com/drive/folders/1iYVB0hM8dARSzn2BqBfnhZB75_J_4IYi?usp=sharing)











