

NVIDIA cuOpt is an accelerated Operations Research optimization API to help developers create complex, real-time fleet routing workflows on NVIDIA GPUs. This repository contains a collection of resources demonstrating use of NVIDIA cuOpt via Python SDK and service APIs.

The cuOpt-Resources repository is under [MIT License](https://github.com/NVIDIA/cuOpt-Resources/blob/branch-22.12/LICENSE.md)

Use of NVIDIA cuOpt is subject to the [End User License Agreement](https://docs.nvidia.com/cuopt/NVIDIA_cuOpt_EULA.pdf)

## [](https://github.com/NVIDIA/cuOpt-Resources#nvidia-cuopt)[NVIDIA cuOpt](https://developer.nvidia.com/cuopt-logistics-optimization)

cuOpt uses highly optimized GPU-accelerated solvers relying on heuristics, metaheuristics, and optimization. In addition to providing dramatically accelerated, world class solutions to some of the most difficult optimization problems, NVIDIA cuOpt prioritizes ease of use through high level Python SDK and service APIs

[cuOpt Docs](https://docs.nvidia.com/cuopt/overview.html)

## [](https://github.com/NVIDIA/cuOpt-Resources#contents)


https://github.com/NVIDIA/cuOpt-Resources