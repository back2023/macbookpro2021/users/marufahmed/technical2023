

https://github.com/maciejzj/cnn-res-degrader


# CNN resolution degrader

[](https://github.com/maciejzj/cnn-res-degrader#cnn-resolution-degrader)

Convolutional neural networks for downscaling aerial images. Trained on a set of real-life high and low-resolution images from the Proba-V satellite mission. Created as a part of data augmentation and generation for super-resolution project.

This is part of a research project published at:

M. Ziaja, J. Nalepa and M. Kawulok, "Data Augmentation for Multi-Image Super-Resolution," IGARSS 2022 - 2022 IEEE International Geoscience and Remote Sensing Symposium, Kuala Lumpur, Malaysia, 2022, pp. 119-122, [doi: 10.1109/IGARSS46834.2022.9884609](https://ieeexplore.ieee.org/document/9884609).


