


## About

Coarse-to-Fine CNN for Image Super-Resolution (IEEE Transactions on Multimedia,2021)


## Coarse-to-Fine CNN for Image Super-Resolution（CFSRCNN）is conducted by Chunwei Tian, Yong Xu, Wangmeng Zuo, Bob Zhang, Lunke Fei and Chia-Wen Lin and is accpted by IEEE Transactions on Multimedia (IF:8.182), 2020. It is implemented by Pytorch. And this work is reported by 52CV at [https://mp.weixin.qq.com/s/N5SXUFmqYMN0XQJbVvBPEg](https://mp.weixin.qq.com/s/N5SXUFmqYMN0XQJbVvBPEg). Besides, it was also reported by CCF MM at [https://mp.weixin.qq.com/s/-rUmRDFbPvKMjU0EIYV3Rg](https://mp.weixin.qq.com/s/-rUmRDFbPvKMjU0EIYV3Rg). It is a ESI highly-cited paper and a homepage paper of the IEEE TMM. It is an excellent paper for Shzhen in 2022. It is collected by the OSCS.

[](https://github.com/hellloxiaotian/CFSRCNN#coarse-to-fine-cnn-for-image-super-resolutioncfsrcnnis-conducted-by-chunwei-tian-yong-xu-wangmeng-zuo-bob-zhang-lunke-fei-and-chia-wen-lin-and-is-accpted-by-ieee-transactions-on-multimedia-if8182-2020-it-is-implemented-by-pytorch-and-this-work-is-reported-by-52cv-at-httpsmpweixinqqcomsn5sxufmqymn0xqjbvvbpeg-besides-it-was-also-reported-by-ccf-mm-at-httpsmpweixinqqcoms-rumrdfbpvkmju0eiyv3rg-it-is-a-esi-highly-cited-paper-and-a-homepage-paper-of-the-ieee-tmm-it-is-an-excellent-paper-for-shzhen-in-2022-it-is-collected-by-the-oscs)

## This paper uses high- and low-frequency features to enhance the stability of image super-resolution model.

