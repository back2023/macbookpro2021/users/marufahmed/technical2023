

https://medium.com/pangeo/climpred-verification-of-weather-and-climate-forecasts-dd88333942d9


`climpred`, a package for weather and climate forecast verification, recently joined the `pangeo` community. Here, we introduce `climpred` to the pangeo community and encourage open source contributions. You can find our documentation [here](https://climpred.readthedocs.io/en/stable/).

