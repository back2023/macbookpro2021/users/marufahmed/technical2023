

https://earth.bsc.es/gitlab/external/c3s512-seasonal-forecast-eqc-ecmwf


# Overview

A specific python-based software tool has been developed for the verification of the different seasonal forecast systems available in the Climate Data Store (CDS). This tool provides a complete set of functionalities ranging from direct downloading of selected datasets from the CDS to the computation and plotting of the specified metrics. All these functionalities are integrated in to a workflow that automatically identifies what operations are required and which ones can be skipped. It runs starting from a configuration file, which allows a high degree of parametrization, including all required dataset download parameters, computation specifications (e.g. chunks sizes on selected dimensions, number of cores, maximum allowed memory usage...), metrics parameters and plenty of plotting details. The tool has been developed in a modular fashion to allow easy debugging, modification and extension.

The software uses the xarray package to facilitate multidimensional array manipulation and it allows efficient parallel computing, by integrating the Dask library. For some of the verification metrics (fRPSS and fCRPSS), the R package SpecsVerification is used, which is a specific library for seasonal forecast verification, also used in the preoperational contract C3S_51_Lot3. This R package is wrapped using the python R2Py package.

An extended documentation including the description of all the parameters that can be set in the `conf.yaml` and other details can be found [here](https://earth.bsc.es/gitlab/external/c3s512-seasonal-forecast-eqc/-/blob/s-prototype/C3S512_sf-prototype_documentation.pdf). A detail description of each of the verification metrics can be found in [C3S512_Seasonal_Forecast_verification_metrics_general_description.pdf](https://earth.bsc.es/gitlab/external/c3s512-seasonal-forecast-eqc/-/blob/s-prototype/). It is recommended to have a look at these documents

