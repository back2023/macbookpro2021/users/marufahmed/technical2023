

https://lightning.ai/docs/torchmetrics/stable/


TorchMetrics is a collection of 100+ PyTorch metrics implementations and an easy-to-use API to create custom metrics. It offers:

- A standardized interface to increase reproducibility
- Reduces Boilerplate
- Distributed-training compatible
- Rigorously tested
- Automatic accumulation over batches
- Automatic synchronization between multiple devices
    

You can use TorchMetrics in any PyTorch model, or within [PyTorch Lightning](https://lightning.ai/docs/pytorch/stable/) to enjoy the following additional benefits:

- Your data will always be placed on the same device as your metrics
- You can log [`Metric`](https://lightning.ai/docs/torchmetrics/stable/references/metric.html#torchmetrics.Metric "torchmetrics.Metric") objects directly in Lightning to reduce even more boilerplate

