
https://docs.nvidia.com/deeplearning/modulus/modulus-core/api/modulus.metrics.html


## [Basics](https://docs.nvidia.com/deeplearning/modulus/modulus-core/api/modulus.metrics.html#basics)

Modulus provides several general and domain-specific metric calculations you can leverage in your custom training and inference workflows. These metrics are optimized to operate on PyTorch tensors.


