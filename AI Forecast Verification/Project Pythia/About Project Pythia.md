
https://projectpythia.org/about.html


# About Project Pythia[](https://projectpythia.org/about.html#about-project-pythia "Link to this heading")

Project Pythia is the education working group for [Pangeo](https://pangeo.io/) and is an educational resource for the entire geoscience community. Project Pythia is a home for Python-centered learning resources that are _open-source_, _community-owned_, _geoscience-focused_, and _high-quality_.

## Why do we need Project Pythia?[](https://projectpythia.org/about.html#why-do-we-need-project-pythia "Link to this heading")

Scientists working in a multitude of disciplines rely heavily on computing technologies for their research. Numerical simulations run on supercomputers are used in the study of climate, weather, atmospheric chemistry, wildfires, space weather, and more. Similarly, a tremendous volume of digital data produced by numerical simulations, or observations made with instruments, are analyzed with the help of powerful computers and software. Thus, today’s scientists require not only expertise in their scientific discipline, but also require high-level technical skills to effectively analyze, manipulate, and make sense of potentially vast volumes of data. Computing environments change rapidly, and two technologies that have emerged and are being adopted by scientific communities relatively recently are Cloud Computing platforms and a software ecosystem of scientific tools built around the open source programming language called Python. Project Pythia provides a public, web-accessible training resource that will help educate current, and aspiring, earth scientists to more effectively use both the Scientific Python Ecosystem and Cloud Computing to make sense of huge volumes of numerical scientific data.

