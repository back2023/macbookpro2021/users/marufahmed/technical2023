

https://xclim.readthedocs.io/en/stable/explanation.html#why-use-xclim



## Other Python projects similar to xclim[](https://xclim.readthedocs.io/en/stable/explanation.html#other-python-projects-similar-to-xclim "Link to this heading")

xclim has been developed within an ecosystem of several existing projects that deal with climate and statistical correction/downscaling and has both influenced and been influenced by their approaches:

- icclim ([icclim Source Code](https://github.com/cerfacs-globc/icclim); [icclim Documentation](https://icclim.readthedocs.io/en/stable/index.html))
    - xclim aimed to reimplement icclim using xarray-natives for the computation of climate indices. Starting from version 5.0 of icclim, xclim has become a core dependency for this project.
    - The icclim developers have prepared a documentation page comparing xclim and icclim ([xclim_and_icclim](https://icclim.readthedocs.io/en/stable/explanation/xclim_and_icclim.html)).
        
- climate_indices ([climate_indices Source Code](https://github.com/monocongo/climate_indices); [climate_indices Documentation](https://climate-indices.readthedocs.io/en/latest/index.html))
    - Provides several moisture- and drought-related indicators not implemented at-present in xclim (SPI, SPEI, PDSI, etc.). It also offers a robust command-line interface and uses xarray in its backend.
    - There is currently an ongoing discussion about the merging of climate_indices and xclim: [GH/1273](https://github.com/Ouranosinc/xclim/issues/1273).
        
- MetPy ([MetPy Source Code](https://github.com/Unidata/MetPy); [MetPy Documentation](https://unidata.github.io/MetPy/latest/index.html))
    - MetPy is built for reading, visualizing, and performing calculations specifically on standards-compliant, operational weather data. Like xclim, it makes use of xarray.
    - xclim adopted its standards and unit-handling approaches from MetPy and associated project cf-xarray.
        
- climpred ([climpred Source Code](https://github.com/pangeo-data/climpred); [climpred Documentation](https://climpred.readthedocs.io/en/stable/index.html))
    - climpred is designed to analyze and validate weather and climate forecast data against observations, reconstructions, and simulations. Similar to xclim, it leverages xarray, dask, and cf_xarray for object handling, distributed computation, and metadata validation, respectively    
- pyet ([pyet Source Code](https://github.com/pyet-org/pyet); [pyet Documentation](https://pyet.readthedocs.io/en/latest/))
    - pyet is a tool for calculating/estimating evapotranspiration using many different accepted methodologies and employs a similar design approach as xclim, based on xarray-natives.
    
- xcdat ([xcdat Source Code](https://github.com/xCDAT/xcdat); [xcdat Documentation](https://xcdat.readthedocs.io/en/latest/))
    
- GeoCAT ([GeoCAT Documentation](https://geocat.ucar.edu/))
    - GeoCAT is an ensemble of tools developed specifically for scalable data analysis and visualization of structures and unstructured gridded earth science datasets. GeoCAT tools rely on many of the same tools that xclim uses in its stack (notably, xarray, dask, and jupyter notebooks).
        
- scikit-downscale ([scikit-downscale Source Code](https://github.com/pangeo-data/scikit-downscale), [scikit-downscale Documentation](https://scikit-downscale.readthedocs.io/en/latest/))
    - scikit-downscale offers algorithms for statistical downscaling. xclim drew inspiration from its fit/predict architecture API approach. The suite of downscaling algorithms offered between both projects differs.