

https://nvidia.github.io/earth2studio/modules/generated/statistics/earth2studio.statistics.acc.html


Statistic for calculating the anomaly correlation coefficient of two tensors over a set of given dimensions, with respect to some optional climatology.

