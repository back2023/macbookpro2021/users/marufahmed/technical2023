
https://pypi.org/project/verif/

Released: Mar 11, 2024

Verif is a command-line tool that lets you verify the quality of weather forecasts for point locations. It can also compare forecasts from different forecasting systems (that have different models, post-processing methods, etc).

The program reads files with observations and forecasts in a specific format (see “Input files” below). The input files contain information about dates, forecast lead times, and locations such that statistics can be aggregated across different dimensions. To ensure a fair comparison among files, Verif will discard data points where one or more forecast systems have missing forecasts. Since Verif is a command-line tool, it can be used in scripts to automatically create verification figures.

Verif version 1.2 has been released (see “Installation Instruction” below). We welcome suggestions for improvements. Verif is developed by Thomas Nipen ([thomasn@met.no](mailto:thomasn%40met.no)), with contributions from many.

