

https://sites.baylor.edu/lourenco_paz/2021/12/25/an-introduction-to-statistical-learning-with-applications-in-python/

I came across this [very interesting Github repository](https://github.com/qx0731/Sharing_ISL_python) by Qiuping X., in which she posted the codes she prepared in Python for the book “An Introduction to Statistical Learning with Applications in R”  by Gareth James, Daniela Witten,  Trevor Hastie, and Robert Tibshirani.  This is very useful for those that are learning Python and certainly facilitates the migration from R to Python too.



