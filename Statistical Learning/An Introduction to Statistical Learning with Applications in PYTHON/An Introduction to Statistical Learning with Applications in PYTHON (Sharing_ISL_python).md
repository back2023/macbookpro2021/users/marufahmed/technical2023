
https://github.com/qx0731/Sharing_ISL_python

I love the book << An Introduction to Statistical Learning with Applications in R>> by Gareth James • Daniela Witten • Trevor Hastie and Robert Tibshirani. This book has been super helpful for me.

In this repository, I have implemented the same/similar functionality in Python. The code is in a script format to show the thought process. Hope this could help this book reach more broad audience. _Don't let the language barriers stop you from exploring something fun and useful._

Please refer [https://www.statlearning.com/](https://www.statlearning.com/) for more details. In 2nd Edition, the authors introduced a R library ISLR2 for all the dataset used in the book.

### [](https://github.com/qx0731/Sharing_ISL_python#setup-for-this-repository)