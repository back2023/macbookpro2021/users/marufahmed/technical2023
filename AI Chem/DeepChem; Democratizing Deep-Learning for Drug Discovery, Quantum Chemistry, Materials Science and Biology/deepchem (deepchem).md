

https://github.com/deepchem/deepchem/tree/master


## About

Democratizing Deep-Learning for Drug Discovery, Quantum Chemistry, Materials Science and Biology


DeepChem aims to provide a high quality open-source toolchain that democratizes the use of deep-learning in drug discovery, materials science, quantum chemistry, and biology.


