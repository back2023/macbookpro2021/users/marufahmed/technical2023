
https://github.com/sksq96/pytorch-summary


## Use the new and updated [torchinfo](https://github.com/TylerYep/torchinfo).

## Keras style `model.summary()` in PyTorch

Keras has a neat API to view the visualization of the model which is very helpful while debugging your network. Here is a barebone code to try and mimic the same in PyTorch. The aim is to provide information complementary to, what is not provided by `print(your_model)` in PyTorch.

