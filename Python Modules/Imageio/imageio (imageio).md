
https://github.com/imageio/imageio


Website: [https://imageio.readthedocs.io/](https://imageio.readthedocs.io/)

Imageio is a Python library that provides an easy interface to read and write a wide range of image data, including animated images, video, volumetric data, and scientific formats. It is cross-platform, runs on Python 3.8+, and is easy to install.

Professional support is available via [Tidelift](https://tidelift.com/funding/github/pypi/imageio).
 
