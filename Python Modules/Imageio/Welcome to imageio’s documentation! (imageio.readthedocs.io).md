

https://imageio.readthedocs.io/en/stable/


Imageio is a Python library that provides an easy interface to read and write a wide range of image data, including animated images, volumetric data, and scientific formats. It is cross-platform, runs on Python 3.5+, and is easy to install.

Main website: [https://imageio.readthedocs.io/](https://imageio.readthedocs.io/)


