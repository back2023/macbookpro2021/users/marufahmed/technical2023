

https://stackoverflow.com/questions/45527889/how-do-i-use-f-string-with-regex-in-python



```python
rmonth = 'a'
regex = fr'(\d{1,2})/(\d{1,2})/(\d{4}|\d{2})'
date_found = re.findall(regex, lines)
```

The new fstrings in Python interpret brackets in their own way. You can escape brackets you want to see in the output by doubling them:

```python
regex = fr'(\d{{1,2}})/(\d{{1,2}})/(\d{{4}}|\d{{2}})'
```



 