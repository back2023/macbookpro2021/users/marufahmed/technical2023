


https://stackoverflow.com/questions/68669915/create-dask-local-cluster-and-share-it-among-different-jupiter-notebook



The easiest thing to do is to write the scheduler file that contains connection information:

```python
from dask.distributed import Client
client = Client(memory_limit='12GB')
client.write_scheduler_file("dask_scheduler.json")
```

In a different notebook, you would use:

```python
from dask.distributed import Client
client = Client(scheduler_file='dask_scheduler.json')
```


