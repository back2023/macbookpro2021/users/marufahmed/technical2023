https://wiki.cen.uni-hamburg.de/ifm/TO/pyOM2

pyOM2 is a numerical circulation ocean model powered by [Python](https://www.python.org/). Features are:

* Cartesian or pseudo-spherical coordinate systems

* Hydrostatic or non-hydrostatic configurations

* energetically consistent parameterisations

* Fortran and Python front end

* Graphical User Interface

* fully parallelized using [MPI](http://www.mpi-forum.org/)

Idealized and realistic configurations are simple and easy to configure and to integrate. Fortran and a Python version are based on the identical Fortran90 core code. Several idealized and realistic examples are