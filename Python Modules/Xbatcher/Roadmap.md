
https://xbatcher.readthedocs.io/en/latest/roadmap.html

Authors: Joe Hamman and Ryan Abernathey Date: February 7, 2019

## Background and scope[](https://xbatcher.readthedocs.io/en/latest/roadmap.html#background-and-scope "Permalink to this heading")

Xbatcher is a small library for iterating xarray objects in batches. The goal is to make it easy to feed xarray datasets to machine learning libraries such as [Keras](https://keras.io/) or [PyTorch](https://pytorch.org/). For example, implementing a simple machine learning workflow may look something like this: