
https://github.com/xarray-contrib/xbatcher


Xbatcher is a small library for iterating Xarray DataArrays and Datasets in batches. The goal is to make it easy to feed Xarray objects to machine learning libraries such as [PyTorch](https://pytorch.org/) or [TensorFlow](https://www.tensorflow.org/). 
View the [![docs](https://camo.githubusercontent.com/1a969013d90085f91dbea2fb48572fee8161d8e0650f374366d2f5de23d9d5db/687474703a2f2f72656164746865646f63732e6f72672f70726f6a656374732f78626174636865722f62616467652f3f76657273696f6e3d6c6174657374)](http://xbatcher.readthedocs.org/en/latest/?badge=latest) for more info.
 