
**disropt** is a Python package developed within the excellence research program ERC in the project [OPT4SMART](http://www.opt4smart.eu/). The aim of this package is to provide an easy way to run distributed optimization algorithms that can be executed by a network of peer copmuting systems.


**disropt** currently supports MPI in order to emulate peer-to-peer communication. However, custom communication protocols can be also implemented.


https://disropt.readthedocs.io/en/latest/index.html