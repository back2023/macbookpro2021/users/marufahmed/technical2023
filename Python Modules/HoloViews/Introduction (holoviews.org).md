
https://holoviews.org/getting_started/Introduction.html

HoloViews is an [open-source](https://github.com/holoviz/holoviews) Python library for data analysis and visualization. Python already has excellent tools like numpy, pandas, and xarray for data processing, and bokeh and matplotlib for plotting, so why yet another library?

**HoloViews helps you understand your data better, by letting you work seamlessly with both the data _and_ its graphical representation.**



