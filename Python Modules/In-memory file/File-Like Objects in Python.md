
https://dev.to/bluepaperbirds/file-like-objects-in-python-1njf

[Python](https://python.org/) supports file like objects, that don't write to the disk but stay in the memory.

You can create file like objects with [StringIO](https://docs.python.org/2/library/stringio.html). From Python version > 3 this is part of the [io module](https://docs.python.org/3/library/io.html).

These files live only inside the computer memory, not on the disk. Python can [read files](https://pythonbasics.org/read-file/) from the disk, but this article focuses on files in memory.

