

https://docs.wradlib.org/en/latest/index.html#omega-radlib-an-open-source-library-for-weather-radar-data-processing


**Release:** 2.2.0.dev1+gc273696  
**Date:** 2024-08-30

The ωradlib project has been initiated in order facilitate the use of weather radar data as well as to provide a common platform for research on new algorithms. ωradlib is an open source library which is well documented and easy to use. It is written in the free programming language [Python](https://www.python.org/).



