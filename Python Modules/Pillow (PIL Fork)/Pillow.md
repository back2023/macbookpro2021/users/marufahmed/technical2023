
# Pillow[](https://pillow.readthedocs.io/en/stable/index.html#pillow "Permalink to this heading")

Pillow is the friendly PIL fork by [Jeffrey A. Clark (Alex) and contributors](https://github.com/python-pillow/Pillow/graphs/contributors). PIL is the Python Imaging Library by Fredrik Lundh and contributors.

Pillow for enterprise is available via the Tidelift Subscription. [Learn more](https://tidelift.com/subscription/pkg/pypi-pillow?utm_source=pypi-pillow&utm_medium=docs&utm_campaign=enterprise).