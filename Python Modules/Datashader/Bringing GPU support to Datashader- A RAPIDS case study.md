
[Datashader](https://datashader.org/) is a Python library for rapidly creating principled visual representations of large datasets. It is written in pure Python and relies heavily on [numpy](https://numpy.org/), [pandas](https://pandas.pydata.org/), [Dask](https://dask.org/), [xarray](http://xarray.pydata.org/en/stable/), and the [Numba](http://numba.pydata.org/) Just-in-Time compiler. See the Datashader [documentation](https://datashader.org/) for examples of the kinds of visualizations it is designed to create.


https://medium.com/rapids-ai/bringing-gpu-support-to-datashader-62a693f2c554