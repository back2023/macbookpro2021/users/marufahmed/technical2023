

https://ipyvolume.readthedocs.io/en/docs/index.html


IPyvolume is a Python library to visualize 3d volumes and glyphs (e.g. 3d scatter plots), in the Jupyter notebook, with minimal configuration and effort. It is currently pre-1.0, so use at own risk. IPyvolume’s _volshow_ is to 3d arrays what matplotlib’s imshow is to 2d arrays.

Other (more mature but possibly more difficult to use) related packages are [yt](http://yt-project.org/), [VTK](https://www.vtk.org/) and/or [Mayavi](http://docs.enthought.com/mayavi/mayavi/).

Feedback and contributions are welcome: [Github](https://github.com/maartenbreddels/ipyvolume), [Email](mailto:maartenbreddels%40gmail.com) or [Twitter](https://twitter.com/maartenbreddels).


