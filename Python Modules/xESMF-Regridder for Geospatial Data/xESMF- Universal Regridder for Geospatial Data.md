https://xesmf.readthedocs.io/en/latest/

### Regridding Overview
https://climatedataguide.ucar.edu/climate-tools/regridding-overview
1. Regridding is the process of interpolating from one grid resolution to a different grid resolution. This could involve temporal, vertical or spatial ('horizontal') interpolations.
2. The most commonly used methods of climate grid interpolation are bilinear, conservative and patch. The bilinear method is easy to program and apply when the source and destination grids are rectilinear.




