```
import xesmf as xe
...
ModuleNotFoundError: No module named 'ESMF'
```
Try
https://earthsystemmodeling.org/esmpy_doc/release/ESMF_8_0_1/html/install.html
```
conda install -c nesii -c conda-forge esmpy
```
