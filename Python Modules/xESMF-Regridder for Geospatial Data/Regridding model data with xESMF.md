
https://nordicesmhub.github.io/forces-2021/learning/example-notebooks/xesmf_regridding.html



[![logo](https://nordicesmhub.github.io/forces-2021/_static/logo.png =5x)

### eScience Tools in Climate Science - Linking Observations with Modelling
https://nordicesmhub.github.io/forces-2021/index.html

https://nordicesmhub.github.io/forces-2021/intro.html


## Galaxy[](https://nordicesmhub.github.io/forces-2021/intro.html#galaxy "Permalink to this headline")

[Galaxy](https://galaxyproject.org/) is an open, accessible, reproducible, and transparent computational research platform that can be accessed via command line, the Jupyter notebook and/or through a user-friendly Graphical Interface.

## Pangeo[](https://nordicesmhub.github.io/forces-2021/intro.html#pangeo "Permalink to this headline")

[PANGEO](https://pangeo.io/) is a community platform for Big Data geoscience. The Pangeo software ecosystem involves open source tools such as xarray, iris, dask, jupyter, and many other packages. We will be using these Python packages during this course and encourage attendees to join the Pangeo community and contribute to promote **open**, **reproducible** and **scalable science**.

## Reliance[](https://nordicesmhub.github.io/forces-2021/intro.html#reliance "Permalink to this headline")

[Research Lifecycle Management technologies for Earth Science Communities and Copernicus users in EOSC (RELIANCE)](https://www.reliance-project.eu/) is an INFRAEOSC-07-2020 Research and Innovation action - Grant number 101017501 that has started in January 2021 and will end in December 2022.

A suite of innovative and interconnected services integrated into EOSC- HUB and used by the EOSC scientific communities to support thematic and multidisciplinary research in Earth Science.

## RoHub[](https://nordicesmhub.github.io/forces-2021/intro.html#rohub "Permalink to this headline")

[RoHub](https://reliance.rohub.org/) is a Research object management platform supporting the preservation and lifecycle management of scientific investigations, research campaigns and operational processes. As the only existing platform implementing natively the full research object model and paradigm, resources associated to a particular experiment are aggregated in a single digital entity (research object), and metadata relevant to understand and interpret the content is represented as semantic metadata that are user and machine readable.