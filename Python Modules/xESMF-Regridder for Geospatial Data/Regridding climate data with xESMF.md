
https://pavics-sdi.readthedocs.io/en/latest/notebooks/regridding.html



PAVICS is a research platform dedicated to climate analysis and visualization. It bundles data search, analytics and visualization services. PAVICS is developped by Ouranos, CRIM and the [birdhouse](http://bird-house.github.io/) community and been funded by the [CANARIE](http://www.canarie.ca/) research software program.

https://pavics-sdi.readthedocs.io/en/latest/index.html