## Installation
https://xesmf.readthedocs.io/en/latest/installation.html


### Testing your installation
xESMF itself is a lightweight package, but its dependency ESMPy is a quite heavy and sometimes might be installed incorrectly. To validate & debug your installation, you can use pytest to run the test suites:
```bash
$ pip install pytest
$ pytest -v --pyargs xesmf  # should all pass
```
Error-1
```bash
ERROR  - ModuleNotFoundError: No module named 'ESMF'
```

Next, Install try (Already insalled - NOT work)
```
conda install -c conda-forge xesmf
conda install -c conda-forge dask netCDF4
```
Next, Install try
```
conda install -c conda-forge esmpy xarray numpy shapely cf_xarray sparse numba
pip install git+https://github.com/pangeo-data/xesmf.git
```
Passed test

Error-2
```bash
    import xesmf as xe
ModuleNotFoundError: No module named 'xesmf'
...
OSError: could not get source code
```
Try (Working)
1. ESMpy must be installed through Conda or compiled manually; it is not available through PyPI.
2. When installing ESMpy 8.4 through conda, the environment must be _activated_ before the package is used, which often means deactivating and re-activating it. 
     See this [xESMF issue](https://github.com/pangeo-data/xESMF/issues/224) and the related [ESMpy issue](https://github.com/conda-forge/esmf-feedstock/issues/91).
Working (after reactivating the env. / Restart notebook server)
