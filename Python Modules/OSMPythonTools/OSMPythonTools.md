
https://wiki.openstreetmap.org/wiki/OSMPythonTools

The python package **OSMPythonTools** provides easy access to OpenStreetMap related services, among them an [Overpass endpoint](https://wiki.openstreetmap.org/wiki/Overpass_API "Overpass API"), [Nominatim](https://wiki.openstreetmap.org/wiki/Nominatim "Nominatim"), and the [OSM API](https://wiki.openstreetmap.org/wiki/API "API").


