
Scientific visualization in the Jupyter notebook

From Paraview to Mayavi, there are multiple solutions for data analysis on 3D meshes on the desktop. Most of these tools provide high-level APIs that can be driven with a scripting language like Python. For example, one could control Paraview from a Jupyter Notebook. But this is not ideal as it relies on a desktop application for the rendering, which prevents using tools like [MyBinder](https://mybinder.org/).


https://blog.jupyter.org/ipygany-jupyter-into-the-third-dimension-29a97597fc33







