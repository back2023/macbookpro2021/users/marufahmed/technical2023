
https://github.com/Kitware/trame-components

trame-components extend trame **widgets** with helper components that are core to trame widgets.

This package is not supposed to be used by itself but rather should come as a dependency of **trame**. For any specificity, please refer to [the trame documentation](https://kitware.github.io/trame/).


