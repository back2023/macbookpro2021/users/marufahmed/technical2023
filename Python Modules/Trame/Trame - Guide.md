
https://kitware.github.io/trame/guide/

## What is Trame[​](https://kitware.github.io/trame/guide/#what-is-trame)

Trame is a Python integration framework to easily build web applications with minimal knowledge of web development or technology. Before trame, building such applications typically required a full-stack developer at least a day. Now any Python developer can build applications in minutes.

## Why Trame[​](https://kitware.github.io/trame/guide/#why-trame)

There are variety of tools and frameworks available for building web applications, but very few are capable of providing interactive 3D visualization. Trame does this by leveraging VTK and/or ParaView (as well as integrating other tools such as Vega). Trame is the culmination of decades of work: VTK/ParaView, ParaViewWeb, and vtk.js are just some of the components that trame builds upon, but does so in such a way as to hide the complexity of these underlying systems. For example, by using vtk.js and JavaScript it is possible to build powerful web applications, but this requires significant web-development knowledge. But with trame, the full power of frameworks such as VTK/ParaView are available without the burden of web development.

- Open-source - You can confidently build, deploy, and commercialize applications without the usual hassles associated with proprietary systems, and with the knowledge that trame will not disappear - it is not tied to the fortunes of any proprietary vendor.
- All-in-one platform - Unlike other libraries or platforms, trame comes with most all the components you need to build visualization analytics applications; and if a capability is missing it can easily be added.
- Design - Trame apps look beautiful out of the box. Our built-in Material Design widget library enables you to create beautiful desktop-like GUI components.
- Real apps - With Trame, you get high-performing interactive applications that can operate locally, or across the web.

## How Trame works[​](https://kitware.github.io/trame/guide/#how-trame-works)

Building apps with Trame is this simple:

1. **Install trame** - Create a Python virtual environment and `pip install trame`. Once working locally, deploy it using Docker or as Desktop app bundle.
2. **Business logic** - Create your processing functions in plain Python and the **state** that needs to be shared with the UI.
3. **State reactivity** - Define methods which respond to state change (e.g. a slider changing a sampling parameter).
4. **Design your UI** - Build beautiful, accessible user interfaces by defining the layout of your application and connecting your **state** and **functions** to your UI elements directly.
5. **Run it anywhere** - Once working, you can choose to run it locally as a desktop or client/server application, or deploy it in the cloud and use it as a service.