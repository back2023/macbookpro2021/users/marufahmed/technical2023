
## Project description

[![Test and Release](https://pypi-camo.global.ssl.fastly.net/dac0a2530eb07ff79ba285d4e5a0a4a9f4e82ce8/68747470733a2f2f6769746875622e636f6d2f4b6974776172652f7472616d652f616374696f6e732f776f726b666c6f77732f746573745f616e645f72656c656173652e796d6c2f62616467652e737667)](https://github.com/Kitware/trame/actions/workflows/test_and_release.yml) ![Download stats](https://pypi-camo.global.ssl.fastly.net/7ce0529034dab4f95d47c42c90e86a03e92650d8/68747470733a2f2f696d672e736869656c64732e696f2f707970692f646d2f7472616d65)

**trame** - a web framework that weaves together open source components into customized visual analytics easily.

**trame** is French for

- the core that ties things together
- a guide providing the essence of a task
