
https://trame.readthedocs.io/en/latest/


Note

Trame supports desktop, client/server, cloud and Jupyter as execution environment. But we also provide a refined API to make it more robust and future proof. Trame allow integration of vue2/3 components into its Web applications while also streamlining community contribution for new widgets.

# trame: simple, powerful, innovative[](https://trame.readthedocs.io/en/latest/#trame-simple-powerful-innovative "Link to this heading")

[![Test and Release](https://github.com/Kitware/trame/actions/workflows/test_and_release.yml/badge.svg)](https://github.com/Kitware/trame/actions/workflows/test_and_release.yml) ![Download stats](https://img.shields.io/pypi/dm/trame)

**trame** - a web framework that weaves together open source components into customized visual analytics easily.

**trame** is French for

- the core that ties things together
    
- a guide providing the essence of a task
