
https://pypi.org/project/pooch/


"Pooch manages your Python library's sample data files: it automatically downloads and stores them in a local directory, with support for versioning and corruption checks."

_Are you a scientist or researcher? Pooch can help you too!_

- Automatically download your data files so you don't have to keep them in your GitHub repository.
- Make sure everyone running the code has the same version of the data files (enforced through the SHA256 hashes).
