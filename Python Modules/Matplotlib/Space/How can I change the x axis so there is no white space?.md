
https://stackoverflow.com/questions/42045767/how-can-i-change-the-x-axis-so-there-is-no-white-space/42045987#42045987

To set the margin to `0` on the x axis, use

```python
plt.margins(x=0)
```

or

```python
ax.margins(x=0)
```


```python
plt.rcParams['axes.xmargin'] = 0
```





