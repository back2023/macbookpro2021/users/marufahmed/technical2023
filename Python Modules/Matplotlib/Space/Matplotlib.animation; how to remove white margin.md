
https://stackoverflow.com/questions/15882395/matplotlib-animation-how-to-remove-white-margin


Passing `None` as an arguement to `subplots_adjust` does not do what you think it does [(doc)](http://matplotlib.org/api/figure_api.html#matplotlib.figure.Figure.subplots_adjust). It means 'use the deault value'. To do what you want use the following instead:

```python
fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=None, hspace=None)
```






