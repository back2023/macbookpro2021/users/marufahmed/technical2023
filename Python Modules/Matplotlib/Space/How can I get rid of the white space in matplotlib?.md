
https://datascience.stackexchange.com/questions/90355/how-can-i-get-rid-of-the-white-space-in-matplotlib


[This stackoverflow answer](https://stackoverflow.com/a/42045987/9435355) should solve your issue. You can use either `plt.margins(x=0)` or `ax.margins(x=0)` to remove all margins on the x-axis.

