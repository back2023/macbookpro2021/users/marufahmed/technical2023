
https://stackoverflow.com/questions/6541123/improve-subplot-size-spacing-with-many-subplots


Please review [matplotlib: Tight Layout guide](https://matplotlib.org/stable/tutorials/intermediate/tight_layout_guide.html) and try using [`matplotlib.pyplot.tight_layout`](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.tight_layout.html), or [`matplotlib.figure.Figure.tight_layout`](https://matplotlib.org/stable/api/figure_api.html#matplotlib.figure.Figure.tight_layout)




