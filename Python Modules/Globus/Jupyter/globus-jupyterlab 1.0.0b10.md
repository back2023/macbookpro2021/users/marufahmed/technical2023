
Globus Jupyterlab is an extension to Jupyterlab for easily submitting Globus Transfers within a running lab environment. Integration within the Jupyterlab filemanager makes starting transfers just a few clicks away!

https://pypi.org/project/globus-jupyterlab/

