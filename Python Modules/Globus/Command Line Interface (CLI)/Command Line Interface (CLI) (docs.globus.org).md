
This page provides information about the Globus CLI, a standalone application that can be installed on the user’s machine and used to access the Globus service.

The CLI provides an interface to Globus services from the shell, and is suited to both interactive and simple scripting use cases.

It is open source and available at [https://github.com/globus/globus-cli](https://github.com/globus/globus-cli)


https://docs.globus.org/cli/

