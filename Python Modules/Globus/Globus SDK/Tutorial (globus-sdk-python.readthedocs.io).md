
This is a tutorial in the use of the Globus SDK. It takes you through a simple step-by-step flow for registering your application, getting tokens, and using them with our service.

https://globus-sdk-python.readthedocs.io/en/stable/tutorial.html

