
The Python Software Development Kit (SDK) allows users to manage data via their own scripts or tools built on Globus. This provides a lot of opportunities for automating workflows.

https://vlaams-supercomputing-centrum-vscdocumentation.readthedocs-hosted.com/en/latest/globus/python_sdk.html