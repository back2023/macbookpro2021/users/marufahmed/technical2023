
## Globus SDK for Python

This SDK provides a convenient Pythonic interface to [Globus](https://www.globus.org/) APIs.

https://github.com/globus/globus-sdk-python

