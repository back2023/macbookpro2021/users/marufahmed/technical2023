

# Quickstart 

**Globus Compute** client and endpoint software releases are available on [PyPI](https://pypi.org/project/funcx/).

You can try Globus Compute on a hosted [Jupyter notebook](https://jupyter.demo.globus.org/hub/user-redirect/lab/tree/globus-jupyter-notebooks/Compute_Introduction.ipynb)

###  installing-jupyter-for-tutorial-notebooks-optional
https://funcx.readthedocs.io/en/latest/quickstart.html#installing-jupyter-for-tutorial-notebooks-optional