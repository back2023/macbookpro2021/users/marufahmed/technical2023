
**Vega-Altair** is a declarative visualization library for Python. Its simple, friendly and consistent API, built on top of the powerful [Vega-Lite](http://vega.github.io/vega-lite) grammar, empowers you to spend less time writing code and more time exploring your data.

https://altair-viz.github.io/index.html

