
This example shows how to make a basic histogram, based on the vega-lite docs [https://vega.github.io/vega-lite/examples/histogram.html](https://vega.github.io/vega-lite/examples/histogram.html)

https://altair-viz.github.io/gallery/simple_histogram.html