

The era5 python code is an interface to the CDS api to download ERA5 data from the CDS data server. It uses a modified version of the CDS api which stops after a request has been submitted and executed. The target download url is saved and downloads are run in parallel by the code using the Pool multiprocessing module. As well as managing the downloads the code gets all the necessary information on available variables from local json configuration files. Before submitting a request the code will check that the file is not already available locally by quering a sqlite database. After downloading new files it is important to update the database to avoid downloading twice the same file. Files are first downloaded in a staging area, a quick qc to see if the file is a valid netcdf file is run and finally the file is converted to netcdf4 format with internal compression. The code default behaviour is to download netcdf files, but it's also possible to download grib, however we haven't tested the full workflow for this option.

### [](https://github.com/coecms/era5#getting-started)Getting started

To run a download:

era5 download -s surface -y 2018 -m 11 -p 228.128


https://github.com/coecms/era5


