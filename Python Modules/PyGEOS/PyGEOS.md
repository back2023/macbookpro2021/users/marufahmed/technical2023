
https://pygeos.readthedocs.io/en/latest/index.html


**Important note**: PyGEOS was merged with Shapely ([https://shapely.readthedocs.io](https://shapely.readthedocs.io/)) in December 2021 and has been released as part of Shapely 2.0. The development now takes place at the Shapely repository. Please raise issues or create pull request over there. PyGEOS itself will in principle not receive updates anymore.
 