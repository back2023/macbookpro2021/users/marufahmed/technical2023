
https://github.com/OSGeo/gdal


GDAL is an open source MIT licensed translator library for raster and vector geospatial data formats.

- Main site: [https://gdal.org](https://gdal.org/) - Developer and user docs, links to other resources
- GIT repository: [https://github.com/OSGeo/gdal](https://github.com/OSGeo/gdal)
- Bug tracker: [https://github.com/OSGeo/gdal/issues](https://github.com/OSGeo/gdal/issues)
- Download: [https://download.osgeo.org/gdal](https://download.osgeo.org/gdal)
- Wiki: [https://trac.osgeo.org/gdal](https://trac.osgeo.org/gdal) - Various user and developer contributed documentation and hints
- Mailing list: [https://lists.osgeo.org/mailman/listinfo/gdal-dev](https://lists.osgeo.org/mailman/listinfo/gdal-dev)

The GDAL project uses a [custom governance](https://github.com/OSGeo/gdal/blob/master/GOVERNANCE.md) and is fiscally sponsored by [NumFOCUS](https://numfocus.org/). Consider making a [tax-deductible donation](https://numfocus.org/donate-to-gdal) to help the project pay for developer time, professional services, travel, workshops, and a variety of other needs.