
https://geonetcast.wordpress.com/2017/08/28/geonetclass-manipulating-gfs-grib2-data-with-python-part-ii/


This is the second part of the **GFS GRIB2 / Python** tutorial series.

You may find the other parts by clicking at the links below:

- [Part 1: Reading GRIB2 files and making a basic plot](https://geonetcast.wordpress.com/2017/08/25/geonetclass-manipulating-gfs-grib2-data-with-python-part-i/)

In this part, we’ll do the following:

- **Select the region to visualize, subsecting the grib file**
- **Create “contour” and “filled contour” plots**

This tutorial series assumes you’re already familiar with the **GOES-16 / Python** tutorial series found at this [Blog’s Tutorials Page](https://geonetcast.wordpress.com/gnc-a-product-manipulation-tutorials/). It also assumes that you have installed all the libraries used in them (basemap, gdal and netcdf).





