
https://github.com/kokkos/pykokkos?tab=readme-ov-filev

# PyKokkos

PyKokkos is a framework for writing high-performance Python code similar to Numba. In contrast to Numba, PyKokkos kernels are primarily parallel and are also performance portable, meaning that they can run efficiently on different hardware (CPUs, NVIDIA GPUs, and AMD GPUs) with no changes required.

For more information about PyKokkos, see the PyKokkos GitHub pages: [https://kokkos.github.io/pykokkos/index.html](https://kokkos.github.io/pykokkos/index.html)


