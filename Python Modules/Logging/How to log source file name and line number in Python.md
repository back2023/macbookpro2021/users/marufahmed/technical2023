
https://stackoverflow.com/questions/533048/how-to-log-source-file-name-and-line-number-in-python


Sure, check [formatters](http://docs.python.org/library/logging.html#formatter-objects) in logging docs. Specifically the lineno and pathname variables.

> **%(pathname)s** Full pathname of the source file where the logging call was issued(if available).
> 
> **%(filename)s** Filename portion of pathname.
> 
> **%(module)s** Module (name portion of filename).
> 
> **%(funcName)s** Name of function containing the logging call.
> 
> **%(lineno)d** Source line number where the logging call was issued (if available).

Looks something like this:

```python
formatter = logging.Formatter('[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s','%m-%d %H:%M:%S')
```


