
https://github.com/team-ocean/veros

Veros, _the versatile ocean simulator_, aims to be the swiss army knife of ocean modeling. It is a full-fledged primitive equation ocean model that supports anything between idealized toy models and [realistic, high-resolution, global ocean simulations](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2021MS002717). And because Veros is written in pure Python, the days of struggling with complicated model setup workflows, ancient programming environments, and obscure legacy code are finally over.

_In a nutshell, we want to enable high-performance ocean modelling with a clear focus on flexibility and usability._

Veros supports a NumPy backend for small-scale problems, and a high-performance [JAX](https://github.com/google/jax) backend with CPU and GPU support. It is fully parallelized via MPI and supports distributed execution on any number of nodes, including multi-GPU architectures (see also [our benchmarks](https://veros.readthedocs.io/en/latest/more/benchmarks.html)).

The dynamical core of Veros is based on [pyOM2](https://wiki.cen.uni-hamburg.de/ifm/TO/pyOM2), an ocean model with a Fortran backend and Fortran and Python frontends.

To learn more about Veros, make sure to [visit our documentation](https://veros.readthedocs.io/en/latest/).

#### [](https://github.com/team-ocean/veros#how-about-a-demonstration)