https://dionhaefner.github.io/2021/12/supercharged-high-resolution-ocean-simulation-with-jax/


Our Python ocean model [Veros](https://github.com/team-ocean/veros) (which I maintain) now fully supports [JAX](https://github.com/google/jax) as its computational backend. As a result, Veros has much better performance than before on both CPU and GPU, while all model code is still written in Python. In fact, we can now do high-resolution ocean simulations on a handful of GPUs, with the performance of entire CPU clusters!