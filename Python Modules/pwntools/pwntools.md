
`pwntools` is a CTF framework and exploit development library. Written in Python, it is designed for rapid prototyping and development, and intended to make exploit writing as simple as possible.

The primary location for this documentation is at [docs.pwntools.com](https://docs.pwntools.com/en/latest), which uses [readthedocs](https://readthedocs.org/). It comes in three primary flavors:

- [Stable](https://docs.pwntools.com/en/stable)
    
- [Beta](https://docs.pwntools.com/en/beta)
    
- [Dev](https://docs.pwntools.com/en/dev)

https://docs.pwntools.com/en/stable/index.html