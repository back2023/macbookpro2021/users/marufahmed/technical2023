
https://stackoverflow.com/questions/1392413/calculating-a-directorys-size-using-python


And a oneliner for fun using [os.listdir](http://docs.python.org/library/os.html?highlight=shutil#os.listdir) (_Does not include sub-directories_):

```python
import os
sum(os.path.getsize(f) for f in os.listdir('.') if os.path.isfile(f))
```

