

https://www.tutorialspoint.com/How-to-calculate-a-directory-size-using-Python


Hence, to get the size of a directory we must walk through the hierarchy to get the sizes of all the files in it. Python provides several ways to do it.

- Using os.path.getsize() method
    
- Using os.stat().st_size property
    
- Using du command in *NIX OSes
