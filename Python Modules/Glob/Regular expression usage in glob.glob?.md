
https://stackoverflow.com/questions/13031989/regular-expression-usage-in-glob-glob



The easiest way would be to filter the glob results yourself. Here is how to do it using a simple loop comprehension:

```python
import glob
res = [f for f in glob.glob("*.txt") if "abc" in f or "123" in f or "a1b" in f]
for f in res:
    print f
```

You could also use a regexp and no `glob`:

```python
import os
import re
res = [f for f in os.listdir(path) if re.search(r'(abc|123|a1b).*\.txt$', f)]
for f in res:
    print f
```

(By the way, naming a variable `list` is a bad idea since `list` is a Python type...)

