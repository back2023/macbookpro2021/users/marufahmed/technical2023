


https://github.com/ecmwf/cfgrib/issues/66#issuecomment-496268903
[@enyfeo](https://github.com/enyfeo) using the new heuristic for cfgrib.open_datasets introduced with version 0.9.7 you should get all, or almost all, variable correctly.


```python
>>> import cfgrib
>>> cfgrib.open_datasets('EFAS.grib')
```