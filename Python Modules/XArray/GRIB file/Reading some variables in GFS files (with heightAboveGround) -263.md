
https://github.com/ecmwf/cfgrib/issues/263

***
https://github.com/ecmwf/cfgrib/issues/263#issuecomment-970485007
Now, cfgrib does not like variables with different coordinates (see also [#13](https://github.com/ecmwf/cfgrib/issues/13)). In this case, it will first read variable 'refd' and take the level coordinates to be 1000 and 4000. Then it hits variable '2t' with a level of 2. This will not work - see the note in the readme:  
[https://github.com/ecmwf/cfgrib#filter-heterogeneous-grib-files](https://github.com/ecmwf/cfgrib#filter-heterogeneous-grib-files)

So you will need to also filter by variable to get only those that have the same levels. Before you ask, I'm not sure how to handle the awkward case of u/u10/u100 having different names and paramIds!

I do hope that this helps though.

Best regards,  
Iain
***
**[iainrussell](https://github.com/iainrussell)** commented [on Nov 17, 2021](https://github.com/ecmwf/cfgrib/issues/263#issuecomment-970711696)
https://github.com/ecmwf/cfgrib/issues/263#issuecomment-970711696

Ok, no problem. Metview's Python interface can also be used for pre-processing, but it requires some binaries to be installed (usually through conda). We are, however, discussing some equivalent pure Python features that could help in this sort of situation, because its interface for GRIB handling is very good.

I'll close this issue now though.

***
**[aguerrero217](https://github.com/aguerrero217)** commented [on May 10](https://github.com/ecmwf/cfgrib/issues/263#issuecomment-1540838485)
https://github.com/ecmwf/cfgrib/issues/263#issuecomment-1540838485
**Other Option is filter by levels**  
The problem is that there are too many levels. So you can obtain the data by filter  
Here is an example :

_For the 2m height in Python2 looks like this:_

`data_2maboveground=xarray.open_dataset(local_filename,engine="cfgrib" ,filter_by_keys={'typeOfLevel': 'heightAboveGround','level':2})`

_For the 2m height in Python3 looks like this:_

`data_2maboveground=xarray.open_dataset(local_filename,engine="cfgrib" ,backend_kwargs={'filter_by_keys':{'typeOfLevel': 'heightAboveGround','level':2}})`  
PD. I added ('typeOfLevel':'heightAboveGround') cause I have more data in the GRIB