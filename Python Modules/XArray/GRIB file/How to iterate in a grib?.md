
https://stackoverflow.com/questions/66304427/how-to-iterate-in-a-grib


I usually use pygrib for GRIB file, but xarray also good I think.
```python
G = pygrib.open('file.grb'):
for g in G:
    print(g)
```

Get coords and values like this for example...
```python
latlons = g.latlons()
values = g.values
```

check out all the other data available with keys.. need to pick out valid times and forecast step offset, depending on the data.

`print(g.keys())`




`xarray` provides an interface to the gridded data (as you can see [in the documentation](http://xarray.pydata.org/en/stable/data-structures.html)).

I use the package `cfgrib` to access GRIB data. It's solid and developed by ECMWF ([https://github.com/ecmwf/cfgrib](https://github.com/ecmwf/cfgrib)).

```python
import xarray as xr

d = xr.open_dataset('gefs.wave.t00z.c00.global.0p25.f000.grib2', engine = 'cfgrib')
print(d)
```


```python
print(d['swh'])
```







