
https://gis.stackexchange.com/questions/173775/how-to-extract-specific-information-from-grib-files


Another idea would be to use pygrib module:

```python
import pygrib

    grbs= pygrib.open("my_file.grb")

    # use grbs.select to select the grids you are interested in (shortName, typeOfLevel, level=500, marsParam, dataDate, dataTime, ...)

    DATA=np.array(grbs.select(marsParam=my_param,dataDate=my_date,dataTime=my_time))

    # DATA will contain 3 arrays
    # DATA[0] for values
    # DATA[1] for longitudes
    # DATA[2] for latitudes

    # from the "values" array, extract in lon and lat
    DATA1=DATA[0].data(lat1=my_y1,lat2=my_y2,lon1=my_x1,lon2=my_x2))
```
