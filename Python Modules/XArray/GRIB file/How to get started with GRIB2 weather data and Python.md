
https://spire.com/tutorial/spire-weather-tutorial-intro-to-processing-grib2-data-with-python/

```bash
conda install -c anaconda xarray

conda install -c conda-forge pynio

conda install -c anaconda pandas
```
- If you’re having trouble with PyNIO, you can use **cfgrib** as the xarray engine instead: [https://pypi.org/project/cfgrib/](https://pypi.org/project/cfgrib/)

```python
import xarray as xr

ds = xr.open_dataset("path_to_basic_file.grib2", engine="pynio")

for v in ds:
    print("{}, {}, {}".format(v, ds[v].attrs["long_name"], ds[v].attrs["units"]))
    
```
 
### Processing the Data
#### Filtering the xarray data to a specific variable

```python
ds = ds.get("TMP_P0_L103_GLL0")
```
- Selecting multiple variables is also possible by using an array of strings as the input: `ds = ds.get([var1, var2])`





