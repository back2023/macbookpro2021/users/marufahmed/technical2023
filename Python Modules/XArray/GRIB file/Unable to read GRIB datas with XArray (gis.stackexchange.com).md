
https://gis.stackexchange.com/questions/372729/unable-to-read-grib-datas-with-xarray



If you add the `backend_kwargs` line, it will work properly, for example:-

```python
ds = xr.open_dataset('gfs_4_20191010_0000_006.grb2',
                     engine='cfgrib',
                     backend_kwargs={'filter_by_keys': {'typeOfLevel': 'surface'}})
```




