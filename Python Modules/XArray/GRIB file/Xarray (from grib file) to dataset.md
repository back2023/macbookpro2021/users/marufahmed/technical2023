
https://stackoverflow.com/questions/67963199/xarray-from-grib-file-to-dataset


I have a grib file containing monthly precipitation and temperature from 1989 to 2018 (extracted from ERA5-Land).

So the temperature variable is called "t2m" and the precipitation variable "tp". Temperature variable is split in two xarrays but I don't understand why.


https://stackoverflow.com/a/67995661
Here is the answer after a bit of trial and error (only putting the result for tp variable but it's similar for t2m)
```python
import cfgrib
import xarray as xr


# Import data
grib_data = cfgrib.open_datasets('\era5land_extract.grib')


# Merge both tp arrays into one on the time dimension
grib_precip = xr.merge([grib_data[1], grib_data[2]])


# Aggregate data by year
grib_precip_year = grib_precip.resample(time="Y", skipna=True).mean()


# Data from xarray to pandas
grib_precip_pd = grib_precip_year.to_dataframe()
```

