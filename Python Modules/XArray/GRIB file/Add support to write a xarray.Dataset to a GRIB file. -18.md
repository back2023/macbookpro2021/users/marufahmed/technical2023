
https://github.com/ecmwf/cfgrib/issues/18


https://github.com/ecmwf/cfgrib/issues/18#issuecomment-423186927
```python
import xarray as xr
from cfgrib import xarray_store
from cfgrib import xarray_to_grib
import cfgrib

dst = xarray_store.GribDataStore.from_path("User_Guide_Example_Data.grib")
ds = xr.open_dataset(dst)
print(ds)

cfgrib.to_grib(ds, 'User_Guide_Example_Data_from_cfgrib.grib')#, grib_keys={'centre': 'ecmf'})
```

