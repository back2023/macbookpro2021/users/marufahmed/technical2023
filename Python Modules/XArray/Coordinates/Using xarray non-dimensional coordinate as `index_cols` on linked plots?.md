

https://discourse.holoviz.org/t/using-xarray-non-dimensional-coordinate-as-index-cols-on-linked-plots/1730/5
I spent some time to change it around a bit to generate random data so it can stand on itself.

However, my problem still remains. I would like the selection in one plot reflected in the other, preferably using the `idx` `xr.Dataset` column, however I could not find a way to do this.
##### non-dimensional coordinate example
```python
import xarray as xr

import numpy as np
import pandas as pd

#import holoviews as hv
#from holoviews.operation.datashader import datashade
#import param
#import panel as pn

#hv.extension("bokeh")

# Set shape parameters 
x_len = 100
y_len = 120

# Create coordinates
x = np.arange(1, x_len + 1)
y = np.arange(1, y_len + 1)
band = (np.arange(250)).astype(int)
idx = np.arange(y_len * x_len).reshape(x_len, y_len)

# Extract 2 components to plot 
comp1 = np.random.rand(x_len, y_len)
comp2 = np.random.rand(x_len, y_len)

# Create the xr dataset
xrds = xr.Dataset(
    {
        "absorbance": (["y", "x", "band"], np.random.rand(y_len, x_len, 250)),
    },
    coords={
        "x": x,
        "y": y,
        "band": band,
        "idx": (("x", "y"), idx),
        "comp1": (("x", "y"), comp1),
        "comp2": (("x", "y"), comp2),
    }
    )

# Set metadata
xrds.band.attrs = dict(
    standard_name = "wavenumber",
    long_name = "Wavenumber",
    units = "cm⁻¹",
    step = 2, 
)
```

**Output**:
```bash
print(xrds)
<xarray.Dataset>
Dimensions:     (y: 120, x: 100, band: 250)
Coordinates:
  * x           (x) int64 1 2 3 4 5 6 7 8 9 10 ... 92 93 94 95 96 97 98 99 100
  * y           (y) int64 1 2 3 4 5 6 7 8 9 ... 113 114 115 116 117 118 119 120
  * band        (band) int64 0 1 2 3 4 5 6 7 ... 242 243 244 245 246 247 248 249
    idx         (x, y) int64 0 1 2 3 4 5 ... 11994 11995 11996 11997 11998 11999
    comp1       (x, y) float64 0.6102 0.7991 0.03163 ... 0.8359 0.5216 0.152
    comp2       (x, y) float64 0.2744 0.1983 0.3567 ... 0.694 0.4118 0.7583
Data variables:
    absorbance  (y, x, band) float64 0.6944 0.2474 0.5051 ... 0.1199 0.3246
```




https://discourse.holoviz.org/t/using-xarray-non-dimensional-coordinate-as-index-cols-on-linked-plots/1730

"I am using an `xr.Dataset` to store the data and would like to use an index coordinate to sync the selections easily, which I included as a non-dimensional coordinate in the `xarray`."

```python

# Get shape parameters from the data 
x_len = selected_absorbance.shape[1] 
y_len = selected_absorbance.shape[0] 
embed_len = umap_embeddings2d["10n"].shape[1]

# Create coordinates 
x = np.arange(1, x_len + 1) 
y = np.arange(1, y_len + 1) 
band = (np.arange(band_max, band_min+band_change, band_change)).astype(int) 
idx = np.arange(y_len * x_len).reshape(x_len, y_len)

# Extract 2 components to plot 
comp1 = deepcopy(umap_embeddings2d["10n"].iloc[:, 0].to_numpy()).reshape(x_len, y_len) 
comp2 = deepcopy(umap_embeddings2d["10n"].iloc[:, 1].to_numpy()).reshape(x_len, y_len)


# Create the xr dataset 
xrds = xr.Dataset( 
	{ "absorbance": (["y", "x", "band"], selected_absorbance),
	}, 
	coords={ 
		"x": x, 
		"y": y, 
		"band": band, 
		"idx": (("x", "y"), idx), 
		"comp1": (("x", "y"), comp1), 
		"comp2": (("x", "y"), comp2), 
	} 
)

# Set metadata 
xrds.absorbance.attrs = dict( 
	units = "Absorbance", 
	standard_name = "absorbance_maps", 
	long_name = "FTIR absorbance maps for every scanned wavelength", 
)

xrds.band.attrs = dict( 
	standard_name = "wavenumber", 
	long_name = "Wavenumber", 
	units = "cm⁻¹", 
	step = 2, 
)
```


