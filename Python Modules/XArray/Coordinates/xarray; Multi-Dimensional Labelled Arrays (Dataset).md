
https://coderzcolumn.com/tutorials/python/xarray-dataset-multi-dimensional-labelled-arrays#:~:text=In%20the%20next%20cell%2C%20we%20have%20created%20a%20Dataset%20where%20we%20have%20created%20coordinate%20by%20combining%20two%20dimensions%20%27x%27%20and%20%27y%27.
***
***
**2023.10.25**
**Web page ref to a line**
1. Copy the line 
2. Get the encoded text from site like this: https://meyerweb.com/eric/tools/dencoder/
3. Lastly,  combine: `url` + `#:~:text=` + `encoded text`
***
***

In the next cell, we have created a **Dataset** where we have created coordinate by combining two dimensions **'x'** and **'y'**.

- Please make a **NOTE** of how we have provided value to **'index1'** coordinate.
- The value of the dictionary is a tuple with two elements. The first element is again a tuple of two strings that specifies which dimensions it combines.
-  The second value is an array that has the same shape as the combined shape of dimensions **'x'** and **'y'**.
- We have created an array of integers in the range 0-14 and reshaped them as **(3,5)** array to be used as a coordinate value. 
	- When we'll perform indexing on this dataset, value from **'index1'** coordinates will be selected based on **'x'** and **'y'** dimension values used for indexing (E.g - x=0,y=0, index1=0, x=0:2, y=0:2, index1=0,1,3,4).
-  This example explains how we can store some extra information inside of coordinates which can be useful to link more related data.
  
- In order to perform indexing on this **Dataset**, we'll still need to provide all three **'x,y, and z'** dimensions. But we are storing extra details as **'index1'** coordinate which can be a requirement in some situations.

```python
arr1 = np.random.randint(1,100,size=(3, 5,7))
arr2 = np.random.randn(3, 5, 7)

dataset5 = xr.Dataset(data_vars={"Array1": (("x","y","z"), arr1),
                                 "Array2": (("x","y","z"), arr2)},
                      coords={"index1": (("x","y"), np.arange(15).reshape(3,5)),
                              "z": np.arange(7)
                            })

dataset5
```


- Our next example explains how we can combine a different kind of data with **Dataset** object.
- Our dataset consists of 6 different arrays of shape **(3,5,7)**. They all represent measurements of different attributes.
- The dataset has 3 dimensions which are named **'x,y and time'**. All dimension names are specified in the dictionary given to **data_vars** parameter.
  
- The dictionary is given to **coords** parameter creates two new coordinates named **lon** and **lat** which combines dimensions **'x and y'**.
- The value of **'lon'** coordinate is a tuple of two values where the first value is a tuple of two strings representing dimensions and the second value is an array of shape **(3,5)** representing coordinate values. 
- The value of **'lat'** coordinate follows the same structure. 
- The **'time'** dimension is used as it is to represent coordinates in that dimension. We have specified a list of seven dates as the value of **time** coordinate using **pandas.date_range()** function.

- When we'll index our dataset by specifying values for **'x,y, and z'** dimensions, we'll get unique measurements of temperature,
- The location is represented using longitude and latitude which are specified as coordinates and not as part of the data dictionary provided to **data_vars** parameter.
  
```python
temperature = np.random.randint(1,100, size=(3,5,7))
humidity = np.random.randn(3, 5, 7)
pressure = np.random.randn(3, 5, 7)
windspeed = np.random.randn(3, 5, 7)
precitipation = np.random.randn(3, 5, 7)
pm25 = np.random.randn(3, 5, 7)

dataset6 = xr.Dataset(data_vars={"Temperature": (("x","y","time"), temperature),
                                 "Humidity": (("x","y","time"), humidity),
                                 "Pressure": (("x","y","time"), pressure),
                                 "WindSpeed": (("x","y","time"), windspeed),
                                 "Precipitation": (("x","y","time"), precitipation),
                                 "PM25": (("x","y","time"), pm25),
                                },
                      coords={"lon": (("x","y"), np.linspace(1,15,15).reshape(3,5)),
                              "lat": (("x","y"), np.linspace(15,30,15).reshape(3,5)),
                              "time": pd.date_range(start="2021-01-01", periods=7)
                                },
                      attrs={"Summary": "Dataset holds information like temperature, humidity. pressure, windspeed, precipitation and pm 2.5 particle presence based on location (lon, lat) and time.",
                             "lon": "Longitude",
                             "lat": "Latitude",
                             "time": "Date of Record"
                            }
                     )

dataset6
```
  


#### ones_like()[](https://coderzcolumn.com/tutorials/python/xarray-dataset-multi-dimensional-labelled-arrays#ones_like())

- The **ones_like()** method works like its counterpart in `numpy`. 
- It takes as input **Dataset** object and returns another **Dataset** object which has same dimensions as input **Dataset** but all values in the **Dataset** are replaced with **1s**.
```python
xr.ones_like(dataset6)
```

#### zeros_like()[](https://coderzcolumn.com/tutorials/python/xarray-dataset-multi-dimensional-labelled-arrays#zeros_like())

- The **zeros_like()** method works exactly like **ones_like()** but all the values of **Dataset** are **0s**.

```
xr.zeros_like(dataset6)
```

#### full_like()[](https://coderzcolumn.com/tutorials/python/xarray-dataset-multi-dimensional-labelled-arrays#full_like())

The **full_like()** method takes as input **Dataset** object and another value.
```
xr.full_like(dataset6, 101)
```



## 2. Dataset Attributes

The **attrs** attribute returns dictionary of **Dataset** attributes.

```
dataset6.attrs
```

- The **coords** attribute returns coordinates of the dataset. 
- We can extract individual coordinates values by treating the output of **coords** attribute as a dictionary. 
- Each individual coordinate is represented using **xarray DataArray** object.

```
dataset6.coords
```


The **data_vars** attribute returns data that we provided when creating a **Dataset**.
```
dataset6.data_vars
```

We can access individual data from a list of data by treating the output of **data_vars** as a dictionary. The result will be **xarray DataArray** object.

We can access dimensions of **Dataset** using **dims** attribute.
```
dataset6.dims
```

The **indexes** attribute returns indices of different dimensions of **Dataset**.

```
dataset3.indexes
```