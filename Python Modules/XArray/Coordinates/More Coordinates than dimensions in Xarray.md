
https://www.reddit.com/r/learnpython/comments/14dzfa1/more_coordinates_than_dimensions_in_xarray/


"According to the [documentation](https://docs.xarray.dev/en/stable/user-guide/terminology.html), "A DataArray can have more coordinates than dimensions because a single dimension can be labeled by multiple coordinate arrays. ""

```python
import xarray as xr
import numpy as np

# Create coordinate arrays
time = xr.DataArray(np.arange(5), dims='time', coords={'time': ('time', np.arange(5))}) 
latitude = xr.DataArray(np.arange(3), dims='latitude', coords={'latitude': ('latitude', np.arange(3))}) 
longitude = xr.DataArray(np.arange(4), dims='longitude', coords={'longitude': ('longitude', np.arange(4))})

data = xr.DataArray(np.random.rand(5, 3, 4), dims=('time', 'latitude', 'longitude'), coords={'time': time, 'latitude': latitude, 'longitude': longitude})

# works: add a non dimension coordinate
data.coords['stat'] = 'the'

## doesn't work: add a coordinate array to existing dimension
# data.coords['station'] = ['IAH', 'HOU', 'GLS', 'HOU', 'IAH' ]
# Following works
data.coords['station'] = ('time',['IAH', 'HOU', 'GLS', 'HOU', 'IAH' ])
print(data)
```

***
https://www.reddit.com/r/learnpython/comments/14dzfa1/comment/jothmgd/?utm_source=share&utm_medium=web2x&context=3

"The code you provided does not work because you are trying to add a coordinate array to an existing dimension. In order to add multiple coordinate arrays to a single dimension, you need to create a new dimension with the same name as the existing dimension. For example, the following code will create a DataArray with two coordinate arrays on the time dimension:"

```python
import xarray as Xr 
import numpy as np 

# Create coordinate arrays 
time = Xr.DataArray(np.arange(5), dims='time', coords={'time': ('time', np.arange(5))}) 
latitude = Xr.DataArray(np.arange(3), dims='latitude', coords={'latitude': ('latitude', np.arange(3))}) 
longitude = Xr.DataArray(np.arange(4), dims='longitude', coords={'longitude': ('longitude', np.arange(4))}) 

# Create a new dimension with the same name as the existing dimension 
#time2 = Xr.DataArray(np.arange(5), dims='time2', coords={'time2': ('time2', np.arange(5))}) 
# Following works
time2 = Xr.DataArray(np.arange(5)**2, dims='time', coords={'time2': ('time', np.arange(5))})

# Create a DataArray with multiple coordinate arrays on the time dimension 
data = Xr.DataArray(np.random.rand(5, 3, 4), dims=('time', 'latitude', 'longitude'), 
                    coords={'time': time, 'time2': time2, 'latitude': latitude, 'longitude': longitude}) 

print(data)
```

Error-1
https://www.reddit.com/r/learnpython/comments/14dzfa1/comment/jown4vo/?utm_source=share&utm_medium=web2x&context=3
```bash
ValueError: coordinate time2 has dimensions ('time2',), but these are not a subset of the DataArray dimensions ('time', 'latitude', 'longitude')
```

Solution
https://www.reddit.com/r/learnpython/comments/14dzfa1/comment/jowohlz/?utm_source=share&utm_medium=web2x&context=3
```python
timer = Xr.DataArray(np.arange(5)**2, dims='time', coords={'time2': ('time', np.arange(5))})
```