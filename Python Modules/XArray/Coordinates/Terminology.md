
https://docs.xarray.dev/en/stable/user-guide/terminology.html


###### Coordinate[](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Coordinate "Permalink to this term")

An array that labels a dimension or set of dimensions of another `DataArray`. In the usual one-dimensional case, the coordinate array’s values can loosely be thought of as tick labels along a dimension. We distinguish [Dimension coordinate](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Dimension-coordinate) vs. [Non-dimension coordinate](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Non-dimension-coordinate) and [Indexed coordinate](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Indexed-coordinate) vs. [Non-indexed coordinate](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Non-indexed-coordinate). A coordinate named `x` can be retrieved from `arr.coords[x]`. A `DataArray` can have more coordinates than dimensions because a single dimension can be labeled by multiple coordinate arrays. However, only one coordinate array can be a assigned as a particular dimension’s dimension coordinate array. As a consequence, `len(arr.dims) <= len(arr.coords)` in general.


###### Dimension coordinate[](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Dimension-coordinate "Permalink to this term")

A one-dimensional coordinate array assigned to `arr` with both a name and dimension name in `arr.dims`. Usually (but not always), a dimension coordinate is also an [Indexed coordinate](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Indexed-coordinate) so that it can be used for label-based indexing and alignment, like the index found on a [`pandas.DataFrame`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html#pandas.DataFrame "(in pandas v2.1.1)") or [`pandas.Series`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.html#pandas.Series "(in pandas v2.1.1)").


###### Non-dimension coordinate[](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Non-dimension-coordinate "Permalink to this term")

A coordinate array assigned to `arr` with a name in `arr.coords` but _not_ in `arr.dims`. These coordinates arrays can be one-dimensional or multidimensional, and they are useful for auxiliary labeling. As an example, multidimensional coordinates are often used in geoscience datasets when [the data’s physical coordinates (such as latitude and longitude) differ from their logical coordinates](https://docs.xarray.dev/en/stable/examples/multidimensional-coords.html). Printing `arr.coords` will print all of `arr`’s coordinate names, with the corresponding dimension(s) in parentheses. For example, `coord_name (dim_name) 1 2 3 ...`.



###### Indexed coordinate[](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Indexed-coordinate "Permalink to this term")

A coordinate which has an associated [Index](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Index). Generally this means that the coordinate labels can be used for indexing (selection) and/or alignment. An indexed coordinate may have one or more arbitrary dimensions although in most cases it is also a [Dimension coordinate](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Dimension-coordinate). It may or may not be grouped with other indexed coordinates depending on whether they share the same index. Indexed coordinates are marked by `*` when printing a `DataArray` or `Dataset`.


###### Non-indexed coordinate[](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Non-indexed-coordinate "Permalink to this term")

A coordinate which has no associated [Index](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Index). It may still represent fixed labels along one or more dimensions but it cannot be used for label-based indexing and alignment.


###### Index[](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Index "Permalink to this term")

An _index_ is a data structure optimized for efficient data selection and alignment within a discrete or continuous space that is defined by coordinate labels (unless it is a functional index). By default, Xarray creates a `PandasIndex` object (i.e., a [`pandas.Index`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Index.html#pandas.Index "(in pandas v2.1.1)") wrapper) for each [Dimension coordinate](https://docs.xarray.dev/en/stable/user-guide/terminology.html#term-Dimension-coordinate). For more advanced use cases (e.g., staggered or irregular grids, geospatial indexes), Xarray also accepts any instance of a specialized `Index` subclass that is associated to one or more arbitrary coordinates. The index associated with the coordinate `x` can be retrieved by `arr.xindexes[x]` (or `arr.indexes["x"]` if the index is convertible to a [`pandas.Index`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Index.html#pandas.Index "(in pandas v2.1.1)") object). If two coordinates `x` and `y` share the same index, `arr.xindexes[x]` and `arr.xindexes[y]` both return the same `Index` object.
