
https://xarray-spatial.org/v0.0.9/zonal_statistics.html



`xarray-spatial` implements common raster analysis functions using Numba and provides an easy-to-install, easy-to-extend codebase for raster analysis.

https://xarray-spatial.org/v0.0.9/index.html

