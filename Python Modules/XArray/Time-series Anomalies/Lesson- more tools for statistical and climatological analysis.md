
https://fabienmaussion.info/climate_system/week_03/01_Lesson_MoreDataCrunching.html


### Welcome[#](https://fabienmaussion.info/climate_system/welcome.html#welcome "Permalink to this headline")

Practicals of the master lecture [707712 - Physics of the Climate System](https://lfuonline.uibk.ac.at/public/lfuonline_lv.details?sem_id_in=22W&lvnr_id_in=707712&sprache_in=en), taught to 1st semester students following a [Master in Atmospheric Sciences](https://www.uibk.ac.at/acinn/studies/master-in-atmospheric-sciences.html.en) curriculum from the Department of Atmospheric and Cryospheric Sciences ([ACINN](https://www.uibk.ac.at/acinn)) at the University of Innsbruck.

https://fabienmaussion.info/climate_system/welcome.html