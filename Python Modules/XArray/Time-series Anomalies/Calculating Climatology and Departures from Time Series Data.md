
https://xcdat.readthedocs.io/en/latest/examples/climatology-and-departures.html


### xCDAT: Xarray Climate Data Analysis Tools

xCDAT is an extension of [xarray](https://github.com/pydata/xarray) for climate data analysis on structured grids. It serves as a modern successor to the Community Data Analysis Tools ([CDAT](https://github.com/CDAT/cdat)) library.


#### Project Motivation[#](https://xcdat.readthedocs.io/en/latest/index.html#project-motivation "Permalink to this heading")

The goal of xCDAT is to provide generalizable features and utilities for simple and robust analysis of climate data. xCDAT’s design philosophy is focused on reducing the overhead required to accomplish certain tasks in xarray. xCDAT aims to be compatible with structured grids that are [CF-compliant](https://cfconventions.org/) (e.g., CMIP6). Some key xCDAT features are inspired by or ported from the core CDAT library, while others leverage powerful libraries in the xarray ecosystem (e.g., [xESMF](https://pangeo-xesmf.readthedocs.io/en/latest/), [xgcm](https://xgcm.readthedocs.io/en/latest/index.html), [cf_xarray](https://cf-xarray.readthedocs.io/en/latest/index.html)) to deliver robust APIs.

The xCDAT core team’s mission is to provide a maintainable and extensible package that serves the needs of the climate community in the long-term. We are excited to be working on this project and hope to have you onboard!


https://xcdat.readthedocs.io/en/latest/index.html#


### xgcm: General Circulation Model Postprocessing with xarray

**xgcm** is a python packge for working with the datasets produced by numerical [General Circulation Models](https://en.wikipedia.org/wiki/General_circulation_model) (GCMs) and similar gridded datasets that are amenable to [finite volume](https://en.wikipedia.org/wiki/Finite_volume_method) analysis. In these datasets, different variables are located at different positions with respect to a volume or area element (e.g. cell center, cell face, etc.) xgcm solves the problem of how to interpolate and difference these variables from one position to another.
https://xgcm.readthedocs.io/en/latest/index.html


### # Welcome to `cf_xarray`

`cf_xarray` mainly provides an accessor (`DataArray.cf` or `Dataset.cf`) that allows you to interpret [Climate and Forecast metadata convention](http://cfconventions.org/) attributes present on [xarray](https://xarray.pydata.org/) objects.

https://cf-xarray.readthedocs.io/en/latest/index.html


