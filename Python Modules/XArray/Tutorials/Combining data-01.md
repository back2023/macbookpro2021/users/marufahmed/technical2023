## Combining data (docs.xarray.dev)
https://docs.xarray.dev/en/stable/user-guide/combining.html

### Combining along multiple dimensions
1. For combining many objects along multiple dimensions xarray provides [`combine_nested()`](https://docs.xarray.dev/en/stable/generated/xarray.combine_nested.html#xarray.combine_nested "xarray.combine_nested") and [`combine_by_coords()`](https://docs.xarray.dev/en/stable/generated/xarray.combine_by_coords.html#xarray.combine_by_coords "xarray.combine_by_coords").
2. [`combine_nested()`](https://docs.xarray.dev/en/stable/generated/xarray.combine_nested.html#xarray.combine_nested "xarray.combine_nested") requires specifying the order in which the objects should be combined, while [`combine_by_coords()`](https://docs.xarray.dev/en/stable/generated/xarray.combine_by_coords.html#xarray.combine_by_coords "xarray.combine_by_coords") attempts to infer this ordering automatically from the coordinates in the data.
3. These functions can be used by [`open_mfdataset()`](https://docs.xarray.dev/en/stable/generated/xarray.open_mfdataset.html#xarray.open_mfdataset "xarray.open_mfdataset") to open many files as one dataset. The particular function used is specified by setting the argument `'combine'` to `'by_coords'` or `'nested'`. This is useful for situations where your data is split across many files in multiple locations, which have some known relationship between one another.


