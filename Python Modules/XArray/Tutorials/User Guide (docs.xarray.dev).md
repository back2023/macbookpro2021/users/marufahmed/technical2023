https://docs.xarray.dev/en/stable/user-guide/index.html

## Terminology
https://docs.xarray.dev/en/stable/user-guide/terminology.html

### Dimension
In xarray, a `DataArray` object’s _dimensions_ are its named dimension axes, and the name of the `i`-th dimension is `arr.dims[i]`. If an array is created without dimension names, the default dimension names are `dim_0`, `dim_1`, and so forth.

### Coordinate
1. An array that labels a dimension or set of dimensions of another `DataArray`.
2. There are two types of coordinate arrays: _dimension coordinates_ and _non-dimension coordinates_. 
3. A `DataArray` can have more coordinates than dimensions because a single dimension can be labeled by multiple coordinate arrays.

### Dimension coordinate
1. A one-dimensional coordinate array assigned to `arr` with both a name and dimension name in `arr.dims`.
2. Dimension coordinates are used for label-based indexing and alignment
3. Dimension coordinates are marked by `*` when printing a `DataArray` or `Dataset`.

### Non-dimension coordinate
1. A coordinate array assigned to `arr` with a name in `arr.coords` but _not_ in `arr.dims`.
2. However, non-dimension coordinates are not indexed, and any operation on non-dimension coordinates that leverages indexing will fail.

### Index
1. Xarray creates indexes for dimension coordinates so that operations along dimensions are fast, 
2. while non-dimension coordinates are not indexed.


## Data Structures

### DataArray
1. [`xarray.DataArray`](https://docs.xarray.dev/en/stable/generated/xarray.DataArray.html#xarray.DataArray "xarray.DataArray") is xarray’s implementation of a labeled, multi-dimensional array.
2. It has several key properties:
	-   `values`: a [`numpy.ndarray`](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html#numpy.ndarray "(in NumPy v1.24)") holding the array’s values
    
	-   `dims`: dimension names for each axis (e.g., `('x', 'y', 'z')`)
    
	-   `coords`: a dict-like container of arrays (_coordinates_) that label each point (e.g., 1-dimensional arrays of numbers, datetime objects or strings)
    
	-   `attrs`: [`dict`](https://docs.python.org/3/library/stdtypes.html#dict "(in Python v3.11)") to hold arbitrary metadata (_attributes_)
	  
3. The array values in a [`DataArray`](https://docs.xarray.dev/en/stable/generated/xarray.DataArray.html#xarray.DataArray "xarray.DataArray") have a single (homogeneous) data type. To work with heterogeneous or structured data types in xarray, use coordinates, or put separate `DataArray` objects in a single [`Dataset`](https://docs.xarray.dev/en/stable/generated/xarray.Dataset.html#xarray.Dataset "xarray.Dataset")

### Dataset
1. [`xarray.Dataset`](https://docs.xarray.dev/en/stable/generated/xarray.Dataset.html#xarray.Dataset "xarray.Dataset") is xarray’s multi-dimensional equivalent of a [`DataFrame`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html#pandas.DataFrame "(in pandas v1.5.3)").
2. It is a dict-like container of labeled arrays ([`DataArray`](https://docs.xarray.dev/en/stable/generated/xarray.DataArray.html#xarray.DataArray "xarray.DataArray") objects) with aligned dimensions.
3. It is designed as an in-memory representation of the data model from the [netCDF](https://www.unidata.ucar.edu/software/netcdf/) file format.

An example of how we might structure a dataset for a weather forecast:
![[dataset-diagram.webp]]