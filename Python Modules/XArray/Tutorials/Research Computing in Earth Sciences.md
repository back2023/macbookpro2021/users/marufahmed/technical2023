Xarray Fundamentals
https://rabernat.github.io/research_computing_2018/xarray.html

### Xarray data structures
xarray has two fundamental data structures:
-   a `DataArray`, which holds a single multi-dimensional variable and its coordinates
-   a `Dataset`, which holds multiple variables that potentially share the same coordinates

A `DataArray` has four essential attributes:
-   `values`: a `numpy.ndarray` holding the array’s values
-   `dims`: dimension names for each axis (e.g., `('x', 'y', 'z')`)
-   `coords`: a dict-like container of arrays (coordinates) that label each point (e.g., 1-dimensional arrays of numbers, datetime objects or strings)
-   `attrs`: an `OrderedDict` to hold arbitrary metadata (attributes)

A Dataset holds many DataArrays which potentially can share coordinates.


### Coordinates vs. Data Variables 

Data variables can be modified through arithmentic operations or other functions. Coordinates are always keept the same.

1. The `*` symbol in the representation  indicates "dimension coordinates" (they describe the coordinates associated with data variable axes) 
2. Others are "non-dimension coordinates". We can make any variable a non-dimension coordiante.

### Working with Labeled Data
1. Can use regular numpy indexing and slicing on DataArrays
2. Use xarray's `.sel()` method to use label-based indexing.
3. `.sel()` also supports slicing.
4. `.sel()` also works on the whole Dataset

### Computation
1. Xarray dataarrays and datasets work seamlessly with arithmetic operators and numpy array functions.
2. can also combine multiple xarray datasets in arithemtic operations

### Broadcasting
1. an operation on arrays with differenty coordinates will result in automatic broadcasting


### Reductions
1. can reduce xarray DataArrays along any number of axes.
2. can perform them on dimensions.



