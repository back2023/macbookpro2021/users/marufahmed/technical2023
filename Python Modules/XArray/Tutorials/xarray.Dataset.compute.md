
Dataset.compute(_**kwargs_)[[source]](https://github.com/pydata/xarray/blob/main/xarray/core/dataset.py#L927-L947)[](https://xarray.pydata.org/en/v2023.05.0/generated/xarray.Dataset.compute.html#xarray.Dataset.compute "Permalink to this definition")

Manually trigger loading and/or computation of this dataset’s data from disk or a remote source into memory and return a new dataset. Unlike load, the original dataset is left unaltered.

Normally, it should not be necessary to call this method in user code, because all xarray functions should either work on deferred data or load data automatically. However, this method can be necessary when working with many file objects on disk.


https://xarray.pydata.org/en/v2023.05.0/generated/xarray.Dataset.compute.html

