
https://climate-cms.org/posts/2019-07-29-multi-apply-along-axis.html

We’ve had a few questions lately about how to calculate a time correlation of two different models at every grid point. Numpy’s `apply_along_axis()` function is super useful for applying a function to the time axis at each grid point of a single array, but what if we have more than one input array we need to do a calculation on?

To accomplish this we can use a trick - we can combine all the input arrays into a single big array that we can call `apply_along_axis()` on, and use a helper function to split the time axis of the big array back into its component pieces.


