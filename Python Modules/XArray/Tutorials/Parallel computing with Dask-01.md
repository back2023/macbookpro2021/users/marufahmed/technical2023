## Parallel computing with Dask
https://docs.xarray.dev/en/stable/user-guide/dask.html

### What is a Dask array
1. Unlike NumPy, which has eager evaluation, operations on Dask arrays are lazy.
2. Operations queue up a series of tasks mapped over blocks

### Reading and writing data
1. to create a `Dataset` filled with Dask arrays is to load the data from a netCDF file or files supply a `chunks` argument to [`open_dataset()`](https://docs.xarray.dev/en/stable/generated/xarray.open_dataset.html#xarray.open_dataset "xarray.open_dataset") or using the [`open_mfdataset()`](https://docs.xarray.dev/en/stable/generated/xarray.open_mfdataset.html#xarray.open_mfdataset "xarray.open_mfdataset") function.
2. It is also entirely equivalent to opening a dataset using [`open_dataset()`](https://docs.xarray.dev/en/stable/generated/xarray.open_dataset.html#xarray.open_dataset "xarray.open_dataset") and then chunking the data using the `chunk` method
3. To open multiple files simultaneously in parallel using Dask delayed, use `open_mfdataset()`.
4. This function will automatically concatenate and merge datasets into one in the simple cases that it understands (see [`combine_by_coords()`](https://docs.xarray.dev/en/stable/generated/xarray.combine_by_coords.html#xarray.combine_by_coords "xarray.combine_by_coords") for the full disclaimer). 
5. By default, [`open_mfdataset()`](https://docs.xarray.dev/en/stable/generated/xarray.open_mfdataset.html#xarray.open_mfdataset "xarray.open_mfdataset") will chunk each netCDF file into a single Dask array; again, supply the `chunks` argument to control the size of the resulting Dask arrays.
6. A dataset can also be converted to a Dask DataFrame using [`to_dask_dataframe()`](https://docs.xarray.dev/en/stable/generated/xarray.Dataset.to_dask_dataframe.html#xarray.Dataset.to_dask_dataframe "xarray.Dataset.to_dask_dataframe").


### Using Dask with xarray
1. Nearly all existing xarray methods (including those for indexing, computation, concatenating and grouped operations) have been extended to work automatically with Dask arrays.
2. When you load data as a Dask array in an xarray data structure, almost all xarray operations will keep it as a Dask array; when this is not possible, they will raise an exception rather than unexpectedly loading data into memory.
3. Converting a Dask array into memory generally requires an explicit conversion step. One notable exception is indexing operations: to enable label based indexing, xarray will automatically load coordinate labels into memory.

