## Reading and writing files (docs.xarray.dev)
https://docs.xarray.dev/en/stable/user-guide/io.html

### netCDF
1. The recommended way to store xarray data structures is [netCDF](https://www.unidata.ucar.edu/software/netcdf/)
2. Xarray is based on the netCDF data model, so netCDF files on disk directly correspond to [`Dataset`](https://docs.xarray.dev/en/stable/generated/xarray.Dataset.html#xarray.Dataset "xarray.Dataset") objects (more accurately, a group in a netCDF file directly corresponds to a [`Dataset`](https://docs.xarray.dev/en/stable/generated/xarray.Dataset.html#xarray.Dataset "xarray.Dataset") object.
3. Recent versions of netCDF are based on the even more widely used HDF5 file-format.
4. As netCDF files correspond to [`Dataset`](https://docs.xarray.dev/en/stable/generated/xarray.Dataset.html#xarray.Dataset "xarray.Dataset") objects, these functions internally convert the `DataArray` to a `Dataset` before saving, and then convert back when loading, ensuring that the `DataArray` that is loaded is always exactly the same as the one that was saved.
5. Data is _always_ loaded lazily from netCDF files.

#### Reading multi-file datasets
1. NetCDF files are often encountered in collections, e.g., with different files corresponding to different model runs or one file per timestamp. 
    Xarray can straightforwardly combine such files into a single Dataset by making use of [`concat()`](https://docs.xarray.dev/en/stable/generated/xarray.concat.html#xarray.concat "xarray.concat"), [`merge()`](https://docs.xarray.dev/en/stable/generated/xarray.merge.html#xarray.merge "xarray.merge"), [`combine_nested()`](https://docs.xarray.dev/en/stable/generated/xarray.combine_nested.html#xarray.combine_nested "xarray.combine_nested") and [`combine_by_coords()`](https://docs.xarray.dev/en/stable/generated/xarray.combine_by_coords.html#xarray.combine_by_coords "xarray.combine_by_coords").
2. Xarray includes support for manipulating datasets that don’t fit into memory with [dask](http://dask.org/). If you have dask installed, you can open multiple files simultaneously in parallel using `open_mfdataset()` 
3. This function automatically concatenates and merges multiple files into a single xarray dataset. It is the recommended way to open multiple files with xarray.

###  Zarr
1. [Zarr](https://zarr.readthedocs.io/) is a Python package that provides an implementation of chunked, compressed, N-dimensional arrays.
2. Xarray can’t open just any zarr dataset, because xarray requires special metadata (attributes) describing the dataset dimensions and coordinates. 
	At this time, xarray can only open zarr datasets with these special attributes, such as zarr datasets written by xarray, [netCDF](https://docs.unidata.ucar.edu/nug/current/nczarr_head.html), or [GDAL](https://gdal.org/drivers/raster/zarr.html).