https://tristansalles.github.io/EnviReef/5-xarray/advanced.html


# [Welcome](https://tristansalles.github.io/EnviReef/welcome.html#welcome "Permalink to this headline")

Lecture notes of the Environmental Science master unit [ENVI5809 - Environmental Simulation Modelling](https://www.sydney.edu.au/courses/units-of-study/2021/envi/envi5809.html), taught to 2nd semester students at the University of Sydney.

https://tristansalles.github.io/EnviReef/welcome.html