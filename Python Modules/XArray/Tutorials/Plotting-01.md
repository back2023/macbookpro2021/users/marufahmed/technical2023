## Plotting
https://docs.xarray.dev/en/stable/user-guide/plotting.html

1. Xarray plotting functionality is a thin wrapper around the popular [matplotlib](https://matplotlib.org/) library.


### Two Dimensions 
1. The default method [`DataArray.plot()`](https://docs.xarray.dev/en/stable/generated/xarray.DataArray.plot.html#xarray.DataArray.plot "xarray.DataArray.plot") calls [`xarray.plot.pcolormesh()`](https://docs.xarray.dev/en/stable/generated/xarray.plot.pcolormesh.html#xarray.plot.pcolormesh "xarray.plot.pcolormesh") by default when the data is two-dimensional.
2. However, for large arrays, `imshow` can be much faster than `pcolormesh`.
3. Contour plot using `DataArray.plot.contour()`
4. Filled contour plot using `DataArray.plot.contourf()`
5. Surface plot using `DataArray.plot.surface()`
6. Since this is a thin wrapper around matplotlib, all the functionality of matplotlib is available.

### Faceting
1. Faceting here refers to splitting an array along one or two dimensions and plotting each group.
2. The easiest way to create faceted plots is to pass in `row` or `col` arguments to the xarray plotting methods/functions. This returns a `xarray.plot.FacetGrid` object. 

### Maps
1. you’ll need to have Cartopy installed
2. 