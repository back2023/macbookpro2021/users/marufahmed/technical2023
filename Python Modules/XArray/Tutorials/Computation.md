
The labels associated with [`DataArray`](https://docs.xarray.dev/en/stable/generated/xarray.DataArray.html#xarray.DataArray "xarray.DataArray") and [`Dataset`](https://docs.xarray.dev/en/stable/generated/xarray.Dataset.html#xarray.Dataset "xarray.Dataset") objects enables some powerful shortcuts for computation, notably including aggregation and broadcasting by dimension names.

https://docs.xarray.dev/en/stable/user-guide/computation.html