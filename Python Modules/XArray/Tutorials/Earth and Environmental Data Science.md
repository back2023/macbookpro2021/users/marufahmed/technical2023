1. This book grew out of a course developed at Columbia University called _Research Computing in Earth Science_.

### Combing Data: Concat and Merge
1. The ability to combine many smaller arrays into a single big Dataset is one of the main advantages of Xarray.
2. we need to learn two operations that help us combine data:
	- `xr.contact`: to concatenate multiple arrays into one bigger array along their dimensions
	-   `xr.merge`: to combine multiple different arrays into a dataset
3. Concat: let’s re-combine the subsetted data.
   Xarray won’t check the values of the coordinates before `concat`. It will just stick everything together into a new array.
4. We can also concat data along a _new_ dimension.
   The data is aligned using an _outer_ join along the non-concat dimensions.
5. We can merge both DataArrays and Datasets.





