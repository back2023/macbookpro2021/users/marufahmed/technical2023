
https://towardsdatascience.com/how-to-create-xarray-datasets-cf1859c95921
### Defining a dataset from scratch

`[Xarray](https://xarray.pydata.org/en/stable/)` is an open-source Python package for working with labeled multi-dimensional datasets. It is indispensable when working with [NetCDF](https://www.unidata.ucar.edu/software/netcdf/) formatted data, which is common in the Earth science community.


