
```bash
ValueError: did not find a match in any of xarray's currently installed IO backends ['netcdf4', 'scipy']. Consider explicitly selecting one of the installed engines via the ``engine`` parameter, or installing additional IO dependencies, see:
```
Try - 1 (NOT worked)
https://stackoverflow.com/a/67998060
```bash
cd /scratch/fp0/mah900/WeatherBench
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

conda install -c conda-forge netcdf4 h5netcdf scipy pydap zarr fsspec cftime rasterio cfgrib pooch
```
Try - 2
This error message can also be triggered when simply having an error in the path to the file to be opened.
https://stackoverflow.com/a/74641999
```bash
# added /* to dir path 
 '/g/data/rt52/era5/pressure-levels/reanalysis/cc/1959/*'
```
