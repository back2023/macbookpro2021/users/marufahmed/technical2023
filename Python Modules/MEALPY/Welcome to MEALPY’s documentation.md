

MEALPY is a Python library that contains the largest number of the cutting-edge population-based _meta-heuristic algorithms_ — a field that provides a fast and efficient way to find the (approximation) global optimal point of mathematical optimization problems.

Population-based meta-heuristic algorithms (PMAs) are the most popular algorithms in the field of optimization. There are several types of PMAs, including:

- Evolutionary inspired computing
- Swarm inspired computing
- Physics inspired computing
- Human inspired computing
- Biology inspired computing
- Mathematical inspired computing
- And others such as: Music inspired, System inspired computing,…

https://mealpy.readthedocs.io/en/latest/index.html


