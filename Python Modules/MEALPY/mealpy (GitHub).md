
# Introduction

MEALPY is the largest python library for most of the cutting-edge nature-inspired meta-heuristic algorithms (population-based). Population meta-heuristic algorithms (PMA) are the most popular algorithms in the field of approximate optimization.

- **Free software:** GNU General Public License (GPL) V3 license
- **Total algorithms**: 209 (129 original, 46 official variants, 34 developed variants)
- **Documentation:** [https://mealpy.readthedocs.io/en/latest/](https://mealpy.readthedocs.io/en/latest/)
- **Python versions:** >=3.7x
- **Dependencies:** numpy, scipy, pandas, matplotlib

### [](https://github.com/thieu1995/mealpy#goals)Goals

Our goals are to implement all of the classical as well as the state-of-the-art nature-inspired algorithms, create a simple interface that helps researchers access optimization algorithms as quickly as possible, and share knowledge of the optimization field with everyone without a fee. What you can do with mealpy:

- Analyse parameters of meta-heuristic algorithms.
- Perform Qualitative and Quantitative Analysis of algorithms.
- Analyse rate of convergence of algorithms.
- Test and Analyse the scalability and the robustness of algorithms.
- Save results in various formats (csv, json, pickle, png, pdf, jpeg)
- Export and import models can also be done with Mealpy.

https://github.com/thieu1995/mealpy

