
https://github.com/ecmwf/skinnywms


he skinny WMS is a small WMS server that will help you to visualise your NetCDF and Grib Data. The principle is simple: skinny will browse the directory, or the single file passed as argument, and try to interpret each NetCDF or GRIB files. From the metadata, it will be built the getCapabilities document, and find a relevant style to plot the data.

[![Docs](https://camo.githubusercontent.com/740c335b04f9e93d56cb5e78f69ef7ff7a29dff5d78b6689d5c1fa46ae50af8c/68747470733a2f2f696d672e736869656c64732e696f2f72656164746865646f63732f736b696e6e79776d733f6c6162656c3d72656164253230746865253230646f6373)](https://skinnywms.readthedocs.io/) [![Upload Python Package](https://github.com/ecmwf/skinnywms/actions/workflows/python-publish.yml/badge.svg)](https://github.com/ecmwf/skinnywms/actions) [![PyPI version](https://camo.githubusercontent.com/51e4d3512aa59a40e92ee05246e29aa5fc0a5dd6434d6a1a1c7fd5e1ea6a42c1/68747470733a2f2f62616467652e667572792e696f2f70792f736b696e6e79776d732e737667)](https://badge.fury.io/py/skinnywms) [![Upload Python Package](https://github.com/ecmwf/skinnywms/actions/workflows/docker-release.yml/badge.svg)](https://github.com/ecmwf/skinnywms/actions) [![Docker Pulls](https://camo.githubusercontent.com/ac9d61222c4bb8d879aeda57611e55b206b8c9d644eb678e36521850b3643e5c/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f70756c6c732f65636d77662f736b696e6e79776d73)](https://hub.docker.com/r/ecmwf/skinnywms) [![Anaconda Version](https://camo.githubusercontent.com/0821be628a8d78908c1a25cb9bec0e308ad27ce2080942a69423c983a71f594a/68747470733a2f2f616e61636f6e64612e6f72672f636f6e64612d666f7267652f736b696e6e79776d732f6261646765732f76657273696f6e2e737667)](https://anaconda.org/conda-forge/skinnywms) [![Anaconda Downloads](https://camo.githubusercontent.com/4b8ded0123284ce4c06f83c89566091afa52370f389e1508e19521a4b67374c3/68747470733a2f2f616e61636f6e64612e6f72672f636f6e64612d666f7267652f736b696e6e79776d732f6261646765732f646f776e6c6f6164732e737667)](https://anaconda.org/conda-forge/skinnywms) [![License](https://camo.githubusercontent.com/fe2d8cf079470b04983b02ae706cee63581981b793860efe9f1751037352b624/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6963656e73652f65636d77662f736b696e6e79776d73)](https://github.com/ecmwf/skinnywms)

## [Features:](https://github.com/ecmwf/skinnywms#features)

SkinnyWMS implements 3 of the WMS endpoints:

- **getCapabilities**: Discover the data, build an XML Document presenting each identified parameter in the file(s) as a layer with the list of their predefined styles. (There is always a default style)
- **getMap** : Return the selected layer suing the selected style.
- **getLegendGraphic**: Return the legend.


