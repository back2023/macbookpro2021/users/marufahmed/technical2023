
~~Welcome to KvikIO’s documentation!~~

https://docs.rapids.ai/api/kvikio/stable/

KvikIO is a Python and C++ library for high performance file IO. It provides C++ and Python bindings to [cuFile](https://docs.nvidia.com/gpudirect-storage/api-reference-guide/index.html), which enables [GPUDirect Storage](https://developer.nvidia.com/blog/gpudirect-storage/) (GDS). KvikIO also works efficiently when GDS isn’t available and can read/write both host and device data seamlessly.

KvikIO is a part of the [RAPIDS](https://rapids.ai/) suite of open-source software libraries for GPU-accelerated data science.