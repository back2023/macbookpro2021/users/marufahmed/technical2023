
https://doc.sagemath.org/html/en/prep/Intro-Tutorial.html

This [Sage](http://www.sagemath.org/) document is the first in a series of tutorials developed for the MAA PREP Workshop “Sage: Using Open-Source Mathematics Software with Undergraduates” (funding provided by NSF DUE 0817071). It is licensed under the Creative Commons Attribution-ShareAlike 3.0 license ([CC BY-SA](http://creativecommons.org/licenses/by-sa/3.0/)).

If you are unsure how to log on to a Sage server, start using a local installation, or to create a new worksheet, you might find the [prelude on logging in](https://doc.sagemath.org/html/en/prep/Logging-On.html) helpful.


