
**In this tutorial, you’ll learn about:**

- Storing images on disk as `.png` files
- Storing images in lightning memory-mapped databases (LMDB)
- Storing images in hierarchical data format (HDF5)

https://realpython.com/storing-images-in-python/





