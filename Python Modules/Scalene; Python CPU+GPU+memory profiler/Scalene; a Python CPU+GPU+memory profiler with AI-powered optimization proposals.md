

https://github.com/plasma-umass/scalene


## About Scalene

Scalene is a high-performance CPU, GPU _and_ memory profiler for Python that does a number of things that other Python profilers do not and cannot do. It runs orders of magnitude faster than many other profilers while delivering far more detailed information. It is also the first profiler ever to incorporate AI-powered proposed optimizations.


