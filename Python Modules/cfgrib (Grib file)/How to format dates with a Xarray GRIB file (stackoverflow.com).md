
https://stackoverflow.com/questions/71551608/how-to-format-dates-with-a-xarray-grib-file


```python
ds=xr.open_dataset('./Data/ecmwf_dataset/1month_anomaly_Global_ea_2t_197901_1981-2010_v02.grib',
                   engine='cfgrib',backend_kwargs={'indexpath': ''})


time = ds['time'].dt.strftime('%m-%d-%Y')
ax.set_title('Arctic 2-m Temperature Anomaly: \n' + time.values, size=fontsize, weight='bold')
```
