
https://github.com/ecmwf/cfgrib/issues/156

I followed the example in the README for writing a GRIB file from a simple Dataset
```python
import xarray as xr
import cfgrib

ds2 = xr.Dataset({'skin_temperature': (('latitude', 'longitude'), np.zeros((5, 6)) + 300.)})
ds2.coords['latitude'] = np.linspace(90., -90., 5)
ds2.coords['longitude'] = np.linspace(0., 360., 6, endpoint=False)
ds2.skin_temperature.attrs['GRIB_shortName'] = 'skt'

cfgrib.to_grib(ds2, 'out.grib2', grib_keys={'edition': 2})
```
That was successful.

Now, I would like to specify the datetime to the grib2 file, but when I add the coordinate `time`, `valid_time`, or `step` I get an error.


