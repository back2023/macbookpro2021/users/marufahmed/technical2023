
https://kpegion.github.io/Pangeo-at-AOES/examples/read-grib.html

we demonstrate: 
	1. How to read a grib file in Python using `xarray` 
	2. How to make a contour plot of the data

Read the data using `xarray` `open_dataset` [http://xarray.pydata.org/en/stable/generated/xarray.open_dataset.html](http://xarray.pydata.org/en/stable/generated/xarray.open_dataset.html)
```python
ds=xr.open_dataset(path+fname,engine='cfgrib',backend_kwargs={'indexpath': ''})
```

- We can also use `xarray` to select only the 500 hPa level using the coordinate information rather than having to identify the array index. This is done using the `xr.sel` method
```python
ds_z500=ds.sel(isobaricInhPa=500)
```
- We will use the `matplotlib` `plt.contourf` function for a filled contour plot. It works very similar to Matlab plotting functions.
```python
plt.contourf(ds_z500['z'])
plt.colorbar()
```








