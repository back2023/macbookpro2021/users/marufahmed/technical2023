
https://github.com/ecmwf/cfgrib

### Installation
```python
$ conda install -c conda-forge cfgrib
```

### Binary dependencies
- _cfgrib_ depends on the [eccodes python package](https://pypi.org/project/eccodes) to access the ECMWF _ecCodes_ binary library, when not using _conda_ please follow the _System dependencies_ section there.
- You may run a simple selfcheck command to ensure that your system is set up correctly:
```python
$ python -m cfgrib selfcheck
Found: ecCodes v2.20.0.
Your system is ready.
```


## Usage 
### Read-only _xarray_ GRIB engine 
```python
$ pip install xarray
```

```python
>>> import xarray as xr
>>> ds = xr.open_dataset('era5-levels-members.grib', engine='cfgrib')
>>> ds
```

The _cfgrib_ `engine` supports all read-only features of _xarray_ like:
	- merge the content of several GRIB files into a single dataset using `xarray.open_mfdataset`,
	- work with larger-than-memory datasets with [dask](https://dask.org/),
	- allow distributed processing with [dask.distributed](http://distributed.dask.org/).

### Read arbitrary GRIB keys

- By default _cfgrib_ reads a limited set of ecCodes recognised _keys_ from the GRIB files and exposes them as `Dataset` or `DataArray` attributes with the `GRIB_` prefix. 
	- It is possible to have _cfgrib_ read additional keys to the attributes by adding the `read_keys` dictionary key to the `backend_kwargs` with values the list of desired GRIB keys:
```python
>>> ds = xr.open_dataset('era5-levels-members.grib', engine='cfgrib',
...                      backend_kwargs={'read_keys': ['experimentVersionNumber']})
>>> ds.t.attrs['GRIB_experimentVersionNumber']
'0001'
```

### Translate to a custom data model 

- Contrary to netCDF the GRIB data format is not self-describing and several details of the mapping to the _Unidata Common Data Model_ are arbitrarily set by the software components decoding the format.
- Details like names and units of the coordinates are particularly important because _xarray_ broadcast and selection rules depend on them. 
- `cf2cfm` is a small coordinate translation module distributed with _cfgrib_ that make it easy to translate CF compliant coordinates, like the one provided by _cfgrib_, to a user-defined custom data model with set `out_name`, `units` and `stored_direction`.
```python
>>> import cf2cdm
>>> ds = xr.open_dataset('era5-levels-members.grib', engine='cfgrib')
>>> cf2cdm.translate_coords(ds, cf2cdm.ECMWF)
```

To translate to the Common Data Model of the Climate Data Store use:
```python
>>> import cf2cdm
>>> cf2cdm.translate_coords(ds, cf2cdm.CDS)
```


### [Filter heterogeneous GRIB files](https://github.com/ecmwf/cfgrib#filter-heterogeneous-grib-files)

- `xr.open_dataset` can open a GRIB file only if all the messages with the same `shortName` can be represented as a single hypercube. 
	- For example, a variable `t` cannot have both `isobaricInhPa` and `hybrid` `typeOfLevel`'s, as this would result in multiple hypercubes for the same variable. 
	- Opening a non-conformant GRIB file will fail with a `ValueError: multiple values for unique key...` error message, see [#2](https://github.com/ecmwf/cfgrib/issues/2).
- Furthermore if different variables depend on the same coordinate, for example `step`, the values of the coordinate must match exactly. 
	- For example, if variables `t` and `z` share the same `step` coordinate, they must both have exactly the same set of steps. 
	- Opening a non-conformant GRIB file will fail with a `ValueError: key present and new value is different...` error message, see [#13](https://github.com/ecmwf/cfgrib/issues/13).

- In most cases you can handle complex GRIB files containing heterogeneous messages by passing the `filter_by_keys` key in `backend_kwargs` to select which GRIB messages belong to a well formed set of hypercubes.
- For example to open [US National Weather Service complex GRIB2 files](http://ftpprd.ncep.noaa.gov/data/nccf/com/nam/prod/) you can use:
```python
>>> xr.open_dataset('nam.t00z.awp21100.tm00.grib2', engine='cfgrib',
...     backend_kwargs={'filter_by_keys': {'typeOfLevel': 'surface'}})
```

```python
>>> xr.open_dataset('nam.t00z.awp21100.tm00.grib2', engine='cfgrib',
...     backend_kwargs={'filter_by_keys': {'typeOfLevel': 'heightAboveGround', 'level': 2}})
```


### [Automatic filtering](https://github.com/ecmwf/cfgrib#automatic-filtering)
- _cfgrib_ also provides a function that automates the selection of appropriate `filter_by_keys` and returns a list of all valid `xarray.Dataset`'s in the GRIB file.
```python
>>> import cfgrib
>>> cfgrib.open_datasets('nam.t00z.awp21100.tm00.grib2')
```

## [Advanced usage](https://github.com/ecmwf/cfgrib#advanced-usage)

## [Write support](https://github.com/ecmwf/cfgrib#write-support)

- **Please note that write support is Alpha.** 
- Only `xarray.Dataset`'s in _canonical_ form, that is, with the coordinates names matching exactly the _cfgrib_ coordinates, can be saved at the moment:
```python
>>> from cfgrib.xarray_to_grib import to_grib
>>> ds = xr.open_dataset('era5-levels-members.grib', engine='cfgrib').sel(number=0)
>>> ds
...
>>> to_grib(ds, 'out1.grib', grib_keys={'edition': 2})
>>> xr.open_dataset('out1.grib', engine='cfgrib')
...

```

Per-variable GRIB keys can be set by setting the `attrs` variable with key prefixed by `GRIB_`, for example:
```python
>>> import numpy as np
>>> import xarray as xr
```

### [Dataset / Variable API](https://github.com/ecmwf/cfgrib#dataset--variable-api)

The use of _xarray_ is not mandatory and you can access the content of a GRIB file as an hypercube with the high level API in a Python interpreter:

```python
>>> ds = cfgrib.open_file('era5-levels-members.grib')
>>> ds.attributes['GRIB_edition']
1
>>> sorted(ds.dimensions.items())
[('isobaricInhPa', 2), ('latitude', 61), ('longitude', 120), ('number', 10), ('time', 4)]
>>> sorted(ds.variables)
['isobaricInhPa', 'latitude', 'longitude', 'number', 'step', 't', 'time', 'valid_time', 'z']
>>> var = ds.variables['t']
>>> var.dimensions
('number', 'time', 'isobaricInhPa', 'latitude', 'longitude')
>>> var.data[:, :, :, :, :].mean()
262.92133
>>> ds = cfgrib.open_file('era5-levels-members.grib')
>>> ds.attributes['GRIB_edition']
1
>>> sorted(ds.dimensions.items())
[('isobaricInhPa', 2), ('latitude', 61), ('longitude', 120), ('number', 10), ('time', 4)]
>>> sorted(ds.variables)
['isobaricInhPa', 'latitude', 'longitude', 'number', 'step', 't', 'time', 'valid_time', 'z']
>>> var = ds.variables['t']
>>> var.dimensions
('number', 'time', 'isobaricInhPa', 'latitude', 'longitude')
>>> var.data[:, :, :, :, :].mean()
262.92133
```

### [GRIB index file](https://github.com/ecmwf/cfgrib#grib-index-file)

- By default _cfgrib_ saves the index of the GRIB file to disk appending `.idx` to the GRIB file name. 
	- Index files are an **experimental** and completely optional feature, feel free to remove them and try again in case of problems. 
	- Index files saving can be disable passing adding `indexpath=''` to the `backend_kwargs` keyword argument.

### [Geographic Coordinate Caching](https://github.com/ecmwf/cfgrib#geographic-coordinate-caching)

- By default, _cfgrib_ caches computed geography coordinates for each record in the GRIB file when opening a dataset, which significantly speeds up dataset creation. 
	- This cache can theoretically grow unboundedly in memory in long-lived applications which read many different grid types. 
- Should it be necessary, caching can be disabled by passing:
	- `backend_kwargs=dict(cache_geo_coords=False)`  to `xarray.open_dataset()` , 
	- `cfgrib.open_dataset()` , or `cfgrib.open_datasets()` .
