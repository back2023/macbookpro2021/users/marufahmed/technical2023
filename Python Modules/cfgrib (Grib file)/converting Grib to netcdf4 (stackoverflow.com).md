
https://stackoverflow.com/questions/64937550/converting-grib-to-netcdf4

Yes there is a very comfortable way using `python`, `xarray` and `cfgrib` (requires ECCODES).

Installing cfgrib as mentioned here on the [github](https://github.com/ecmwf/cfgrib) page you will have all required grib tables in the eccodes installation.

Afterwards you just have to open your grib file:

```python
import xarray

data = xarray.open_dataset('path_to_grib_file.grib1', engine='cfgrib')
data.to_netcdf('netcdf_file.nc')
```



