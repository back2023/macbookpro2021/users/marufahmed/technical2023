
https://pythonhosted.org/Autologging/examples-traced.html


- [Trace all methods of a class using a module-named logger](https://pythonhosted.org/Autologging/examples-traced.html#module-traced-class)
- [Trace all methods of a class using a user-named logger](https://pythonhosted.org/Autologging/examples-traced.html#named-traced-class)
- [Trace only certain methods of a class](https://pythonhosted.org/Autologging/examples-traced.html#specific-traced-methods)
- [Trace a nested class](https://pythonhosted.org/Autologging/examples-traced.html#traced-nested-class)
- [Trace a function using a module-named logger](https://pythonhosted.org/Autologging/examples-traced.html#module-traced-function)
- [Trace a function using a user-named logger](https://pythonhosted.org/Autologging/examples-traced.html#named-traced-function)
- [Trace a nested function](https://pythonhosted.org/Autologging/examples-traced.html#traced-nested-function)


