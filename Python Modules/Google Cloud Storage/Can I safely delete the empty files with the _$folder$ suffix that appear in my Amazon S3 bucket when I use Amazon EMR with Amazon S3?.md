

The "_$folder$" files are placeholders. Apache Hadoop creates these files when you use the **-mkdir** command to create a folder in an S3 bucket. Hadoop doesn't create the folder until you [PUT](https://docs.aws.amazon.com/AmazonS3/latest/API/RESTObjectPUT.html) the first object. If you delete the "_$folder$" files before you PUT at least one object, Hadoop can't create the folder. This results in a "No such file or directory" error.

https://repost.aws/knowledge-center/emr-s3-empty-files

