
https://stackoverflow.com/questions/12575421/convert-a-1d-array-to-a-2d-array-in-numpy

 
```python
> import numpy as np
> A = np.array([1,2,3,4,5,6])

```


You want to [`reshape`](http://docs.scipy.org/doc/numpy/reference/generated/numpy.reshape.html) the array.
```python
B = np.reshape(A, (-1, 2))
```
where `-1` infers the size of the new dimension from the size of the input array.
Sorce: https://stackoverflow.com/a/12575451



If your sole purpose is to convert a 1d array X to a 2d array just do:
```python
X = np.reshape(X,(1, X.size))
```
Source: https://stackoverflow.com/a/59739493