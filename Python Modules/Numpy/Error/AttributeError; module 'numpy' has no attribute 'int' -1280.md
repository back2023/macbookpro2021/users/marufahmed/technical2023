
https://github.com/WongKinYiu/yolov7/issues/1280

|   |
|---|
|This happens because you have installed the latest numpy version 1.24.0 where `np.int` is depreciated. Instead install a lower version of numpy `pip install "numpy<1.24.0"`.<br><br>Or you can change the `np.int` to just `int`. However, there are still some issues with tensorboard logging with the latest numpy version, so better to downgrade.|
Source: https://github.com/WongKinYiu/yolov7/issues/1280#issuecomment-1360917977


