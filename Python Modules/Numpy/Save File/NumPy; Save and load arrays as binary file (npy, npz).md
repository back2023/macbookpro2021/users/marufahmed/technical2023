
https://note.nkmk.me/en/python-numpy-load-save-savez-npy-npz/


In NumPy, you can save arrays as NumPy-specific binary files (`npy`, `npz`). It is possible to save and load while maintaining information such as data type and shape.

This article describes the following contents.

- Advantages and disadvantages of saving in binary format
- Load `npy` and `npz`: `np.load()`
- Save a single array as `npy`: `np.save()`
- Save multiple arrays as `npz`: `np.savez()`
- Save multiple arrays as compressed `npz`: `np.savez_compressed()`

