

https://numpy.org/doc/stable/user/basics.creation.html


## Introduction[](https://numpy.org/doc/stable/user/basics.creation.html#introduction "Permalink to this heading")

There are 6 general mechanisms for creating arrays:

1. Conversion from other Python structures (i.e. lists and tuples)
2. Intrinsic NumPy array creation functions (e.g. arange, ones, zeros, etc.)
3. Replicating, joining, or mutating existing arrays
4. Reading arrays from disk, either from standard or custom formats
5. Creating arrays from raw bytes through the use of strings or buffers
6. Use of special library functions (e.g., random)

You can use these methods to create ndarrays or [Structured arrays](https://numpy.org/doc/stable/user/basics.rec.html#structured-arrays). This document will cover general methods for ndarray creation.