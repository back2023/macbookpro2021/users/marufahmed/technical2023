
This tutorial introduces PyGAD, an open-source Python library for implementing the genetic algorithm and training machine learning algorithms. PyGAD supports 19 parameters for customizing the genetic algorithm for various applications.

Within this tutorial we'll discuss 5 different applications of the genetic algorithm and build them using PyGAD.

The outline of the tutorial is as follows:

- PyGAD Installation
- Getting Started with PyGAD
- Fitting a Linear Model
- Reproducing Images
- 8 Queen Puzzle
- Training Neural Networks
- Training Convolutional Neural Networks

https://blog.paperspace.com/genetic-algorithm-applications-using-pygad/


