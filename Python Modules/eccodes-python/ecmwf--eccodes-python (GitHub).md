
https://github.com/ecmwf/eccodes-python

Python 3 interface to decode and encode GRIB and BUFR files via the [ECMWF ecCodes library](https://confluence.ecmwf.int/display/ECC/).

Features:

- reads and writes GRIB 1 and 2 files,
- reads and writes BUFR 3 and 4 files,
- supports all modern versions of Python 3.11, 3.10, 3.9, 3.8 and PyPy3,
- works on most _Linux_ distributions and _MacOS_, the _ecCodes_ C-library is the only system dependency,
- PyPI package can be installed without compiling, at the cost of being twice as slow as the original _ecCodes_ module,
- an optional compile step makes the code as fast as the original module but it needs the recommended (the most up-to-date) version of _ecCodes_.

Limitations:

- Microsoft Windows support is untested.





