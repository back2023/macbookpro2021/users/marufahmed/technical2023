

`google-colab` should be removed from pip given its misleading branding.

The package isn't authored by Google, and the libraries don't work outside of the Colab environment.

https://stackoverflow.com/questions/58476725/using-google-colab-auth-authenticate-user-on-local-machine-get-operationalerro

