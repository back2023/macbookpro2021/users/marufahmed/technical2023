
https://github.com/fossasia/visdom


### Creating, organizing & sharing visualizations of live, rich data. Supports [Python](https://pypi.org/project/visdom/).

Jump To: [Setup](https://github.com/fossasia/visdom#setup), [Usage](https://github.com/fossasia/visdom#usage), [API](https://github.com/fossasia/visdom#api), [Customizing](https://github.com/fossasia/visdom#customizing-visdom), [Contributing](https://github.com/fossasia/visdom#contributing), [License](https://github.com/fossasia/visdom#license)


Visdom aims to facilitate visualization of (remote) data with an emphasis on supporting scientific experimentation.  
Broadcast visualizations of plots, images, and text for yourself and your collaborators. Organize your visualization space programmatically or through the UI to create dashboards for live data, inspect results of experiments, or debug experimental code.
