https://www.star.nesdis.noaa.gov/atmospheric-composition-training/software_panoply_install.php


[**Panoply**](https://www.giss.nasa.gov/tools/panoply/) is a NASA-developed data viewer for netCDF, HDF and GRIB files. It allows users to quickly and easily open a satellite data file, examine the contents, and make a very basic plot of the data. It’s helpful for troubleshooting if you encounter errors when using Python to work with satellite data.

Follow the instructions below to download and install Panoply on your computer.