
https://unidata.github.io/netcdf4-python/
* netcdf4-python is a Python interface to the netCDF C library.
* [netCDF](http://www.unidata.ucar.edu/software/netcdf/) version 4 has many features not found in earlier versions of the library and is implemented on top of [HDF5](http://www.hdfgroup.org/HDF5).