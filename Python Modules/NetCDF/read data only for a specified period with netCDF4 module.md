https://stackoverflow.com/questions/51439723/how-to-read-data-only-for-a-specified-period-with-netcdf4-module

```python
import netCDF4
import dateutil.parser

nc = netCDF4.Dataset(file.nc, 'r')

# all_times variable includes the time:units attribute
all_times = nci.variables['time']

sdt = dateutil.parser.parse("2015-07-20T00:00:00")
edt = dateutil.parser.parse("2015-07-24T23:00:00")

st_idx = netCDF4.date2index(sdt, all_times)
et_idx = netCDF4.date2index(edt, all_times)

data = nc.variables['temperature'][st_idx:et_idx+1,:] #I want to read between 2015-07-20 00:00 to 2015-07-24 23:00
```
 