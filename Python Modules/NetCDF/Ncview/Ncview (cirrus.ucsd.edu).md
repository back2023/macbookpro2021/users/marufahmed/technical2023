
https://cirrus.ucsd.edu/ncview/

Ncview is a visual browser for [netCDF](https://www.unidata.ucar.edu/software/netcdf/) format files. Typically you would use ncview to get a quick and easy, push-button look at your netCDF files.