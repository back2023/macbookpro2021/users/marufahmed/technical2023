
https://cupy-xarray.readthedocs.io/source/cupy-basics.html


Welcome to the CuPy-Xarray Notebooks repository for Scipy-2023! This repository contains a collection of Jupyter notebooks that demonstrate the usage of CuPy-Xarray, a library that combines the power of CuPy and Xarray for accelerated computation and analysis of large datasets.


- As we can see in the above examples, CuPy arrays look just like NumPy arrays, except that Cupy arrays are stored on GPUs vs. Numpy arrays are stored on CPUs.

### Basic Operations[](https://cupy-xarray.readthedocs.io/source/cupy-basics.html#basic-operations "Permalink to this heading")

- CuPy provides equivalents for many common NumPy functions, although not all. Most of CuPy’s functions have the same function call as their NumPy counterparts.
- Cupy also provides equivalant functions for some SciPy functions, but its implementation is not as extensive as NumPy’s.
	- See [here](https://docs.cupy.dev/en/stable/reference/comparison.html) for a full list of CuPy’s Numpy and Scipy equivalent functions.
- Basic arithmetic operations is exactly identical between numpy and cupy.

- ### Data Transfer[](https://cupy-xarray.readthedocs.io/source/cupy-basics.html#data-transfer "Permalink to this heading")
#### Data Transfer to a Device[](https://cupy-xarray.readthedocs.io/source/cupy-basics.html#data-transfer-to-a-device "Permalink to this heading")
- `cupy.asarray()` can be used to move a numpy array to a device (GPU). 
#### Move array from GPU to the CPU[](https://cupy-xarray.readthedocs.io/source/cupy-basics.html#move-array-from-gpu-to-the-cpu "Permalink to this heading")
- Moving a device array to the host (i.e. CPU) can be done by `cupy.asnumpy()` as follows:
- We can also use `cupy.ndarray.get()`

### Device Information
- `cupy.ndarray.device` attribute can be used to determine the device allocated to a CUPY array
- To obtain the total number of accessible devices, you can utilize the getDeviceCount function.
  
- The default behavior runs code on Device 0, but we can transfer arrays other devices with CuPy using `cp.cuda.Device()`.
- If you want to change to a different GPU device, you can do so by utilizing the “device” context manager.

There are several reasons for this:

- The size of our arrays: The GPU’s performance relies on parallelism, processing thousands of values simultaneously. To fully leverage the GPU’s capabilities, we require a significantly larger array. As we see in the above example, for bigger matrix size we see more speed-ups.
    
- The simplicity of our calculation: Transferring a calculation to the GPU involves considerable overhead compared to executing a function on the CPU. If our calculation lacks a sufficient number of mathematical operations (known as “arithmetic intensity”), the GPU will spend most of its time waiting for data movement.
    
- Data copying to and from the GPU impacts performance: While including copy time can be realistic for a single function, there are instances where we need to execute multiple GPU operations sequentially. In such cases, it is advantageous to transfer data to the GPU and keep it there until all processing is complete.




# A real world example
https://cupy-xarray.readthedocs.io/source/real-example-1.html




## Advanced workflows and automatic parallelization using `apply_ufunc`

