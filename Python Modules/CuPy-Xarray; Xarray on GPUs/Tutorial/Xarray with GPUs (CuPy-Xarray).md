
https://negin513.github.io/cupy-xarray-tutorials/README.html













# Xarray and CuPy (Real Examples)
https://negin513.github.io/cupy-xarray-tutorials/notebooks/Notebook5_Xarray_Cupy_Example.html


```python
import numpy as np
import xarray as xr
import cupy as cp
import cupy_xarray  # Adds .cupy to Xarray objects
```

### Reading data
- The registry of open data on AWS - [https://registry.opendata.aws/nex-gddp-cmip6/](https://registry.opendata.aws/nex-gddp-cmip6/).
```python
import s3fs

fs = s3fs.S3FileSystem(anon=True, default_fill_cache=False)

scenario = "ssp245"
var = "tasmax"
years = list(range(2020, 2022))

file_objs = [
    fs.open(
        f"nex-gddp-cmip6/NEX-GDDP-CMIP6/ACCESS-CM2/{scenario}/r1i1p1f1/{var}/{var}_day_ACCESS-CM2_{scenario}_r1i1p1f1_gn_{year}.nc"
    )
    for year in years
]
```

- Read using `open_mfdataset`
```python
da = xr.open_mfdataset(file_objs, engine="h5netcdf")[var].load()
```

- We can **convert** the underlying numpy array to cupy array using `cupy.as_cupy()`.
```python
da = da.as_cupy()
```

- **Check** if data is **`cupy`** Array
```
da.cupy.is_cupy
```