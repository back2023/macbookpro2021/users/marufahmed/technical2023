

https://shapely.readthedocs.io/en/stable/


Manipulation and analysis of geometric objects in the Cartesian plane.

Shapely is a BSD-licensed Python package for manipulation and analysis of planar geometric objects. It is using the widely deployed open-source geometry library [GEOS](https://libgeos.org/) (the engine of [PostGIS](https://postgis.net/), and a port of [JTS](https://locationtech.github.io/jts/)). Shapely wraps GEOS geometries and operations to provide both a feature rich Geometry interface for singular (scalar) geometries and higher-performance NumPy ufuncs for operations using arrays of geometries. Shapely is not primarily focused on data serialization formats or coordinate systems, but can be readily integrated with packages that are.

