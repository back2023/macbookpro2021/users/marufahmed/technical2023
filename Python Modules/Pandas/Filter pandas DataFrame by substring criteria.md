

https://stackoverflow.com/questions/11350770/filter-pandas-dataframe-by-substring-criteria


 

[Vectorized string methods (i.e. `Series.str`)](https://pandas.pydata.org/pandas-docs/stable/user_guide/text.html#string-methods) let you do the following:

```python
df[df['A'].str.contains("hello")]
```

This is available in pandas [0.8.1](https://pandas.pydata.org/pandas-docs/stable/whatsnew/v0.8.1.html) and up.