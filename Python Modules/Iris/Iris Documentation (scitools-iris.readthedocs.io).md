
https://scitools-iris.readthedocs.io/en/v3.0.0/index.html

- visualisation interface based on [matplotlib](https://matplotlib.org/) and [cartopy](https://scitools.org.uk/cartopy/docs/latest/),
- unit conversion,
- subsetting and extraction,
- merge and concatenate,
- aggregations and reductions (including min, max, mean and weighted averages),
- interpolation and regridding (including nearest-neighbor, linear and area-weighted), and
- operator overloads (`+`, `-`, `*`, `/`, etc.).

A number of file formats are recognised by Iris, including CF-compliant NetCDF, GRIB, and PP, and it has a plugin architecture to allow other formats to be added seamlessly.






