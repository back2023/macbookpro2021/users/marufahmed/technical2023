

For the eager folks out there, this intro will give you a quick overview of the following operations:

- Construct a graph
- Set attributes of nodes and edges
- Plot a graph using matplotlib
- Save the plot as an image
- Export and import a graph as a `.gml` file


https://python.igraph.org/en/stable/tutorials/quickstart.html








