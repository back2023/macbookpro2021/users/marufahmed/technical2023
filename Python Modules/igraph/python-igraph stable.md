
Python interface of [igraph](https://igraph.org/), a fast and open source C library to manipulate and analyze graphs (aka networks). It can be used to:

> - Create, manipulate, and analyze networks.
>     
> - Convert graphs from/to [networkx](https://networkx.org/documentation/stable/), [graph-tool](https://graph-tool.skewed.de/) and many file formats.
>     
> - Plot networks using [Cairo](https://www.cairographics.org/), [matplotlib](https://matplotlib.org/), and [plotly](https://plotly.com/python/).



https://python.igraph.org/en/stable/index.html

