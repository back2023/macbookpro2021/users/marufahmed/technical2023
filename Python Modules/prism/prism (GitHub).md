

This package allows users to access and visualize data from the [Oregon State PRISM project](https://prism.nacse.org/). Data are all in the form of gridded rasters for the continental US at 4 different temporal scales: daily, monthly, annual, and 30 year normals. Please see their webpage for a full description of the data products, or [see their overview](https://www.prism.oregonstate.edu/documents/PRISM_datasets_aug2013.pdf).


https://github.com/ropensci/prism