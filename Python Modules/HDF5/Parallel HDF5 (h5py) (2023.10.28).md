https://docs.h5py.org/en/latest/mpi.html

- Read-only parallel access to HDF5 files works with no special preparation: each process should open the file independently and read data normally (avoid opening the file and then forking).

- [Parallel HDF5](https://portal.hdfgroup.org/display/HDF5/Parallel+HDF5) is a feature built on MPI which also supports _writing_ an HDF5 file in parallel. To use this, both HDF5 and h5py must be compiled with MPI support turned on, as described below.

## Building against Parallel HDF5

- HDF5 must be built with at least the following options:
```bash
$./configure --enable-parallel --enable-shared
```
Note that `--enable-shared` is required.

- Often, a “parallel” version of HDF5 will be available through your package manager. 
- You can check to see what build options were used by using the program `h5cc`:
```bash
$ h5cc -showconfig
```

- Once you’ve got a Parallel-enabled build of HDF5, h5py has to be compiled in “MPI mode”. 
- Set your default compiler to the `mpicc` wrapper and build h5py with the `HDF5_MPI` environment variable:
```bash
$ export CC=mpicc
$ export HDF5_MPI="ON"
$ export HDF5_DIR="/path/to/parallel/hdf5"  # If this isn't found by default
$ pip install .
```