
https://spyking-circus.readthedocs.io/en/latest/introduction/hdf5.html

### h5py parallel build
https://github.com/conda-forge/h5py-feedstock/issues/44

### Parallel HDF5
https://docs.h5py.org/en/stable/mpi.html


### Building against Parallel HDF5
https://docs.h5py.org/en/stable/build.html#source-installation

Installation instructions for Parallel HDF5
https://accserv.lepp.cornell.edu/svn/packages/hdf5/release_docs/INSTALL_parallel

The steps of hdf5 installation：
https://hpc.sustech.edu.cn/ref/hdf5.pdf

### Build and Install MPI, parallel HDF5, and h5py from Source on Linux
https://drtiresome.com/2016/08/23/build-and-install-mpi-parallel-hdf5-and-h5py-from-source-on-linux/





### Problems
Problem with python version using parallel h5py in linux
https://github.com/h5py/h5py/issues/694

Install issue while building h5py (pyproject.toml) #2035
https://github.com/h5py/h5py/issues/2035

Need better instructions for compiling with mpi + conda #759
https://github.com/h5py/h5py/issues/759

Can't install h5py
https://stackoverflow.com/questions/64663862/cant-install-h5py