
https://docs.h5py.org/en/latest/build.html#building-against-parallel-hdf5

- It is highly recommended that you use a pre-built version of h5py, either from a Python Distribution, an OS-specific package manager, or a pre-built wheel from PyPI.

- Be aware however that most pre-built versions lack MPI support, and that they are built against a specific version of HDF5. If you require MPI support, or newer HDF5 features, you will need to build from source.

- After installing h5py, you should run the tests to be sure that everything was installed correctly. 
- This can be done in the python interpreter via:

```python
import h5py
h5py.run_tests()
```


## Pre-built installation (recommended)

Pre-build h5py can be installed via many Python Distributions, OS-specific package managers, or via h5py wheels.

```
$ conda install h5py
```

```
$ pip install h5py
```

## Source installation

To install h5py from source, you need:

- A supported Python version with development headers
- HDF5 1.10.4 or newer with development headers
    - HDF5 versions newer than the h5py version you’re using might not work.
    - Odd minor versions of HDF5 (e.g. 1.13) are experimental, and might not work. Use a ‘maintenance’ version like 1.12.x if possible.
    - If you need support for older HDF5 versions, h5py up to version 3.9 supported HDF5 1.8.4 and above.
- A C compiler

- On Unix platforms, you also need `pkg-config` unless you explicitly specify a path for HDF5 as described in [Custom installation](https://docs.h5py.org/en/latest/build.html#custom-install).

- There are notes below on installing HDF5, Python and a C compiler on different platforms.

- Building h5py also requires several Python packages, but in most cases pip will automatically install these in a build environment for you, so you don’t need to deal with them manually. See [Development installation](https://docs.h5py.org/en/latest/build.html#dev-install) for a list.
```bash
$ pip install --no-binary=h5py h5py
```


## Custom installation

- Remember that pip installs wheels by default. To perform a custom installation with pip, you should use:
```
$ pip install --no-binary=h5py h5py
```
or build from a git checkout or downloaded tarball to avoid getting a pre-built version of h5py.

You can specify build options for h5py as environment variables when you build it from source:
```
$ HDF5_DIR=/path/to/hdf5 pip install --no-binary=h5py h5py
$ HDF5_VERSION=X.Y.Z pip install --no-binary=h5py h5py
$ CC="mpicc" HDF5_MPI="ON" HDF5_DIR=/path/to/parallel-hdf5 pip install --no-binary=h5py h5py
```


## Building against Parallel HDF5 

- If you just want to build with `mpicc`, and don’t care about using Parallel HDF5 features in h5py itself:

```bash
$ export CC=mpicc
$ pip install --no-binary=h5py h5py
```

- If you want access to the full Parallel HDF5 feature set in h5py ([Parallel HDF5](https://docs.h5py.org/en/latest/mpi.html#parallel)), you will further have to build in MPI mode. 
- This can be done by setting the `HDF5_MPI` environment variable:
```bash
$ export CC=mpicc
$ export HDF5_MPI="ON"
$ pip install --no-binary=h5py h5py
```
- You will need a shared-library build of Parallel HDF5 as well, i.e. built with `./configure --enable-shared --enable-parallel`.