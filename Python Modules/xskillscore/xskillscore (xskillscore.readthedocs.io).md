

## Deterministic Metrics[](https://xskillscore.readthedocs.io/en/stable/quick-start.html#Deterministic-Metrics "Permalink to this headline")

`xskillscore` offers a suite of correlation-based and distance-based deterministic metrics:

### Correlation-Based[](https://xskillscore.readthedocs.io/en/stable/quick-start.html#Correlation-Based "Permalink to this headline")

- Effective Sample Size (`effective_sample_size`)
    
- Pearson Correlation (`pearson_r`)
    
- Pearson Correlation effective p value (`pearson_r_eff_p_value`)
    
- Pearson Correlation p value (`pearson_r_p_value`)
    
- Slope of Linear Fit (`linslope`)
    
- Spearman Correlation (`spearman_r`)
    
- Spearman Correlation effective p value (`spearman_r_eff_p_value`)
    
- Spearman Correlation p value (`spearman_r_p_value`)
    

### Distance-Based[](https://xskillscore.readthedocs.io/en/stable/quick-start.html#Distance-Based "Permalink to this headline")

- Coefficient of Determination (`r2`)
    
- Mean Absolute Error (`mae`)
    
- Mean Absolute Percentage Error (`mape`)
    
- Mean Error (`me`)
    
- Mean Squared Error (`mse`)
    
- Median Absolute Error (`median_absolute_error`)
    
- Root Mean Squared Error (`rmse`)
    
- Symmetric Mean Absolute Percentage Error (`smape`)