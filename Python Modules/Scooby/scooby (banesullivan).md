
https://github.com/banesullivan/scooby

_Great Dane turned Python environment detective_

This is a lightweight tool for easily reporting your Python environment's package versions and hardware resources.

Install from [PyPI](https://pypi.org/project/scooby/)

