
https://setuptools.pypa.io/en/latest/userguide/package_discovery.html


`Setuptools` provides powerful tools to handle package discovery, including support for namespace packages.

Normally, you would specify the packages to be included manually in the following manner:



