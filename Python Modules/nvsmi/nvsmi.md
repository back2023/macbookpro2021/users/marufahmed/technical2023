

https://pypi.org/project/nvsmi/


## Project description

# nvsmi

A (user-)friendly wrapper to `nvidia-smi`.

It can be used to filter the GPUs based on resource usage (e.g. to choose the least utilized GPU on a multi-GPU system).

## Usage

### CLI

```
nvsmi --help
nvsmi ls --help
nvsmi ps --help
```

### As a library

```
import nvsmi

nvsmi.get_gpus()
nvsmi.get_available_gpus()
nvsmi.get_gpu_processes()
```