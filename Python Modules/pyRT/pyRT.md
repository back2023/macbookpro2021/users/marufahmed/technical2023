

**PyRT** (pronounced _pirate_) is a raytracer/image generator for **Python 3.5** and higher. This project is mainly done with the following in mind:

- Ray Tracing in the Jupyter Notebook
- Teaching ray tracing
- Exploring ray tracing concepts for geo data using Python.
- Rendering geo data, including large point clouds.
- Implementing new algorithms for rendering large 3D city models.
- Creating 3D-Maps from OpenStreetMap data
- Server-side rendering / cloud based rendering
- ...

https://github.com/martinchristen/pyRT