
https://stackoverflow.com/questions/13118029/deleting-folders-in-python-recursively


Try [`shutil.rmtree`](https://docs.python.org/library/shutil.html#shutil.rmtree):

```python
import shutil
shutil.rmtree('/path/to/your/dir/')
```


