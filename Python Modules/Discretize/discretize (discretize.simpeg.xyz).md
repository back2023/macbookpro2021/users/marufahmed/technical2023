

**discretize** - A python package for finite volume discretization.

The vision is to create a package for finite volume simulation with a focus on large scale inverse problems. This package has the following features:

- modular with respect to the spacial discretization
- built with the inverse problem in mind
- supports 1D, 2D and 3D problems
- access to sparse matrix operators
- access to derivatives to mesh variables


Currently, discretize supports:

- Tensor Meshes (1D, 2D and 3D)
- Cylindrically Symmetric Meshes
- QuadTree and OcTree Meshes (2D and 3D)
- Logically Rectangular Meshes (2D and 3D)
- Triangular (2D) and Tetrahedral (3D) Meshes
