
https://colab.research.google.com/github/ecmwf-lab/climetlab-s2s-ai-challenge/blob/main/notebooks/demo_grib.ipynb

```python
import climetlab as cml
import climetlab_s2s_ai_challenge

print(f"Climetlab version : {cml.__version__}")
print(f"Climetlab-s2s-ai-challenge plugin version : {climetlab_s2s_ai_challenge.__version__}")
```


```pyhton
cmlds = cml.load_dataset(
"s2s-ai-challenge-training-input", dev=is_test, origin="ecmwf", date=20200102, parameter="tp", format=FORMAT
)
```

```python
for field in list(cmlds)[0:2]:
print(field)
print(field.valid_datetime(), field.shape)
print(field.to_numpy())
```

```python
cmlds.to_xarray()
```