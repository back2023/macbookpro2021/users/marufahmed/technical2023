
https://climetlab.readthedocs.io/en/latest/examples/06-era5-temperature.html


```python
import numpy as np
import matplotlib.pyplot as plt
```

```python
import climetlab as cml
```

```python
cml.plotting_options(width=400)
```

## Surface temperature in France

```python
ds = cml.load_dataset("era5-temperature", period=(1979, 1982), domain="France", time=12)
```

```python
%%time
len(ds)
```

```python
cml.plot_map(ds[-1])
```