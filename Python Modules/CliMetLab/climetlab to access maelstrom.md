https://git.ecmwf.int/projects/MLFET/repos/maelstrom-downscaling-ap5/browse
```bash
!pip install climetlab climetlab_maelstrom_downscaling 
import climetlab as cml 
ds = cml.load_dataset("maelstrom-downscaling", dataset="training") 
ds.to_xarray()
```