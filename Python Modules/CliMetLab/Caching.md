https://climetlab.readthedocs.io/en/latest/guide/caching.html


## Cache location[](https://climetlab.readthedocs.io/en/latest/guide/caching.html#cache-location "Permalink to this headline")

> The cache location is defined by the `cache‑directory` setting. Its default value depends on your system:
> > - `/tmp/climetlab-$USER` for Linux,
> > - `C:\\Users\\$USER\\AppData\\Local\\Temp\\climetlab-$USER` for Windows
> > - `/tmp/.../climetlab-$USER` for MacOS
