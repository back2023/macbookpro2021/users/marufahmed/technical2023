
https://climetlab.readthedocs.io/en/latest/examples/01-source-file.html

```python
import climetlab as cml
```

## Plot GRIB data

```python
source = cml.load_source("file", "test.grib")
for s in source:
    cml.plot_map(s)
```

## Plot NetCDF data

```python
source = cml.load_source("file", "test.nc")
for s in source:
    cml.plot_map(s)
```