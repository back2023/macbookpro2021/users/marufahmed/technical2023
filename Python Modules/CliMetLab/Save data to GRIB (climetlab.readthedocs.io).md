
https://climetlab.readthedocs.io/en/latest/examples/51-saving-to-grib.html

To generate a GRIB, CliMetLab providesthe function `cml.new_grib_output()`, requiring the following arguments:

- A **numpy array** containing the data to save.
- An object containing a **template** for the GRIB metadata, such as a CliMetLab source
- (Optional) A **dictionary of metadata** to override the template metadata.

## A template for the GRIB metadata

```python
import climetlab as cml

s = cml.load_source(
    "cds",
    "reanalysis-era5-single-levels",
    variable=["2t", "msl"],
    product_type="reanalysis",
    area=[50, -50, 20, 50],
    date="2012-12-12",
    time="12",
)

template = s[0]  # temperature template
```

Similarly, From disk
```python
import climetlab as cml

s = cml.load_source( "file", '/scratch/fp0/mah900/ai-models/test_nci_1.grib')

template = s[0]  #
```

## An `numpy` array with the data:

```python
import numpy as np
import math

def generate_numpy_field(shape, ref=273.15, offset1=0, offset2=0):
    f = np.zeros(shape=shape, dtype=np.float64)
    for i in range(f.shape[0]):
        for j in range(f.shape[1]):
            f[i, j] = (
                ref
                + (math.sin((i + offset1) / 45.0) + math.sin((j + offset2) / 90.0)) * 15
            )
    return f

arr = generate_numpy_field(template.shape)
```
Result:
```python
print(arr.shape)
(721, 1440)
```


## Write to GRIB

### Method 1
```python
output = cml.new_grib_output("test1.grib", template=template)
output.write(arr)
output.close()
```

### Method 2
The template can also be added in the `.write()` method instead of the `cml.new_grib_output()` function:
```python
output = cml.new_grib_output("test2.grib")
output.write(arr, template=template)
output.close()
```

### Method 3

```python
with cml.new_grib_output("test3.grib", template=template) as output:
    output.write(arr)
```


## Write Several fields
Once a GRIB output has been open, several fields can be written to it, as follows:

```python
with cml.new_grib_output("test4.grib", template=template) as output:
    for i in range(24):
        f = generate_numpy_field(template.shape, offset1=i, offset2=i)
        output.write(f, metadata={"time": i})
```

