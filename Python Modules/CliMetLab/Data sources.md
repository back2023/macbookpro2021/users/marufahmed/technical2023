
https://climetlab.readthedocs.io/en/latest/guide/sources.html

- A data **Source** is an object created using `cml.load_source(name, *args, **kwargs)` with the appropriate name and arguments, which provides access to the data.
	- The source **name** is a string that uniquely identifies the source type.
	- The **arguments** (args and kwargs) are used to specify the data location to access the data. They can include additional parameters to access the data.
	- The **additional functionalities** include python code related to caching, plotting and interacting with other data.


## cds
- The `"cds"` data source accesses the [Copernicus Climate Data Store](https://cds.climate.copernicus.eu/) (CDS), using the [cdsapi](https://pypi.org/project/cdsapi/) package. A typical _cdsapi_ request has the following format:
```python
import cdsapi
client = cdsapi.Client()
client.retrieve("dataset-name",
                {"parameter1": "value1",
                 "parameter2": "value2",
                 ...})
```
to perform the same operation with _CliMetLab_, use the following code:
```python
import climetlab as cml
data = cml.load_source("cds",
                       "dataset-name",
                       {"parameter1": "value1",
                        "parameter2": "value2",
                        ...})
```

- Data downloaded from the CDS is stored in the the [cache](https://climetlab.readthedocs.io/en/latest/guide/caching.html#caching).
- To access data from the CDS, you will need to register and retrieve an access token. The process is described [here](https://cds.climate.copernicus.eu/api-how-to).

## mars

- The `"mars"` source allows handling data from the Meteorological Archival and Retrieval System (MARS).
- To access data from the MARS, you will need to register and retrieve an access token.




## multi (advanced usage)

```python
>>> ds = load_source( "multi", source1, source2, ...)
```