
https://climetlab.readthedocs.io/en/latest/contributing/grib.html

CliMetLab provides built-in functionalities regarging GRIB format handling.

## Reading GRIB files

- CliMetLab can read _gridded_ GRIB files (or urls) and provide data as `xarray.Dataset` objects. This can be performed using `cml.load_source("file", "myfile.grib")`.
 - In addition to reading GRIB from local or remote sources, CliMetLab can also use a precomputed index, avoiding the need to parse the GRIB file to know its content. Using this index allows partial read of local files, and merging of mutliple GRIB sources.
- This can be performed using `cml.load_source("indexed-directory", "my/dir")`. To allow fast access to the data in the directory, CliMetLab relies on an index. Note that the index must have been created on this directory, CliMetLab will create one (see GRIB indexing below).

## Writing GRIB files

There are two ways to write GRIB files:

- To save data from MARS, CDS or other, when GRIB is already the native format of the data,
    use the `source.save(filename)` method. This method is implemented only on a sources relying on GRIB.

- CliMetLab also supports writing custom GRIB files, with **modified values or custom attributes**
    through the function `` `cml.new_grib_output() ``. See usage example in the example notebook ([Examples](https://climetlab.readthedocs.io/en/latest/examples.html#examples)).

## Building indexes

- CliMetLab can create GRIB index files through its command line interface.

## How to export files from the CliMetLab cache to another directory ?

- When using CliMetLab to access MARS, CDS or other source, data is cached into the CliMetLab cache directory (the cache folder is `climetlab settings cache-directory`).

