
https://climetlab.readthedocs.io/en/latest/guide/data_handling.html
## Methods provided by CliMetLab data objects[](https://climetlab.readthedocs.io/en/0.13.10/guide/data_handling.html#methods-provided-by-climetlab-data-objects "Permalink to this headline")

Methods provided by CliMetLab data objects (such as a Dataset, a data Source or a Reader): Depending on the data, some of these methods are or are not available.

A CliMetLab data object provides methods to access and use its data.
```python
>>> source.to_xarray() # for gridded data
>>> source.to_pandas() # for non-gridded data
>>> source.to_numpy() # When the data is a n-dimensional array.
>>> source.to_tfrecord() # Experimental
```


## Iterating

When a CliMetLab data source or dataset provides a list of fields, it can be iterated over to access each field (in a given order see [below](https://climetlab.readthedocs.io/en/latest/guide/data_handling.html#order-by)).

```python
>>> import climetlab as cml
>>> ds = cml.load_source(
         "cds",
         "reanalysis-era5-single-levels",
         param=["2t", "msl"],
         product_type="reanalysis",
         grid='5/5',
         date=["2012-12-12", "2012-12-13"],
         time=[600, 1200, 1800],
    )

>>> len(ds)
10

>>> for f in ds: print(f)
```

Similarly
```python
import climetlab as cml
ds = cml.load_source( "file", '/scratch/fp0/mah900/ai-models/test_nci_1.grib')
for f in ds: print(f)
```


## Selection with `[...]`[](https://climetlab.readthedocs.io/en/latest/guide/data_handling.html#selection-with "Permalink to this headline")

When a CliMetLab data source or dataset provides a list of fields, it can be [iterated](https://climetlab.readthedocs.io/en/latest/guide/data_handling.html#iter) over to access each field (in a given order see [below](https://climetlab.readthedocs.io/en/latest/guide/data_handling.html#order-by)).
```python
>>> len(ds)

>>> print(f[0])

>>> for f in ds[0:3]: print(f)

>>> for f in ds[0:5:2]: print(f)

```


## Selection with `.sel()`

- When a CliMetLab data source or dataset provides a list of fields, it can be [iterated](https://climetlab.readthedocs.io/en/latest/guide/data_handling.html#iter) over to access each field (in a given order see [below](https://climetlab.readthedocs.io/en/latest/guide/data_handling.html#order-by)).

- The method `.sel()` allows filtering this list to **select a subset** of the list of fields.

- For instance, the following examples shows how to select various subsets of fields from a list of fields.
	- After selection the required list of fields, the selected data from this subset is available with the methods `.to_numpy()`, `.to_pytorch()`, `.to_xarray()`, etc…

- This list of fields can be filtered to extract on the fields corresponding to the 2m-temperature parameter with `.sel(param="2t")`:
```python
>>> subset = ds.sel(param="2t")

>>> len(subset)

>>> for f in subset: print (f)

```

- This list of fields can be filtered to extract on the fields corresponding to 12h time with `.sel(time=1200)`:
```python
>>> subset = ds.sel(time=1200)
 
>>> len(subset)
 
>>> for f in subset:print (f)
```

- Or both filters can be applied simultaneously with `.sel(param="2t", time=1200)`.
```python
>>> subset = ds.sel(param="2t", time=1200)
```

- Filtering on multiple values is also possible by providing a list of values `.sel(param="2t", time=[600, 1200])`.
```python
>>> subset = ds.sel(param="2t", time=[600, 1200])
```


## Ordering with `.order_by()`

