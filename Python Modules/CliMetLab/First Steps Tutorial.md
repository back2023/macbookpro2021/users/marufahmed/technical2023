
https://climetlab.readthedocs.io/en/latest/firststeps.html


## Getting data[](https://climetlab.readthedocs.io/en/latest/firststeps.html#getting-data "Permalink to this headline")

_CliMetLab_ provides two main ways to access climate and meteorological data
1. Data Sources ([details](https://climetlab.readthedocs.io/en/latest/guide/sources.html))
2. Datasets ([details](https://climetlab.readthedocs.io/en/latest/guide/datasets.html))

### Data Sources
- In _CliMetLab_, a _Data Source_ refers to a local or remote storage server or data archive from where we can download or access files related to climate data.


```python
import climetlab as cml
!wget https://raw.githubusercontent.com/ecmwf/climetlab/main/docs/examples/test.grib
```

- GRIB is a file format for storage and sharing of gridded meteorological data. You can think of gridded data as weather or some other data that is associated with specific locations using coordinates. For example, wind speed data for every longitude and latitude on a two dimensional grid.

- The GRIB format (version 1 and 2) is [endorsed by WMO](https://en.wikipedia.org/wiki/GRIB). GRIB (GRIdded Binary) is a binary file format so you cannot look at it using a text editor. But you sure can use _CliMetLab_ to explore it:
```python
grib_data = cml.load_source("file", "test.grib")

cml.plot_map(grib_data, title=True, path="test-grib-plot.svg")
```


- You can also use `load_source` to directly download and load files from remote server, in one step. For example, let’s download and plot a NetCDF file:
```python
netcdf_url = "https://raw.githubusercontent.com/ecmwf/climetlab/main/docs/examples/test.nc"
netcdf_data = cml.load_source("url", netcdf_url)
cml.plot_map(netcdf_data)
```


- If your data is gridded data, like in our examples above, you can simply call `to_xarray()` method to convert it to an `Xarray` object:
```python
grib_xr = grib_data.to_xarray()
# as well as:
netcdf_xr = netcdf_data.to_xarray()
```


- Observations are usually data points corresponding to time for a certain location unlike gridded data that contains data points that is usually for a range of locations. For example, monthly average temperature of Lahore for the past ten years.
- The following example downloads a `.csv` file from NOAA’s _International Best Track Archive for Climate Stewardship_ ([IBTrACS](https://www.ncdc.noaa.gov/ibtracs/)) using the `url` data source. The file is downloaded into the local cache. We then convert it to a Pandas frame. The rows corresponding to the severe tropical cyclone [Uma](https://en.wikipedia.org/wiki/1986%E2%80%9387_South_Pacific_cyclone_season#Severe_Tropical_Cyclone_Uma) are extracted and plotted.
```python
import climetlab as cml

data = cml.load_source(
    "url",
    (
        "https://www.ncei.noaa.gov/data/international-best-track-archive-for-climate-stewardship-ibtracs"
        "/v04r00/access/csv/ibtracs.SP.list.v04r00.csv"
    ),
)

pd = data.to_pandas()
uma = pd[pd.NAME == "UMA:VELI"]
cml.plot_map(uma, style="cyclone-track")
```

- The Data Sources implement various methods to access and decode data. When data are downloaded from a remote site, they are [cached](https://climetlab.readthedocs.io/en/latest/guide/caching.html#caching) on the local computer.

The first argument to `load_source` can take the following values:

|Argument value|Description|
|---|---|
|`"file"`|A path to a local file name. [Read more](https://climetlab.readthedocs.io/en/latest/guide/sources.html#data-sources-file)|
|`"url"`|A URL to a remote file. [Read more](https://climetlab.readthedocs.io/en/latest/guide/sources.html#data-sources-url)|
|`"cds"`|A request to retrieve data from the [Copernicus Climate Data Store](https://cds.climate.copernicus.eu/) (CDS). Requires an account. [Read more](https://climetlab.readthedocs.io/en/latest/guide/sources.html#data-sources-cds)|
|`"mars"`|A request to retrieve data from ECMWF’s meteorological archive (MARS), using the [ECMWF web API](https://www.ecmwf.int/en/forecasts/access-forecasts/ecmwf-web-api). Requires an account. [Read more](https://climetlab.readthedocs.io/en/latest/guide/sources.html#data-sources-mars)|


### Datasets[](https://climetlab.readthedocs.io/en/latest/firststeps.html#datasets "Permalink to this headline")

- Datasets are a higher-level concept compared to data sources.
```python
import climetlab as cml

data = cml.load_dataset("hurricane-database", bassin="atlantic")
print(data.home_page)

#print
https://www.aoml.noaa.gov/hrd/hurdat/Data_Storm.html

irma = data.to_pandas(name="irma", year=2017)
cml.plot_map(irma)
```



