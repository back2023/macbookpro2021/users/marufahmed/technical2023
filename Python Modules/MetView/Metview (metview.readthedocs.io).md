
https://metview.readthedocs.io/en/latest/index.html#

**Metview** is a meteorological workstation application designed to be a complete working environment for both the operational and research meteorologist. Its capabilities include powerful data access, processing and visualisation. It features both a powerful **icon-based user interface** for interactive work and a **Python** interface for batch processing.

Metview was developed as part of a cooperation between ECMWF and INPE (Brazilian National Institute for Space Research).


