
https://metview.readthedocs.io/en/latest/examples/analysing_data.html

n this notebook we will demonstrate how to:
- find locations of extreme values from GRIB data
- compute and plot a time series extracted from a point
- mask values that are not of interest
- compute wind speed
- compute and plot a vertical cross section
- compute and plot a vertical profile



