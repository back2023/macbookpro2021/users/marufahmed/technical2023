
https://docs.python.org/3/library/site.html

- **This module is automatically imported during initialization.** The automatic import can be suppressed using the interpreter’s [`-S`](https://docs.python.org/3/using/cmdline.html#cmdoption-S) option.
- Importing this module will append site-specific paths to the module search path and add a few builtins, unless [`-S`](https://docs.python.org/3/using/cmdline.html#cmdoption-S) was used.
- To explicitly trigger the usual site-specific additions, call the [`site.main()`](https://docs.python.org/3/library/site.html#site.main "site.main") function.

- It starts by constructing up to four directories from a head and a tail part. 
	- For the head part, it uses `sys.prefix` and `sys.exec_prefix`; empty heads are skipped. 
	- For the tail part, it uses the empty string and then `lib/site-packages` (on Windows) or `lib/python_X.Y_/site-packages` (on Unix and macOS). 
	- For each of the distinct head-tail combinations, it sees if it refers to an existing directory, and if so, adds it to `sys.path` and also inspects the newly added path for configuration files.





