

https://github.com/rasterio/rasterio


# Rasterio

Rasterio reads and writes geospatial raster data.

[![https://app.travis-ci.com/rasterio/rasterio.svg?branch=master](https://camo.githubusercontent.com/3f07d551c2a210739f42fc7b196a14fd772c575f0e3cc7cecdab862acf438f5c/68747470733a2f2f6170702e7472617669732d63692e636f6d2f726173746572696f2f726173746572696f2e7376673f6272616e63683d6d6173746572)](https://app.travis-ci.com/rasterio/rasterio) [![https://coveralls.io/repos/github/mapbox/rasterio/badge.svg?branch=master](https://camo.githubusercontent.com/47614b56dd6325066780ee78e3fb2437cbf57e44c900d19b00dcdd02058b49a7/68747470733a2f2f636f766572616c6c732e696f2f7265706f732f6769746875622f6d6170626f782f726173746572696f2f62616467652e7376673f6272616e63683d6d6173746572)](https://coveralls.io/github/mapbox/rasterio?branch=master) [![https://img.shields.io/pypi/v/rasterio](https://camo.githubusercontent.com/00ba831fda6701dfeaabde08b5061ec7698eead8c85805e0d173348d4845af5d/68747470733a2f2f696d672e736869656c64732e696f2f707970692f762f726173746572696f)](https://pypi.org/project/rasterio/)

Geographic information systems use GeoTIFF and other formats to organize and store gridded, or raster, datasets. Rasterio reads and writes these formats and provides a Python API based on N-D arrays.

Rasterio 1.4 works with Python 3.9+, Numpy 1.21+, and GDAL 3.3+. Official binary packages for Linux, macOS, and Windows with most built-in format drivers plus HDF5, netCDF, and OpenJPEG2000 are available on PyPI.

Read the documentation for more details: [https://rasterio.readthedocs.io/](https://rasterio.readthedocs.io/).