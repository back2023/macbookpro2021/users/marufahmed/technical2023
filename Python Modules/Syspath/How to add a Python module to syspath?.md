
https://askubuntu.com/questions/470982/how-to-add-a-python-module-to-syspath


To find out what is included in $PYTHONPATH, run the following code in python (3):

```python
import sys
print(sys.path)
```

**How to add a directory**

_Occasionally_

From within a python file, you can add path(s) occasionally to the default path by adding the following lines in the head section of your python application or script:

```python
import sys
sys.path.insert(0, "/path/to/your/package_or_module")
```

_For example:_

if I have a folder: `/home/myname/pythonfiles`, and I want to import the file `module_1.py`, located in that directory, I add this to the head section of my code:

```python
import sys
sys.path.insert(0, "/home/myname/pythonfiles")
```

And I can simply import the file `module_1.py` by:

```python
import module_1
```

When I create a _package_ and want to import module(s) from the package, I need to create a folder in `$PYTHONPATH`, containing the modules, accompanied by a (can be empty) file called `__init__.py`

_For example:_

To import from a package (folder) called `my_package` in `/home/myname/pythonfiles` , add the `/home/myname/pythonfiles` path to your `$PYTHONPATH`, like in example 1, and import the module called `module_2.py` (inside the package folder) simply with: `

```python
from <packagename> import module_2
```

_Adding directories to `$PYTHONPATH` permanently:_

Add the following line to your `~/.profile` file.

```python
export PYTHONPATH=$PYTHONPATH:/path/you/want/to/add
```