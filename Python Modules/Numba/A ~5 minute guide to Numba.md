
Numba is a just-in-time compiler for Python that works best on code that uses NumPy arrays and functions, and loops. The most common way to use Numba is through its collection of decorators that can be applied to your functions to instruct Numba to compile them. When a call is made to a Numba-decorated function it is compiled to machine code “just-in-time” for execution and all or part of your code can subsequently run at native machine code speed!

Out of the box Numba works with the following:

- OS: Windows (32 and 64 bit), OSX, Linux (64 bit). Unofficial support on *BSD.
    
- Architecture: x86, x86_64, ppc64le, armv8l (aarch64), M1/Arm64.
    
- GPUs: Nvidia CUDA.
    
- CPython
    
- NumPy 1.21 - 1.24

https://numba.readthedocs.io/en/stable/user/5minguide.html