
https://adityamangal98.medium.com/how-to-share-data-like-lists-and-dictionaries-between-processes-in-parallel-processing-in-python-7f73dab0916


**Introduction:** In Python, the multiprocessing module allows us to create and manage multiple processes to achieve parallel execution. When working with multiple processes, you may encounter scenarios where you need to share data between these processes efficiently. In this blog post, we will explore different approaches to sharing data, such as lists and dictionaries, between processes in Python.

Before we dive into the code, let’s quickly understand the two main methods of **inter-process communication (IPC)** supported by the multiprocessing module: shared memory and message passing.

1. Shared Memory: Shared memory allows processes to access the same region of memory, enabling them to share data without the need for serialization and deserialization. Python’s multiprocessing module provides a useful class called `Value` for sharing a single value, and `Array` for sharing sequences like lists and arrays.
2. Message Passing: Message passing involves passing data between processes using message queues. Python’s multiprocessing module provides a class called `Queue` that allows processes to enqueue and dequeue messages.

