
https://stackoverflow.com/questions/8640367/manager-dict-in-multiprocessing


## EDIT for _Python 3.6_ or later:

https://stackoverflow.com/a/46228938

[Since _Python 3.6_](https://docs.python.org/3/whatsnew/3.6.html#multiprocessing), per [this changeset](https://hg.python.org/cpython/rev/39e7307f9aee) following [this issue](https://bugs.python.org/issue6766), it's also possible to [use nested Proxy Objects](https://docs.python.org/3/library/multiprocessing.html#multiprocessing.managers.SyncManager.list) which automatically propagate any changes performed on them to the containing Proxy Object. Thus, replacing the line `d[1] = []` with `d[1] = manager.list()` would correct the issue as well:

 
```python
from multiprocessing import Process, Manager

manager = Manager()
d = manager.dict()

def f():
    d[1].append(4)
    # the __str__() method of a dict object invokes __repr__() on each of its items,
    # so explicitly invoking __str__() is required in order to print the actual list items
    print({k: str(v) for k, v in d.items()})

if __name__ == '__main__':
    d[1] = manager.list()
    p = Process(target=f)
    p.start()
    p.join()
```
 