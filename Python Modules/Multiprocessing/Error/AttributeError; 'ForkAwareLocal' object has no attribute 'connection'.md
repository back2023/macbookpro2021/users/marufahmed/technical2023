
### Shutting down manager error "AttributeError: 'ForkAwareLocal' object has no attribute 'connection'" when using namespace and shared memory dict 
https://stackoverflow.com/questions/60049527/shutting-down-manager-error-attributeerror-forkawarelocal-object-has-no-attr

### Python Multiprocessing Manager Error-‘ForkAwareLocal’ object has no attribute

https://jaime-lin.medium.com/python-multiprocessing-manager-error-forkawarelocal-object-has-no-attribute-496009c94aed

The root cause of this problem is the main thread is done so crash the connection between two processes. However, the subprocess is still running and it needs to throw stuff out via the queue. So keep the main thread alive is our solution.



