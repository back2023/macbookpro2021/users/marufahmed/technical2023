
https://discuss.pytorch.org/t/keras-and-pytorch-cnn2d-gives-different-output-shape/180631



Keras seems to use the channels-last memory layout while PyTorch defaults to channels-first.  
If you need to create matching shapes manually you could `permute` the PyTorch tensor.