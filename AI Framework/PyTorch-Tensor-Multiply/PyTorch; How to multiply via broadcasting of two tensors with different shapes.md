
https://stackoverflow.com/questions/65121614/pytorch-how-to-multiply-via-broadcasting-of-two-tensors-with-different-shapes

"When applying broadcasting in pytorch (as well as in numpy) you need to start at the _last_ dimension (check out [https://pytorch.org/docs/stable/notes/broadcasting.html](https://pytorch.org/docs/stable/notes/broadcasting.html)). If they do not match you need to reshape your tensor. In your case they can't directly be broadcasted:"

```python
      [3]  # the two values in the last dimensions are not one and do not match
[3, 5, 5]
```

"Instead you can redefine `A = A[:, None, None]` before muliplying such that you get shapes"

```python
[3, 1, 1]
[3, 5, 5]
```

"which satisfies the conditions for broadcasting."

