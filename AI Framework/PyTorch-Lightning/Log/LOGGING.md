

https://pytorch-lightning.readthedocs.io/en/0.10.0/logging.html


Lightning supports the most popular logging frameworks (TensorBoard, Comet, etc…). To use a logger, simply pass it into the `Trainer`. Lightning uses TensorBoard by default.

