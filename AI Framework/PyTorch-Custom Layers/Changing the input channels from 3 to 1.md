

https://discuss.pytorch.org/t/changing-the-input-channels-from-3-to-1/125764


[Kapil_Rana](https://discuss.pytorch.org/u/Kapil_Rana)

[Jul 2021](https://discuss.pytorch.org/t/changing-the-input-channels-from-3-to-1/125764/2 "Post date")

You just need to change the first layer.  
There are plenty of solutions already discussed here.

![](https://discuss.pytorch.org/user_avatar/discuss.pytorch.org/alex_ge/48/17912_2.png) [Modify ResNet or VGG for single channel grayscale](https://discuss.pytorch.org/t/modify-resnet-or-vgg-for-single-channel-grayscale/22762)

> Hi, I’m working on infrared data which I convert to grayscale 64x64 (although I can use other sizes, but usually my GPU runs out of memory). I used VGG11 but I manually recreated the architecture in order to use it, which is a very cumbersome and error prone task. I was wondering if there is an easier way to modify VGG19 or ResNet architectures in a fast and simpler way to use my 64x64 single channel input, and if yes, would that make sense since those models are fine-tuned for 3 channel RGB? …

![](https://discuss.pytorch.org/user_avatar/discuss.pytorch.org/nishanth_sasankan/48/13219_2.png) [Grayscale images for resenet and deeplabv3](https://discuss.pytorch.org/t/grayscale-images-for-resenet-and-deeplabv3/48693) [vision](https://discuss.pytorch.org/c/vision/5)

> How can I modify a resnet or VGG network to use grayscale images. I am loading the network the following way m=torchvision.models.segmentation.fcn_resnet50(pretrained=False, progress=True, num_classes=2, aux_loss=None) Is there some way I can tweak this model after loading it? Thanks


