
https://github.com/CompVis/stable-diffusion/issues/86


You ran out of GPU memory. Try using [nvitop](https://github.com/XuehaiPan/nvitop) to monitor your gpu memory usage.  
You can try this branch: [https://github.com/basujindal/stable-diffusion](https://github.com/basujindal/stable-diffusion)  
It trades speed for a lower memory usage.

