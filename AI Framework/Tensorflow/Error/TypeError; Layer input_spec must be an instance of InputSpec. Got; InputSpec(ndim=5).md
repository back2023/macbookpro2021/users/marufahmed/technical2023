
"For that to work you just need to downgrade tensorflow to 2.0.0"
Source: 
TypeError: Layer input_spec must be an instance of InputSpec. Got: InputSpec(shape=(None, 128, 768), ndim=3)
https://stackoverflow.com/questions/68835665/typeerror-layer-input-spec-must-be-an-instance-of-inputspec-got-inputspecsha

***

"Anything under `tf.python.*` is private, intended for development only, rather than for public use.  <br>Importing from tensorflow.python or any other modules (including import tensorflow_core...) is not supported, and can break unannounced.So, it is suggested not to use anything with tf.python.*.<br><br>You can use `tf.keras.layers.InputSpec` instead of `tensorflow.python.keras.engine.input_spec`. Please find the gist of working code [here](https://colab.sandbox.google.com/gist/synandi/b7fa1ca299d9901f650e8a8bdf55c992/59863.ipynb). Thank you!"
Source: 
Layer input_spec must be an instance of InputSpec! #59863
https://github.com/tensorflow/tensorflow/issues/59863



