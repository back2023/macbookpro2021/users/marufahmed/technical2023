To use the TensorFlow-specific APIs for SageMaker distributed model parallism, you need to add the following import statement at the top of your training script.

```python
import smdistributed.modelparallel.tensorflow as smp
```

https://sagemaker.readthedocs.io/en/v2.152.0/api/training/smp_versions/v1.10.0/smd_model_parallel_tensorflow.html