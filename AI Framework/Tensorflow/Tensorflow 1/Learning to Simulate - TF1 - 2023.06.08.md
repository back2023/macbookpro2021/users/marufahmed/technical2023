
```bash
 cd /scratch/vp91/
 mkdir mah900
 cd mah900/
 git clone https://github.com/deepmind/deepmind-research.git

 #Download data
 mkdir datasets
 ./deepmind-research/learning_to_simulate/download_dataset.sh WaterRamps ./datasets
 mkdir models
```

Run, V100, 1 GPU
```bash
qsub -I g1_h12.pbs
...
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/sTF1.15

cd /scratch/vp91/mah900/deepmind-research/

python -m learning_to_simulate.train \
--data_path=/scratch/vp91/mah900/datasets/WaterRamps \
--model_path=/scratch/vp91/mah900/models/WaterRamps
# Num of steps: 'num_steps', int(2e7)
# Will take 333.33 hours (with 6sec per 100 steps)
 
time python -m learning_to_simulate.train \
--data_path=/scratch/vp91/mah900/datasets/WaterRamps \
--model_path=/scratch/vp91/mah900/models/WaterRamps_v100 \
--num_steps=20000
...
I0608 12:03:06.665736 22440140973568 basic_session_run_hooks.py:260] loss = 0.040020753, step = 19800 (7.868 sec)
INFO:tensorflow:global_step/sec: 12.6154
I0608 12:03:14.591527 22440140973568 basic_session_run_hooks.py:692] global_step/sec: 12.6154
INFO:tensorflow:loss = 0.13204671, step = 19900 (7.927 sec)
I0608 12:03:14.592792 22440140973568 basic_session_run_hooks.py:260] loss = 0.13204671, step = 19900 (7.927 sec)
INFO:tensorflow:Saving checkpoints for 20000 into /scratch/vp91/mah900/models/WaterRamps_v100/model.ckpt.
I0608 12:03:22.837437 22440140973568 basic_session_run_hooks.py:606] Saving checkpoints for 20000 into /scratch/vp91/mah900/models/WaterRamps_v100/model.ckpt.
INFO:tensorflow:Loss for final step: 0.31712475.
I0608 12:03:23.712878 22440140973568 estimator.py:371] Loss for final step: 0.31712475.

real	26m41.109s
user	35m20.247s
sys	2m2.205s


```

A100, 1 GPU

```bash
qsub -I a1_h4.pbs
 ...
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/sTF1.15

cd /scratch/vp91/mah900/deepmind-research/

time python -m learning_to_simulate.train \
--data_path=/scratch/vp91/mah900/datasets/WaterRamps \
--model_path=/scratch/vp91/mah900/models/WaterRamps_A100 \
--num_steps=20000
...
I0608 12:25:57.010136 22932748292416 basic_session_run_hooks.py:260] loss = 0.09780865, step = 19800 (3.795 sec)
INFO:tensorflow:global_step/sec: 26.1494
I0608 12:26:00.833797 22932748292416 basic_session_run_hooks.py:692] global_step/sec: 26.1494
INFO:tensorflow:loss = 0.19019367, step = 19900 (3.824 sec)
I0608 12:26:00.834294 22932748292416 basic_session_run_hooks.py:260] loss = 0.19019367, step = 19900 (3.824 sec)
INFO:tensorflow:Saving checkpoints for 20000 into /scratch/vp91/mah900/models/WaterRamps_A100/model.ckpt.
I0608 12:26:04.750577 22932748292416 basic_session_run_hooks.py:606] Saving checkpoints for 20000 into /scratch/vp91/mah900/models/WaterRamps_A100/model.ckpt.
INFO:tensorflow:Loss for final step: 0.3294315.
I0608 12:26:05.897952 22932748292416 estimator.py:371] Loss for final step: 0.3294315.

real	15m29.973s
user	16m19.713s
sys	1m6.755s

```

Tensor-board
```bash
qsub -v LOG_DIR="/scratch/vp91/mah900/models/WaterRamps_A100/" /scratch/fp0/mah900/ClimaX/tensorboard.pbs
```



```bash
time python -m learning_to_simulate.train \
--data_path=/scratch/vp91/mah900/datasets/WaterRamps \
--model_path=/scratch/vp91/mah900/models/WaterRamps_A100 \
--num_steps=500000
...
I0608 18:25:28.735253 22705988694336 basic_session_run_hooks.py:692] global_step/sec: 26.9724
INFO:tensorflow:loss = 0.03375674, step = 444003 (3.707 sec)
I0608 18:25:28.735714 22705988694336 basic_session_run_hooks.py:260] loss = 0.03375674, step = 444003 (3.707 sec)
INFO:tensorflow:global_step/sec: 27.2651
I0608 18:25:32.402947 22705988694336 basic_session_run_hooks.py:692] global_step/sec: 27.2651
INFO:tensorflow:loss = 0.050545134, step = 444103 (3.668 sec)
I0608 18:25:32.403401 22705988694336 basic_session_run_hooks.py:260] loss = 0.050545134, step = 444103 (3.668 sec)
client_loop: send disconnect: Broken pipe
...
I0608 20:40:21.433348 22724693340480 basic_session_run_hooks.py:260] loss = 0.030090868, step = 499907 (3.853 sec)
INFO:tensorflow:Saving checkpoints for 500000 into /scratch/vp91/mah900/models/WaterRamps_A100/model.ckpt.
I0608 20:40:25.046251 22724693340480 basic_session_run_hooks.py:606] Saving checkpoints for 500000 into /scratch/vp91/mah900/models/WaterRamps_A100/model.ckpt.
INFO:tensorflow:Loss for final step: 0.035569355.
I0608 20:40:26.080650 22724693340480 estimator.py:371] Loss for final step: 0.035569355.

real	38m33.645s
user	47m7.704s
sys	3m21.129s



```
Error 
```
tensorflow.python.framework.errors_impl.NotFoundError: libmpi.so.40: cannot open shared object file: No such file or directory
```
Because of using horovod code, while running the previous data.
Working after removing all Horovod code. 



### Generate some trajectory rollouts on the test set:
```bash
mkdir /scratch/vp91/mah900/rollouts/
cd /scratch/vp91/mah900/deepmind-research/
python -m learning_to_simulate.train \
    --mode="eval_rollout" \
    --data_path=/scratch/vp91/mah900/datasets/WaterRamps \
    --model_path=/scratch/vp91/mah900/models/WaterRamps_A100/ \
    --output_path=/scratch/vp91/mah900/rollouts/WaterRamps_A100

```
 

#### Install Jupyter Lab
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/sTF1.15
conda install -c conda-forge jupyterlab
```
### ARE
```
/scratch/fp0/mah900/Miniconda2023 
/scratch/fp0/mah900/env/sTF1.15
```


### Compare V100 & A100
```bash
\
time python -m learning_to_simulate.train \
--data_path=/scratch/vp91/mah900/datasets/WaterRamps \
--model_path=/scratch/vp91/mah900/models/WaterRamps_V100/tmp \
--num_steps=41000


\
time python -m learning_to_simulate.train \
--data_path=/scratch/vp91/mah900/datasets/WaterRamps \
--model_path=/scratch/vp91/mah900/models/WaterRamps_A100/tmp \
--num_steps=41000

\
time python -m learning_to_simulate.train \
--data_path=/scratch/vp91/mah900/datasets/WaterRamps \
--model_path=/scratch/vp91/mah900/models/WaterRamps_C1/tmp \
--num_steps=3000
...
I0609 12:09:03.754358 23305631421952 basic_session_run_hooks.py:260] loss = 0.47773018, step = 2601 (62.796 sec)
INFO:tensorflow:Saving checkpoints for 2612 into /scratch/vp91/mah900/models/WaterRamps_C1/tmp/model.ckpt.
I0609 12:09:11.404681 23305631421952 basic_session_run_hooks.py:606] Saving checkpoints for 2612 into /scratch/vp91/mah900/models/WaterRamps_C1/tmp/model.ckpt.
INFO:tensorflow:global_step/sec: 1.43193
I0609 12:10:13.589183 23305631421952 basic_session_run_hooks.py:692] global_step/sec: 1.43193
INFO:tensorflow:loss = 0.20210183, step = 2701 (69.836 sec)
I0609 12:10:13.589936 23305631421952 basic_session_run_hooks.py:260] loss = 0.20210183, step = 2701 (69.836 sec)
INFO:tensorflow:global_step/sec: 1.2553
I0609 12:11:33.251404 23305631421952 basic_session_run_hooks.py:692] global_step/sec: 1.2553
INFO:tensorflow:loss = 0.1372814, step = 2801 (79.663 sec)
I0609 12:11:33.252544 23305631421952 basic_session_run_hooks.py:260] loss = 0.1372814, step = 2801 (79.663 sec)
INFO:tensorflow:global_step/sec: 1.44075
I0609 12:12:42.659791 23305631421952 basic_session_run_hooks.py:692] global_step/sec: 1.44075
INFO:tensorflow:loss = 0.1371894, step = 2901 (69.408 sec)
I0609 12:12:42.660655 23305631421952 basic_session_run_hooks.py:260] loss = 0.1371894, step = 2901 (69.408 sec)
INFO:tensorflow:Saving checkpoints for 3000 into /scratch/vp91/mah900/models/WaterRamps_C1/tmp/model.ckpt.
I0609 12:13:55.843358 23305631421952 basic_session_run_hooks.py:606] Saving checkpoints for 3000 into /scratch/vp91/mah900/models/WaterRamps_C1/tmp/model.ckpt.
INFO:tensorflow:Loss for final step: 0.26537347.
I0609 12:13:59.252132 23305631421952 estimator.py:371] Loss for final step: 0.26537347.

real	36m30.759s
user	242m2.393s
sys	18m11.460s

```

 

### Horovod Test
```

```

