### 2023.06.07

###### does tensorflow 1.15 work with cuda 11 and cudnn 8?
Source: https://www.reddit.com/r/tensorflow/comments/uj1t1s/does_tensorflow_115_work_with_cuda_11_and_cudnn_8/
1. Tf1.15 is not compatible with cuda 11/cudnn 8.... You have to use cuda 10.0 or cuda 9.2
2. But NVIDIA maintains a repo, forked from official tf1.15.5, to support new CUDA.


###### TRAINING WITH TF 1.15 ON RTX 3090
Source: https://www.appsloveworld.com/tensorflow/90/training-with-tf-1-15-on-rtx-3090

```bash
# create new conda env
conda create --name nv-tf15

# install nvidia-version of TF 1.15
pip install nvidia-pyindex
pip install nvidia-tensorflow

# install pytorch (likely optional)
pip install torch==1.10.2+cu113 torchvision==0.11.3+cu113 torchaudio==0.10.2+cu113 -f https://download.pytorch.org/whl/cu113/torch_stable.html

# tensorboard wasn't importing properly, so I did this
conda install tensorboard
```

If you can't upgrade your code base to 2.X TF. You may want to use the nvidia-tensorflow version which is basically a nvidia maintained version of tensorflow 1.15 which is compatible with CUDA >11 (and so on RTX 30 gpus).
Source: https://developer.nvidia.com/blog/accelerating-tensorflow-on-a100-gpus/

```

```

###### cannot install Tensorflow Version 1.15 through pip 
Python version 3.7 is required. 
```bash 
conda create -n tf python=3.7
```
Source: https://stackoverflow.com/questions/61491893/i-cannot-install-tensorflow-version-1-15-through-pip

###### tensorflow 1.15 installation numpy dependency
**TF 1.15.0** requires **"numpy<2.0,>=1.16.0"**  
**TF 1.15.5** requires **"numpy<1.19.0,>=1.16.0"**  
Source: https://stackoverflow.com/questions/74933267/tensorflow-1-15-installation-numpy-dependency

###### Environment: TensorFlow
Source: https://master--floydhub-docs.netlify.app/guides/tensorflow/#tensorflow-115
TensorFlow-1.15
```bash
absl-py==0.8.1
scikit-learn==0.22
matplotlib==3.1.2
numpy==1.17.4
```


Git repo:
`https://github.com/deepmind/deepmind-research/tree/master/learning_to_simulate`

```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
mkdir -p /scratch/vp91/env/tf1_15
conda create -p /scratch/vp91/env/tf1_15 python=3.7

conda activate /scratch/vp91/env/tf1_15
pip install nvidia-pyindex
```
Error-1
```bash
 /scratch/vp91/env/tf1_15/lib/python3.8/site-packages/setuptools/__init__.py:84: _DeprecatedInstaller: setuptools.installer and fetch_build_eggs are deprecated.
      !!
                   ********************************************************************************
              Requirements should be satisfied by a PEP 517 installer.
              If you are using pip, you can try `pip install --use-pep517`.
              ********************************************************************************
...
× Encountered error while trying to install package.
╰─> nvidia-pyindex

**note**: This is an issue with the package mentioned above, not pip.
**hint**: See above for output from the failure.
```
Compatible version not needed???

Clean and Retry
```bash
# Does not work
rm -r /scratch/vp91/env/tf1_15
conda create -p /scratch/vp91/env/tf1_15 python=3.8
conda activate /scratch/vp91/env/tf1_15
pip install -U pip setuptools
pip install nvidia-pyindex
```

```bash
# Does Not work 
rm -r /scratch/vp91/env/tf1_15
conda create -p /scratch/vp91/env/tf1_15 python=3.9
conda activate /scratch/vp91/env/tf1_15
conda install -c conda-forge python-devtools
pip3 install --upgrade pip setuptools requests
python -m pip3 install nvidia-pyindex
```

```bash
# Does not work
conda create -p /scratch/vp91/env/tf1_15 python=3.10
conda activate /scratch/vp91/env/tf1_15
conda install -c conda-forge python-devtools
pip install --upgrade pip setuptools requests
pip install pyproject-toml

```

2023.06.07
None works cleaning env
```
conda deactivate
rm -r /scratch/vp91/env/tf1_15
```

### Working
Source: https://github.com/NVIDIA/tensorflow/issues/15
```bash
rm -r /scratch/fp0/mah900/env/sTF1.15
conda create -p /scratch/fp0/mah900/env/sTF1.15 python=3.8
conda activate /scratch/fp0/mah900/env/sTF1.15

# Seems working
pip install --extra-index-url=https://pypi.ngc.nvidia.com --trusted-host pypi.ngc.nvidia.com nvidia-tensorflow

pip install --extra-index-url=https://pypi.ngc.nvidia.com --trusted-host pypi.ngc.nvidia.com nvidia-tensorflow[horovod]

```



Test A100 - 1 GPU
```bash
qsub -I ~\conda\a1_h.5.pbs
...
[mah900@gadi-dgx-a100-0001 ~] . "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
[mah900@gadi-dgx-a100-0001 ~] conda activate /scratch/fp0/mah900/env/sTF1.15
library libcudart.so.12
Horovod v0.27.0:

Available Frameworks:
    [X] TensorFlow
    [X] PyTorch
    [ ] MXNet

Available Controllers:
    [X] MPI
    [ ] Gloo 

Available Tensor Operations:
    [X] NCCL
    [ ] DDL
    [ ] CCL
    [X] MPI
    [ ] Gloo

...
(/scratch/fp0/mah900/env/sTF1.15) [mah900@gadi-dgx-a100-0001 ~]$ python
from tensorflow.python.client import device_lib
local_device_protos = device_lib.list_local_devices()
Created TensorFlow device (/device:GPU:0 with 78879 MB memory) -> physical GPU (device: 0, name: NVIDIA A100-SXM4-80GB, pci bus id: 0000:87:00.0, compute capability: 8.0)

[x.name for x in local_device_protos if x.device_type == 'GPU']
['/device:GPU:0']

```
Source: https://stackoverflow.com/questions/38559755/how-to-get-current-available-gpus-in-tensorflow

Test A100 - 2GPU
```bash
 qsub -I  ~/conda/a2_h.5.pbs
 
from tensorflow.python.client import device_lib
local_device_protos = device_lib.list_local_devices()
Created TensorFlow device (/device:GPU:0 with 78879 MB memory) -> physical GPU (device: 0, name: NVIDIA A100-SXM4-80GB, pci bus id: 0000:07:00.0, compute capability: 8.0)

2023-06-07 23:55:22.697308: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1359] Created TensorFlow device (/device:GPU:1 with 78879 MB memory) -> physical GPU (device: 1, name: NVIDIA A100-SXM4-80GB, pci bus id: 0000:0f:00.0, compute capability: 8.0)

[x.name for x in local_device_protos if x.device_type == 'GPU']
['/device:GPU:0', '/device:GPU:1']
```

Test A100-8GPUs
```

```


### Requirement file
```python

import matplotlib (No module named 'matplotlib')
import tree (No module named 'tree')
import sklearn (No module named 'sklearn')
import tensorflow_probability as tfp (No module named 'tensorflow_probability')
import sonnet as snt (No module named 'sonnet')
import numpy (ok)
import tensorflow as tf1 (ok)
import graph_nets (No module named 'graph_nets')
import absl (ok)
 
```

`requirment.txt`
```bash
graph-nets>=1.1
dm-sonnet<2
tensorflow_probability<0.9
scikit-learn
dm-tree
matplotlib
# Run 
pip install --no-cache-dir -r requirements.txt

```
Error-1
```
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.

nvidia-tensorflow 1.15.5+nv23.3 requires gast==0.3.3, but you have gast 0.2.2 which is incompatible.
```
Try
```
pip install gast==0.3.3
```
Source: https://github.com/tensorflow/tensorflow/issues/38734
Error-2
```bash
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.

tensorflow-probability 0.8.0 requires gast<0.3,>=0.2, but you have gast 0.3.3 which is incompatible.
```
Try
```bash
pip install tensorflow-probability==0.10.0 --no-cache-dir
```
Source: https://www.kaggle.com/code/alexisbcook/kernel228d6327bc
```bash
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.

dm-sonnet 1.36 requires tensorflow-probability<0.9.0,>=0.8.0, but you have tensorflow-probability 0.10.0 which is incompatible.
```
Try
```bash
pip install tensorflow-probability==0.9.0 --no-cache-dir
```
Try
```bash
pip install "dm-sonnet<2"  --no-cache-dir
```
Error-4
```
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.

nvidia-tensorflow 1.15.5+nv23.3 requires gast==0.3.3, but you have gast 0.2.2 which is incompatible.
```
Try
```bash
pip install gast==0.3.3 "dm-sonnet<2" --no-cache-dir
...
Successfully installed cloudpickle-2.2.1 dm-sonnet-1.35 gast-0.3.3 tensorflow-probability-0.20.1
```

### 2023.06.08
Test 
```python
#train.py
import collections
import functools
import json
import os
import pickle

from absl import app
from absl import flags
from absl import logging
import numpy as np
import tensorflow.compat.v1 as tf
from tensorflow.compat.v1 import estimator as tf_estimator
import tree

#from learning_to_simulate import learned_simulator
#from learning_to_simulate import noise_utils
#from learning_to_simulate import reading_utils
```


```python
#learned_simulator.py
import graph_nets as gn
import sonnet as snt
import tensorflow.compat.v1 as tf

```
Error-5
```bash
import graph_nets as gn
...
ImportError: This version of TensorFlow Probability requires TensorFlow version >= 2.12; Detected an installation of version 1.15.5. Please upgrade TensorFlow to proceed.

SystemError: Sonnet requires tensorflow_probability (minimum version 0.4.0) to be installed. If using pip, run `pip install tensorflow-probability` or `pip install tensorflow-probability-gpu`
```
Try
```bash
#Not worked
pip install tensorflow-probability==0.11.0 --no-cache-dir
# Not worked 
pip install tensorflow-probability==0.9.0 --no-cache-dir
# Showed install error but impor work 
#(So, the version is imcompatitbe for install but imoport works for now)
pip install tensorflow-probability==0.8 --no-cache-dir


```
`connectivity_utils.py`
```python
import functools

import numpy as np
from sklearn import neighbors
import tensorflow.compat.v1 as tf
```
Error 
```bash
ModuleNotFoundError: No module named 'sklearn'
```
Try (worked)
```bash
pip install -U scikit-learn scipy matplotlib
```
source: https://stackoverflow.com/questions/46113732/modulenotfounderror-no-module-named-sklearn

`graph_network.py` (worked)
```python
from typing import Callable

import graph_nets as gn
import sonnet as snt
import tensorflow as tf
```

`render_rollout.py` (worked)
```python
import pickle

from absl import app
from absl import flags

from matplotlib import animation
import matplotlib.pyplot as plt
import numpy as np
```