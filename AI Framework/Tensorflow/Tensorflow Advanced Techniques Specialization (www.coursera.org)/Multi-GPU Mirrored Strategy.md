
https://github.com/giacomomiolo/tensorflow-advanced-techniques-specialization/blob/master/Course%202/Labs/C2_W4_Lab_2_multi-GPU-mirrored-strategy.ipynb


In this ungraded lab, you'll go through how to set up a Multi-GPU Mirrored Strategy. The lab environment only has a CPU but we placed the code here in case you want to try this out for yourself in a multiGPU device.

**Notes:**

- If you are running this on Coursera, you'll see it gives a warning about no presence of GPU devices.
- If you are running this in Colab, make sure you have selected your `runtime` to be `GPU`.
- In both these cases, you'll see there's only 1 device that is available.
- One device is sufficient for helping you understand these distribution strategies.



