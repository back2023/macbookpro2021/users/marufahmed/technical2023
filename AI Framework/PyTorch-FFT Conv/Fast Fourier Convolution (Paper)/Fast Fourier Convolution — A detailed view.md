

https://medium.com/@SambasivaraoK/fast-fourier-convolution-a-detailed-view-a5149aae36c4


Hello all, Hope you are doing good. In this post, we will go through a research paper named “[Fast Fourier Convolution](https://papers.nips.cc/paper/2020/file/2fd5d41ec6cfab47e32164d5624269b1-Paper.pdf)”. This paper explores the problems with the convolution operator in CNNs and proposes a new operator to replace the convolution operator. This has improved the accuracies for computer vision tasks such as image classification, action recognition etc.,

The Resnet architecture has been modified to include the FFC operator in place of convolution layers and has shown an improvement of 1.5 in Top-1 accuracy on the Imagenet dataset. The accuracies have improved in a similar manner for action recognition and human keypoint detection tasks across different architectures. More of this is in the results section.


