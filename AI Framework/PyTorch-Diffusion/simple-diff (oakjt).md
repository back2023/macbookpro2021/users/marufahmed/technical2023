

https://github.com/oakjt/simple-diff


## About

Simple diffusion implementation in PyTorch


# Simple Diffusion

[](https://github.com/oakjt/simple-diff#simple-diffusion)

This repository contains a Jupyter notebook for PyTorch implementation of a simple diffusion model without text embeddings.

