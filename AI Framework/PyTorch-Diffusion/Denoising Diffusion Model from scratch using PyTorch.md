

https://medium.com/@mickael.boillaud/denoising-diffusion-model-from-scratch-using-pytorch-658805d293b4


# **I Introduction**

Before delving into the nitty-gritty details of how the Denoising Diffusion Probabilistic Model (DDPM) works, let’s take a look at some historical perspectives on Generative AI.


