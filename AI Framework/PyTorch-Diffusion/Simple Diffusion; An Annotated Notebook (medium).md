

https://medium.com/@gian.favero/simple-diffusion-an-annotated-notebook-21ee342a6f52


# Background

There are a few papers in the diffusion model literature that I frequently find myself coming back to, but none more than “simple diffusion: End-to-end diffusion for high resolution images” by Hoogeboom et al.

This tutorial serves as an interface between the beautiful, yet simply presented, concepts in the [paper](https://arxiv.org/pdf/2301.11093) with a PyTorch implementation. The full repository can be found [here](https://github.com/faverogian/simpleDiffusion/).


