

https://www.kaggle.com/code/setseries/diffusion-models-pytorch

## Guided Diffusion Model implementation in pytorch[¶](https://www.kaggle.com/code/setseries/diffusion-models-pytorch#Guided-Diffusion-Model-implementation-in-pytorch)

linkcode

- Fundamentally, diffusion models work by destroying training data through the successive addition of Gaussian noise, and then learning to recover the data by reversing this noising process.
    
- In other words, diffusion models are parameterized Markov chains models trained to gradually denoise data. After training, we can use the diffusion model to generate data by simply passing randomly sampled noise through the learned denoising process.
    
- In simple words, diffusion models undergo the process of transforming a random collection of numbers (the “latents tensor”) into a processed collection of numbers containing the right image information.

