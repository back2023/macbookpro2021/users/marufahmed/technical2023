
https://github.com/lucidrains/denoising-diffusion-pytorch


Implementation of [Denoising Diffusion Probabilistic Model](https://arxiv.org/abs/2006.11239) in Pytorch. It is a new approach to generative modeling that may [have the potential](https://ajolicoeur.wordpress.com/the-new-contender-to-gans-score-matching-with-langevin-sampling/) to rival GANs. It uses denoising score matching to estimate the gradient of the data distribution, followed by Langevin sampling to sample from the true distribution.

This implementation was inspired by the official Tensorflow version [here](https://github.com/hojonathanho/diffusion)

Youtube AI Educators - [Yannic Kilcher](https://www.youtube.com/watch?v=W-O7AZNzbzQ) | [AI Coffeebreak with Letitia](https://www.youtube.com/watch?v=344w5h24-h8) | [Outlier](https://www.youtube.com/watch?v=HoKDTa5jHvg)

[Flax implementation](https://github.com/yiyixuxu/denoising-diffusion-flax) from [YiYi Xu](https://github.com/yiyixuxu)

[Annotated code](https://huggingface.co/blog/annotated-diffusion) by Research Scientists / Engineers from [🤗 Huggingface](https://huggingface.co/)

Update: Turns out none of the technicalities really matters at all | ["Cold Diffusion" paper](https://arxiv.org/abs/2208.09392) | [Muse](https://muse-model.github.io/)


