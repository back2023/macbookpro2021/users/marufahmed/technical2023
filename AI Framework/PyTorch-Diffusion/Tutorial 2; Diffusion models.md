

https://deeplearning.neuromatch.io/tutorials/W2D4_GenerativeModels/student/W2D4_Tutorial2.html#


# Tutorial Objectives[](https://deeplearning.neuromatch.io/tutorials/W2D4_GenerativeModels/student/W2D4_Tutorial2.html#tutorial-objectives "Permalink to this heading")

- Understand the idea behind Diffusion generative models: using score to enable reversal of diffusion process.
    
- Learn the score function by learning to denoise data.
    
- Hands-on experience in learning the score to a generate certain distribution.

