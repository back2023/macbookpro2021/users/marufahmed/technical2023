
https://towardsdatascience.com/implementing-yann-lecuns-lenet-5-in-pytorch-5e05a0911320


Recently, I watched the [Data Science Pioneers](https://www.datascience.movie/) movie by Dataiku, in which several data scientists talked about their jobs and how they apply data science in their daily jobs. In one of the talks, they mention how Yann LeCun’s Convolutional Neural Network architecture (also known as [LeNet-5)](https://en.wikipedia.org/wiki/LeNet) was used by the American Post office to automatically identify handwritten zip code numbers. Another real-world application of the architecture was recognizing the numbers written on cheques by banking systems. It makes sense to point out that the LeNet-5 paper was published in 1998. That is one of the reasons why it is a good starting point to understand how CNNs work, before moving to more complex and modern architectures.




