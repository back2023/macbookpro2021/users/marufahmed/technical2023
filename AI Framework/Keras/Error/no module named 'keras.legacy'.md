
https://stackoverflow.com/questions/63062833/no-module-named-keras-legacy/63063107


It looks like the Keras team removed the legacy module as of version 2.4.0. ([https://github.com/keras-team/keras/releases](https://github.com/keras-team/keras/releases))

Try downgrading your version to 2.3.1: `conda install keras=2.3.1`


