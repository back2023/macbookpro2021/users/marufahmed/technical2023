
https://stackoverflow.com/questions/42785026/tf-nn-conv2d-vs-tf-layers-conv2d

As GBY mentioned, they use the same implementation.

There is a slight difference in the parameters.

For tf.nn.conv2d:

```python
filter: A Tensor. Must have the same type as input. A 4-D tensor of shape [filter_height, filter_width, in_channels, out_channels]
```

For tf.layers.conv2d:

```python
filters: Integer, the dimensionality of the output space (i.e. the number of filters in the convolution).
```

I would use tf.nn.conv2d when loading a pretrained model (example code: [https://github.com/ry/tensorflow-vgg16](https://github.com/ry/tensorflow-vgg16)), and tf.layers.conv2d for a model trained from scratch.


