
https://stackoverflow.com/questions/37891954/keras-how-do-i-predict-after-i-trained-a-model

"
[`model.predict()`](http://keras.io/models/model/#predict) expects the first parameter to be a numpy array. You supply a list, which does not have the `shape` attribute a numpy array has.

Otherwise your code looks fine, except that you are doing nothing with the prediction. Make sure you store it in a variable, for example like this:

```python
prediction = model.predict(np.array(tk.texts_to_sequences(text)))
print(prediction)
```
"