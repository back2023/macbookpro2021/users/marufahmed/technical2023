

https://docs.w3cub.com/tensorflow~2.3/keras/utils/multi_gpu_model


**Warning:** THIS FUNCTION IS DEPRECATED. It will be removed after 2020-04-01. Instructions for updating: Use [`tf.distribute.MirroredStrategy`](https://docs.w3cub.com/tensorflow~2.3/distribute/mirroredstrategy) instead.

Specifically, this function implements single-machine multi-GPU data parallelism. It works in the following way:

- Divide the model's input(s) into multiple sub-batches.
- Apply a model copy on each sub-batch. Every model copy is executed on a dedicated GPU.
- Concatenate the results (on CPU) into one big batch.

E.g. if your `batch_size` is 64 and you use `gpus=2`, then we will divide the input into 2 sub-batches of 32 samples, process each sub-batch on one GPU, then return the full batch of 64 processed samples.

This induces quasi-linear speedup on up to 8 GPUs.

This function is only available with the TensorFlow backend for the time being.


