
https://stackoverflow.com/questions/64780641/whats-the-equivalent-of-tf-keras-input-in-pytorch

There's no equivalent in PyTorch to the Keras' Input. All you have to do is pass on the inputs as a tensor to the PyTorch model.

For eg: If you're working with a Conv net:

```python

# Keras Code
input_image = Input(shape=(32,32,3)) # An input image of 32x32x3 (HxWxC)
feature = Conv2D(16, activation='relu', kernel_size=(3, 3))(input_image)
```

```python
# PyTorch code
input_tensor = torch.randn(1, 3, 32, 32) # An input tensor of shape BatchSizexChannelsxHeightxWidth
feature = nn.Conv2d(in_channels=3, out_channels=16, kernel_size=(2, 2))(input_tensor)
```

If it is a normal Dense layer:

```python
# Keras
dense_input = Input(shape=(1,))
features = Dense(100)(dense_input)
```

```python
# PyTorch
input_tensor = torch.tensor([10]) # A 1x1 tensor with value 10
features = nn.Linear(1, 100)(input_tensor)
```