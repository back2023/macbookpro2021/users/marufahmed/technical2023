

### `LeakyReLU` class

`keras.layers.LeakyReLU(negative_slope=0.3, **kwargs)`

Leaky version of a Rectified Linear Unit activation layer.

This layer allows a small gradient when the unit is not active.

Source: https://keras.io/api/layers/activation_layers/leaky_relu/


# LEAKYRELU[](https://pytorch.org/docs/stable/generated/torch.nn.LeakyReLU.html#leakyrelu)

```
CLASS_torch.nn.LeakyReLU(_negative_slope=0.01_, _inplace=False_)
```

Sorce: https://pytorch.org/docs/stable/generated/torch.nn.LeakyReLU.html