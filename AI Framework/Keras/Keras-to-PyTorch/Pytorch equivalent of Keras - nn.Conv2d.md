
https://discuss.pytorch.org/t/pytorch-equivalent-of-keras/29412/1

The `in_channels` in Pytorch’s `nn.Conv2d` correspond to the number of channels in your input.  
Based on the input shape, it looks like you have 1 channel and a spatial size of `28x28`.  
Your first conv layer expects 28 input channels, which won’t work, so you should change it to 1.

Also the `Dense` layers in Keras give you the number of output units.  
For `nn.Linear` you would have to provide the number if `in_features` first, which can be calculated using your layers and input shape or just by printing out the shape of the activation in your `forward` method.

Let’s walk through your layers:
- After the first conv layer, your output will have the shape `[batch_size, 28, 26, 26]`. The 28 is given by the number of kernels your conv layer is using. Since you are not using any padding and leave the stride and dilation as 1, a kernel size of 3 will crop 1 pixel in each spatial dimension. Therefore you’ll end up with 28 activation maps of spatial size `26x26`.
- The max pooling layer will halve your spatial size, so that you’ll en up with `[batch_size, 28, 13, 13]`.
- The linear layer should therefore take `28*13*13=4732` input features.