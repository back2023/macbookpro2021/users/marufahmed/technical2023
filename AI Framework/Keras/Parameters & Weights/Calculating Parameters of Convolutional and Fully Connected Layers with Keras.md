

https://dingyan89.medium.com/calculating-parameters-of-convolutional-and-fully-connected-layers-with-keras-186590df36c6


When we build a model of deep learning, we always use a convolutional layer followed by a pooling layer and several fully-connected layers. It is necessary to know how many parameters in our model as well as the output shape of each layer. Let’s first see [LeNet-5](http://yann.lecun.com/exdb/publis/pdf/lecun-98.pdf)[1] which a classic architecture of the convolutional neural network.

