
https://stackoverflow.com/questions/67353874/tensorflow-and-torch-on-the-same-environment

nstall tensorflow-gpu=2.6.0 and PyTorch with cudatoolkit=11.3 and its working fine with anaconda.

```
conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch
conda install tensorflow-gpu
```

***


 
It also depends on your CUDA version required for tensorflow, pytorch and other frameworks you might use (like rapids, spacy, etc.).

So far I use:

```
conda install -c conda-forge "cudatoolkit=11.7"
conda install -c conda-forge tensorflow-gpu
conda install -c pytorch pytorch torchvision torchaudio "pytorch-cuda=11.7"
```

Prefer to do that within a conda environment and not to mess with conda base enviroment.

 
