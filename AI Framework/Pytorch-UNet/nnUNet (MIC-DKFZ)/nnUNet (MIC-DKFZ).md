
https://github.com/MIC-DKFZ/nnUNet

# Welcome to the new nnU-Net!

[](https://github.com/MIC-DKFZ/nnUNet#welcome-to-the-new-nnu-net)

Click [here](https://github.com/MIC-DKFZ/nnUNet/tree/nnunetv1) if you were looking for the old one instead.

Coming from V1? Check out the [TLDR Migration Guide](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/tldr_migration_guide_from_v1.md). Reading the rest of the documentation is still strongly recommended ;-)

## **2024-04-18 UPDATE: New residual encoder UNet presets available!**

[](https://github.com/MIC-DKFZ/nnUNet#2024-04-18-update-new-residual-encoder-unet-presets-available)

Residual encoder UNet presets substantially improve segmentation performance. They ship for a variety of GPU memory targets. It's all awesome stuff, promised! Read more 👉 [here](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/resenc_presets.md) 👈

Also check out our [new paper](https://arxiv.org/pdf/2404.09556.pdf) on systematically benchmarking recent developments in medical image segmentation. You might be surprised!

# What is nnU-Net?

[](https://github.com/MIC-DKFZ/nnUNet#what-is-nnu-net)

Image datasets are enormously diverse: image dimensionality (2D, 3D), modalities/input channels (RGB image, CT, MRI, microscopy, ...), image sizes, voxel sizes, class ratio, target structure properties and more change substantially between datasets. Traditionally, given a new problem, a tailored solution needs to be manually designed and optimized - a process that is prone to errors, not scalable and where success is overwhelmingly determined by the skill of the experimenter. Even for experts, this process is anything but simple: there are not only many design choices and data properties that need to be considered, but they are also tightly interconnected, rendering reliable manual pipeline optimization all but impossible!