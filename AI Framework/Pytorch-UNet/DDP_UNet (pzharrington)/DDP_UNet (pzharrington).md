
https://github.com/pzharrington/DDP_UNet

## About

3D U-Net for pytorch DDP training

# DDP 3D U-Net

[](https://github.com/pzharrington/DDP_UNet#ddp-3d-u-net)

---

Basic 3D U-Net in pytorch, with support for distriuted data parallel (DDP) training and automatic mixed precision training. Currently configured for N-body -> hydro project.

