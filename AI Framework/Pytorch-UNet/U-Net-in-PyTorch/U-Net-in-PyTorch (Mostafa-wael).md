
https://github.com/Mostafa-wael/U-Net-in-PyTorch/tree/main


# U-Net-in-PyTorch

[](https://github.com/Mostafa-wael/U-Net-in-PyTorch/tree/main#u-net-in-pytorch)

This is an implementation of the U-Net model from the paper, [U-Net: Convolutional Networks for Biomedical Image Segmentation](https://papers.labml.ai/paper/1505.04597).



