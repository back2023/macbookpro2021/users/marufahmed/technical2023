

https://github.com/clemkoa/u-net/tree/master


# U-net

[](https://github.com/clemkoa/u-net/tree/master#u-net)

A simple pytorch implementation of U-net, as described in the paper: [https://arxiv.org/abs/1505.04597](https://arxiv.org/abs/1505.04597)


# U-Net: Convolutional Networks for Biomedical Image Segmentation

https://arxiv.org/pdf/1505.04597


