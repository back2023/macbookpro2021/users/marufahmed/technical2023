

https://stackoverflow.com/questions/59115888/transforms-normalize-between-0-and-1-when-using-lab




As you can see, PyTorch complains about the Tensor size, since you lack a channel.

Additionally, the "usual" mean and std values are computed on ImageNet dataset, and are useful if the statistics of your data match the ones of that dataset.

As you work with two channels only, I assume that your domain might be fairly different from 3-channels natural images. In that case I would simply use `0.5` for both `mean` and `std`, such that the minimum value 0 will be converted to `(0 - 0.5) / 0.5 = -1` and the maximum value of 1 to `(1 - 0.5) / 0.5 = 1`.

```python
transform = transforms.Compose([
   transforms.ToTensor(),
   transforms.Normalize(mean=[0.5, 0.5],
                        std=[0.5, 0.5])
])
```

---

Edit: I would recommend zero-centering of the input.

However, if for some reason you must have it in range [0, 1], calling only `ToTensor()` would suffice.