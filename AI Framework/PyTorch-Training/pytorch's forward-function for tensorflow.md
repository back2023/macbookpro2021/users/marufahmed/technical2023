https://stackoverflow.com/questions/74990695/pytorchs-forward-function-for-tensorflow

"The forward function in nn.module in Pytorch can be replaced by the function `"__call__()"` in tf.module in Tensorflow or the function `call()` in tf.keras.layers.layer in keras. This is an example of a simple dense layer in tensorflow and keras:"