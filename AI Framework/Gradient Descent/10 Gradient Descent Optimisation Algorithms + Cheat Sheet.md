
Gradient descent is an optimization algorithm used for minimizing the cost function in various ML algorithms. Here are some common gradient descent optimisation algorithms used in the popular deep learning frameworks such as TensorFlow and Keras.

https://www.kdnuggets.com/2019/06/gradient-descent-algorithms-cheat-sheet.html