
https://www.sciencedirect.com/topics/mathematics/convolution-theorem

The so-called convolution theorem states that the Fourier transform of a convolution is equal to the multiplicative product of the individual Fourier transforms of the two quantities to be convolved.



# Convolution Theorem
https://www.sciencedirect.com/topics/computer-science/convolution-theorem



