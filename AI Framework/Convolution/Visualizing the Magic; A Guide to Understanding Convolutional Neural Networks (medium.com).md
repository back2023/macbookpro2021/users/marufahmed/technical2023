
https://medium.com/@neethamadhu.ma/visualizing-the-magic-a-guide-to-understanding-convolutional-neural-networks-c701978373c0


Neural networks are considered to be “Black boxes” in the machine learning community. Although it is true to a certain extent, we still can understand how Convolutional Neural Networks — CNNs see the world by visualizing the outputs of those layers.

I’ve been referring to the book “Deep Learning with Python” by François Chollet for the past few days and this is what I learned from it.

I assume that you have experience with some sort of image classification model at this point. (At least MNIST Handwritten Digit classification model). Therefore, implementing and training the CNN image classification model is not explained in this article.



# VisualizingCNN (GitHub)
Neethamadhu-Madurasinghe/VisualizingCNN

https://github.com/Neethamadhu-Madurasinghe/VisualizingCNN



