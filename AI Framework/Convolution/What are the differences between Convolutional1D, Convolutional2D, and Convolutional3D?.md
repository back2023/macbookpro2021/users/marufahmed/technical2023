

The only difference is the dimensionality of the input space. The input for a convolutional layer has the following shape:

input_shape = (batch_size,input_dims,channels)

- Input shape for **conv1D**: (batch_size,W,channels)
    
    Example: 1 second stereo voice signal sampled at 44100 Hz, shape: (batch_size,44100,2)
    
- Input shape for **conv2D**: (batch_size,(H,W),channels)
    
    Example: 32x32 RGB image, shape: (batch_size,32,32,3)
    
- Input shape for **conv3D**: (batch_size,(H,w,D),channels)
    
    Example (more tricky): 1 second video of 32x32 RGB images at 24 fps, shape: (batch_size,32,32,3,24)

https://datascience.stackexchange.com/questions/51470/what-are-the-differences-between-convolutional1d-convolutional2d-and-convoluti