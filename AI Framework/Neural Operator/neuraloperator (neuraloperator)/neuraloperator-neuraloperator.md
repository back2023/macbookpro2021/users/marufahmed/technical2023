

## Neural Operator

`neuraloperator` is a comprehensive library for learning neural operators in PyTorch. It is the official implementation for Fourier Neural Operators and Tensorized Neural Operators.

Unlike regular neural networks, neural operators enable learning mapping between function spaces, and this library provides all of the tools to do so on your own data.

NeuralOperators are also resolution invariant, so your trained operator can be applied on data of any resolution.

### [](https://github.com/neuraloperator/neuraloperator#installation)


https://github.com/neuraloperator/neuraloperator