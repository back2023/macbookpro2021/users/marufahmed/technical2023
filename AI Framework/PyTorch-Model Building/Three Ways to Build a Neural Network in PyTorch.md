

https://towardsdatascience.com/three-ways-to-build-a-neural-network-in-pytorch-8cea49f9a61a


**TLDR;**  
This isn’t meant to be a tutorial on PyTorch nor an article that explains how a neural network works. Instead, I thought it would be a good idea to share some of the stuff I’ve learned in the Udacity Bertelsmann Scholarship, AI Program. Having said this, the goal of this article is to illustrate a few different ways that one can create a neural network in PyTorch.

Prerequisites: I assume you know what a neural network is and how they work…so let’s dive in!


