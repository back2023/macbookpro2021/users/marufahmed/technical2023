

 So in this tutorial, I will show you how you can use [`PyTorch Lightning`](https://github.com/PyTorchLightning/pytorch-lightning) to predict real estate prices of houses through matching image data and tabular information. [You can find the sample data sets used here](https://1drv.ms/u/s!AqUPqx8G81xZiL1l80RtZbjPj43MhA?e=KagzKc). The full working code is available through my [GitHub repository](https://github.com/MarkusRosen/pytorch_multi_input_example).

https://rosenfelder.ai/multi-input-neural-network-pytorch/


### [pytorch_multi_input_example](https://github.com/MarkusRosen/pytorch_multi_input_example)

https://github.com/MarkusRosen/pytorch_multi_input_example

