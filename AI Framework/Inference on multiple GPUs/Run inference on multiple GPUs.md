
https://discuss.pytorch.org/t/run-inference-on-multiple-gpus/179628


It seems one cannot use `DDP` to run inference. Hence my question boils down to: **what’s the easiest way to run inference using multiple GPUs?**

https://discuss.pytorch.org/t/run-inference-on-multiple-gpus/179628/2
***
indeed, I didn’t quite get what `DDP` should be used for.  
I managed to get things working using `torch.utils.data.distributed.DistributedSampler` & sending inputs to the right gpus. I’ve added hereunder a snippet of the code working, in case someone is interested.




