
https://gharibim.github.io/main/

# MOHAMED GHARIBI

ML ENGINEER @IBM · PHD IN CS · RESEARCH ENGINEER @OPENMINEDORG · FEDERATED LEARNING | SMPC | DP  
(913) 850-1395 · [MGGVF@MAIL.UMKC.EDU](mailto:name@email.com)


The author of [Federated Learning Course on Udemy](https://www.udemy.com/course/federated_learning). I hold the TensorFlow Developer Certificate from Google.

You are welcome to read my blogs on Intro to DL, Embeddings, nbdev, [and many others...](https://medium.com/@gharibimo)

[](https://medium.com/@gharibimo)