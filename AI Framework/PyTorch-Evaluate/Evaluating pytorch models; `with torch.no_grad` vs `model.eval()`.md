

https://stackoverflow.com/questions/55627780/evaluating-pytorch-models-with-torch-no-grad-vs-model-eval

## TL;DR:

[Use _both_](https://github.com/pytorch/pytorch/issues/19160#issuecomment-482188706). They do different things, and have different scopes.

- `with torch.no_grad` - disables tracking of gradients in `autograd`.
- `model.eval()` changes the `forward()` behaviour of the module it is called upon
- eg, it disables dropout and has batch norm use the entire population statistics

