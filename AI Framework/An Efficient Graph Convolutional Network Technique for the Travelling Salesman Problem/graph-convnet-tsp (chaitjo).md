
https://github.com/chaitjo/graph-convnet-tsp
# An Efficient Graph Convolutional Network Technique for the Travelling Salesman Problem


This repository contains code for the paper [**"An Efficient Graph Convolutional Network Technique for the Travelling Salesman Problem"**](https://arxiv.org/abs/1906.01227) by Chaitanya K. Joshi, Thomas Laurent and Xavier Bresson.

We introduce a new learning-based approach for approximately solving the Travelling Salesman Problem on 2D Euclidean graphs. We use deep Graph Convolutional Networks to build efficient TSP graph representations and output tours in a non-autoregressive manner via highly parallelized beam search. Our approach outperforms all recently proposed autoregressive deep learning techniques in terms of solution quality, inference speed and sample efficiency for problem instances of fixed graph sizes.

