

[PyGAD](https://pygad.readthedocs.io/) is a genetic algorithm Python 3 library for solving optimization problems. One of these problems is training machine learning algorithms.

PyGAD has a module called [pygad.kerasga](https://github.com/ahmedfgad/KerasGA). It trains Keras models using the genetic algorithm. On January 3rd, 2021, a new release of [PyGAD 2.10.0](https://pygad.readthedocs.io/) brought a new module called [pygad.torchga](https://github.com/ahmedfgad/TorchGA) to train PyTorch models. It’s very easy to use, but there are a few tricky steps.

So, in this tutorial, we’ll explore how to use PyGAD to train PyTorch models.

https://neptune.ai/blog/train-pytorch-models-using-genetic-algorithm-with-pygad


