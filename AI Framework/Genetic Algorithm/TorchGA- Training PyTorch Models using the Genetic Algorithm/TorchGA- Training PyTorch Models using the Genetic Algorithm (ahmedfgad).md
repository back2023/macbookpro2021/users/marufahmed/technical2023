
[TorchGA](https://github.com/ahmedfgad/TorchGA) is part of the [PyGAD](https://pypi.org/project/pygad) library for training [PyTorch](https://pytorch.org/) models using the genetic algorithm (GA). This feature is supported starting from [PyGAD](https://pypi.org/project/pygad) 2.10.0.

The [TorchGA](https://github.com/ahmedfgad/TorchGA) project has a single module named `torchga.py` which has a class named `TorchGA` for preparing an initial population of PyTorch model parameters.

[PyGAD](https://pypi.org/project/pygad) is an open-source Python library for building the genetic algorithm and training machine learning algorithms. Check the library's documentation at [Read The Docs](https://pygad.readthedocs.io/): [https://pygad.readthedocs.io](https://pygad.readthedocs.io/)

# [](https://github.com/ahmedfgad/TorchGA#donation)


https://github.com/ahmedfgad/TorchGA