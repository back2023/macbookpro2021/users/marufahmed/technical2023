
https://stackoverflow.com/questions/57237352/what-does-unsqueeze-do-in-pytorch




`unsqueeze` turns an n.d. tensor into an (n+1).d. one by adding an extra dimension of depth 1. However, since it is ambiguous which axis the new dimension should lie across (i.e. in which direction it should be "unsqueezed"), this needs to be specified by the [`dim`](https://pytorch.org/docs/stable/generated/torch.unsqueeze.html) argument.