
https://github.com/lucidrains/vit-pytorch

## Vision Transformer - Pytorch

[](https://github.com/lucidrains/vit-pytorch#vision-transformer---pytorch)

Implementation of [Vision Transformer](https://openreview.net/pdf?id=YicbFdNTTy), a simple way to achieve SOTA in vision classification with only a single transformer encoder, in Pytorch. Significance is further explained in [Yannic Kilcher's](https://www.youtube.com/watch?v=TrdevFK_am4) video. There's really not much to code here, but may as well lay it out for everyone so we expedite the attention revolution.

For a Pytorch implementation with pretrained models, please see Ross Wightman's repository [here](https://github.com/rwightman/pytorch-image-models).

The official Jax repository is [here](https://github.com/google-research/vision_transformer).

A tensorflow2 translation also exists [here](https://github.com/taki0112/vit-tensorflow), created by research scientist [Junho Kim](https://github.com/taki0112)! 🙏


