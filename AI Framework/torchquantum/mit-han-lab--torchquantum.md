
https://github.com/mit-han-lab/torchquantum

## A PyTorch Library for Quantum Simulation and Quantum Machine Learning


# 👋 Welcome

#### [](https://github.com/mit-han-lab/torchquantum#what-it-is-doing)What it is doing

Quantum simulation framework based on PyTorch. It supports statevector simulation and pulse simulation (coming soon) on GPUs. It can scale up to the simulation of 30+ qubits with multiple GPUs.

#### [](https://github.com/mit-han-lab/torchquantum#who-will-benefit)Who will benefit

Researchers on quantum algorithm design, parameterized quantum circuit training, quantum optimal control, quantum machine learning, quantum neural networks.

#### [](https://github.com/mit-han-lab/torchquantum#differences-from-qiskitpennylane)Differences from Qiskit/Pennylane

Dynamic computation graph, automatic gradient computation, fast GPU support, batch model tersorized processing.






