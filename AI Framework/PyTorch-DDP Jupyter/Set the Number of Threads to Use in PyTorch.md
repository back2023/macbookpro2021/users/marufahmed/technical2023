
https://jdhao.github.io/2020/07/06/pytorch_set_num_threads/


In this post, I will share how PyTorch set the number of the threads to use for its operations.

`torch.set_num_threads()` is used to set the number of threads used for intra operations on CPU. According to discussions [here](https://stackoverflow.com/q/41233635/6064933), intra operation roughly means operations executed within an operation, for example, for matrix multiplication. By default, pytorch will use all the available cores on the computer, to verify this, we can use `torch.get_num_threads()` get the default threads number.

For operations supporting parallelism, increase the number of threads will usually leads to faster execution on CPU. Apart from setting the number of threads via `torch.set_num_threads`, we can also set the env variable `OMP_NUM_THREADS` or `MKL_NUM_THREADS` to set the number of threads. Below is a toy script to verify this (adapted from code [here](https://github.com/pytorch/pytorch/issues/7087#issue-318787926)):

