
https://discuss.pytorch.org/t/how-to-use-netcdf-data-for-dcgan/124870


"You could try to create an “image-like” input tensor in the shape `[batch_size, channels, height, width]`, where the `channels` could be set to 1, since you are dealing with a single float for each pixel location, and the `height` and `width` could be set to the `lat` and `lon`."