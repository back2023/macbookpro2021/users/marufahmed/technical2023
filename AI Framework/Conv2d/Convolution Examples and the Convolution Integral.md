

https://dspillustrations.com/pages/posts/misc/convolution-examples-and-the-convolution-integral.html


In this notebook, we will illustrate the convolution operation. The convolution of two signals is a fundamental operation in signal processing. Mainly, because the output of any linear time-invariant (LTI) system is given by the convolution of its impulse response with the input signal. Another important application of convolution is the [convolution theorem](http://dspillustrations.com/pages/posts/misc/the-convolution-theorem-and-application-examples.html), which states that multiplication in time-domain corresponds to convolution in frequency domain and vice versa.


