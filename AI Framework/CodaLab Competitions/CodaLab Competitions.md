
**Welcome to CodaLab main server!**

- This is a NEW server, you must re-create your Codalab account if you previously had one.
- We provide a default CPU queue for small jobs (typically for result submission competitions). For code submission, organizers are expected to provide their own queue, see [instructions](https://github.com/codalab/codalab-competitions/wiki/User_Using-your-own-compute-workers).


https://codalab.lisn.upsaclay.fr/

