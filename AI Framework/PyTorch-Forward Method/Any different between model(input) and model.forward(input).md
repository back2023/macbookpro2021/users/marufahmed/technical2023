
https://discuss.pytorch.org/t/any-different-between-model-input-and-model-forward-input/3690


You should avoid calling `Module.forward`.  
The difference is that all the hooks are dispatched in the `__call__` function, so if you call `.forward` and have hooks in your model, the hooks won’t have any effect


