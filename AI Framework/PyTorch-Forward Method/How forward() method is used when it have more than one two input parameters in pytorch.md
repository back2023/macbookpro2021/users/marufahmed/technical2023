
https://stackoverflow.com/questions/60463821/how-forward-method-is-used-when-it-have-more-than-one-two-input-parameters-in


```python
def forward(self, input1, input2, input3):
    x = self.layer1(input1)
    y = self.layer2(input2)
    z = self.layer3(input3)

    net = torch.cat((x,y,z),1)
     
    return net
```

