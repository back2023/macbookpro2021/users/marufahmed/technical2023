
https://www.tensorflow.org/tutorials/generative/generate_images_with_stable_diffusion


## Overview

In this guide, we will show how to generate novel images based on a text prompt using the KerasCV implementation of [stability.ai](https://stability.ai/)'s text-to-image model, [Stable Diffusion](https://github.com/CompVis/stable-diffusion).

Stable Diffusion is a powerful, open-source text-to-image generation model. While there exist multiple open-source implementations that allow you to easily create images from textual prompts, KerasCV's offers a few distinct advantages. These include [XLA compilation](https://www.tensorflow.org/xla) and [mixed precision](https://www.tensorflow.org/guide/mixed_precision) support, which together achieve state-of-the-art generation speed.

In this guide, we will explore KerasCV's Stable Diffusion implementation, show how to use these powerful performance boosts, and explore the performance benefits that they offer.


