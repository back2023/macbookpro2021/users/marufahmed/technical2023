

https://medium.com/@bloggerlifeofdang/medical-image-reconstruction-using-generative-adversarial-networks-gans-87c2c3b224d


_A Pytorch based implementation to GANs to reconstruct medical images_

I will discuss the process of image reconstruction on the Kaggle dataset of NIH chest data using GANs. All the code is implemented in Pytorch and both the results will be shown here, good and bad in which the model got overfitted.


