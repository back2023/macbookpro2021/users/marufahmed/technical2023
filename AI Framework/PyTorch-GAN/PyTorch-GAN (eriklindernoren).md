

https://github.com/eriklindernoren/PyTorch-GAN


## PyTorch-GAN

Collection of PyTorch implementations of Generative Adversarial Network varieties presented in research papers. Model architectures will not always mirror the ones proposed in the papers, but I have chosen to focus on getting the core ideas covered instead of getting every layer configuration right. Contributions and suggestions of GANs to implement are very welcomed.

**See also:** [Keras-GAN](https://github.com/eriklindernoren/Keras-GAN)

## [](https://github.com/eriklindernoren/PyTorch-GAN#table-of-contents)Table of Contents

- [Installation](https://github.com/eriklindernoren/PyTorch-GAN#installation)
- [Implementations](https://github.com/eriklindernoren/PyTorch-GAN#implementations)
    - [Auxiliary Classifier GAN](https://github.com/eriklindernoren/PyTorch-GAN#auxiliary-classifier-gan)
    - [Adversarial Autoencoder](https://github.com/eriklindernoren/PyTorch-GAN#adversarial-autoencoder)
    - [BEGAN](https://github.com/eriklindernoren/PyTorch-GAN#began)
    - [BicycleGAN](https://github.com/eriklindernoren/PyTorch-GAN#bicyclegan)
    - [Boundary-Seeking GAN](https://github.com/eriklindernoren/PyTorch-GAN#boundary-seeking-gan)
    - [Cluster GAN](https://github.com/eriklindernoren/PyTorch-GAN#cluster-gan)
    - [Conditional GAN](https://github.com/eriklindernoren/PyTorch-GAN#conditional-gan)
    - [Context-Conditional GAN](https://github.com/eriklindernoren/PyTorch-GAN#context-conditional-gan)
    - [Context Encoder](https://github.com/eriklindernoren/PyTorch-GAN#context-encoder)
    - [Coupled GAN](https://github.com/eriklindernoren/PyTorch-GAN#coupled-gan)
    - [CycleGAN](https://github.com/eriklindernoren/PyTorch-GAN#cyclegan)
    - [Deep Convolutional GAN](https://github.com/eriklindernoren/PyTorch-GAN#deep-convolutional-gan)
    - [DiscoGAN](https://github.com/eriklindernoren/PyTorch-GAN#discogan)
    - [DRAGAN](https://github.com/eriklindernoren/PyTorch-GAN#dragan)
    - [DualGAN](https://github.com/eriklindernoren/PyTorch-GAN#dualgan)
    - [Energy-Based GAN](https://github.com/eriklindernoren/PyTorch-GAN#energy-based-gan)
    - [Enhanced Super-Resolution GAN](https://github.com/eriklindernoren/PyTorch-GAN#enhanced-super-resolution-gan)
    - [GAN](https://github.com/eriklindernoren/PyTorch-GAN#gan)
    - [InfoGAN](https://github.com/eriklindernoren/PyTorch-GAN#infogan)
    - [Least Squares GAN](https://github.com/eriklindernoren/PyTorch-GAN#least-squares-gan)
    - [MUNIT](https://github.com/eriklindernoren/PyTorch-GAN#munit)
    - [Pix2Pix](https://github.com/eriklindernoren/PyTorch-GAN#pix2pix)
    - [PixelDA](https://github.com/eriklindernoren/PyTorch-GAN#pixelda)
    - [Relativistic GAN](https://github.com/eriklindernoren/PyTorch-GAN#relativistic-gan)
    - [Semi-Supervised GAN](https://github.com/eriklindernoren/PyTorch-GAN#semi-supervised-gan)
    - [Softmax GAN](https://github.com/eriklindernoren/PyTorch-GAN#softmax-gan)
    - [StarGAN](https://github.com/eriklindernoren/PyTorch-GAN#stargan)
    - [Super-Resolution GAN](https://github.com/eriklindernoren/PyTorch-GAN#super-resolution-gan)
    - [UNIT](https://github.com/eriklindernoren/PyTorch-GAN#unit)
    - [Wasserstein GAN](https://github.com/eriklindernoren/PyTorch-GAN#wasserstein-gan)
    - [Wasserstein GAN GP](https://github.com/eriklindernoren/PyTorch-GAN#wasserstein-gan-gp)
    - [Wasserstein GAN DIV](https://github.com/eriklindernoren/PyTorch-GAN#wasserstein-gan-div)

