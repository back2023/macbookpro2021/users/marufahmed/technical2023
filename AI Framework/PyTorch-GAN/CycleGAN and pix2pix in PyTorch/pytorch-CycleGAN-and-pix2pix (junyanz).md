

https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix


## About

Image-to-Image Translation in PyTorch

# CycleGAN and pix2pix in PyTorch

[](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix#cyclegan-and-pix2pix-in-pytorch)

**New**: Please check out [img2img-turbo](https://github.com/GaParmar/img2img-turbo) repo that includes both pix2pix-turbo and CycleGAN-Turbo. Our new one-step image-to-image translation methods can support both paired and unpaired training and produce better results by leveraging the pre-trained StableDiffusion-Turbo model. The inference time for 512x512 image is 0.29 sec on A6000 and 0.11 sec on A100.

Please check out [contrastive-unpaired-translation](https://github.com/taesungp/contrastive-unpaired-translation) (CUT), our new unpaired image-to-image translation model that enables fast and memory-efficient training.

We provide PyTorch implementations for both unpaired and paired image-to-image translation.

The code was written by [Jun-Yan Zhu](https://github.com/junyanz) and [Taesung Park](https://github.com/taesungp), and supported by [Tongzhou Wang](https://github.com/SsnL).

This PyTorch implementation produces results comparable to or better than our original Torch software. If you would like to reproduce the same results as in the papers, check out the original [CycleGAN Torch](https://github.com/junyanz/CycleGAN) and [pix2pix Torch](https://github.com/phillipi/pix2pix) code in Lua/Torch.

**Note**: The current software works well with PyTorch 1.4. Check out the older [branch](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/tree/pytorch0.3.1) that supports PyTorch 0.1-0.3.

You may find useful information in [training/test tips](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/tips.md) and [frequently asked questions](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/qa.md). To implement custom models and datasets, check out our [templates](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix#custom-model-and-dataset). To help users better understand and adapt our codebase, we provide an [overview](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/overview.md) of the code structure of this repository.

**CycleGAN: [Project](https://junyanz.github.io/CycleGAN/) | [Paper](https://arxiv.org/pdf/1703.10593.pdf) | [Torch](https://github.com/junyanz/CycleGAN) | [Tensorflow Core Tutorial](https://www.tensorflow.org/tutorials/generative/cyclegan) | [PyTorch Colab](https://colab.research.google.com/github/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/CycleGAN.ipynb)**






## Talks and Course

[](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix#talks-and-course)

pix2pix slides: [keynote](http://efrosgans.eecs.berkeley.edu/CVPR18_slides/pix2pix.key) | [pdf](http://efrosgans.eecs.berkeley.edu/CVPR18_slides/pix2pix.pdf), CycleGAN slides: [pptx](http://efrosgans.eecs.berkeley.edu/CVPR18_slides/CycleGAN.pptx) | [pdf](http://efrosgans.eecs.berkeley.edu/CVPR18_slides/CycleGAN.pdf)

CycleGAN course assignment [code](http://www.cs.toronto.edu/~rgrosse/courses/csc321_2018/assignments/a4-code.zip) and [handout](http://www.cs.toronto.edu/~rgrosse/courses/csc321_2018/assignments/a4-handout.pdf) designed by Prof. [Roger Grosse](http://www.cs.toronto.edu/~rgrosse/) for [CSC321](http://www.cs.toronto.edu/~rgrosse/courses/csc321_2018/) "Intro to Neural Networks and Machine Learning" at University of Toronto. Please contact the instructor if you would like to adopt it in your course.

## Colab Notebook

[](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix#colab-notebook)

TensorFlow Core CycleGAN Tutorial: [Google Colab](https://colab.research.google.com/github/tensorflow/docs/blob/master/site/en/tutorials/generative/cyclegan.ipynb) | [Code](https://github.com/tensorflow/docs/blob/master/site/en/tutorials/generative/cyclegan.ipynb)

TensorFlow Core pix2pix Tutorial: [Google Colab](https://colab.research.google.com/github/tensorflow/docs/blob/master/site/en/tutorials/generative/pix2pix.ipynb) | [Code](https://github.com/tensorflow/docs/blob/master/site/en/tutorials/generative/pix2pix.ipynb)

PyTorch Colab notebook: [CycleGAN](https://colab.research.google.com/github/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/CycleGAN.ipynb) and [pix2pix](https://colab.research.google.com/github/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/pix2pix.ipynb)

ZeroCostDL4Mic Colab notebook: [CycleGAN](https://colab.research.google.com/github/HenriquesLab/ZeroCostDL4Mic/blob/master/Colab_notebooks_Beta/CycleGAN_ZeroCostDL4Mic.ipynb) and [pix2pix](https://colab.research.google.com/github/HenriquesLab/ZeroCostDL4Mic/blob/master/Colab_notebooks_Beta/pix2pix_ZeroCostDL4Mic.ipynb)


## Other implementations

[](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix#other-implementations)

### CycleGAN

[](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix#cyclegan)

[[Tensorflow]](https://github.com/leehomyc/cyclegan-1) (by Harry Yang), [[Tensorflow]](https://github.com/architrathore/CycleGAN/) (by Archit Rathore), [[Tensorflow]](https://github.com/vanhuyz/CycleGAN-TensorFlow) (by Van Huy), [[Tensorflow]](https://github.com/XHUJOY/CycleGAN-tensorflow) (by Xiaowei Hu), [[Tensorflow2]](https://github.com/LynnHo/CycleGAN-Tensorflow-2) (by Zhenliang He), [[TensorLayer1.0]](https://github.com/luoxier/CycleGAN_Tensorlayer) (by luoxier), [[TensorLayer2.0]](https://github.com/tensorlayer/cyclegan) (by zsdonghao), [[Chainer]](https://github.com/Aixile/chainer-cyclegan) (by Yanghua Jin), [[Minimal PyTorch]](https://github.com/yunjey/mnist-svhn-transfer) (by yunjey), [[Mxnet]](https://github.com/Ldpe2G/DeepLearningForFun/tree/master/Mxnet-Scala/CycleGAN) (by Ldpe2G), [[lasagne/Keras]](https://github.com/tjwei/GANotebooks) (by tjwei), [[Keras]](https://github.com/simontomaskarlsson/CycleGAN-Keras) (by Simon Karlsson), [[OneFlow]](https://github.com/Ldpe2G/DeepLearningForFun/tree/master/Oneflow-Python/CycleGAN) (by Ldpe2G)

### pix2pix

[](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix#pix2pix)

[[Tensorflow]](https://github.com/affinelayer/pix2pix-tensorflow) (by Christopher Hesse), [[Tensorflow]](https://github.com/Eyyub/tensorflow-pix2pix) (by Eyyüb Sariu), [[Tensorflow (face2face)]](https://github.com/datitran/face2face-demo) (by Dat Tran), [[Tensorflow (film)]](https://github.com/awjuliani/Pix2Pix-Film) (by Arthur Juliani), [[Tensorflow (zi2zi)]](https://github.com/kaonashi-tyc/zi2zi) (by Yuchen Tian), [[Chainer]](https://github.com/pfnet-research/chainer-pix2pix) (by mattya), [[tf/torch/keras/lasagne]](https://github.com/tjwei/GANotebooks) (by tjwei), [[Pytorch]](https://github.com/taey16/pix2pixBEGAN.pytorch) (by taey16)



