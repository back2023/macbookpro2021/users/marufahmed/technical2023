

https://towardsdatascience.com/how-to-train-a-gan-on-128-gpus-using-pytorch-9a5b27a52c73


If you’re into GANs, you know it can take a reaaaaaally long time to generate nice-looking outputs. With distributed training we can cut down that time dramatically.

In a different tutorial, I cover [9 things you can do to speed up your PyTorch models](https://towardsdatascience.com/9-tips-for-training-lightning-fast-neural-networks-in-pytorch-8e63a502f565). In this tutorial we’ll implement a GAN, and train it on 32 machines (each with 4 GPUs) using distributed DataParallel.


