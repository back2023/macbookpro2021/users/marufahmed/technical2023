

https://github.com/cyclomon/3dbraingen


## About

Official Pytorch Implementation of "Generation of 3D Brain MRI Using Auto-Encoding Generative Adversarial Network" (accepted by MICCAI 2019)


This repository provides a PyTorch implementation of 3D brain Generation. It can successfully generates plausible 3-dimensional brain MRI with Generative Adversarial Networks. Trained models are also provided in this page.


## Paper


"Generation of 3D Brain MRI Using Auto-Encoding Generative Adversarial Networks"

The 22nd International Conference on Medical Image Computing and Computer Assisted Intervention(MICCAI 2019) : ([https://arxiv.org/abs/1908.02498](https://arxiv.org/abs/1908.02498))