
https://stackoverflow.com/questions/69537802/pytorch-cuda-todevice-vs-device-device-for-tensors?rq=3


"The [`torch.Tensor.to`](https://pytorch.org/docs/stable/generated/torch.Tensor.to.html) function will make a copy of your tensor on the destination device. While setting the `device` option on initialization will place it there on init, so there is no copy involved."

