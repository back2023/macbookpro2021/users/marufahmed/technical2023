
https://stackoverflow.com/questions/50954479/using-cuda-with-pytorch


You can use the [`tensor.to(device)`](https://pytorch.org/docs/stable/tensors.html?highlight=#torch.Tensor.to) command to move a tensor to a device.

The [`.to()`](https://pytorch.org/docs/stable/nn.html?highlight=#torch.nn.Module.to) command is also used to move a whole model to a device, like in the post you linked to.

Another possibility is to set the device of a tensor during creation using the `device=` keyword argument, like in `t = torch.tensor(some_list, device=device)`

To set the device dynamically in your code, you can use

```python
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
```

to set cuda as your device if possible.

There are various code examples on [PyTorch Tutorials](https://pytorch.org/tutorials/index.html) and in the documentation linked above that could help you.

[Share](https://stackoverflow.com/a/51014127 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/51014127/edit)


