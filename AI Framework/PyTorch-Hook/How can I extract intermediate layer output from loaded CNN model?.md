

https://discuss.pytorch.org/t/how-can-i-extract-intermediate-layer-output-from-loaded-cnn-model/77301


You could register a forward hook on `model.fc3` as described [here 10.0k](https://discuss.pytorch.org/t/how-can-l-load-my-best-model-as-a-feature-extractor-evaluator/17254/6) or alternatively manipulate the `forward` method and return the output activation of `self.fc3` as well as from `self.out`.


