

_Course website_: [https://uvadlc.github.io/](https://uvadlc.github.io/)
_Course edition_: DL1 - Fall 2022, DL2 - Spring 2022, Being kept up to date
_Repository_: [https://github.com/phlippe/uvadlc_notebooks](https://github.com/phlippe/uvadlc_notebooks)
_Recordings_: [YouTube Playlist](https://www.youtube.com/playlist?list=PLdlPlO1QhMiAkedeu0aJixfkknLRxk1nA)
_Author_: Phillip Lippe


https://uvadlc-notebooks.readthedocs.io/en/latest/index.html