
_Note: To look at the notebooks in a nicer format, visit our RTD website: [https://uvadlc-notebooks.readthedocs.io/en/latest/](https://uvadlc-notebooks.readthedocs.io/en/latest/)_

_Course website_: [https://uvadlc.github.io/](https://uvadlc.github.io/)  
_Course edition_: Fall 2022 (Nov. 01 - Dec. 24) - Being kept up to date  
_Recordings_: [YouTube Playlist](https://www.youtube.com/playlist?list=PLdlPlO1QhMiAkedeu0aJixfkknLRxk1nA)  
_Author_: Phillip Lippe

For this year's course edition, we created a series of Jupyter notebooks that are designed to help you understanding the "theory" from the lectures by seeing corresponding implementations. We will visit various topics such as optimization techniques, transformers, graph neural networks, and more (for a full list, see below). The notebooks are there to help you understand the material and teach you details of the **PyTorch** framework, including **PyTorch Lightning**. Further, we provide one-to-one translations of the notebooks to **JAX+Flax** as alternative framework.

The notebooks are presented in the first hour of every group tutorial session. During the tutorial sessions, we will present the content and explain the implementation of the notebooks. You can decide yourself whether you just want to look at the filled notebook, want to try it yourself, or code along during the practical session. The notebooks are not directly part of any mandatory assignments on which you would be graded or similarly. However, we encourage you to get familiar with the notebooks and experiment or extend them yourself. Further, the content presented will be relevant for the graded assignment and exam.

The tutorials have been integrated as official tutorials of PyTorch Lightning. Thus, you can also view them in [their documentation](https://pytorch-lightning.readthedocs.io/en/latest/).


https://github.com/phlippe/uvadlc_notebooks
