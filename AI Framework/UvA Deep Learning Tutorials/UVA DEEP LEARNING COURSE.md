
MSc in Artificial Intelligence for the University of Amsterdam.

## About

---

Deep learning is primarily a study of multi-layered neural networks, spanning over a great range of model architectures. This course is taught in the MSc program in Artificial Intelligence of the University of Amsterdam. In this course we study the theory of deep learning, namely of modern, multi-layered neural networks trained on big data. The course is taught by Assistant Professor [Yuki Asano](mailto:%20y.m.asano@uva.nl) with Head Teaching Assistants [Christos Athanasiadis](mailto:c.athanasiadis@uva.nl) and [Phillip Lippe](https://phlippe.github.io/). The teaching assistants are [Joris Baan](mailto:j.s.baan@uva.nl), [Piyush Bagad](mailto:piyush.bagad@student.uva.nl), [Leonard Bereska](mailto:l.f.bereska@uva.nl), [Floor Eijkelboom](mailto:eijkelboomfloor@gmail.com), [Alex Gabel](mailto:a.gabel@uva.nl), [Danilo de Goede](mailto:danilogoede@gmail.com), [Ivona Najdenkoska](mailto:i.najdenkoska@uva.nl), [Angelos Nalmpantis](mailto:angelos.nalmpantis@student.uva.nl), [Apostolos Panagiotopoulos](mailto:apostolos.panagiotopoulos@student.uva.nl), [Konstantinos Papakostas](mailto:konstantinos.papakostas@student.uva.nl), [Tadija Radusinovic](mailto:tadija.radusinovic@student.uva.nl), [Sarah Rastegar](mailto:s.rastegar2@uva.nl), [Mohammadreza Salehi](mailto:s.salehidehnavi@uva.nl), [Tin Hadzi Veljkovic](mailto:tin.hadzi@gmail.com), [Pengwan Yang](mailto:yangpengwan2016@gmail.com)



https://uvadlc.github.io/



