
https://www.kaggle.com/code/vortanasay/saving-loading-and-cont-training-model-in-pytorch


# Saving and Loading Models in PyTorch[¶](https://www.kaggle.com/code/vortanasay/saving-loading-and-cont-training-model-in-pytorch#Saving-and-Loading-Models-in-PyTorch)

In this notebook, I'll show you how to save, load and continue to train models with PyTorch.

[1] and [2] provide a good start for me to expands on this topic.

Imagine a case when you have been training your model for hours and suddenly the machine crashes or you lose connection to the remote GPU that you have been training your model on. Disaster right? Consider another case that you trained your model for certain epochs which already took a considerable amount of time, but you are not satisfied with the performance and you wish you had trained it for more epochs[2].

- If you follow along please, make sure checkpoint and best_model directories are created in your work directory:
    - I am going to save latest checkpoint in checkpoint directory
    - I am going to save best model/checkpoint in best_model directory


Reference:

- [[1]](https://www.kaggle.com/davidashraf/saving-and-loading-models-in-pytorch) [https://www.kaggle.com/davidashraf/saving-and-loading-models-in-pytorch](https://www.kaggle.com/davidashraf/saving-and-loading-models-in-pytorch)
- [[2]](https://medium.com/analytics-vidhya/saving-and-loading-your-model-to-resume-training-in-pytorch-cb687352fa61) [https://medium.com/analytics-vidhya/saving-and-loading-your-model-to-resume-training-in-pytorch-cb687352fa61](https://medium.com/analytics-vidhya/saving-and-loading-your-model-to-resume-training-in-pytorch-cb687352fa61)
- [[3]](https://pytorch.org/tutorials/beginner/saving_loading_models.html) [https://pytorch.org/tutorials/beginner/saving_loading_models.html](https://pytorch.org/tutorials/beginner/saving_loading_models.html)

