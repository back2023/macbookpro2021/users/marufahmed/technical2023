
http://www.idris.fr/eng/ia/apprentissage-distribue-eng.html

The training of a neural network model is faced with two major problems:

- The **training time** can be very long (several weeks).
    
- The model can be **too large for the GPU memory** (32 GB or 16 GB, depending on the partition used).
    

The solution to these two problems can be found in multi-GPU distribution. We distinguish between two techniques:

- **Data parallelism** which accelerates the model training by distributing each batch of data and therefore, each iteration upgrade of the model. In this case, the model is replicated on each utilised GPU to treat a data subset. It is _adapted to trainings involving voluminous data or large-sized batches_. This is the most documented distribution process and is the easiest one to implement.
    
- **Model parallelism** which is _adapted to models which are too large_ to be replicated and manipulated with the chosen batch size in each GPU. However, it is more difficult to implement, particularly in multi-nodes. It can be optimised with pipeline techniques but it cannot accelerate the training as significantly as data parallelism.
    

These two techniques can be used simultaneously (hybrid parallelism) to solve both problems at the same time (training time and model size) by grouping the GPUs in function of the task to be effectuated.


