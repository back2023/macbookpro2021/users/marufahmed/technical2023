http://www.idris.fr/eng/ia/model-parallelism-pytorch-eng.html


The methodology presented on this page shows how to concretely adapt a model which is too large for use on a single GPU. It illustrates the concepts presented on the main page: [Jean Zay: Multi-GPU and multi-node distribution for training a TensorFlow or PyTorch model](http://www.idris.fr/eng/ia/apprentissage-distribue-eng.html "web:eng:ia:apprentissage-distribue-eng").

The procedure involves the following steps:

- Adaptation of the model
    
- Adaptation of the training loop
    
- Configuration of the Slurm computing environment
    

The steps concerning the creation of the DataLoader and the saving/loading of the model are not modified, in comparison to a model deployed on a single GPU.





This Jupyter Notebook is run from a front end, after having loaded a PyTorch module (see [our JupyterHub documentation](http://www.idris.fr/eng/jean-zay/pre-post/jean-zay-jupyterhub-eng.html "web:eng:jean-zay:pre-post:jean-zay-jupyterhub-eng") for more information on how to run Jupyter Notebook).
http://www.idris.fr/static/notebook-ia/demo_model_parallelism_pytorch-eng.ipynb


