
#### 1. Use Pip
Conda install has 'glibc 2.29' not found problem.
Instead, use pip, recommanded  (https://szymonmaszke.github.io/torchdatasets/)
   ```bash
   pip install  torchdata
   ```
  ### Note:
  However, pip did uninstalled and re-installed torch
  
#### 2.  Tried. (Not Work)
   ```bash
   conda install -c conda-forge torchdata
   ...
   The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    portalocker-2.7.0          |   py39hf3d152e_0          31 KB  conda-forge
    python_abi-3.9             |           2_cp39           4 KB  conda-forge
    torchdata-0.4.1            |     pyh8b8bddf_0          54 KB  conda-forge
    ------------------------------------------------------------
                                           Total:          88 KB
    ...
    #Test
    python
    from torchdata import _torchdata as _torchdata      
    # Error
    ...
      File "/scratch/fp0/mah900/env/climaX_test/lib/python3.9/site-packages/torchdata/datapipes/iter/transform/callable.py", line 10, in <module>

    from torch.utils.data.datapipes.utils.common import _check_lambda_fn

ImportError: cannot import name '_check_lambda_fn' from 'torch.utils.data.datapipes.utils.common' (/scratch/fp0/mah900/env/climaX_test/lib/python3.9/site-packages/torch/utils/data/datapipes/utils/common.py)                                 
```

#### 3. Tried. (Not worked)
```
 conda install torchdata -c pytorch-nightly
 ...
 # Does find conda matching packages 
 Your installed version is: 2.28
```
