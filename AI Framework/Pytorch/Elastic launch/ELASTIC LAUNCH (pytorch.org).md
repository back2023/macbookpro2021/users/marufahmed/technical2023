
https://pytorch.org/elastic/0.2.0rc1/distributed.html

## Elastic launcher[](https://pytorch.org/elastic/0.2.0rc1/distributed.html#module-torchelastic.distributed.launch)

This module provides similar functionality as `torch.distributed.launch`, with the following additional functionalities:

1. Worker failures are handled gracefully by restarting all workers.
2. Worker `RANK` and `WORLD_SIZE` are assigned automatically.
3. Number of nodes is allowed to change between min and max sizes (elasticity).




