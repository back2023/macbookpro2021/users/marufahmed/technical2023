

**Author**: [Adam Paszke](https://github.com/apaszke)

This tutorial shows how to use PyTorch to train a Deep Q Learning (DQN) agent on the CartPole-v0 task from the [OpenAI Gym](https://gym.openai.com/).

**Task**

The agent has to decide between two actions - moving the cart left or right - so that the pole attached to it stays upright. You can find an official leaderboard with various algorithms and visualizations at the [Gym website](https://gym.openai.com/envs/CartPole-v0).

https://sebarnold.net/tutorials/intermediate/reinforcement_q_learning.html




