
https://lambdalabs.com/blog/multi-node-pytorch-distributed-training-guide


[PyTorch](https://pytorch.org/) is designed to be the framework that's both easy to use and delivers performance at scale. Indeed it has become [the most popular deep learning framework](https://paperswithcode.com/trends), by a mile among the research community. However, despite some [lengthy official tutorials](https://pytorch.org/tutorials/beginner/dist_overview.html) and a few helpful [community blogs](https://leimao.github.io/blog/PyTorch-Distributed-Training/), it is not always clear what exactly has to be done to make your PyTorch training to work **across multiple nodes**.


