
https://medium.com/codex/a-comprehensive-tutorial-to-pytorch-distributeddataparallel-1f4b42bb1b51

===== update 2023.8.19 =====

It has been a long time since I wrote this post. Today if you guys want to perform distributed training & evaluation, just turn to `transformers.Trainer` and `accelerate.Accelerator` , both of which were created and maintained by huggingface. They are very convenient tools to accomplish the same thing (and **far beyond**) as I wroted in the following. 🤗

===== Original Post =====

The limited computation resource at school discourages distibuted training across multiple gpus. I started to learn it for the first time when I joined Microsoft as an intern. It’s basically an easy job to wrap the model with DDP (short for DistributedDataParallel). What frustrated me was that I cannot properly adjust my workflow for multi-gpu, including `DataLoader`, `Sampler`, training and evaluating. The tutorials and blogs on Internet hardly includes all these stuff. After addressing so many bugs I came across, I’ve come up with the best practice so far.


