
https://forums.fast.ai/t/distributed-and-parallel-training-explained/73892


# [What is the difference between DataParallel and DistributedDataParallel?](https://discuss.pytorch.org/t/what-is-the-difference-between-dataparallel-and-distributeddataparallel/6108)
https://discuss.pytorch.org/t/what-is-the-difference-between-dataparallel-and-distributeddataparallel/6108

`DataParallel` is for performing [training on multiple GPUs, single machine 1.0k](http://pytorch.org/docs/master/nn.html?highlight=dataparallel#torch.nn.DataParallel).  
`DistributedDataParallel` is [useful when you want to use multiple machines 1.2k](https://github.com/pytorch/pytorch/blob/master/torch/nn/parallel/distributed.py#L24-L78).



