

_A solid foundation for your next vision and language research/production project_

Authored by: Facebook AI Research ([Amanpreet Singh](https://github.com/apsdehal) and [Vedanuj Goswami](https://github.com/vedanuj))

Reasoning across modalities is critical to intelligence. There is a growing need to model interactions between modalities (e.g., vision, language) — both to improve AI predictions on existing tasks and to enable new applications. Multimodal AI problems range from visual question answering, image captioning and visual dialogue to embodied AI, virtual assistants and detecting hateful content on social media. Better tools — both for researchers to develop novel ideas and for practitioners to productionize use cases — have potential to accelerate progress in multimodal AI.



https://medium.com/pytorch/bootstrapping-a-multimodal-project-using-mmf-a-pytorch-powered-multimodal-framework-464f75164af7