

## What is model ensembling?[](https://pytorch.org/functorch/1.13/notebooks/ensembling.html#what-is-model-ensembling)

Model ensembling combines the predictions from multiple models together. Traditionally this is done by running each model on some inputs separately and then combining the predictions. However, if you’re running models with the same architecture, then it may be possible to combine them together using `vmap`. `vmap` is a function transform that maps functions across dimensions of the input tensors. One of its use cases is eliminating for-loops and speeding them up through vectorization.


https://pytorch.org/functorch/1.13/notebooks/ensembling.html


