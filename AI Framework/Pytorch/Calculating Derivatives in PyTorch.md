
After going through this tutorial, you’ll learn:

- How to calculate derivatives in PyTorch.
- How to use autograd in PyTorch to perform auto differentiation on tensors.
- About the computation graph that involves different nodes and leaves, allowing you to calculate the gradients in a simple possible manner (using the chain rule).
- How to calculate partial derivatives in PyTorch.
- How to implement the derivative of functions with respect to multiple values.

https://machinelearningmastery.com/calculating-derivatives-in-pytorch/