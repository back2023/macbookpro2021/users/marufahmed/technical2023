
https://www.run.ai/guides/multi-gpu/pytorch-multi-gpu-4-techniques-explained


PyTorch provides a Python-based library package and a deep learning platform for scientific computing tasks. Learn four techniques you can use to accelerate tensor computations with PyTorch [multi GPU](https://www.run.ai/guides/multi-gpu) techniques—data parallelism, distributed data parallelism, model parallelism, and elastic training.

**In this article, you will learn:**

- [Technique 1: Data Parallelism](https://www.run.ai/guides/multi-gpu/pytorch-multi-gpu-4-techniques-explained#Technique-1)
- [Technique 2: Distributed Data Parallelism](https://www.run.ai/guides/multi-gpu/pytorch-multi-gpu-4-techniques-explained#Technique-2)
- [Technique 3: Model Parallelism](https://www.run.ai/guides/multi-gpu/pytorch-multi-gpu-4-techniques-explained#Technique-3)
- [Technique 4: Elastic Training](https://www.run.ai/guides/multi-gpu/pytorch-multi-gpu-4-techniques-explained#Technique-4)
- [PyTorch Multi GPU With Run:AI](https://www.run.ai/guides/multi-gpu/pytorch-multi-gpu-4-techniques-explained#PyTorch-Multi)