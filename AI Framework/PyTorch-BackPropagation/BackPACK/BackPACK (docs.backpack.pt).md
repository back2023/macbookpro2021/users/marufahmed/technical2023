


https://docs.backpack.pt/en/1.3.0/use_cases/example_custom_module.html


This tutorial shows how to support a custom module in a simple fashion. We focus on [BackPACK’s first-order extensions](https://docs.backpack.pt/en/master/extensions.html#first-order-extensions). They don’t backpropagate additional information and thus require less functionality be implemented.


