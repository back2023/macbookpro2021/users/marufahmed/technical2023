

The collection of pre-trained, state-of-the-art AI models.

# [](https://github.com/axinc-ai/ailia-models#about-ailia-sdk)About ailia SDK

[ailia SDK](https://axinc.jp/en/solutions/ailia_sdk.html) is a self-contained cross-platform high speed inference SDK for AI. ailia SDK provides a consistent C++ API on Windows, Mac, Linux, iOS, Android, Jetson and Raspberry Pi. It also supports Unity(C#), Python and JNI for efficient AI implementation. The ailia SDK makes great use of the GPU via Vulkan and Metal to serve accelerated computing.

https://github.com/axinc-ai/ailia-models
