
_mlprodict_ was initially started to help implementing converters to [ONNX](https://onnx.ai/). The main feature is a python runtime for [ONNX](https://onnx.ai/). It gives more feedback than [onnxruntime](https://github.com/microsoft/onnxruntime) when the execution fails.

http://www.xavierdupre.fr/app/mlprodict/helpsphinx/index.html

