
https://stackoverflow.com/questions/72808402/pytorch-identifying-batch-size-as-number-of-channels-in-conv2d-layer

"In pytorch, `nn.Conv2d` assumes the input (mostly image data) is shaped like: `[B, C_in, H, W]`, where `B` is the batch size, `C_in` is the number of channels, `H` and `W` are the height and width of the image. The output has a similar shape `[B, C_out, H_out, W_out]`. Here, `C_in` and `C_out` are `in_channels` and `out_channels`, respectively. `(H_out, W_out)` is the output image size, which may or may not equal `(H, W)`, depending on the kernel size, the stride and the padding.

However, it is confusing to apply conv2d to reduce `[128, 248, 46]` inputs to `[128, 50, 46]`. Are they image data with height 248 and width 46? If so you can reshape the inputs to `[128, 1, 248, 46]` and use `in_channels = 1` and `out_channels = 1` in conv2d."