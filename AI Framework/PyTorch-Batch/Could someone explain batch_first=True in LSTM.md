
https://discuss.pytorch.org/t/could-someone-explain-batch-first-true-in-lstm/15402

"If your input data is of shape `(seq_len, batch_size, features)` then you don’t need `batch_first=True` and your LSTM will give output of shape `(seq_len, batch_size, hidden_size)`.

If your input data is of shape `(batch_size, seq_len, features)` then you need `batch_first=True` and your LSTM will give output of shape `(batch_size, seq_len, hidden_size)`."