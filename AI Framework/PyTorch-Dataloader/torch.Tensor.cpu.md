https://pytorch.org/docs/stable/generated/torch.Tensor.cpu.html


Tensor.cpu(_memory_format=torch.preserve_format_) → [Tensor](https://pytorch.org/docs/stable/tensors.html#torch.Tensor "torch.Tensor")[](https://pytorch.org/docs/stable/generated/torch.Tensor.cpu.html#torch.Tensor.cpu)

Returns a copy of this object in CPU memory.

If this object is already in CPU memory and on the correct device, then no copy is performed and the original object is returned.