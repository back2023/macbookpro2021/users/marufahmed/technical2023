

https://stackoverflow.com/questions/65279115/how-to-use-collate-fn-with-dataloaders


Basically, the `collate_fn` receives a list of tuples if your `__getitem__` function from a Dataset subclass returns a tuple, or just a normal list if your Dataset subclass returns only one element. Its main objective is to create your batch without spending much time implementing it manually. Try to see it as a glue that you specify the way examples stick together in a batch. If you don’t use it, PyTorch only put `batch_size` examples together as you would using torch.stack (not exactly it, but it is simple like that).

Suppose for example, you want to create batches of a list of varying dimension tensors. The below code pads sequences with 0 until the maximum sequence size of the batch, that is why we need the collate_fn, because a standard batching algorithm (simply using `torch.stack`) won’t work in this case, and we need to manually pad different sequences with variable length to the same size before creating the batch.

