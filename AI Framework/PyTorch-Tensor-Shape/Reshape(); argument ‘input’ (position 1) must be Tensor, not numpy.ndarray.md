

https://discuss.pytorch.org/t/reshape-argument-input-position-1-must-be-tensor-not-numpy-ndarray/194901

Did you try to pass a tensor instead of a `numpy.array` as described in the error message?  
You can create tensors from `numpy.array`s via `tensor = torch.from_numpy(array)`.

