
https://discuss.pytorch.org/t/how-to-specify-filters-in-conv2d/133855

"out_channels are filters. The in_channels should be the previous layers out_channels. But if you are on the first Conv2d layer, the in_channels are 3 for rgb or 1 for grayscale."

[https://pytorch.org/docs/stable/generated/torch.nn.Conv2d.html 242](https://pytorch.org/docs/stable/generated/torch.nn.Conv2d.html)