
https://stackoverflow.com/questions/70231487/output-dimensions-of-convolution-in-pytorch

"
The calculation of feature maps is `[(W−K+2P)/S]+1` and here `[]` brackets means floor division. In your example padding is `zero`, so the calculation is `[(68-9+2*0)/4]+1 ->[14.75]=14 -> [14.75]+1 = 15` and `[(224-9+2*0)/4]+1 -> [53.75]=53 -> [53.75]+1 = 54`."

```python
import torch

conv1 = torch.nn.Conv2d(3, 16, stride=4, kernel_size=(9,9))
input = torch.rand(1, 3, 68, 224)

print(conv1(input).shape)
# torch.Size([1, 16, 15, 54])
```