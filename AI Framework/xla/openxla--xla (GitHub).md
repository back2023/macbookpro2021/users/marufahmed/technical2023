
https://github.com/openxla/xla


# XLA

XLA (Accelerated Linear Algebra) is an open-source machine learning (ML) compiler for GPUs, CPUs, and ML accelerators.

The XLA compiler takes models from popular ML frameworks such as PyTorch, TensorFlow, and JAX, and optimizes them for high-performance execution across different hardware platforms including GPUs, CPUs, and ML accelerators.

## [](https://github.com/openxla/xla#get-started)Get started

If you want to use XLA to compile your ML project, refer to the corresponding documentation for your ML framework:

- [PyTorch](https://pytorch.org/xla)
- [TensorFlow](https://www.tensorflow.org/xla)
- [JAX](https://jax.readthedocs.io/en/latest/notebooks/quickstart.html)

If you're not contributing code to the XLA compiler, you don't need to clone and build this repo. Everything here is intended for XLA contributors who want to develop the compiler and XLA integrators who want to debug or add support for ML frontends and hardware backends.