

https://www.kaggle.com/code/elanrob/image-captioning-with-pytorch-lstm


# Image Captioning with PyTorch LSTM[¶](https://www.kaggle.com/code/elanrob/image-captioning-with-pytorch-lstm#Image-Captioning-with-PyTorch-LSTM)

We'll be using a PyTorch implementation of an _LSTM_ (Long Short Term Memory) neural network to create this image captioning model. The image input will be preprocessed with one of torchvision's pretrained model and the partial caption (truncated at mid-sentence) will be preprocessed with the _word2vec_ algorithm.


