

Traditional neural network models are composed of a finite number of layers. Neural Differential Equations (NDEs), a core model class of the so-called _continuous-depth learning_ framework, challenge this notion by defining forward inference passes as the solution of an [initial value problem](https://en.wikipedia.org/wiki/Initial_value_problem). This effectively means that NDEs can be thought of as being comprised of a _continuum_ of layers, where the **vector field** itself is parametrized by an **arbitrary neural network**. Since [seminal work](https://arxiv.org/abs/1806.07366) that initially popularized the idea, [the](https://arxiv.org/abs/2001.01328) [framework](https://arxiv.org/abs/2002.08071) [has](https://arxiv.org/abs/1907.03907) [grown](https://arxiv.org/abs/2007.09601) [quite](https://arxiv.org/abs/2005.08926) [large](https://arxiv.org/abs/2002.02798), seeing applications in control, generative modeling and forecasting.



https://towardsdatascience.com/neural-odes-with-pytorch-lightning-and-torchdyn-87ca4a7c6ffd



