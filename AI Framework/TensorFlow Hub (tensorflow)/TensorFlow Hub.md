
https://github.com/tensorflow/hub/tree/master

[TensorFlow Hub](https://tfhub.dev/) is a repository of reusable assets for machine learning with [TensorFlow](https://www.tensorflow.org/). In particular, it provides pre-trained SavedModels that can be reused to solve new tasks with less training time and less training data.

This GitHub repository hosts the `tensorflow_hub` Python library to download and reuse SavedModels in your TensorFlow program with a minimum amount of code, as well as other associated code and documentation.
