https://www.tutorialspoint.com/how-to-join-tensors-in-pytorch

Both **torch.cat()** and **torch.stack()** are used to join the tensors. So, what is the basic difference between these two methods?

- **torch.cat()** concatenates a sequence of tensors along an existing dimension, hence not changing the dimension of the tensors.
    
- **torch.stack()** stacks the tensors along a new dimension, as a result, it increases the dimension.

