
https://discuss.pytorch.org/t/how-to-convert-a-list-of-tensors-to-a-pytorch-tensor/175666

"Depending on what exactly you want, you’ll most likely want to use either [stack 110](https://pytorch.org/docs/stable/generated/torch.stack.html?highlight=stack#torch.stack) (concatenation along a new dimension) or [cat 27](https://pytorch.org/docs/stable/generated/torch.cat.html?highlight=cat) (concatenation along an existing dimension)."

