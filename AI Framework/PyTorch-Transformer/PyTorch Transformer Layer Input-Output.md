

https://jamesmccaffrey.wordpress.com/2022/02/03/pytorch-transformer-layer-input-output/



The PyTorch neural library has a Transformer layer that can be used to construct a Transformer Architecture (TA) model. Typically, a library-defined Embedding layer, and a program-defined Positional layer, and a library-defined Linear layer are combined with a library-defined Transformer layer to create a TA model.



