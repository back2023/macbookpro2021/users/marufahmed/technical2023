
https://stackoverflow.com/questions/72274562/mseloss-from-pytorch


Please look in Pytorch docs: [https://pytorch.org/docs/stable/generated/torch.nn.MSELoss.html](https://pytorch.org/docs/stable/generated/torch.nn.MSELoss.html)

You need to create an MSELoss object before calling with the target and predictions.

```python
loss = nn.MSELoss()
input = torch.zeros(64, requires_grad=True)
target = torch.ones(64)
output = loss(input, target)
```