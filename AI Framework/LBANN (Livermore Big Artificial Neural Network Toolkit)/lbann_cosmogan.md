

**Project aim**:  
Build Generative Adversarial Neural networks to produce images of matter distribution in the universe for different sets of cosmological parameters. Dataset consisits of N-body cosmology simulations. The code is built using the LBANN framework.

https://github.com/vmos1/lbann_cosmogan


