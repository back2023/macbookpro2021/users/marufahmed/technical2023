
https://stats.stackexchange.com/questions/343763/fine-tuning-vs-transferlearning-vs-learning-from-scratch


**Transfer learning is when a model developed for one task is reused to work on a second task. Fine-tuning is one approach to transfer learning where you change the model output to fit the new task and train only the output model.**

In **Transfer Learning** or Domain Adaptation, we train the model with a dataset. Then, we train the same model with another dataset that has a different distribution of classes, or even with other classes than in the first training dataset).

In **Fine-tuning**, an approach of Transfer Learning, we have a dataset, and we use let's say 90% of it in training. Then, we train the same model with the remaining 10%. Usually, we change the learning rate to a smaller one, so it does not have a significant impact on the already adjusted weights. 
- You can also have a base model working for a similar task and then freezing some of the layers to keep the old knowledge when performing the new training session with the new data. The output layer can also be different and have some of it frozen regarding the training.

In my experience learning from scratch leads to better results, but it is much costly than the others especially regarding time and resources consumption.

**Using Transfer Learning you should freeze some layers**, mainly the pre-trained ones and only train in the added ones, **and decrease the learning rate** to adjust the weights without mixing their meaning for the network. If you speed up the learning rate you normally face yourself with poor results due to the big steps in the gradient descent optimisation. This can lead to a state where the neural network cannot find the global minimum but only a local one.

**Using a pre-trained model in a similar task, usually have great results when we use Fine-tuning**. However, **if you do not have enough data in the new dataset or even your hyperparameters are not the best ones, you can get unsatisfactory results**. Machine learning always depends on its dataset and network's parameters. And in that case, **you should only use the "standard" Transfer Learning**.

**So, we need to evaluate the trade-off between the resources and time consumption with the accuracy we desire, to choose the best approach.**



Also note, **transfer learning cannot be used all by itself** when learning from new data because of frozen parameters. Transfer learning needs to be combined with either fine tuning or training from scratch when learning from new data.













