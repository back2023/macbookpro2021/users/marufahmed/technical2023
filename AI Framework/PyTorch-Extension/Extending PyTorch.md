
https://pytorch.org/docs/master/notes/extending.html


In this note we’ll cover ways of extending [`torch.nn`](https://pytorch.org/docs/master/nn.html#module-torch.nn "torch.nn"), [`torch.autograd`](https://pytorch.org/docs/master/torch.html#module-torch.autograd "torch.autograd"), [`torch`](https://pytorch.org/docs/master/torch.html#module-torch "torch"), and writing custom C extensions utilizing our C libraries.


