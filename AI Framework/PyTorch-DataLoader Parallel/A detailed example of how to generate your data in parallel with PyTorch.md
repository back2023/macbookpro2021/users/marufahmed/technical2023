

https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel



## Motivation

Have you ever had to load a dataset that was so memory consuming that you wished a magic trick could seamlessly take care of that? Large datasets are increasingly becoming part of our lives, as we are able to harness an ever-growing quantity of data.

We have to keep in mind that in some cases, even the most state-of-the-art configuration won't have enough memory space to process the data the way we used to do it. That is the reason why we need to find other ways to do that task efficiently. In this blog post, we are going to show you how to **generate your data on multiple cores in real time** and **feed it right away** to your **deep learning model**.

This tutorial will show you how to do so on the GPU-friendly framework _PyTorch_, where an **efficient data generation scheme** is crucial to leverage the full potential of your GPU during the training process.
 
