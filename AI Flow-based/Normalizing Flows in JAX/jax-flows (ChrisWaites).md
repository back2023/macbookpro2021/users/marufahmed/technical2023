
https://github.com/ChrisWaites/jax-flows


# Normalizing Flows in JAX

 [![GitHub](https://camo.githubusercontent.com/317d1829c5ed73113999c264ce9528df8ec747fe6c0826499bd963d2b1ed5649/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6963656e73652f43687269735761697465732f6a61782d666c6f77732e7376673f636f6c6f723d626c7565)](https://github.com/ChrisWaites/jax-flows/blob/master/LICENSE)[![Documentation](https://camo.githubusercontent.com/7835909c2ca5fbbecf9a394a39c34617b4b68868af8438e3498a44495859e520/68747470733a2f2f696d672e736869656c64732e696f2f776562736974652f687474702f6a61782d666c6f77732e72656164746865646f63732e696f2e7376673f646f776e5f636f6c6f723d72656426646f776e5f6d6573736167653d6f66666c696e652675705f6d6573736167653d6f6e6c696e65)](https://jax-flows.readthedocs.io/en/latest/)

Implementations of normalizing flows (RealNVP, Glow, MAF) in the [JAX](https://github.com/google/jax/) deep learning framework.

## [](https://github.com/ChrisWaites/jax-flows#what-are-normalizing-flows)What are normalizing flows?

[Normalizing flow models](http://akosiorek.github.io/ml/2018/04/03/norm_flows.html) are _generative models_, i.e. they infer the underlying probability distribution of an observed dataset. With that distribution we can do a number of interesting things, namely sample new realistic points and query probability densities.



