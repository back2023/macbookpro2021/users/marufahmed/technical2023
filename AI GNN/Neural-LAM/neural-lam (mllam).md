
https://github.com/mllam/neural-lam


## About

Neural Weather Prediction for Limited Area Modeling

Neural-LAM is a repository of graph-based neural weather prediction models for Limited Area Modeling (LAM). The code uses [PyTorch](https://pytorch.org/) and [PyTorch Lightning](https://lightning.ai/pytorch-lightning). Graph Neural Networks are implemented using [PyG](https://pyg.org/) and logging is set up through [Weights & Biases](https://wandb.ai/).

The repository contains LAM versions of:

- The graph-based model from [Keisler (2022)](https://arxiv.org/abs/2202.07575).
- GraphCast, by [Lam et al. (2023)](https://arxiv.org/abs/2212.12794).
- The hierarchical model from [Oskarsson et al. (2023)](https://arxiv.org/abs/2309.17370).



