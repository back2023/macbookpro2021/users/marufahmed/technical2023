
https://github.com/google/trax

# Trax — Deep Learning with Clear Code and Speed

[![train tracks](https://camo.githubusercontent.com/6f1f2f1c71ce14de6dafa7325813f7a45573e70f026246bd7f65f52ecca2bdb9/68747470733a2f2f696d616765732e706578656c732e636f6d2f70686f746f732f3436313737322f706578656c732d70686f746f2d3436313737322e6a7065673f646c266669743d63726f702663726f703d656e74726f707926773d333226683d3231)](https://camo.githubusercontent.com/6f1f2f1c71ce14de6dafa7325813f7a45573e70f026246bd7f65f52ecca2bdb9/68747470733a2f2f696d616765732e706578656c732e636f6d2f70686f746f732f3436313737322f706578656c732d70686f746f2d3436313737322e6a7065673f646c266669743d63726f702663726f703d656e74726f707926773d333226683d3231) [![PyPI version](https://camo.githubusercontent.com/39505301bef5ab996cfaba29cbb1d6e2d4f673b4bcfe9a309d8ad4f5f457fa93/68747470733a2f2f62616467652e667572792e696f2f70792f747261782e737667)](https://badge.fury.io/py/trax) [![GitHub Issues](https://camo.githubusercontent.com/79efb5804c0d29faeddad37076c47beac2c76b541a0487e9a7ebbbd115a704f5/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6973737565732f676f6f676c652f747261782e737667)](https://github.com/google/trax/issues) [![GitHub Build](https://github.com/google/trax/actions/workflows/build.yaml/badge.svg)](https://github.com/google/trax/actions/workflows/build.yaml/badge.svg) [![Contributions welcome](https://camo.githubusercontent.com/4c7a1f7fc12ad8d5d41426272e1651664a994c8fd884100bf1c502604e747501/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f636f6e747269627574696f6e732d77656c636f6d652d627269676874677265656e2e737667)](https://github.com/google/trax/blob/master/CONTRIBUTING.md) [![License](https://camo.githubusercontent.com/a0336f108dbbb0b60eb442f4778035f553725d027a67dfa91f9bd719b5007afb/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4c6963656e73652d417061636865253230322e302d627269676874677265656e2e737667)](https://opensource.org/licenses/Apache-2.0) [![Gitter](https://camo.githubusercontent.com/9847ff4337174c20e5fe5d125928134dca305514e5c0b554013c3839992c9cf2/68747470733a2f2f696d672e736869656c64732e696f2f6769747465722f726f6f6d2f6e776a732f6e772e6a732e737667)](https://gitter.im/trax-ml/community)

[Trax](https://trax-ml.readthedocs.io/en/latest/) is an end-to-end library for deep learning that focuses on clear code and speed. It is actively used and maintained in the [Google Brain team](https://research.google.com/teams/brain/). This notebook ([run it in colab](https://colab.research.google.com/github/google/trax/blob/master/trax/intro.ipynb)) shows how to use Trax and where you can find more information.

1. **Run a pre-trained Transformer**: create a translator in a few lines of code
2. **Features and resources**: [API docs](https://trax-ml.readthedocs.io/en/latest/), where to [talk to us](https://gitter.im/trax-ml/community), how to [open an issue](https://github.com/google/trax/issues) and more
3. **Walkthrough**: how Trax works, how to make new models and train on your own data

We welcome **contributions** to Trax! We welcome PRs with code for new models and layers as well as improvements to our code and documentation. We especially love **notebooks** that explain how models work and show how to use them to solve problems!

Here are a few example notebooks:-

- [**trax.data API explained**](https://github.com/google/trax/blob/master/trax/examples/trax_data_Explained.ipynb) : Explains some of the major functions in the `trax.data` API
- [**Named Entity Recognition using Reformer**](https://github.com/google/trax/blob/master/trax/examples/NER_using_Reformer.ipynb) : Uses a [Kaggle dataset](https://www.kaggle.com/abhinavwalia95/entity-annotated-corpus) for implementing Named Entity Recognition using the [Reformer](https://arxiv.org/abs/2001.04451) architecture.
- [**Deep N-Gram models**](https://github.com/google/trax/blob/master/trax/examples/Deep_N_Gram_Models.ipynb) : Implementation of deep n-gram models trained on Shakespeares works

