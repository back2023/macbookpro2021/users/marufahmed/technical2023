

https://github.com/Michael-MuChienHsu/R_Unet?tab=readme-ov-file



# R_Unet

[](https://github.com/Michael-MuChienHsu/R_Unet?tab=readme-ov-file#r_unet)

This project applies recurrent method upon U-net to perform pixel level video frame prediction.  
Part of our result is published at [IEEE GCCE 2020](https://ieeexplore.ieee.org/document/9292008). [pdf](https://www.ams.giti.waseda.ac.jp/data/pdf-files/2020_GCCE_hsu.pdf).  

# Brief introduction

[](https://github.com/Michael-MuChienHsu/R_Unet?tab=readme-ov-file#brief-introduction)

Taking advantage of LSTM and U-net encode-decoder, we wish to be able in predicting next (n) frame(s).  
Currently using a 2 layer LSTM network (V1) or convolution LSTM (V2) as RNN network applying on latent feature of U net  
In our latest v4 model, we use convolutional LSTM in each level and take short cut in v2 out  

On the other hand, we are now using v4_mask model to train mask, image input and mask, image prediction output  
This model holds same structure as v4 but simply change output layer to output mask tensor.


