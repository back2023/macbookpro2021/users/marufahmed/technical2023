


https://mask-cond-video-diffusion.github.io/


- General purpose model for video generation, forward/backward prediction, and interpolation
- Uses a [score-based diffusion loss function](https://yang-song.github.io/blog/2021/score/) to generate novel frames
- Injects Gaussian noise into the current frames and denoises them conditional on past and/or future frames
- Randomly _masks_ past and/or future frames during training which allows the model to handle the four cases:
    - Unconditional Generation : both past and future are unknown
    - Future Prediction : only the past is known
    - Past Reconstruction : only the future is known
    - Interpolation : both past and present are known
- Uses a [2D convolutional U-Net](https://arxiv.org/abs/2006.11239) instead of a complex 3D or recurrent or transformer architecture
- Conditions on past and future frames through concatenation or space-time adaptive normalization
- Produces high-quality and diverse video samples
- Trains with only 1-4 GPUs
- Scales well with the number of channels, and could be scaled much further than in the paper

