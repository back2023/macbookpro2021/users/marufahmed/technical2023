

https://github.com/voletiv/mcvd-pytorch


Official implementation of MCVD: Masked Conditional Video Diffusion for Prediction, Generation, and Interpolation ([https://arxiv.org/abs/2205.09853](https://arxiv.org/abs/2205.09853))


