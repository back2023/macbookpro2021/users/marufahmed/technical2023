

https://github.com/Tobi-r9/RaMViD


Official code for the paper:

> [Diffusion Models for Video Prediction and Infilling](https://arxiv.org/abs/2206.07696)  
> Tobias Höppe, Arash Mehrjou, Stefan Bauer, Didrik Nielsen, Andrea Dittadi  
> TMLR, 2022

**Project website: [https://sites.google.com/view/video-diffusion-prediction](https://sites.google.com/view/video-diffusion-prediction)**

This code and README are based on [https://github.com/openai/improved-diffusion](https://github.com/openai/improved-diffusion)


