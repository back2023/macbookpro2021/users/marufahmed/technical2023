

https://medium.com/analytics-vidhya/review-of-futuregan-predict-future-video-frames-using-generative-adversarial-networks-gans-3120d90d54e0


Proposed by Aigner et al., [**FutureGAN**](https://arxiv.org/abs/1810.01325) is a GAN based framework for predicting future video frames. Video predictionis the ability to predict future video frames based onthe context of a sequence of previous frames. Unlike static images, videos provide complex transformations and motion patterns in the time dimension. Therefore to accurately predict future video frames the model needs to take care of both the temporal and spatial components. Typically Recurrent Neural Networks are used to model the temporal dynamics. However the authors of FutureGAN proposed the use of **Spatio-Temporal 3d-Convolutions** in Progressively Growing manner to predict future video frames.


