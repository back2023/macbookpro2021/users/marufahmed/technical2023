
https://github.com/TUM-LMF/FutureGAN


# FutureGAN - _Official PyTorch Implementation_


This is the official PyTorch implementation of FutureGAN. The code accompanies the paper ["FutureGAN: Anticipating the Future Frames of Video Sequences using Spatio-Temporal 3d Convolutions in Progressively Growing GANs"](https://arxiv.org/abs/1810.01325).

[![](https://github.com/TUM-LMF/FutureGAN/raw/master/imgsrc/Examples_FutureGAN.gif)](https://github.com/TUM-LMF/FutureGAN/blob/master/imgsrc/Examples_FutureGAN.gif)  
Predictions generated by our FutureGAN (red) conditioned on input frames (black).

