
https://github.com/XPixelGroup/HAT


# HAT [![Replicate](https://camo.githubusercontent.com/b4c3e26b84f9481a4dc498d3fce7583a10951e7535eb753e6895cfc7a330ae10/68747470733a2f2f7265706c69636174652e636f6d2f636a7762772f6861742f6261646765)](https://replicate.com/cjwbw/hat)

### [](https://github.com/XPixelGroup/HAT#activating-more-pixels-in-image-super-resolution-transformer-paper-link)Activating More Pixels in Image Super-Resolution Transformer [[Paper Link]](https://arxiv.org/abs/2205.04437)

[Xiangyu Chen](https://chxy95.github.io/), [Xintao Wang](https://xinntao.github.io/), [Jiantao Zhou](https://www.fst.um.edu.mo/personal/jtzhou/), [Yu Qiao](https://scholar.google.com.hk/citations?user=gFtI-8QAAAAJ) and [Chao Dong](https://scholar.google.com.hk/citations?user=OSDCB0UAAAAJ&hl=zh-CN)

### [](https://github.com/XPixelGroup/HAT#hat-hybrid-attention-transformer-for-image-restoration-paper-link)HAT: Hybrid Attention Transformer for Image Restoration [[Paper Link]](https://arxiv.org/abs/2309.05239)

[Xiangyu Chen](https://chxy95.github.io/), [Xintao Wang](https://xinntao.github.io/), [Wenlong Zhang](https://wenlongzhang0517.github.io/), [Xiangtao Kong](https://xiangtaokong.github.io/), [Jiantao Zhou](https://www.fst.um.edu.mo/personal/jtzhou/), [Yu Qiao](https://scholar.google.com.hk/citations?user=gFtI-8QAAAAJ) and [Chao Dong](https://scholar.google.com.hk/citations?user=OSDCB0UAAAAJ&hl=zh-CN)

## [](https://github.com/XPixelGroup/HAT#updates)