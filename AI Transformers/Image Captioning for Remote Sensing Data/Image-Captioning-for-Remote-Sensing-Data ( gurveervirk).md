
https://github.com/gurveervirk/Image-Captioning-for-Remote-Sensing-Data/tree/main?tab=readme-ov-file#image-captioning-for-remote-sensing-data


# Image Captioning for Remote Sensing Data

## Approach

- Found and compared models on HuggingFace and selected 2 models for finetuning:
    
    - [`nlpconnect/vit-gpt2-image-captioning`](https://huggingface.co/nlpconnect/vit-gpt2-image-captioning)
    - [`Salesforce/blip-image-captioning-base`](https://huggingface.co/Salesforce/blip-image-captioning-base)
- Finetuned both on the dataset (splits used for this were **train** and **valid** for training and evaluation purposes)
    
- Tested them on the **test** split and further tuned the training parameters
    
- Tested BLEU score for these models ("valid" split used):
    
    - Base models:
        
        - nlpconnect/vit-gpt2-image-captioning: 0.58
        - Salesforce/blip-image-captioning-base: 0.51
    - Finetuned (best):
        
        - nlpconnect/vit-gpt2-image-captioning: 0.55
        - Salesforce/blip-image-captioning-base: 0.56