
https://github.com/microsoft/unilm

## [aka.ms/GeneralAI](https://aka.ms/GeneralAI)

# [](https://github.com/microsoft/unilm#hiring)Hiring

We are hiring at all levels (including FTE researchers and interns)! If you are interested in working with us on Foundation Models (aka large-scale pre-trained models) and General AI, NLP, MT, Speech, Document AI and Multimodal AI, please send your resume to [](mailto:fuwei@microsoft.com)[fuwei@microsoft.com](mailto:fuwei@microsoft.com).

# [](https://github.com/microsoft/unilm#foundation-architecture)Foundation Architecture

### [](https://github.com/microsoft/unilm#torchscale---transformers-at-any-scale-repo)TorchScale - Transformers at (any) Scale ([repo](https://github.com/microsoft/torchscale))

Fundamental research to develop new architectures for foundation models and AI, focusing on modeling generality and capability, as well as training stability and efficiency.

> Stability - [**DeepNet**](https://github.com/microsoft/unilm/tree/master/deepnet): scaling Transformers to 1,000 Layers and beyond

> Generality - [**Foundation Transformers (Magneto)**](https://arxiv.org/abs/2210.06423): towards true general-purpose modeling across tasks and modalities (including language, vision, speech, and multimodal)

> Capability - A [**Length-Extrapolatable**](https://arxiv.org/abs/2212.10554) Transformer

> Efficiency & Transferability - [**X-MoE**](https://github.com/microsoft/unilm/tree/master/xmoe): scalable & finetunable sparse Mixture-of-Experts (MoE)

### [](https://github.com/microsoft/unilm#revolutionizing-transformers-for-mllms-and-ai)Revolutionizing Transformers for (M)LLMs and AI

> [**RetNet**](https://arxiv.org/abs/2307.08621): Retentive Network: A Successor to Transformer for Large Language Models

> [**LongNet**](https://arxiv.org/abs/2307.02486): Scaling Transformers to 1,000,000,000 Tokens

# [](https://github.com/microsoft/unilm#foundation-models)Foundation Models

### [](https://github.com/microsoft/unilm#llm--mllm-multimodal-llm)LLM / MLLM (Multimodal LLM)

> [**Kosmos-2**](https://github.com/microsoft/unilm/tree/master/kosmos-2): **Grounding Multimodal Large Language Models to the World**

> [**Kosmos-1**](https://arxiv.org/abs/2302.14045): **A Multimodal Large Language Model (MLLM)**

> [**MetaLM**](https://github.com/microsoft/unilm/tree/master/metalm): **Language Models are General-Purpose Interfaces**

**The Big Convergence** - Large-scale self-supervised pre-training across `tasks` (predictive and generative), `languages` (100+ languages), and `modalities` (language, image, audio, layout/format + language, vision + language, audio + language, etc.)

### [](https://github.com/microsoft/unilm#language--multilingual)Language & Multilingual

> [**UniLM**](https://github.com/microsoft/unilm/tree/master/unilm): unified pre-training for language understanding and generation

> [**InfoXLM/XLM-E**](https://github.com/microsoft/unilm/tree/master/infoxlm): multilingual/cross-lingual pre-trained models for 100+ languages

> [**DeltaLM/mT6**](https://github.com/microsoft/unilm/tree/master/deltalm): encoder-decoder pre-training for language generation and translation for 100+ languages

> [**MiniLM**](https://github.com/microsoft/unilm/tree/master/minilm): small and fast pre-trained models for language understanding and generation

> [**AdaLM**](https://github.com/microsoft/unilm/tree/master/adalm): domain, language, and task adaptation of pre-trained models

> [**EdgeLM**](https://github.com/microsoft/unilm/tree/master/edgelm)(`NEW`): small pre-trained models on edge/client devices

> [**SimLM**](https://github.com/microsoft/unilm/tree/master/simlm) (`NEW`): large-scale pre-training for similarity matching

> [**E5**](https://github.com/microsoft/unilm/tree/master/e5) (`NEW`): text embeddings

> [**MiniLLM**](https://arxiv.org/abs/2306.08543) (`NEW`): Knowledge Distillation of Large Language Models

### [](https://github.com/microsoft/unilm#vision)Vision

> [**BEiT**](https://github.com/microsoft/unilm/tree/master/beit)/[**BEiT-2**](https://github.com/microsoft/unilm/tree/master/beit2): generative self-supervised pre-training for vision / BERT Pre-Training of Image Transformers

> [**DiT**](https://github.com/microsoft/unilm/tree/master/dit): self-supervised pre-training for Document Image Transformers

> [**TextDiffuser**](https://github.com/microsoft/unilm/tree/master/textdiffuser) (`NEW`): Diffusion Models as Text Painters

### [](https://github.com/microsoft/unilm#speech)Speech

> [**WavLM**](https://github.com/microsoft/unilm/tree/master/wavlm): speech pre-training for full stack tasks

> [**VALL-E**](https://github.com/microsoft/unilm/tree/master/valle): a neural codec language model for TTS

### [](https://github.com/microsoft/unilm#multimodal-x--language)Multimodal (X + Language)

> [**LayoutLM**](https://github.com/microsoft/unilm/tree/master/layoutlm)/[**LayoutLMv2**](https://github.com/microsoft/unilm/tree/master/layoutlmv2)/[**LayoutLMv3**](https://github.com/microsoft/unilm/tree/master/layoutlmv3): multimodal (text + layout/format + image) **Document Foundation Model** for [Document AI](https://www.microsoft.com/en-us/research/project/document-ai/) (e.g. scanned documents, PDF, etc.)

> [**LayoutXLM**](https://github.com/microsoft/unilm/tree/master/layoutxlm): multimodal (text + layout/format + image) **Document Foundation Model** for multilingual Document AI

> [**MarkupLM**](https://github.com/microsoft/unilm/tree/master/markuplm): markup language model pre-training for visually-rich document understanding

> [**XDoc**](https://github.com/microsoft/unilm/tree/master/xdoc): unified pre-training for cross-format document understanding

> [**UniSpeech**](https://arxiv.org/abs/2101.07597): unified pre-training for self-supervised learning and supervised learning for ASR

> [**UniSpeech-SAT**](https://arxiv.org/pdf/2110.05752.pdf): universal speech representation learning with speaker-aware pre-training

> [**SpeechT5**](https://arxiv.org/abs/2110.07205): encoder-decoder pre-training for spoken language processing

> [**SpeechLM**](https://arxiv.org/abs/2209.15329): Enhanced Speech Pre-Training with Unpaired Textual Data

> [**VLMo**](https://github.com/microsoft/unilm/tree/master/vlmo): Unified vision-language pre-training

> [**VL-BEiT**](https://github.com/microsoft/unilm/tree/master/vl-beit) (`NEW`): Generative Vision-Language Pre-training - evolution of **BEiT** to multimodal

> [**BEiT-3**](https://github.com/microsoft/unilm/tree/master/beit3) (`NEW`): a general-purpose multimodal foundation model, and a major milestone of **The Big Convergence** of Large-scale Pre-training Across Tasks, Languages, and Modalities.

### [](https://github.com/microsoft/unilm#toolkits)Toolkits

> [**s2s-ft**](https://github.com/microsoft/unilm/tree/master/s2s-ft): sequence-to-sequence fine-tuning toolkit

> [**Aggressive Decoding**](https://arxiv.org/pdf/2205.10350.pdf) (`NEW`): lossless and efficient sequence-to-sequence decoding algorithm

### [](https://github.com/microsoft/unilm#applications)Applications

> [**TrOCR**](https://github.com/microsoft/unilm/tree/master/trocr): transformer-based OCR w/ pre-trained models

> [**LayoutReader**](https://github.com/microsoft/unilm/tree/master/layoutreader): pre-training of text and layout for reading order detection

> [**XLM-T**](https://github.com/microsoft/unilm/tree/master/xlmt): multilingual NMT w/ pretrained cross-lingual encoders

## [](https://github.com/microsoft/unilm#links)Links

### [](https://github.com/microsoft/unilm#llmops-repo)LLMOps ([repo](https://github.com/microsoft/lmops))

General technology for enabling AI capabilities w/ LLMs and MLLMs.


