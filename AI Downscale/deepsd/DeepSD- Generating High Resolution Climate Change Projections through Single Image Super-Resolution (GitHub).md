
The code in this repository contains the Stacked Super-resolution CNN proposed in our recent KDD paper, [DeepSD](http://www.kdd.org/kdd2017/papers/view/deepsd-generating-high-resolution-climate-change-projections-through-single).

When cloning the repository, make sure to use the `--recursive` argument as DeepSD directly relys on our [SRCNN library](https://github.com/tjvandal/srcnn-tensorflow). Ie. `git clone --recursive https://github.com/tjvandal/deepsd.git`.



https://github.com/tjvandal/deepsd