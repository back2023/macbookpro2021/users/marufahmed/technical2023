


## About

A project on how to incorporate physics constraints into deep learning architectures for downscaling or other super--resolution tasks.

[https://arxiv.org/pdf/2208.05424.pdf](https://arxiv.org/pdf/2208.05424.pdf)

https://github.com/paulaharder/constrained-downscaling



# Physics-constrained deep learning for climate downscaling

This code belongs to a paper currently under review, a preprint can be found at: [https://arxiv.org/pdf/2208.05424.pdf](https://arxiv.org/pdf/2208.05424.pdf)
https://github.com/RolnickLab/constrained-downscaling
