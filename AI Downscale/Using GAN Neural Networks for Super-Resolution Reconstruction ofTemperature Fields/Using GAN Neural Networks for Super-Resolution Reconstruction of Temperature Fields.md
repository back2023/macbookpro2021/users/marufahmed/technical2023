
https://www.techscience.com/iasc/v35n1/48180/pdf


### Abstract: 
A Generative Adversarial Neural (GAN) network is designed based on  
deep learning for the Super-Resolution (SR) reconstruction task of temperature  
fields (comparable to downscaling in the meteorological field), which is limited  
by the small number of ground stations and the sparse distribution of observa-  
tions, resulting in a lack of fineness of data. To improve the network’s generaliza-  
tion performance, the residual structure, and batch normalization are used.  
Applying the nearest interpolation method to avoid over-smoothing of the climate  
element values instead of the conventional Bicubic interpolation in the computer  
vision field. Sub-pixel convolution is used instead of transposed convolution or  
interpolation methods for up-sampling to speed up network inference. The experi-  
mental dataset is the European Centre for Medium-Range Weather Forecasts Rea-  
nalysis v5 (ERA5) with a bidirectional resolution of 0:1  0:1. On the other  
hand, the task aims to scale up the size by a factor of 8, which is rare compared  
to conventional methods. The comparison methods include traditional interpola-  
tion methods and a more widely used GAN-based network such as the SRGAN.  
The final experimental results show that the proposed scheme advances the per-  
formance of Root Mean Square Error (RMSE) by 37.25%, the Peak Signal-to-  
noise Ratio (PNSR) by 14.4%, and the Structural Similarity (SSIM) by 10.3%  
compared to the Bicubic Interpolation. For the traditional SRGAN network, a  
relatively obvious performance improvement is observed by experimental demon-  
stration. Meanwhile, the GAN network can converge stably and reach the approx-  
imate Nash equilibrium for various initialization parameters to empirically  
illustrate the effectiveness of the method in the temperature fields.  
Keywords: Super-resolution; deep learning; ERA5 dataset; GAN networks