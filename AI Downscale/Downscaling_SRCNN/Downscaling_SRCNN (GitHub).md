
This is a code repository for "Statistical treatment of convolutional neural network super-resolution of inland surface wind for subgrid-scale variability quantification" by Getter, Bessac, Rudi and Feng (submitted Feb 2023). The submitted draft can be found [here](https://arxiv.org/pdf/2211.16708.pdf).

https://github.com/DanielGetter/Downscaling_SRCNN

