
https://github.com/jleinonen/downscaling-rnn-gan

This is a reference implementation of a stochastic, recurrent super-resolution GAN for downscaling time-evolving fields, intended for use in the weather and climate sciences domain. This code supports the paper "Stochastic Super-Resolution for Downscaling Time-Evolving Atmospheric Fields With a Generative Adversarial Network", IEEE Transactions in Geoscience and Remote Sensing, 2020, [https://doi.org/10.1109/TGRS.2020.3032790](https://doi.org/10.1109/TGRS.2020.3032790), the accepted version is available at [https://arxiv.org/abs/2005.10374](https://arxiv.org/abs/2005.10374).

## [](https://github.com/jleinonen/downscaling-rnn-gan#obtaining-the-data)Obtaining the data

The radar precipitation dataset (MCH-RZC in the paper) can be downloaded at [https://doi.org/10.7910/DVN/ZDWWMG](https://doi.org/10.7910/DVN/ZDWWMG) by following the instructions there. The GOES cloud optical thickness dataset (GOES-COT) can be found [in this data repository](https://doi.org/10.5281/zenodo.3835849) as "goes-samples-2019-128x128.nc".

## [](https://github.com/jleinonen/downscaling-rnn-gan#obtaining-the-trained-network)Obtaining the trained network

The trained generator weights selected for use in the paper are included in the `models` directory. The weights for the other time steps can be found [here](https://doi.org/10.5281/zenodo.3835849).




