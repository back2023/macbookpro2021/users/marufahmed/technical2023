Next install try. 
### From the previous install file: (Working)
`/home/900/mah900/Technical/AI Downscale/DL4DS - Deep Learning for empirical DownScaling/05-Install.md`

```bash
module purge
rm -r /scratch/fp0/mah900/env/dl4ds
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"

conda create -p /scratch/fp0/mah900/env/dl4ds python=3.8 cartopy jupyterlab scipy matplotlib basemap -c conda-forge
conda activate /scratch/fp0/mah900/env/dl4ds 

cd /scratch/fp0/mah900/dl4ds
module load  cuda/11.2.2
module load cudnn/8.1.1-cuda11
python -m pip uninstall tensorflow
python -m pip install numpy==1.20.* tensorflow==2.10 dl4ds climetlab climetlab_maelstrom_downscaling scikit-learn --no-cache-dir
```
(Install Working)
### ARE:  (Working)
```bash
1gpu
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00+gdata/rt52+gdata/hh5+scratch/fp0

cuda/11.2.2 cudnn/8.1.1-cuda11
/scratch/fp0/mah900/Miniconda2023
/scratch/fp0/mah900/env/dl4ds/
```

Install Numba
```bash
conda install numba
```
https://numba.pydata.org/numba-doc/latest/user/installing.html


# 2023.05.13
### Install `ipywidgets` (Not Worked)
Source: https://ipywidgets.readthedocs.io/en/stable/user_install.html#installation
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/dl4ds 
module load  cuda/11.2.2 cudnn/8.1.1-cuda11
# Does not work, just shows numeric values
# conda install -c conda-forge ipywidgets 
# conda uninstall ipywidgets

# Does not work either 
# pip install ipywidgets
```
### `ipywidgets` - Another Try, Reinstall (Working)
Source: https://github.com/jupyter-widgets/ipywidgets/issues/2552#issuecomment-528961566
```bash
conda uninstall jupyterlab ipywidgets nodejs 
pip uninstall ipywidgets

conda install -c conda-forge nodejs ipywidgets jupyterlab
jupyter labextension install @jupyter-widgets/jupyterlab-manager
```

##### Error-1 
```
Javascript Error: IPython is not defined
```
Need to Install   `ipympl`
Source: https://stackoverflow.com/questions/51922480/javascript-error-ipython-is-not-defined-in-jupyterlab
Tried (Not worked)
```bash
conda install -c conda-forge ipympl
jupyter lab build
```
Try later --> reinstalling all. 

# 2023.05.14
### Install `tabulate` (Working)
Source: https://www.statology.org/create-table-in-python/
```bash
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/dl4ds 
module load  cuda/11.2.2 cudnn/8.1.1-cuda11
pip install tabulate
```

Error-1: `TypeError: module object is not callable`
Source: https://www.freecodecamp.org/news/typeerror-module-object-is-not-callable-python-error-solved/
```bash
# Use
import tabulate as tb
# Instead of 
import tabulate
```