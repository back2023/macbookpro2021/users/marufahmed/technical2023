
* Try installing again, after founding out that old install (`empirical_downscaling`) is not working

### Installing in a new Conda env
```bash
cd /scratch/fp0/mah900
git clone https://github.com/carlos-gg/dl4ds.git

. /scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh
conda create -p /scratch/fp0/mah900/env/dl4ds -c anaconda python=3.9 

conda activate /scratch/fp0/mah900/env/dl4ds
pip install dl4ds
```
Source: [https://github.com/carlos-gg/dl4ds](https://github.com/carlos-gg/dl4ds)

###### Error-1
```bash
lib/cartopy/trace.cpp:767:10: fatal error: geos_c.h: No such file or directory
       #include "geos_c.h"
                ^~~~~~~~~~
      compilation terminated.
      error: command '/opt/nci/bin/gcc' failed with exit code 1
      [end of output]
```
Try
```bash
conda install -c conda-forge cartopy
```
Source: https://github.com/SciTools/cartopy/issues/1471#issuecomment-686277026

Test
```python
python

import numpy as np
import xarray as xr
import ecubevis as ecv
import dl4ds as dds
import scipy as sp
import netCDF4 as nc
import climetlab as cml

import tensorflow as tf 
from tensorflow import keras
from tensorflow.keras import models
```
###### Error-2: 
import error
Try: 
running `pip install dl4ds` the second time. 
Working

###### Error-3
```bash
ModuleNotFoundError: No module named 'sklearn'
...
ModuleNotFoundError: No module named 'climetlab'
```
Try
```bash
pip3 install -U scikit-learn scipy matplotlib
pip3 install climetlab 
```
sklearn: https://stackoverflow.com/questions/46113732/modulenotfounderror-no-module-named-sklearn
climetlab: https://climetlab.readthedocs.io/en/0.9.7/installing.html
Test again
```python
import dl4ds as dds
import climetlab as cml
```
Warning
```bash
TF-TRT Warning: Could not find TensorRT
```
That warning is issued because internally TensorFlow calls the TensorRT optimizer for certain objects unnecessarily so the warning can be ignored.
Source: https://github.com/tensorflow/tensorrt/issues/252#issuecomment-862327018

Install Jupyter
```bash
conda install -c anaconda jupyter
```
For new users, we highly recommend installing Anaconda. 
(source: https://test-jupyter.readthedocs.io/en/latest/install.html)

### ARE: 
(Does not work)
```bash
1gpu
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00+gdata/rt52+gdata/hh5+scratch/fp0

cuda/11.6.1 cudnn/8.6.0-cuda11 
/scratch/fp0/mah900/Miniconda2023
/scratch/fp0/mah900/env/dl4ds/

```
Train error: Tensorflow

### Start another install
```bash
# Clean Env
module purge
conda deacitvate
rm -rf /scratch/fp0/mah900/env/dl4ds

# Parameters
module load cuda/11.6.1 cudnn/8.6.0-cuda11 
. /scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh

# Create and install 
conda create -p /scratch/fp0/mah900/env/dl4ds -c conda-forge -c anaconda python=3.9 cartopy jupyter
conda activate /scratch/fp0/mah900/env/dl4ds

export SKLEARN_ALLOW_DEPRECATED_SKLEARN_PACKAGE_INSTALL=True
pip3 install -U dl4ds scikit-learn scipy matplotlib climetlab --no-cache-dir
#conda install -c conda-forge -c anaconda cartopy jupyter
#pip install dl4ds --no-cache-dir
#pip3 install -U scikit-learn scipy matplotlib  --no-cache-dir
#pip3 install climetlab --no-cache-dir
pip install jupyterlab
```
Pip options, source: https://www.mankier.com/1/pip
###### Error-4
```bash
  × python setup.py egg_info did not run successfully.
  │ exit code: 1
  ╰─> [18 lines of output]
		The 'sklearn' PyPI package is deprecated, use 'scikit-learn'
		...
		- replace 'sklearn' by 'scikit-learn' in your pip requirements files
		  (requirements.txt, setup.py, setup.cfg, Pipfile, etc ...)
		...
		- as a last resort, set the environment variable
		  SKLEARN_ALLOW_DEPRECATED_SKLEARN_PACKAGE_INSTALL=True to avoid this error
```
Error-5
```bash
Jupyter: /scratch/fp0/mah900/env/dl4ds/bin/jupyter
ERROR: Jupyter Lab exited with non-zero code (1)
```
Tty: `pip install jupyterlab` (https://stackoverflow.com/questions/57403953/why-jupyterlab-not-installed)

Does not work.
Next try, in next install file [[Install - 2023.04.21]]