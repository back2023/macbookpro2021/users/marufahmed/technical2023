##### 1. Script to download all daily data

```bash
# /scratch/fp0/mah900/empirical_downscaling/daily_01.sh

cd /g/data/wb00/admin/staging/TERN/monthly_data

# Max temp
wget -c https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/MaximumTemperature/tmaxscr.monthly.ccam10_ACCESS1-0Q_rcp45.nc

# Mean temp
wget -c https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/MeanTemperature/tscr_ave.monthly.ccam10_ACCESS1-0Q_rcp45.nc

# Min temp
wget -c https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/MinimumTemperature/tminscr.monthly.ccam10_ACCESS1-0Q_rcp45.nc

# Pan Eva 
wget -c https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/PanEvaporation/epan_ave.monthly.ccam10_ACCESS1-0Q_rcp45.nc

# Preci
wget -c https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/Precipitation/rnd24.monthly.ccam10_ACCESS1-0Q_rcp45.nc

# Relative Humid
wget -c https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/RelativeHumidity/rhscrn.monthly.ccam10_ACCESS1-0Q_rcp45.nc

# Solar Rad
wget -c https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/SolarRadiation/sgdn_ave.monthly.ccam10_ACCESS1-0Q_rcp45.nc

# Wind speed
wget -c https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/WindSpeed/u10.monthly.ccam10_ACCESS1-0Q_rcp45.nc

```


```bash
time /scratch/fp0/mah900/empirical_downscaling/daily_01.sh
...
real 3m22.432s
user 0m6.634s
sys 0m15.562s
```

##### 2. Change 'TERN-04.ipynb' notebook to be capable of running different nc files

```bash
#
# scratch/fp0/mah900/empirical_downscaling/TERN-04.ipynb
# File is changed
mkdir -p /g/data/wb00/admin/staging/TERN/monthly_train


```


##### 3. Change 'TERN-03.ipynb' notebook into a gallery 

```
# scratch/fp0/mah900/empirical_downscaling/TERN-03.ipynb
```