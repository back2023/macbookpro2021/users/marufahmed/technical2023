
Outperforms SRCNN, VDSR, SRResNet, Won the NTIRE2017 Super-Resolution Challenge

Inthis story, **Enhanced Deep Super-Resolution Network (EDSR) and Multi-Scale Deep Super-Resolution System (MDSR)**, by Seoul National University, is reviewed.

https://sh-tsang.medium.com/review-edsr-mdsr-enhanced-deep-residual-networks-for-single-image-super-resolution-super-4364f3b7f86f