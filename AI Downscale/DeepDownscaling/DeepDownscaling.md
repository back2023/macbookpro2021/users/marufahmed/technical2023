

### Deep learning approaches for statistical downscaling in climate

Transparency and reproducibility are key ingredients to develop top-quality science. For this reason, this repository is aimed at hosting and maintaining updated versions of the code and notebooks needed to (partly or fully) reproduce the results of the papers developed in the Santander MetGroup dealing with the application of deep learning techniques for statistical dowscaling in climate.


https://github.com/SantanderMetGroup/DeepDownscaling#deep-learning-approaches-for-statistical-downscaling-in-climate


