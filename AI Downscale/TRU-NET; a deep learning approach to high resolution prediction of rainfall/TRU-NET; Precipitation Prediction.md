
https://github.com/Rilwan-Adewoyin/TRUNET

Code for predicting precipitation using model field data from IFS-ERA5 (temperature, geopotential, wind velocity, etc.) as predictors for sub-areas across the British Isle. The Data used in this project is available at the following Google Drive link. Users must download and decompress the Rain_Data_Mar20 folder. Keywords: TRU_NET, Downscaling, Cross Attention, Hierarchical GRU



