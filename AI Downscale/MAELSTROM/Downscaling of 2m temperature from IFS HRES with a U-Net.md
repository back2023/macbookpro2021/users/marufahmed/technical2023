https://git.ecmwf.int/projects/MLFET/repos/maelstrom-downscaling-ap5/browse/notebooks/demo_downscaling_dataset.ipynb

### maelstrom-nogwd
https://git.ecmwf.int/projects/MLFET/repos/maelstrom-nogwd/browse

### maelstrom-downscaling-ap5
https://git.ecmwf.int/projects/MLFET/repos/maelstrom-downscaling-ap5/browse

### [maelstrom-downscaling-ap5](https://git.ecmwf.int/projects/MLFET/repos/maelstrom-downscaling-ap5/browse)/[notebooks](https://git.ecmwf.int/projects/MLFET/repos/maelstrom-downscaling-ap5/browse/notebooks)/demo_downscaling_dataset.ipynb
https://git.ecmwf.int/projects/MLFET/repos/maelstrom-downscaling-ap5/browse/notebooks/demo_downscaling_dataset.ipynb

## maelstrom-downscaling-ap5
https://git.ecmwf.int/projects/MLFET/repos/maelstrom-downscaling-ap5/browse

### Downscaling from GCMs 
https://confluence.csiro.au/display/CCAM/Downscaling+from+GCMs


### Downscaling of 2m temperature from IFS HRES with a U-Net
https://git.ecmwf.int/projects/MLFET/repos/maelstrom-downscaling-ap5/browse/notebooks/demo_downscaling_dataset.ipynb
