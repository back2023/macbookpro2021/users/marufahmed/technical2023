
# Downscaling

Climate Downscaling using Computer Vision Techniques

This is a python implementation for the KDD 2020 paper "Climate Downscaling Using YNet: a Deep Convolutional Network with Skip Connections and Fusion"


https://github.com/yuminliu/Downscaling