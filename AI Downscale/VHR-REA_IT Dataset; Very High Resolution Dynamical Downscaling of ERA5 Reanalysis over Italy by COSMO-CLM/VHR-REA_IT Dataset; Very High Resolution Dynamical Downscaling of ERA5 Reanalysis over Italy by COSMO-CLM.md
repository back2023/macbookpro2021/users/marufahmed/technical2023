
https://www.academia.edu/73829031/VHR_REA_IT_Dataset_Very_High_Resolution_Dynamical_Downscaling_of_ERA5_Reanalysis_over_Italy_by_COSMO_CLM

This work presents a new dataset for recent climate developed within the Highlander project by dynamically downscaling ERA5 reanalysis, originally available at ≃31 km horizontal resolution, to ≃2.2 km resolution (i.e., convection permitting scale). Dynamical downscaling was conducted through the COSMO Regional Climate Model (RCM). The temporal resolution of output is hourly (like for ERA5). Runs cover the whole Italian territory (and neighboring areas according to the necessary computation boundary) to provide a very detailed (in terms of space–time resolution) and comprehensive (in terms of meteorological fields) dataset of climatological data for at least the last 30 years (01/1989-12/2020). These types of datasets can be used for (applied) research and downstream services (e.g., for decision support systems).


