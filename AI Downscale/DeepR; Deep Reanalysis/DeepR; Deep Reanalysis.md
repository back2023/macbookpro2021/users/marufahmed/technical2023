
https://github.com/ECMWFCode4Earth/DeepR





Global reanalysis downscaling to regional scales by means of deep learning techniques.

- The DeepR project, led by Antonio Pérez, Mario Santa Cruz, and Javier Diez, was conceived and executed with the aim of downscaling ERA5 data to a finer resolution, thus enabling enhanced accuracy and applicability across a wide range of industries and research domains.


## Project Motivation
- The ERA5 Global reanalysis data, with its spatial resolution of approximately 0.25 degrees, 
	- Still, its limitations in resolution can hinder precise decision-making and analysis across diverse domains. 
- The primary motivation behind the DeepR project was to bridge this gap by downscaling ERA5 data to a finer resolution of approximately 0.05 degrees, termed as CERRA resolution. 
	- This enhancement aimed to unlock the full potential of climate data for improved decision support.

## Super-Resolution in Climate Science

- The project drew inspiration from the field of image processing and computer vision, specifically the concept of super-resolution. 
- In image processing, super-resolution involves augmenting the resolution or quality of an image, typically generating a high-resolution image from one or more low-resolution iterations. 
- DeepR adapted this concept to climate science, making it a super-resolution task tailored to atmospheric fields.
  
  
## Data: The Foundation of DeepR

- The project relies on extensive datasets sourced from the publicly accessible Climate Data Store (CDS), ensuring transparency and open access to valuable climate information.
- The data used in this project has been generously provided by our mentors and is used in its raw form without any processing. To download the data from the repository, you can access the [`european_weather_cloud.py`](https://github.com/ECMWFCode4Earth/DeepR/blob/main/scripts/download/european_weather_cloud.py) script.
- Additionally, we have developed a script to directly download data from the Climate Data Store. You can find this script at [`climate_data_store.py`](https://github.com/ECMWFCode4Earth/DeepR/blob/main/scripts/download/climate_data_store.py).



### Focus on a specific domain

To achieve this spatial selection of the data, we utilize the [`data_spatial_selection.py`](https://github.com/ECMWFCode4Earth/DeepR/blob/main/scripts/processing/data_spatial_selection.py) script, which transforms the data into the desired domain.


## Modeling

The two main modeling approaches covered are:

### [](https://github.com/ECMWFCode4Earth/DeepR#diffusion-model)Diffusion model


### Tailored UNet


### Convolutional Swin2SR


### Training configuration: commons


