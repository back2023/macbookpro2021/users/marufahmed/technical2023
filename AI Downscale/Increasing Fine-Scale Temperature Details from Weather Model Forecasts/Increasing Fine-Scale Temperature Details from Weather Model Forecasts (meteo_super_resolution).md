
https://github.com/menardai/meteo_super_resolution

## Challenge

Increase the resolution (the level of detail) of 2D surface temperature forecasts obtained from Environment and Climate Change Canada (ECCC)’s weather forecast model, using as labelled images 2D temperature analysis at higher resolution. The scale factor between the model and the higher resolution analysis is 4 (from 10 km to 2.5 km).


