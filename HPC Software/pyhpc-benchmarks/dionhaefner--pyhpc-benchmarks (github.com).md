
https://github.com/dionhaefner/pyhpc-benchmarks


# HPC benchmarks for Python

This is a suite of benchmarks to test the _sequential CPU_ and GPU performance of various computational backends with Python frontends.

Specifically, we want to test which high-performance backend is best for _geophysical_ (finite-difference based) _simulations_.







