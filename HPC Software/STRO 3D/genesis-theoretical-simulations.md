
The ASTRO 3D Genesis Theoretical Simulation program, led by CIs Stuart Wyithe, Chris Power and Darren Croton, uses large-scale super-computer theoretical simulations to study galaxy formation and evolution over the last 12 billion years of cosmic time.

https://astro3d.org.au/genesis-theoretical-simulations/