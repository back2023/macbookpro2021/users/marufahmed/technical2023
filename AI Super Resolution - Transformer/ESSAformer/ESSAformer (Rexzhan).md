
https://github.com/Rexzhan/ESSAformer


# ESSAformer
[](https://github.com/Rexzhan/ESSAformer#essaformer)

The implementation of ESSAformer, a network for Hyperspectral image super-resolution.

## 2023.10.11
[](https://github.com/Rexzhan/ESSAformer#20231011)

The core code and trained model on Chikuseix4 are uploaded.

## Enviornment
[](https://github.com/Rexzhan/ESSAformer#enviornment)

The code is tested under Pytorch 1.12.1, Python 3.8. And it might works on higer Pytorch version.




