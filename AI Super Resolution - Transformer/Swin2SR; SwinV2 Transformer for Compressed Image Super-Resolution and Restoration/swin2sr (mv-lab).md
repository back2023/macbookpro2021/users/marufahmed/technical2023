

## About

[ECCV] Swin2SR: SwinV2 Transformer for Compressed Image Super-Resolution and Restoration. Advances in Image Manipulation (AIM) workshop ECCV 2022. Try it out! over 3.3M runs [https://replicate.com/mv-lab/swin2sr](https://replicate.com/mv-lab/swin2sr)


