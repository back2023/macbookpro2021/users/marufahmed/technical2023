

https://github.com/AAleka/Retinal-Image-Super-Resolution-ViT-CNN


## Retinal Image Super-Resolution using Vision Transformer and Convolutional Neural Network

[](https://github.com/AAleka/Retinal-Image-Super-Resolution-ViT-CNN#retinal-image-super-resolution-using-vision-transformer-and-convolutional-neural-network)

1. Create anaconda environment using the following code: "conda env create -f env.yml".
    
2. Download EyeQ dataset and place them in datasets folder.
    
3. Run Train.py selecting the dataset.
    
4. Run Test.py selecting the dataset.
